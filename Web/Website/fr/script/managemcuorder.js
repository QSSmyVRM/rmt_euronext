/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
function movePosition(to) {
	var list = document.getElementById("BridgeList");
	var index = list.selectedIndex;
	var total = list.options.length-1;

	if (index == -1) return false;
	if (to == +1 && index == total) return false;
	if (to == -1 && index == 0) return false;

	var items = new Array;
	var values = new Array;

	for (i = total; i >= 0; i--) {
		items[i] = list.options[i].text;
		values[i] = list.options[i].value;
	}

	for (i = total; i >= 0; i--) {
		if (index == i) {
			list.options[i + to] = new Option(items[i],values[i], 0, 1);
			list.options[i] = new Option(items[i + to], values[i+to]);
			i--;
		}
		else {
			list.options[i] = new Option(items[i], values[i]);
		   }
	}
	list.focus();
}

function change_mcu_order_prompt(promptpicture, prompttitle, bridges, title1) {
    //ZD 100422 Starts
    
    var Title = "";
    if (title1 == "MCUs")
        Title = "MCU";
    else if (title1 == "Color days")
        Title = "Jour de couleur"
    else if (title1 == "Templates")
        Title = "Mod&#232;le";
    //ZD 100422 Ends
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");
    // FB 2050 Starts
	promptbox.position = 'absolute';
	promptbox.top = mousedownY - 5 + 'px';
	promptbox.left = mousedownX + 10 + 'px';
	promptbox.width = '280px'; //ZD 100422
	promptbox.height = '160px';//ZD 100422
	promptbox.backgroundColor = 'white';
	promptbox.border = 'outset 1px #bbbbbb';
    // FB 2050 Ends
	bsary = bridges.split("||")

	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td class='tableHeader'>" + prompttitle + "</td></tr></table>" 
//	Window Dressing
	m += "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='tablebody'>";
	m += "  <tr>";
//	Window Dressing
	m += "    <td align='left' class='blackblodtext'>" + Title + "</td>"; // FB 2579
	m += "    <td></td>";
	m += "  </tr>"
	m += "  <tr>";
	m += "    <td>"
    m += "      <select multiple name='BridgeList' style='height:65px' id='BridgeList' class='SelectFormat' tabindex='1' onblur='document.getElementById(\"cyanup\").focus();'>" // FB 2050 //ZD 100420
    for (i = 0; i < bsary.length - 1; i++) {
		bary = bsary[i].split("``");
		m += "<option value='" + bary[0] + "'>" + bary[1] + "</option>"
    }
    m += "      </select>"
	m += "    </td>"
	m += "    <td valign='middle'>"
	m += "      <a href='#' id='cyanup' tabindex='2' onclick='this.childNodes[0].click();return false;'><img border='0' src='image/cyan_up.gif' width='20' height='20' onClick='movePosition(-1)' alt='Cyan Up'></a>"//ZD 100419
	m += "      <br>"
	m += "      <a href='#' tabindex='3' onclick='this.childNodes[0].click();return false;'><img border='0' src='image/cyan_down.gif' width='20' height='20' onClick='movePosition(+1)' alt='Cyan Down'></a>"//ZD 100419
	m += "    </td>";
	m += "  </tr>"
	m += "  <tr>"
	m += "    <td colspan=1 >" //ZD 100422  Starts
	m += "      <span style='color: #666666;'>* S&#233;lectionner " + Title + " pour changer l'ordre</span>"
	m += "     </td>"//ZD 100422  Starts
	m += "  </tr>"
	m += "  <tr><td align='right' colspan=2>"
	//code added for Soft Edge button
	//m += "    <input type='button' class='altShortBlueButtonFormat' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveOrder(document.getElementById(\"BridgeList\"));'>"
	//m += "    <input type='button' class='altShortBlueButtonFormat' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "    <button tabindex='4' class='altShortBlueButtonFormat' style='width:80px' onClick='saveOrder(document.getElementById(\"BridgeList\"));'>Soumettre</button>"
	m += "    <button tabindex='5' class='altShortBlueButtonFormat' style='width:80px' onClick='cancelthis();'>Annuler</button>"
	m += "  </td></tr>"
	m += "</table>" 
	
	document.getElementById('prompt').innerHTML = m;
	document.getElementById('BridgeList').focus();
	
} 


function saveOrder(cb) 
{ 
	var list = document.getElementById("BridgeList");
	var neworder = "";
	
	for (i = 0; i < list.options.length; i++) {
		neworder += list.options[i].value + ";"
	}
	document.frmManagebridge.Bridges.value = neworder;
	frmsubmit('SAVE', '');
} 


function cancelthis()
{
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
}