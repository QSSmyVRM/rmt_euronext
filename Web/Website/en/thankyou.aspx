<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved	
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.		
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_Thankyou" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">


<html xmlns="http://www.w3.org/1999/xhtml" >
  
<head runat="server">
    <title><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, thankyou_title%>" runat="server"></asp:Literal></title>
  
</head>
<body>
    <form id="form1" runat="server">
     <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <div>
        <table width="100%" border="0" cellpadding="0" cellspacing="0">
            <tr valign="top"> 
              <td id="thankTop" runat="server"  width="100%" height="72" colspan="2">
              </td>
            </tr>
            <tr>
               <td valign="top" align="right" style="width:20%;" >
	              <br/><br/><br/><br/><br/><br/><br/><br/>
                  <img runat="server" src="../image/company-logo/SiteLogo.jpg" alt="<%$ Resources:WebResources, SuperAdministrator_SiteLogo%>" /> <%--FB 1830--%> <%--ZD 100419--%>
                </td>
                <td align="center">
	                 <p><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, thankyou_ThankyouYouh%>" runat="server"></asp:Literal></p>
	                    <p>Click <a href="../en/genlogin.aspx"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, thankyou_here%>" runat="server"></asp:Literal></a>
                        <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, loginagain%>" runat="server"></asp:Literal></p> <%--FB 1830--%>
	                    <%
                            if (Application["ssoMode"] != null && Application["ssoMode"].ToString().ToUpper() == "NO")
	                        {
                                if (Application["Client"] != null)
                                    if (Application["Client"].ToString().ToUpper().Equals("PSU"))
                                        Response.Redirect("https://webaccess.psu.edu/cgi-bin/logout");
		                                Response.Cookies["VRMuser"]["act"] = "";
		                                Response.Cookies["VRMuser"]["pwd"] = "";
                                        string browserlang = "en-US"; ////ZD 103174 start
                                        if (Session["loginbrowserlang"] != null && Session["loginbrowserlang"].ToString() != "")
                                            browserlang = Session["loginbrowserlang"].ToString(); //ZD 103174 end
                                        
                                            
                                          
                                // FB 2397 Starts
                                if (Request.QueryString["t"] != null)
                                    if (Request.QueryString["t"].ToString().Equals("1"))
                                        Response.Redirect("~/en/genlogin.aspx?m=2"); //ZD 103174 //ZD 103579
                                // FB 2397 Ends
                                Response.Redirect("~/en/genlogin.aspx?m=1"); //FB 1830 //ZD 103174 //ZD 103579
                            }
	                           
                        %>
                </td>
            </tr>            
        </table>
    </div>
    </form>
    <script language="javascript" type="text/javascript">
            var obj = document.getElementById('thankTop');
            if(obj != null)
            {
//            if (window.screen.width <= 1024)
//                obj.background = "../en/image/company-logo/StdBanner.jpg"; // Organization Css Module 
//            else
//                obj.background = "../en/image/company-logo/HighBanner.jpg";  //Organization Css Module 
            //FB 1830
            if (window.screen.width <= 1024)
                obj.background = "../image/company-logo/StdBanner.jpg";
            else
                obj.background = "../image/company-logo/HighBanner.jpg";  
            }
   
 
</script>
</body>
</html>
