<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.	
*
* You should have received a copy of the myVRM license with	
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>

<%@ Page Language="C#" Inherits="ns_ManageBatchReport.ManageBatchReport" Buffer="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%--FB 2779--%>
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%@ Register Assembly="DevExpress.Web.ASPxEditors.v10.2, Version=10.2.11.0, Culture=neutral,PublicKeyToken=b88d1754d700e49a"
    Namespace="DevExpress.Web.ASPxEditors" TagPrefix="dxAxEd" %>
<script type="text/javascript" src="script/cal-flat.js"></script>
<script type="text/javascript" src='../<%=Session["language"]%>/lang/calendar-en.js'></script>
<script type="text/javascript" src="script/calview.js"></script>
<script type="text/javascript" src="script/calendar-setup.js"></script>
<script type="text/javascript" src="script/calendar-flat-setup.js"></script>
<script language="javascript" type="text/javascript">
    //ZD 100604 start
    var img = new Image();
    img.src = "../en/image/wait1.gif";
    //ZD 100604 End
    function fnChangeOption(a1) {
        if (a1 == "5") {
            document.getElementById("CustomRow").style.display = "";
            document.getElementById("tdSpace").style.display = "";
            document.getElementById("DateRow1").style.display = "None";
            document.getElementById("DateRow2").style.display = "None";
            document.getElementById("DateRowEnd1").style.display = "None";
            document.getElementById("DateRowEnd2").style.display = "None";



            ValidatorEnable(document.getElementById("reqSDate"), false);
            ValidatorEnable(document.getElementById("reqEDate"), false);
        }
        else {
            document.getElementById("CustomRow").style.display = "None";
            document.getElementById("tdSpace").style.display = "None";
            document.getElementById("DateRow1").style.display = "";
            document.getElementById("DateRow2").style.display = "";
            document.getElementById("DateRowEnd1").style.display = "";
            document.getElementById("DateRowEnd2").style.display = "";

            //ValidatorEnable(document.getElementById("reqSDate"), true); 
            //ValidatorEnable(document.getElementById("reqEDate"), true); 
        }
    }

    function fnCancel() {
        DataLoading(1); //ZD 100176
        window.location.replace('organisationsettings.aspx');
    }

    function fnShowMenu() {
        var reportsList = document.getElementById("ReportsList");
        var type = document.getElementById("DrpAllType");
        var confScheRptDivList = document.getElementById("ConfScheRptDivList");
        var resourseType = document.getElementById("lstResourseType"); //FB 2410
        var rptListue;
        var lstUsageRpts;

        reportsList.value = ReportsList.GetValue();
        type = DrpAllType.GetValue();
        confScheRptDivList.value = ConfScheRptDivList.GetValue();
        lstUsageRpts = lstUsageReports.GetValue();

        if (reportsList)
            rptListValue = reportsList.value;

        //document.getElementById("lstResourseType").style.display = "Block";

        HideControls(rptListValue);
        document.getElementById("tblDate").style.display = "Block";
        switch (rptListValue) {
            case "1":
                document.getElementById("ConfScheRptCell1").style.display = "Block";
                document.getElementById("ConfScheRptCell2").style.display = "Block";

                if (confScheRptDivList.value == "5") {
                    document.getElementById("ConferenceRptCell").style.display = "Block";
                }
                else
                    document.getElementById("ConferenceRptCell").style.display = "None";

                break;
            case "2":
                document.getElementById("UserReportsCell1").style.display = "Block";
                document.getElementById("UserReportsCell2").style.display = "Block";
                document.getElementById("tblDate").style.display = "None";
                ValidatorEnable(document.getElementById("reqDFrom"), false);
                ValidatorEnable(document.getElementById("reqDTo"), false);
                break;
            case "3":
                document.getElementById("UsageReportCell1").style.display = "Block";
                document.getElementById("UsageReportCell2").style.display = "Block";
                break;
            case "4":
                document.getElementById("tdAllType1").style.display = "Block";
                document.getElementById("tdAllType2").style.display = "Block";
                var drpchrtValue = DrpChrtType.GetValue();
                if (type == "1") {
                    document.getElementById("tdConfType1").style.display = "block";
                    document.getElementById("tdConfType2").style.display = "block";
                }
                break;
            case "5":
                document.getElementById("tdDaily1").style.display = "block";
                document.getElementById("tdDaily2").style.display = "block";
                document.getElementById("tdStatus1").style.display = "block";
                document.getElementById("tdStatus2").style.display = "block";
                break;
            case "6":

                resourseType.value = lstResourseType.GetValue();
                var rType = resourseType.value;
                if (rType != "5" && rType != "7") {
                    document.getElementById("tblDate").style.display = "None";
                    ValidatorEnable(document.getElementById("reqDFrom"), false);
                    ValidatorEnable(document.getElementById("reqDTo"), false);
                }
                document.getElementById("tdResourse1").style.display = "block";
                document.getElementById("tdResourse2").style.display = "block";
                if (resourseType.value == "1") {
                    document.getElementById("tdUser1").style.display = "block";
                    document.getElementById("tdUser2").style.display = "block";
                }
                else if (resourseType.value == "2") {
                    document.getElementById("tdRoom1").style.display = "block";
                    document.getElementById("tdRoom2").style.display = "block";
                }
                else if (resourseType.value == "3" || resourseType.value == "4") {
                    document.getElementById("tdRoom1").style.display = "none";
                    document.getElementById("tdRoom2").style.display = "none";
                    document.getElementById("tdUser1").style.display = "none";
                    document.getElementById("tdUser2").style.display = "none";
                }
                else if (resourseType.value == "5") {
                    document.getElementById("tdRoomConf1").style.display = "block";
                    document.getElementById("tdRoomConf2").style.display = "block";
                    //document.getElementById("DateSelRow").style.display = "Block";
                } //ZD 102468
                else if (resourseType.value == "7") {
                    document.getElementById("tdCustomOptions1").style.display = "block";
                    document.getElementById("tdCustomOptions2").style.display = "block";
                }
                break;
            case "7":
                document.getElementById("tdOrg1").style.display = "block";
                document.getElementById("tdOrg2").style.display = "block";
                document.getElementById("tdResourse1").style.display = "block";
                document.getElementById("tdResourse2").style.display = "block";
                if (resourseType.value == "1") {
                    document.getElementById("tdUser1").style.display = "block";
                    document.getElementById("tdUser2").style.display = "block";
                }
                else if (resourseType.value == "2") {
                    document.getElementById("tdRoom1").style.display = "block";
                    document.getElementById("tdRoom2").style.display = "block";
                }
                else if (resourseType.value == "3" || resourseType.value == "4") {
                    document.getElementById("tdRoom1").style.display = "none";
                    document.getElementById("tdRoom2").style.display = "none";
                    document.getElementById("tdUser1").style.display = "none";
                    document.getElementById("tdUser2").style.display = "none";
                }
                else if (resourseType.value == "5") {
                    document.getElementById("tdRoomConf1").style.display = "block";
                    document.getElementById("tdRoomConf2").style.display = "block";
                    //document.getElementById("DateSelRow").style.display = "Block";
                }
                break;
        }
        return true;
    }

    function HideControls() {
        var args = HideControls.arguments;
        if (args[0] != "4") {
            document.getElementById("tdAllType1").style.display = "none";
            document.getElementById("tdAllType2").style.display = "none";
        }

        if (args[0] != "1") {
            document.getElementById("ConfScheRptCell1").style.display = "none";
            document.getElementById("ConfScheRptCell2").style.display = "none";
        }

        document.getElementById("tdConfType1").style.display = "none";
        document.getElementById("tdConfType2").style.display = "none";
        document.getElementById("UsageReportCell1").style.display = "none";
        document.getElementById("UsageReportCell2").style.display = "none";
        document.getElementById("UserReportsCell1").style.display = "none";
        document.getElementById("UserReportsCell2").style.display = "none";
        document.getElementById("tdDaily1").style.display = "none";
        document.getElementById("tdDaily2").style.display = "none";
        document.getElementById("tdStatus1").style.display = "none";
        document.getElementById("tdStatus2").style.display = "none";
        document.getElementById("tdOrg1").style.display = "none";
        document.getElementById("tdOrg2").style.display = "none";
        document.getElementById("tdUser1").style.display = "none";
        document.getElementById("tdUser2").style.display = "none";
        document.getElementById("tdRoom1").style.display = "none";
        document.getElementById("tdRoom2").style.display = "none";
        document.getElementById("tdRoomConf1").style.display = "none";
        document.getElementById("tdRoomConf2").style.display = "none";
        //FB 2410
        document.getElementById("tdResourse1").style.display = "none";
        document.getElementById("tdResourse2").style.display = "none";
        document.getElementById("ConferenceRptCell").style.display = "None"; //ZD 101708
        document.getElementById("tdCustomOptions1").style.display = "none"; //ZD 102468
        document.getElementById("tdCustomOptions2").style.display = "none";
        

    }

    function isOverInstanceLimit(cb) {
        csl = parseInt("<%=CustomSelectedLimit%>");

        if (!isNaN(csl)) {
            if (cb.length >= csl) {
                alert(EN_211)
                return true;
            }
        }

        return false;
    }

    function SortDates() {
        var temp;
        datecb = document.frmManageReport.CustomDate;
        var dateary = new Array();

        for (var i = 0; i < datecb.length; i++) {
            dateary[i] = datecb.options[i].value;

            dateary[i] = ((parseInt(dateary[i].split("/")[0], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[0], 10) : parseInt(dateary[i].split("/")[0], 10)) + "/" + ((parseInt(dateary[i].split("/")[1], 10) < 10) ? "0" + parseInt(dateary[i].split("/")[1], 10) : parseInt(dateary[i].split("/")[1], 10)) + "/" + (parseInt(dateary[i].split("/")[2], 10));
        }

        for (i = 0; i < dateary.length - 1; i++)
            for (j = i + 1; j < dateary.length; j++)
                if (mydatesort(dateary[i], dateary[j]) > 0) {
                    temp = dateary[i];
                    dateary[i] = dateary[j];
                    dateary[j] = temp;
                }

        for (var i = 0; i < dateary.length; i++) {
            datecb.options[i].text = dateary[i];
            datecb.options[i].value = dateary[i];
        }

        return false;
    }


    function removedate(cb) {
        if (cb.selectedIndex != -1) {
            cb.options[cb.selectedIndex] = null;
        }
        cal.refresh();
    }

    //ZD 100176 start
    function DataLoading(val) {
        if (val == "1")
            document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
        else
            document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
    }
    //ZD 100176 End

    //ZD 101708
    function deleteApprover(id) {
        eval("document.getElementById('hdnApprover" + (id) + "')").value = "";
        eval("document.getElementById('txtApprover" + (id) + "')").value = "";
    }

    function getVNOCEmailListnew() {
        var Confvnoc = document.getElementById("hdnVNOCOperator");
        url = "VNOCparticipantlist.aspx?cvnoc=" + Confvnoc.value;
        if (!window.winrtc) {
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        }
        else if (!winrtc.closed) {
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        }
        else {
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
            winrtc.focus();
        }
    }

    function deleteVNOC() {
        document.getElementById('txtRVNOCOperator').value = "";
        document.getElementById('hdnVNOCOperator').value = "";
    }

    function getYourOwnEmailListNew(i) {
        url = "emaillist2main.aspx?t=e&frm=approverNET&fn=Setup&n=" + i;
        if (!window.winrtc) {
            winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
        else
            if (!winrtc.closed) {
                winrtc.close();
                winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
                winrtc.focus();
            } else {
                winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
                winrtc.focus();
            }
    }

    function fnShowRooms() {
        var _cntrl = document.getElementById('<%=RoomDiv.ClientID%>');
        if (_cntrl.style.display == 'none')
            _cntrl.style.display = 'block';
        else
            _cntrl.style.display = 'none';
    }

    function fnSelectAll() {
        var args = fnSelectAll.arguments;
        var rooms = '';
        if (args[0] != null) {
            if (args[1] != null) {
                var this_ = document.getElementById(args[1]);

                fnAssignChecked(this_, args[0].checked);
            }
        }
    }

    fnAssignDefault('S');

    function fnAssignDefault() {
        var args = fnAssignDefault.arguments
        if (args[0] == 'S')
            fnAssignValue('<%=cblRoom.ClientID%>');
        else {
            document.frmReports.chkSelectall.checked = true;
            fnSelectAll(document.frmReports.chkSelectall, '<%=cblRoom.ClientID%>');
            fnAssignValue('<%=cblRoom.ClientID%>');
        }
    }

    function fnAssignChecked(this_, checkValue) {

        if (this_ != null) {
            var checkBoxArray = this_.getElementsByTagName('input');

            for (var i = 0; i < checkBoxArray.length; i++) {
                var checkBoxRef = checkBoxArray[i];

                checkBoxRef.checked = checkValue;
            }
        }
    }


    function fnDeselectAll() {

        var args = fnDeselectAll.arguments;

        if (args[0] != null) {
            if (!args[0].checked) {
                if (args[1] != null) {
                    var allCheck = document.getElementById(args[1])
                    if (allCheck) {
                        if (allCheck.checked)
                            allCheck.checked = false;
                    }
                }
            }
        }

        var checkBoxArray = document.getElementById("cblRoom");
        var len = 0;
        if (navigator.userAgent.indexOf("MSIE") > -1)
            len = checkBoxArray.childNodes[0].childNodes.length;
        else
            len = checkBoxArray.childNodes[1].childNodes.length;

        for (var i = 0; i < len - 1; i++) {
            if (document.getElementById(cblRoom.id + '_' + [i]).checked == false)
                document.getElementById("chkSelectall").checked = false;
        }

    }


    function fnAssignValue() {
        if (document.frmManageReport != null)//ZD 102102
            document.frmManageReport.txtRooms.value = ''
        //document.frmManageReport.hdnRoomIDs.value = ''

        var args = fnAssignValue.arguments;
        var rooms = '';

        if (args[0] != null) {
            var this_ = document.getElementById(args[0])
            if (this_) {
                var checkBoxArray = this_.getElementsByTagName('input');
                var checkedValues = '';

                for (var i = 0; i < checkBoxArray.length; i++) {
                    var checkBoxRef = checkBoxArray[i];
                    if (checkBoxRef.checked) {

                        var labelArray = checkBoxRef.parentNode.getElementsByTagName('label');      //Edited for FF

                        if (labelArray.length > 0) {
                            if (rooms.length > 0)
                                rooms += ', ';

                            rooms += labelArray[0].innerHTML;
                        }
                    }
                }

                if (rooms != '')
                    document.frmManageReport.txtRooms.value = rooms;
            }
        }
    }

    document.onclick = getClickId;
    function getClickId(e) {
        e = e || window.event;
        e = e.target || e.srcElement;
        var idlist = e.id
        idlist += e.parentNode.id
        idlist += e.parentNode.parentNode.id
        idlist += e.parentNode.parentNode.parentNode.id
        idlist += e.parentNode.parentNode.parentNode.parentNode.id
        idlist += e.parentNode.parentNode.parentNode.parentNode.parentNode.id
        if (idlist.indexOf("imgRoom") > -1)
            return false;
        if (idlist.indexOf('RoomDiv') == -1)
            document.getElementById('RoomDiv').style.display = 'none';
    }


</script>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Manage Batch Report</title>
    <script type="text/javascript" src="inc/functions.js"></script>
    <script language="javascript" type="text/javascript" src="../en/Organizations/Original/Javascript/RGBColorPalette.js"> </script>
    <script type="text/javascript" src="script/settings2.js"></script>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css" />
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css" />
    <%--<link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main.css" />--%>
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" />
    <link href="Css/myVRMStyle.css" type="text/css" rel="stylesheet" />
    <%--ZD 101708--%>
    <style type="text/css">
        .style1
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            font-family: Arial;
            text-decoration: none;
            width: 54px;
        }
        .style2
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            font-family: Arial;
            text-decoration: none;
            width: 19%;
        }
        .style3
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            font-family: Arial;
            text-decoration: none;
            width: 100px;
        }
        .style4
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            font-family: Arial;
            text-decoration: none;
            width: 208px;
        }
        .style5
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            font-family: Arial;
            text-decoration: none;
            width: 19%;
        }
        .style6
        {
            font-weight: bold;
            font-size: 10pt;
            color: black;
            font-family: Arial;
            text-decoration: none;
            width: 208px;
            height: 31px;
        }
        .style7
        {
            height: 31px;
        }
    </style>
</head>
<body>
    <form id="frmManageReport" runat="server" method="post" onsubmit="DataLoading(1);">
    <%--ZD 100176--%>
    <%--ZD 101022 start--%>
    <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true">
        <Scripts>
            <asp:ScriptReference Path="~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>" />
        </Scripts>
    </asp:ScriptManager>
    <%--ZD 101022 End--%>
    <input type="hidden" runat="server" id="hdnDateList" />
    <input type="hidden" runat="server" id="hdnCompareDate" />
    <h3>
        <asp:Label ID="lblHeader" runat="server"></asp:Label>
    </h3>
    <table width="700px" align="center" style="border-collapse: collapse" border="0"
        cellspacing="0" cellpadding="0">
        <%-- FB 2767 --%>
        <tr>
            <td align="center" colspan="2">
                <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                <div id="dataLoadingDIV" align="center" style="display: none">
                    <img border='0' src='image/wait1.gif' alt="<asp:Literal Text='<%$ Resources:WebResources, Loading%>' runat='server' />" />
                </div>
                <%--ZD 100176--%>
                <%--ZD 100678 End--%>
            </td>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <table width="80%" cellpadding="4" cellspacing="0">
                    <tr>
                        <td  class="subtitleblueblodtext" >
                            <asp:Label ID="Label1" runat="server" Text="<%$ Resources:WebResources, ManageBatchReport_Label1%>"></asp:Label>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="80%" cellpadding="0" cellspacing="0" align="center">
                    <tr>
                        <td align="left" class="style6">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_Jobs%>"
                                runat="server"></asp:Literal>
                        </td>
                        <td class="style7">
                            <div>
                                <table border="0" style="margin-left: -3px">
                                    <tr>
                                        <td>
                                            <dxAxEd:ASPxComboBox ID="drpJobName" runat="server" SelectedIndex="0" Width="205"
                                                AutoPostBack="true" OnSelectedIndexChanged="DisplayDetailsByID" CssClass="altText"
                                                ClientInstanceName="drpJobName" TextField="JobName" ValueField="Id">
                                            </dxAxEd:ASPxComboBox>
                                        </td>
                                        <td>
                                            <asp:ImageButton ImageUrl="image/btn_delete.gif" runat="server" ID="delJobName" ToolTip="<%$ Resources:WebResources, DeleteJobName%>"
                                                OnClick="DeleteBatchReportConf" Visible="false" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" class="style4">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_ReportName%>"
                                runat="server"></asp:Literal>
                        </td>
                        <td>
                            <asp:TextBox ID="txtJob" runat="server" MaxLength="256" CssClass="altText" Width="203"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqJob" runat="server" ValidationGroup="Submit" ControlToValidate="txtJob"
                                ErrorMessage="<%$ Resources:WebResources, Required%>" Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regJob" ValidationGroup="Submit" ControlToValidate="txtJob"
                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr style="height: 10px;">
                    </tr>
                    <tr>
                        <td valign="top" class="style4" align="left">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_Reports%>"
                                runat="server"></asp:Literal>
                        </td>
                        <td align="left" nowrap="nowrap">
                            <dxAxEd:ASPxComboBox ID="ReportsList" runat="server" SelectedIndex="0" Width="205"
                                CssClass="altText" ClientInstanceName="ReportsList">
                                <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                <Items>
                                    <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ReportDetails_ConferenceSche%>"
                                        Value="1" />
                                    <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ReportDetails_UserReports%>"
                                        Value="2" />
                                    <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, GraphicalReport_UsageReports%>"
                                        Value="3" />
                                    <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, PersonalReports%>" Value="5" />
                                    <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, UtilizationReport%>" Value="8" />
                                </Items>
                            </dxAxEd:ASPxComboBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%;" nowrap="nowrap">
                            <div id="ConfScheRptCell1" style="display: none; padding-top: 10px">
                                <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_ConferenceSche%>"
                                    runat="server"></asp:Literal></b></div>
                        </td>
                        <td style="width: 145px;" nowrap="nowrap">
                            <div id="ConfScheRptCell2" style="display: none; padding-top: 10px">
                                <dxAxEd:ASPxComboBox ID="ConfScheRptDivList" runat="server" SelectedIndex="0" Width="205"
                                    CssClass="altSelectFormat" ClientInstanceName="ConfScheRptDivList">
                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                    <Items>
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, DailySchedule%>" Value="2" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, CalendarReport%>" Value="1" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, PRIAllocation%>" Value="3" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ResourceAllocation%>" Value="4" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ConferenceReports%>" Value="5"
                                            Selected="true" />
                                    </Items>
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr style="height: 5px">
                    </tr>
                    <tr>
                        <td>
                            <div id="UserReportsCell1" style="display: none; padding-top: 20px;" runat="server">
                                <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_UserReports%>"
                                    runat="server"></asp:Literal></b>
                            </div>
                        </td>
                        <td>
                            <div id="UserReportsCell2" style="display: none; padding-top: 20px;" runat="server">
                                <dxAxEd:ASPxComboBox ID="lstUserReports" runat="server" SelectedIndex="0" Width="205px"
                                    CssClass="altSelectFormat">
                                    <Items>
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ContactList%>" Value="1" />
                                    </Items>
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="UsageReportCell1" style="display: none; padding-top: 10px" runat="server">
                                <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_UsageReports%>"
                                    runat="server"></asp:Literal></b>
                            </div>
                        </td>
                        <td>
                            <div id="UsageReportCell2" style="display: none; padding-top: 10px" runat="server">
                                <dxAxEd:ASPxComboBox ID="lstUsageReports" runat="server" SelectedIndex="0" Width="205px"
                                    CssClass="altSelectFormat" ClientInstanceName="lstUsageReports">
                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="tdAllType1" style="display: none; padding-top: 10px">
                                <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_Category%>"
                                    runat="server"></asp:Literal></b>
                            </div>
                        </td>
                        <td>
                            <div id="tdAllType2" style="display: none; padding-top: 10px">
                                <dxAxEd:ASPxComboBox ID="DrpAllType" runat="server" SelectedIndex="0" Width="205px"
                                    CssClass="altSelectFormat" ClientInstanceName="DrpAllType">
                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="tdConfType1" style="display: none; padding-top: 10px">
                                <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_ConferenceType%>"
                                    runat="server"></asp:Literal></b>
                            </div>
                        </td>
                        <td>
                            <div id="tdConfType2" style="display: none; padding-top: 10px">
                                <dxAxEd:ASPxComboBox ID="lstConfType" runat="server" SelectedIndex="0" Width="205"
                                    CssClass="altSelectFormat">
                                    <Items>
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="1" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, AudioConferencesOnly%>" Value="6" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, VideoConferencesOnly%>" Value="2" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, PointtoPointOnly%>" Value="4" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, RoomConferencesOnly%>" Value="7" />
                                    </Items>
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="tdDaily1" style="display: none; padding-top: 10px">
                                <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_Monthly%>"
                                    runat="server"></asp:Literal></b>
                            </div>
                        </td>
                        <td>
                            <div id="tdDaily2" style="display: none; padding-top: 10px">
                                <dxAxEd:ASPxComboBox ID="lstDailyMonthly" runat="server" SelectedIndex="0" Width="205"
                                    CssClass="altSelectFormat" ClientInstanceName="lstDailyMonthly">
                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                    <Items>
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, CalendarWorkorder_btnMonthly%>"
                                            Value="2" />
                                    </Items>
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="tdStatus1" style="display: none; padding-top: 10px">
                                <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_Status%>"
                                    runat="server"></asp:Literal></b>
                            </div>
                        </td>
                        <td>
                            <div id="tdStatus2" style="display: none; padding-top: 10px">
                                <dxAxEd:ASPxComboBox ID="lstStatusType" runat="server" SelectedIndex="0" Width="205"
                                    CssClass="altSelectFormat" ClientInstanceName="lstStatusType">
                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                    <Items>
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Ongoing%>" Value="2" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Pending%>" Value="3" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Reservation%>" Value="4" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Terminated%>" Value="5" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Deleted%>" Value="6" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ConferenceSetup_Public%>"
                                            Value="7" />
                                    </Items>
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="tdOrg1" style="display: none; padding-top: 10px">
                                <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_SelectOrganiza%>"
                                    runat="server"></asp:Literal></b>
                            </div>
                        </td>
                        <td>
                            <div id="tdOrg2" style="display: none; padding-top: 10px">
                                <dxAxEd:ASPxComboBox ID="lstOrg" runat="server" SelectedIndex="0" Width="40%" CssClass="altSelectFormat"
                                    ValueField="OrgID" TextField="OrganizationName">
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="tdResourse1" style="display: none; padding-top: 10px">
                                <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_ResourceReport%>"
                                    runat="server"></asp:Literal></b></div>
                        </td>
                        <td>
                            <div id="tdResourse2" style="display: none; padding-top: 10px">
                                <dxAxEd:ASPxComboBox ID="lstResourseType" runat="server" SelectedIndex="0" Width="205"
                                    CssClass="altSelectFormat" ClientInstanceName="lstResourseType">
                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                    <Items>
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, UserReport%>" Value="1" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, RoomReport%>" Value="2" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, EndpointReport%>" Value="3" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, MCUReport%>" Value="4" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, DailyScheduleReport%>" Value="5" />
                                    </Items>
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="tdUser1" style="display: none; padding-top: 10px">
                                <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_UserReport%>"
                                    runat="server"></asp:Literal></b>
                            </div>
                        </td>
                        <td>
                            <div id="tdUser2" style="display: none; padding-top: 10px">
                                <dxAxEd:ASPxComboBox ID="lstUserType" runat="server" SelectedIndex="0" Width="50%"
                                    CssClass="altSelectFormat" ClientInstanceName="lstUserType">
                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                    <Items>
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, All%>" Value="1" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Active%>" Value="2" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Inactive%>" Value="3" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Blocked%>" Value="4" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, ScheduledUsage%>" Value="5" /> <%--ZD 102043--%>
                                    </Items>
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="tdRoom1" style="display: none; padding-top: 10px">
                                <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_RoomReports%>"
                                    runat="server"></asp:Literal></b>
                            </div>
                        </td>
                        <td>
                            <div id="tdRoom2" style="display: none; padding-top: 10px">
                                <dxAxEd:ASPxComboBox ID="lstRoomType" runat="server" SelectedIndex="0" Width="40%"
                                    CssClass="altSelectFormat" ClientInstanceName="lstRoomType">
                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                    <Items>
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Active%>" Value="1" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, Deactive%>" Value="2" />
                                    </Items>
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="tdRoomConf1" style="display: none; padding-top: 10px">
                                <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_DailySchedule%>"
                                    runat="server"></asp:Literal></b>
                            </div>
                        </td>
                        <td>
                            <div id="tdRoomConf2" style="display: none; padding-top: 10px">
                                <dxAxEd:ASPxComboBox ID="lstConfRoomRpt" runat="server" SelectedIndex="0" Width="83%"
                                    CssClass="altSelectFormat" ClientInstanceName="lstConfRoomRpt">
                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                    <Items>
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, UsagebyRoomScheduled%>" Value="1" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, UsagebyRoomActual%>" Value="2" />
                                        <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, UsagebyRoomConference%>" Value="3" />
                                    </Items>
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                     <%--ZD ZD 102468 Start--%>
                    <tr >
                        <td>
                            <div id="tdCustomOptions1" style="display: none; padding-top: 10px">
                            <b class="blackblodtext">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, CustomOptions%>" runat="server"></asp:Literal></b>
                            </div>
                        </td>
                        <td>
                            <div id="tdCustomOptions2" style="display: none; padding-top: 10px">
                                <dxAxEd:ASPxComboBox ID="lstCustomOptType" runat="server" SelectedIndex="0" Width="75%"
                                    CssClass="altSelectFormat" ClientInstanceName="lstCustomOptType">
                                    <ClientSideEvents TextChanged="function(s, e) { return fnShowMenu(); }" />
                                </dxAxEd:ASPxComboBox>
                            </div>
                        </td>
                    </tr>
                    <%--ZD ZD 102468 End--%>
                    <tr>
                        <td align="left" nowrap="nowrap" style="padding-top: 10px" class="style5">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_EmailAddresst%>"
                                runat="server"></asp:Literal>
                        </td>
                        <td style="padding-top: 10px">
                            <asp:TextBox ID="txtEmail" runat="server" MaxLength="256" CssClass="altText" Width="205"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="reqEmail" runat="server" ValidationGroup="Submit"
                                ControlToValidate="txtEmail" ErrorMessage="<%$ Resources:WebResources, Required%>"
                                Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="regEmail1_1" ValidationGroup="Submit" ControlToValidate="txtEmail"
                                Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidEmailAddress%>"
                                ValidationExpression="[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                            <asp:RegularExpressionValidator ID="regEmail1_2" ValidationGroup="Submit" ControlToValidate="txtEmail"
                                Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters14%>"
                                ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:#$%&'~]*$"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="80%" align="left" cellspacing="0" cellpadding="4">
                    <tr>
                        <td align="left" class="subtitleblueblodtext" colspan="2">
                                <asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_FrequencyofDe%>"
                                runat="server"></asp:Literal><br />
                            <dxAxEd:ASPxRadioButtonList ID="rblFrequency" ClientInstanceName="rblFrequency" RepeatDirection="Horizontal"
                                runat="server" ValueType="System.Int32" TextWrap="false">
                                <Border BorderWidth="0px"></Border>
                                <Items>
                                    <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, CalendarWorkorder_btnDaily%>"
                                        Value="1" Selected="true" />
                                    <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, CalendarWorkorder_btnWeekly%>"
                                        Value="2" />
                                    <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, BiWeekly%>" Value="3" />
                                    <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, CalendarWorkorder_btnMonthly%>"
                                        Value="4" />
                                    <dxAxEd:ListEditItem Text="<%$ Resources:WebResources, UISettings_Custom%>" Value="5" />
                                </Items>
                                <ClientSideEvents SelectedIndexChanged="function(s, e) {fnChangeOption(s.GetValue()); } " />
                            </dxAxEd:ASPxRadioButtonList>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%" border="0" cellpadding="4" cellspacing="3">
                    <tr>
                        <td width="1%">
                        </td>
                        <td valign="top" width="18%">
                            <div id="DateRow1" runat="server">
                            <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_DateRow1%>" runat="server"></asp:Literal></span>
                            </div>
                        </td>
                        <td width="32%" valign="top">
                            <div id="DateRow2" runat="server">
                                <dxAxEd:ASPxDateEdit ID="startDateedit" ClientInstanceName="startDateedit" runat="server"
                                    Width="205">
                                    <%--ZD 100423--%>
                                    <ClientSideEvents KeyDown="function(s, e) {
                                    if (e.htmlEvent.KeyCode == ASPxKey.Enter)
                                        s.HideDropDown();
                                }" GotFocus="function(s, e) {
                                    s.ShowDropDown();
                                }" />
                                </dxAxEd:ASPxDateEdit>
                                <asp:RequiredFieldValidator ID="reqSDate" runat="server" ValidationGroup="Submit"
                                    ControlToValidate="startDateedit" ErrorMessage="<%$ Resources:WebResources, Required%>"
                                    Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                        </td>
                        <td width="16%">
                            <div id="DateRowEnd1" runat="server">
                                <span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_DateRowEnd1%>" runat="server"></asp:Literal></span>
                                </div>
                        </td>
                        <td width="32%">
                            <div id="DateRowEnd2" runat="server">
                                <dxAxEd:ASPxDateEdit ID="endDateedit" ClientInstanceName="endDateedit" runat="server"
                                    Width="205">
                                    <%--ZD 100423--%>
                                    <ClientSideEvents KeyDown="function(s, e) {
                                    if (e.htmlEvent.KeyCode == ASPxKey.Enter)
                                        s.HideDropDown();
                                }" GotFocus="function(s, e) {
                                    s.ShowDropDown();
                                }" />
                                </dxAxEd:ASPxDateEdit>
                                <asp:RequiredFieldValidator ID="reqEDate" runat="server" ValidationGroup="Submit"
                                    ControlToValidate="endDateedit" ErrorMessage="<%$ Resources:WebResources, Required%>"
                                    Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                            </div>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="CustomRow" runat="server" style="display: none;">
            <td colspan="2">
                <div id="divCustom" style="z-index: 0; position: absolute;">
                    <table width="60%" border="0">
                        <tr valign="top">
                            <td align="left" valign="top" class="blackblodtext" width="25%" nowrap="nowrap">
                                <br />
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_CustomDates%>"
                                    runat="server"></asp:Literal>
                            </td>
                            <td align="left">
                                <table border="0" width="100%" cellspacing="0">
                                    <tr>
                                        <td style="width: 30%">
                                            <a href="#" class="name" onclick="return false">
                                                <div id="flatCalendarDisplay" style="float: right; clear: both; background-color: #E0E0E0">
                                                    <%--ZD 100426--%>
                                                </div>
                                            </a>
                                            <br />
                                            <div id="preview" style="font-size: 80%; text-align: center; padding: 2px">
                                            </div>
                                        </td>
                                        <td>
                                        </td>
                                        <td style="width: 20%" valign="top">
                                            <span class="blueSText"><b>
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_SelectedDate%>"
                                                    runat="server"></asp:Literal></b></span><br />
                                            <asp:ListBox runat="server" ID="CustomDate" Rows="8" CssClass="altSmall0SelectFormat"
                                                onchange="JavaScript: removedate(this);" onblur="javascript:document.getElementById('errLabel').innerHTML = '' "
                                                EnableViewState="true"></asp:ListBox>
                                            <br />
                                            <span class="blueSText">
                                                <asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_clickadateto%>"
                                                    runat="server"></asp:Literal></span>
                                        </td>
                                        <td>
                                            <%--ZD 100423--%>
                                            <button id="btnsortDates" runat="server" class="altMedium0BlueButtonFormat" onclick="javascript:return SortDates();"
                                                style="width: 90px">
                                                <asp:Literal Text='<%$ Resources:WebResources, ManageBatchReport_btnsortDates%>'
                                                    runat='server' /></button>
                                            <%--ZD 100420--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr id="tdSpace" style="display: none; height: 211px">
            <td colspan="2" style="height: 211px">
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <%-- FB 2767 Starts --%>
                <table id="tblDate" border="0" width="100%" cellpadding="4" cellspacing="3">
                    <tr>
                        <%-- FB 2917 Starts --%>
                        <td align="left" class="subtitleblueblodtext" colspan="5">
                            <asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_DefaultParamet%>"
                                runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td width="1%">
                        </td>
                        <td width="18%" valign="top">
                            <span class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_DateFrom%>" runat="server"></asp:Literal></span>
                            <%--FB 2917--%>
                        </td>
                        <td width="32%" valign="top">
                            <dxAxEd:ASPxDateEdit ID="dateFrom" ClientInstanceName="dateFrom" runat="server" Width="205">
                                <%--ZD 100423--%>
                                <ClientSideEvents KeyDown="function(s, e) {
                        if (e.htmlEvent.KeyCode == ASPxKey.Enter)
                            s.HideDropDown();
                    }" GotFocus="function(s, e) {
                        s.ShowDropDown();
                    }" />
                            </dxAxEd:ASPxDateEdit>
                            <asp:RequiredFieldValidator ID="reqDFrom" runat="server" ValidationGroup="Submit"
                                ControlToValidate="dateFrom" ErrorMessage="<%$ Resources:WebResources, Required%>"
                                Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                        <td width="16%" valign="top">
                            <span class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, ManageBatchReport_DateTo%>" runat="server"></asp:Literal></span>
                            <%--FB 2917--%>
                        </td>
                        <td valign="top" width="32%">
                            <dxAxEd:ASPxDateEdit ID="dateTo" ClientInstanceName="dateTo" runat="server" Width="205">
                                <%--ZD 100423--%>
                                <ClientSideEvents KeyDown="function(s, e) {
                        if (e.htmlEvent.KeyCode == ASPxKey.Enter)
                            s.HideDropDown();
                    }" GotFocus="function(s, e) {
                        s.ShowDropDown();
                    }" />
                            </dxAxEd:ASPxDateEdit>
                            <asp:RequiredFieldValidator ID="reqDTo" runat="server" ValidationGroup="Submit" ControlToValidate="dateTo"
                                ErrorMessage="<%$ Resources:WebResources, Required%>" Font-Bold="True" Display="Dynamic"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <table width="100%">
                    <tr>
                        <td id="ConferenceRptCell" style="display: none;" width="100%">
                            <table width="100%" border="0" cellpadding="4" cellspacing="3">
                                <tr id="QueryTypeRow" style="height:35px;">
                                    <td width="1%"></td>
                                    <td colspan="2" align="right">
                                        <span class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources, ReportDetails_OperatorsForB%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td colspan="2">
                                        <dxAxEd:ASPxComboBox ID="QueryType" runat="server" SelectedIndex="0" Width="152px"
                                            CssClass="altSelectFormat">
                                            <Items>
                                                <dxAxEd:ListEditItem Value="AND" Text="<%$ Resources:WebResources, AND%>"></dxAxEd:ListEditItem>
                                                <dxAxEd:ListEditItem Value="OR" Text="<%$ Resources:WebResources, OR%>"></dxAxEd:ListEditItem>
                                            </Items>
                                        </dxAxEd:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr id="ConferenceTitleRow">
                                    <td width="1%"></td>
                                    <td nowrap="nowrap">
                                        <span class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources, ReportDetails_ConferenceTitl%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="ConferenceName" runat="server" CssClass="altText" Enabled="true"
                                            MaxLength="256"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="regConfName" ControlToValidate="ConferenceName"
                                            ValidationGroup="btnviewRep" Display="dynamic" runat="server" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters12%>"
                                            ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                    </td>
                                    <td>
                                        <span class="blackblodtext">
                                            <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ReportDetails_Host%>"
                                                runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtApprover4" runat="server" CssClass="altText" Enabled="false"></asp:TextBox>
                                        &nbsp;<a href="" onclick="this.childNodes[0].click();return false;"><img id="Img8"
                                            border="0" onclick="javascript:getYourOwnEmailListNew(3)" src="image/edit.gif"
                                            alt="Edit" style="cursor: pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>" /></a>
                                        <a href="javascript: deleteApprover(4);" onmouseover="window.status='';return true;">
                                            <img id="Img1" border="0" src="image/btn_delete.gif" runat="server" alt="delete"
                                                width="16" height="16" style="cursor: pointer;" title="<%$ Resources:WebResources, Delete%>" /></a>
                                        <%--FB 2798--%>
                                        <asp:TextBox ID="hdnApprover4" runat="server" BackColor="Transparent" BorderColor="White"
                                            BorderStyle="None" Width="0px" ForeColor="Black" Style="display: none"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="ConferenceRequestorRow" valign="top">
                                    <td width="1%"></td>
                                    <td>
                                        <span class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Requestor%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="txtApprover6" runat="server" CssClass="altText" Enabled="false"></asp:TextBox>
                                        &nbsp;<a href="" onclick="this.childNodes[0].click();return false;"><img id="Img2"
                                            border="0" onclick="javascript:getYourOwnEmailListNew(5)" src="image/edit.gif"
                                            alt="Edit" style="cursor: pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>" /></a>
                                        <a href="javascript: deleteApprover(6);" onmouseover="window.status='';return true;">
                                            <img id="Img3" border="0" src="image/btn_delete.gif" runat="server" alt="delete"
                                                width="16" height="16" style="cursor: pointer;" title="<%$ Resources:WebResources, Delete%>" /></a>
                                        <asp:TextBox ID="hdnApprover6" runat="server" BackColor="Transparent" BorderColor="White"
                                            BorderStyle="None" Width="0px" ForeColor="Black" Style="display: none"></asp:TextBox>
                                    </td>
                                    <td>
                                        <span class="blackblodtext">
                                            <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ReportDetails_VNOCOperator%>"
                                                runat="server"></asp:Literal></span>
                                    </td>
                                    <td valign="top">
                                        <asp:TextBox ID="txtRVNOCOperator" runat="server" CssClass="altText" Enabled="false"></asp:TextBox>
                                        &nbsp;<a href="" onclick="javascript:this.childNodes[0].click();return false;"><img id="img9" border="0" onclick="javascript:getVNOCEmailListnew()" alt="Edit" src="image/VNOCedit.gif"
                                                style="cursor: pointer;" runat="server" title="<%$ Resources:WebResources, SelectVNOCOperator%>" /></a>
                                        <a href="javascript: deleteVNOC();" onmouseover="window.status='';return true;">
                                            <img id="Img5" border="0" src="image/btn_delete.gif" runat="server" alt="delete"
                                                width="16" height="16" style="cursor: pointer;" title="<%$ Resources:WebResources, Delete%>"></a>
                                        <asp:TextBox ID="hdnVNOCOperator" runat="server" BackColor="Transparent" BorderColor="White"
                                            BorderStyle="None" Width="0px" ForeColor="Black" Style="display: none"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr id="CallurlRow">
                                    <td width="1%"></td>
                                    <td>
                                        <span class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources, ReportDetails_CallURI%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <asp:TextBox runat="server" CssClass="alttext" ID="CallURL" Width="120px"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="regCallURL" ControlToValidate="CallURL" ValidationGroup="btnviewRep"
                                            Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters21%>"
                                            ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+?|`\[\]{}\=$%&()~]*$"></asp:RegularExpressionValidator>
                                    </td>
                                    <td nowrap="nowrap">
                                        <span class="blackblodtext">
                                            <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ReportDetails_TimeZone%>"
                                                runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <dxAxEd:ASPxComboBox ID="lstConferenceTZ" runat="server" SelectedIndex="0" Width="205"
                                            CssClass="altSelectFormat" ValueField="timezoneID" TextField="timezoneName">
                                        </dxAxEd:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr id="tdBridge">
                                    <td width="1%"></td>
                                    <td nowrap="nowrap">
                                        <span class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources, ReportDetails_MCU%>" runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <dxAxEd:ASPxComboBox ID="lstBridges" runat="server" SelectedIndex="0" Width="205"
                                            CssClass="altSelectFormat" ValueField="BridgeID" TextField="BridgeName">
                                        </dxAxEd:ASPxComboBox>
                                    </td>
                                    <td nowrap="nowrap">
                                        <span class="blackblodtext">
                                            <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ReportDetails_Endpoint%>"
                                                runat="server"></asp:Literal></span>
                                    </td>
                                    <td>
                                        <dxAxEd:ASPxComboBox ID="lstEndpoint" runat="server" SelectedIndex="0" Width="205"
                                            CssClass="altSelectFormat" ValueField="EndpointID" TextField="EndpointName">
                                        </dxAxEd:ASPxComboBox>
                                    </td>
                                </tr>
                                <tr id="RoomRow">
                                    <td width="1%"></td>
                                    <td nowrap="nowrap" >
                                        <span class="blackblodtext">
                                            <asp:Literal Text="<%$ Resources:WebResources, ReportDetails_Room%>" runat="server"></asp:Literal></span><br />
                                    </td>
                                    <td  onclick='fnAssignValue("<%=cblRoom.ClientID%>")'>
                                        <asp:TextBox ID="txtRooms" runat="server" Width="180" CssClass="altText" Enabled="false" />
                                        <img id="imgRoom" onclick="fnShowRooms()" src="image/DDImage.gif" alt="Edit" style="cursor: hand;
                                            vertical-align: top;" runat="server" />
                                        <br />
                                        <div id="RoomDiv" class="RoomDiv" runat="server" style="display: none; width: 205px;">
                                            &nbsp;<asp:CheckBox ID="chkSelectall" runat="server" Text="<%$ Resources:WebResources, ReportDetails_chkSelectall%>" />
                                            <asp:CheckBoxList ID="cblRoom" runat="server" DataTextField="roomName" DataValueField="roomID">
                                            </asp:CheckBoxList>
                                        </div>
                                    </td>
                                    <td colspan="2">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%-- FB 2767 Ends --%>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="2" style="padding-right: 50px">
                <br />
                <%-- FB 2767 --%>
                <%--<input class="altLongBlueButtonFormat" id="btnCancel" onclick="fnCancel()" type="button" value="Cancel"
                    name="btnCancel" style="width: 20%" />--%>
                <button type="button" id="btnCancel" class="altLongYellowButtonFormat" onclick="fnCancel();"
                    style="width: 100px">
                    <asp:Literal Text='<%$ Resources:WebResources, Cancel%>' runat='server' /></button>
                <%--ZD 100423--%>
                <%--ZD 100420--%>
                <button type="button" id="btnReset" runat="server" class="altLongBlueButtonFormat"
                    style="width: 20%" onserverclick="ResetPage" onclick="javascript:DataLoading(1);">
                    <asp:Literal Text='<%$ Resources:WebResources, Reset%>' runat='server' /></button>
                <%--ZD 100176--%>
                <button type="button" id="btnCreate" runat="server" class="altLongBlueButtonFormat"
                    style="width: 20%" onclick="fnListValue();" onserverclick="SaveReportConfiguration"
                    validationgroup="Submit">
                    <asp:Literal Text='<%$ Resources:WebResources, ManageBatchReport_btnCreate%>' runat='server' /></button>
                <%-- FB 2917 Ends --%>
            </td>
        </tr>
    </table>
    <img src="keepalive.asp" alt="KeepAlive" name="KeepAlive" width="1px" height="1px" />
    <%--ZD 100419--%>
    </form>
    <script type="text/javascript">
        fnShowMenu();
    </script>
    <%--code added for Soft Edge button--%>
    <script type="text/javascript" src="inc/softedge.js"></script>
    <script type="text/javascript">
        document.onkeydown = function (evt) {
            evt = evt || window.event;
            var keyCode = evt.keyCode;
            if (keyCode == 8) {
                if (document.getElementById("btnCancel") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnCancel").click();
                        return false;
                    }
                }
                if (document.getElementById("btnGoBack") != null) { // backspace
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnGoBack").click();
                        return false;
                    }
                }
            }
            fnOnKeyDown(evt);
        };

        // FB 2767 Starts //FB 2917 Starts
        //        if (navigator.userAgent.indexOf("Trident/6.0") > -1) {
        //            document.getElementById("tdDateFrom").style.paddingRight = "5px";
        //            document.getElementById("tdDateTo").style.paddingRight = "5px";
        //        }
        //        else if (navigator.userAgent.indexOf("MSIE") > -1) {
        //            document.getElementById("tdDateFrom").style.paddingRight = "42px";
        //            document.getElementById("tdDateTo").style.paddingRight = "42px";
        //        }
        //        else if (navigator.userAgent.indexOf("Firefox") > -1) {
        //            document.getElementById("tdDateFrom").style.paddingRight = "60px";
        //            document.getElementById("tdDateTo").style.paddingRight = "60px";
        //        }
        //        else {
        //            document.getElementById("tdDateFrom").style.paddingLeft = "85px";
        //            document.getElementById("tdDateTo").style.paddingLeft = "85px";
        //        }
        //        // FB 2767 Ends //FB 2917 Ends

        var servertoday = new Date();
        var dFormat;
        dFormat = "<%=format %>";

        showFlatCalendar(1, dFormat)

        function fnListValue() {

            //ZD 102468 - Start
            var reportsList1 = document.getElementById("ReportsList");
            var resourseType = document.getElementById("lstResourseType");
            if (reportsList1)
                rptListValue1 = reportsList1.value;

            switch (rptListValue1) 
            {
                case "2":
                    document.getElementById("tblDate").style.display = "None";
                    ValidatorEnable(document.getElementById("reqDFrom"), false);
                    ValidatorEnable(document.getElementById("reqDTo"), false);
                    break;
                case "6":

                    resourseType.value = lstResourseType.GetValue();
                    var rType1 = resourseType.value;

                    if (rType1 == "5" || rType1 == "7") 
                    {
                        ValidatorEnable(document.getElementById("reqDFrom"), true);
                        ValidatorEnable(document.getElementById("reqDTo"), true);
                    }
                    else if (rType1 != "5" && rType1 != "7")
                    {
                        ValidatorEnable(document.getElementById("reqDFrom"), false);
                        ValidatorEnable(document.getElementById("reqDTo"), false);                    
                    }
                    break;
                default:
                    ValidatorEnable(document.getElementById("reqDFrom"), true);
                    ValidatorEnable(document.getElementById("reqDTo"), true);
                    break;
            }


            //ZD 102468 - End
            if (!Page_ClientValidate()) //FB 2410
                return Page_IsValid;

            var Date1 = document.getElementById("startDateedit_I").value;
            var Date2 = document.getElementById("endDateedit_I").value;

            var Date3 = document.getElementById("dateFrom_I").value;
            var Date4 = document.getElementById("dateTo_I").value;

            var ret = fnCompareDate2(Date1, Date2, 0);
            if (ret == false)
                return ret;

            var ret1 = fnCompareDate2(Date3, Date4, 1);
            if (ret1 == false)
                return ret1;

            if (rblFrequency.GetValue() == "5") {
                var datecb = document.frmManageReport.CustomDate;
                var datelist = document.getElementById("hdnDateList");

                if (datecb.length < 1) {
                    document.getElementById("errLabel").innerHTML = RSpleaseSelectDates;
                    return false;
                }

                for (var i = 0; i < datecb.length; i++) {
                    if (datelist.value == "")
                        datelist.value = datecb.options[i].value;
                    else
                        datelist.value = datelist.value + "," + datecb.options[i].value;
                }

                ValidatorEnable(document.getElementById("reqSDate"), false);
                ValidatorEnable(document.getElementById("reqEDate"), false);

                return true;
            }
            else {
                ValidatorEnable(document.getElementById("reqSDate"), true);
                ValidatorEnable(document.getElementById("reqEDate"), true);

                if (!Page_ClientValidate())
                    return Page_IsValid;

                return true;
            }

        }


        if (document.getElementById("CustomRow").style.display != "none") {
            document.getElementById("tdSpace").style.display = "block"
        }


        function fnCompareDate2(Date1, Date2, par) {


            dFormat = "<%=format %>";
            var str1 = Date1.toString();
            var str2 = Date2.toString();

            var dt1 = parseInt(str1.substring(0, 2), 10);
            var mon1 = parseInt(str1.substring(3, 5), 10);
            var yr1 = parseInt(str1.substring(6, 10), 10);
            var dt2 = parseInt(str2.substring(0, 2), 10);
            var mon2 = parseInt(str2.substring(3, 5), 10);
            var yr2 = parseInt(str2.substring(6, 10), 10);

            if (dFormat == "MM/dd/yyyy") {
                var temp1 = "";
                var temp2 = "";
                temp1 = mon1;
                mon1 = dt1;
                dt1 = temp1;

                temp2 = mon2;
                mon2 = dt2;
                dt2 = temp2;

                var date1 = new Date(yr1, mon1, dt1);
                var date2 = new Date(yr2, mon2, dt2);
            }
            else {
                var date1 = new Date(yr1, mon1, dt1);
                var date2 = new Date(yr2, mon2, dt2);
            }

            if (date2 < date1) {
                if (par == 0) {
                    alert(RSstartDateLess)
                }
                else {
                    alert(RSdateFromLess);
                }
                return false;
            }
            //            else {
            //                alert("Submitting ...");
            //                document.form1.submit();
            //            }
        }

  
    </script>
    <!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
</body>
</html>
