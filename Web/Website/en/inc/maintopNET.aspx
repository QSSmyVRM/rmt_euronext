﻿<!--ZD 100147 Start-->
<!--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/-->
<!--ZD 100147 ZD 100886 End-->
<%     
    
    if(Session["userID"] == null)
        Response.Redirect("~/en/genlogin.aspx"); //FB 1830

    Session["xadminAlert"] = ""; // ZD 100172
    if(Session["adminAlert"] != null)
    {
        Session["xadminAlert"] = Session["adminAlert"].ToString();
        Session["adminAlert"] = "";
    }
%>
<!DOCTYPE html>
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <%-- FB 2779 --%>
<html>
<head>
    <title>myVRM</title>
    <meta name="Description" content="myVRM (Videoconferencing Resource Management) is a revolutionary Web-based software application that manages any video conferencing environment.">
    <meta name="Keywords" content="myVRM, Videoconferencing Resource Management, video conferencing, video bridges, video endpoints">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="LANGUAGE" content="en">
    <!--  #INCLUDE FILE="../inc/browserdetect.aspx"  -->
    <%--FB 1861, FB 1913--%>
    <!--  #INCLUDE FILE="../inc/Holiday.aspx"  -->
    <meta name="DOCUMENTCOUNTRYCODE" content="us">
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/border-table.css">
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="css/main-table.css">
	<script type="text/javascript">    		    // FB 2790
	    var path = '<%=Session["OrgCSSPath"]%>';
	    path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".css"; // FB 2815
	    document.write('<link rel="stylesheet" title="Expedite base styles" type="text/css" href="' + path + '?' + new Date().getTime() + '">');
    </script>    <%-- Organization Css Module --%>

    <script language="javascript" src="script/RoboHelp_CSH.js"></script>
    
    <%--ZD 100428 starts--%>
    <script language="javascript">
        //ZD 100604 start
        var img = new Image();
        img.src = "../en/image/wait1.gif";
        //ZD 100604 End
    function tabnavigation()
    {  
        var nodecount = 1;
        if (navigator.appName.indexOf('Internet Explorer') != -1) {
            nodecount = 0;
        }
       
        if(document.activeElement.childNodes[nodecount].id=='tabnav1')
        {
          document.getElementById('tdtabnav1').childNodes[nodecount].onmouseover();
            if(document.activeElement.childNodes[nodecount].id=='tabnav1')
                document.getElementById('accountset').focus();
               
                
         }
         else
         {
           document.getElementById('tdtabnav2').childNodes[nodecount].childNodes[nodecount].onmouseover();
           if(document.activeElement.id=='tdtabnav2')  
           {
                var elelist=document.getElementById('tabnav2li').getElementsByTagName('a');
                for(var i=0;i<elelist.length;i++)
                {
                    if(elelist[i].parentNode.style.display=='')
                    {
                         document.getElementById(elelist[i].id).focus();
                         break;
                    }
                }
            }

        }
        
        
  
     }
     function tabnavigationleftarrow(){
      document.getElementById('orglist').parentNode.onmouseover();
        document.getElementById('orglist').childNodes[0].childNodes[0].focus();
        
     }
     function onblurmouseout()
     {
      var nodecount = 1;
        if (navigator.appName.indexOf('Internet Explorer') != -1) {
            nodecount = 0;
        }
      document.getElementById('tdtabnav1').childNodes[0].onmouseout();
      document.getElementById('tdtabnav2').focus();
      
     }
     
     function onblurmouseout1(var1)
     {
    
        var nodecount = 1;
        if (navigator.appName.indexOf('Internet Explorer') != -1) {
            nodecount = 0;
        }
        if(var1=="alstSetting")
        {  //,,aswitchorgMain,aLi4
            if (document.getElementById('Li1').parentNode.style.display == 'none')
                if (document.getElementById('aswitchorgMain').parentNode.style.display == 'none')
                if (document.getElementById('aLi4').parentNode.style.display == 'none') {
                    document.getElementById('tdtabnav2').childNodes[nodecount].childNodes[nodecount].onmouseout();
                  
            }
            
        }
       else if(var1=="aLi")
       {
           if (document.getElementById('aswitchorgMain').parentNode.style.display == 'none')
               if (document.getElementById('aLi4').parentNode.style.display == 'none') {
                   document.getElementById('tdtabnav2').childNodes[nodecount].childNodes[nodecount].onmouseout();
                  
           }
       }
       else
       if(var1=="aswitchorgMain")
       {
           if (document.getElementById('aLi4').parentNode.style.display == 'none') {
               document.getElementById('tdtabnav2').childNodes[nodecount].childNodes[nodecount].onmouseout();
              
           }
        
       }
       else
        if(var1=="aLi4")
       {
           document.getElementById('tdtabnav2').childNodes[nodecount].childNodes[nodecount].onmouseout();
          
        }  
       
             
     }
     function onmouseoverout2()
     {
        var nodecount = 1;
        if (navigator.appName.indexOf('Internet Explorer') != -1) {
            nodecount = 0;
        }
         document.getElementById('tdtabnav1').childNodes[nodecount].onmouseout();
         document.getElementById('tdtabnav2').focus();
        
     
     }
    
    </script>
   <%-- ZD 100428 Ends--%>

    <%--FB 2719 Starts--%>
    <style type="text/css">
        /* ZD 100426 Starts */
        .file_input_textbox {
            height:20px; 
            width:200px; 
            float:left; 
            }

        .file_input_div {
            position: relative;
            width:80px; 
            height:30px;
            overflow: hidden;
            float:left; }

        .file_input_button {
            width: 80px;
            position:absolute;
            top:0px; 
            border:1px solid #A7958B;
            padding:2px 8px 2px 8px; 
            font-weight: normal;
            height:24px;
            margin:0px; 
            margin-right:5px; 
            vertical-align:bottom; 
            background-color:#D4D0C8; 
            }

        .file_input_hidden {
            font-size :45px;
            position:absolute;
            right:0px;top:0px;
            cursor:pointer; 
            opacity:0; 
            filter:alpha(opacity=0); 
            -ms-filter:"alpha(opacity=0)";
            -khtml-opacity:0;
            -moz-opacity:0;
            }
        /* ZD 100426 End */        
        #menuHomeRed
        {
            background-image: url('../en/image/menu_icons/xhome_red.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        #menuCalRed
        {
            background-image: url('../en/image/menu_icons/xcal_red.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        #menuConfRed
        {
            background-image: url('../en/image/menu_icons/xconf_red.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        #menuSetRed
        {
            background-image: url('../en/image/menu_icons/xset_red.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        #menuAdminRed
        {
            background-image: url('../en/image/menu_icons/xadmin_red.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
       
        #menuSiteRed
        {
            background-image: url('../en/image/menu_icons/xsite_red.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        /* FB 2779 Starts */
        #menuHomeRed:hover
        {
            background-image: url('../en/image/menu_icons/xblack_home.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuCalRed:hover
        {
            background-image: url('../en/image/menu_icons/xblack_cal.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuConfRed:hover
        {
            background-image: url('../en/image/menu_icons/xblack_conf.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSetRed:hover
        {
            background-image: url('../en/image/menu_icons/xblack_set.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuAdminRed:hover
        {
            background-image: url('../en/image/menu_icons/xblack_admin.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSiteRed:hover
        {
            background-image: url('../en/image/menu_icons/xblack_site.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        /* FB 2779 Ends */
        
        /*ZD 101388 START */
        #menuInstantConferenceRed
        {
            background-image: url('../en/image/menu_icons/xInstantConferences_red.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuInstantConferenceRed:hover
        {
            background-image: url('../en/image/menu_icons/xblack_InstantConferences.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        /*ZD 101388 END */

    </style>
    <style type="text/css">
       #menuHomeBlue
        {
            background-image: url('../en/image/menu_icons/xhome_blue.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        #menuCalBlue
        {
            background-image: url('../en/image/menu_icons/xcal_blue.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        #menuConfBlue
        {
            background-image: url('../en/image/menu_icons/xconf_blue.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        #menuSetBlue
        {
            background-image: url('../en/image/menu_icons/xset_blue.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        #menuAdminBlue
        {
            background-image: url('../en/image/menu_icons/xadmin_blue.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
       
        #menuSiteBlue
        {
            background-image: url('../en/image/menu_icons/xsite_blue.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }        
        /* FB 2779 Starts */
        #menuHomeBlue:hover
        {
            background-image: url('../en/image/menu_icons/xblack_home.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuCalBlue:hover
        {
            background-image: url('../en/image/menu_icons/xblack_cal.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuConfBlue:hover
        {
            background-image: url('../en/image/menu_icons/xblack_conf.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSetBlue:hover
        {
            background-image: url('../en/image/menu_icons/xblack_set.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuAdminBlue:hover
        {
            background-image: url('../en/image/menu_icons/xblack_admin.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSiteBlue:hover
        {
            background-image: url('../en/image/menu_icons/xblack_site.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        /* FB 2779 Ends */
        
        /*ZD 101388 START */
        #menuInstantConferenceBlue
        {
            background-image: url('../en/image/menu_icons/xInstantConferences_blue.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuInstantConferenceBlue:hover
        {
            background-image: url('../en/image/menu_icons/xblack_InstantConferences.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        /*ZD 101388 END */
        
    </style>
    <style type="text/css">
        #menuHome
        {
            background-image: url('../en/image/menu_icons/xhome.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuHome:hover
        {
            background-image: url('../en/image/menu_icons/xblack_home.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuCal
        {
            background-image: url('../en/image/menu_icons/xcal.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuCal:hover
        {
            background-image: url('../en/image/menu_icons/xblack_cal.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuConf
        {
            background-image: url('../en/image/menu_icons/xconf.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuConf:hover
        {
            background-image: url('../en/image/menu_icons/xblack_conf.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSet
        {
            background-image: url('../en/image/menu_icons/xset.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSet:hover
        {
            background-image: url('../en/image/menu_icons/xblack_set.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuAdmin
        {
            background-image: url('../en/image/menu_icons/xadmin.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuAdmin:hover
        {
            background-image: url('../en/image/menu_icons/xblack_admin.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSite
        {
            background-image: url('../en/image/menu_icons/xsite.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuSite:hover
        {
            background-image: url('../en/image/menu_icons/xblack_site.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        
        /*ZD 101388 START */
        #menuInstantConference
        {
            background-image: url('../en/image/menu_icons/xInstantConferences.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        #menuInstantConference:hover
        {
            background-image: url('../en/image/menu_icons/xblack_InstantConferences.png');
            background-repeat: no-repeat;
            width: 90px;
            height: 70px;
        }
        /*ZD 101388 END */
    </style>


    <%--FB 2719 Starts--%>
    <style type="text/css">
        @media print
        {
            #btprint
            {
                display: none;
            }
        }
        /* FB 2719 Starts */
        /* Menu - 1 Style starts */
        /*ZD 100428 starts */
        
        #menu1:focus
        {
            outline: none;
        }
        #tabnav1, #tabnav1 ul
        {
            margin: 0;
            padding: 0;
            list-style-type: none;
            list-style-position: outside;
            position: relative;
            line-height: 1.5em;
        }
        #tabnav1 a
        {
            display: block;
            padding: 0px 5px;
            border: 1px;
            color: #5E5D5E;
            background-color: #E0E0E0;
        }
        #tabnav1 a:hover
        {
            color: #fff;
            background-color: #7c7c7c;
        }
        #tabnav1 a:focus  
        {
            color: #fff;
            background-color: #7c7c7c;
        }
        #tabnav1 li
        {
            float: inherit;
            position: relative;
        }
        #tabnav1 ul
        {
            position: absolute;
            width: 130px;
            display: none;
            width: 100px;
            top: 1.5em;
        }
        #tabnav1 li ul a
        {
            width: 145px;
            height: 30px;
            float: inherit;
            background-color: #E0E0E0; /* select ul background */
        }
        #tabnav1 ul ul
        {
            top: auto;
        }
        #tabnav1 li ul ul
        {
            margin: 0px 0 0 10px;
        }
        #tabnav1 li:hover ul ul, #tabnav1 li:hover ul ul ul, #tabnav1 li:hover ul ul ul ul
        {
            display: none;
        }
        #tabnav1 li:focus ul ul, #tabnav1 li:focus ul ul ul, #tabnav1 li:focus ul ul ul ul
        {
            display: none;
        }
        #tabnav1 li:hover ul, #tabnav1 li li:hover ul, #tabnav1 li li li:hover ul, #tabnav1 li li li li:hover ul
        {
            display: block;
        }
         #tabnav1 li:focus ul, #tabnav1 li li:focus ul, #tabnav1 li li li:focus ul, #tabnav1 li li li li:focus ul
        {
            display: block;
        }
        /* Menu - 1 Style Ends *//* Menu - 2 Style starts */#tabnav2, #tabnav2 ul
        {
            margin: 0;
            padding: 0;
            list-style-type: none;
            list-style-position: outside;
            position: relative;
            line-height: 1.5em;
        }
           /* Menu - 1 Style Ends *//* Menu - 2 Style starts */#tabnav2:focus, #tabnav2 ul:focus ul
        {
            margin: 0;
            padding: 0;
            list-style-type: none;
            list-style-position: outside;
            position: relative;
            line-height: 1.5em;
        }
        #tabnav2 a
        {
            display: block;
            padding: 0px 1px 0px 5px;
            border: 1px;
            color: #5E5D5E;
            background-color: #E0E0E0;
        }
        #tabnav2 a:hover
        {
            color: #fff;
            background-color: #7c7c7c;
        }
          #tabnav2 a:focus
        {
            color: #fff;
            background-color: #7c7c7c;
        }
        #tabnav2 li
        {
            float: inherit;
            position: relative;
        }
        #tabnav2 ul
        {
            position: absolute;
            width: 130px;
            display: none;
            width: 100px;
            top: 1.5em;
        }
        #tabnav2 li ul a
        {
            width: 180px;
            height: 30px;
            float: inherit;
            background-color: #E0E0E0; /* select ul background */
        }
         #tabnav2 li ul a:focus li ul a
        {
            width: 180px;
            height: 30px;
            float: inherit;
            background-color: #E0E0E0; /* select ul background */
        }
        #tabnav2 ul ul
        {
            top: auto;
        }
        #tabnav2 li ul ul
        {
            margin: 0px 0 0 1px;
        }
        #tabnav2 li:hover ul ul, #tabnav2 li:hover ul ul ul, #tabnav2 li:hover ul ul ul ul
        {
            display: none;
        }
           #tabnav2 li:focus ul ul, #tabnav2 li:focus ul ul ul, #tabnav2 li:focus ul ul ul ul
        {
            display: none;
        }
        #tabnav2 li:hover ul, #tabnav2 li li:hover ul, #tabnav2 li li li:hover ul, #tabnav2 li li li li:hover ul
        {
            display: block;
        }
         #tabnav2 li:focus ul, #tabnav2 li li:focus ul, #tabnav2 li li li:focus ul, #tabnav2 li li li li:focus ul
        {
            display: block;
        }
        /* Menu - 2 Style Ends *//* Menu - 3 Starts */#orglist, #orglist ul
        {
            margin: 0;
            padding: 0;
            list-style-type: none;
            list-style-position: outside;
            position: relative;
            line-height: 1.5em;
        }
        #orglist a
        {
            display: block;
            padding: 0px 1px 0px 5px;
            border: 1px;
            color: #5E5D5E;
            background-color: #E0E0E0;
        }
        #orglist a:hover
        {
            color: #fff;
            background-color: #7c7c7c;
        }
         #orglist a:focus
        {
            color: #fff;
            background-color: #7c7c7c;
        }
        #orglist li
        {
            float: right;
            position: relative;
        }
        #orglist ul
        {
            position: absolute;
            width: 130px;
            display: none;
            width: 100px;
            top: 0em;
        }
        #orglist li ul a
        {
            width: 130px;
            height: 30px;
            float: right;
            background-color: #E0E0E0;
        }
        #orglist ul ul
        {
            top: auto;
        }
        #orglist li ul ul
        {
            margin: 0px 0 0 0px;
        }
        #orglist li:hover ul ul, #orglist li:hover ul ul ul, #orglist li:hover ul ul ul ul
        {
            display: none;
        }
         #orglist li:focus ul ul, #orglist li:focus ul ul ul, #orglist li:focus ul ul ul ul
        {
            display: none;
        }
        #orglist li:hover ul, #orglist li li:hover ul, #orglist li li li:hover ul, #orglist li li li li:hover ul
        {
            display: block;
        }
        #orglist li:focus ul, #orglist li li:focus ul, #orglist li li li:focus ul, #orglist li li li li:focus ul
        {
            display: block;
        }
        /*ZD 100428 Ends */
        /* FB 2719 Ends */</style>
    <%--FB 2719 Ends--%>

    <script language="javascript">
  <!--
    
    // FB 2687 Starts
    function preventBack(){window.history.forward();}
    setTimeout("preventBack()", 0);
    window.onunload=function(){null};
    // FB 2687 Ends
    
   //FB 2719 Starts 
    function fnmenu1mover(val) {            
            document.getElementById("tabnav1li").style.display = "block";
            document.getElementById("tabnav2").style.visibility = "hidden";            
     }
        
     function fnmenu1mout(val) {            
        document.getElementById("tabnav1li").style.display = "none";
        document.getElementById("tabnav2").style.visibility = "visible";            
     }
     
     //FB 2979 - Start
     function fnmenu2mover(val) {
                    
            document.getElementById("tabnav2li").style.display = "block";
            if( '<%=Session["hashelp"]%>' != null)
            {
                if( '<%=Session["hashelp"]%>' == 'True')
                {
                    if(document.getElementById("Li4") != null)
                        document.getElementById("Li4").style.display = "block";
                    if(document.getElementById("Li3") != null)
                        document.getElementById("Li3").style.display = "block";
                }
                else
                {
                    if(document.getElementById("Li4") != null)
                        document.getElementById("Li4").style.display = "none";
                    if(document.getElementById("Li3") != null)
                        document.getElementById("Li3").style.display = "none";
                }
            }            
     }
     //FB 2979 - End
        
     function fnmenu2mout(val) {        
        document.getElementById("tabnav2li").style.display = "none";
     }  
     //FB 2719 Ends
            
    version = '<%=Application["Version"]%>';
    copyrightsDur = '<%=Application["CopyrightsDur"]%>';//FB 1648
    var timerRunning = false;
	var toTimer = null;
	defaultConfTempJS = '<%=Session["defaultConfTemp"]%>'; //FB 1746
    var isExpressUser = '<%=Session["isExpressUser"]%>'; //FB 1779
	var curtalker = ""
	curtalker = "<%=Session["IMTalker"]%>";
	var EnableAudioBridge = '<%=Session["EnableAudioBridges"]%>'; //FB 2023
	var EnableEM7Opt='<%=Session["EnableEM7"]%>'; //FB 2633
	var EnableCallMonitor='<%=Session["EnableCallmonitor"]%>'; //FB 2996
	var UsrCrossAccess ='<%=Session["UsrCrossAccess"]%>';//ZD 100164
	var EnableAdvancedUserOption='<%=Session["EnableAdvancedUserOption"]%>';//ZD 100164
	var EnableCloudInstallation='<%=Session["EnableCloudInstallation"]%>';//ZD 100164
	var hasOngoing='<%=Session["hasOngoing"]%>';//ZD 101388 Starts
	var hasReservations='<%=Session["hasReservations"]%>';
	var hasPublic='<%=Session["hasPublic"]%>';
	var hasPending='<%=Session["hasPending"]%>';
	var hasApproval='<%=Session["hasApproval"]%>';
	var hasConferenceSupport='<%=Session["hasConferenceSupport"]%>';
	var hasOnMCU='<%=Session["hasOnMCU"]%>';//ZD 101388 End
	var hasConferenceList= '<%=Session["hasConferenceList"]%>';//ZD 101388 End

	/* Commented for FB 2397
	function displayAlert()
	{
		alert("Your session will expire in 1 minute due to inactivity.\nPlease click on the Refresh button of your browser to avoid time out.");
	}
	*/
	// ZD 100732 Starts
	function setCookie(cname,cvalue,exdays)
    {
    var d = new Date();
    d.setTime(d.getTime()+(exdays*24*60*60*1000));
    var expires = "expires="+d.toGMTString();
    document.cookie = cname + "=" + cvalue + "; " + expires;
    }

    function getCookie(cname)
    {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) 
      {
      var c = fnTrim(ca[i]);
      if (c.indexOf(name)==0) return c.substring(name.length,c.length);
      }
    return "";
    }
    
    function fnTrim(str)     
    {     
        return str.replace(/^\s+|\s+$/g,"");     
    }
        
    var cur;
	document.onmouseover = startTimer;
	
	function logout()
	{
//	    alert("in .NET");
        var c = getCookie(window.location.host);
        if(cur == c)
		    window.location.href = "thankyou.aspx?t=1"; // FB 2397
		else
		    window.location.href = "genlogin.aspx";
	}
	
	function startTimer()
	{
	    cur = Date().toString();
	    while(cur.indexOf(" ") > -1)
	    {
	        cur = cur.replace(" ","");
	        cur = cur.replace(":","");
	        cur = cur.replace("(","");
	        cur = cur.replace(")","");
	    }
	    setCookie(window.location.host,cur,365);
		t2 = parseInt("<%=Application["timeoutSecond"]%>", 10) * 1000;
		t1 = t2 - 60000;
        clearTimeout(toTimer);
		//toTimer = setTimeout('displayAlert()',t1); // Commented for FB 2397
		toTimer = setTimeout('logout()',t2);
		timerRunning = true;
	}
	// ZD 100732 Ends
	
	function stopTimer() {
		if (timerRunning) {
			clearTimeout(toTimer);
		}
	}
	
	
	function openFeedback()
	{
//		window.open ('dispatcher/gendispatcher.asp?cmd=GetFeedback', 'Feedback', 'width=450,height=310,resizable=no,scrollbars=no,status=no,left=' + (screen.availWidth-478) + ',top=198');//Login Management
		window.open ('feedback.aspx?wintype=pop', 'Feedback', 'width=450,height=410,resizable=no,scrollbars=no,status=no,left=' + (screen.availWidth-478) + ',top=198'); //Login Management
		
	}
	
	function close_popwin()
	{
		if (window.winrtc) {
			winrtc.close();
		}
		if (window.wincrt) {
			wincrt.close();
		}
	}
	
	
	function errorHandler( e, f, l ){
		alert("An error has ocurred in the JavaScript on this page.\nFile: " + f + "\nLine: " + l + "\nError:" + e);
		return true;
	}
	
//    function adjustWidth(obj)
//    {
//        if(obj.style.width == "")
//        if (obj.src.indexOf("lobbytop1024.jpg") >= 0)
//        {
            
            
                
//        }
  //-->
    </script>

    <% if (Application["global"] == null )
    Response.Redirect("expired_page.aspx");
if (Application["global"].Equals("enable") ) { %>

    <script language="javascript">
  <!--

        function international(cb) {
            if (cb.options[cb.selectedIndex].value != "") {
                str = "" + window.location;
                newstr = str.replace(/\/en\//i, "/" + cb.options[cb.selectedIndex].value + "/");
                if ((ind1 = newstr.indexOf("sct=")) != -1) {
                    if ((ind2 = newstr.indexOf("&", ind1)) != -1)
                        newstr = newstr.substring(0, ind1 - 1) + newstr.substring(ind2, newstr.length - 1);
                    else
                        newstr = newstr.substring(0, ind1 - 1);
                }
                newstr += ((newstr.indexOf("?") == -1) ? "?" : "&") + "sct=" + cb.selectedIndex;
                document.location = newstr;
            }
        }



       
        
	
  //-->
    </script>

    <%}%>
</head>

<script language="JavaScript">
    startTimer(); // FB 2397

    //ZD 100156 - Start
    var wd = (screen.width - 25) + 'px';
    var ht = (screen.height - 25) + 'px';
    var style = "<style>body{width:" + wd + ";height:" + ht + ";}</style>";
    document.write(style);

    window.onresize = fnAdjMenu;
    window.onscroll = fnAdjMenu;
    
    function fnAdjMenu() 
    {
        var diff = document.body.scrollLeft || window.scrollX || document.documentElement.scrollLeft;
        
        if(document.getElementById("tblTopMenu")!= null)
            document.getElementById("tblTopMenu").style.left = 0 - diff + 'px';
            
        if(document.getElementById("menu1") != null)
            document.getElementById("menu1").style.left = 250 - diff + 'px';

            
    }
    setTimeout("fnAdjMenu()", 100);
    //ZD 100156 - End

    function ModalPopUp() { // ZD 100167

        var screenWidth = (window.innerWidth - 700) / 2;
        var screenHeight = (window.innerHeight - 400) / 2;
        document.getElementById('modalDiv').style.display = 'block';
        document.getElementById('contentDiv').style.display = 'block';
        document.getElementById('contentDiv').style.left = screenWidth + "px";
        document.getElementById('contentDiv').style.top = screenHeight + "px";
    }
    if (document.getElementById('lnk1') != null) // ZD 100167
        document.getElementById('lnk1').onclick = ModalPopUp;

</script>

<%
if (Session["sMenuArrayMask"] == null ){ // ZD 101233
	Response.Redirect ("expired_page.aspx");
}
if (Session["userInterface"].Equals("2") ) {
%>
<%--ZD 100428 starts--%>
<body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0"
    marginwidth="0">
    <input id="advRep" type="hidden" value='<%=Session["EnableAdvancedReport"]%>' />
    <input id="callMon" type="hidden" value='<%=Session["EnableCallmonitor"]%>' />
    <input id="userAdminLevel" type="hidden" value='<%=Session["Admin"]%>' />
    <input id="isExpressUser" type="hidden" value='<%=Session["isExpressUser"]%>' />
    <input id="isExpressUserAdv" type="hidden" value='<%=Session["isExpressUserAdv"]%>' />
    <input id="isExpressManage" type="hidden" value='<%=Session["isExpressManage"]%>' />
     <input id="UsrCrossAccess" type="hidden" value='<%=Session["UsrCrossAccess"]%>' /><%--ZD 100164--%>
     <input id="isCloud" type="hidden" value='<%=Session["EnableCloudInstallation"]%>' /><%--ZD 100167--%>
    <%--ZD 101388 Start --%>
    <div id="topExpDiv1" style="width:1600px; height:1400px; z-index: 100000; position:Fixed; left:0px; top:0px; display:<%= Session["hdModalDiv"] %>; background-color:Gray; filter: alpha(opacity=70) !important; opacity:0.7; overflow:hidden;">
     <div id="dataLoadingDIV1" name="dataLoadingDIV1" align="center" style="display:block; position:fixed; top:25%; left:50%">
                        <img border="0" src="image/wait1.gif" alt="<asp:Literal Text='<%$ Resources:WebResources, Loading%>' runat='server'></asp:Literal>" />
                     </div></div><%--ZD 101500--%>
    <div id="modalDiv" align="center" style="width:100%; height:100%; z-index: 9999; position:fixed; left:0px; top:0px; background-color:gray; opacity:0.5; display:none"></div>
    <div id="contentDiv" align="center" style="width:700px; height:400px; z-index: 10000; position:absolute; background-color:white; display:none">
        <iframe id="frmInstantConf" src="InstantConferences.aspx" width="100%" height="100%"></iframe>        
    </div>
    <%--ZD 101388 End --%>
    <div id="topDiv" style="width:1600px; height:1400px; z-index: 10000px; position:absolute; overlap: left:0px; top:0px; background-color:White;"></div>  <%--FB 2738--%>
    <div id="topHeader" style="position:fixed; background-color:white; z-index:1000; width:110%; height:90px"></div>
    <table tabIndex="-1"  id="tblTopMenu" style="position:fixed; background-color:white; z-index:1000" width="100%" border="0" cellpadding="0" cellspacing="0" class="btprint"><%-- FB 2811 --%>
        <tr valign="top" width="100%"><%--ZD 100156--%>
            <!--<td width="100%" height="72" align="right" valign="bottom" background="image/Lobbytop.jpg">-->
            <td width="60%" height="90" nowrap>
                <%-- FB 2719 --%>
                <%--<img border="0" src="<%=Session["CompanyLogo"]%>" id="Img1" onload="javascript:adjustWidth(this)"/>--%>
                <img id="mainTop" width="200" height="75" alt="<%$ Resources:WebResources, esthetic_CompanyLogo%>" style="margin-left: 5px; margin-top: 10px;margin-right: 650px;"
                    runat="server" /><%--ZD 100156--%>
                <%--Edited for FF FB 2719--%>
            </td>           
            <td align="right" valign="bottom" colspan="2">
                <table border="0" cellpadding="0" cellspacing="0" style="padding-right: 25px"> <%--FB 2793--%>
                    <tr>
                        <td style="width:130px" nowrap="nowrap">
                            <table border="0" width="100%">
                                <tr>
                                    <td colspan="2" id="tdtabnav1"  tabIndex="0" style="height: 30px">
                                        <%--FB 2719 Starts --%>
                                        <%if(Request.Browser.Type.ToUpper().Contains("MSIE")){ %>
                                        <ul tabIndex="-1"  style="text-align:left" id="tabnav1" onmouseover="javascript:fnmenu1mover(this)" onmouseout="javascript:fnmenu1mout(this)">
                                            <li><b style="color: red">
                                                <%=Session["userName"].ToString()%></b>
                                                <ul  style="display: none; margin-left: -125px; margin-top: -2px; text-align: left" id="tabnav1li">
                                                    <li><a id="accountset" href="ManageUserProfile.aspx"><asp:Literal Text="<%$ Resources:WebResources, AccountSettings%>" runat="server"></asp:Literal></a></li>
                                                    <li><a id="acclogout"  onblur="onblurmouseout();" href="thankyou.aspx"><asp:Literal Text="<%$ Resources:WebResources, Logout%>" runat="server"></asp:Literal></a></li> <%--FB 2846--%>
                                                </ul>
                                            </li>
                                        </ul>
                                        <%}else{ %>
                                        <ul tabIndex="-1" style="text-align:left" id="tabnav1" onmouseover="javascript:fnmenu1mover(this)" onmouseout="javascript:fnmenu1mout(this)">
                                            <li><b style="color: red">
                                                <%=Session["userName"].ToString()%></b>
                                                <ul  style="display: none; margin-left: -5px; text-align: left" id="tabnav1li">
                                                    <li><a id="accountset"  href="ManageUserProfile.aspx"><asp:Literal Text="<%$ Resources:WebResources, AccountSettings%>" runat="server"></asp:Literal></a></li>
                                                    <li><a id="acclogout" onblur="onmouseoverout2();" href="thankyou.aspx"><asp:Literal Text="<%$ Resources:WebResources, Logout%>" runat="server"></asp:Literal></a></li> <%--FB 2846--%>
                                                </ul>
                                            </li>
                                        </ul>
                                        <%} %>
                                    </td>
                                </tr>
                                <tr tabIndex="-1">
                                    <td  tabIndex="-1" colspan="3" style="height: 1px; background-color: Gray">
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" valign="bottom">
                                        <table border="0">
                                            <tr>
                                                <td align="left">
                                                    <table border="0">
                                                        <tr>
                                                            <td  id="tdtabnav2" tabIndex="0">
                                                                <%if(Request.Browser.Type.ToUpper().Contains("MSIE")){ %>
                                                                <ul style="text-align:left" id="tabnav2">
                                                                    <li  onmouseover="javascript:fnmenu2mover(this)" onmouseout="javascript:fnmenu2mout(this)">
                                                                        <b style="color: #5E5D5E;white-space: nowrap;"><%=Session["organizationName"].ToString()%></b>
                                                                        <ul style="display: none; margin-left: -80px; margin-top: -2px" id="tabnav2li">
                                                                            <li id="lstSetting" onmouseover="document.getElementById('orglist').style.display='none'">
                                                                                <a id="alstSetting" onblur="onblurmouseout1('alstSetting');" onblur="onblurmouseout1();" href="OrganisationSettings.aspx"><asp:Literal Text="<%$ Resources:WebResources, Settings%>" runat="server"></asp:Literal></a></li>
                                                                            <li  id="searchOption" onmouseover="document.getElementById('orglist').style.display='none'"> <%--FB 2952 ZD 101233--%>
                                                                                <a id="aLi" onblur="onblurmouseout1('aLi');" onfocus='document.getElementById("orglist").parentNode.onmouseout();' href="SearchConferenceInputParameters.aspx"><asp:Literal Text="<%$ Resources:WebResources, Search%>" runat="server"></asp:Literal></a></li>
                                                                            <li id="switchorgMain" style="display:none" onmouseover="document.getElementById('orglist').style.display='block'">
                                                                                <a id="aswitchorgMain" onblur="onblurmouseout1('aswitchorgMain');"  onblur="onblurmouseout1();" href="#"><asp:Literal  Text="<%$ Resources:WebResources, OrganisationSettings_ChgOrg%>" runat="server"></asp:Literal></a>
                                                                                <ul id="orglist" style="margin-left: -135px; margin-top: -41px; position:absolute; z-index:10000"">
                                                                                </ul>
                                                                            </li>
                                                                            <li id="Li3" onmouseover="document.getElementById('orglist').style.display='none'"> <%--FB 2979--%>
                                                                                <a id="aLi4" onblur="onblurmouseout1('aLi4');" onfocus='document.getElementById("orglist").parentNode.onmouseout();'  onclick="javascript:openhelp();return false;" href="#" ><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Help%>" runat="server"></asp:Literal></a></li>  
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                                <%}else{ %>
                                                                <ul style="text-align:left" id="tabnav2">
                                                                    <li onmouseover="javascript:fnmenu2mover(this)" onmouseout="javascript:fnmenu2mout(this)">
                                                                        <b  style="color: #5E5D5E;white-space: nowrap;"><%=Session["organizationName"].ToString()%></b>
                                                                        <ul style="display: none; margin-left: -28px" id="tabnav2li">
                                                                            <li id="lstSetting" onmouseover="document.getElementById('orglist').style.display='none'"><%-- FB 2952--%>
                                                                                <a id="alstSetting" onblur="onblurmouseout1('alstSetting');" href="OrganisationSettings.aspx"><asp:Literal Text="<%$ Resources:WebResources, Settings%>" runat="server"></asp:Literal></a></li>
                                                                            <li  id="searchOption" onmouseover="document.getElementById('orglist').style.display='none'"> <%--FB 2952 ZD 101233--%>
                                                                                <a id="aLi" onblur="onblurmouseout1('aLi');" onfocus='document.getElementById("orglist").parentNode.onmouseout();' href="SearchConferenceInputParameters.aspx"><asp:Literal Text="<%$ Resources:WebResources, Search%>" runat="server"></asp:Literal></a></li>            
                                                                            <li  id="switchorgMain" style="display:none" onmouseout="document.getElementById('orglist').style.display='none'" onmouseover="document.getElementById('orglist').style.display='block'"> <%--FB 2952--%>
                                                                                <a onblur="onblurmouseout1('aswitchorgMain');" id="aswitchorgMain" href="#"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_ChgOrg%>" runat="server"></asp:Literal></a>
                                                                                <ul id="orglist" style="margin-left: -100px; margin-top: -31px; position:absolute; z-index:10000">
                                                                                </ul>
                                                                            </li>
                                                                            <li  id="Li4"> <%--FB 2979--%>
                                                                                <a id="aLi4"  onfocus='document.getElementById("orglist").parentNode.onmouseout();' onblur="onblurmouseout1('aLi4');" onclick="javascript:openhelp();return false;" href="#" ><asp:Literal  Text="<%$ Resources:WebResources, UserMenuController_Help%>" runat="server"></asp:Literal></a></li>                                                                               
                                                                        </ul>
                                                                    </li>
                                                                </ul>
                                                                <%} %>
                                                                <%--FB 2719 Ends--%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
                <%--ZD 100428 Ends--%>
                <form name="frmMenu">
                <input type="hidden" name="menumask" value="<% =Session["sMenuArrayMask"] %>"> <%-- ZD 101233 --%>
                <input type="hidden" name="adminlevel" value="<% =Session["admin"] %>">
                <input type="hidden" name="userinterface" value="<% =Session["userInterface"] %>">
                <input type="hidden" name="feedback_enable" value="<% =Session["hasFeedback"] %>">
                <input type="hidden" name="help_enable" value="<% =Session["hasHelp"] %>">
                <input type="hidden" name="roomfood" value="<%=Convert.ToInt16(Session["hkModule"]) * 4 + Convert.ToInt16(Session["foodModule"])*2 + Convert.ToInt16(Session["roomModule"])%>">
                <input type="hidden" name="p2p" value="<%=Session["P2PEnable"]%>">
                <input type="hidden" name="ssoMode" value="<%=Application["ssoMode"]%>">
                <input type="hidden" name="txtClient" id="txtClient" value="<%=Application["Client"]%>">
                <input type="hidden" name="txtSAlerts" id="txtSAlerts" value="<%= Session["Alerts"] %>" />
                <input type="hidden" name="txtSAlertsP" id="txtSAlertsP" value="<%= Session["AlertsP"] %>" />
                <input type="hidden" name="txtSAlertsA" id="txtSAlertsA" value="<%= Session["AlertsA"] %>" />
                <input type="hidden" name="hdnCalDefaultView" id="hdnCalDefaultView" value="<%= Session["EnableCalDefaultDisplay"] %>" /> <%--ZD 102356--%>
                </form>
                <iframe tabIndex="-1" name="ifrmListen" style="border:none" width="0" height="0" src="blank.htm"><%-- FB 2782 --%>
                    <p>
                        Listening page</p>
                </iframe>
            </td>
        </tr>
    </table>
    </td> </tr> </table>
    <%
	}
	else
	{	
    %>
    <body bottommargin="0" leftmargin="0" rightmargin="0" topmargin="0" marginheight="0"
        marginwidth="0">
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="btprint" bgcolor="Green"
            background="image/Lobbytop.jpg">
            <tr valign="top">
                <td bgcolor="Green">
                    <img border="0" src="image/VRM.GIF" width="130" height="72">
                </td>
                <td bgcolor="Green" background="image/Lobbytop.jpg" width="50%">
                    <table>
                        <tr valign="top" width="70%">
                            <!--<td width="100%" height="72" align="right" valign="bottom" background="image/Lobbytop.jpg">-->
                            <td width="70%" height="72">
                                <%--<%--<img border="--%><%--0" src="mirror/image/lobby_logo.jpg" id="Img1" onload="javascript:adjustWidth(this)">--%>
                                --%>
                            </td>
                            <td align="right">
                                <table border="0" cellpadding="0" cellspacing="0" width="80%">
                                    <tr>
                                        <td align="right">
                                            <table>
                                                <tr>
                                                    <td align="left">
                                                        <b><font face="Verdana" size="1" color="#FF0000">
                                                            <%=Session["userName"].ToString()%></font></b>
                                                    </td>
                                                    <td align="left">
                                                        <table border="0">
                                                            <tr>
                                                                <td>
                                                                    |
                                                                </td>
                                                                <td nowrap align="left">
                                                                    <table border="0">
                                                                        <tr>
                                                                            <td align="center">
                                                                                <a href="thankyou.aspx" title="Logout">Logout</a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td align="left">
                                                        <b><font face="Verdana" size="1" color="#FF0000">
                                                            <%=Session["organizationName"].ToString()%></font></b>
                                                    </td>
                                                    <td align="left">
                                                        <table border="0">
                                                            <tr>
                                                                <%--FB 1639--%>
                                                                <% if((Session["UsrCrossAccess"].ToString().Trim()=="1")&& Session["OrganizationsLimit"].ToString().Trim()!= "1")
                                   {
                                                                %>
                                                                <td>
                                                                    |
                                                                </td>
                                                                <td nowrap align="left">
                                                                    <table border="0">
                                                                        <tr>
                                                                            <td align="center">
                                                                                <%-- FB 1849--%>
                                                                                <%--<a href="SuperAdministrator.aspx?c=1">Change</a>--%>
                                                                                <a href="OrganisationSettings.aspx?c=1">Change</a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <% }%>
                                                                <%--else
                                   { Response.Write("&nbsp;");}--%>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="0px">
                                <form name="frmMenu">
                                <input type="hidden" name="menumask" value="<% =Session["sMenuArrayMask"] %>"> <%-- ZD 101233 --%>
                                <input type="hidden" name="adminlevel" value="<% =Session["admin"] %>">
                                <input type="hidden" name="userinterface" value="<% =Session["userInterface"] %>">
                                <input type="hidden" name="feedback_enable" value="<% =Session["hasFeedback"] %>">
                                <input type="hidden" name="help_enable" value="<% =Session["hasHelp"] %>">
                                <input type="hidden" name="roomfood" value="<%=Convert.ToInt16(Session["hkModule"]) * 4 + Convert.ToInt16(Session["foodModule"])*2 + Convert.ToInt16(Session["roomModule"])%>">
                                <input type="hidden" name="p2p" value="<%=Session["P2PEnable"]%>">
                                <input type="hidden" name="ssoMode" value="<%=Application["ssoMode"]%>">
                                <input type="hidden" name="txtClient" id="Hidden1" value="<%=Application["Client"]%>">
                                <input type="hidden" name="txtSAlerts" id="Hidden2" value="<%= Session["Alerts"] %>" />
                                <input type="hidden" name="txtSAlertsP" id="Hidden3" value="<%= Session["AlertsP"] %>" />
                                <input type="hidden" name="txtSAlertsA" id="Hidden4" value="<%= Session["AlertsA"] %>" />
                                </form>
                            </td>
                            <td width="50%" bgcolor="Green" background="image/Lobbytop.jpg">
                                <img border="0" src="image/topright.jpg" width="580" height="72">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <%
}

        %>
        <!-- script begin -->

        <script language="JavaScript">
        
        //ZD 101233 
        if('<%=Session["hasPreferences"]%>'=="0")                 
         document.getElementById("accountset").style.display = "none";          

        if('<%=Session["hasSearch"]%>'=="0")                 
         document.getElementById("searchOption").style.display = "none";          
        
        if('<%=Session["SettingsMenu"]%>'=="0")                 
         document.getElementById("lstSetting").style.display = "none";          
         
//         function switchorg(val) {   
        if(('<%=Session["UsrCrossAccess"]%>'=="1")&& ('<%=Session["OrganizationsLimit"]%>'!= "1"))
         {
           /// document.getElementById("switchorgMain").style.visibility = "visible";  
            document.getElementById("switchorgMain").style.display = 'block';    
            var OrgList = '<%= Session["orgList"].ToString() %>';            
            OrgList = OrgList.split(",");
            var content, orgID, orgName;

            for (var i = 0; i < OrgList.length-1; i++) 
            {            
                orgID = OrgList[i].split("~")[0];
                orgName = OrgList[i].split("~")[1];                
                if(i==0)
                content = "<li><a style='white-space:nowrap; overflow:hidden; width:150px' href='OrganisationSettings.aspx?c="+orgID+"'>"+orgName+"</a></li>"; // FB 2811
                else
                content += "<li><a style='white-space:nowrap; overflow:hidden; width:150px' href='OrganisationSettings.aspx?c="+orgID+"' >"+orgName+"</a></li>"; // FB 2811
            }            
            document.getElementById("orglist").innerHTML = content;
            document.getElementById("orglist").style.visibility = "visible";            
        }
//FB 2719 Ends
        
<!--

var imtime1 = parseInt("<%=Session["ImRefreshRate"]%>", 10);
imtime1 = ( (imtime1 < 0.5) || isNaN(imtime1) ) ? 0.5 : imtime1;

//-->
        </script>

        <script type="text/javascript" src="../<%=Session["language"] %>/script/errorList.js"></script>

        <div class="btprint" id="mainmenu" style="position:fixed; z-index:1000"><%-- FB 2811 --%>

            <script language="javascript" src="../<%=Session["language"]%>/inc/menuinc.js"></script>

            <script type="text/javascript">                // FB 2050
                //var path = '<%=Session["OrgJsPath"]%>';
                var path = '../<%=Session["language"]%>/Organizations/Original/Javascript/menu_array<%=Session["ThemeType"]%>.js';
                //path = path.split('.')[0] + '<%=Session["ThemeType"]%>' + ".js"; // FB 2815
                document.write('<script type="text/javascript" src="' + path + '?' + new Date().getTime() + '"><\/script>');
            </script>

            <script language="javascript1.1" src="../en/mmenu.js"></script>

        </div>

        <script language="javascript1.1" src="script/ajaxmulti.js"></script>

        <link rel="StyleSheet" href="css\myprompt.css" type="text/css" />

        <script language="javascript" src="script/showimuser.js"></script>

        <a name="top"></a>

        <script language="JavaScript">
        
<!--
showMenu(0, "", "", "", "", "<%=Session["IMEnabled"]%>", "<%=Session["listenOn"]%>");

//-->
        </script>
        <div id="topSpace" style="height:120px"></div><%-- FB 2811 --%>
        <%--<table width="100%" border="0" cellpadding="0" cellspacing="0" class="btprint">
        <tr>
             <td colspan="2"> --%>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="btprint">
            <tr style="display:none"> <%-- FB 2811 --%>
                <td>
                    <% 
if (Session["tickerStatus1"].Equals("1") && Session["tickerStatus"].Equals("1") ) { 
                    %>
                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                        <tr valign="top">
                            <td width="3%">
                            </td>
                            <td align="left">
                                <%--<b><font face="Verdana" size="1" color="#FF0000">

<%
Response.Write("Welcome " + Session["userName"] + " - ("+ Session["organizationName"].ToString() + ")" );
%>

</font></b>--%>
                            </td>
                            <td align="right">
                                <b><font face="Verdana" size="1" color="#FF0000">
                                    <%--<%
	string dtCurrent2;
	string frmtDT2;
	DateTime dt2;
	char[] splitter2  = {'/'};
	
	dt2 = DateTime.Parse(Session["systemDate"].ToString() +"  "+  Session["systemTime"].ToString());
	
	frmtDT2 = dt2.ToString("MMMM dd" + ", " + "yyyy");
	
	if(Session["FormatDateType"] != null)
	{
	
	    if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
	        frmtDT2 = dt2.ToString("dddd, dd MMMM yyyy");
	
	}
	    
	    
	dtCurrent2 = frmtDT2 + ", " + Session["systemTime"] ;
	
	if(Session["timeFormat"] != null)
	{
	    if(Session["timeFormat"].ToString() == "0")
	    {
	        dtCurrent2 = frmtDT2 + ", " + dt2.ToString("HH:mm") +" " ;					        					        
	    }
	    
	}
	
	if(Session["timeZoneDisplay"] != null)
	{
	    if(Session["timeZoneDisplay"].ToString() == "1")
	    {
	        dtCurrent2 +=  " " + Session["systemTimezone"]  +" " ;					        					        
	    }
	    
	}
	
	Response.Write(""+ dtCurrent2 +" ");
%>--%>
                                </font></b>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <% 
}
            %>
            <% 
if (Session["tickerStatus"].Equals("0") && Session["tickerPosition"].Equals("0") ) { 
            %>
            <tr>
                <td style="width: 4px">
                </td>
                <td bgcolor="#848484" style="font-weight: bold;" nowrap>
                    <div id="martickerDiv" style="width: 999px; overflow: hidden">
                        <%-- FB 2050 --%>
                        <% 
if(Session["tickerDisplay"].Equals("0")) { 
                        %>
                        
                        <asp:Literal Text="<%$ Resources:WebResources, MyConferences%>" runat="server"></asp:Literal>
                        
                        <%} %>
                        <% 
if(Session["tickerDisplay"].Equals("1")) { 
                        %>
                        <% 
Response.Write(Session["RSSTitle"]);
                        %>
                        <%} %>
                        <%--Edited for FF--%><marquee id="marticker" onmouseover="this.stop()" onmouseout="this.start()"
                            bgcolor="<%=Session["tickerBackground"]%>" scrollamount="<%=Session["tickerSpeed"]%>"></>
<%
       string dtCurrent;
		string frmtDT;
		DateTime dt;
		char[] splitter  = {'/'};

		myVRMNet.NETFunctions obj; 
        obj = new myVRMNet.NETFunctions();
        obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString());

		dt = DateTime.Parse(Session["systemDate"].ToString() +"  "+  Session["systemTime"].ToString());
		
		frmtDT = dt.ToString("dddd, MMMM dd yyyy");
		
		if(Session["FormatDateType"] != null)
		{
		
		    if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
		        frmtDT = dt.ToString("dddd, dd MMMM yyyy");
		
		}
		//ZD 101344
        var strcurrenttime = "Current time is";
        if(Session["language"] != null)
        {
            if(Session["language"].ToString() == "sp")
                strcurrenttime = "La hora actual es";
        }
		    
		dtCurrent= frmtDT + ", " + strcurrenttime + " " + Session["systemTime"] ;
		
		if(Session["timeFormat"] != null)
		{
		    if(Session["timeFormat"].ToString() == "0")
		    {
		        dtCurrent= frmtDT + ", " + strcurrenttime + " " + dt.ToString("HH:mm") +" " ;						        					        
		    }
		    
		}
		
		if(Session["timeZoneDisplay"] != null)
		{
		    if(Session["timeZoneDisplay"].ToString() == "1")
		    {
		        dtCurrent +=  " " + Session["systemTimezone"]  +" " ;					        					        
		    }
		}    
    %>
    <asp:Literal Text="<%$ Resources:WebResources, Welcome%>" runat="server"></asp:Literal>
        <%
        Response.Write(" " + Session["userName"] + ". "+ dtCurrent + "  "+ Session["ticker"] + " ");
    %>
</marquee>
                    </div>
                    <%-- FB 2050 --%>
                </td>
            </tr>
            <% 
}
            %>
            <% 
if (Session["tickerStatus1"].Equals("0") && Session["tickerPosition1"].Equals("0") ) { 
            %>
            <tr style="height: 15px">
                <td style="width: 4px">
                </td>
                <td bgcolor="#BDBDBD" style="font-weight: bold;" nowrap>
                    <div id="marticDiv" style="width: 999px; overflow: hidden">
                        <%-- FB 2050 --%>
                        <% 
if(Session["tickerDisplay1"].Equals("0")) { 
                        %>
                        <asp:Literal Text="<%$ Resources:WebResources, MyConferences%>" runat="server"></asp:Literal>
                        <%} %>
                        <% 
if(Session["tickerDisplay1"].Equals("1")) { 
                        %>
                        <% 
Response.Write(Session["RSSTitle1"]);
                        %>
                        <%} %>
                        <%--Edited for FF--%><marquee id="martic" onmouseover="this.stop()" onmouseout="this.start()"
                            bgcolor="<%=Session["tickerBackground1"]%>" scrollamount="<%=Session["tickerSpeed1"]%>"></>
<%
       string dtCurrent1;
		string frmtDT1;
		DateTime dt1;
		char[] splitter1  = {'/'};

        myVRMNet.NETFunctions obj; 
        obj = new myVRMNet.NETFunctions();
        obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString());
		
		dt1 = DateTime.Parse(Session["systemDate"].ToString() +"  "+  Session["systemTime"].ToString());
		
		frmtDT1 = dt1.ToString("dddd, MMMM dd yyyy");
		
		if(Session["FormatDateType"] != null)
		{
		
		    if (Session["FormatDateType"].ToString() == "dd/MM/yyyy")
		        frmtDT1 = dt1.ToString("dddd, dd MMMM yyyy");
		
		}
		//ZD 101344
        var strcurrenttime = "Current time is";
        if(Session["language"] != null)
        {
            if(Session["language"].ToString() == "sp")
                strcurrenttime = "La hora actual es";
        }
		    
		dtCurrent1 = frmtDT1 + ", " + strcurrenttime + " " + Session["systemTime"] ;
		
		if(Session["timeFormat"] != null)
		{
		    if(Session["timeFormat"].ToString() == "0")
		    {
		        dtCurrent1 = frmtDT1 + ", " + strcurrenttime + " " + dt1.ToString("HH:mm") +" " ;					        					        
		    }
		    
		}
		
		if(Session["timeZoneDisplay"] != null)
		{
		    if(Session["timeZoneDisplay"].ToString() == "1")
		    {
		        dtCurrent1 +=  " " + Session["systemTimezone"]  +" " ;					        					        
		    }
		} 
		%>
    <asp:Literal Text="<%$ Resources:WebResources, Welcome%>" runat="server"></asp:Literal>
        <%
       Response.Write(" " + Session["userName"] + ". "+ dtCurrent1 + "  "+ Session["ticker1"] + " ");
    %>
 
</marquee>
                    </div>
                    <%-- FB 2050 --%>
                </td>
            </tr>
            <%} %>
            <% 

if (Application["global"].Equals("enable") ) { 
            %>
            <tr>
                <td>
                    <table height="25" cellspacing="0" cellpadding="0" align="right" border="0" class="btprint">
                        <tr>
                            <td nowrap>
                                <span class="para_small">Choose language&nbsp;</span>
                            </td>
                            <td>
                                <table cellspacing="0" cellpadding="1" border="0">
                                    <tr>
                                        <form name="frmInternational">
                                        <td>
                                            <select class="para_small" onchange="international(this)" id="select1" name="select1">
                                                <option value="en">English</option>
                                                <option value="ch">Chinese Simplified</option>
                                                <option value="zh">Chinese Traditional</option>
                                                <option value="fr">French</option>
                                                <option value="sp">Spanish</option>
                                            </select>
                                        </td>
                                        </form>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <% 
}

            %>
        </table>

        <script language="javascript" type="text/javascript">

            var obj = document.getElementById('mainTop');

            //FB 1830
            var std = '../en/' + '<%=Session["OrgBanner1024Path"]%>';
            var high = '../en/' + '<%=Session["OrgBanner1600Path"]%>';

            //var std = '../' + '<%=Session["OrgBanner1024Path"]%>';                    
            // var high = '../' + '<%=Session["OrgBanner1600Path"]%>';


            //obj.style.width=window.screen.width-25; // Commented to show actual size of Image
            if (window.screen.width <= 1024)
                obj.src = std;
            else
                obj.src = high;


            /*
            if(obj != null)
            {
            if (window.screen.width <= 1152)
            {
            obj.src = std;
            obj.style.width = window.screen.width - 220;//FB 1981
            }
            else
            {
            obj.src = high;
            //FB 1633 start
            img = new Image(); 
            img.src = obj.src;
            obj.style.width= img.width;   
            //FB 1633 end
            }
        
     }
            if(navigator.appName != "Microsoft Internet Explorer") //Edited for FF
            { 
            //obj.style.height ='72';
            obj.style.height ='70'; //FB 1836//FB 1981
            } */
            //if (document.getElementById("menu1") != null)
                //document.getElementById("menu1").style.borderLeft = '2px solid transparent'; FB 2890

            if (document.getElementById("menuHomeRed") != null) { // FB 2890
                document.getElementById("menuHomeRed").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuHomeRed").parentNode.style.borderRight = '#ccc 1px solid';
            }
            if (document.getElementById("menuCalRed") != null) {
                document.getElementById("menuCalRed").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuCalRed").parentNode.style.borderRight = '#ccc 1px solid';
            }
            if (document.getElementById("menuConfRed") != null) {
                document.getElementById("menuConfRed").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuConfRed").parentNode.style.borderRight = '#ccc 1px solid';
            }
            if (document.getElementById("menuSetRed") != null) {
                document.getElementById("menuSetRed").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuSetRed").parentNode.style.borderRight = '#ccc 1px solid';
            }

            if (document.getElementById("menuAdminRed") != null) {
                document.getElementById("menuAdminRed").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuAdminRed").parentNode.style.borderRight = '#ccc 1px solid';
            }

            if (document.getElementById("menuSiteRed") != null) {
                document.getElementById("menuSiteRed").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuSiteRed").parentNode.style.borderRight = '#ccc 1px solid';
            }

            //ZD 101388 START
            if (document.getElementById("menuInstantConferenceRed") != null) {
                document.getElementById("menuInstantConferenceRed").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuInstantConferenceRed").parentNode.style.borderRight = '#ccc 1px solid';
            }
            //ZD 101388 END

            if (document.getElementById("menuHomeBlue") != null) { // FB 2890
                document.getElementById("menuHomeBlue").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuHomeBlue").parentNode.style.borderRight = '#ccc 1px solid';
            }
            if (document.getElementById("menuCalBlue") != null) {
                document.getElementById("menuCalBlue").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuCalBlue").parentNode.style.borderRight = '#ccc 1px solid';
            }
            if (document.getElementById("menuConfBlue") != null) {
                document.getElementById("menuConfBlue").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuConfBlue").parentNode.style.borderRight = '#ccc 1px solid';
            }
            if (document.getElementById("menuSetBlue") != null) {
                document.getElementById("menuSetBlue").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuSetBlue").parentNode.style.borderRight = '#ccc 1px solid';
            }
            if (document.getElementById("menuAdminBlue") != null)
            {
                document.getElementById("menuAdminBlue").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuAdminBlue").parentNode.style.borderRight = '#ccc 1px solid';
            }
            
            if (document.getElementById("menuSiteBlue") != null) {
                document.getElementById("menuSiteBlue").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuSiteBlue").parentNode.style.borderRight = '#ccc 1px solid';
            }

            //ZD 101388 START
            if (document.getElementById("menuInstantConferenceBlue") != null) {
                document.getElementById("menuInstantConferenceBlue").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuInstantConferenceBlue").parentNode.style.borderRight = '#ccc 1px solid';
            }
            //ZD 101388 END

            if (document.getElementById("menuHome") != null) { // FB 2890
                document.getElementById("menuHome").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuHome").parentNode.style.borderRight = '#ccc 1px solid';
            }
            if (document.getElementById("menuCal") != null) {
                document.getElementById("menuCal").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuCal").parentNode.style.borderRight = '#ccc 1px solid';
            }
            if (document.getElementById("menuConf") != null) {
                document.getElementById("menuConf").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuConf").parentNode.style.borderRight = '#ccc 1px solid';
            }
            if (document.getElementById("menuSet") != null) {
                document.getElementById("menuSet").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuSet").parentNode.style.borderRight = '#ccc 1px solid';
            }

            if (document.getElementById("menuAdmin") != null) {
                document.getElementById("menuAdmin").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuAdmin").parentNode.style.borderRight = '#ccc 1px solid';
            }
            
            if (document.getElementById("menuSite") != null) {
                document.getElementById("menuSite").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuSite").parentNode.style.borderRight = '#ccc 1px solid';
            }

            //ZD 101388 START
            if (document.getElementById("menuInstantConference") != null) {
                document.getElementById("menuInstantConference").parentNode.style.borderLeft = '#ccc 1px solid';
                document.getElementById("menuInstantConference").parentNode.style.borderRight = '#ccc 1px solid';
            }
            //ZD 101388 END

            if (document.getElementById("menu1") != null) {
                document.getElementById("menu1").style.borderRight = '#ccc 1px solid';
                document.getElementById("menu1").tabIndex = '-1';
            }

            var themeType = '<%=Session["ThemeType"]%>';
            if (themeType == 2) {
                document.getElementById("menuTopLine").style.backgroundColor = "#CD2522";
            }
            else if (themeType == 1) {
                document.getElementById("menuTopLine").style.backgroundColor = "#4277B3";
            }
            else
                document.getElementById("menuTopLine").style.backgroundColor = "#cccccc";

            var subMenu = ["Hardware", "Locations", "Users", "Options", "Settings", "Audiovisual", "Catering", "Facility"];
            for (var i = 0; i < subMenu.length; i++) {
                var subItem = document.getElementById(subMenu[i]);
                if (subItem != null) {
                    subItem.parentNode.setAttribute("onfocus", "this.onmouseover()");
                    subItem.parentNode.setAttribute("onblur", "this.onmouseout()");
                    if (i == 3 || i == 4) {
                        subItem.parentNode.parentNode.setAttribute("tabindex", "0");
                    }
                    else
                        subItem.parentNode.setAttribute("tabindex", "0");
                }
            }
            if(document.getElementById("menu11")!=null)
                document.getElementById("menu11").removeAttribute("tabindex");

            if (document.getElementById("lnk0") != null)
                document.getElementById("lnk0").tabIndex = "-1";
            if (document.getElementById("lnk2") != null)
                document.getElementById("lnk2").tabIndex = "-1";

            var msg = '<%=Session["xadminAlert"]%>'; // ZD 100172
            if (msg != "")//ZD 101291
                setTimeout("alert('This is the default system account. Please do not use this account for scheduling purposes.');", 100);                
        </script>

    </body>
</html>

<table width="100%" cellpadding="6">
    <tr>
        <td>