<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%><%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="ns_Login.genlogin" ValidateRequest="true" %>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<%--FB 2779 Ends--%>
<html>
<head>
    <link rel="stylesheet" title="Expedite base styles" type="text/css" href="en/App_Themes/CSS/main.css" />
    <%--FB 2719 Starts--%><%--ZD 100156--%>
    <style>
        
        /* ZD 102405 */
        .ie10f
        {
			height: 0px !important;
			line-height: 0px !important;
			padding-top: 22px !important;
			padding-bottom: 22px !important;
		}
			
        body
        {
        	overflow:visible;
        }
        /*ZD 102189 */
        *
        {
            padding: 0;
            margin: 0;
        }
        #divBody
        {
            position:absolute;
            width:100%;
            height:100%;
            text-align: center; /*handles the horizontal centering*/
        }
        /*handles the vertical centering*/
        .Centered
        {
            display: inline-block;
            vertical-align: middle;
        }

        
        #UserName
        {
            height: 40px;
            width: 275px;
            border: 1px solid #BEBEBE;
            background-color: #FFFFFF;
            color: gray;
            font-size: 15px;
            text-align: left;
            background-image: url('../image/user.png');
            background-repeat: no-repeat;
            background-position: right;
        }
        #UserPassword
        {
            height: 40px;
            width: 275px;
            border: 1px solid #BEBEBE;
            background-color: #FFFFFF;
            color: gray;
            font-size: 15px;
            text-align: left;
            background-image: url('../image/pwd.png');
            background-repeat: no-repeat;
            background-position: right;
        }
        #divFooter span
        {
            color: Black;
        }
    </style>
    <%--FB 2719 Ends--%>

    <script language="javascript" type="text/javascript">
        //ZD 100604 start
        var img = new Image();
        img.src = "../en/image/wait1.gif";

        var imgArray = new Array(); // ZD 100740
        for (var i = 0; i < 3; i++) {
            imgArray[i] = new Image();
            imgArray[i].src = "../image/company-logo/SiteLogo.jpg";
        }

        //ZD 100604 start
            
        //FB-1669 Fix
        //    <!--
        if (document.images) {
            preload_image = new Image(25, 25);
            preload_image.src = "image/wait1.gif";
        }
        //-->
        //Fb-1669 Fix End here

        // FB 2719 Starts
       
        function fnTextFocus(xid, par) {
            if (navigator.userAgent.indexOf("MSIE") == -1) {
                var obj = document.getElementById(xid);
                if (par == 1) {
                    if (trim(obj.value) == genloginUsername) { //ZD 103174
                        obj.value = "";
                    }
                }
                else {
                    if (trim(obj.value) == Password) { //ZD 103174
                        obj.value = "";
                        obj.type = "password";
                    }
                }
            }
        }


        function fnTextBlur(xid, par) {
            if (navigator.userAgent.indexOf("MSIE") == -1) {
                var obj = document.getElementById(xid);
                if (par == 1) {
                    if (trim(obj.value) == "") {
                        obj.value = genloginUsername; //ZD 103174
                    }
                }
                else {
                    if (trim(obj.value) == "") {
                        obj.type = "text";
                        obj.value = Password; //ZD 103174
                    }
                }
            }
			
			// FB 2850 Starts FB 3055 Starts
            strUsr = Trim(document.frmGenlogin.UserName.value);
            var obj2 = document.getElementById("errLabel");
            if (obj2.innerHTML == EnterUsername) //ZD 103174
                obj2.innerHTML = "";
            if (strUsr == genloginUsername || strUsr == "") //ZD 103174
                return false;

            if (strUsr.indexOf('@') == -1 && "<%=mailextn %>" != "") {

                //FB 1943


                if ("<%=mailextn %>" != "") {
                    strUsr = strUsr + "@" + "<%=mailextn %>";
                    document.frmGenlogin.UserName.value = strUsr;
                }
                else {

                    if (obj2) {

                        obj2.innerHTML = InvalidUsername; //ZD 103174
                        DataLoading("0");
                    }
                    return false;
                }
            }
            else if ("<%=mailextn %>" != "") {
                var extn = strUsr.split('@')[1];
                var dotchk = extn.substring(1, extn.length - 1);
                if (dotchk.indexOf('.') == -1) {
                    obj2.innerHTML = InvalidUsername; //ZD 103174
                }
            }
			// FB 2850 Ends FB 3055 Ends
			
        }

        function trim(stringToTrim) {
            return stringToTrim.replace(/^\s+|\s+$/g, "");
        }

        // FB 2719 Ends
    </script>

</head>
<!--  #INCLUDE FILE="inc/maintop5.aspx"  -->

<script language="JavaScript" src="inc/functions.js" type="text/javascript"></script>
<body onresize="fnSetImageSize()"> <%--ZD 103085--%>
<div id="divHeader" style="background-color: White; left: 0px; top: 0px; width: 100%;
    height: 25px; position: absolute; z-index: 100;"> <%--FB 2976 --%>
</div><%-- FB 2779 --%>
<div id="divFooter" style="background-color: White; bottom: 0; width: 100%; height: 40px;
    position: fixed; z-index: 100; left:0px"> <%--FB 2976 --%><%--ZD 100156--%>
</div><%-- FB 2779 ZD 102189--%>
<div id="divBody" style="position:absolute; z-index:-1;overflow-y: hidden;">
<img  id="divBodybg" class="Centered" alt="bg image" src="#" /> <%--FB 2976 --%><%--ZD 100156--%>
</div>
<%--Added for FB 1648--%>
<% if (wizard_enable)
   { %>

<script language="JavaScript" type="text/javascript">
	<!--

    var timerRunning = false;
    var toTimer = null;

    function startTimer() {
        toTimer = setTimeout('startTimer()', 1000);
        checkAndRefresh(0);
        timerRunning = true;
    }

    function stopTimer() {
        checkAndRefresh(1);
        if (timerRunning) {
            clearTimeout(toTimer);
        }
    }

    function NewbuttonLogin() {
        if (document.getElementById("UserName").value == genloginUsername) //ZD 103174
            document.getElementById("UserName").value = "";
        if (document.getElementById("UserPassword").value == Password)//ZD 103174
            document.getElementById("UserPassword").value = "";
        //if (document.getElementById('btnSubmit') != null) { // FB 2850
        //    document.getElementById('btnSubmit').click();
        //}
    }

    function validateLogin() {

        NewbuttonLogin(); // FB 2850

        DataLoading("1");
        var strUsr;
        var stralert;
        document.getElementById("errLabel").style.color = "#9E0B0F" // FB 2850
        var obj1 = document.getElementById("errLabel");
        if(obj1)
          obj1.innerHTML = "";

        if (document.frmGenlogin.UserName.value == "") {

            if (obj1) {

                obj1.innerHTML = EnterUsername
                if (navigator.userAgent.indexOf("MSIE") == -1) { // FB 2850 //ZD 103174 start
                    document.getElementById("UserName").value = genloginUsername 
                    document.getElementById("UserPassword").value = Password
                } //ZD 103174 end
                DataLoading("0");
                return false; // FB 2310
            }
            //return false; 
        }
        else {


            strUsr = Trim(document.frmGenlogin.UserName.value);


            // FB 2850 FB 3055 Starts
            if (strUsr.indexOf('@') == -1 && "<%=mailextn %>" != "") {

                //FB 1943


                if ("<%=mailextn %>" != "") {
                    strUsr = strUsr + "@" + "<%=mailextn %>";
                    document.frmGenlogin.UserName.value = strUsr;
                }
                else {

                    if (obj1) {

                        obj1.innerHTML = InvalidUsername; //ZD 103174
                        DataLoading("0");
                    }
                    return false;
                }
            }

            //FB 1943
            if (checkInvalidChar(strUsr) == false) {

                if (obj1) {

                    obj1.innerHTML = InvalidUsername; //ZD 103174
                    DataLoading("0");
                }
                return false;
            } // FB 3055 Ends
        }
        var str;
        str = Trim(document.frmGenlogin.UserPassword.value);



        if (str == "") {
            if (obj1) {
                obj1.innerHTML = EnterPassword;
                if (navigator.userAgent.indexOf("MSIE") == -1) // FB 2850
                {
                    document.getElementById("UserPassword").value = Password
                    document.getElementById("UserPassword").type = "text"
                }
                DataLoading("0");
                return false; // FB 2310
            }
            //return false;
        }
        else {
            if (checkInvalidPassChar(str) == false) {//FB 2339
                DataLoading("0");
                return false;
            }
        }
        //checkAndRefresh(1);
        //DataLoading("0");
        return true;
    }
//ZD 100590 inncrewin 12/18/2013
    function DataLoading(val) {        
        if (val == "1") {
            document.getElementById("dataLoadingDIV").style.display = ''; //innerHTML = '<b><img border="0" src="image/wait1.gif" alt="Loading.." >';  //Edited for FF FB-1669 //ZD 100419
          }
        else
            document.getElementById("dataLoadingDIV").style.display = 'none';
    }
    //ZD 100590 inncrewin 12/18/2013

	//-->
</script>
 <%--ZD 100590 inncrewin 12/18/2013--%>
<style>
.loading
{
    background-image: url('image/wait1.gif');
    height: 33px;
    background-repeat: no-repeat;
    background-position: center center;
      
}
</style>
<%--ZD 100590 inncrewin 12/18/2013--%>
<% }%>
<% if (Application["global"] != null) if (Application["global"].ToString() == "enable")
       {%>

<script language="JavaScript" type="text/javascript">
	<!--

    function international(cb) {
        if (cb.options[cb.selectedIndex].value != "") {
            str = "" + window.location;
            newstr = str.replace(/\/en\//i, "/" + cb.options[cb.selectedIndex].value + "/");
            if ((ind1 = newstr.indexOf("sct=")) != -1) {
                if ((ind2 = newstr.indexOf("&", ind1)) != -1)
                    newstr = newstr.substring(0, ind1 - 1) + newstr.substring(ind2, newstr.length - 1);
                else
                    newstr = newstr.substring(0, ind1 - 1);
            }
            newstr += ((newstr.indexOf("?") == -1) ? "?" : "&") + "sct=" + cb.selectedIndex;
            document.location = newstr;
        }
    }
	
	//-->
</script>

<% } %>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
</table>
<br />
<center>
    <table width="100%" border="0" cellpadding="3" cellspacing="0">
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td>
                &nbsp;
            </td>
        </tr>
        <tr>
            <td width="100%" valign="top" align="center">
                <table width="100%" cellpadding="0" border="0" align="center">
                    <tr>
                        <td width="100%" valign="top" align="center">
                            <form id="frmGenlogin" method="post" runat="server" onsubmit="return validateLogin();"> <%-- FB 2850 --%><%--ZD 100420--%>
                            <div>
                            <%--ZD 103174 start--%>
                                <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		                            <Scripts>                
			                            <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		                            </Scripts>
	                            </asp:ScriptManager> <%--ZD ZD 103174 End--%>

                                <input type="hidden" name="cmd" value="GetHome" />
                                <input type="hidden" name="start" value="0" />
                                <input type="hidden" name="init" value="1" />
                                <input type="hidden" name="hdnBrowserlang" value="" />
                                <input type="hidden" id="hdnURL" runat="server" /> <%--ZD 101477--%>
                                <%--<input type="hidden" id="hdnscreenres" name="hdnscreenres" runat="server" />--%> <%--ZD 100157--%> <%--ZD 100335--%>
                                <center>
                                    <table id="tabSingin" border="0" cellpadding="0" cellspacing="0" style="background-color: #FFFFFF;
                                        -moz-border-radius: 10px; -webkit-border-radius: 10px; border-radius: 10px; border: 3px solid;
                                        font-family: Arial; font-size: medium; width: 290px; height: 400px;">
                                        <%--<tr valign="middle"> FB 2719
      <td width="100%" height="150" align="center"  colspan="2">
           <img id="siteLogoId" src="../image/company-logo/SiteLogo.jpg" onload="" alt="" /> <%-- FB 2050 --%>
                                        <%--width="234" height="84" FB 2407--%>
                                        <%--</td>
        </tr>--%>

                                        <tr align="left">
                                            <td width="40%" style="padding-left: 20px; 
 border-bottom: 1px solid #BEBEBE;" nowrap="nowrap" colspan="2">
                                                <img id="siteLogoId" src="../image/company-logo/SiteLogo.jpg" style="width: 200px;"
                                                    alt="Site Logo" /><%-- FB 2789 --%> <%--ZD 100419 ZD 100740--%>
                                            </td>
                                        </tr>                                        
                                        <tr align="left">
                                            <td width="40%" style="padding-left: 20px" nowrap="nowrap" colspan="2">
                                                <span style="color: #4E4F4F; font-size:23px; font-family:Calibri">
                                                 <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, genlogin_Signin%>" runat="server"></asp:Literal>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <td width="100%" align="center" colspan="2">
                                             <%--ZD 100590 inncrewin 12/18/2013--%>
                                                <div id="dataLoadingDIV" name="dataLoadingDIV" class="loading" style="display:none" align="center">                                                    
                                                </div> <%-- FB 2850 --%>
                                                <asp:label id="errLabel" runat="server" cssclass="lblError"></asp:label>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <%--<td style="width:141; vertical-align:middle" align="center"> FB 2719                  
                  <label for="UserName" class="blackblodtext">Username</label>
                </td>--%>
                                            <td colspan="2" align="center">
                                                <input type="text" name="UserName" runat="server" onfocus="javascript:return fnTextFocus(this.id,1)"
                                                    onblur="javascript:return fnTextBlur(this.id,1)" value="" id="UserName" maxlength="256" class="ie10f"
                                                    onkeyup="javascript:chkLimit(this,'u');" /> <%-- FB 2851 --%> <%--ZD 102405--%>
                                            </td>
                                        </tr>
                                        <tr valign="top">
                                            <%--<td style="width:141; vertical-align:middle" align="center"> FB 2719
                    
                  <label for="UserPassword" class="blackblodtext">Password</label>
                </td>--%>
                                            <td colspan="2" align="center">
                                                <input type="password" name="UserPassword" onkeydown="if(event.keyCode == 13){document.getElementById('btnCSS').focus();document.getElementById('btnCSS').click();}" runat="server" id="UserPassword" value=""
                                                    onfocus="javascript:return fnTextFocus(this.id,2)" onblur="javascript:return fnTextBlur(this.id,2)" class="ie10f"
                                                    maxlength="256" onkeyup="javascript:chkloginLimit(this,'5');" /><%--FB 2339 FB 2850--%> <%--ZD 102405--%>
                                            </td>
                                        </tr>
                                        <tr valign="top" style="height: 50px;">
                                            <td align="center" colspan="2">
                                                <asp:button id="btnSubmit2" runat="server" text="<%$ Resources:WebResources, genlogin_Signin%>" style="width: 250px; display: none;
                                                    height: 50px; background-color: Red" onclientclick="return validateLogin();"
                                                    /> <%-- 2850 --%>
                                                <asp:button id="btnCSS" runat="server" Text="<%$ Resources:WebResources, genlogin_Signin%>" style="width: 275px; height: 50px;
                                                    color: #FFFFFF; font-size:larger; border: 0PX; background-color:#444444; font-weight:bold"
                                                    onclick="btnSubmit_Click" OnClientClick="return validateLogin();" /><!-- FB 2779 -->
                                                    
                                            </td>
                                        </tr>
                                        <tr valign="top" style="display: none">
                                            <td colspan="2" align="center">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr valign="top" style="height: 10px;">
                                            <%--<td style="width: 25px" align="center">
                                                &nbsp;
                                            </td>--%>
                                            <td style="width: 281px;" align="center"><br /><%--ZD 102227--%>
                                                <asp:checkbox name="RememberMe" id="RememberMe" value="1" runat="server" />
                                                <label for="RememberMe">
                                                    <font style="color: #696B6E; font-size: small; font-weight: bold">
                                                    <asp:Literal Text="<%$ Resources:WebResources, genlogin_Rememberme%>" runat="server"></asp:Literal>
                                                    </font></label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" valign="top" style="padding-top: 10px;">
                                                <table width="281px" border="0">
                                                    <tr valign="top">
                                                        <td id="lblForgotPassword" runat="server" style="width: 150;text-align:center;" nowrap="nowrap"><%--ZD 102227--%>
                                                            &nbsp;<a href="emaillogin.aspx" id="anCEmailLogin" style="color: #4272A8; font-size: small; font-weight: bold" onclick="javascript:fnChgReq(this.id);"><%--ZD 103531--%>
                                                            <asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, genlogin_Forgot%>" runat="server"></asp:Literal>
                                                            </a>
                                                        </td>
                                                        </tr>
                                                        <% if (Application["GetActLNK"] != null) if (Application["GetActLNK"].ToString() == "enable")
                                                               { 
                                                        %>
                                                        <tr valign="top">
                                                        <td id="lblViewPublicConf" runat="server" style="width: 150;text-align:center;" nowrap="nowrap"><!-- FB 2858 --><%--ZD 102227--%>
                                                            &nbsp;<a href="ConferenceList.aspx?t=4&hf=1" id="anCConfList" style="color: #4272A8; font-size: small;
                                                                font-weight: bold; white-space: nowrap" onclick="javascript:fnChgReq(this.id);"><%--ZD 103531--%>
                                                                <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, genlogin_ViewPublic%>" runat="server"></asp:Literal></a>
                                                        </td>
                                                        </tr>
                                                        <% 
                                                            }   
                                                        %>
                                                        <% if (Application["ViwPubLNK"] != null) if (Application["ViwPubLNK"].ToString() == "enable")
                                                               {   
                                                        %>
                                                        <%--<tr valign="top">             
               <td style="width:141" align="center">                    
                  &nbsp;
                </td>
                <td style="width:245">&nbsp;<a href="ConferenceList.aspx?t=4&hf=1" onclick="javascript:DataLoading('1')">View Public Conferences?</a></td>
                </tr>--%>
                                                        <% 
                                                            }   
                                                        %>
                                                    
													<%--ZD 101846 Start--%> 
                                                    <tr id="trReqUsrAcc" runat="server" style="height:10px;">
                                                        <td style="width: 150;text-align:center;" colspan="2" nowrap="nowrap"><!-- FB 2858 --><%--ZD 102227--%>
                                                            &nbsp;<a href="requestaccount.aspx"  id="anCReqAcc" style="color: #4272A8; font-size: small; 
                                                                font-weight: bold; white-space: nowrap" onclick="javascript:fnChgReq(this.id);"><asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, genlogin_NeedAcc%>" runat="server"></asp:Literal></a><%--ZD 103531--%>
                                                        </td>
                                                    </tr>
													<%--ZD 101846 End--%> 
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                            </div>
                            </form>

                            <script language="JavaScript" type="text/javascript">
<!--

                                document.frmGenlogin.UserName.focus();
                                //	startTimer();

//-->
                            </script>

                            <!-------------------------------------------------------------------->
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</center>

<script language="javascript" type="text/javascript">
    if (document.layers) document.captureEvents(Event.KEYPRESS)
    document.onkeypress = kpress;
    function kpress(e) {
        key = (document.layers) ? e.which : window.event.keyCode

        if (key == 13) {

            if (document.getElementById('btnSubmit') != null) {
                //document.getElementById('btnSubmit').click();
            }

        }
    }
    
    // ZD 100263
    if ("<%=mailextn %>" == "") {
        document.getElementById("UserName").setAttribute("autocomplete", "off");
        document.getElementById("UserPassword").setAttribute("autocomplete", "off");
    }
</script>

<br />
<br />

<script language="JavaScript" type="text/javascript"> //mainbottom1.asp Code Lines
	var mt = "";
	var _d = document;

	mt += "<center><table border='0' cellpadding='2' cellspacing='2'>";
	mt += "<tr valign='bottom'>";
	mt += "<td colspan=5 align=center>";
    //FB 1985 - Starts
     
    var browserlang = '<%=Session["browserlang"]%>'; 
	if('<%=Application["Client"]%>'.toUpperCase() == "DISNEY")
	{
      
       if((browserlang.indexOf("en-US")> -1) || (browserlang.indexOf("en")> -1))
            mt += "<span class=srcstext2>myVRM Version <%=Application["Version"].ToString()%>(c), Copyright <%=Application["CopyrightsDur"].ToString()%> myVRM.com.  All Rights Reserved.</span>";//FB 1648
       else if((browserlang.indexOf("fr-CA")> -1) || (browserlang.indexOf("fr")> -1))
            mt += "<span class=contacttext>Version de myVRM <%=Application["Version"].ToString()%>(c), Copyright <%=Application["CopyrightsDur"].ToString()%> myVRM.com.  Tous les droits sont r&#233;serv&#233;s.</span>"; //fb 1172 FB 2827
       else if((browserlang.indexOf("es-US")> -1) || (browserlang.indexOf("es")> -1))
            mt += "<span class=contacttext>Version myVRM <%=Application["Version"].ToString()%>(c), Derechos de autor <%=Application["CopyrightsDur"].ToString()%> myVRM.com.  Todos los Derechos Reservados.</span>"; //fb 1172 FB 2827
	    
	}
	else
	{

     if((browserlang.indexOf("en-US")> -1) || (browserlang.indexOf("en")> -1))
            mt += "<span class=srcstext2>myVRM Version <%=Application["Version"].ToString()%>(c), Copyright <%=Application["CopyrightsDur"].ToString()%> <a href='http://www.myvrm.com' target='_blank'>myVRM.com</a>.  All Rights Reserved.</span>";//FB 1648
     else if((browserlang.indexOf("fr-CA")> -1) || (browserlang.indexOf("fr")> -1))
            mt += "<span class=contacttext>Version de myVRM <%=Application["Version"].ToString()%>(c), Copyright <%=Application["CopyrightsDur"].ToString()%> <a style='color:#3b73b9' href='http://www.myvrm.com' target='_blank'>myVRM.com</a>.  Tous les droits sont r&#233;serv&#233;s.</span>"; //fb 1172 FB 2827
     else if((browserlang.indexOf("es-US")> -1) || (browserlang.indexOf("es")> -1))
            mt += "<span class=contacttext>Versi\u00f3n myVRM <%=Application["Version"].ToString()%>(c), Derechos de autor <%=Application["CopyrightsDur"].ToString()%> <a style='color:#3b73b9' href='http://www.myvrm.com' target='_blank'>myVRM.com</a>.  Todos los Derechos Reservados.</span>"; //fb 1172 FB 2827

	}
    //FB 1985 - Starts
	mt += "</td>";
	mt += "</tr>";
	mt += "</table></center>";

    document.getElementById("divFooter").innerHTML = mt;
	//_d.write(mt)

</script>

<!--code added for CSS Module-->

<script language="javascript" type="text/javascript">
    //    function adjustWidth(obj)
    //    {
    //        alert('hi');
    //        if(obj.style.width == "")
    //        if (obj.src.indexOf("lobbytop1024.jpg") >= 0)
    //        {
    //            obj.style.width=window.screen.width-25;            

    var obj = document.getElementById('mainTop');
    if (obj != null) {
        //FB 1830
        //                if (window.screen.width <= 1024)
        //                    obj.src = "../en/image/company-logo/StdBanner.jpg"; // Organization Css Module 
        //                else
        //                    obj.src = "../en/image/company-logo/HighBanner.jpg";  //Organization Css Module 

        if (window.screen.width <= 1024)
            obj.src = "../image/company-logo/StdBanner.jpg"; // Organization Css Module  //FB 1830
        else
            obj.src = "../image/company-logo/HighBanner.jpg";  //Organization Css Module //FB 1830
    }
    //        }
    //    }

   
 
</script>

<!-- PB gradiend -->
<!-- FB 2050 Start -->

<script type="text/javascript">
    function refreshImage() { // ZD 100740
        var obj = document.getElementById("siteLogoId");
        obj.src = "../image/company-logo/SiteLogo.jpg";
        /*
        if (obj != null) {
            var src = obj.src;
            var pos = src.indexOf('?');
            if (pos >= 0) {
                src = src.substr(0, pos);
            }
            var date = new Date();
            obj.src = src + '?v=' + date.getTime();
        }
        return false;
        */
    }
    //if (navigator.userAgent.indexOf("Chrome") == -1) // FB 3055
    for(var i=100; i<1001; i+=100)
        setTimeout("refreshImage();", i);

    //FB 2487 - Start
    var obj = document.getElementById("errLabel");
    if (obj != null) {
        var strInput = obj.innerHTML.toUpperCase();
        //ZD 103174 start
        var browserlang = '<%=Session["browserlang"]%>';
        if (strInput == "INVALID LOGIN OR PASSWORD. PLEASE TRY AGAIN.") 
        {
            //var browserlang = '<%=Session["browserlang"]%>';
                            if((browserlang.indexOf("en-US")> -1) || (browserlang.indexOf("en")> -1))
                              obj.innerHTML = "Invalid login or password. Please try again.";
                            if((browserlang.indexOf("fr-CA")> -1) || (browserlang.indexOf("fr")> -1))
                               obj.innerHTML = "Login ou mot de passe invalide. Se il vous pla�t essayer � nouveau.";
                            if((browserlang.indexOf("es-US")> -1) || (browserlang.indexOf("es")> -1))
                               obj.innerHTML = "Inicio de sesi�n o contrase�a inv�lidos. Por favor vuelva a intentarlo.";
         }
        if (strInput == "SITE IS CURRENTLY NOT ACTIVATED.  PLEASE CONTACT YOUR VRM ADMINISTRATOR FOR ASSISTANCE") {
           
            if ((browserlang.indexOf("en-US") > -1) || (browserlang.indexOf("en") > -1))
                obj.innerHTML = "Site is currently not activated.  Please contact your VRM administrator for assistance.";
            if ((browserlang.indexOf("fr-CA") > -1) || (browserlang.indexOf("fr") > -1))
                obj.innerHTML = "Le site n'est pas activ� actuellement. Veuillez contacter votre administrateur VRM aux fins d'assistance.";
            if ((browserlang.indexOf("es-US") > -1) || (browserlang.indexOf("es") > -1))
                obj.innerHTML = "Inicio de sesi�n o contrase�a inv�lidos. Por favor vuelva a intentarlo.";
        }

        if (strInput == "YOUR ACCOUNT IS LOCKED DUE TO MULTIPLE INVALID PASSWORD ATTEMPTS.PLEASE CONTACT YOUR VRM ADMINISTRATOR.") {

            if ((browserlang.indexOf("en-US") > -1) || (browserlang.indexOf("en") > -1))
                obj.innerHTML = "Your account is locked due to multiple invalid password attempts.Please contact your VRM administrator."; //ZD 103439
            if ((browserlang.indexOf("fr-CA") > -1) || (browserlang.indexOf("fr") > -1))
                obj.innerHTML = "Votre compte est verrouill� suite � de multiples tentatives avec mot de passe non valide. Veuillez contacter votre administrateur VRM.";
            if ((browserlang.indexOf("es-US") > -1) || (browserlang.indexOf("es") > -1))
                obj.innerHTML = "Su cuenta est� bloqueada debido a las m�ltiples intentos de contrase�a inv�lidas.Por favor contacta el administrador VRM.";
        }
        //ZD 103174 End
        //ZD 103439 Starts
        if (strInput == "YOUR ACCOUNT HAS BEEN LOCKED. PLEASE CONTACT YOUR VRM ADMINISTRATOR FOR ASSISTANCE.") {

            if ((browserlang.indexOf("en-US") > -1) || (browserlang.indexOf("en") > -1))
                obj.innerHTML = "Your account has been locked. Please contact your VRM administrator for assistance."; 
            if ((browserlang.indexOf("fr-CA") > -1) || (browserlang.indexOf("fr") > -1))
                obj.innerHTML = "Votre compte a �t� bloqu�. Veuillez contacter votre administrateur VRM � des fins d'assistance.";
            if ((browserlang.indexOf("es-US") > -1) || (browserlang.indexOf("es") > -1))
                obj.innerHTML = "Su cuenta ha sido bloqueada. Por favor, p�ngase en contacto con su administrador de VRM para obtener ayuda.";
        }
        if (strInput == "YOUR VRM ACCOUNT HAS EXPIRED AND HAS BEEN LOCKED. PLEASE CONTACT YOU LOCAL VRM ADMINISTRATOR FOR FURTHER ASSISTANCE") {

            if ((browserlang.indexOf("en-US") > -1) || (browserlang.indexOf("en") > -1))
                obj.innerHTML = "Your VRM account has expired and has been locked. Please contact you local VRM administrator for further assistance";
            if ((browserlang.indexOf("fr-CA") > -1) || (browserlang.indexOf("fr") > -1))
                obj.innerHTML = "Arriv� � expiration, votre compte VRM a �t� bloqu�. Veuillez contacter votre administrateur VRM local pour recevoir plus d'assistance";
            if ((browserlang.indexOf("es-US") > -1) || (browserlang.indexOf("es") > -1))
                obj.innerHTML = "Su cuenta VRM ha expirado y ha sido bloqueada.  Por favor, p�ngase en contacto con su administrador de VRM local para obtener m�s ayuda.";
        }
        if (strInput == "PLEASE ENTER THE PASSWORD.") {

            if ((browserlang.indexOf("en-US") > -1) || (browserlang.indexOf("en") > -1))
                obj.innerHTML = "Please enter the password.";
            if ((browserlang.indexOf("fr-CA") > -1) || (browserlang.indexOf("fr") > -1))
                obj.innerHTML = "Veuillez saisir le mot de passe.";
            if ((browserlang.indexOf("es-US") > -1) || (browserlang.indexOf("es") > -1))
                obj.innerHTML = "Por favor, introduzca la contrase�a.";
        }
        if (strInput == "YOUR ACCOUNT PASSWORD HAS BEEN EXPIRED. PLEASE CONTACT YOUR VRM ADMINISTRATOR FOR ASSISTANCE.") {

            if ((browserlang.indexOf("en-US") > -1) || (browserlang.indexOf("en") > -1))
                obj.innerHTML = "Your account password has been expired. Please contact your VRM administrator for assistance.";
            if ((browserlang.indexOf("fr-CA") > -1) || (browserlang.indexOf("fr") > -1))
                obj.innerHTML = "Le mot de passe de votre compte a expir�. Veuillez contacter votre administrateur VRM � des fins d'assistance.";
            if ((browserlang.indexOf("es-US") > -1) || (browserlang.indexOf("es") > -1))
                obj.innerHTML = "Tu contrase�a de la cuenta ha caducado. Por favor, p�ngase en contacto con el administrador del VRM para obtener ayuda.";
        }
        //ZD 103439 Ends
        if (((strInput.indexOf("SUCCESS") > -1) && !(strInput.indexOf("UNSUCCESS") > -1) && !(strInput.indexOf("ERROR") > -1))
        || (strInput.indexOf("QUITT�") > -1) || (strInput.indexOf("SATISFACT") > -1))  {
            obj.setAttribute("class", "lblMessage");
            obj.setAttribute("className", "lblMessage");
        }
        else {
            obj.setAttribute("class", "lblError");
            obj.setAttribute("className", "lblError");
        }
    }
    //FB 2487 - End
    // FB 2719 Starts
    /* Commented for FB 2976 
    if (screen.width > 900) {
        document.getElementById("divHeader").style.marginLeft = (screen.width-900)/2 + 'px';
        document.getElementById("divFooter").style.marginLeft = (screen.width - 900) / 2 + 'px';
        document.getElementById("divBody").style.marginLeft = (screen.width - 900) / 2 + 'px';
    } */

    if (navigator.userAgent.indexOf("MSIE") == -1) {
        if(document.getElementById("UserName").value == "") // FB 2850
            document.getElementById("UserName").value = genloginUsername;
        document.getElementById("UserPassword").type = "text";
        document.getElementById("UserPassword").value = Password;
        document.getElementById("btnCSS").focus();
    }

    // FB 2779 Starts //ZD 100996 - Start //ZD 103581 - Start
    var stat = '<%=exist%>';
    var themeType = '<%=Session["ThemeType"]%>';

    function calScreenRatio() {
        var width = screen.width / 16; // 256
        var height = screen.height / 9; // 256
        var variation = width - height;
        var wide = false;
        if(variation > -10 && variation < 10)
            wide = true;

        if (wide == true && stat.indexOf('wide') > -1) {
            // Wide
            document.getElementById("divBodybg").src = "../image/company-logo/LoginBackground.jpg?" + new Date().getTime();
        }
        else if (wide == false && stat.indexOf('normal') > -1) {
            // Normal
            document.getElementById("divBodybg").src = "../image/company-logo/LoginBackgroundNormal.jpg?" + new Date().getTime();
        }
        else {
            if (wide == true)
                fnSetDefaultBg();
            else
                fnSetDefaultNormalBg();
        }
        document.getElementById("tabSingin").style.borderColor = "#888888";
    }

    function fnSetDefaultBg() {
        if (themeType == 2) {
            document.getElementById("divBodybg").src = "../image/company-logo/redPatternWide.png?" + new Date().getTime(); // FB 2976
            document.getElementById("tabSingin").style.borderColor = "#CD2522";
        }
        else if (themeType == 1) {
            document.getElementById("divBodybg").src = "../image/company-logo/bluePatternWide.png?" + new Date().getTime(); // FB 2976
            document.getElementById("tabSingin").style.borderColor = "#4277B3";
        }
        else {
            document.getElementById("divBodybg").src = "../image/company-logo/bluePatternWide.png?" + new Date().getTime(); // FB 2976
            document.getElementById("tabSingin").style.borderColor = "#888888";
        }
    }

    function fnSetDefaultNormalBg() {
        if (themeType == 2) {
            document.getElementById("divBodybg").src = "../image/company-logo/redPatternNormal.png?" + new Date().getTime(); // FB 2976
            document.getElementById("tabSingin").style.borderColor = "#CD2522";
        }
        else if (themeType == 1) {
            document.getElementById("divBodybg").src = "../image/company-logo/bluePatternNormal.png?" + new Date().getTime(); // FB 2976
            document.getElementById("tabSingin").style.borderColor = "#4277B3";
        }
        else {
            document.getElementById("divBodybg").src = "../image/company-logo/bluePatternNormal.png?" + new Date().getTime(); // FB 2976
            document.getElementById("tabSingin").style.borderColor = "#888888";
        }
    }
    calScreenRatio();

//    if (stat != "") {        
//        calScreenRatio();
//    }
//    else {
//        fnSetDefaultBg();
//    }
//    //ZD 102263
	
    function fnSetImageSize() {
        calScreenRatio();
        var imageWidth = document.getElementById("divBodybg").width;
        var imageHeight = document.getElementById("divBodybg").height;
        var screenWidth = document.body.clientWidth;
        var screenHeight = document.body.clientHeight - 0;
        var monitorHeight = screen.height;

        document.getElementById("divBodybg").width = screenWidth;
        document.getElementById("divBodybg").removeAttribute("height");
        document.getElementById("divBodybg").style.marginLeft = '0px';       
        
        document.getElementById("divBodybg").style.marginTop = Math.round((5 / 100) * monitorHeight) + 'px';
        document.getElementById("tabSingin").style.marginTop = Math.round((4 / 100) * monitorHeight) + 'px';
    }

    // Handle query string for src
    // Function before call
//    function fnSetImageSize() { // ZD 102723 ZD 103085
//        var aspectRatio;
//        var imageWidth = document.getElementById("divBodybg").width;
//        var imageHeight = document.getElementById("divBodybg").height;
//        var screenWidth = document.body.clientWidth;  //screen.width;
//        var screenHeight = document.body.clientHeight - 0;  // screen.height - 150;

//        aspectRatio = screenHeight / (imageHeight - 0);
//        document.getElementById("divBodybg").width = imageWidth * aspectRatio;
//        if ((imageWidth * aspectRatio) < screenWidth) {
//            document.getElementById("divBodybg").width = screenWidth;
//            document.getElementById("divBodybg").removeAttribute("height");
//            document.getElementById("divBodybg").style.marginLeft = '0px';
//        }
//        else {
//            document.getElementById("divBodybg").removeAttribute("width");
//            document.getElementById("divBodybg").height = screenHeight;
//            var adj = ((imageWidth * aspectRatio) - screenWidth) / 2;
//            document.getElementById("divBodybg").style.marginLeft = (adj * -1) + 'px';
//        }
//    }
	//ZD 103581 - End

    function fnChkImageSize() {
        var wd = document.getElementById("divBodybg").width;
        if (wd < 100) // ZD 103085
            setTimeout("fnChkImageSize()", 100);
        else {
            //if (wd > 1280)
            //document.getElementById("divBodybg").width = screen.width;
            fnSetImageSize(); // ZD 102723
              
        }
    }

    setTimeout("fnChkImageSize()", 100);

    // FB 2779 Ends //ZD 100996 - End
        
    // FB 2719 Ends

    //ZD 102405
    if (navigator.userAgent.indexOf("Trident") == -1) {
        document.getElementById("UserName").className = "";
        document.getElementById("UserPassword").className = "";        
    }

    //document.getElementById("hdnscreenres").value = screen.width; //ZD 100157 //ZD 100335
    //ZD 101477
    function getURL() {
        if (document.getElementById("hdnURL") != null)
            document.getElementById("hdnURL").value = document.URL;
    }
    getURL();

	//ZD 103531 - Start
    function fnChgReq(arg) 
    {
        var Browserlang = navigator.userLanguage || navigator.language;
        var curReq = document.getElementById(arg).href;

        var qryStr = "?";
        if (curReq.indexOf('?') > -1)
            qryStr = "&";

        if (Browserlang != "" && Browserlang != null)
            document.getElementById(arg).href = curReq + qryStr + 'lang="' + Browserlang + '"';
        else
            document.getElementById(arg).href = curReq + qryStr + 'lang="en-US"';
    
    }
	//ZD 103531 - End
    
</script>
<!-- FB 2050 End -->
</body>
</html>
