<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100866 End--%>
<%@ Page language="c#" Inherits="myVRMAdmin.Web.en.Esthetic"%>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>
<HTML>	
	<BODY bottomMargin="0" leftMargin="0" topMargin="0" rightMargin="0">
		<script language="javascript" src="Organizations/Original/Javascript/RGBColorPalette.js"> </script>

		<script Language="JavaScript">
<!--

var imtime1 = parseInt("", 10);
imtime1 = ( (imtime1 < 0.5) || isNaN(imtime1) ) ? 0.5 : imtime1;

//-->
</script>
<%--color.jpg path changed from Organizations/Original/Image/color.jpg to ../Image/color.jpg for FB 1830--%>		
<!-- script finished -->
		<TABLE cellPadding="2" width="100%">
			<TBODY>
				<TR>
					<TD><!--------------------------------- CONTENT START HERE ---------------> <!--------------------------------- CONTENT GOES HERE --------------->  <!-- Java Script Begin -->
						<!-- Java Script End -->
						<CENTER>
							<H3>Customize User Interface Settings</H3>
							<asp:label id="errLabel" Runat="server" CssClass="lblError"></asp:label>
						</CENTER>
						<FORM id="frmSample" autocomplete="off" name="frmSample" method="post" runat="server"><%--ZD 101190--%>
							<CENTER>
								<TABLE id="Table5" cellSpacing="0" cellPadding="0" width="860" align="center" border="0">
									<TBODY>
										<TR>
											<TD align="center" width="3%">
												<TABLE id="Table6" width="25" align="right" border="0">
													<TBODY>
														<TR>
															<TD align="center" class="altColoredBackground" height="20"><SPAN class="numwhiteblodtext">1</SPAN>
															</TD>
														</TR>
													</TBODY>
												</TABLE>
											</TD>
											<TD width="1%" height="20">&nbsp;</TD>
											<TD align="left" width="96%" colSpan="2" height="20"><SPAN class="subtitleblueblodtext">Select 
													Background Color</SPAN>
											</TD>
										</TR>
										<tr>
											<td height="20"></td>
											<td></td>
											<td vAlign="top" align="center">
												<table cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													<tbody>
														<tr>
															<td vAlign="top" align="center" width="15%">
															</td>
															<td align="left">
																<table cellSpacing="0" cellPadding="3" b0rderColor="#cccccc" border="1" width="95%">
																	<tr class="tableHeader">
																		<td align="center" width="20%" class="tableHeader">Description</td>
																		<td align="center" width="20%" class="tableHeader">Current</td>
																		<td align="center" width="60%" class="tableHeader">New</td>
																	</tr>
																	<tr bgColor="#ffffff">
															            <td>Background Color</td>
															            <td>
																            <table>
																	            <tr>
																		            <td>
																			            <asp:Label ID="lblbodybgcolor1" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid" ></asp:Label>
																			            <asp:Label ID="lblbodybgcolor" Runat="server" ></asp:Label>
																		            </td>
																	            </tr>
																            </table>
															            </td>
															            <td>
																            <table cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
																	            <tbody>
																		            <tr>
																			            <td vAlign="top" align="left" ><asp:radiobutton id="NamedColor" Runat="server" GroupName="ColorSystem" Checked="True"></asp:radiobutton>Named Color</td>
																			            <td align="left"><asp:dropdownlist id="NamedCol" Runat="server" CssClass="altLong0SelectFormat">
																					            <asp:ListItem Value="">Select the Color</asp:ListItem>
																					            <asp:ListItem Value="Green">Green</asp:ListItem>
																					            <asp:ListItem Value="Lime">Lime</asp:ListItem>
																					            <asp:ListItem Value="Teal">Teal</asp:ListItem>
																					            <asp:ListItem Value="Aqua">Aqua</asp:ListItem>
																					            <asp:ListItem Value="Navy">Navy</asp:ListItem>
																					            <asp:ListItem Value="Blue">Blue</asp:ListItem>
																					            <asp:ListItem Value="Purple">Purple</asp:ListItem>
																					            <asp:ListItem Value="Fuchsia">Fuchsia</asp:ListItem>
																					            <asp:ListItem Value="Maroon">Maroon</asp:ListItem>
																					            <asp:ListItem Value="Red">Red</asp:ListItem>
																					            <asp:ListItem Value="Olive">Olive</asp:ListItem>
																					            <asp:ListItem Value="Fuchsia">Fuchsia</asp:ListItem>
																					            <asp:ListItem Value="Yellow">Yellow</asp:ListItem>
																					            <asp:ListItem Value="White">White</asp:ListItem>
																					            <asp:ListItem Value="Silver">Silver</asp:ListItem>
																					            <asp:ListItem Value="Black">Black</asp:ListItem>
																					            <asp:ListItem Value="Gray">Gray</asp:ListItem>
																				            </asp:dropdownlist>
																			            </td>
																		            </tr>
																		            <tr>
																			            <td vAlign="middle" align="left"><asp:radiobutton id="RGBColor" Runat="server" GroupName="ColorSystem"></asp:radiobutton>RGB Palette</td>
																			            <td align="left">
																				            <table>
																					            <tr>
																						            <td><asp:textbox EnableViewState="True" id="TxtRGBColor" Runat="server" CssClass="altText" ></asp:textbox></td>
																						            <td>
																							            <IMG title="Click to select the color" height="23" alt="Color" src="../Image/color.jpg"
																								            width="27" name="rgbimg">  <%-- Organization Css Module --%>
																						            </td>
																					            </tr>
																				            </table>
																			            </td>
																		            </tr>
																	            </tbody>
																            </table>
															            </td>
														            </tr>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<TR>
											<TD align="center" width="3%">
												<TABLE id="Table4" width="25" align="right" border="0">
													<TBODY>
														<TR>
															<TD align="center" class="altColoredBackground" height="20"><SPAN class="numwhiteblodtext">2</SPAN>
															</TD>
														</TR>
													</TBODY>
												</TABLE>
											</TD>
											<TD width="1%" height="20">&nbsp;</TD>
											<TD align="left" width="96%" colSpan="2" height="20"><SPAN class="subtitleblueblodtext">Select 
													General Font Property</SPAN>
											</TD>
										</TR>
										<tr>
											<td height="20"></td>
											<td></td>
											<td vAlign="top" align="center">
												<table id="tablegeneral" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													<tbody>
														<tr>
															<td vAlign="top" align="left" width="15%">
															</td>
															<td align="left">
																<table cellSpacing="0" cellPadding="3" borderColor="#cccccc" border="1" width="95%">
																	<tr>
																		<td align="center" width="15%" class="tableHeader">Description</td>
																		<td align="center" width="22%" class="tableHeader">Current</td>
																		<td align="center" width="32%" class="tableHeader">New</td>
																	</tr>
																	<tr>
																		<td>Font Name</td>
																		<td>
																			<asp:Label ID="lblgenname" Runat="server" ></asp:Label>
																		</td>
																	    <td>
																		    <asp:dropdownlist id="drpgenfont" Runat="server" CssClass="altLong0SelectFormat" Width="150">
																			    <asp:ListItem Value="">Select the Font</asp:ListItem>
																			    <asp:ListItem Value="Arial">Arial</asp:ListItem>
																			    <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																			    <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																			    <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																			    <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																			    <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
																			    <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																			    <asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																			    <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																			    <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
																		    </asp:dropdownlist>
																	    </td>
																	</tr>
																	<tr bgColor="#ffffff">
																	    <td>Font Size</td>
																	    <td>
																		    <asp:Label ID="lblgensize" Runat="server"></asp:Label>
																	    </td>
																	    <td>
                											                <asp:dropdownlist id="drpgensize" Runat="server" CssClass="altLong0SelectFormat" Width="55">
															                    <asp:ListItem Value="">Size</asp:ListItem>
															                    <asp:ListItem Value="8">8</asp:ListItem>
															                    <asp:ListItem Value="9">9</asp:ListItem>
															                    <asp:ListItem Value="10">10</asp:ListItem>
															                    <asp:ListItem Value="11">11</asp:ListItem>
															                    <asp:ListItem Value="12">12</asp:ListItem>
															                    <asp:ListItem Value="14">14</asp:ListItem>
															                    <asp:ListItem Value="16">16</asp:ListItem>
															                    <asp:ListItem Value="18">18</asp:ListItem>
															                    <asp:ListItem Value="20">20</asp:ListItem>
															                    <asp:ListItem Value="22">22</asp:ListItem>
														                    </asp:dropdownlist>
															            </td>
																	</tr>
																	<tr bgColor="#ffffff">
																		<td>Font Color</td>
																		<td>
																		    <asp:Label ID="lblgencolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		    <asp:Label ID="lblgencolor1" Runat="server"></asp:Label>
																	    </td>
																		<td>
																			<asp:textbox EnableViewState="True" id="txtgencolor" Runat="server" CssClass="altText"></asp:textbox>
																			<IMG title="Click to select the color" onclick="FnShowPalette(event,'txtgencolor')" height="23"
																				alt="Color" src="../Image/color.jpg" width="27" name="imggen">  <%-- Organization Css Module --%>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<tr>
											<td align="center" width="3%">
												<table id="tabletitle" width="25" align="right" border="0">
													<tr>
														<td align="center" class="altColoredBackground" height="20"><span class="numwhiteblodtext">3</span>
														</td>
													</tr>
												</table>
											</td>
											<td width="1%" height="20">&nbsp;</td>
											<td align="left" width="96%" colspan="2" height="20"><span class="subtitleblueblodtext"><b>Select 
														Title Font Property</b></span></td>
										</tr>
										<tr>
											<td height="20"></td>
											<td></td>
											<td vAlign="top" align="center">
												<table id="tabletitlefont" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													<tbody>
														<tr>
															<td vAlign="top" align="center" width="15%"><B></B>
															</td>
															<td align="left">
																<table cellSpacing="0" cellPadding="3" borderColor="#cccccc" border="1" width="95%">
																	<tr>
																		<td align="center" width="15%" class="tableHeader">Description</td>
																		<td align="center" width="22%" class="tableHeader">Current</td>
																		<td align="center" width="32%" class="tableHeader">New</td>
																	</tr>
																    <tr bgColor="#ffffff">
																	    <td>Font Name</td>
																	    <td>
																		    <asp:Label ID="lbltitlename" Runat="server"></asp:Label>
        															    </td>
																	    <td>
																	        <asp:dropdownlist id="drptitlefont" Runat="server" CssClass="altLong0SelectFormat" Width="150">
																		        <asp:ListItem Value="">Select the Font</asp:ListItem>
																		        <asp:ListItem Value="Arial">Arial</asp:ListItem>
																		        <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																		        <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																		        <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																		        <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																		        <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
                    													        <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																				<asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																			    <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																			    <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
																		    </asp:dropdownlist>
																		</td>
																	</tr>
																	<tr bgColor="#ffffff">
																		<td>Font Size</td>
																		<td>
																			<asp:Label ID="lbltitlesize" Runat="server"></asp:Label>
																		</td>
																		<td>
																			<asp:dropdownlist id="drptitlesize" Runat="server" CssClass="altLong0SelectFormat" Width="55">
																				<asp:ListItem Value="">Size</asp:ListItem>
																				<asp:ListItem Value="8">8</asp:ListItem>
																				<asp:ListItem Value="9">9</asp:ListItem>
																				<asp:ListItem Value="10">10</asp:ListItem>
																				<asp:ListItem Value="11">11</asp:ListItem>
																				<asp:ListItem Value="12">12</asp:ListItem>
																				<asp:ListItem Value="14">14</asp:ListItem>
																				<asp:ListItem Value="16">16</asp:ListItem>
																				<asp:ListItem Value="18">18</asp:ListItem>
																				<asp:ListItem Value="20">20</asp:ListItem>
																				<asp:ListItem Value="22">22</asp:ListItem>
																			</asp:dropdownlist>
																		</td>
																	</tr>
																	<tr bgColor="#ffffff">
																	    <td>Font Color</td>
																	    <td>
																		    <asp:Label ID="lbltitlecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		    <asp:Label ID="lbltitlecolor1" Runat="server"></asp:Label>
																	    </td>
																	    <td>
																		    <asp:textbox EnableViewState="True" id="txttitlecolor" Runat="server" CssClass="altText"></asp:textbox>
																		    <IMG title="Click to select the color" onclick="FnShowPalette(event,'txttitlecolor')"
																			    height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgtitlefontcolor"> <%-- Organization Css Module --%>
																	    </td>
																	</tr>
																</table>
															</td>
														</tr>
																				</table>
																		</td>
										</tr>
										<tr>
											<td align="center" width="3%">
												<table id="tablesubtitlefont" width="25" align="right" border="0">
													<tr>
														<td align="center" class="altColoredBackground" height="20"><span class="numwhiteblodtext">4</span>
														</td>
													</tr>
												</table>
											</td>
											<td width="1%" height="20">&nbsp;</td>
											<td align="left" width="96%" colspan="2" height="20"><span class="subtitleblueblodtext"><b>Select 
														Subtitle Font Property</b></span></td>
										</tr>
										<tr>
											<td height="20"></td>
											<td></td>
											<td vAlign="top" align="center">
												<table id="Table11" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													<tbody>
														<tr>
															<td vAlign="top" align="left" width="15%"><B></B>
															</td>
															<td align="left">
																<table cellSpacing="0" cellPadding="3" borderColor="#cccccc" border="1" width="95%">
																	<tr>
																		<td align="center" width="15%" class="tableHeader">Description</td>
																		<td align="center" width="22%" class="tableHeader">&nbsp;&nbsp;Current</td>
																		<td align="center" width="32%" class="tableHeader">New</td>
																	</tr>
																	<tr bgColor="#ffffff">
																		<td>Font Name</td>
																		<td>
																			<asp:Label ID="lblsubtitlename" Runat="server"></asp:Label></td>
																		<td>
																			<asp:dropdownlist id="drpsubtitlefont" Runat="server" CssClass="altLong0SelectFormat" Width="150">
																				<asp:ListItem Value="">Select the Font</asp:ListItem>
																				<asp:ListItem Value="Arial">Arial</asp:ListItem>
																				<asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																				<asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																				<asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																				<asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																				<asp:ListItem Value="Georgia">Georgia</asp:ListItem>
																				<asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																				<asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																				<asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																				<asp:ListItem Value="Verdana">Verdana</asp:ListItem>
											    	    					</asp:dropdownlist>
																		</td>
																	</tr>
																	<tr bgColor="#ffffff">
																		<td>Font Size</td>
																		<td>
																			<asp:Label ID="lblsubtitlesize" Runat="server"></asp:Label>
																		<td>
																			<asp:dropdownlist id="drpsubtitlesize" Runat="server" CssClass="altLong0SelectFormat" Width="55">
																				<asp:ListItem Value="">Size</asp:ListItem>
																				<asp:ListItem Value="8">8</asp:ListItem>
																				<asp:ListItem Value="9">9</asp:ListItem>
																				<asp:ListItem Value="10">10</asp:ListItem>
																				<asp:ListItem Value="11">11</asp:ListItem>
																				<asp:ListItem Value="12">12</asp:ListItem>
																				<asp:ListItem Value="14">14</asp:ListItem>
																				<asp:ListItem Value="16">16</asp:ListItem>
																				<asp:ListItem Value="18">18</asp:ListItem>
																				<asp:ListItem Value="20">20</asp:ListItem>
																				<asp:ListItem Value="22">22</asp:ListItem>
																			</asp:dropdownlist>
																		</td>
																	</tr>
																	<tr bgColor="#ffffff">
																		<td>Font Color</td>
																		<td>
																			<asp:Label ID="lblsubtitlecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
    																		<asp:Label ID="lblsubtitlecolor1" Runat="server"></asp:Label>
																		</td>
																		<td>
																			<asp:textbox EnableViewState="True" id="txtsubtitlecolor" Runat="server" CssClass="altText"></asp:textbox>
																			<IMG title="Click to select the color" onclick="FnShowPalette(event,'txtsubtitlecolor')"
																				height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgsubtitlecolor"> <%-- Organization Css Module --%>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
										<tr>
											<td align="center" width="3%">
												<table id="tablebtncolor" width="25" align="right" border="0">
													<tr>
														<td align="center" class="altColoredBackground" height="20"><span class="numwhiteblodtext">5</span>
														</td>
													</tr>
												</table>
											</td>
											<td width="1%" height="20">&nbsp;</td>
											<td align="left" width="96%" colspan="2" height="20"><span class="subtitleblueblodtext"><b>Select 
														Button Font Property</b></span></td>
										</tr>
										<tr>
											<td height="20"></td>
											<td></td>
											<td vAlign="top" align="center">
												<table id="tablebutton" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													<tbody>
														<tr>
															<td vAlign="top" align="left" width="15%"><B></B>
															</td>
															<td align="left">
																<table cellSpacing="0" cellPadding="3" borderColor="#cccccc" border="1" width="95%">
																	<tr>
																		<td align="center" width="19%" class="tableHeader">Description</td>
																		<td align="center" width="22%" class="tableHeader">Current</td>
																		<td align="center" width="32%" class="tableHeader">New</td>
																	</tr>
																	<tr bgColor="#ffffff">
																		<td>Font Name</td>
																		<td><asp:Label ID="lblbttnfontname" Runat="server"></asp:Label></td>
																		<td>
																			<asp:dropdownlist id="dropbttnfont" Runat="server" CssClass="altLong0SelectFormat" Width="150">
																				<asp:ListItem Value="">Select the Font</asp:ListItem>
																				<asp:ListItem Value="Arial">Arial</asp:ListItem>
																				<asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																				<asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																				<asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																				<asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																				<asp:ListItem Value="Georgia">Georgia</asp:ListItem>
																				<asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																				<asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																				<asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																				<asp:ListItem Value="Verdana">Verdana</asp:ListItem>
																			</asp:dropdownlist>
																		</td>
																	</tr>
																	<tr bgColor="#ffffff">
																		<td>Font Size</td>
																		<td>
																			<asp:Label ID="lblbuttonsize" Runat="server"></asp:Label>
																		</td>
																		<td>
																			<asp:dropdownlist id="drpbuttonsize" Runat="server" CssClass="altLong0SelectFormat" Width="55">
																				<asp:ListItem Value="">Size</asp:ListItem>
																				<asp:ListItem Value="8">8</asp:ListItem>
																				<asp:ListItem Value="9">9</asp:ListItem>
																				<asp:ListItem Value="10">10</asp:ListItem>
																				<asp:ListItem Value="11">11</asp:ListItem>
																				<asp:ListItem Value="12">12</asp:ListItem>
																				<asp:ListItem Value="14">14</asp:ListItem>
																				<asp:ListItem Value="16">16</asp:ListItem>
																				<asp:ListItem Value="18">18</asp:ListItem>
																				<asp:ListItem Value="20">20</asp:ListItem>
																				<asp:ListItem Value="22">22</asp:ListItem>
																			</asp:dropdownlist>
																		</td>
																	</tr>
																	<tr bgColor="#ffffff">
																		<td>Font Color</td>
																		<td>
																			<asp:Label ID="lblbttnfontcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																			<asp:Label ID="lblbttnfontcolor1" Runat="server"></asp:Label>
																    	</td>
																		<td>
																			<asp:textbox EnableViewState="True" id="txtbuttonfontcolor" Runat="server" CssClass="altText"></asp:textbox>
																			<IMG title="Click to select the color" onclick="FnShowPalette(event,'txtbuttonfontcolor')"
																				height="23" alt="Color" src="../Image/color.jpg" width="27" name="iimgbuttonfontcolor"> <%-- Organization Css Module --%>
																		</td>
																	</tr>
																    <tr bgColor="#ffffff">
																	    <td nowrap>Font Background Color</td>
																	    <td nowrap>
																		    <asp:Label ID="lblbuttonbgcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		    <asp:Label ID="lblbuttonbgcolor1" Runat="server"></asp:Label>
																	    </td>
																		<td>
																			<asp:textbox EnableViewState="True" id="txtbuttonbgcolor" Runat="server" CssClass="altText"></asp:textbox>
																			<IMG title="Click to select the color" onclick="FnShowPalette(event,'txtbuttonbgcolor')"
																				height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgbttnbgcolor"> <%-- Organization Css Module --%>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
    												</tbody>
												</table>
											</td>
										</tr>
                                        <tr>
											<td align="center" width="3%">
												<table id="table1" width="25" align="right" border="0">
													<tr>
														<td align="center" class="altColoredBackground" height="20"><span class="numwhiteblodtext">6</span>
														</td>
													</tr>
												</table>
											</td>
											<td width="1%" height="20">&nbsp;</td>
											<td align="left" width="96%" colspan="2" height="20"><span class="subtitleblueblodtext"><b>Select 
														Table Header Property</b></span></td>
										</tr>
										<tr>
											<td height="20"></td>
											<td></td>
											<td vAlign="top" align="center">
												<table id="tbltableheader" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													<tr>
    													<td vAlign="top" align="left" width="15%"></td>
														<td align="left">
															<table cellSpacing="0" cellPadding="3" bordercolor="#cccccc" border="1" width="95%">
																<tr class="tableHeader">
																	<td ALIGN="center" width="19%" class="tableHeader">Description</td>
																	<td ALIGN="center" width="22%" class="tableHeader">Current</td>
																	<td ALIGN="center" width="32%" class="tableHeader">New</td>
																</tr>
																<tr bgColor="#ffffff">
																	<td>Fore Color</td>
									        						<td>
																		<asp:Label ID="lbltableheaderforecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		<asp:Label ID="lbltableheaderforecolor1" Runat="server"></asp:Label>
																	</td>
																	<td nowrap>
																		<asp:textbox EnableViewState="True" id="txttableheaderfontcolor" Runat="server" CssClass="altText"></asp:textbox>
																		<IMG title="Click to select the color" onclick="FnShowPalette(event,'txttableheaderfontcolor')"
																			height="23" alt="Color" src="../Image/color.jpg" width="27" name="iimgtablefontcolor"> <%-- Organization Css Module --%>
																	</td>
																</tr>
																<tr bgColor="#ffffff">
																	<td nowrap>Header Background Color</td>
																	<td>
																		<asp:Label ID="lbltableheaderbackcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		<asp:Label ID="lbltableheaderbackcolor1" Runat="server"></asp:Label>
																	</td>
																	<td>
																		<asp:textbox EnableViewState="True" id="txttableheaderbackcolor" Runat="server" CssClass="altText"></asp:textbox>
																		<IMG title="Click to select the color" onclick="FnShowPalette(event,'txttableheaderbackcolor')"
																			height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgtblheaderbgcolor"> <%-- Organization Css Module --%>
																	</td>
																</tr>
    														</table>
															</td>
														</tr>
												</table>
											</td>
										</tr>
										<tr>
											<td align="center" width="3%">
												<table id="table2" width="25" align="right" border="0">
													<tr>
														<td align="center" class="altColoredBackground" height="20"><span class="numwhiteblodtext">7</span>
														</td>
													</tr>
												</table>
											</td>
											<td width="1%" height="20">&nbsp;</td>
											<td align="left" width="96%" colspan="2" height="20"><span class="subtitleblueblodtext"><b>Select 
														Error Message Display Property</b></span></td>
										</tr>
										<tr>
											<td height="20"></td>
											<td></td>
											<td vAlign="top" align="center">
												<table id="Table3" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													<tr>
    													<td vAlign="top" align="left" width="15%"></td>
														<td align="left">
															<table cellSpacing="0" cellPadding="3" bordercolor="#cccccc" border="1" width="95%">
																<tr class="tableHeader">
																	<td ALIGN="center" width="19%" class="tableHeader">Description</td>
																	<td ALIGN="center" width="22%" class="tableHeader">Current</td>
																	<td ALIGN="center" width="32%" class="tableHeader">New</td>
																</tr>
																<tr bgColor="#ffffff">
																	<td>Fore Color</td>
									        						<td>
																		<asp:Label ID="lblerrormessageforecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		<asp:Label ID="lblerrormessageforecolor1" Runat="server"></asp:Label>
																	</td>
																	<td nowrap>
																		<asp:textbox EnableViewState="True" id="txterrormessagefontcolor" Runat="server" CssClass="altText"></asp:textbox>
																		<IMG title="Click to select the color" onclick="FnShowPalette(event,'txterrormessagefontcolor')"
																			height="23" alt="Color" src="../Image/color.jpg" width="27" name="iimgerrormessagefontcolor"> <%-- Organization Css Module --%>
																	</td>
																</tr>
																<tr bgColor="#ffffff">
																	<td nowrap>Background Color</td>
																	<td>
																		<asp:Label ID="lblerrormessagebackcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		<asp:Label ID="lblerrormessagebackcolor1" Runat="server"></asp:Label>
																	</td>
																	<td>
																		<asp:textbox EnableViewState="True" id="txterrormessagebackcolor" Runat="server" CssClass="altText"></asp:textbox>
																		<IMG title="Click to select the color" onclick="FnShowPalette(event,'txterrormessagebackcolor')"
																			height="23" alt="Color" src="../Image/color.jpg" width="27" name="imgerrormessagebgcolor">  <%-- Organization Css Module --%>
																	</td>
																</tr>

																<tr bgColor="#ffffff">
																	<td>Font Size</td>
																		<td>
																			<asp:Label ID="lblerrormessagefontsize" Runat="server"></asp:Label>
																		</td>
																		<td>
																			<asp:dropdownlist id="drperrormessagefontsize" Runat="server" CssClass="altLong0SelectFormat" Width="55">
																				<asp:ListItem Value="">Size</asp:ListItem>
																				<asp:ListItem Value="8">8</asp:ListItem>
																				<asp:ListItem Value="9">9</asp:ListItem>
																				<asp:ListItem Value="10">10</asp:ListItem>
																				<asp:ListItem Value="11">11</asp:ListItem>
																				<asp:ListItem Value="12">12</asp:ListItem>
																				<asp:ListItem Value="14">14</asp:ListItem>
																				<asp:ListItem Value="16">16</asp:ListItem>
																				<asp:ListItem Value="18">18</asp:ListItem>
																				<asp:ListItem Value="20">20</asp:ListItem>
																				<asp:ListItem Value="22">22</asp:ListItem>
																			</asp:dropdownlist>
																		</td>
																</tr>  													</table>
															</td>
														</tr>
												</table>
											</td>
										</tr>
<tr>
											<td align="center" width="3%">
												<table id="table7" width="25" align="right" border="0">
													<tr>
														<td align="center" class="altColoredBackground" height="20"><span class="numwhiteblodtext">7</span>
														</td>
													</tr>
												</table>
											</td>
											<td width="1%" height="20">&nbsp;</td>
											<td align="left" width="96%" colspan="2" height="20"><span class="subtitleblueblodtext"><b>Select 
														Input Text Property</b></span></td>
										</tr>
										<tr>
											<td height="20"></td>
											<td></td>
											<td vAlign="top" align="center">
												<table id="tblInputTextBox" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													<tr>
    													<td vAlign="top" align="left" width="15%"></td>
														<td align="left">
															<table cellSpacing="0" cellPadding="3" bordercolor="#cccccc" border="1" width="95%">
																<tr class="tableHeader">
																	<td ALIGN="center" width="19%" class="tableHeader">Description</td>
																	<td ALIGN="center" width="22%" class="tableHeader">Current</td>
																	<td ALIGN="center" width="32%" class="tableHeader">New</td>
																</tr>
																<tr bgColor="#ffffff">
																	<td>Fore Color</td>
									        						<td>
																		<asp:Label ID="lblinputtextforecolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		<asp:Label ID="lblinputtextforecolor1" Runat="server"></asp:Label>
																	</td>
																	<td nowrap>
																		<asp:textbox EnableViewState="True" id="txtinputtextfontcolor" Runat="server" CssClass="altText"></asp:textbox>
																		<IMG title="Click to select the color" onclick="FnShowPalette(event,'txtinputtextfontcolor')"
																			height="23" alt="Color" src="../Image/color.jpg" width="27" name="imginputtextfontcolor"> <%-- Organization Css Module --%>
																	</td>
																</tr>
																<tr bgColor="#ffffff">
																	<td nowrap>Background Color</td>
																	<td>
																		<asp:Label ID="lblinputtextbackcolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		<asp:Label ID="lblinputtextbackcolor1" Runat="server"></asp:Label>
																	</td>
																	<td>
																		<asp:textbox EnableViewState="True" id="txtinputtextbackcolor" Runat="server" CssClass="altText"></asp:textbox>
																		<IMG title="Click to select the color" onclick="FnShowPalette(event,'txtinputtextbackcolor')"
																			height="23" alt="Color" src="../Image/color.jpg" width="27" name="imginputtextbgcolor"> <%-- Organization Css Module --%>
																	</td>
																</tr>
																<tr bgColor="#ffffff">
																	<td nowrap>Border Color</td>
																	<td>
																		<asp:Label ID="lblinputtextbordercolor" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>
																		<asp:Label ID="lblinputtextbordercolor1" Runat="server"></asp:Label>
																	</td>
																	<td>
																		<asp:textbox EnableViewState="True" id="txtinputtextbordercolor" Runat="server" CssClass="altText"></asp:textbox>
																		<IMG title="Click to select the color" onclick="FnShowPalette(event,'txtinputtextbordercolor')"
																			height="23" alt="Color" src="../Image/color.jpg" width="27" name="imginputtextbordercolor"> <%-- Organization Css Module --%>
																	</td>
																</tr>
																<tr bgColor="#ffffff">
																	<td>Font Size</td>
																		<td>
																			<asp:Label ID="lblinputtextfontsize" Runat="server"></asp:Label>
																		</td>
																		<td>
																			<asp:dropdownlist id="drpinputtextfontsize" Runat="server" CssClass="altLong0SelectFormat" Width="55">
																				<asp:ListItem Value="">Size</asp:ListItem>
																				<asp:ListItem Value="8">8</asp:ListItem>
																				<asp:ListItem Value="9">9</asp:ListItem>
																				<asp:ListItem Value="10">10</asp:ListItem>
																				<asp:ListItem Value="11">11</asp:ListItem>
																				<asp:ListItem Value="12">12</asp:ListItem>
																				<asp:ListItem Value="14">14</asp:ListItem>
																				<asp:ListItem Value="16">16</asp:ListItem>
																				<asp:ListItem Value="18">18</asp:ListItem>
																				<asp:ListItem Value="20">20</asp:ListItem>
																				<asp:ListItem Value="22">22</asp:ListItem>
																			</asp:dropdownlist>
																		</td>
																</tr>
                                                                <tr>
																		<td>Font Name</td>
																		<td>
																			<asp:Label ID="lblinputtextfontname" Runat="server" ></asp:Label>
																		</td>
																	    <td>
																		    <asp:dropdownlist id="drpinputtextfontname" Runat="server" CssClass="altLong0SelectFormat" Width="150">
																			    <asp:ListItem Value="">Select the Font</asp:ListItem>
																			    <asp:ListItem Value="Arial">Arial</asp:ListItem>
																			    <asp:ListItem Value="Bookman Old Style">Bookman Old Style</asp:ListItem>
																			    <asp:ListItem Value="Century Gothic">Century Gothic</asp:ListItem>
																			    <asp:ListItem Value="Comic Sans Ms">Comic Sans Ms</asp:ListItem>
																			    <asp:ListItem Value="Courier New">Courier New</asp:ListItem>
																			    <asp:ListItem Value="Georgia">Georgia</asp:ListItem>
																			    <asp:ListItem Value="Microsoft Sans Serif">Microsoft Sans Serif</asp:ListItem>
																			    <asp:ListItem Value="Tahomo">Tahomo</asp:ListItem>
																			    <asp:ListItem Value="Times New Roman">Times New Roman</asp:ListItem>
																			    <asp:ListItem Value="Verdana">Verdana</asp:ListItem>
																		    </asp:dropdownlist>
																	    </td>
																	</tr>											
															        </table>
															</td>
														</tr>
												</table>
											</td>
										</tr>
										<TR>
											<TD align="center" width="3%">
												<TABLE id="tableelements" width="25" align="right" border="0">
													<TBODY>
														<TR>
															<TD align="center" class="altColoredBackground" height="20"><SPAN class="numwhiteblodtext">8</SPAN>
															</TD>
														</TR>
													</TBODY>
												</TABLE>
											</TD>
											<TD width="1%" height="20">&nbsp;</TD>
											<TD align="left" width="96%" colSpan="2" height="20">
											<SPAN class="subtitleblueblodtext">Customize additional UI Elements</SPAN>
											</TD>
										</TR>
										<tr>
											<td height="20"></td>
											<td></td>
											<td vAlign="top" align="center">
												<table id="tablebanner" cellSpacing="0" cellPadding="2" width="100%" align="left" border="0">
													<tbody>
														<tr>
															<td vAlign="top" align="left" width="15%"><B>Banner</B>
															</td>
															<td align="left"><INPUT class="altText" id="Bannerfile" type="file" size="27" name="Banner" runat="server">
															</td>
														</tr>
														<tr>
															<td vAlign="top" align="left"><B>Company Logo</B>
															</td>
															<td align="left"><INPUT class="altText" id="companylogo" type="file" size="27" name="CompanyLogo" runat="server">
															</td>
														</tr>
														<tr>
															<td colSpan="2">&nbsp;</td>
														</tr>
														<tr>
															<td vAlign="top" align="left"><B>Menu Options</B>
															</td>
															<td align="left">
																<table cellSpacing="0" cellPadding="3" bgColor="#cccccc" border="0" width="95%">
																	<tr class="tableHeader">
																		<td align="center" width="29%" rowspan="2" nowrap class="tableHeader">Description</td>
																		<td align="center" width="37%" colspan="2" nowrap class="tableHeader">Menu Bar</td>
																		<td align="center" width="35%" colspan="2" nowrap class="tableHeader">Dropdown List</td>
																	</tr>
																	<tr class="tableHeader">
																		<td align="center" class="tableHeader">Current</td>
																		<td align="center" class="tableHeader">New</td>
																		<td align="center" class="tableHeader">Current</td>
																		<td align="center" class="tableHeader">New</td>
																	</tr>
																	<tr>
																		<td style="PADDING-RIGHT: 0px; PADDING-LEFT: 0px; PADDING-BOTTOM: 0px; PADDING-TOP: 0px" colSpan="5" bgcolor="#ffffff" >
																			<div id="md" style="BORDER-RIGHT: #cccccc 0px solid; BORDER-TOP: #cccccc 1px solid; OVERFLOW: auto; BORDER-LEFT: #cccccc 0px solid; WIDTH: 100%; BORDER-BOTTOM: #cccccc 0px solid; POSITION: relative; HEIGHT: 290px">
																				<table id="Table10" cellSpacing="1" cellPadding="3" bgColor="#cccccc" border="0" width="100%">
																					<tr bgColor="#ffffff">
																						<td>Mouse Off Font</td>
																						<td align="left"><asp:Label ID="lblm_offfont" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_offfont1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="m_mouseofffont" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'m_mouseofffont')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image3"><%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																						<td align="left"><asp:Label ID="lbld_offfont" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_offfont1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="d_mouseofffont" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_mouseofffont')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image4">  <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																					<tr bgColor="#ffffff">
																						<td>Mouse Off Background</td>
																						<td align="left"><asp:Label ID="lblm_Offback" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_Offback1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="m_mouseOffback" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td>
																										<IMG title="Click to select the color" onclick="FnShowPalette(event,'m_mouseOffback')" height="23" alt="Color" src="Organizations/Original/Image/color.jpg" width="20" name="image5"> <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																						<td align="left"><asp:Label ID="lbld_Offback" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_Offback1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="d_mouseOffback" Runat="server" CssClass="altText" Width="70px" ></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_mouseOffback')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image6"> <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																					<tr bgColor="#ffffff">
																						<td>Mouse On Font</td>
																						<td align="left"><asp:Label ID="lblm_Onfont" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_Onfont1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="m_mouseOnfont" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'m_mouseOnfont')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image7"> <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																						<td align="left"><asp:Label ID="lbld_Onfont" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_Onfont1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="d_mouseOnfont" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_mouseOnfont')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image8"> <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																					<tr bgColor="#ffffff">
																						<td>Mouse On Background</td>
																						<td align="left"><asp:Label ID="lblm_Onback" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_Onback1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="m_mouseOnback" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'m_mouseOnback')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image9"> <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																						<td align="left"><asp:Label ID="lbld_Onback" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_Onback1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="d_mouseOnback" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_mouseOnback')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image10"> <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																					<tr bgColor="#ffffff">
																						<td>Menu Border</td>
																						<td align="left"><asp:Label ID="lblm_mborder" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_mborder1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="m_menuborder" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'m_menuborder')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image11"> <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																						<td align="left"><asp:Label ID="lbld_mborder" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_mborder1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="d_menuborder" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_menuborder')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image12"> <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																					<tr bgColor="#ffffff">
																						<td>Menu Header Font Color</td>
																						<td align="left"><asp:Label ID="lblm_hfontcol" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_hfontcol1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="m_headfontcol" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'m_headfontcol')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image13">  <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																						<td align="left"><asp:Label ID="lbld_hfontcol" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_hfontcol1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="d_headfontcol" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_headfontcol')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image14"> <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																					<tr bgColor="#ffffff">
																						<td>Menu Header Background Color</td>
																						<td align="left"><asp:Label ID="lblm_hbackcol" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lblm_hbackcol1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="m_headbackcol" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'m_headbackcol')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image15"> <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																						<td align="left"><asp:Label ID="lbld_hbackcol" Runat="server" BorderWidth="1px" BorderColor="#000000" BorderStyle="Solid"></asp:Label>&nbsp;<asp:Label ID="lbld_hbackcol1" Runat="server"></asp:Label></td>
																						<td>
																							<table>
																								<tr>
																									<td><asp:textbox EnableViewState="True" id="d_headbackcol" Runat="server" CssClass="altText" Width="70px"></asp:textbox></td>
																									<td><IMG title="Click to select the color" onclick="FnShowPalette(event,'d_headbackcol')"
																											height="23" alt="Color" src="../Image/color.jpg" width="20" name="image16"> <%-- Organization Css Module --%>
																									</td>
																								</tr>
																							</table>
																						</td>
																					</tr>
																				</table>
																			</div>
																		</td>
																	</tr>
																</table>
															</td>
														</tr>
													</tbody>
												</table>
											</td>
										</tr>
									</TBODY>
								</TABLE>
								<BR>
								<TABLE id="Table17" cellSpacing="0" cellPadding="0" width="70%" border="0">
									<TBODY>
										<TR>
											<TD align="center">
												<asp:button id="btnOriginal" Runat="server" CssClass="altShort0BlueButtonFormat" Text="Default Configuration"></asp:button>&nbsp;&nbsp;
											</TD>
											<TD align="center">
												<asp:button id="btnPreview" Runat="server" CssClass="altShort0BlueButtonFormat" Text="Preview"></asp:button>&nbsp;&nbsp;
											</TD>
											<TD align="center">
												<input class="altShort0BlueButtonFormat" onclick="FnCancel()" type="button" value="Cancel"
													name="btnCancel">&nbsp;&nbsp;
											</TD>
											<TD align="center">
												<asp:button id="btnSave" Runat="server" CssClass="altShort0BlueButtonFormat" Text="Submit"></asp:button>&nbsp;&nbsp;
											</TD>
										</TR>
									</TBODY>
								</TABLE>
							</CENTER>
						</FORM>
					</TD>
				</TR>
<tr><td height=20></td></tr>
			</TBODY>
		</TABLE>
		<script language="javascript">
			function Browser() 
			{
			var ua, s, i;
			this.isIE    = false;
			this.isNS    = false;
			this.version = null;

			ua = navigator.userAgent;

			s = "MSIE";
			if ((i = ua.indexOf(s)) >= 0) {
				this.isIE = true;
				this.version = parseFloat(ua.substr(i + s.length));
				return;
			}

			s = "Netscape6/";
			if ((i = ua.indexOf(s)) >= 0) {
				this.isNS = true;
				this.version = parseFloat(ua.substr(i + s.length));
				return;
			}

			// Treat any other "Gecko" browser as NS 6.1.

			s = "Gecko";
			if ((i = ua.indexOf(s)) >= 0) {
				this.isNS = true;
				this.version = 6.1;
				return;
			}
			}

			var browser = new Browser();
			FnHideShow();
			//Div Height Patch for Netscape 
			if (browser.isNS) 
			{
				var divmd = document.getElementById("md");
				divmd.style.height = "260px"; 		
			}
			//Div Height Patch for Netscape 


			function FnShowPalette()
			{
				var args = FnShowPalette.arguments;
				var x,y,e;
				e= args[0]; 
				if (!e) e = window.event;
				if (browser.isIE) 
				{
					x = e.clientX + document.documentElement.scrollLeft + document.body.scrollLeft;
					y = e.clientY + document.documentElement.scrollTop + document.body.scrollTop;
				}
				if (browser.isNS) 
				{
					x = e.clientX + window.scrollX;
					y = e.clientY + window.scrollY;
				}
				show_RGBPalette(args[1],x,y);
			}

			function FnHideShow()
			{
				if(document.frmSample.ColorSystem[0].checked == true)
				{
					document.frmSample.NamedCol.disabled = false;
				}
				else
				{
					document.frmSample.NamedCol.disabled = true;
				}
				
				if(document.frmSample.ColorSystem[1].checked == true)
				{
					document.frmSample.TxtRGBColor.disabled = false;
					document.frmSample.rgbimg.onclick = function(e)
					{	FnShowPalette(e,'TxtRGBColor');	}
				}
				else
				{
					document.frmSample.TxtRGBColor.disabled = true;
					document.frmSample.rgbimg.onclick = function()
					{}
				}
			}

			function FnValidateForm()
			{
				if(document.frmSample.ColorSystem[0].checked == true)
				{
					if(document.frmSample.NamedCol.value == "0")
					{
						alert("Please select the named color.");
						return false;
					}
				}
				
				if(document.frmSample.ColorSystem[1].checked == true)
				{
					if(document.frmSample.TxtRGBColor.value == "")
					{
						alert("Please select the RGB color.");
						return false;
					}
				}
				return true;	
			}

			function FnConfirm()
			{
				if(confirm("All existing settings will be lost"))
				{
					return true;
				}
				else
				{
					
					return false;
				}
			}
			
			function FnPreview()
			{
				window.open('Preview.aspx?styletype=P','Preview','resizable=yes,scrollbars=yes,toolbar=no, width=900,height=600')
			}

			function FnCancel()
			{
				//window.location.replace('genlogin.aspx');
				window.location.replace('SuperAdministrator.aspx');
			}
			function FnOriginal()
			{
				document.frmSample.submit();
			}

		</script>
	</BODY>

</HTML>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->