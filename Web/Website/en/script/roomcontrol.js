/*ZD 100147 Start*/
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
/*ZD 100147 ZD 100886 End*/
function room_controls_para_prompt(promptpicture, prompttitle, temprature, shade, lights) 
{
	promptbox = document.createElement('div'); 
	promptbox.setAttribute ('id' , 'prompt');
	document.getElementsByTagName('body')[0].appendChild(promptbox);
	promptbox = eval("document.getElementById('prompt').style");

	promptbox.position = 'absolute';
	promptbox.top = mousedownY - 60;
	promptbox.left = mousedownX - 320;
	promptbox.width = 300;
	promptbox.border = 'outset 1 #bbbbbb';
//	promptbox.style.zIndex=1000;
	
	
	lightsary=lights.split("||");
	lightlen=lightsary.length-1;
	var lightid=new Array(lightlen);
	var lightname=new Array(lightlen);
	var lightswitch=new Array(lightlen);
	for (var i=0; i<lightlen; i++)
	{
		lightary=lightsary[i].split("&&");
		lightid[i]=lightary[0];
		lightname[i]=lightary[1];
		lightswitch[i]=lightary[2];
	}

	
	m = "<table cellspacing='0' cellpadding='0' border='0' width='100%'><tr valign='middle'><td width='22' height='22' style='text-indent:2;' class='titlebar'><img src='" + promptpicture + "' height='18' width='18' alt='Prompt'></td><td class='titlebar'>" + prompttitle + "</td></tr></table>" //ZD 100419
	m += "<table cellspacing='0' cellpadding='0' border='0' width='100%' class='promptbox'>";
	m += "  <tr>";
	m += "    <td align='right'><label>Shade On</label></td>";
	m += "    <td align='left'><input type='checkbox' name='shd' id='shd' value='1'" + ( (parseInt(shade)==1) ? " checked" : "") + "></td>";
	m += "    <td width='1'></td>";
	m += "    <td align='right'><label>Temperature</label></td>";
	m += "    <td align='left'>";
	m += "      <table><tr>";
	m += "        <td valign='middle'><input type='text' name='tmpr' id='tmpr' maxlength='3' value='" + temprature + "' style='width: 30pt'></td>";
	m += "        <td><table><tr><td><img src='image/up.gif' height='9' width='9' onMouseDown='starttemp(1);' onMouseUp='endtemp();' alt='Up'></td></tr><tr><td><img src='image/down.gif' height='9' width='9' onMouseDown='starttemp(-1);' onMouseUp='endtemp();' alt='down'></td></tr></table></td>";//ZD 100419
	m += "      </tr></table>";
	m += "    </td>";
	m += "  </tr>";
	m += "  <tr>";
	m += "    <td align='right'><label>Light On</label></td>";
	m += "    <td align='left' colspan='4' valign='top'>";
	m += "          <table>";
	
	j=0;
	for (var i=0; i< parseInt((lightlen+2)/3); i++) {
	
		m += "            <tr>";
		m += "              <td align='right'>" + lightname[j] + "</td>";
		m += "              <td align='left'><input type='checkbox' name='lght"+j+"' id='lght"+j+"' value='1'" + ( (parseInt(lightswitch[j])==1) ? " checked" : "") + "></td>";
		j++;
		if (j < lightlen) {
			m += "              <td align='right'>" + lightname[j] + "</td>";
			m += "              <td align='left'><input type='checkbox' name='lght"+j+"' id='lght"+j+"' value='1'" + ( (parseInt(lightswitch[j])==1) ? " checked" : "") + "></td>";
		}
		j++;
		if (j < lightlen) {
			m += "              <td align='right'>" + lightname[j] + "</td>";
			m += "              <td align='left'><input type='checkbox' name='lght"+j+"' id='lght"+j+"' value='1'" + ( (parseInt(lightswitch[j])==1) ? " checked" : "") + "></td>";
		}
		m += "            </tr>";
		j++;
	}
	
	m += "          </table>";
	m += "    </td>";
	m += "  </tr>"

	m += "  <tr><td align='right' colspan=5 height='1'><hr align=center width='80%'></td></tr>"

	m += "  <tr><td align='right' colspan=5>"
	m += "    <input type='button' class='prompt' value='Submit' onMouseOver='this.style.border=\"1 outset #dddddd\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='saveInput(\"" + lights + "\");'>"
	m += "    <input type='button' class='prompt' value='Cancel' onMouseOver='this.style.border=\"1 outset transparent\"' onMouseOut='this.style.border=\"1 solid transparent\"' onClick='cancelthis();'>"
	m += "  </td></tr>"
	m += "</table>";

	document.getElementById('prompt').innerHTML = m;
} 


function saveInput(lights) 
{
	var newlght = "";

	lightsary=lights.split("||");
	lightlen=lightsary.length-1;

	for (var i=0; i<lightlen; i++)
	{
		lightary=lightsary[i].split("&&");
		newlght += lightary[0] + "&&" + lightary[1] + "&&" + ( (document.getElementById("lght" + i).checked) ? 1 : 0) + "||";
	}
	
	post_rmcontrol (document.getElementById("tmpr").value, document.getElementById("shd").checked ? 1 : 0, newlght);

	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
} 


function cancelthis()
{
	document.getElementsByTagName("body")[0].removeChild(document.getElementById("prompt"));
}
