<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100866 End--%>
<%@ Page Language="C#" Inherits="ns_Bridges.BridgeDetails" Buffer="true" %>
<%@ Register Assembly="MetaBuilders.WebControls.ComboBox" Namespace="MetaBuilders.WebControls" TagPrefix="mbcbb" %> <%--FB 1938--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">  <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!--Window Dressing-->
<% if (Request.QueryString["hf"] != null && Request.QueryString["hf"].ToString().Equals("1"))
   {
%>
        <!-- #INCLUDE FILE="inc/maintop4.aspx" --> 
<% }
else
    {
%>
        <!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<%
    }
%>

<script type="text/javascript">

  var servertoday = new Date(); //FB 1938

</script>
<%--ZD 101028 start--%>
<style type="text/css">
    a img { outline:none;
    text-decoration:none;
    border:0;
    }
</style> 
<%--ZD 101028 End--%>
<script language="javascript">
 //ZD 100604 start
        var img = new Image();
        img.src = "../en/image/wait1.gif";
//ZD 100604 End
//FB 1766 - Start
function disableservname()
{
    var enable = document.getElementById("chkEnableIVR");
    if((enable != null) && (enable.checked != true) )
    {
      document.getElementById("reqServName").innerHTML = "";
      document.getElementById("txtIVRName").value = "";
      document.getElementById("txtIVRName").disabled = true;
    }
    else
    {
      document.getElementById("txtIVRName").disabled = false;
    }
}
function RequireIVRName()
{   
    if(document.getElementById("chkEnableCDR") != null) //FB 2660
        CDRDays();
    var txtIVRName = document.getElementById("txtIVRName");
    var enable = document.getElementById("chkEnableIVR");
    if (enable != null)
    {    
        if (enable.checked && txtIVRName.value == '')
        {
        document.getElementById("reqServName").style.display = 'block';
        document.getElementById("reqServName").innerHTML = ReqIVRServiceName;
        document.getElementById("txtIVRName").focus();
        return false;
        }
    }
    if (!Page_ClientValidate())//FB 2714
               return Page_IsValid;
}
//FB 1766 - End
function DeleteApprover(id)
{
    document.getElementById("hdnApprover" + id).value="";
    document.getElementById("txtApprover" + id).value="";
    document.getElementById("hdnApprover" + id + "_1").value="";
    document.getElementById("txtApprover" + id + "_1").value="";
}

function SavePassword()
{
    for(i=1;i<5;i++)
    {
        document.getElementById("txtApprover" + i).value = document.getElementById("txtApprover" + i + "_1").value;
        document.getElementById("hdnApprover" + i).value = document.getElementById("hdnApprover" + i + "_1").value;
    }
    if(document.getElementById("txtPassword1") != null)//ZD 100040
        document.getElementById("hdnTempPwd").value = document.getElementById("txtPassword1").value; // ZD 100369 508 Issues
}

function ShowISDN(obj)
{
    if (obj.checked)
        document.getElementById("trISDN").style.display="";
    else
        document.getElementById("trISDN").style.display="none";
}



function getYourOwnEmailList (i)	// -1, 0, 1, 2
{
	//url = "dispatcher/conferencedispatcher.asp?frm=approverNET&frmname=frmBridgesetting&no=" + i + "&cmd=GetEmailList&emailListPage=1&wintype=pop"; //Login Management	
	url = "../en/emaillist2main.aspx?t=e&frm=approverNET&fn=frmBridgesetting&n=" + i; //Login Management
	
	if (!window.winrtc) {	// has not yet been defined
		winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no"); //FB 2735
		winrtc.focus();
	} else // has been defined
	    if (!winrtc.closed) {     // still open
	    	winrtc.close();
	        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2735
			winrtc.focus();
		} else {
	        winrtc = window.open(url, "", "width=920,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");//FB 2735
	        winrtc.focus();
		}
}

//ZD 100369_MCU
  function getYourOwnAddressList() {
        
        var url = "../en/emaillist2main.aspx?t=e&frm=party2NET&chk=frmbridge&fn=Setup&n=";

        if (!window.winrtc) {
            winrtc = window.open(url, "", "width=920,height=470px,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else if (!winrtc.closed) {
            winrtc.close();
            winrtc = window.open(url, "", "width=920,height=470px ,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        } else {
            winrtc = window.open(url, "", "width=920ss,height=470,top=0,left=0,resizable=yes,scrollbars=yes,status=no");
            winrtc.focus();
        }
    }
    
    function ClearAllSelection()
    {
        document.getElementById("txtMultipleAssistant").value = "";
    }
    
    //ZD 100369 End

//FB 1937 - Start
function ShowHideRow(frm)
{
    var trow1 = document.getElementById("trMPIServices");
    var trow2 = document.getElementById("trIPServices");
    var trow3 = document.getElementById("trMCUCards");
    var trow4 = document.getElementById("trISDN1");
    var trow5 = document.getElementById("trE164Services"); //FB 2636
    var chkExpand = document.getElementById("chkExpandCollapse")
    var frmCheck, frmVal
    frmVal = 0;
    if(frm)
        frmVal = frm;
    
    if(chkExpand)
    {
        if(frmVal == 1)
            chkExpand.checked = true;
        
        frmCheck = chkExpand.checked;
        
        if (frmCheck == true)
        {   
            if(trow1 != null)
                trow1.style.display = "none";
            if(trow2 != null)
                trow2.style.display = "none";
            if(trow3 != null)
                trow3.style.display = "none";
            if(trow4 != null)
                trow4.style.display = "none";      
           if(trow5 != null) //FB 2636
                trow5.style.display = "none";                                     
        }
        else
        {
            if(trow1 != null)
                trow1.style.display = "block";
            if(trow2 != null)
                trow2.style.display = "block";
            if(trow3 != null)
                trow3.style.display = "block";
            if(trow4 != null)
                trow4.style.display = "block"; 
            if(trow5 != null) //FB 2636
                trow5.style.display = "block";   
        }
    }
    
    if(frmVal == 1)
        return true;
}


function fnShowHide(arg)
{
    var varg
    
    if(arg == 2)
    {
        varg = 2;
        arg = 1;
    }
    
    if (arg == '1')    
      document.getElementById("UsageReportDiv").style.display = 'block';
    else if (arg == '0')
    {
      document.getElementById("UsageReportDiv").style.display = 'none';
      return false;
    }
    
    if(varg == 2)
        ShowHideRow(1);
    
    return true; 
}

function fnCheck()
{
    var rptTime = document.getElementById("ReportTime_Text");
    if(rptTime)
    {
        if(rptTime.value == "")
        {
            document.getElementById("reqStartTime").style.display = 'block';
            return false;
        }
        else if('<%=Session["timeFormat"]%>' == "0")
        {
            if(rptTime.value.search(/[0-2][0-9]:[0-5][0-9]/)==-1 ) 
            {
                document.getElementById("regStartTime").style.display = 'block';
                rptTime.focus();
                return false;
            }
        }
        else if('<%=Session["timeFormat"]%>' == "2")//FB 2588
        {
            if(rptTime.value.search(/[0-2][0-9][0-5][0-9][Z|z]/)==-1 ) 
            {
                document.getElementById("regStartTime").style.display = 'block';
                rptTime.focus();
                return false;
            }
        }
        else if (rptTime.value.search(/[0|1][0-9]:[0-5][0-9] ?[a|A|p|P][M|m]/)==-1)
        {
            document.getElementById("regStartTime").style.display = 'block';
            rptTime.focus();
            return false;
        }   
    }
    
    var rptDate = document.getElementById("ReportDate");
    if(rptDate)
    {
        if(rptDate.value == "")
        {
            document.getElementById("reqStartData").style.display = 'block';
            return false;
        }
        else if (rptDate.value.search(/(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d/)==-1) 
        {
            document.getElementById("regStartDate").style.display = 'block';
            rptTime.focus();
            return false;
        }
    }
    

    return true;
}  
//FB 1937 - End

    function fnCancel() //FB 2565
	{
	    DataLoading(1); // ZD 100176
		window.location.replace('ManageBridge.aspx');  //CSS Project
	}


//FB 2660 Starts
function CDRDays() {
        var enableCDR=document.getElementById("chkEnableCDR");

        if ((enableCDR != null) && enableCDR.checked) {//FB 2714
            document.getElementById("spCDREvent").style.display = 'block';
            document.getElementById("divtxtCDR").style.display = 'block';
            //FB 2714 Start
            ValidatorEnable(document.getElementById("regDeleteCBR"), true);
            document.getElementById("regDeleteCBR").enabled = "true";
            //FB 2714 End
        }
        else {
            //FB 2714 Start
            ValidatorEnable(document.getElementById("regDeleteCBR"), false);
            document.getElementById("regDeleteCBR").enabled = "false";
            //FB 2714 End
            document.getElementById("spCDREvent").style.display = 'none';
            document.getElementById("divtxtCDR").style.display = 'none';
        }
    }
//FB 2660 Ends

//FB 2714 Starts
    function isNumberKey(evt)
      {
         var charCode = (evt.which) ? evt.which : event.keyCode
         if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

         return true;
      }
//FB 2714 Ends
//FB 3054 Starts
function PasswordChange(par, par1) 
{
    document.getElementById("hdnPasschange").value = true;
    document.getElementById("confPassword").value = false;
    if (par == 1)
        document.getElementById("hdnPW1Visit").value = true;
    else if (par == 9) { //ZD 103550
        document.getElementById("hdnBJNAppKey1").value = true;
        if (par1 == 1)
            document.getElementById("hdnBJNAppKeyVisit1").value = true;

    }
    else if (par == 10) {

        document.getElementById("hdnBJNAppSecret1").value = true;
        if (par1 == 1)
            document.getElementById("hdnBJNAppSecretVisit1").value = true;
    }
    else
        document.getElementById("hdnPW2Visit").value = true;
}  
function DMAPasswordChange(par) 
{
    document.getElementById("hdnDMSPW").value = true;
    if (par == 1)
        document.getElementById("hdnDMA1Visit").value = true;
    else
        document.getElementById("hdnDMA2Visit").value = true;
} 
 function fnTextFocus(xid,par) {
 
          // ZD 100263 Starts
    var obj1 = document.getElementById("txtDMAPassword");
    var obj2 = document.getElementById("txtDMARetypePwd");
    var obj3 = document.getElementById("txtPassword1");
    var obj4 = document.getElementById("txtPassword2");
    
    
    var obj = document.getElementById(xid);
    if (par == 1) {
     // ZD 100263 Starts
    if (obj3.value == "" && obj4.value == "") {
        document.getElementById("txtPassword1").style.backgroundImage = "";
        document.getElementById("txtPassword2").style.backgroundImage = "";
        document.getElementById("txtPassword1").value = "";
        document.getElementById("txtPassword2").value = "";
    }
    return false;
    // ZD 100263 Ends
        if(document.getElementById("hdnPW2Visit") != null)
        {
            if(document.getElementById("hdnPW2Visit").value == "false")
            { 
            document.getElementById("txtPassword1").value = ""; 
            document.getElementById("txtPassword2").value = "";
            }
            else
            {
            document.getElementById("txtPassword1").value = "";
            }
        }
        else
        {
            document.getElementById("txtPassword1").value = ""; 
            document.getElementById("txtPassword2").value = "";
        }
    }
    else if (par == 2)
    {
     // ZD 100263 Starts
    if (obj3.value == "" && obj4.value == "") {
        document.getElementById("txtPassword1").style.backgroundImage = "";
        document.getElementById("txtPassword2").style.backgroundImage = "";
        document.getElementById("txtPassword1").value = "";
        document.getElementById("txtPassword2").value = "";
    }
    return false;
    // ZD 100263 Ends
        if(document.getElementById("hdnPW1Visit") != null)
        {
            if(document.getElementById("hdnPW1Visit").value == "false")
            {
                document.getElementById("txtPassword1").value = "";
                document.getElementById("txtPassword2").value = "";
            }
            else
            {
                document.getElementById("txtPassword2").value = "";
            }
        }
        else
        {
            document.getElementById("txtPassword1").value = ""; 
            document.getElementById("txtPassword2").value = "";
        }

    }
    else if(par ==3)
    {
if (obj1.value == "" && obj2.value == "") {
        document.getElementById("txtDMAPassword").style.backgroundImage = "";
        document.getElementById("txtDMARetypePwd").style.backgroundImage = "";
        document.getElementById("txtDMAPassword").value = "";
        document.getElementById("txtDMARetypePwd").value = "";
    }
    return false;
        if(document.getElementById("hdnDMA2Visit") != null)
        {
            if(document.getElementById("hdnDMA2Visit").value == "false")
            { 
            document.getElementById("txtDMAPassword").value = ""; 
            document.getElementById("txtDMARetypePwd").value = "";
            }
            else
            {
            document.getElementById("txtDMAPassword").value = "";
            }
        }
        else
        {
            document.getElementById("txtDMAPassword").value = ""; 
            document.getElementById("txtDMARetypePwd").value = "";
        }
    }//ZD 103550
    else if (par == 9) {
        var obj1 = document.getElementById("hdnBJNAppKey");
        if (obj1.value == "") {
            document.getElementById("txtBJNAppKey").style.backgroundImage = "";
            document.getElementById("txtBJNAppKey").value = "";
        }
        return false;
    }
    else if (par == 10) {
        var obj1 = document.getElementById("hdnBJNAppSecret");
        if (obj1.value == "") {
            document.getElementById("txtBJNAppSecret").style.backgroundImage = "";
            document.getElementById("txtBJNAppSecret").value = "";

        }
        return false;
    }
    else
    {if (obj1.value == "" && obj2.value == "") {
        document.getElementById("txtDMAPassword").style.backgroundImage = "";
        document.getElementById("txtDMARetypePwd").style.backgroundImage = "";
        document.getElementById("txtDMAPassword").value = "";
        document.getElementById("txtDMARetypePwd").value = "";
    }
    return false;
        if(document.getElementById("hdnDMA1Visit") != null)
        {
            if(document.getElementById("hdnDMA1Visit").value == "false")
            { 
            document.getElementById("txtDMAPassword").value = ""; 
            document.getElementById("txtDMARetypePwd").value = "";
            }
            else
            {
            document.getElementById("txtDMARetypePwd").value = "";
            }
        }
        else
        {
            document.getElementById("txtDMAPassword").value = ""; 
            document.getElementById("txtDMARetypePwd").value = "";
        }
    }
        
        
        
        if(document.getElementById("cmpValPassword1")!= null)
        {
            ValidatorEnable(document.getElementById('cmpValPassword1'), false);
            ValidatorEnable(document.getElementById('cmpValPassword1'), true);
        }
         if(document.getElementById("cmp2")!= null)
        {
            ValidatorEnable(document.getElementById('cmp2'), false);
            ValidatorEnable(document.getElementById('cmp2'), true);
        }
           if(document.getElementById("CmptxtDMAPassword")!= null)
        {
            ValidatorEnable(document.getElementById('CmptxtDMAPassword'), false);
            ValidatorEnable(document.getElementById('CmptxtDMAPassword'), true);
        }
            if(document.getElementById("CmptxtDMARetypePwd")!= null)
        {
            ValidatorEnable(document.getElementById('CmptxtDMARetypePwd'), false);
            ValidatorEnable(document.getElementById('CmptxtDMARetypePwd'), true);
        }
}
//FB 3054 Ends
//ZD 100176 start
function DataLoading(val) 
{
 if (val == "1")
    document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
 else
    document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100176 End

//ZD 100369_MCU
function disablePoll()
{
    var Failenable = document.getElementById("chkFailover");
    
    if((Failenable != null) && (Failenable.checked != true) )
        document.getElementById("lblPollStatus").style.display = 'none';
    else
        document.getElementById("lblPollStatus").style.display = 'block';
}

//ZD 100369_MCU

</script>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>MCU Details</title>
    <div id="dataLoadingDIV" align="center"  style="display:none">
        <img border='0' src='image/wait1.gif'  alt='Loading..' />
    </div> <%--ZD 100176--%><%--ZD 100678--%>
    <script type="text/javascript" src="inc/functions.js"></script>
	<%--FB 1938--%>
    <script type="text/javascript" src="script/cal-flat.js"></script>
    <script type="text/javascript" src="../<%=Session["language"] %>/lang/calendar-en.js" ></script>
    <script type="text/javascript" src="script/calendar-setup.js"></script>
    <script type="text/javascript" src="script/calendar-flat-setup.js"></script>
    <%--ZD 100664 Start--%>
    <script src="script/CallMonitorJquery/jquery.1.4.2.js" type="text/javascript"></script> 
    <script type="text/javascript">
     $(document).ready(function() {
           $('.treeNode').click(function() {
            var target = $(this).attr('tag');
            if ($('#' + target).is(":visible")) {
                $('#' + target).hide("slow");
                $(this).attr('src', 'image/loc/nolines_plus.gif');
            } else {
                $('#' + target).show("slow");
                
                $(this).attr('src', 'image/loc/nolines_minus.gif');
            }
            return false; //ZD 100040

        });
        });
    </script>
    <%--ZD 100664 End--%>
    
    <link rel="stylesheet" type="text/css" media="all" href="css/calendar-blue.css" /> <%--FB 1938--%> <%--FB 1982--%>
    
</head>
<body>
    <form id="frmMCUManagement" runat="server" method="post" autocomplete="off" onsubmit="return true;DataLoading(1);"> <%--ZD 100176--%> <%--ZD 101190--%> 
    <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
    <input id="confPassword" runat="server" type="hidden" />
    <input id="txtApprover1_1" runat="server" type="hidden" />
    <input id="txtApprover2_1" runat="server" type="hidden" />
    <input id="txtApprover3_1" runat="server" type="hidden" />
    <input id="txtApprover4_1" runat="server" type="hidden" />
    <input id="hdnApprover1_1" runat="server" type="hidden" />
    <input id="hdnApprover2_1" runat="server" type="hidden" />
    <input id="hdnApprover3_1" runat="server" type="hidden" />
    <input id="hdnApprover4_1" runat="server" type="hidden" />
    <input id="hdnTempPwd" runat="server" type="hidden" value="" /> <%-- ZD 100369 508 Issues --%>
    <%--Code changed for FB 1425 QA Bug -Start--%>
      <input type="hidden" id="hdntzone" runat="server"/>
      <%--Code changed for FB 1425 QA Bug -End--%>
       <input type="hidden" id="hdnMcuCount" runat="server"/> <%--FB 2486 --%>
       <input type="hidden" id="hdnRPRMUserid" runat="server"/> <%--FB 2709 --%>
       <input type="hidden" id="hdnRPRMCallCount" runat="server"/> <%--FB 2709 --%>
       <input type="hidden" id="hdnPasschange" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnDMSPW" value="false" runat="server"/> <%--FB 3054--%>
        <input type="hidden" id="hdnPW1Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnPW2Visit" value="false" runat="server"/> <%--FB 3054--%>
        <input type="hidden" id="hdnDMA1Visit" value="false" runat="server"/> <%--FB 3054--%>
       <input type="hidden" id="hdnDMA2Visit" value="false" runat="server"/> <%--FB 3054--%>
       <%--ZD 103550--%>
       <input type="hidden" id="hdnBJNAppKey" runat="server" />
        <input type="hidden" id="hdnBJNAppKey1" runat="server" /> 
        <input type="hidden" id="hdnBJNAppKeyVisit1" value="false" runat="server" /> 
        <input type="hidden" id="hdnBJNAppSecret" runat="server" />
        <input type="hidden" id="hdnBJNAppSecret1" runat="server" /> 
        <input type="hidden" id="hdnBJNAppSecretVisit1" value="false" runat="server" /> 
      <%--FB 1938 - Start --%>
     <div id="UsageReportDiv"  runat="server" align="center" style="top: 170px;left:105px; POSITION: absolute; WIDTH:92%; HEIGHT: 350px;VISIBILITY: visible; Z-INDEX: 3; display:none"> 
      <table width="75%" border="1" style="border-color:Blue;" cellpadding="0" cellspacing="0">
        <tr>
            <td>
              <table cellpadding="2" cellspacing="1"  width="100%" class="tableBody" align="center" bgcolor='#E1E1E1'><%--ZD 100426--%>
                 <tr>
                    <td class="subtitleblueblodtext" align="center" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_MCUPortUsage%>" runat="server"></asp:Literal></td>            
                 </tr>
                 <tr>
                    <td width="27%" style="font-weight:bold" class="blackblodtext" align="left"> <!-- FB 2050 --><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_TotalAudioVide%>" runat="server"></asp:Literal></td> 
                    <td style="font-weight:bold" class="blackblodtext" align="left">
                      :&nbsp;<asp:Label ID="LblVideoused" runat="server" ></asp:Label>
                   </td>           
                 </tr>
                 <tr>
                    <td width="20%" style="font-weight:bold" class="blackblodtext" align="left"> <!-- FB 2050 --><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_TotalAudioVideoAvail%>" runat="server"></asp:Literal></td> 
                    <td style="font-weight:bold" class="blackblodtext" align="left">
                      :&nbsp;<asp:Label ID="LblVideoavail" runat="server" ></asp:Label>
                   </td>            
                 </tr>
                 <tr>
                    <td width="20%" style="font-weight:bold" class="blackblodtext" align="left"> <!-- FB 2050 --><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_TotalAudioOnl%>" runat="server"></asp:Literal></td>
                    <td style="font-weight:bold" class="blackblodtext" align="left">
                      :&nbsp;<asp:Label ID="LblAudioused" runat="server" ></asp:Label>
                   </td>             
                 </tr>
                 <tr>
                    <td width="20%" style="font-weight:bold" class="blackblodtext" align="left"> <!-- FB 2050 --><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_TotalAudioOnlyAvail%>" runat="server"></asp:Literal></td>
                    <td style="font-weight:bold" class="blackblodtext" align="left">
                      :&nbsp;<asp:Label ID="LblAudioavail" runat="server" ></asp:Label>
                   </td>             
                 </tr>
                 <tr>
                    <td class="subtitleblueblodtext" align="center" colspan="2"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_Conferences%>" runat="server"></asp:Literal></td>            
                 </tr>
                 <tr>
                    <td align="center" colspan="2">
                       <div  style="width: 95%;height: 350px;overflow: auto;" id="dd" runat="server"  >
                            <asp:DataGrid ID="dgUsageReport" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="100%" AutoGenerateColumns="false" ShowFooter="false" runat="server" style="border-collapse:separate" BackColor='#A1A1A1'> <%--ZD 100426--%>
                                 <HeaderStyle HorizontalAlign="Left" />
                                <Columns> <%-- FB 2050 Starts --%>
                                    <asp:BoundColumn HeaderText="<%$ Resources:WebResources, ConferenceName%>" DataField="ConfName"  ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" HeaderStyle-Width="20%" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="<%$ Resources:WebResources, ConfNo%>" DataField="ConfID" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="<%$ Resources:WebResources, StartDate%>" DataField="StartDate" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="<%$ Resources:WebResources, EndDate%>" DataField="EndDate" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="<%$ Resources:WebResources, AudioPortUsed%>" DataField="AudioOnly" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderText="<%$ Resources:WebResources, VideoPortUsed%>" DataField="AudioVideo" ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left"></asp:BoundColumn>
                                </Columns> <%-- FB 2050 Ends --%>
                            </asp:DataGrid>
                            <asp:Label id="lblDetails" text="<%$ Resources:WebResources, BridgeDetails_lblDetails%>" cssclass="lblError" runat="server"></asp:Label>
                        </div>
                    </td>
                  </tr>
                  <tr>
                   <td align="center" colspan="2">
                      <button ID="BtnClose" class="altMedium0BlueButtonFormat" runat="server" onclick="javascript:return fnShowHide('0');">
						<asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_BtnClose%>" runat="server"></asp:Literal></button>
                   </td>
                  </tr>
                </table>
            </td>
        </tr>
    </table>
    </div>
      <%--FB 1938 - End --%>
    
    <div>
      <input type="hidden" id="helpPage" value="65">
        
        <table width="100%">
            <tr>
                <td align="center">
                    <h3>
                        <asp:Label ID="lblHeader" runat="server"></asp:Label>
                    </h3>
                </td>
            </tr>
            
            <%--ZD 100664 - Start --%>
            
            <tr id="trHistory" runat="server">
                <td>
                    <table width="100%" border="0">
                            <tr>
                                <td valign="top" align="left" style="width: 2px">
                                    <span class="blackblodtext" style="vertical-align:top"><asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, History%>" runat="server"></asp:Literal></span>
                                        <a href="#" onmouseup="if(event.keyCode == 13){this.childNodes[0].click();return false;}" onkeydown="if(event.keyCode == 13){document.getElementById('imgHistory').click();return false;}">
                                        <img id="imgHistory" src="image/loc/nolines_plus.gif" class="treeNode" style="border:none" alt="Expand/Collapse" border="0" tag="tblHistory" /></a>
                                </td>
                             </tr>
                    </table>
                </td>
            </tr>
            <tr>
                 <td> 
                      <div id="tblHistory" style="display:none; width:600px; float:left">
                        <table cellpadding="4" cellspacing="0" border="0" style="border-color:Gray; border-radius:15px; background-color:#ECE9D8"  width="75%" class="tableBody" align="center" >
                            <tr>
                                <td class="subtitleblueblodtext" align="center">
                                    <asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, Details%>" runat="server"></asp:Literal>
                                </td>            
                            </tr>
                            <tr align="center">
                                <td align="left">
                                    <asp:DataGrid ID="dgChangedHistory" runat="server" AutoGenerateColumns="False" CellPadding="4" GridLines="None" AllowSorting="true" 
                                        BorderStyle="solid" BorderWidth="0" ShowFooter="False"  
                                        Width="100%" Visible="true" style="border-collapse:separate" >
                                        <SelectedItemStyle  CssClass="tableBody"/>
                                        <AlternatingItemStyle CssClass="tableBody" />
                                        <ItemStyle CssClass="tableBody"  />                        
                                        <FooterStyle CssClass="tableBody"/>
                                        <HeaderStyle CssClass="tableHeader" />
                                        <Columns>
                                            <asp:BoundColumn DataField="ModifiedUserId"  HeaderStyle-Width="0%" Visible="false" ItemStyle-BackColor="#ECE9D8" HeaderStyle-CssClass="tableHeader"></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ModifiedDateTime" HeaderText="<%$ Resources:WebResources, Date%>" ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                            <asp:BoundColumn DataField="ModifiedUserName" HeaderText="<%$ Resources:WebResources, User%>"  ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                            <asp:BoundColumn DataField="Description" HeaderText="<%$ Resources:WebResources, Description%>"  ItemStyle-BackColor="#ECE9D8" ></asp:BoundColumn>
                                        </Columns>
                                    </asp:DataGrid>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <%--ZD 100664 - End --%>
            <tr>
                <td align="left" style="font-weight:bold">
                  <asp:Label ID="lblRequired" ForeColor="red" Font-Size="XX-Small" Text="<%$ Resources:WebResources, BridgeDetails_lblRequired%>" runat="server" Font-Italic="true"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Label ID="errLabel" runat="server" Text="" CssClass="lblError"></asp:Label>
                    <asp:CustomValidator id="customvalidation" runat="server" display="dynamic" OnServerValidate="ValidateIP" />           
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_BasicConfigura%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2">
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_MCUName%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtMCUID" Visible="false" runat="server"></asp:TextBox>
                                <asp:TextBox ID="txtMCUName" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtMCUName" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regMCUName" ControlToValidate="txtMCUName" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold;" class="blackblodtext" id ="tdLogin1" runat="server" ><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_MCULogin%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td> <%--window dressing--%>
                            <td width="30%" align="left" id ="tdLogin2" runat="server">
                                <asp:TextBox CssClass="altText" ID="txtMCULogin" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtMCULogin" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationGroup="Submit" ControlToValidate="txtMCULogin" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters29%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator> <%--Polycom CMA--%>
                            </td>
                        </tr>
                        <tr id="trPwd" runat="server"> <%--ZD 103550--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_MCUPassword%>" runat="server"></asp:Literal></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPassword1" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" TextMode="Password" CssClass="altText" runat="server" onblur="PasswordChange(1)" onfocus=" fnTextFocus(this.id,1)" ></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ControlToValidate="txtPassword1" ValidationGroup="Submit" Display="dynamic" ErrorMessage="Required" runat="server"></asp:RequiredFieldValidator>FB 1408--%>
                                <%--<asp:RegularExpressionValidator ID="reg1" ValidationExpression="{1,128}" ControlToValidate="txtPassword1" Display="Dynamic" ErrorMessage="<br>Only numeric passwords between 1000-9999 are allowed." runat="server"></asp:RegularExpressionValidator>
                                --%><asp:CompareValidator ID="cmpValPassword1" runat="server" ValidationGroup="Submit" ControlToCompare="txtPassword2"
                                    ControlToValidate="txtPassword1" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, EnterPassword%>"></asp:CompareValidator> <%--FB 2232--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ValidationGroup="Submit" ControlToValidate="txtPassword1" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters20%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\+;|`,\[\]{}\x22;=^:()~]*$"></asp:RegularExpressionValidator>
                            </td>   
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_RetypePassword%>" runat="server"></asp:Literal></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox CssClass="altText" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" ID="txtPassword2" TextMode="Password" runat="server" onblur="PasswordChange(2)" onfocus=" fnTextFocus(this.id,2)"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Submit" ControlToValidate="txtPassword2" Display="dynamic" ErrorMessage="Required" runat="server"></asp:RequiredFieldValidator>FB 1408--%>
                                <asp:CompareValidator ID="cmp2" runat="server" ControlToCompare="txtPassword1"
                                    ControlToValidate="txtPassword2" Display="Dynamic" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch%>"></asp:CompareValidator> <%--FB 2232--%>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ValidationGroup="Submit" ControlToValidate="txtPassword2" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters20%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\+;|`,\[\]{}\x22;=^:()~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <%--Code changed for FB 1425 QA Bug -Start--%>
                            <td width="20%" align="left" style="font-weight:bold" id ="TzTD1" runat="server" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_TzTD1%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left" id ="TzTD2" runat="server">
                                <asp:DropDownList ID="lstTimezone" CssClass="altSelectFormat" DataTextField="timezoneName" DataValueField="timezoneID" runat="server"></asp:DropDownList>
                            </td>
                            <%--Code changed for FB 1425 QA Bug -End--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext" ><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_MCUType%>" runat="server"></asp:Literal></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:DropDownList ID="lstMCUType" CssClass="altSelectFormat" OnSelectedIndexChanged="ChangeFirmwareVersion" AutoPostBack="true" DataTextField="name" DataValueField="ID" runat="server"></asp:DropDownList>
                                <asp:DropDownList ID="lstInterfaceType" Visible="false" DataTextField="interfaceType" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_MCUStatus%>" runat="server"></asp:Literal></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:DropDownList ID="lstStatus" CssClass="altSelectFormat" runat="server" DataTextField="name" DataValueField="ID"></asp:DropDownList>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"  id ="tdFirm1" runat="server"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetailsViewOnly_FirmwareVersio%>" runat="server"></asp:Literal></td> <%--window dressing--%>
                            <td width="30%" align="left" id ="tdFirm2" runat="server">
                                <asp:DropDownList ID="lstFirmwareVersion" CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_Administrator%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtApprover4" ReadOnly="true" EnableViewState="true" CssClass="altText" runat="server"></asp:TextBox>
                                <a href="javascript: getYourOwnEmailList(3);" onmouseover="window.status='';return true;"  id="imgAdminsearch" runat="server"><img border="0" src="image/edit.gif" alt="edit" WIDTH="17" HEIGHT="15" style="cursor:pointer;" runat="server" title="<%$ Resources:WebResources, AddressBook%>"></a> <%--FB 2798--%>                                
                                <asp:TextBox ID="hdnApprover4" runat="server" Width="0" Height="0" ForeColor="transparent" BorderColor="transparent"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" ControlToValidate="hdnApprover4" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ManageRoomProfile_Multipleassist%>" runat="server"></asp:Literal><br />(<asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, Semicolonseparated%>" runat="server"></asp:Literal>)</td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtMultipleAssistant" Rows="2" TextMode="MultiLine" Width="245px" runat="server" CssClass="altText"></asp:TextBox><%--ZD 100040--%>
                                <a href="" onclick="this.childNodes[0].click();return false;"><img id="Img5" onclick="javascript:getYourOwnAddressList()" src="image/edit.gif" style="cursor: pointer;" alt="Address Book" runat="server" title="<%$ Resources:WebResources, AddressBook%>" /></a> <%--ZD 101028--%>
                                <a href="" onclick="this.childNodes[0].click();return false;"><img border='0' src='image/deleteall.gif' alt="Delete All"  style="cursor:pointer;" title="<%$ Resources:WebResources, RemoveAll%>" runat="server" id="ImageDel" width='18' height='18' onclick="JavaScript:ClearAllSelection()"/></a>  <%--ZD 101028--%>
                            </td>
                        </tr>
                        
                        <%--Api port starts--%>
                        <tr>
                        <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_APIPort%>" runat="server"></asp:Literal></td>
                        <td width="30%" align="left">
                        <asp:TextBox ID="txtapiportno" CssClass="altText" runat="server" MaxLength="5"></asp:TextBox>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator13" ValidationGroup="Submit" ControlToValidate="txtapiportno" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                        </td>
                        </tr>
                        <%--Api port ends--%>
                        <%--FB 1937--%>
                         <tr id="trAVPorts" runat="server"> <%--ZD 103550--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext" valign="top"><label id="lblAVPorts" runat="server"><%--ZD 100040--%>
                            <asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, BridgeDetails_TotalAudioVideo%>" runat="server"></asp:Literal></label></td> <%--window dressing FB 1937--%><%--FB 2659--%>
                            <td width="30%" align="left">
                                <%--ZD 100040 - Start--%>    
                                <table border="0" style="width:220px; margin-left:-6px;">
                                <tr>
                                <td id="tdImgVideoPort" style="width: 10px" runat="server" valign="top" align="left">
                                    <span class="blackblodtext" style="vertical-align:top"></span>
                                        <a href="#" onkeydown="if(event.keyCode == 13){this.childNodes[0].click();return false;}" onclick="return false;"><%--ZD 100040--%>
                                        <img id="img_VideoPort" src="image/loc/nolines_plus.gif" class="treeNode" style="border:none; width:22px" alt="Expand/Collapse" tag="divVideoPort" /></a><%--ZD 100040--%>
                                </td>          
                                <td><%--ZD 100040--%>
                                <div id="divVideoPort" style="display:none">
                                 <asp:DataGrid ID="dgVideoPort" BorderColor="Gray" BorderWidth="1px" BorderStyle="Solid" runat="server" AutoGenerateColumns="False" CellPadding="2" GridLines="None" AllowSorting="true" ShowHeader="false" 
                                 ShowFooter="False" Width="100%" Visible="true" style="border-collapse:separate"><%--ZD 100040--%>                                   
                                <AlternatingItemStyle CssClass="tableBody" HorizontalAlign="left" />                                
                                <ItemStyle CssClass="tableBody" HorizontalAlign="left" />                            
                                <Columns>
                                    <asp:BoundColumn DataField="resolutionId" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn>                                      
                                        <ItemTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td style="width:70%;">
                                                    <asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.resolution") %>'></asp:Label>
                                                </td>                                            
                                                <td style="width:30%" class="tableBody">                                                
                                                    <asp:TextBox ID="txtVideoPort" runat="server" MaxLength="5" Text='<%# DataBinder.Eval(Container, "DataItem.qty") %>' Width="35px"></asp:TextBox>
                                                    <asp:RegularExpressionValidator ID="RegVideoPort" runat="server" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Numericsonly%>" SetFocusOnError="True" ToolTip="<%$ Resources:WebResources, Numericsonly%>" ControlToValidate="txtVideoPort" ValidationExpression="\d+" Display="Dynamic"></asp:RegularExpressionValidator>
                                                    <asp:Label ID="lblResolutionID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.resolutionId") %>'></asp:Label>                                              
                                                </td>
                                                </tr>
                                                </table>
                                                </ItemTemplate>
                                        <FooterTemplate>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <SelectedItemStyle BackColor="Beige" />
                                </asp:DataGrid>
                                </div><%--ZD 100040--%>
                                </td>                     
                                </tr>
                                </table>                                
                                <asp:TextBox CssClass="altText" ID="txtMaxVideoCalls" runat="server" Visible="false"></asp:TextBox>                              
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ValidationGroup="Submit" ControlToValidate="txtMaxVideoCalls" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly2%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>                                
                                <%--ZD 100040 - End--%>
                            </td>
                            <td  width="20%" align="left" id="tdAudio"  style="font-weight:bold" class="blackblodtext" runat="server" valign="top"><label id="lblAudioPorts" runat="server"><%--ZD 100040--%>
                            <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, BridgeDetails_TotalAudioOnly%>" runat="server"></asp:Literal></label></td> <%--window dressing FB 1937--%><%--FB 2659--%>
                            <td  width="30%" align="left" id="tdtxtAudio" runat="server" valign="top"> <%--ZD 100040--%>
                                <asp:TextBox ID="txtMaxAudioCalls" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ValidationGroup="Submit" ControlToValidate="txtMaxAudioCalls" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly2%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <%--ZD 100369_MCU Starts--%>
                         <tr id="trURLandPoll" runat="server"> <%--ZD 103550--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_URLAccess%>" runat="server"></asp:Literal></td> 
                            <td width="30%" align="left">
                                <asp:DropDownList ID="drpURLAccess" CssClass="altSelectFormat" runat="server" Width="210px"><%--ZD 100040--%>
                                    <asp:ListItem Text="http" Value="0"></asp:ListItem>
                                    <asp:ListItem Text="https" Value="1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <%--ZD 100369_MCU Starts--%>
                            <td  width="20%" align="left" id="tdFailover"  style="font-weight:bold" class="blackblodtext" runat="server">
                                <label id="lblFailover" runat="server"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, FailOverPolling%>" runat="server"></asp:Literal></label>
                            </td> 
                            <td width="30%" align="left">
                                <div id="divFailover" runat="server" style="margin-left:-1px" >
                                    <table style="border-collapse:collapse">
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="chkFailover" runat="server" onclick="javascript:disablePoll();" />
                                            </td>
                                            <td>
                                                <label id="lblPollStatus" runat="server"></label>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <%--ZD 100369_MCU Starts--%>
                        </tr>
                         <tr id="trVirMCUandPublic" runat="server"> <%--ZD 103550--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext" valign="top"><asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, BridgeDetails_VirtualMCU%>" runat="server"></asp:Literal></td> <%--window dressing--%><%--ZD 100040--%> 
                            <td width="30%" align="left" valign="top"><%--ZD 100040--%> 
                            <%--ZD 100040 - Start--%> 
                                <table border="0" style="width:220px; margin-left:-6px;">
                                <tr>
                                <td valign="top" style="width: 7%">
                                <asp:CheckBox ID="chkIsVirtual" runat="server" />
                                </td>
                                <td></td>
                                </tr>
                                <tr>
                                <td id="tdImgLoadBalance" style="display:none;width: 10px" runat="server" valign="top" align="left">
                                    <span class="blackblodtext" style="vertical-align:top"></span>
                                        <a href="#" onkeydown="if(event.keyCode == 13){this.childNodes[0].click();return false;}" onclick="return false;"><%--ZD 100040--%>
                                        <img id="img_LoadBalance" src="image/loc/nolines_plus.gif" class="treeNode" style="border:none; width:22px" alt="Expand/Collapse" tag="divloadBalance" /></a><%--ZD 100040--%>
                                </td>             

                                <td><%--ZD 100040--%>  
                                <div id="divloadBalance" style="display:none">                                                              
                                 <asp:DataGrid ID="dgloadBalance" runat="server" BorderColor="Gray" BorderWidth="1px" BorderStyle="Solid" AutoGenerateColumns="False" CellPadding="2" GridLines="None" AllowSorting="true"
                                 ShowFooter="False" Width="100%" Visible="true" style="border-collapse:separate"><%--ZD 100040--%>                                   
                                <AlternatingItemStyle CssClass="tableBody" HorizontalAlign="left" />
                                <HeaderStyle CssClass="tableHeader" HorizontalAlign="left" />
                                <ItemStyle CssClass="tableBody" HorizontalAlign="left" />                            
                                <Columns>
                                    <asp:BoundColumn DataField="bridgeId" Visible="false"></asp:BoundColumn>
                                    <asp:TemplateColumn>
                                        <HeaderTemplate>
                                        <table width="100%">
                                            <tr><%--FB 2839 Start--%>
                                                <td width="70%" class="tableHeader"><asp:Literal ID="Literal19" Text="<%$ Resources:WebResources, MCU%>" runat="server"></asp:Literal></td>
                                                <td width="30%" class="tableHeader"><asp:Literal ID="Literal20" Text="<%$ Resources:WebResources, MCULBGroupCreate_Loadbalance%>" runat="server"></asp:Literal></td>                                               
                                            </tr>
                                        </table>
                                    </HeaderTemplate>                                      
                                        <ItemTemplate>
                                        <table width="100%">
                                            <tr>
                                                <td style="width:70%; white-space: normal">
                                                    <asp:Label ID="lblbridgeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bridgeName") %>'></asp:Label>
                                                </td>                                            
                                                <td style="width:30%" class="tableBody"> 
                                                    <asp:CheckBox ID="chkLoadMCU" runat="server" Enabled="false"  Checked='<%# DataBinder.Eval(Container, "DataItem.loadBalance").Equals("1") %>'  /> 
                                                    <asp:Label ID="lblbridgeId" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.bridgeId") %>'></asp:Label>                                                                                                                                                       
                                                </td>
                                                </tr>
                                                </table>
                                                </ItemTemplate>
                                        <FooterTemplate>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                                <SelectedItemStyle BackColor="Beige" />
                                </asp:DataGrid>
                                </div><%--ZD 100040--%>
                                </td>
                                </tr>
                                </table>
                                <%--ZD 100040 - End--%>
                            </td>
                             <%--FB 1920 - Starts--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext" valign="top"><%--ZD 100040--%>
                                <asp:Label ID="lblIsPublic" runat="server"><asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, Public%>" runat="server"></asp:Literal></asp:Label>
                            </td>
                            <td width="30%" align="left" valign="top"><%--ZD 100040--%>
                                <asp:CheckBox ID="chkbxIsPublic" runat="server" />
                            </td>
                            <%--FB 1920 - Ends--%>
                        </tr>
                         <%--ZD 100369_MCU End--%>
                        <%--FB 2660 Starts--%>
                        <tr id="trCDR" runat="server"> <%--ZD 103550--%>
                            <td width="20%" align="left"> <span class="blackblodtext" id="spEnableCDR" runat="server"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_spEnableCDR%>" runat="server"></asp:Literal></span></td>
                            <td width="30%" align="left">
                                <%--ZD 100040 - Start--%>
                                <table border="0" style="margin-left:-6px">
                                    <tr>
                                        <td>
                                            <asp:CheckBox ID="chkEnableCDR" runat="server" onclick="javascript:CDRDays();" /> 
                                        </td>
                                    </tr>
                                </table>
                                <%--ZD 100040 - End--%>
                            </td>
                            <td width="20%" align="left"> <span class="blackblodtext" id="spCDREvent" runat="server"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_spCDREvent%>" runat="server"></asp:Literal></span></td>
                            <td width="30" align="left"> 
                            <div id="divtxtCDR" runat="server" nowrap="nowrap"> <%--FB 2714 Starts--%>
                                <asp:TextBox ID="txtdltCDR" CssClass="altText" onkeypress="return isNumberKey(event)" runat="server" Width="50px" MaxLength="3"></asp:TextBox> <asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_divtxtCDR%>" runat="server"></asp:Literal>
                                <asp:RegularExpressionValidator ID="regDeleteCBR" Enabled="false" ValidationGroup="Submit" ControlToValidate="txtdltCDR"
                                 Display="dynamic" runat="server" SetFocusOnError="true"
                                ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly2%>" ValidationExpression="\d+"></asp:RegularExpressionValidator><%--FB 2714 End--%>
                            </div>
                            </td>
                         </tr>
                         <%--FB 2660 Ends--%>
                        <tr style="display:none">
                            <%--FB 2501 CallMonitor Favourite Starts--%>
                            <td width="20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_SetFavourite%>" runat="server"></asp:Literal></td> <%--FB 2591--%>
                            <td width="30%" align="left">
                                <asp:CheckBox ID="chkSetFavourite" runat="server" /> <%--FB 2591--%>
                            </td>
                            <%--FB 2501 Ends--%>
                            <td width="20%">&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                  
                         
                         </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_MCUApprovers%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2">
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_PrimaryApprove%>" runat="server"></asp:Literal></td><%--window dressing--%>
                            <td width="800px" align="left"><%--Edited For FF--%>
                                <asp:TextBox ID="txtApprover1" ReadOnly="true" EnableViewState="true" CssClass="altText" runat="server"></asp:TextBox><%--FB 2659--%>
                                <a href="javascript: getYourOwnEmailList(0);" onmouseover="window.status='';return true;" id="imgApprover1" runat="server" ><img border="0" src="image/edit.gif" style="vertical-align:top;cursor:pointer;" alt="edit" WIDTH="17" HEIGHT="15" runat="server" title="<%$ Resources:WebResources, AddressBook%>"></a><%--Edited For FF FB 2798--%>
                                <a href="" onclick="this.childNodes[0].click();return false;"><img border="0" src="image/btn_delete.gif" id="imgDelApprover1" runat="server"  style="vertical-align:top;cursor:pointer;" alt="delete" WIDTH="16" HEIGHT="16" onclick="javascript:DeleteApprover(1)"  title="<%$ Resources:WebResources, Delete%>" ></a><%--Edited For FF FB 2798--%> <%--ZD 100420--%>
                            </td>
                            <td width="50%" align="left">
                                <asp:TextBox ID="hdnApprover1" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_SecondaryAppro%>" runat="server"></asp:Literal></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtApprover2" ReadOnly="true" CssClass="altText" runat="server"></asp:TextBox><%--FB 2659--%>
                                <a href="javascript: getYourOwnEmailList(1);" onmouseover="window.status='';return true;" id="imgApprover2" runat="server"><img border="0" src="image/edit.gif" style="vertical-align:top;cursor:pointer;" alt="edit" WIDTH="17" HEIGHT="15" runat="server" title="<%$ Resources:WebResources, AddressBook%>"></a><%--Edited For FF FB 2798--%>
                                <a href="" onclick="this.childNodes[0].click();return false;"><img border="0" src="image/btn_delete.gif" id="imgDelApprover2" runat="server" alt="delete" WIDTH="16" HEIGHT="16" style="vertical-align:top;cursor:pointer;" onclick="javascript:DeleteApprover(2)"  title="<%$ Resources:WebResources, Delete%>" ></a><%--Edited For FF FB 2798--%><%--ZD 100420--%>
                            </td>
                            <td width="50%" align="left">
                                <asp:TextBox ID="hdnApprover2" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, OrganisationSettings_SecondaryAppro2%>" runat="server"></asp:Literal></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtApprover3" ReadOnly="true" CssClass="altText" runat="server"></asp:TextBox>
                                <a href="javascript: getYourOwnEmailList(2);" onmouseover="window.status='';return true;"  id="imgApprover3" runat="server"><img border="0" src="image/edit.gif" style="vertical-align:top;cursor:pointer;" alt="edit" WIDTH="17" HEIGHT="15" runat="server" title="<%$ Resources:WebResources, AddressBook%>"></a> <%--Edited For FF FB 2798--%>                           
                                <a href="" onclick="this.childNodes[0].click();return false;"><img border="0" src="image/btn_delete.gif"  id="imgDelApprover3" runat="server"  alt="delete" WIDTH="16" HEIGHT="16" style="vertical-align:top;cursor:pointer;" onclick="javascript:DeleteApprover(3)"  title="<%$ Resources:WebResources, Delete%>" ></a><%--Edited For FF FB 2798--%><%--ZD 100420--%>
                            </td>
                            <td width="50%" align="left">
                                <asp:TextBox ID="hdnApprover3" Width="0" Height="0" ForeColor="transparent" BackColor="transparent" BorderWidth="0" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                    </table>                 
                </td>
            </tr>
             <tr id="tr7" runat="server"> <%--ZD 103550--%>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <span class="subtitleblueblodtext"><asp:Literal ID="Literal21" Text="<%$ Resources:WebResources, BJNConfiguration%>" runat="server"></asp:Literal></span>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="tr8" runat="server">
                <td align="center">
                    <table width="90%" cellspacing="3" cellpadding="2" border="0">
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">
                                <asp:Literal ID="Literal22" Text="<%$ Resources:WebResources, BJNAppKey%>" runat="server"></asp:Literal>
                            </td>
                            <td width="30%" align="left" >
                                <asp:TextBox ID="txtBJNAppKey" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" runat="server" 
                                    CssClass="altText" TextMode="Password" Width="200px" onblur="PasswordChange(9,1)" onfocus=" fnTextFocus(this.id,9)"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator16" ControlToValidate="txtBJNAppKey" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="left" class="blackblodtext">
                                <asp:Literal ID="Literal23" Text="<%$ Resources:WebResources, BJNAppSecret%>" runat="server"></asp:Literal> 
                            </td>
                            <td width="30%" align="left" >
                                    <asp:TextBox ID="txtBJNAppSecret" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" onchange="javascript:PreservePassword()" runat="server" 
                                    CssClass="altText" TextMode="Password" Width="200px" onblur="PasswordChange(10,1)" onfocus=" fnTextFocus(this.id,10)"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator15" ControlToValidate="txtBJNAppSecret" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                             <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">
                                <asp:Literal ID="Literal18" Text="<%$ Resources:WebResources, Address%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                            </td>
                            <td width="30%" align="left" >
                                <asp:TextBox ID="txtBJNAddress" runat="server" CssClass="altText" Width="200px"></asp:TextBox>                                
                                <asp:RegularExpressionValidator ID="regBJNAdd" ControlToValidate="txtBJNAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters33%>" ValidationExpression="^[^<>&]*$"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="reqBJNAdd" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, Required%>" ControlToValidate="txtBJNAddress" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                     </table>
                 </td>
            </tr>
            <tr id="tr9" runat="server">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Label cssclass="subtitleblueblodtext" id="lblHeader1" runat="server" text="<%$ Resources:WebResources, BridgeDetails_lblHeader1%>"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>  <%-- Split tr1 row as tr1, tr2 and tr5 for FB 1937--%>
			<%--FB 2636 Starts --%>
            <tr id="tr3" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_PortA%>" runat="server"></asp:Literal></td><%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPortA" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortA" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="<%$ Resources:WebResources, InvalidIPAddress%>" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortA" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_PortB%>" runat="server"></asp:Literal></td><%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPortB" CssClass="altText" runat="server"></asp:TextBox>
                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator3" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortB" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="Invalid IP Address" Display="dynamic" runat="server"></asp:RegularExpressionValidator>--%> <%--ZD 101196--%>
<%--                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" Enabled="true" ControlToValidate="txtPortB" ErrorMessage="Required" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
--%>                            </td>
                        </tr>

                        <%--FB 2003 - Start--%>
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ISDNAudioPref%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left">
                               <asp:TextBox CssClass="altText" ID="txtISDNAudioPref" MaxLength="4" runat="server"></asp:TextBox>
			                   <asp:RegularExpressionValidator ID="regISDNAudioPref" ControlToValidate="txtISDNAudioPref" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationGroup="Submit" ValidationExpression="^(\(|\d| |-|\))*$"></asp:RegularExpressionValidator>
			                </td>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ISDNVideoPref%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left">
                               <asp:TextBox CssClass="altText" ID="txtISDNVideoPref" MaxLength="4" runat="server"></asp:TextBox>
			                   <asp:RegularExpressionValidator ID="regISDNVideoPref" ControlToValidate="txtISDNVideoPref" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationGroup="Submit" ValidationExpression="^(\(|\d| |-|\))*$"></asp:RegularExpressionValidator>
			                </td>
                        </tr>
                        <%--FB 2003 - End--%>
						<tr id="tr6" runat="server">
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ISDNGateway%>" runat="server"></asp:Literal></td> 
                            <td width="30" align="left"> 
                                <asp:TextBox id="txtISDNGateway" cssclass="altText" runat="server"></asp:TextBox>
                                
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtISDNGateway" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="<%$ Resources:WebResources, InvalidIPAddress%>" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%">&nbsp;</td>
                            <td width="30%">&nbsp;</td>
                  
                         
                         </tr>
                          <%--FB 2610 starts--%>
                         <tr id="trTempExtNumber" runat="server" visible="true">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_Ext%>" runat="server"></asp:Literal>
                            </td>
                            <td  width="30%" align="left">
                                <asp:TextBox ID="txtTempExtNumber" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                            </td>
							<%--ZD 100522 Start--%>
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Label runat="server" ID="lblDomain"> <asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_DomainName%>" runat="server"></asp:Literal></asp:Label> 
                            </td>
                            <td align="left" >
                                <asp:TextBox ID="txtMCUDomain" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox><br /> <asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ExDomain%>" runat="server"></asp:Literal>
                            </td>
                            <%--ZD 100522 End--%>
                        </tr>
                        <tr>
                            <%--FB 2636 starts--%>
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Label runat="server" ID="lblE164"><asp:Literal ID="Literal13" Text="<%$ Resources:WebResources, E164Dailing%>" runat="server"></asp:Literal></asp:Label> 
                            </td>
                            <td align="left" ><%-- FB 2779 --%>
                                <asp:CheckBox ID="chkE164" runat="server" />
                            </td>
                         <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Label runat="server" ID="lblEH323"><asp:Literal ID="Literal14" Text="<%$ Resources:WebResources, H323Dailing%>" runat="server"></asp:Literal></asp:Label> 
                            </td>
                            <td align="left" ><%-- FB 2779 --%>
                                <asp:CheckBox ID="chkH323" runat="server" />
                            </td>
                        </tr>
                        <%--FB 2636 End --%>
                        <%--FB 2610 Ends--%>
                        <%--ZD 101522, 101603 Starts --%>
                        <tr width="100%" id="trsyslocs" runat="server" >
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal ID="Literal17" Text="<%$ Resources:WebResources, SystemLocation%>" runat="server"></asp:Literal>
                            </td>
                            <td width="30%" align="left" nowrap="nowrap">
                                <asp:DropDownList Width="44%" ID="lstSystemLocation" CssClass="alt2SelectFormat" runat="server"
                                    DataValueField="SysLocId" DataTextField="Name">
                                </asp:DropDownList>
                                <asp:Button ID="btnSysLocation" runat="server" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, GetSystemLocation%>"
                                    OnClick="BindSystemLocation" />
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <%--ZD 101522, 101603 End --%>
                    </table>
                </td>
            </tr>
			<%--FB 2636 Ends --%>
            <tr id="tr1" runat="server">
                <td align="center" >
                    <table width="90%" border="0">
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ControlPortIP%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td><%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPortP" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPortP" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortP" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="<%$ Resources:WebResources, InvalidIPAddress%>" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="reqPortP" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortP" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <%--FB 2016 Starts--%>
                        <tr width="100%" id="trConfServiceID" runat="server">
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ConferenceServ%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td><%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtConfServiceID" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RngValidConfServiceID" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtConfServiceID" ValidationExpression="^\d{0,9}$" ErrorMessage="<%$ Resources:WebResources, InvalidServiceID%>" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                <%--<asp:rangevalidator id="RngValidConfServiceID" runat="server"
                                    display="dynamic" controltovalidate="txtConfServiceID" errormessage="Invalid Conference Service ID"
                                   minimumvalue="0" maximumvalue="1000"  type="Integer"></asp:rangevalidator>--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtConfServiceID" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <%--FB 2016 Ends--%>
                        <%--FB 2591 Starts--%>
                        <%--FB 2427 Starts--%>
                        <tr width="100%" id="trProfileID" runat="server">
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ProfileID%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left">
                                <%--<asp:TextBox ID="txtProfileID" CssClass="altText" runat="server"></asp:TextBox>--%>
                                <asp:DropDownList ID="txtProfileID" CssClass="altSelectFormat" runat="server" DataValueField="Id" DataTextField="Name" > <%--FB 2556 --%>
                                <asp:ListItem Selected="True" Text="None" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <%--<asp:RegularExpressionValidator ID="RegtxtProfileID" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtProfileID" ValidationExpression="^\d{0,9}$" ErrorMessage="Invalid Profile ID" Display="dynamic" runat="server"></asp:RegularExpressionValidator>--%><%--Commented for FB 2591--%>
                                <asp:Button ID="btnGetProfiles" runat="server" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, BridgeDetails_btnGetProfiles%>" OnClick="GetProfiles" />
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <%--FB 2427 Ends--%>
                        <%--FB 2591 Ends--%>
                        <%--FB 1907 - Start--%>
                        <tr width="100%" id="trEnableIVR" runat="server">
                            <td width="20%" align="left" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_EnableIVR%>" runat="server"></asp:Literal></td>
                            <td width="15%" align="left">
                                <asp:CheckBox ID="chkEnableIVR" runat="server" onclick="javascript:disableservname();" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trIVRName" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_IVRServiceNam%>" runat="server"></asp:Literal></td>
                            <td align="left" width="55%">
                                <asp:TextBox ID="txtIVRName" MaxLength="99" CssClass="altText" runat="server" Width="128px"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="reqServName" runat="server" ControlToValidate="lstIVR"
                                    Display="dynamic" SetFocusOnError="true" Text="" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                            
                            </td>
                            <td style="width: 20%; display: none" align="left" valign="top">
                                <asp:DropDownList ID="lstIVR" runat="server" CssClass="altSelectFormat" DataTextField="Name"
                                    DataValueField="ID">
                                    <asp:ListItem Text="<%$ Resources:WebResources, PleaseSelect%>" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr id="trEnableRecord" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_EnableRecordin%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:CheckBox ID="chkEnableRecord" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trEnableLPR" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_EnableLPR%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:CheckBox ID="chkLPR" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trEnableMessage" runat="server"><%--FB 2486--%>
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_EnableMessagin%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:CheckBox ID="chkMessage" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                         <%--FB 2610 starts--%>
                         <tr id="trExtensionNumber" runat="server" visible="true">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_Ext%>" runat="server"></asp:Literal>
                            </td>
                            <td  width="30%" align="left">
                                <asp:TextBox ID="txtExtNumber" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <%--FB 2610 Ends--%>
                         <%--ZD 100522 Start--%>
                         <tr>
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Label runat="server" ID="lblTempDomain"><asp:Literal  Text="<%$ Resources:WebResources, BridgeDetails_DomainName%>" runat="server"></asp:Literal></asp:Label> 
                            </td>
                            <td align="left" >
                                <asp:TextBox ID="txtTempDomain" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox> <asp:Literal ID="Literal15" Text="<%$ Resources:WebResources, BridgeDetails_ExDomain%>" runat="server"></asp:Literal>
                            </td>
                        </tr>
                        <%--ZD 100522 End--%>
                        <%--FB 2441 Starts--%>
                        <tr id="trTemplate" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_MCUDomain%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDomain" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="RequiredFieldValidator9" runat="server" ControlToValidate="txtDomain" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <%--FB 2709 --%>
                        <tr id="trRPRMLogin" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_RPRMUserid%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtRPRMLogin" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="RequiredFieldValidator10" runat="server" ControlToValidate="txtRPRMLogin" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                            </td>                      
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ConcurrentCall%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtCallsCount" MaxLength="4" CssClass="altText" runat="server" ></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="RequiredFieldValidator11" runat="server" ControlToValidate="txtCallsCount" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="LoginCountValidator" ControlToValidate="txtCallsCount" ValidationGroup="Submit"
                                        Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly2%>"
                                        ValidationExpression="[\d]+"></asp:RegularExpressionValidator>
                                 <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtCallsCount" ErrorMessage="<%$ Resources:WebResources, ReqConcurrentCall%>" Operator="GreaterThan" Type="Integer" ValueToCompare="0" /> <%--FB 2709--%>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr id="trRPRMemail" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_RPRMEmailAddr%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>  
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtRPRMEmail" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regEmailID" ControlToValidate="txtRPRMEmail"  runat="server" Display="Dynamic" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, ReqEmailID%>" ValidationExpression="^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator  ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtRPRMEmail" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                            </td>                      
                        </tr>

                        <%--ZD 101078 Start--%>
                        <tr id="trURL" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, TMSAccessURL%>" runat="server"></asp:Literal>
                                </td>
                            <td align="left">
                                <asp:TextBox ID="txtURL" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegURL" ControlToValidate="txtURL" ValidationGroup="Submit"
                                    Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters30%>"
                                    ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;?|!`,\[\]{}\x22;=^@#$%&()']*$"></asp:RegularExpressionValidator>
                            </td>
                            <td colspan="3"></td>
                        </tr>
                        <%--ZD 101078 End--%>

                        <%--FB 2709 --%>
                        <tr id="trSynchronous" runat="server">
                            <td id="tdSynchronous" runat="server" width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_tdSynchronous%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <input type="checkbox" id="chkSynchronous" runat="server" />
                            </td>
                            <td id="tdSendMail" runat="server" width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_tdSendMail%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:CheckBox ID="chkSendmail" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                        
                        <tr id="trEhancedMCU" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_DMAMonitoring%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:CheckBox ID="ChkEhancedMCU" runat="server" /><%--FB 2441 II --%>
                            </td>
                            <td colspan="3"></td>
                        </tr>
                        
                        <tr id="trDMAName" runat="server"> 
                         <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_DMAName%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>  
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDMAName" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="rfvSubject"  runat="server" ControlToValidate="txtDMAName" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator><%--FB 2441 II --%>                                
                            </td>
                        <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_DMALogin%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDMALogin" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="reqtxtDMALogin"  runat="server" ControlToValidate="txtDMALogin" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator><%--FB 2441 II --%>
                            </td>
                        
                        </tr>
                        <tr id="trDMAPassword" runat="server"> 
                             <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_DMAPassword%>" runat="server"></asp:Literal></td> <%--window dressing--%>
                            <td width="30%" align="left">
                            	<%--FB 2441 II Starts--%>
                                <asp:TextBox ID="txtDMAPassword" TextMode="Password" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" CssClass="altText" runat="server" onblur="DMAPasswordChange(1)" onfocus=" fnTextFocus(this.id,3)"></asp:TextBox>
                                <asp:CompareValidator ID="CmptxtDMAPassword" runat="server" ValidationGroup="Submit" ControlToCompare="txtDMARetypePwd"
                                    ControlToValidate="txtDMAPassword" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, EnterPassword%>"></asp:CompareValidator> <%--FB 2232--%>
                                <asp:RegularExpressionValidator ID="RgltxtDMAPassword" ValidationGroup="Submit" ControlToValidate="txtDMAPassword" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters20%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:&()~]*$"></asp:RegularExpressionValidator>
                                <%--FB 2441 II Ends--%>
                            </td>
                            <%--FB 2441 II Starts--%>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_DMARetypePass%>" runat="server"></asp:Literal></td> <%--window dressing--%>
                            <td width="30%" align="left">
                                <asp:TextBox CssClass="altText" ID="txtDMARetypePwd" style="background-image:url('../image/pwdbg.png'); background-repeat:no-repeat" TextMode="Password" runat="server" onblur="DMAPasswordChange(2)" onfocus=" fnTextFocus(this.id,4)"></asp:TextBox>
                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ValidationGroup="Submit" ControlToValidate="txtPassword2" Display="dynamic" ErrorMessage="Required" runat="server"></asp:RequiredFieldValidator>FB 1408--%>
                                <asp:CompareValidator ID="CmptxtDMARetypePwd" runat="server" ControlToCompare="txtDMAPassword"
                                    ControlToValidate="txtDMARetypePwd" Display="Dynamic" ValidationGroup="Submit" ErrorMessage="<%$ Resources:WebResources, PasswordNotMatch%>"></asp:CompareValidator> <%--FB 2232--%>
                                <asp:RegularExpressionValidator ID="RgltxtDMARetypePwd" ValidationGroup="Submit" ControlToValidate="txtDMARetypePwd" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters20%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>+;|`,\[\]{}\x22;=^:&()~]*$"></asp:RegularExpressionValidator>
                            </td>
                             <%--FB 2441 II Ends--%>
                           
                        
                        </tr>
                        <tr id="trDMAURL" runat="server"> 
                         <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_DMAURL%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDMAURL" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="ReqtxtDMAURL" runat="server" ControlToValidate="txtDMAURL" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator><%--FB 2441 II --%>
                            </td>
                   
                         <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_DMAPort%>" runat="server"></asp:Literal></td>
                            <td align="left">
                                <asp:TextBox ID="txtDMAPort"  CssClass="altText" runat="server" MaxLength="5"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator18" ValidationGroup="Submit" ControlToValidate="txtDMAPort" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                
                            </td>
                        
                        </tr>
						 <%--FB 2441 II Starts--%>
						 <tr width="100%" id="trDMATemplate" runat="server">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_DMADomain%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtDMADomain"  CssClass="altText" runat="server" ></asp:TextBox><%-- ZD 104144--%>
                                <asp:RequiredFieldValidator  ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtDMADomain" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                                
                            </td>
                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ProfileID%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left">
                                <asp:DropDownList Width="44%" ID="lstTemplate" CssClass="altSelectFormat" runat="server" DataValueField="Id" DataTextField="Name" >
                                <asp:ListItem Selected="True" Text="None" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Button ID="Button1" runat="server" CssClass="altMedium0BlueButtonFormat" Width="175px" Text="<%$ Resources:WebResources, BridgeDetails_Button1%>" OnClick="GetProfiles" />
                            </td>
                            
                        </tr>
                        <%--FB 2441 II Ends--%>
                        <%--FB 2689 Start--%>
                        <tr id="trDMADialinprefix" runat="server"> 
                          <td width="20%" align="left" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_DialinPrefix%>" runat="server"></asp:Literal><span class="reqfldstarText">*</span></td>
                            <td align="left" >
                                <asp:TextBox ID="txtDialinprefix" MaxLength="50" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RequiredFieldValidator  ID="reqtxtDMAprefix"  runat="server" ControlToValidate="txtDialinprefix" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>"></asp:RequiredFieldValidator>
                            </td>
                            <%--ZD 104221 start--%>

                            <td width="20%" align="left" style="font-weight:bold" class="blackblodtext" >
                            <asp:Literal ID="Literal19" Text="<%$ Resources:WebResources, PoolOrder%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left"  nowrap="nowrap">
                                <asp:DropDownList Width="44%" ID="drpPoolOrder" CssClass="altSelectFormat"  runat="server" DataValueField="Id" DataTextField="Name" >
                                <asp:ListItem Selected="True" Text="None" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Button ID="btnPoolOrder" runat="server" CssClass="altMedium0BlueButtonFormat" Width="175px" Text="<%$ Resources:WebResources, GetPoolOrder%>" OnClick="GetPoolOrders" />
                                
                            </td>
                            <%--ZD 104221 End--%>
                        </tr>
                        <%--FB 2689 End--%>
                        
                        <tr id="trDMATestCon" runat="server"><%-- FB 2441 II--%>
                            <td align="center" colspan="4">
                                <button runat="server" ID="btn_DMATestConnection" ValidationGroup="TestConnection" onserverclick="TestConnection" class="altLongBlueButtonFormat">
								<asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_btnDMATestConnection%>" runat="server"></asp:Literal></button><%--ZD 100420--%>                                
                            </td>
                            <td></td>
                            <td></td>
                        </tr>
                        <%--FB 2441 Ends--%>
						<%--FB 2556 --%>
                         <tr id="trMultiTenant" runat="server" visible="true">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_IsMultiTenant%>" runat="server"></asp:Literal>
                            </td>
                            <td  width="30%" align="left">
                                <asp:CheckBox ID="chkMultiTenant" runat="server" />
                            </td>
                        </tr>
                        <tr id="iViewOrgDetails1" runat="server" visible="true">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ScopiaDesktop%>" runat="server"></asp:Literal>
                            </td>
                            <td  width="30%" align="left">
                                <asp:TextBox ID="txtScopiaURL" CssClass="altText" runat="server"></asp:TextBox>
                            </td>
                             <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ScopiaOrganiza%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left" nowrap="nowrap">
                                <%--<asp:TextBox ID="txtScopiaOrgID" CssClass="altText" runat="server"></asp:TextBox>--%>
                                <asp:DropDownList Width="44%" ID="lstScopiaOrg" CssClass="alt2SelectFormat" runat="server" DataValueField="Id" DataTextField="Name" >
                                <asp:ListItem Selected="True" Text="<%$ Resources:WebResources, None%>" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Button ID="btnMCUSilo" Width="50%" runat="server" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, BridgeDetails_btnMCUSilo%>" OnClick="GetMCUSilo" />
                            </td>
                        </tr>
                        <tr id="iViewOrgDetails2" runat="server" visible="true">
                            <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ScopiaService%>" runat="server"></asp:Literal>
                            </td>
                            <td  width="30%" align="left">
                            
                                <%--<asp:TextBox ID="txtScopiaServiceID" CssClass="altText" runat="server"></asp:TextBox>--%>
                                <asp:DropDownList Width="44%" ID="lstScopiaService" CssClass="alt2SelectFormat" runat="server" DataValueField="Id" DataTextField="Name" >
                                <asp:ListItem Selected="True" Text="<%$ Resources:WebResources, None%>" Value="0"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:Button ID="btnMCUService" style="width:150px;" runat="server" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, BridgeDetails_btnMCUService%>" OnClick="GetMCUService" />
                            </td>
                             <td width="20%" align="left" valign="top" style="font-weight: bold" class="blackblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ScopiaOrganizaLogin%>" runat="server"></asp:Literal>
                            </td>
                            <td  width="30%" align="left">
                                <asp:TextBox ID="txtScopiaOrgLogin" CssClass="altText" runat="server"></asp:TextBox>
                            </td>
                        </tr>
                        <%--FB 2556 --%>
                     </table>
                </td>
            </tr><%-- FB 1937--%>
            <tr id="tr5" runat="server"> 
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td align="left" valign="top" style="font-weight:bold" id="tdExpandCollapse"  class="subtitleblueblodtext">
                                <asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_tdExpandCollapse%>" runat="server"></asp:Literal><input id="chkExpandCollapse" type="checkbox" onclick="javascript:ShowHideRow()" runat="server" class="btprint" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>             
            <tr id="tr2" runat="server">
                <td align="center" >
                    <table width="90%" >
                        <%--FB 1907 - End--%>
                        <tr id="trMCUCards" runat="server" align="center"><%--window dressing--%>
                            <td colspan="4">
                             <%--FB 1766 - Start
                             <table width="100%">
                                <tr>
                                   <td width="20%" align="left" style="font-weight:bold" class="blackblodtext">Enable IVR</td>
                                   <td width="15%" align="left">
                                      <asp:CheckBox ID="chkEnableIVR" runat="server" onclick="javascript:disableservname();" />
                                   </td>
                                   <td>&nbsp;</td>
                                   <td>&nbsp;</td>                                
                                </tr>
                                <tr>
                                   <td width="20%" align="left" valign="top" style="font-weight:bold" class="blackblodtext">IVR Service Name</td>
                                   <td align="left">
                                      <asp:TextBox ID="txtIVRName" MaxLength="99" CssClass="altText" runat="server"></asp:TextBox>
                                   </td>
                                   <td width="25%">
                                      <asp:RequiredFieldValidator ID="reqServName" runat="server" ControlToValidate="lstIVR" Display="dynamic" SetFocusOnError="true" Text="" ValidationGroup="Submit"></asp:RequiredFieldValidator>
                                   </td>
                                   <td style="width:20%;display:none" align="left" valign="top"> 
                                      <asp:DropDownList ID="lstIVR" runat="server"  CssClass="altSelectFormat" DataTextField="Name" DataValueField="ID">
                                      <asp:ListItem Text="Please Select..." Value="-1"></asp:ListItem>
                                      </asp:DropDownList>
                                   </td>
                                </tr>
                              </table>
                              FB 1766 - End--%>
                              <h5 class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_MCUCards%>" runat="server"></asp:Literal></h5>
                              <asp:DataGrid ID="dgMCUCards" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server" style="border-collapse:separate"> <%--Edited for FF--%>
                                 <HeaderStyle CssClass="tableHeader" Height="30" HorizontalAlign="Left" VerticalAlign="Middle" />
                                 <ItemStyle HorizontalAlign="left" />
                                 <Columns>
                                    <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
                                    <asp:BoundColumn HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" DataField="Name" HeaderText="<%$ Resources:WebResources, CardName%>" ItemStyle-Width="40%" HeaderStyle-Width="50%"></asp:BoundColumn><%--window dressing--%>
                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, MaximumConnections%>" ItemStyle-CssClass="tableBody"  HeaderStyle-CssClass="tableHeader" FooterStyle-BorderStyle="NotSet"><%--window dressing--%>
                                       <ItemTemplate>
                                          <asp:TextBox ID="txtMaxCalls" runat="server" CssClass="altText" Text='<%#DataBinder.Eval(Container, "DataItem.MaximumCalls") %>'></asp:TextBox>
                                          <asp:RangeValidator runat="server" ID="rangeMaxCalls" ValidationGroup="Submit" ControlToValidate="txtMaxCalls" ErrorMessage="<%$ Resources:WebResources, ReqMaxCalls%>" MinimumValue="0" MaximumValue="100" Type="integer" SetFocusOnError="true"></asp:RangeValidator> 
                                       </ItemTemplate>
                                    </asp:TemplateColumn>
                                 </Columns>
                              </asp:DataGrid>
                            </td>
                        </tr>
                        <tr id="trIPServices" runat="server" align="center"><%--window dressing--%>
                            <td colspan="4">
                                <h5 class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_IPServices%>" runat="server"></asp:Literal></h5>
                                <asp:DataGrid ID="dgIPServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
<%--                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
--%>                                        <asp:TemplateColumn  ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, SNo%>" ><%--ZD 100425--%>
                                            <ItemTemplate>
                                            <%--Window Dressing--%>
                                                <asp:DropDownList ID="lstOrder" CssClass="altText" EnableViewState="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ChangeIPOrder" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.SortID") %>'>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Name%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtName" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Submit" ControlToValidate="txtName" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" ></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regIPName" ValidationGroup="Submit" ControlToValidate="txtName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, AddressType%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstAddressType" DataTextField="Name" DataValueField="ID" CssClass="altText" runat="server" OnInit="BindAddressType" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.addressType") %>'></asp:DropDownList>  
                                                <asp:RequiredFieldValidator ControlToValidate="lstAddressType" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" InitialValue="-1" ></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Address%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAddress" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.address") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ValidationGroup="Submit" ControlToValidate="txtAddress" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" ></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regIPAddress" ValidationGroup="Submit" ControlToValidate="txtAddress" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters31%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|`,\[\]{}\x22;=^:$%&()\-'~]*$"></asp:RegularExpressionValidator> <%--FB 2267--%><%--ZD 101125--%>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, NetworkAccess%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstNetworkAccess" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>'>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Public%>" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Private%>" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Both%>" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Usage%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstUsage" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>'>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, AudioConferencing%>" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, VideoConferencing%>" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Both%>" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Delete%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDelete" runat="server" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button Text="<%$ Resources:WebResources, BridgeDetails_Add%>" OnClick="AddNewIPService" CssClass="altShortBlueButtonFormat" runat="server" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoIPServices" Text="<%$ Resources:WebResources, BridgeDetails_lblNoIPServices%>" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                <%--<asp:Button ID="btnAddNewIPService" Text="<%$ Resources:WebResources, BridgeDetails_btnAddNewIPService%>" OnClick="AddNewIPService" CssClass="altShortBlueButtonFormat" runat="server" />--%>
                                <button ID="btnAddNewIPService" onserverclick="AddNewIPService" class="altShortBlueButtonFormat" runat="server"><asp:Literal ID="Literal16" Text="<%$ Resources:WebResources, BridgeDetails_Add%>" runat="server"></asp:Literal></button><%--ZD 100420--%>
                                </td>
                            </tr>
                        <tr align="center" id="trISDN1" runat="server"><%--window dressing--%> <%--FB 2636  --%>
                                <td colspan="4">
                                
                                <h5 class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ISDNServices%>" runat="server"></asp:Literal></h5>
                                <asp:DataGrid ID="dgISDNServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
<%--                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
--%>                                        <asp:TemplateColumn ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, SNo%>"><%--ZD 100425--%>
                                            <ItemTemplate>
                                            <%--Window Dressing--%>
                                                <asp:DropDownList ID="lstOrder" CssClass="altText" EnableViewState="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ChangeISDNOrder" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.SortID") %>'>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Name%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtName" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqISDNName" runat="server" ValidationGroup="Submit" Display="dynamic" ControlToValidate="txtName" ErrorMessage="<%$ Resources:WebResources, Required%>" CssClass="lblError"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regISDNName" ValidationGroup="Submit" ControlToValidate="txtName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Prefix%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtPrefix" CssClass="altText" Width="50" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.prefix") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqISDNPrefix" runat="server" ValidationGroup="Submit" Display="dynamic" ControlToValidate="txtPrefix" ErrorMessage="<%$ Resources:WebResources, Required%>" CssClass="lblError"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, StartRange%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtStartRange" CssClass="altText" Width="80" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.startRange") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqISDNStartRange" ValidationGroup="Submit" runat="server" Display="dynamic" ControlToValidate="txtStartRange" ErrorMessage="<%$ Resources:WebResources, Required%>" CssClass="lblError"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, EndRange%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtEndRange" CssClass="altText" Width="80" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.endRange") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="reqISDNEndRange" runat="server" ValidationGroup="Submit" Display="dynamic" ControlToValidate="txtEndRange" ErrorMessage="<%$ Resources:WebResources, Required%>" CssClass="lblError"></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, RanageSortOrder%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstRangeSortOrder" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.RangeSortOrder") %>'>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, StarttoEnd%>" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, EndtoStart%>" Value="1"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, NetworkAccess%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstNetworkAccess" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>'>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Public%>" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Private%>" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Both%>" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Usage%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstUsage" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>'>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, AudioConferencing%>" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, VideoConferencing%>" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Both%>" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Delete%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDelete" runat="server" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                                <asp:Button ID="btnAddISDNService" Text="<%$ Resources:WebResources, BridgeDetails_btnAddISDNService%>" OnClick="AddNewISDNService" ValidationGroup="New" CssClass="altShortBlueButtonFormat" runat="server" />
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <asp:Label ID="lblNoISDNServices" Text="<%$ Resources:WebResources, BridgeDetails_lblNoISDNServices%>" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                <%--<asp:Button ID="Button2" Text="<%$ Resources:WebResources, BridgeDetails_btnAddISDNService%>" OnClick="AddNewISDNService" ValidationGroup="New" CssClass="altShortBlueButtonFormat" runat="server" />--%>
                                <button ID="btnAddISDNService" onserverclick="AddNewISDNService" ValidationGroup="New" class="altShortBlueButtonFormat" runat="server">
                                <asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, BridgeDetails_btnAddISDNService%>" runat="server"></asp:Literal></button><%--ZD 100420--%>
                                </td>
                            </tr>
                        <tr id="trMPIServices" runat="server" align="center"><%--window dressing--%>
                            <td colspan="4">
                                <h5 class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_MPIServices%>" runat="server"></asp:Literal></h5>
                                <asp:DataGrid ID="dgMPIServices" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server">
                                    <HeaderStyle CssClass="tableHeader" Height="30" />
                                    <FooterStyle Height="30" />
                                    <Columns>
<%--                                        <asp:BoundColumn DataField="ID" Visible="false"></asp:BoundColumn>
--%>                                        <asp:TemplateColumn ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, SNo%>" ><%--ZD 100425--%>
                                            <ItemTemplate>
                                            <%--Window Dressing--%>
                                                <asp:DropDownList ID="lstOrder" CssClass="altText" EnableViewState="true" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ChangeIPOrder" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.SortID") %>'>
                                                    <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                    <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                    <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                    <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                    <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                    <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Name%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtName" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.name") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ValidationGroup="Submit" ControlToValidate="txtName" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" ></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="regMPIName" ControlToValidate="txtName" ValidationGroup="Submit" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=^:@#$%&()'~]*$"></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, AddressType%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstAddressType" DataTextField="Name" DataValueField="ID" CssClass="altText" runat="server" OnInit="BindAddressType" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.addressType") %>'></asp:DropDownList>  
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="lstAddressType" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" InitialValue="-1" ></asp:RequiredFieldValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Address%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAddress" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.address") %>'></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" ></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator is="regMPIName" ControlToValidate="txtAddress" ValidationGroup="Submit" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Alphanumericcharacters%>" ValidationExpression="\w+" runat="server"  ></asp:RegularExpressionValidator>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, NetworkAccess%>" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstNetworkAccess" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.networkAccess") %>'>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Public%>" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Private%>" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Both%>" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, Usage%>"  ItemStyle-CssClass="tableBody" > 
                                            <ItemTemplate>
                                                <asp:DropDownList ID="lstUsage" CssClass="altText" runat="server" SelectedValue='<%#DataBinder.Eval(Container, "DataItem.usage") %>'>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, AudioConferencing%>" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, VideoConferencing%>" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="<%$ Resources:WebResources, Both%>" Value="3"></asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Delete%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                            <ItemTemplate>
                                                <asp:CheckBox ID="chkDelete" runat="server" />
                                            </ItemTemplate>
                                            <FooterTemplate>
                                               <%-- <asp:Button ID="btnAddNewMPIService" Text="<%$ Resources:WebResources, BridgeDetails_btnAddNewMPIService%>" OnClick="AddNewMPIService" CssClass="altShortBlueButtonFormat" runat="server" />--%>
                                            </FooterTemplate>
                                        </asp:TemplateColumn>
                                    </Columns>
                                </asp:DataGrid>
                                <%--<asp:Label ID="lblNoMPIServices" Text="<%$ Resources:WebResources, BridgeDetails_lblNoMPIServices%>" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                                <asp:Button ID="btnAddNewMPIService" Text="<%$ Resources:WebResources, BridgeDetails_btnAddNewMPIService%>" OnClick="AddNewMPIService" CssClass="altShortBlueButtonFormat" runat="server" />--%>
                                </td>
                            </tr>                            
                        <%--FB 2636 Starts --%>
                        <tr id="trE164Services" runat="server" align="center">
                        <td colspan="4">
                            <h5 class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_E164Services%>" runat="server"></asp:Literal></h5>
                            <asp:DataGrid ID="dgE164Services" GridLines="Horizontal" BorderStyle="Solid" BorderColor="blue" Width="75%" AutoGenerateColumns="false" ShowFooter="true" runat="server">
                                <HeaderStyle CssClass="tableHeader" Height="30" />
                                <FooterStyle Height="30" />
                                <Columns>
                                   <asp:TemplateColumn ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, SNo%>"><%--ZD 100425--%>
                                        <ItemTemplate>
                                        <asp:TextBox ID="lstOrder" CssClass="altText" Width="20px" Enabled="false" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SortID") %>'></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, StartRange%>" ItemStyle-CssClass="tableBody" >
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtStartRange" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.StartRange") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqFieldValidator2" ValidationGroup="Submit" ControlToValidate="txtStartRange" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" ></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regE164StartRange2" ControlToValidate="txtStartRange" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderStyle-CssClass="tableHeader" HeaderText="<%$ Resources:WebResources, EndRange%>" ItemStyle-CssClass="tableBody" >
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtEndRange" CssClass="altText" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EndRange") %>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="ReqFieldValidator3" ValidationGroup="Submit" ControlToValidate="txtEndRange" Display="dynamic" ErrorMessage="<%$ Resources:WebResources, Required%>" runat="server" ></asp:RequiredFieldValidator>
                                            <asp:RegularExpressionValidator ID="regE164StartRange3" ControlToValidate="txtEndRange" ValidationGroup="Submit" Display="dynamic" runat="server"  SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, NumericValuesOnly%>" ValidationExpression="\d+"></asp:RegularExpressionValidator>
                                        </ItemTemplate>
                                    </asp:TemplateColumn>
                                    <asp:TemplateColumn HeaderText="<%$ Resources:WebResources, Delete%>" HeaderStyle-CssClass="tableHeader" ItemStyle-CssClass="tableBody" >
                                        <ItemTemplate>
                                            <asp:CheckBox ID="chkDelete" runat="server" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <%--<asp:Button ID="btnAddNewE164Service" Text="Add" OnClick="AddNewE164Service" CssClass="altShortBlueButtonFormat" runat="server" />--%>
                                            <button ID="btnAddNewE164Service" onserverclick="AddNewE164Service" class="altShortBlueButtonFormat" runat="server">
												<asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_btnAddNewE164Service%>" runat="server"></asp:Literal></button><%--ZD 100420--%>
                                        </FooterTemplate>
                                    </asp:TemplateColumn>
                                </Columns>
                            </asp:DataGrid>
                            <asp:Label ID="lblNoE164Services" Text="<%$ Resources:WebResources, BridgeDetails_lblNoE164Services%>" CssClass="lblError" runat="server" Visible="false"></asp:Label>
                            <%--<asp:Button ID="btnAddNewE164Service" Text="<%$ Resources:WebResources, BridgeDetails_btnAddNewE164Service%>" OnClick="AddNewE164Service" CssClass="altShortBlueButtonFormat" runat="server" />--%>
                            <button ID="btnAddNewE164Service" onserverclick="AddNewE164Service" class="altShortBlueButtonFormat" runat="server">
                            <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, BridgeDetails_btnAddNewE164Service%>" runat="server"></asp:Literal></button><%--ZD 100420--%>
                            </td>
                        </tr>
                        <%-- FB 2636 Ends --%>
                    </table>
                </td>
            </tr>
            <tr id="tr4" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                            <td width="20%" align="left" style="font-weight:bold"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_PortA%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtPortT" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortT" ValidationExpression="^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$" ErrorMessage="<%$ Resources:WebResources, InvalidIPAddress%>" Display="dynamic" runat="server"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" Enabled="true" ValidationGroup="Submit" ControlToValidate="txtPortT" ErrorMessage="<%$ Resources:WebResources, Required%>" Display="dynamic" runat="server"></asp:RequiredFieldValidator>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold">&nbsp;</td>
                            <td width="30%" align="left">&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trAlert" runat="server"> <%--FB 2556 --%>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_Alerts%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trAlertchks" runat="server"> <%--FB 2556 --%>
                <td align="center" >
                    <table width="90%">
                        <tr>
                            <td width="20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ISDNThreshold%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left">
                                <asp:CheckBox ID="chkISDNThresholdAlert" onclick="javascript:ShowISDN(this)" runat="server" />
                            </td>
                            <td width="20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_MalfunctionAle%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left">
                                <asp:CheckBox ID="chkMalfunctionAlert" runat="server" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="trISDN" style="display:none" runat="server">
                <td align="center" >
                    <table width="90%">
                        <tr>
                            <td width="20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_MCUISDNPortC%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtMCUISDNPortCharge" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ValidationGroup="Submit" ControlToValidate="txtMCUISDNPortCharge" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, Invalidformat%>" ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ISDNLineCost%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtISDNLineCost" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ValidationGroup="Submit" ControlToValidate="txtISDNLineCost" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, Invalidformat%>" ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ISDNMaxCost%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtISDNMaxCost" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ValidationGroup="Submit" ControlToValidate="txtISDNMaxCost" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, Invalidformat%>" ValidationExpression="^\d+(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ISDNThresholdTimeframe%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left">
                                <asp:RadioButtonList ID="rdISDNThresholdTimeframe" runat="server" CssClass="blackblodtext">
                                    <asp:ListItem Text="<%$ Resources:WebResources, Monthly%>" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="<%$ Resources:WebResources, Yearly%>" Value="2"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                         <tr>
                            <td width="20%" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ISDNThresholdPercentage%>" runat="server"></asp:Literal></td>
                            <td width="30%" align="left">
                                <asp:TextBox ID="txtISDNThresholdPercentage" CssClass="altText" runat="server"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator12" ValidationGroup="Submit" ControlToValidate="txtISDNThresholdPercentage" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, Invalidformat%>" ValidationExpression="^\d{0,2}(?:\.\d{0,2})?$"></asp:RegularExpressionValidator>
                            </td>
                            <td width="20%" align="left" style="font-weight:bold">&nbsp;</td>
                            <td width="30%" align="left">&nbsp;</td>
                        </tr>
                   </table>
                </td>
            </tr>
            <%--FB 1642 - DTMF Start--%>
            <tr style="display:none"> <%--FB 2655--%>
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_DTMFSettings%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr style="display:none"> <%--FB 2655--%>
                <td align="center">                
                    <table cellpadding="3" cellspacing="0" width="90%" border="0">
                         <tr>
                            <td align="left" height="21" style="font-weight:bold" class="blackblodtext" width="20%"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_PreConference%>" runat="server"></asp:Literal></td>
                            <td style="text-align: left;" width="30%">
                                <asp:TextBox ID="PreConfCode" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPreConfCode" ControlToValidate="PreConfCode" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" 
                                ErrorMessage="<%$ Resources:WebResources, InvalidCharacters27%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align="left" height="21" style="font-weight:bold" class="blackblodtext" width="20%">
                                <asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_PreLeaderPin%>" runat="server"></asp:Literal>
                            </td>
                            <td style="text-align: left;" width="30%">
                                <asp:TextBox ID="PreLeaderPin" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPreLeaderPin" ControlToValidate="PreLeaderPin" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" 
                                ErrorMessage="<%$ Resources:WebResources, InvalidCharacters27%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" height="21" style="font-weight:bold" class="blackblodtext" width="20%"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_PostLeaderPin%>" runat="server"></asp:Literal></td>
                            <td style="text-align: left;" width="30%">
                                <asp:TextBox ID="PostLeaderPin" runat="server" CssClass="altText"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="regPostLeaderPin" ControlToValidate="PostLeaderPin" Display="dynamic" runat="server" ValidationGroup="Submit" SetFocusOnError="true" 
                                ErrorMessage="<%$ Resources:WebResources, InvalidCharacters27%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\<>^+;?|!`\[\]{}\x22;=:@$%&'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td colspan="2"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%--FB 1938 - Start--%>
            <tr id="ReportRow1" runat="server">
                <td align="Left">
                    <table cellspacing="5">
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_UtilizationRep%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr id="ReportRow" runat="server">
                <td align="center">                
                    <table cellpadding="3" cellspacing="0" width="90%" border="0">
                        <tr>
                            <td width="30%" align="left"> <%-- FB 2050 --%>
                                 <asp:TextBox ID="ReportDate" runat="server" CssClass="altText" Width="35%" ></asp:TextBox>
                                 <a href="" onclick="this.childNodes[0].click();return false;"><img src="image/calendar.gif" alt="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" border="0" width="20" height="20" id="cal_triggerd" style="cursor: pointer;vertical-align:middle;" title="<asp:Literal Text='<%$ Resources:WebResources, DateSelector%>' runat='server'></asp:Literal>" onclick="return showCalendar('<%=ReportDate.ClientID %>', 'cal_triggerd', 1, '<%=format%>');" /></a><%--ZD 100420--%> 
                                 <span class="blackblodtext">@</span> 
                                 <mbcbb:ComboBox ID="ReportTime" runat="server" CssClass="altText" Rows="10" 
                                                                Style="width: auto" ValidationGroup="Submit1">
                                                            </mbcbb:ComboBox> <%--ZD 100284--%>
                                <asp:RequiredFieldValidator ID="reqStartTime" runat="server" ControlToValidate="ReportTime:Text" ValidationGroup="Submit1" 
                                Display="Dynamic" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, Required_Time%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regStartTime" runat="server" ControlToValidate="ReportTime:Text"
                                SetFocusOnError="true" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, InvalidTime%>" ValidationExpression="[0-1][0-9]:[0-5][0-9] ?[A|a|P|p][M|m]"></asp:RegularExpressionValidator>
                                <asp:RequiredFieldValidator ID="reqStartData" runat="server" ControlToValidate="ReportDate" ValidationGroup="Submit1" Display="Dynamic" ErrorMessage="<%$ Resources:WebResources, Required_Date%>"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="regStartDate" runat="server" ControlToValidate="ReportDate" ValidationGroup="Submit1" Display="Dynamic" SetFocusOnError="true"  ErrorMessage="<%$ Resources:WebResources, InvalidDate1%> " ValidationExpression="(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\d\d|(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d"></asp:RegularExpressionValidator>
                            </td>
                            <td align="left"> <!-- FB 2050 -->
                                <asp:Button runat="server" ID="UsageReport" style="width:150px;" Text="<%$ Resources:WebResources, BridgeDetails_UsageReport%>" CssClass="altMedium0BlueButtonFormat" ValidationGroup="Submit1" 
                                CausesValidation="True" OnClientClick="javascript: return fnCheck();"  OnClick="MCUusageReport"/>  
                            </td>
                        </tr>                      
                    </table>
                </td>
            </tr>
            <%--FB 1938 - End--%>
            <tr>
                <td align="center">
                    <table width="90%" border="0">
                        <tr>
                            <td align="left" style="width:65%"> <%-- FB 2050 --%>
                                <%--<asp:Button runat="server" ID="btnTestConnection" ValidationGroup="TestConnection" OnClick="TestConnection" Text="Test Connection" CssClass="altLongBlueButtonFormat" />--%>  <%--OnClientClick="javascript:testConnection();return false;"--%>
                                <button runat="server" ID="btnTestConnection" ValidationGroup="TestConnection" onserverclick="TestConnection" class="altLongBlueButtonFormat">
								<asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_btnTestConnection%>" runat="server"></asp:Literal></button><%--ZD 100420--%>
                            </td>
                            <td align="left"> <%-- FB 2050 --%>
                                <%--<input type="button" id="btnCancel" class="altLongYellowButtonFormat" value="Cancel" onclick="fnCancel()" style="width:100px" />--%> <%--FB 2565--%><%--ZD 100428--%>
                                <button type="button" id="btnCancel" class="altLongYellowButtonFormat" onclick="fnCancel()" style="width:100px">
									<asp:Literal Text="<%$ Resources:WebResources, Cancel%>" runat="server"></asp:Literal></button> <%--ZD 100428--%>
                                <asp:Button ID="btnSubmit" OnClick="SubmitMCU" runat="server" ValidationGroup="Submit"  Text="<%$ Resources:WebResources, BridgeDetails_btnSubmit%>" OnClientClick="javascript:return RequireIVRName();" Width="100px" /><%--FB 1766 FB 2786--%>
                            </td>
                            
                        </tr>
                        <tr id="trCodianMessage" runat="server"> <%--FB Case 698--%>
                            <td class="cmmttext" align="left"> <%-- FB 2050 --%>
                                <asp:Literal Text="<%$ Resources:WebResources, BridgeDetails_ManagementAPI%>" runat="server"></asp:Literal>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:DropDownList CssClass="altLong0SelectFormat" ID="lstAddressType" Visible="false" runat="server" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                    <asp:DropDownList runat="server" ID="lstNetworkAccess" Visible="false" CssClass="altLong0SelectFormat">
                        <asp:ListItem Text="<%$ Resources:WebResources, Public%>" Value="1"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, Private%>" Value="2"></asp:ListItem>
                        <asp:ListItem Text="<%$ Resources:WebResources, Both%>" Value="3"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
        </table>
    </div>
<script language="javascript">

    function fnUpdateTempPwd() // ZD 100369 508 Issues
    {
        var tmpPwd = document.getElementById("hdnTempPwd").value;
        if(tmpPwd != "")
        {
            document.getElementById("txtPassword1").value = tmpPwd;
            document.getElementById("txtPassword2").value = tmpPwd;
        }
    }
    fnUpdateTempPwd();

    SavePassword();
    //FB 1937
    document.getElementById("reqStartTime").controltovalidate = "ReportTime_Text";
    document.getElementById("regStartTime").controltovalidate = "ReportTime_Text";
    
    //ZD 100284 - Start
    if (document.getElementById("ReportTime_Text")) {
        var confstarttime_text = document.getElementById("ReportTime_Text");
        confstarttime_text.onblur = function() {
        formatTimeNew('ReportTime_Text', 'regStartTime',"<%=Session["timeFormat"]%>")
        };
    }
    // ZD 100263
    if ('<%=Session["BridgeID"]%>' == 'new') {
        document.getElementById("txtPassword1").style.backgroundImage = "";
        document.getElementById("txtPassword2").style.backgroundImage = "";
    }    
    
    //ZD 100420 - Start
    if(document.getElementById('txtProfileID')!=null)    
        document.getElementById('txtProfileID').setAttribute("onblur", "document.getElementById('btnGetProfiles').focus();document.getElementById('btnGetProfiles').setAttribute('onfocus', '');");        
    
    if(document.getElementById('lstTemplate')!=null)    
        document.getElementById('lstTemplate').setAttribute("onblur", "document.getElementById('Button1').focus();document.getElementById('Button1').setAttribute('onfocus', '');");
    
    if(document.getElementById('txtDialinprefix')!=null)    
        document.getElementById('txtDialinprefix').setAttribute("onblur", "document.getElementById('btn_DMATestConnection').focus();document.getElementById('btn_DMATestConnection').setAttribute('onfocus', '');");    
    
    if(document.getElementById('lstScopiaOrg')!=null)
        document.getElementById('lstScopiaOrg').setAttribute("onblur", "document.getElementById('btnMCUSilo').focus(); document.getElementById('btnMCUSilo').setAttribute('onfocus', ''); ");
    
    if(document.getElementById('lstScopiaService')!=null)
        document.getElementById('lstScopiaService').setAttribute("onblur", "document.getElementById('btnMCUService').focus(); document.getElementById('btnMCUService').setAttribute('onfocus', '');");
    
    if(document.getElementById('ReportTime_Text')!=null)
    {                
        document.getElementById('ReportTime_Text').setAttribute("onblur", "document.getElementById('UsageReport').focus(); document.getElementById('UsageReport').setAttribute('onfocus', '');");
    }
    
    if(document.getElementById('btnTestConnection')!=null)
        document.getElementById('btnTestConnection').setAttribute("onblur", "document.getElementById('Close').focus(); document.getElementById('Close').setAttribute('onfocus', '');");
    
    if(document.getElementById('btnCancel')!=null)
        document.getElementById('btnCancel').setAttribute("onblur", "document.getElementById('btnSubmit').focus(); document.getElementById('btnSubmit').setAttribute('onfocus', '');");
	//ZD 100420 - End
    if(document.getElementById('lstSystemLocation')!=null) //ZD 101522
        document.getElementById('lstSystemLocation').setAttribute("onblur", "document.getElementById('btnSysLocation').focus(); document.getElementById('btnSysLocation').setAttribute('onfocus', '');");

    
</script>

<%--ZD 100428 START- Close the popup window using the esc key--%>
<script language="javascript" type="text/javascript">
    function EscClosePopup() {
            document.getElementById('BtnClose').click();
    }
</script>
<%--ZD 100428 END--%>

<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
<img src="keepalive.asp" name="myPic" width="1px" height="1px" alt="Keepalive" style="display:none"> <%--ZD 1001419--%>
    </form>
<% if (Request.QueryString["hf"] != null)
   {
       if (!Request.QueryString["hf"].ToString().Equals("1"))
       {
        %>
 
<% }
}%>
   
</body>
</html>
<script type="text/javascript">
    document.onkeydown = function(evt) {
        evt = evt || window.event;
        var keyCode = evt.keyCode;
        if (keyCode == 8) {
            if (document.getElementById("btnCancel") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnCancel").click();
                    return false;
                }
            }
            if (document.getElementById("btnGoBack") != null) { // backspace
                var str = document.activeElement.type;
                if (!(str == "text" || str == "textarea" || str == "password")) {
                    document.getElementById("btnGoBack").click();
                    return false;
                }
            }
        }
        fnOnKeyDown(evt);
    };

    //ZD 103550
    function PreservePassword()
    {
        document.getElementById("hdnBJNAppKey").value = document.getElementById("txtBJNAppKey").value;
        document.getElementById("hdnBJNAppSecret").value = document.getElementById("txtBJNAppSecret").value;
    }

    //ZD 100040 - Start
    function ShowHideLoadbal() 
    {
        if (document.getElementById("chkIsVirtual") != null) 
        {
            if (document.getElementById("chkIsVirtual").checked == true) 
            {
                document.getElementById("tdImgLoadBalance").style.display = '';                
            }
            else 
            {
                document.getElementById("img_LoadBalance").src = "image/loc/nolines_plus.gif";
                document.getElementById('divloadBalance').style.display = 'none';
                document.getElementById("tdImgLoadBalance").style.display = 'none'; 
                                                 
            }
        }

    }
    //ZD 100040 - End

    function ChgbtnHight()
    {
        var lang = "<%=Session["language"] %>";
        if (lang == "fr" || lang == "sp")
          document.getElementById('btnPoolOrder').style.height = "40px";        
    }
    ChgbtnHight();


</script>