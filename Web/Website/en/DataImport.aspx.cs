//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Xml;
//using Excel = Microsoft.Office.Interop.Excel;
using System.Data.Common;
using System.Data.OleDb;


namespace ns_DataImport
{
    public partial class DataImport : System.Web.UI.Page
    {
        String schemaPath = "C:\\VRMSchemas_V1.8.3\\";
        static DataSet ds;
        static DataTable dtMaster;
        //code added for BCS - start
        DataView dvZone;
        DataTable dtZone;
        myVRMNet.NETFunctions obj;
        DataSet dsUsers = new DataSet();
        DataView dvUsers;
        DataTable dtUsers = new DataTable();
        DataTable dtEpt = new DataTable();
        DataTable dtRoom = new DataTable();
        DataTable dtMCU = new DataTable();
		DataTable dtBasic = new DataTable();
        DataTable dtParticipants = new DataTable();
        DataTable dtRoomparty = new DataTable();
        DataTable dtAVParameters = new DataTable();
        DataTable dtRecurrencePattern = new DataTable();
        DataTable dtCustomOptions = new DataTable();//ZD 102909
        DataTable dtConferecne = new DataTable();
        //private IUserDao m_IuserDao;
        MyVRMNet.Util utilObj;
        protected String strRequest = ""; //ZD 100456

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("DataImport.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
            
//            if (IsPostBack)
//                //tblDataLoading.Visible = false;
            newErrGrid.Visible = false;
            InfoGrid.Visible = false; //ZD 104091
			//ZD 100456
            if (Request.QueryString["t"] != null)
                strRequest = Request.QueryString["t"];

            if (strRequest == "1")
            {
                btnImportTier1.Enabled = false;
                btnImportTier2.Enabled = false;
                btnImportDepartment.Enabled = false;
                btnImportConferences.Enabled = false;
                btnImportDefaultCSSXML.Enabled = false;

                //ZD 102029
                trTier1.Visible = false;
                trTier2.Visible = false;
                trDept.Visible = false;
                trCSS.Visible = false;

                lblMCuNo.Text = "1";
                lblEptNo.Text = "2";
                lblRmNo.Text = "3";
                lblUsrNo.Text = "4";
                lblConfNo.Text = "5";
                //btnImportDepartment.Enabled = false;
                //btnImportDepartment.Enabled = false;
            }
            
            //ZD 102029
            if (Application["ConfImport"] != null && Application["ConfImport"].ToString().ToLower() == "yes")
                btnImportConferences.Enabled = true;
            else
                trConf.Visible = false;

			//FB 2519 start
            if(btnImportTier1.Enabled==true)
                btnImportTier1.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportTier1.Attributes.Add("Class", "btndisable");

            if (btnImportTier1.Enabled == true)
                btnImportTier2.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportTier2.Attributes.Add("Class", "btndisable");

            if (btnImportDepartment.Enabled == true)
                btnImportDepartment.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportDepartment.Attributes.Add("Class", "btndisable");

            if (btnImportRooms.Enabled == true)
                btnImportRooms.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportRooms.Attributes.Add("Class", "btndisable");

            if (btnImportUsers.Enabled == true)
                btnImportUsers.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportUsers.Attributes.Add("Class", "btndisable");

            if (btnImportmcu.Enabled == true)
                btnImportmcu.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportmcu.Attributes.Add("Class", "btndisable");

            if (btnImportEndpoints.Enabled == true)
                btnImportEndpoints.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportEndpoints.Attributes.Add("Class", "btndisable");

            if (btnImportConferences.Enabled == true)
                btnImportConferences.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportConferences.Attributes.Add("Class", "btndisable");

            if (btnImportDefaultCSSXML.Enabled == true)
                btnImportDefaultCSSXML.Attributes.Add("Class", "altShortBlueButtonFormat");
            else
                btnImportDefaultCSSXML.Attributes.Add("Class", "btndisable");
            
            //FB 2519 End

        }

        protected void ImportTier1s(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtRoom = Session["dtRoomtable"] as DataTable;
                dtTemp = dtRoom.DefaultView.ToTable();//(true, new String[] { dtRoom.Columns["Room Name"].ColumnName, dtRoom.Columns["Tier One"].ColumnName, dtRoom.Columns["Tier Two"].ColumnName, dtRoom.Columns["Floor"].ColumnName, dtRoom.Columns["Room"].ColumnName, dtRoom.Columns["Room Phone"].ColumnName, dtRoom.Columns["Room Administrator"].ColumnName, dtRoom.Columns["Projector Available"].ColumnName, dtRoom.Columns["Media (None Audio-only Audio-Video)"].ColumnName, dtRoom.Columns["Endpoint name"].ColumnName, dtRoom.Columns["Time Zone"].ColumnName, dtRoom.Columns["Department"].ColumnName });
                Tier1 objTier1 = new Tier1(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                int cnt = 0;
                objTier1.Process(ref cnt);
                errLabel.Text = cnt.ToString()+ obj.GetTranslatedText("Top Tier(s) are imported successfully!");
                btnImportTier1.Enabled = false;
                btnImportTier1.Attributes.Add("Class", "btndisable");//FB 2519
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void GenerateDataTable(Object sender, EventArgs e)
        {
            try
            {
                if (fleMasterCSV.HasFile)
                {

                    string sourcefile = fleMasterCSV.FileName;
                    fleMasterCSV.SaveAs(Server.MapPath(".") + @"\upload\\" + sourcefile);
                    
                }
               ds = CreateDataTableFromCSV();
               if (ds != null)//ZD 101730 start
                errLabel.Text = obj.GetTranslatedText("Data Table has been generated successfully!");

               txtFile.Value = obj.GetTranslatedText("No file selected");
               txtFileinput.Value = obj.GetTranslatedText("No file selected");//ZD 101730 End
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportTier2s(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtRoom = Session["dtRoomtable"] as DataTable;
                dtTemp = dtRoom.DefaultView.ToTable();//(true, new string[] { dtRoom.Columns["Room Name"].ColumnName, dtRoom.Columns["Tier One"].ColumnName, dtRoom.Columns["Tier Two"].ColumnName, dtRoom.Columns["Floor"].ColumnName, dtRoom.Columns["Room"].ColumnName, dtRoom.Columns["Room Phone"].ColumnName, dtRoom.Columns["Room Administrator"].ColumnName, dtRoom.Columns["Projector Available"].ColumnName, dtRoom.Columns["Media (None Audio-only Audio-Video)"].ColumnName, dtRoom.Columns["Endpoint name"].ColumnName, dtRoom.Columns["Time Zone"].ColumnName, dtRoom.Columns["Department"].ColumnName });
                Tier2 objTier2 = new Tier2(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                int cnt = 0;
                objTier2.Process(ref cnt);
                errLabel.Text = cnt.ToString() + " " +  obj.GetTranslatedText("Middle Tier(s) are imported successfully!");
                btnImportTier2.Enabled = false;
                btnImportTier2.Attributes.Add("Class", "btndisable");//FB 2519
                
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportDepartments(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                dtRoom = Session["dtRoomtable"] as DataTable;//FB 2519
                dtTemp = dtRoom.DefaultView.ToTable();//(true, new string[] { dtMaster.Columns["Department"].ColumnName });//FB 2519
                Department objDepartment = new Department(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                int cnt = 0;
                objDepartment.Process(ref cnt);
                errLabel.Text = cnt.ToString() + " "+ obj.GetTranslatedText("Department(s) are imported successfully!");
                btnImportDepartment.Enabled = false;
                btnImportDepartment.Attributes.Add("Class", "btndisable");//FB 2519
                
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportRooms(object sender, EventArgs e)
        {
            try
            {
                //ZD 100456
                DataTable dterror = new DataTable();
                DataTable dtInformation = new DataTable(); //ZD 104091
                dterror.Columns.Add("Row No", typeof(string));
                dterror.Columns.Add("Reason", typeof(string));
				//ZD 104091
                dtInformation.Columns.Add("Row No", typeof(string));
                dtInformation.Columns.Add("Reason", typeof(string));

                bool alluserimport = true;

                DataTable dtTemp = new DataTable();
                dtRoom = Session["dtRoomtable"] as DataTable;
                Session.Remove("dtRoomtable");
                if (dtRoom != null)
                {
                    errLabel.Text = "";
                    dtTemp = dtRoom.DefaultView.ToTable();//(true, new string[] { dtRoom.Columns["Room Name"].ColumnName, dtRoom.Columns["Tier One"].ColumnName, dtRoom.Columns["Tier Two"].ColumnName, dtRoom.Columns["Floor"].ColumnName, dtRoom.Columns["Room"].ColumnName, dtRoom.Columns["Room Phone"].ColumnName, dtRoom.Columns["Room Administrator"].ColumnName, dtRoom.Columns["Projector Available"].ColumnName, dtRoom.Columns["Media (None Audio-only Audio-Video)"].ColumnName, dtRoom.Columns["Endpoint name"].ColumnName, dtRoom.Columns["Time Zone"].ColumnName , dtRoom.Columns["Department"].ColumnName });
                    Room objRoom = new Room(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                    int cnt = 0;
                    objRoom.Process(ref cnt, ref alluserimport, ref dterror, ref dtInformation);//ZD 100456//ZD 104091
                    errLabel.Text = cnt.ToString() + " " + obj.GetTranslatedText("Room(s) are imported successfully!");
                    btnImportRooms.Enabled = false;
                    btnImportRooms.Attributes.Add("Class", "btndisable");//FB 2519
                    //ZD 100456
                    String roomxmlPath = "";
                    roomxmlPath = HttpContext.Current.Request.MapPath(".").ToString() + "\\" + Session["RoomXmlPath"].ToString();

                    if (File.Exists(roomxmlPath))
                    {
                        if (obj.WaitForFile(roomxmlPath))
                            File.Delete(roomxmlPath);
                    }

                    //ZD 100456
                    if (alluserimport == false && dterror.Rows.Count > 0)//ZD 104091
                    {
                        newErrGrid.Visible = true;
                        newErrGrid.DataSource = dterror;
                        newErrGrid.DataBind();
                        errLabel.Text = obj.GetTranslatedText("Failed to create Room's are listed below");
                        dterror.Clear();
                        ds = null;
                    }
                    //ZD 104091
                    if (alluserimport == false && dtInformation.Rows.Count > 0)
                    {
                        InfoGrid.Visible = true;
                        InfoGrid.DataSource = dtInformation;
                        InfoGrid.DataBind();
                        lblinfo.Text = obj.GetTranslatedText("List of rooms are imported without images."); 
                        dtInformation.Clear();
                        ds = null;
                    }
                }
                else //ZD 102029 
                    errLabel.Text = obj.GetTranslatedText("Please Upload the File");
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        protected void ImportUsers(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                DataTable dterror = new DataTable();
                dterror.Columns.Add("Row No", typeof(string));
                dterror.Columns.Add("Reason", typeof(string));
                bool alluserimport = true;
                dtUsers = Session["dtUserstable"] as DataTable;
                Session.Remove("dtUserstable");
                if (dtUsers != null)
                {
                    errLabel.Text = "";
                    dtTemp = dtUsers.DefaultView.ToTable();//(true, new string[] { dtUsers.Columns["First Name"].ColumnName, dtUsers.Columns["Last Name"].ColumnName, dtUsers.Columns["Outlook/Notes"].ColumnName, dtUsers.Columns["Initial Password"].ColumnName, dtUsers.Columns["Email"].ColumnName, dtUsers.Columns["Timezone"].ColumnName, dtUsers.Columns["User Role (User/Admin/Super Adminetc)"].ColumnName, dtUsers.Columns["Assigned MCU (only in case of multiple)"].ColumnName });
                    User objUser = new User(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                    int cnt = 0;
                    objUser.Process(ref cnt, ref alluserimport, ref dterror);
                    errLabel.Text = cnt.ToString() + " " + obj.GetTranslatedText("User(s) are imported successfully!");
                    btnImportUsers.Enabled = false;
                    btnImportUsers.Attributes.Add("Class", "btndisable");//FB 2519

                    if (alluserimport == false)
                    {
                        newErrGrid.Visible = true;
                        newErrGrid.DataSource = dterror;
                        newErrGrid.DataBind();
                        errLabel.Text = obj.GetTranslatedText("Failed to create User's are listed below");
                        dterror.Clear();
                        ds = null;
                    }
                }
                else //ZD 102029 
                    errLabel.Text = obj.GetTranslatedText("Please Upload the File");
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        
        protected void Importmcu(object sender, EventArgs e)
        {
            try
            {
                DataTable dtTemp = new DataTable();
                DataTable dterror = new DataTable(); //ZD 100456
                dterror.Columns.Add("Row No", typeof(string));
                dterror.Columns.Add("Reason", typeof(string));
                bool alluserimport = true;
                dtMCU = Session["dtMCUtable"] as DataTable;
                Session.Remove("dtMCUtable");
                if (dtMCU != null)
                {
                    dtTemp = dtMCU.DefaultView.ToTable();//(true, new string[] { dtMCU.Columns["Name"].ColumnName, dtMCU.Columns["Login"].ColumnName, dtMCU.Columns["Password"].ColumnName, dtMCU.Columns["Vendor Type"].ColumnName, dtMCU.Columns["Timezone"].ColumnName, dtMCU.Columns["Control Port IP Address"].ColumnName, dtMCU.Columns["Firmware version"].ColumnName });
                    mcu objmcu = new mcu(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                    int cnt = 0;
                    objmcu.Process(ref cnt, ref alluserimport, ref dterror); //ZD 100456
                    errLabel.Text = cnt.ToString() + " " + obj.GetTranslatedText("MCU(s) are imported successfully!");
                    btnImportmcu.Enabled = false;
                    btnImportmcu.Attributes.Add("Class", "btndisable");//FB 2519

                    if (alluserimport == false)
                    {
                        newErrGrid.Visible = true;
                        newErrGrid.DataSource = dterror;
                        newErrGrid.DataBind();
                        errLabel.Text = obj.GetTranslatedText("Failed to create/update MCU's are listed below");
                        dterror.Clear();
                        ds = null;
                    }
                }
                else //ZD 102029 
                    errLabel.Text = obj.GetTranslatedText("Please Upload the File");
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }

        protected void ImportEndpoints(object sender, EventArgs e) 
        {
            try
            {
                //ZD 100456
                DataTable dterror = new DataTable(); 
                dterror.Columns.Add("Row No", typeof(string));
                dterror.Columns.Add("Reason", typeof(string));
                bool alluserimport = true;

                DataTable dtTemp = new DataTable();
                dtEpt = Session["dtEpttable"] as DataTable;
                Session.Remove("dtEpttable");
                if (dtEpt != null)
                {
                    errLabel.Text = "";
                    dtTemp = dtEpt.DefaultView.ToTable();//(true, new string[] {dtEpt.Columns["Name"].ColumnName, dtEpt.Columns["Firmware Version"].ColumnName, dtEpt.Columns["Model"].ColumnName, dtEpt.Columns["Address"].ColumnName, dtEpt.Columns["Address Type"].ColumnName, dtEpt.Columns["Password"].ColumnName, dtEpt.Columns["Preferred Bandwidth(kbps)"].ColumnName, dtEpt.Columns["Preferred Dialing Option"].ColumnName, dtEpt.Columns["MCU Assignment"].ColumnName, dtEpt.Columns["Located outside the network"].ColumnName, dtEpt.Columns["SIP Address"].ColumnName });
                    Endpoint objEndpoint = new Endpoint(1, dtTemp, Application["MyVRMServer_ConfigPath"].ToString());
                    int cnt = 0;
                    objEndpoint.Process(ref cnt, ref alluserimport, ref dterror); //ZD 100456
                    errLabel.Text = cnt.ToString() + " " + obj.GetTranslatedText("EndPoint(s) are imported successfully!");
                    btnImportEndpoints.Enabled = false;
                    btnImportEndpoints.Attributes.Add("Class", "btndisable");//FB 2519

                    if (alluserimport == false)
                    {
                        newErrGrid.Visible = true;
                        newErrGrid.DataSource = dterror;
                        newErrGrid.DataBind();
                        errLabel.Text = obj.GetTranslatedText("Failed to create/update Endpoint's are listed below");
                        dterror.Clear();
                        ds = null;
                    }
                }
                else //ZD 102029 
                    errLabel.Text = obj.GetTranslatedText("Please Upload the File");
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        
        public void ImportConferences(object sender, EventArgs e)
        {
            if (ds!=null)
            {
                try
                {
                    errLabel.Text = "";
                    DataTable dtTemp = new DataTable();
                    DataTable dtTemp1 = new DataTable();
                    DataTable dtTemp2 = new DataTable();
                    DataTable dtTemp3 = new DataTable();
                    DataTable dttemp4 = new DataTable();
                    DataTable dttemp5 = new DataTable(); //ZD 102909
                    DataTable dterror = new DataTable();
                    DataGrid err = new DataGrid();

                    dterror.Columns.Add("Row No", typeof(string));
                    dterror.Columns.Add("Reason", typeof(string));
                    bool allConferenceimport = true;

                    //dterror.Columns.Add("Conf_No", typeof(int));
                    //dterror.Columns.Add("Conference name", typeof(string));
                    //dterror.Columns.Add("Reason", typeof(string));
                    dtConferecne = Session["dtConferecnetable"] as DataTable;
                    dtParticipants = Session["dtParticipantstable"] as DataTable;
                    dtRoomparty = Session["dtRoompartytable"] as DataTable;
                    dtAVParameters = Session["dtAVParameterstable"] as DataTable;
                    dtRecurrencePattern = Session["dtRecurrencePatterntable"] as DataTable;
                    dtCustomOptions = Session["dtCustomOptionstable"] as DataTable;//ZD 102909
                    dtTemp = dtConferecne.DefaultView.ToTable();
                    dtTemp1 = dtRecurrencePattern.DefaultView.ToTable();
                    dtTemp2 = dtParticipants.DefaultView.ToTable();
                    dtTemp3 = dtRoomparty.DefaultView.ToTable();
                    dttemp4 = dtAVParameters.DefaultView.ToTable();
                    dttemp5 = dtCustomOptions.DefaultView.ToTable();//ZD 102909
                    string configPath = Application["MyVRMServer_ConfigPath"].ToString();
                    Conference objConference = new Conference(1, dtTemp, dtTemp1, dtTemp2, dtTemp3, dttemp4, dttemp5, Application["MyVRMServer_ConfigPath"].ToString());//ZD 102909

                    int cnt = 0;
                    String[] value = objConference.Process(ref cnt, ref allConferenceimport, ref dterror);

                    if (allConferenceimport == false)
                    {
                        newErrGrid.Columns[0].HeaderText = "Conf No";
                        newErrGrid.Visible = true;
                        newErrGrid.DataSource = dterror;
                        newErrGrid.DataBind();
                        errLabel.Text = obj.GetTranslatedText("Failed to create Conference's are listed below");
                        dterror.Clear();
                        ds = null;
                    }
                    else if (value[0] == "yes")
                    {
                        newErrGrid.Columns[0].HeaderText = "Conf No";//ZD 102909
                        newErrGrid.Visible = true;
                        newErrGrid.DataSource = dterror;
                        newErrGrid.DataBind();
                        errLabel.Text = obj.GetTranslatedText("Conference(s) are imported successfully!");
                        btnImportConferences.Enabled = false;
                        btnImportConferences.Attributes.Add("Class", "btndisable");
                        dterror.Clear();
                        ds = null;
                    }
                    else if (value[0] == "no")
                    {
                        errLabel.Text = obj.GetTranslatedText("Excel Sheet Empty");
                        ds = null;
                    }
                    
                }
                catch (Exception ex)
                {
                    errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
                    ds = null;
                }
            }
            else
                errLabel.Text = obj.GetTranslatedText("Please Upload the File");
        }

        protected DataSet CreateDataTableFromCSV()
        {
            
            StreamReader sr = null;
            try
            {
                ds = new DataSet();
                ns_Logger.Logger log = new ns_Logger.Logger();
                String ExternalFileName = fleMasterCSV.FileName;
                ExternalFileName = Server.MapPath(".") + @"\upload\\" + ExternalFileName;
                String[] excelSheets = GetExcelSheetNames(ExternalFileName);
                if (excelSheets != null)
                {
                    for (int j = 0; j < excelSheets.Length; j++)
                    {
                        DataTable dt = new DataTable();
                        String shtname = excelSheets[j].Replace("$", "").Replace("'", "");

                        //if (shtname == "General Info")
                        //    continue;

                        switch (shtname)
                        {
                            case "Room Data":
                            case "Data Room":
                            case "Les donn�es sur les chambres":
                                dtRoom = ds.Tables[j];
                                Session["dtRoomtable"] = dtRoom;
                                break;
                            case "MCU Data":
                            case "MCU donn�es":
                                dtMCU = ds.Tables[j];
                                Session["dtMCUtable"] = dtMCU;
                                break;
                            case "Endpoint Data":
                            case "Punto final de datos":
                            case "Endpoint donn�es":
                                dtEpt = ds.Tables[j];
                                Session["dtEpttable"] = dtEpt;
                                break;
                            case "User Data":
                            case "datos del usuario":
                            case "Les donn�es de l_utilisateur":
                                dtUsers = ds.Tables[j];
                                Session["dtUserstable"] = dtUsers;
                                break;
                            case "Basic Details":
                                GenerateTableSchemaConference(ref dt);
                                dtConferecne = ds.Tables[j];
                                Session["dtConferecnetable"] = dtConferecne;
                                break;
                            case "Participants":
                                GenerateTableSchemaConference1(ref dt);
                                dtParticipants = ds.Tables[j];
                                Session["dtParticipantstable"] = dtParticipants;
                                break;
                            case "Rooms":
                                GenerateTableSchemaConference2(ref dt);
                                dtRoomparty = ds.Tables[j];
                                Session["dtRoompartytable"] = dtRoomparty;
                                break;
                            case "AV Parameters":
                                GenerateTableSchemaConference3(ref dt);
                                dtAVParameters = ds.Tables[j];
                                Session["dtAVParameterstable"] = dtAVParameters;
                                break;
                            case "Recurrence Pattern":
                                GenerateTableSchemaConference4(ref dt);
                                dtRecurrencePattern = ds.Tables[j];
                                Session["dtRecurrencePatterntable"] = dtRecurrencePattern;
                                break;
                            case "Entity Code": //ZD 102909
                                GenerateTableSchemaConference5(ref dt);
                                dtCustomOptions = ds.Tables[j];
                                Session["dtCustomOptionstable"] = dtCustomOptions;
                                break;
                            default:
                                break;

                        }


                        btnImportConferences.Enabled = true;
                        ds.Tables.Add(dt);
                    }
                    return ds;
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Excel is not Uploaded");
                    txtFile.Value = obj.GetTranslatedText("No file selected");
                    txtFileinput.Value = obj.GetTranslatedText("No file selected");//ZD 101730 End
                    return null;
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
                return null;
            }
            finally
            {
                if (sr != null)
                    sr.Close();
                sr = null;
            }
        }

     
       

        //code added for BCS - start
        protected String GetConferenceTimeZoneID(String tZoneStr)
        {
            
            DataSet dsZone = new DataSet();
            myVRMNet.NETFunctions obj;
            String tZoneID = "33";
            String tZone = "";
            String[] tZoneArr = null;
             String[] tZoneGMTArr = null;
            try
            {
                //code added for BCS - start
                //Time Zone

                String zoneInXML = "<GetTimezones><UserID>11</UserID></GetTimezones>";
                String zoneOutXML;

                tZone = tZoneStr;

                if (dtZone == null)
                {
                    if (dsZone.Tables.Count == 0)
                    {
                        obj = new myVRMNet.NETFunctions();

                        zoneOutXML = obj.CallMyVRMServer("GetTimezones", zoneInXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                       
                        obj = null;

                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(zoneOutXML);
                        String sel = xmldoc.SelectSingleNode("//Timezones/selected").InnerText;
                        XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");
                        if (nodes.Count > 0)
                        {
                            XmlTextReader xtr;

                            foreach (XmlNode node in nodes)
                            {
                                xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                                dsZone.ReadXml(xtr, XmlReadMode.InferSchema);
                            }

                            if (dsZone.Tables.Count > 0)
                            {
                                dvZone = new DataView(dsZone.Tables[0]);
                                dtZone = dvZone.Table;
                            }
                        }
                        //code added for BCS - start
                    }
                }

                if (tZone != "")
                {

                    tZoneArr = tZone.Split(' ');

                    foreach (String s in tZoneArr)
                    {
                        if (s.ToString().ToUpper() == "GMT")
                        {
                            tZoneID = "31";
                            break;
                        }
                        else if (s.Contains("GMT"))
                        {
                            if (dtZone.Rows.Count > 0)
                            {
                                foreach (DataRow row in dtZone.Rows)
                                {
                                    if (row["timezoneName"].ToString().Contains(s))
                                    {
                                        tZoneID = row["timezoneID"].ToString();
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }

            return tZoneID;
        }

        protected String GetUsers(String firstName, String lastName)
        {

            string userid = "11";

            if (dsUsers.Tables.Count == 0)
            {
                obj = new myVRMNet.NETFunctions();

                String inXML = "";
                inXML += "<login>";
                inXML += "  <userID>11</userID>";
                inXML += "  <user>";
                inXML += "      <firstName>" + firstName + "</firstName>";
                inXML += "      <lastName>" + lastName + "</lastName>";
                inXML += "      <email></email>";
                inXML += "  </user>";
                inXML += "</login>";
                String outXML = obj.CallCOM("SearchUser", inXML, Application["COM_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                //String userName = xmldoc.SelectSingleNode("/oldUser/userName/firstName").InnerText + " " + xmldoc.SelectSingleNode("/oldUser/userName/lastName").InnerText;

                XmlNodeList nodes = xmldoc.SelectNodes("//users/user");
                if (nodes.Count > 0)
                {
                    XmlTextReader xtr;

                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        dsUsers.ReadXml(xtr, XmlReadMode.InferSchema);
                    }

                    if (dsUsers.Tables.Count > 0)
                    {
                        dvUsers = new DataView(dsUsers.Tables[0]);
                        dtUsers = dvUsers.Table;
                    }
                }
            }

            if (dtUsers.Rows.Count > 0)
            {
                foreach (DataRow row in dtUsers.Rows)
                {
                    //String name = row["UserFirstName"].ToString() + "," + row["UserLastName"].ToString();
                    String name = row["FirstName"].ToString() + "," + row["LastName"].ToString();
                    if (name.ToLower() == firstName.ToLower() + "," + lastName.ToLower())
                    {
                        userid = row["userID"].ToString();
                        break;
                    }
                }
            }

            return userid;

        }
        //code added for BCS - end

        //Code added for Organization/CSS Module  -- Start
        #region ImportDefaultCSSXML
        protected void ImportDefaultCSSXML(object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML = "<SetDefaultCSSXML>";
                myVRMNet.NETFunctions obj = new myVRMNet.NETFunctions();
                inXML += obj.OrgXMLElement();//Organization Module 
                myVRMNet.ImageUtil imageUtil = new myVRMNet.ImageUtil();
                string textXML = "";
                if (cssXMLFileUpload.PostedFile.FileName != "")
                    textXML = imageUtil.ConvertImageToBase64(cssXMLFileUpload.PostedFile.FileName);
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Please Upload the File");
                    errLabel.Visible = true;
                }

                inXML += "<DefaultCSSXml>" + textXML + "</DefaultCSSXml>";
                inXML += "</SetDefaultCSSXML>";

                String outXML = obj.CallMyVRMServer("SetDefaultCSSXML", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    this.errLabel.Text = obj.ShowErrorMessage(outXML);
                    this.errLabel.Visible = true;
                }
                else
                {
                    errLabel.Text = obj.GetTranslatedText("Default CSS XML has been imported successfully!");
                   // btnImportDefaultCSSXML.Enabled = false;
                }
                txtFile.Value = obj.GetTranslatedText("No file selected");
                txtFileinput.Value = obj.GetTranslatedText("No file selected");//ZD 101730 End
                
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace + "<br>" + ex.Message;
            }
        }
        #endregion
        //Code added for Organization/CSS Module -- End

        private String[] GetExcelSheetNames(string excelFile)
        {
            OleDbConnection objConn = null;
            System.Data.DataTable dt = null;

            try
            {
                // Connection String. Change the excel file to the file you
                // will search.
                //ZD 100456
                String connString = "";
                String[] excelSheets = null;
                //ZD 103896
                try
                {
                    connString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                "Data Source=" + excelFile + ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=1\"";


                    // Create connection object by using the preceding connection string.
                    objConn = new OleDbConnection(connString);

                    // Open connection with the database.
                    objConn.Open();
                    // Get the data table containg the schema guid.
                    dt = objConn.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);

                    if (dt == null)
                        return null;

                    excelSheets = new String[dt.Rows.Count];
                    int i = 0;

                    // Add the sheet name to the string array.
                    foreach (DataRow row in dt.Rows)
                    {
                        excelSheets[i] = row["TABLE_NAME"].ToString();
                        i++;
                    }


                    for (int j = 0; j < excelSheets.Length; j++)
                    {
                        dt = new DataTable();
                        //ZD 101714
                        string strName = excelSheets[j].ToString();
                        String tempName = "";

                        if (strName.IndexOf("Basic Details") > -1 || strName.IndexOf("Recurrence Pattern") > -1 || strName.IndexOf("Participants") > -1
                            || strName.IndexOf("Rooms") > -1 || strName.IndexOf("AV Parameters") > -1 || strName.IndexOf("Entity Code") > -1) //ZD 102909
                            tempName = excelSheets[j].ToString().Replace("'", "") + "A5:P65536";
                        else
                            tempName = excelSheets[j].ToString();

                        new OleDbDataAdapter("SELECT * FROM [" + tempName + "]", objConn).Fill(dt);
                        ds.Tables.Add(dt);
                    }
                }
                catch (Exception ex)
                {
                    if (objConn != null)
                    {
                        objConn.Close();
                        objConn.Dispose();
                    }

                    dt = getDataTableFromExcel(excelFile);

                    excelSheets = new String[1];
                    excelSheets[0] = dt.TableName;
                    ds.Tables.Add(dt);
                }

                return excelSheets;
            }
            catch (Exception ex)
            {
                return null;
            }
            finally
            {
                // Clean up.
                if (objConn != null)
                {
                    objConn.Close();
                    objConn.Dispose();
                }
                if (dt != null)
                {
                    dt.Dispose();
                }
            }
        }

        //ZD 103896
        #region getDataTableFromExcel 
        //ZD 104862
        // This method is particularly for Export Excel generated from Manage DataImport /Export Page
        // In this page, used third party tool to avoid empty column creation.
        // The export excel created by this third party tool is not supported for "GetOleDbSchemaTable"
        public static DataTable getDataTableFromExcel(string path)
        {
            try
            {
                using (var pck = new OfficeOpenXml.ExcelPackage())
                {
                    using (var stream = File.OpenRead(path))
                    {
                        pck.Load(stream);
                    }
                    var ws = pck.Workbook.Worksheets[1];

                    int totalRows = ws.Dimension.End.Row;
                    int totalCols = ws.Dimension.End.Column;
                    DataTable dt = new DataTable(ws.Name);
                    DataRow dr = null;
                    Boolean isHeadingCreated = false;
                    for (int i = 1; i <= totalRows; i++)
                    {
                        //ZD 104862 - Start
                        if (i > 1 && isHeadingCreated == true) dr = dt.Rows.Add();
                        for (int j = 1; j <= totalCols; j++)
                        {
                            //if (i == 1)
                            if (ws.Cells[i, 1].Value != null && ws.Cells[i, 1].Value.ToString() == "id")
                            {
                                isHeadingCreated = true;
                                if (ws.Cells[i, j].Value != null)
                                    dt.Columns.Add(ws.Cells[i, j].Value.ToString());
                                else
                                    dt.Columns.Add("");
                            }
                            else if(isHeadingCreated)
                            {
                                if (ws.Cells[i, j].Value != null && dr != null)

                                    dr[j - 1] = ws.Cells[i, j].Value.ToString();
                                else
                                    dt.Columns.Add("");
                            }
                        }
                        //ZD 104862 - End
                    }
                    return dt;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        protected void GenerateTableSchemaConference(ref DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("Conf_No*")) dt.Columns.Add("Conf_No*");
                if (!dt.Columns.Contains("Conference Name*")) dt.Columns.Add("Conference Name*");
                if (!dt.Columns.Contains("Conference Host")) dt.Columns.Add("Conference Host");
                if (!dt.Columns.Contains("Start Date* [MM/DD/YYYY]")) dt.Columns.Add("Start Date* [MM/DD/YYYY]");
                if (!dt.Columns.Contains("Start Time* [hh:mm tt]")) dt.Columns.Add("Start Time* [hh:mm tt]");
                if (!dt.Columns.Contains("Duration (Minutes)")) dt.Columns.Add("Duration (Minutes)");
                if (!dt.Columns.Contains("Setup Duration (mins)")) dt.Columns.Add("Setup Duration (mins)");
                if (!dt.Columns.Contains("Start Time* [hh:mm tt]")) dt.Columns.Add("Start Time* [hh:mm tt]");
                if (!dt.Columns.Contains("Duration (Minutes)")) dt.Columns.Add("Duration (Minutes)");
                if (!dt.Columns.Contains("Teardown Duration (mins)")) dt.Columns.Add("Teardown Duration (mins)");
                if (!dt.Columns.Contains("Password")) dt.Columns.Add("Password");
                if (!dt.Columns.Contains("Conference Type")) dt.Columns.Add("Conference Type");
                if (!dt.Columns.Contains("Time Zone*")) dt.Columns.Add("Time Zone*");
                if (!dt.Columns.Contains("Description")) dt.Columns.Add("Description");
                if (!dt.Columns.Contains("Public (Yes/No)")) dt.Columns.Add("Public (Yes/No)");
                if (!dt.Columns.Contains("Open For Registration (Yes/No)")) dt.Columns.Add("Open For Registration (Yes/No)");
                if (!dt.Columns.Contains("Recurrance (Yes/No)")) dt.Columns.Add("Recurrance (Yes/No)");

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void GenerateTableSchemaConference1(ref DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("Conf_No*")) dt.Columns.Add("Conf_No*");
                if (!dt.Columns.Contains("First Name")) dt.Columns.Add("First Name");
                if (!dt.Columns.Contains("Last Name")) dt.Columns.Add("Last Name");
                if (!dt.Columns.Contains("Email")) dt.Columns.Add("Email");
            }
            catch (Exception Ex)
            {
                throw Ex;
            }

        }
        protected void GenerateTableSchemaConference2(ref DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("Conf_No*")) dt.Columns.Add("Conf_No*");
                if (!dt.Columns.Contains("Room Name")) dt.Columns.Add("Room Name");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void GenerateTableSchemaConference3(ref DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("Conf_No*")) dt.Columns.Add("Conf_No*");
                if (!dt.Columns.Contains("Restrict Network Access to")) dt.Columns.Add("Restrict Network Access to");
                if (!dt.Columns.Contains("Restrict Usage to")) dt.Columns.Add("Restrict Usage to");
                if (!dt.Columns.Contains("Max Video ports")) dt.Columns.Add("Max Video ports");
                if (!dt.Columns.Contains("Max Audio Port")) dt.Columns.Add("Max Audio Port");
                if (!dt.Columns.Contains("Video Codecs")) dt.Columns.Add("Video Codecs");
                if (!dt.Columns.Contains("Audio Codecs")) dt.Columns.Add("Audio Codecs");
                if (!dt.Columns.Contains("Dual Stram Mode (Yes/ No)")) dt.Columns.Add("Dual Stram Mode (Yes/ No)");
                if (!dt.Columns.Contains("Encryption (Yes /No)")) dt.Columns.Add("Encryption (Yes /No)");
                if (!dt.Columns.Contains("Single Dial in (Yes /No)")) dt.Columns.Add("Single Dial in (Yes /No)");
                if (!dt.Columns.Contains("Maximum Line Rate")) dt.Columns.Add("Maximum Line Rate");
            }
            catch (Exception EX)
            {
                throw EX;
            }
        }
        protected void GenerateTableSchemaConference4(ref DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("Conf_No*")) dt.Columns.Add("Conf_No*");
                if (!dt.Columns.Contains("Recurrance Pattern String")) dt.Columns.Add("Recurrance Pattern String");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        //ZD 102909 - Start
        #region EntityCode
        protected void GenerateTableSchemaConference5(ref DataTable dt)
        {
            try
            {
                if (!dt.Columns.Contains("Conf_No*")) dt.Columns.Add("Conf_No*");
                if (!dt.Columns.Contains("Entity Code")) dt.Columns.Add("Entity Code");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        //ZD 102909 - End
     }
  }