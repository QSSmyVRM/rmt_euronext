<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" Inherits="ns_EndpointList.EndpointList" %><%--ZD 100170--%>
<%--FB 2779 Starts--%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<%--FB 2779 Ends--%>
<head id="Head1" runat="server">

<script language="javascript">
//ZD 100604 start
var img = new Image();
img.src = "../en/image/wait1.gif";
//ZD 100604 End
function ViewBridgeDetails(bid)
{
    url = "BridgeDetailsViewOnly.aspx?hf=1&bid=" + bid;
    window.open(url, "BrdigeDetails", "width=900,height=800,resizable=yes,scrollbars=yes,status=no");
    return false;
}
function CheckSelection(obj)
{
//      alert(obj.type);
      if (obj.tagName == "INPUT" && obj.type == "radio")
            for (i=0; i<document.frmEndpoints.elements.length;i++)
            {
                var obj1 = document.frmEndpoints.elements[i];
                if (obj1.id != obj.id)
                    obj1.checked = false;
            }
}
//ZD 100176 start
function DataLoading(val) {    
    if (val == "1")
        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
    else
        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
}
//ZD 100176 End
</script>
    <title>Manage Endpoints</title>
</head>
<div id="dataLoadingDIV" name="dataLoadingDIV" align="center" style="display:none">
    <img border='0' src='image/wait1.gif' alt='Loading..' />
</div><%--ZD 100678--%>
<body>
    <form id="frmEndpoints" runat="server" method="post">        
      <%--ZD 101022 start--%>
        <asp:ScriptManager ID="scpMgrUI" runat="server" EnableScriptLocalization="true" >
		    <Scripts>                
			    <asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		    </Scripts>
	    </asp:ScriptManager> <%--ZD 101022 End--%>
        <center><table border="0" width="98%" cellpadding="2" cellspacing="2">
            <tr>
                <td align="center">
                    <h3><asp:Label id="lblHeader" runat="server" text="<%$ Resources:WebResources, EndpointList_lblHeader%>"></asp:Label></h3><br />
                     <asp:Label id="errLabel" runat="server"  cssclass="lblError"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="Left">
                    <table>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Label id="lblSubHeader1" text="<%$ Resources:WebResources, EndpointList_lblSubHeader1%>" runat="server" cssclass="subtitleblueblodtext"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
              <%--Endpoint Search--%>
             <tr>
             <td>
              <iframe id="EndpointFrame" onfocus="setTimeout('window.scrollTo(0,0);', 1000);" runat="server" width="100%" valign="top" height="625px" scrolling="no"></iframe>
             </td>
             </tr>
            <tr style="display:none">
                <td align="center">
                <asp:DataGrid ID="dgEndpointList" runat="server" AutoGenerateColumns="False" Font-Names="Verdana" Font-Size="Small"
                 Width="95%" OnItemDataBound="LoadProfiles" OnItemCreated="BindRowsDeleteMessage" CellPadding="4" GridLines="None"
                 BorderColor="blue" BorderStyle="solid" BorderWidth="1" AllowSorting="True" OnSortCommand="SortGrid"
                 OnEditCommand="EditEndpoint" OnCancelCommand="DeleteEndpoint" ShowFooter="true">
                <%--Window Dressing - Start--%>
                <SelectedItemStyle CssClass="tableBody" Font-Bold="True" />
                <EditItemStyle CssClass="tableBody" />
                <AlternatingItemStyle CssClass="tableBody" />
                <ItemStyle CssClass="tableBody" />
                <FooterStyle CssClass="tableBody" />
                <%--Window Dressing - End--%>
                    <Columns>
                        <asp:BoundColumn DataField="ID" Visible="false" HeaderText="ID" SortExpression="ID"></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Select" HeaderStyle-CssClass="tableHeader" Visible="false">
                            <ItemTemplate>
                                <asp:RadioButton ID="rdSelectEndpoint" runat="server" onclick="javascript:CheckSelection(this)" />
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn ItemStyle-HorizontalAlign="left" ItemStyle-CssClass="tableBody" HeaderStyle-HorizontalAlign="left" HeaderStyle-CssClass="tableHeader" HeaderText="Endpoint<br>Name" SortExpression="EndpointName">
                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblEPName" Text='<%#DataBinder.Eval(Container, "DataItem.EndpointName") %>'></asp:Label>
                            </ItemTemplate>
                            <FooterTemplate>
                                <br /><asp:RadioButton onclick="javascript:CheckSelection(this)" visible="true" checked="true" id="rdNewEndpoint" runat="server" text="<%$ Resources:WebResources, EndpointList_rdNewEndpoint%>"></asp:RadioButton>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="DefaultProfileName" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="DefaultProfileName" HeaderText="Default<br>Profile">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="TotalProfiles" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="TotalProfiles" HeaderText="Total<br>Profiles">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="VideoEquipment" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="VideoEquipment" HeaderText="Endpoint<br>Model">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                         <asp:BoundColumn DataField="DefaultProtocol" ItemStyle-CssClass="tableBody"  ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="DefaultProtocol" HeaderText="Video<br>Protocol">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="AddressType"  ItemStyle-CssClass="tableBody" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left" SortExpression="AddressType" HeaderText="Address<br>Type">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="Address"  ItemStyle-CssClass="tableBody" SortExpression="Address" HeaderText="Address" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="IsOutside"  ItemStyle-CssClass="tableBody" SortExpression="IsOutside" HeaderText="Outside<br>Network" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="ConnectionType"  ItemStyle-CssClass="tableBody"  SortExpression="ConnectionType" HeaderText="Dialing<br>Option" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:BoundColumn DataField="LineRate"  ItemStyle-CssClass="tableBody" SortExpression="LineRate" HeaderText="Bandwidth" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="left">
                            <HeaderStyle CssClass="tableHeader" HorizontalAlign="left"/>
                        </asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Assigned<br>MCU"  ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left" > 
                        <ItemTemplate>
                            <asp:Label ID="lblBridgeID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Bridge")%>' Visible="false"></asp:Label>
                            <asp:Label ID="lblBridgeName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.BridgeName")%>'></asp:Label>
                            <br /><asp:LinkButton Text="<%$ Resources:WebResources, EndpointList_btnViewBridgeDetails%>" runat="server" ID="btnViewBridgeDetails" Visible='<%# !DataBinder.Eval(Container, "DataItem.BridgeName").ToString().Equals("") %>'></asp:LinkButton>
                        </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Profiles" Visible="false"  ItemStyle-CssClass="tableBody" HeaderStyle-CssClass="tableHeader" ItemStyle-HorizontalAlign="Left">
                            <ItemTemplate>
                                <asp:DropDownList CssClass="altLong4SelectFormat" runat="server" ID="lstProfiles" DataTextField="ProfileName" DataValueField="ProfileID" Visible='<%#Request.QueryString["t"].ToUpper().Equals("TC") %>'></asp:DropDownList><%-- SelectedValue='<%#DataBinder.Eval(Container, "DataItem.DefaultProfileID") %>' --%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <asp:Button id="btnSubmitNew" runat="server" cssclass="altLongBlueButtonFormat" visible="true" text="<%$ Resources:WebResources, Submit%>" onclick="CreateNewEndpoint"></asp:Button>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn Visible="false" ItemStyle-CssClass="tableBody" >
                            <ItemTemplate>
                                <asp:TextBox ID="txtProfilesXML" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProfilesXML") %>'></asp:TextBox>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="ProfilesXML" Visible="false" ItemStyle-CssClass="tableBody" ></asp:BoundColumn>
                        <asp:TemplateColumn HeaderText="Actions" ItemStyle-CssClass="tableBody" >
                            <HeaderStyle CssClass="tableHeader" />
                            <ItemTemplate>
                                <asp:LinkButton text="<%$ Resources:WebResources, EndpointList_btnEdit%>" runat="server" id="btnEdit" commandname="Edit" onclientclick="DataLoading(1)"></asp:LinkButton>&nbsp;&nbsp;<%--ZD 100176--%>
                                <asp:LinkButton text="<%$ Resources:WebResources, EndpointList_btnDelete%>" runat="server" id="btnDelete" commandname="Cancel" visible="true" onclientclick="DataLoading(1)"></asp:LinkButton>&nbsp;&nbsp;<%--ZD 100176--%>
                            </ItemTemplate>
                            <FooterTemplate>
                                <b><span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EndpointList_TotalEndpoints%>" runat="server"></asp:Literal></span> <asp:Label id="lblTotalRecords" runat="server" text=""></asp:Label> </b>
                                <br />
				     <%--Added for License Modification START--%>                                
				    <b><span class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, EndpointList_LicensesRemaini%>" runat="server"></asp:Literal></span><asp:Label id="lblRemaining" runat="server" text=""></asp:Label> </b>
                            </FooterTemplate>
                        </asp:TemplateColumn>
                    </Columns>
                </asp:DataGrid>
                    <asp:Label id="lblNoEndpoints" runat="server" text="<%$ Resources:WebResources, EndpointList_lblNoEndpoints%>" visible="False" cssclass="lblError"></asp:Label>
                </td>
            </tr>
            <tr style="display:none">
                <td>
                    <asp:Table ID="tblPage" Visible="false" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server">
                            <asp:TableCell ID="TableCell1" Font-Bold="True" Font-Names="Verdana" Font-Size="Small" ForeColor="Blue" runat="server">Pages: </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server"></asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                </td>
            </tr>
            <tr style="display:none">
                <td align="Left">
                    <table>
                        <tr>
                            <td>&nbsp;</td>
                            <td>
                                <SPAN class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, EndpointList_SearchEndpoint%>" runat="server"></asp:Literal></SPAN>
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
             <tr style="display:none">
                <td align="Left">
                    <table width="95%">
                        <tr>
                            <%--Window Dressing - Start --%>
                            <td align="right" class="blackblodtext"><b><asp:Literal Text="<%$ Resources:WebResources, EndpointList_EndpointName%>" runat="server"></asp:Literal></b></td>
                            <td>
                                <asp:TextBox ID="txtEndpointName" runat="server" CssClass="altText" Text="" ValidationGroup="Search" ></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ControlToValidate="txtEndpointName" Display="dynamic" runat="server" SetFocusOnError="true" ErrorMessage="<%$ Resources:WebResources, InvalidCharacters2%>" ValidationExpression="^(a-z|A-Z|0-9)*[^\\/<>+;?|!`,\[\]{}\x22;=:@^#$%&()'~]*$"></asp:RegularExpressionValidator>
                            </td>
                            <td align="right" class="blackblodtext"><b><asp:Literal Text="<%$ Resources:WebResources, EndpointList_EndpointType%>" runat="server"></asp:Literal></b></td>
                            <td>
                                <asp:DropDownList ID="lstAddressType" runat="server" CssClass="altText" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                <asp:DropDownList ID="lstBridges" Visible="false" runat="server" CssClass="altText" DataTextField="BridgeName" DataValueField="BridgeID"></asp:DropDownList>
                                <asp:DropDownList ID="lstLineRate" Visible="false" runat="server" CssClass="altText" DataTextField="LineRateName" DataValueField="LineRateID"></asp:DropDownList>
                                <asp:DropDownList ID="lstVideoEquipment" Visible="false" runat="server" CssClass="altText" DataTextField="VideoEquipmentName" DataValueField="VideoEquipmentID"></asp:DropDownList>
                                <asp:DropDownList ID="lstVideoProtocol" Visible="false" runat="server" CssClass="altText" DataTextField="Name" DataValueField="ID"></asp:DropDownList>
                                <asp:DropDownList ID="lstConnectionType" runat="server" DataTextField="Name" DataValueField="ID" Visible="false"></asp:DropDownList>                                 <asp:DropDownList ID="DropDownList1" runat="server" DataTextField="Name" DataValueField="ID" Visible="false"></asp:DropDownList> <%--Fogbugz case 427--%>
                            </td>
                            <%--Window Dressing - End --%>
                        </tr>
                        <tr>
                            <td colspan="4" align="right">
                                <asp:Button runat="server" cssclass="altLongBlueButtonFormat" text="<%$ Resources:WebResources, Submit%>" validationgroup="Search" onclick="SearchEndpoint"></asp:Button>
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
            <tr>
                <td align="Left">
                    <table>
                        <tr id="trNew" runat="server">
                            <td>&nbsp;</td>
                            <td>
                                <%--<SPAN class=subtitleblueblodtext>Create New Endpoint <asp:Label ID="lblAddSelection" Text=" or Choose Selected" Visible="false" runat="server" ></asp:Label></SPAN>--%><%--Commented for FB 2094--%> 
                                <SPAN class="subtitleblueblodtext"><asp:Label id="Label1" text="<%$ Resources:WebResources, EndpointList_Label1%>" visible="false" runat="server"></asp:Label></SPAN><%--FB 2094--%> 
                            </td>
                        </tr>
                    </table>
                </td>
             </tr>
            <tr>
                <td align="Left">
                    <table width="95%">
                        <tr>
                            <td align="right">
                                <button ID="btnSubmit" runat="server" style="width:240px;" onserverclick="CreateNewEndpoint"> <%--ZD 101714--%>
                                <asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, EndpointList_btnSubmit%>" runat="server"></asp:Literal>
                                </button><%--FB 2094--%> <%-- FB 2796--%>
                            </td>
                        </tr>

                    </table>
                </td>
             </tr>
        </table>
            <asp:TextBox Visible="false" ID="txtEndpointID" runat="server"></asp:TextBox>
        </center>
    </form>
</body>
</html>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->