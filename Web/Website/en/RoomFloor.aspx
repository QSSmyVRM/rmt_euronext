﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_RoomFloor" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<!-- #INCLUDE FILE="inc/maintopNET.aspx" -->
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css" >
        .iemask
        {
            -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=50)";
        }
    </style>
    <script type="text/javascript">        

        var IsIE = false;
        if (navigator.userAgent.indexOf('Trident') > -1)
            IsIE = true;

        document.onkeydown = function (evt) {
            evt = evt || window.event;
            var keyCode = evt.keyCode;
            if (keyCode == 27) {
                if (document.getElementById("confBox").style.display == "block") {
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnNo").click();
                        return false;
                    }
                }
                if (document.getElementById("divFlrPlanList").style.display == "block") {
                    var str = document.activeElement.type;
                    if (!(str == "text" || str == "textarea" || str == "password")) {
                        document.getElementById("btnClose").click();
                        return false;
                    }
                }
            }
            fnOnKeyDown(evt);
        };

        var xx, yy;
        document.body.onclick = function (e) {
            if (document.activeElement.tagName == "BODY" || document.activeElement.tagName == "DIV") {
                e = e || window.event;
                xx = e.pageX - 2; // PRBAU TEMP
                yy = e.pageY - 4;
                if (e.pageX == undefined) {
                    xx = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft - 2; // PRABU TEMP
                    yy = e.clientY + document.body.scrollTop + document.documentElement.scrollTop - 4;
                }
            }
        };

        var elemId = "";
        var markCounter = 0;
        function fnConfirmMark() {
            try {
                //debugger;
                document.getElementById('errLabel').innerHTML = '';
                markCounter = parseInt(document.getElementById("markCount").value, 10);
                var iDiv = document.createElement('div');
                elemId = "divMark" + markCounter.toString();
                iDiv.id = elemId;
                iDiv.style.backgroundColor = "#FFCC00";
                iDiv.style.zIndex = "900";
                iDiv.style.position = "absolute";
                iDiv.style.fontSize = "8px"; // PRABU TEMP
                iDiv.style.left = xx + 'px';
                iDiv.style.top = yy + 'px';
                iDiv.innerHTML = markCounter.toString();
                document.getElementsByTagName('body')[0].appendChild(iDiv);
                document.getElementById("clientMask").style.display = "block";
                document.getElementById("confBox").style.display = "block";
                document.getElementById("confBox").style.left = (xx + 25) + 'px';
                document.getElementById("confBox").style.top = (yy + 25) + 'px';
            }
            catch (ex) {
                console.log(ex);
            }
        }

        function fnConfirmMark2(stat) {
            try {
                //debugger;
                if (stat == true) {
                    document.getElementById(elemId).style.backgroundColor = "lightgreen";
                    document.getElementById("markCount").value = markCounter + 1;
                    var markVal = document.getElementById("markedValues");
                    if (markVal.value == "")
                        document.getElementById("markedValues").value = xx + "x" + yy;
                    else
                        document.getElementById("markedValues").value += "," + xx + "x" + yy;
                    document.getElementById("loadingDiv").style.display = "block";
                    document.getElementById("btnAddRoom").click();
                }
                else {
                    var elem = document.getElementById(elemId);
                    elem.parentNode.removeChild(elem);
                    document.getElementById("clientMask").style.display = "none";
                }
                document.getElementById("confBox").style.display = "none";
            }
            catch (ex) {
                console.log(ex);
            }
        }

        function fnConfirmRemove(objId) {
            try {
                //debugger;
                var rowInd = document.getElementById(objId).parentNode.parentNode.rowIndex;
                var curId = "";
                if(IsIE)
                    curId = document.getElementById(objId).parentNode.parentNode.childNodes[0].innerHTML;
                else
                    curId = document.getElementById(objId).parentNode.parentNode.childNodes[1].innerHTML;
                var divId = "divMark" + curId;
                document.getElementById(divId).style.backgroundColor = "red";
                var conf = confirm(removemarkedlocation);
                if (conf == true) {
                    document.getElementById('errLabel').innerHTML = '';
                    fnDeleteMark(rowInd);
                    fnHoldMarkingState(false);
                    return true;
                }
                else {
                    document.getElementById(divId).style.backgroundColor = "lightgreen";
                    return false;
                }
            }
            catch (ex) {
                console.log(ex);
            }

        }

        function fnDeleteMark(rowInd) {
            try {
                //debugger;
                var markVal = document.getElementById("markedValues").value.split(',');
                var roomIds = document.getElementById("hdnRoomIds").value.split(',');
                var newVal = "";
                var newRoomVal = "";
                for (var i = 0; i < markVal.length; i++) {
                    if (i == rowInd)
                        continue;
                    if (newVal == "") {
                        newVal = markVal[i];
                        newRoomVal = roomIds[i];
                    }
                    else {
                        newVal += "," + markVal[i];
                        newRoomVal += "," + roomIds[i];
                    }
                }
                document.getElementById("markedValues").value = newVal;
                document.getElementById("hdnRoomIds").value = newRoomVal;
                var markCount = parseInt(document.getElementById("markCount").value, 10);
                for (var i = 0; i < markCount; i++) {
                    var mid = "divMark" + (i + 1).toString();
                    if (document.getElementById(mid) != null) {
                        var elem = document.getElementById(mid);
                        elem.parentNode.removeChild(elem);
                    }
                }
                document.getElementById("markCount").value = markCount - 1;
                document.getElementById("clientMask").style.display = "block";
                document.getElementById("loadingDiv").style.display = "block";
            }
            catch (ex) {
                console.log(ex);
            }
        }

        function fnSelectRoom(selectId) {
            try {
                //debugger;
                document.getElementById('hdnSelectedRoom').value = "";
                document.getElementById("hdnRowInd").value = document.getElementById(selectId).parentNode.parentNode.rowIndex;
                var url = "RoomSearch.aspx?hf=1&ConfType=8&frm=RoomFloor";
                window.open(url, "RoomSearch", "width=" + screen.availWidth + ",height=600px,resizable=no,scrollbars=yes,status=no,top=0,left=0");
                return false;
            }
            catch (ex) {
                console.log(ex);
            }
        }

        function fnResumeSelection(selected) {
            try {
                //debugger;
                if (selected != "") {
                    document.getElementById('errLabel').innerHTML = '';
                    document.getElementById("clientMask").style.display = "block";
                    document.getElementById("loadingDiv").style.display = "block";
                    document.getElementById("hdnSelectedRoom").value = selected;
                    document.getElementById("btnUpdateRoom").click();
                }
            }
            catch (ex) {
                console.log(ex);
            }
        }

        function fnPostResponse() {
            document.getElementById("clientMask").style.display = "none";
            document.getElementById("loadingDiv").style.display = "none";
        }

        function fnOpenSavedPlans(stat) {
            //debugger;
            if(stat)
                document.getElementById('errLabel2').innerHTML = '';
            document.getElementById("clientMask").style.display = "block";
            document.getElementById("divFlrPlanList").style.display = "block";
            return false;
        }

        function fnCloseSavedPlans() {
            //debugger;
            document.getElementById("clientMask").style.display = "none";
            document.getElementById("divFlrPlanList").style.display = "none";
            return false;
        }

        function fnValidateUpload() {
            try {
                var errLab = document.getElementById("errLabel");
                errLab.innerHTML = "";
                errLab.style.color = "#cc0000";
                if (document.getElementById("fleFloorPlan").value == "") {
                    errLab.innerHTML = selectfloorplan;
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    return false;
                }
                else {
                    document.getElementById("txtFileName").value = "";
                    __doPostBack('btnUploadImage', '');
                }
            }
            catch (ex) {
                console.log(ex);
            }
        }

    </script>
    <script type="text/javascript" src="script/CallMonitorJquery/jquery.1.4.2.js" ></script>
</head>
<body>
    <form id="form1" runat="server">
    <input id="imgDiff" type="hidden" runat="server" />
    <input id="hdnHoldState" type="hidden" runat="server" value="" />
    <input id="hdnHoldPopup" type="hidden" runat="server" value="" />
    <asp:ScriptManager runat="server" EnableScriptLocalization="true">
		<Scripts>                
			<asp:ScriptReference Path= "~/ResourceScript/StringResources.js" ResourceUICultures="<%$ Resources:WebResources, UICulture%>"  />
		</Scripts>
    </asp:ScriptManager>
    <div>
        <div id="clientMask" class="iemask" style="z-index:1000; position:fixed; left:0px; top:0px; width:1366px; height:768px; display:none; background-color:White; opacity:0.5" ></div>
        <div id="loadingDiv" align="center" style="display:none; position:fixed; top:175px;">
            <img border="0" src="image/wait1.gif" alt="<asp:Literal Text='<%$ Resources:WebResources, Loading%>' runat='server'></asp:Literal>" />
        </div>
        <div id="confBox" style="position:absolute; z-index:1001; width:320px; height:120px; background-color:#eee; display:none; border:3px solid gray; border-radius:15px" >
            <table width="100%" >
                <tr>
                    <td colspan="2" align="center" >
                        <b class="subtitleblueblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConfirmSelection%>" runat="server" /></b>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" class="blackblodtext" align="center" >
                        <br />
                        <asp:Literal Text="<%$ Resources:WebResources, suretomark%>" runat="server" />
                        <br /><br />
                    </td>
                </tr>
                <tr>
                    <td align="center">
                        <input type="button" runat="server" onclick="fnConfirmMark2(true);" class="altMedium0BlueButtonFormat" value="<%$ Resources:WebResources, Yes%>" style="width:60px" />
                    </td>
                    <td align="center" >
                        <input id="btnNo" type="button" runat="server" onclick="fnConfirmMark2(false)" class="altMedium0BlueButtonFormat" value="<%$ Resources:WebResources, No%>" style="width:60px" />
                    </td>
                </tr>
            </table>
        </div>
        <table border="0" width="80%" align="center" >
            <tr>
                <td align="center" colspan="100%">
                    <h3><asp:Literal Text="<%$ Resources:WebResources, RoomFloorPlan%>" runat="server" /></h3>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="100%" >
                    <asp:Label ID="errLabel" CssClass="lblMessage" runat="server" EnableViewState="false" ></asp:Label>
                </td>
            </tr>
            <tr>
                <td colspan="100%" style="width:100%" >
                    <table border="0" width="100%" >
                        <tr>
                            <td colspan="2">
                                <table border="0" width="100%">
                                    <tr>
                                        <td style="text-align:left; width:30%" >
                                            <div>
                                                <input id="txtFileName" type="text" class="file_input_textbox" readonly="readonly" value='<%$ Resources:WebResources, Nofileselected%>' runat="server" style=" height:18px;width:175px"/>
                                                <div class="file_input_div">
                                                    <button type="button" id="BtnBrowse1" class="file_input_button" style="vertical-align:middle" onclick="document.getElementById('fleFloorPlan').click();return false;" ><asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, Browse%>" runat="server"></asp:Literal></button>
                                                    <input type="file" class="file_input_hidden" accept="image/*" id="fleFloorPlan" contenteditable="false" enableviewstate="true" runat="server" onchange="document.getElementById('errLabel').innerHTML = ''; getfilename(this)"/>
                                                </div>
                                            </div>
                                        </td>
                                        <td style="text-align:left; width:40%; vertical-align:top" >
                                            <button type="button" id="btnUploadImage" style="width:200px" runat="server" class="altMedium0BlueButtonFormat" onclick="javascript:return fnValidateUpload();" onserverclick="uploadImage" ><asp:Literal Text="<%$ Resources:WebResources, ManageRoomProfile_btnUploadImages%>" runat="server" /></button>
                                            <img src="image/info.png" alt="" title='<asp:Literal Text="<%$ Resources:WebResources, flrplansize%>" runat="server" />'>
                                        </td>
                                        <td align="right" style="width:30%" >
                                            <input id="btnPopup" runat="server" style="width:200px" type="button" class="altMedium0BlueButtonFormat" value="<%$ Resources:WebResources, OpenSavedPlans%>" onclick="javascript:return fnOpenSavedPlans(true);" />
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td class="subtitleblueblodtext" align="left">
                                <asp:Literal Text="<%$ Resources:WebResources, SelectedFloorPlan%>" runat="server" />
                            </td>
                            <td class="subtitleblueblodtext" align="left" >
                                <asp:Literal Text="<%$ Resources:WebResources, IdentifiedLocations%>" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="width:50%; vertical-align:top" >
                                <div id="mapContainer" runat="server" style="background-color: lightgray; width: 400px;
                                    height: 300px; text-align: left; border: 1px solid gray">
                                    <img runat="server" onclick="setTimeout('fnConfirmMark()', 100);" id="planMap" src="" alt="" style="cursor: crosshair" />
                                    <span id="noImgText" runat="server" ><br /><b><asp:Literal Text="<%$ Resources:WebResources, Pleaseuploadfloorplan%>" runat="server" /></b></span>
                                </div>
                            </td>
                            <td align="left" style="width:50%; vertical-align:top; border:1px solid lightgray" >
                                <asp:UpdatePanel runat="server">
                                    <ContentTemplate>
                                        <input id="markCount" type="hidden" runat="server" value="1" />
                                        <input id="markedValues" type="hidden" runat="server" value="" />
                                        <input id="hdnRowInd" type="hidden" runat="server" value="" />
                                        <input id="hdnSelectedRoom" type="hidden" runat="server" />
                                        <input id="hdnRoomIds" type="hidden" runat="server" />
                                        <input id="hdnTierName" type="hidden" runat="server" />
                                        <table class="tableHeader" width="100%" >
                                            <tr>
                                                <td style="width:10%" ><asp:Literal Text="<%$ Resources:WebResources, SNo%>" runat="server" /></td>
                                                <td style="width:40%"><asp:Literal Text="<%$ Resources:WebResources, MappedRoom%>" runat="server" /></td>
                                                <td style="width:35%">
                                                    <asp:Literal Text="<%$ Resources:WebResources, RoomSearch_Hyper1%>" runat="server" />
                                                    <img src="image/info.png" alt="" title='<asp:Literal Text="<%$ Resources:WebResources, tierselectionmsg%>" runat="server" />' />
                                                </td>
                                                <td style="width:15%"><asp:Literal Text="<%$ Resources:WebResources, Action%>" runat="server" /></td>
                                            </tr>
                                        </table>
                                        <div style="height:280px; overflow-y:auto; overflow-x:hidden;">
                                            <span id="lblNoData" runat="server" ><br /><b><asp:Literal Text="<%$ Resources:WebResources, NoData%>" runat="server" /></b></span>
                                            <asp:GridView ID="planMark" width="100%" runat="server" ShowHeader="false" AutoGenerateColumns="false" CellPadding="4" OnRowCommand="RemoveMarkedRow" GridLines="None" >
                                                <RowStyle CssClass="tableBody" />
                                                <Columns>
                                                    <asp:BoundField DataField="no" ItemStyle-Width="10%" />
                                                    <asp:TemplateField ItemStyle-Width="40%">
                                                        <ItemTemplate>
                                                            <input type="text" readonly="readonly" value="<%# DataBinder.Eval(Container.DataItem,"roomname") %>" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-Width="35%" >
                                                        <ItemTemplate>
                                                            <asp:Button ID="selectRoom" runat="server" CssClass="altMedium0BlueButtonFormat" Text="<%$ Resources:WebResources, EndpointSearch_SelectEP%>" Width="100px" OnClientClick="javascript:return fnSelectRoom(this.id);" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField ItemStyle-Width="15%" >
                                                        <ItemTemplate>
                                                            <asp:LinkButton ID="removeLink" runat="server" Text="<%$ Resources:WebResources, ConferenceSetup_btnRemove1%>" CommandName="remove" OnClientClick="javascript:return fnConfirmRemove(this.id);" ></asp:LinkButton>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                        <div style="display:none">
                                                <asp:Button runat="server" ID="btnAddRoom" OnClick="addNewMark" />
                                                <asp:Button runat="server" ID="btnUpdateRoom" OnClick="updateSelectedRoom" />
                                        </div>
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="100%">
                    <table style="width: 50%">
                        <tr>
                            <td align="center">
                                <input id="btnReset" runat="server" type="button" value="<%$ Resources:WebResources, Reset%>" class="altMedium0BlueButtonFormat"
                                    style="width: 100pt;" onclick="javascript:return fnResetPage()" />
                            </td>
                            <td align="center">
                                <input type="submit" runat="server" onclick="javascript:return fnValidateFloorPlan();" onserverclick="submitFloorPlan" value="<%$ Resources:WebResources, Allocation_AllocationSubmit%>" id="btnSubmit" class="altMedium0BlueButtonFormat"
                                    style="width: 100pt;" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <div id="divFlrPlanList" style="position:fixed; z-index:1001; width:700px; height:500px; background-color:#eee; display:none; border:3px solid gray; border-radius:15px; top:50px" >
            <table align="center" width="90%" >
                <tr>
                    <td align="center" colspan="100%" class="subtitleblueblodtext">
                        <br /><asp:Literal Text="<%$ Resources:WebResources, FloorPlanList%>" runat="server" /><br />
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="100%" >
                        <asp:Label ID="errLabel2" CssClass="lblMessage" runat="server" EnableViewState="false" ></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td colspan="100%">
                        <table class="tableHeader" width="100%" >
                            <tr>
                                <td style="width:10%" ><asp:Literal Text="<%$ Resources:WebResources, SNo%>" runat="server" /></td>
                                <td style="width:70%"><asp:Literal Text="<%$ Resources:WebResources, UserMenuController_Tiers%>" runat="server" /></td>
                                <td style="width:10%"><%= actionText %></td>
                                <td style="width:10%"></td>
                            </tr>
                        </table>
                        <div style="height:300px; overflow-y:auto; overflow-x:hidden;">
                            <span id="grdNoData" runat="server" ><br /><b><asp:Literal Text="<%$ Resources:WebResources, NoData%>" runat="server" /></b></span>
                            <asp:GridView ID="grdPlanList" width="100%" ShowHeader="false" runat="server" AutoGenerateColumns="false" CellPadding="4" OnRowCommand="importSelectedPlan" GridLines="None" >
                                <RowStyle CssClass="tableBody" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%" >
                                        <ItemTemplate>
                                            <span><%# Container.DataItemIndex + 1 %></span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="70%" >
                                        <ItemTemplate>
                                            <span><%# DataBinder.Eval(Container.DataItem, "Tier1Name")%> &gt; <%# DataBinder.Eval(Container.DataItem, "Tier2Name")%></span>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%" >
                                        <ItemTemplate>
                                            <asp:LinkButton ID="editLink" runat="server" Text='<%# editText %>' CommandName="editPlan" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-Width="10%" >
                                        <ItemTemplate>
                                            <asp:LinkButton ID="deleteLink" runat="server" Text="<%$ Resources:WebResources, ConferenceList_btnDelete%>" Visible='<%# Session["admin"].ToString() != "3" %>' OnClientClick="javascript:return confirm(deletefloorplan);" CommandName="deletePlan" ></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="center" colspan="100%" >
                        <input id="btnClose" type="button" runat="server" class="altMedium0BlueButtonFormat" value="<%$ Resources:WebResources, BridgeDetails_BtnClose%>" onclick="javascript:return fnCloseSavedPlans();" />
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
<script type="text/javascript">

    function fnResetPage() {
        window.location.href = window.location.href;
        return false;
    }

    function fnValidateFloorPlan() {
        try {
            //debugger;
            var markedVal = document.getElementById("markedValues");
            var errLab = document.getElementById("errLabel");
            var roomIds = document.getElementById("hdnRoomIds");
            errLab.innerHTML = "";
            errLab.style.color = "#cc0000";
            if (document.getElementById("planMap") == null) {
                errLab.innerHTML = uploadfloorplan;
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            }
            if (markedVal.value == "") {
                errLab.innerHTML = markhotdeskingroom;
                $("html, body").animate({ scrollTop: 0 }, "slow");
                return false;
            }
            else {
                var arrMarkVal = markedVal.value.split(',');
                var arrRoomIds = roomIds.value.split(',');
                if ((arrMarkVal.length != arrRoomIds.length) || (roomIds.value == "")) {
                    errLab.innerHTML = selectroomformarked;
                    $("html, body").animate({ scrollTop: 0 }, "slow");
                    return false;
                }
            }
            var imgObj = document.getElementById("planMap").getBoundingClientRect();
            var diffX=0, diffY=0;
            if (IsIE) {
                diffX = imgObj.left + document.documentElement.scrollLeft + document.body.scrollLeft;
                diffY = imgObj.top + document.documentElement.scrollTop + document.body.scrollTop;
            }
            else {
                diffX = Math.round(imgObj.left) + window.pageXOffset;
                diffY = Math.round(imgObj.top) + window.pageYOffset;
            }
            var arrMarkVal = markedVal.value.split(',');
            var subArr = null;
            var finalVal = "";
            for (var i = 0; i < arrMarkVal.length; i++) {
                subArr = arrMarkVal[i].split('x');
                if (finalVal == "")
                    finalVal = (parseInt(subArr[0], 10) - diffX) + "x" + (parseInt(subArr[1], 10) - diffY);
                else
                    finalVal += "," + (parseInt(subArr[0], 10) - diffX) + "x" + (parseInt(subArr[1], 10) - diffY);
            }
            document.getElementById("imgDiff").value = finalVal;
            return true;
        }
        catch (ex) {
            console.log(ex);
        }
    }

    function fnHoldMarkingState(stat) {
        try {
            //debugger;
            document.getElementById("hdnHoldState").value = ""
            var imgObj, diffX = 0, diffY = 0;
            if (stat == true) {
                imgObj = document.getElementById("planMap").getBoundingClientRect();
                if (IsIE) {
                    diffX = imgObj.left + document.documentElement.scrollLeft + document.body.scrollLeft;
                    diffY = imgObj.top + document.documentElement.scrollTop + document.body.scrollTop;
                }
                else {
                    diffX = Math.round(imgObj.left) + window.pageXOffset;
                    diffY = Math.round(imgObj.top) + window.pageYOffset;
                }
            }
            var newMarked = "";
            var marked = document.getElementById("markedValues");
            if (marked.value != "") {
                var markArr = marked.value.split(',');
                var subMarkArr = null;
                for (var i = 0; i < markArr.length; i++) {
                    subMarkArr = markArr[i].split('x');
                    var iDiv = document.createElement('div');
                    var elemId = "divMark" + (i + 1).toString();
                    iDiv.id = elemId;
                    iDiv.style.backgroundColor = "lightgreen";
                    iDiv.style.zIndex = "900";
                    iDiv.style.position = "absolute";
                    iDiv.style.fontSize = "8px"; // PRABU TEMP
                    iDiv.style.left = (parseInt(subMarkArr[0]) + diffX) + 'px';
                    iDiv.style.top = (parseInt(subMarkArr[1]) + diffY) + 'px';
                    iDiv.innerHTML = (i + 1).toString();
                    document.getElementsByTagName('body')[0].appendChild(iDiv);
                    if (stat == true) {
                        if (newMarked == "")
                            newMarked = (parseInt(subMarkArr[0]) + diffX) + "x" + (parseInt(subMarkArr[1]) + diffY);
                        else
                            newMarked += "," + (parseInt(subMarkArr[0]) + diffX) + "x" + (parseInt(subMarkArr[1]) + diffY);
                    }
                }
            }
            if (stat == true) {
                document.getElementById("markedValues").value = newMarked;
            }
        }
        catch (ex) {
            console.log(ex);
        }
    }
    if (document.getElementById("hdnHoldState").value == "1")
        setTimeout('fnHoldMarkingState(true)', 100);
    if (document.getElementById("hdnHoldState").value == "0")
        setTimeout('fnHoldMarkingState(false)', 100);

    function fnSetDimension() {
        try {
            var w = screen.width;
            var h = screen.height;
            document.getElementById("loadingDiv").style.left = Math.round(w / 2) + 'px';
            document.getElementById("clientMask").style.width = w + 'px';
            document.getElementById("clientMask").style.height = h + 'px';
            document.getElementById("divFlrPlanList").style.left = Math.round((w - 800) / 2) + 'px';
        }
        catch (ex) {
            console.log(ex);
        }
    }
    fnSetDimension();

    if (document.getElementById("hdnHoldPopup").value == "1") {
        document.getElementById("hdnHoldPopup").value = "";
        //document.getElementById("btnPopup").click();
        fnOpenSavedPlans(false);
    }
</script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->
