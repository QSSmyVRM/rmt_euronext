<%--ZD 100147 start--%>
<%--/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/--%>
<%--ZD 100147 ZD 100886 End--%>
<%@ Page Language="C#" AutoEventWireup="true" Inherits="en_ConfirmTemplate" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"> <!-- FB 2050 -->
<meta http-equiv="X-UA-Compatible" content="IE=8" /> <!-- FB 2050 -->
<!-- #INCLUDE FILE="inc/maintopNET.aspx" --> 
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Confirm Template</title>
</head>
<body>
          <script language="JavaScript">
<!--
      //ZD 100604 start
        var img = new Image();
        img.src = "../en/image/wait1.gif";
     //ZD 100604 End
	function frmsubmit(opr) {
	    DataLoading(1); // ZD 100176
		switch (opr) {
		    case "MODIFY"://ZD 100263		        
		        //document.frmConfirmtemplate.action = "dispatcher/userdispatcher.asp";
		        //document.frmConfirmtemplate.action = "managetemplate2.aspx?tid=<%=templateID%>&cmd=GetOldTemplate";
		        document.frmConfirmtemplate.action = "managetemplate2.aspx?tid=<%=templateID%>&cmd=O";		        
		        document.frmConfirmtemplate.cmd.value = "GetOldTemplate";
		        break;
			case "LIST":
				//document.frmConfirmtemplate.action = "dispatcher/conferencedispatcher.asp?cmd=GetTemplateList&frm=manage";
				document.frmConfirmtemplate.action = "ManageTemplate.aspx";
				//document.frmConfirmtemplate.cmd.value="GetTemplateList";
				break;
		}
		
		document.frmConfirmtemplate.opr.value = opr;
		document.frmConfirmtemplate.submit ();
	}
	//ZD 100176 start
	function DataLoading(val) {
	    if (val == "1")
	        document.getElementById("dataLoadingDIV").style.display = 'block'; //ZD 100678
	    else
	        document.getElementById("dataLoadingDIV").style.display = 'none'; //ZD 100678
	}
	//ZD 100176 End

//-->
</script>
<div id="TempOK" runat="server" style="display:block">

            <center>
              <h3><asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_Congratulations%>" runat="server"></asp:Literal>  <% =userName %>!</h3>
            </center>            
			<br/>
            
            <center>
			<table width="90%" border="0" cellspacing="2" cellpadding="4">
              <tr> 
              <%--Window Dressing--%>
                <td colspan="3"  class="lblMessage" align="center"><%--FB 2487--%> 
                  <p><b><%if(Application["Client"].ToString().ToUpper() == "MOJ") {%>Your template has been successfully submitted. You can use it for creating hearings later.<%}else{ %> 
                  <asp:Literal ID="Literal3" Text="<%$ Resources:WebResources, ConfirmTemplate%>" runat="server"></asp:Literal><%} %></b></p><%--Edited for FB 1428--%>
                </td>
              </tr>
              <tr>
                <td align="center" colspan="3">
                    <asp:Label ID="errLabel" runat="server" Text="" ForeColor="red" CssClass="lblError"></asp:Label>
                </td>
              </tr>
              <div id="dataLoadingDIV"  name="dataLoadingDIV" align="center" style="display:none">
                   <img border='0' src='image/wait1.gif' alt='Loading..' />
              </div><%--ZD 100678--%>
              <tr> 
                <td colspan="3">&nbsp;</td>
              </tr>
              
              <tr> 
                <td width="5%">&nbsp;</td>
              <%--Window Dressing--%>
                <td width="30%" align="left" class="subtitlexxsblueblodtext"><span style="margin-left:-20px"><b><asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_TemplateSummar%>" runat="server"></asp:Literal></b></span></td>
                <td width="65%">&nbsp; </td>
              </tr>
			  <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_TemplateName%>" runat="server"></asp:Literal></td><%--FB 2508--%>
                <td align="left">
                  <% =templateName %> &nbsp;&nbsp; 
				  <font color="darkblue"><b><i>

<% 
	if (templatePublic == "1" ) {%>
		<asp:Literal ID="Literal5" Text="<%$ Resources:WebResources, Public%>" runat="server"></asp:Literal>
        <%--        Response.Write ("public");--%>

	<%} else {%>
		<asp:Literal ID="Literal6" Text="<%$ Resources:WebResources, private%>" runat="server"></asp:Literal>
<%--		Response.Write ("private");--%>
	
       <%} %>

                  </i></b></font>
                </td>
              </tr>
 			  <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_TemplateDescri%>" runat="server"></asp:Literal></td><%--FB 2508--%>
                <td align="left"><% =templateDescription %></td>
              </tr>
              <tr> 
                <td></td>
                <td></td>
                <td></td>
              </tr>
              
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="subtitlexxsblueblodtext"><span style="margin-left:-20px"><b><%if(Application["Client"].ToString().ToUpper() == "MOJ") {%>Hearing Summary<%}else{ %> 
                <asp:Literal ID="Literal4" Text="<%$ Resources:WebResources, ConferenceSummary%>" runat="server"></asp:Literal><%} %></b></span></td><%--Edited for FB 1428--%>
                <td>&nbsp; </td>
              </tr>
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext" valign="top"><asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_Name%>" runat="server"></asp:Literal></td><%--FB 2508--%>
                <td align="left">
                  <% =confName %>&nbsp;&nbsp; 
				  <font color="darkblue"><b><i>
                  
                    <% 
	                if (publicConf == "1" ) {%>
		                <asp:Literal ID="Literal7" Text="<%$ Resources:WebResources, Public%>" runat="server"></asp:Literal>
                        <%--        Response.Write ("public");--%>

	                <%} else {%>
		                <asp:Literal ID="Literal8" Text="<%$ Resources:WebResources, private%>" runat="server"></asp:Literal>
                <%--		Response.Write ("private");--%>
	
                       <%} %>
                  </i></b></font>
                </td>
              </tr>
              <%if(!(Application["Client"].ToString().ToUpper() == "MOJ")){%><%--Added for MOJ Phase 2 QA --%>
			  <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_Password%>" runat="server"></asp:Literal></td>
                <td align="left"><% =confPassword %></td>
              </tr>
              <tr> 
              <%} %><%--Added for MOJ Phase 2 QA --%>
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext" valign="top"><%if(Application["Client"].ToString().ToUpper() == "MOJ") {%>Hearing Description<%}else{ %>
                <asp:Literal ID="Literal12" Text="<%$ Resources:WebResources, ExpressConference_ConferenceDesc%>" runat="server"></asp:Literal><%} %></td><%--Edited for FB 1428--%><%--FB 2508 ZD 101714--%>
                <td align="left"><% =description %></td>
              </tr>
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_Duration%>" runat="server"></asp:Literal></td>
                <td align="left">
                <%
	                dhour = (Int32)durationMin / 60;
                    dmin =(Int32)durationMin - dhour * 60;

	                //Code added for FB 1216 - start
                    if (dhour == 0)
	                    Response.Write(dmin  + " mins"); 
                    else if( dmin == 0 )
	                    Response.Write(dhour + " hrs");
	                else
	                    Response.Write(dhour + " hrs and " + dmin  + " mins");
                	
                    //Code added for FB 1216 - end
                %> <%--//ZD 100528--%>
                </td>
              </tr>
              
			  <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_Location%>" runat="server"></asp:Literal></td>
                <td align="left"><% =locations %></td>
              </tr>           
              <tr id="trPart" runat="server"> <%--Added for FB 1425 QA Bug--%>
                <td></td>
              <%--Window Dressing--%>
                <td valign="top" align="left" class="blackblodtext"><asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_ParticipantsLi%>" runat="server"></asp:Literal></td>
                <td align="left"><table border="0" cellspacing="2" cellpadding="0"><% =invited + invitee + cc %></table></td>
              </tr>
              <tr> 
                <td></td>
                <td></td>
                <td></td>
              </tr>
             
             
             <%-- ZD 101377 START--%>
               <tr id="trtempSecure" runat="server"> 
                <td></td>
               <td align="left" class="blackblodtext" valign="top"><asp:Literal ID="Literal9" Text="<%$ Resources:WebResources, ConferenceSetup_NetworkState%>" runat="server"></asp:Literal></td><%--FB 2508--%>
                <td align="left">
                  
                    <% 
	                if (NetworkState == "0" ) {%>
		                <asp:Literal ID="Literal10" Text="<%$ Resources:WebResources, ConferenceSetup_NATOUnclassified%>" runat="server"></asp:Literal>
                        
                    <%} else {%>
		                <asp:Literal ID="Literal11" Text="<%$ Resources:WebResources, ConferenceSetup_NATOSecret%>" runat="server"></asp:Literal>
	
                       <%} %>
                  
                </td>
              </tr>
             <%--ZD 101377 END--%>              
              
              
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="subtitlexxsblueblodtext"><span style="margin-left:-20px"><b><asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_AdditionalInfo%>" runat="server"></asp:Literal></b></span></td>
                <td></td>
              </tr>
              <tr> 
                <td></td>
              <%--Window Dressing--%>
                <td align="left" class="blackblodtext"> 
                 <asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_TollfreeHelpL%>" runat="server"></asp:Literal>
                </td>
                <td align="left">
                <asp:Label id="lblContPhone" runat="server"></asp:Label> <%--ZD 100369--%>
                <%--<%=Application["contactPhone"]%>--%>
                </td>
              </tr>
            </table>
            </center>
            
			<br><br>


			<form name="frmConfirmtemplate" method="POST" action="" >
             
			  <input type="hidden" name="cmd" value="" />
			  <input type="hidden" name="templateID" value="<% =templateID %>" />
			  <input type="hidden" name="opr" value="" />
			  
			  <div align="center">
	            <table>
                  <tr> 
                    <td align="center"> 
					  <%--<input type="button" runat="server" id="BtnEdit" name="ConfirmTemplateSubmit" value="Edit Template Settings" class="altLongBlueButtonFormat" onclick="JavaScript: frmsubmit('MODIFY');">--%><%--ZD 100420--%>
					  <button runat="server" id="BtnEdit" name="ConfirmTemplateSubmit" class="altLongBlueButtonFormat" onclick="JavaScript: frmsubmit('MODIFY');" style="width:250px"><asp:Literal ID="Literal1" Text="<%$ Resources:WebResources, ConfirmTemplate_EditTemplateSettings%>" runat="server"></asp:Literal></button><%--ZD 100420--%>
                    </td>
                    <td width="10%">&nbsp;</td>
                    <td align="center"> 
                      <%--code added for Soft Edge button--%>                    
					  <%--<input type="button" onfocus="this.blur()" name="ConfirmTemplateSubmit0" value="Back to Template List" class="altLongBlueButtonFormat" onclick="JavaScript: frmsubmit('LIST');">--%><%--ZD 100420--%>
					  <button onfocus="this.blur()" id="btnCancel" name="ConfirmTemplateSubmit0" class="altLongBlueButtonFormat" onclick="JavaScript: frmsubmit('LIST');" style="width:250px"><asp:Literal ID="Literal2" Text="<%$ Resources:WebResources, ConfirmTemplate_BacktoTemplateList%>" runat="server"></asp:Literal></button> <%--ZD 100420--%> <%--ZD 100369--%>
                    </td>
                  </tr>
                </table>
              </div>	
              
    </div>  
 <div id="divError" runat="server" style="display:none">
  <center>
 <table width="90%" border="0" cellspacing="2" cellpadding="4">
  <tr>
    <td align="center" height="10">
    </td>
  </tr>
  <tr>
    <td align="center">
      <font size="4"><b><asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_Yourrequestco%>" runat="server"></asp:Literal></b></font>
    </td>
  </tr>
  <tr>
    <td align="center" height="20">
    </td>
  </tr>
    <tr>
        <td align="center">
        <font size='3' color='red'><b><asp:Literal Text="<%$ Resources:WebResources, ConfirmTemplate_Youarenotper%>" runat="server"></asp:Literal>
        </b>
        </font>
        </td>
    </tr>
    <tr>
    <td align="center" height="20">
    </td>
  </tr>
  </table>
  </center>
 </div>
   
<script language="JavaScript">
<!--
 
if(document.getElementById("TempOK").style.display=='block')
    {
    document.frmConfirmtemplate.ConfirmTemplateSubmit0.focus ();
    document.getElementById("divError").style.display='none';
    }
    //Added for FB 1425 QA Bug START
    if('<%=Application["Client"].ToString().ToUpper()%>' =="MOJ")
	document.getElementById("trPart").style.display ="none";
	//Added for FB 1425 QA Bug END

document.onkeydown = function(evt) {
    evt = evt || window.event;
    var keyCode = evt.keyCode;
    if (keyCode == 8) {
        if (document.getElementById("btnCancel") != null) { // backspace
            var str = document.activeElement.type;
            if (!(str == "text" || str == "textarea" || str == "password")) {
                document.getElementById("btnCancel").click();
                return false;
            }
        }
    }
    fnOnKeyDown(evt);
};

//-->
</script>
<%--ZD 100420--%>
<script type="text/javascript">
    document.getElementById('BtnEdit').setAttribute("onblur", "document.getElementById('btnCancel').focus(); document.getElementById('btnCancel').setAttribute('onfocus', '');"); // ZD 100369
</script>
<%--ZD 100420--%>
<%--code added for Soft Edge button--%>
<script type="text/javascript" src="inc/softedge.js"></script>
<!-- #INCLUDE FILE="inc/mainbottomNET.aspx" -->			


</body>
</html>
