/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Xml;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExpertPdf.HtmlToPdf;
using System.Web.UI.HtmlControls;
using System.Text.RegularExpressions;
using System.Text;
using System.IO;
using System.Web;
using System.Collections;


/// <summary>
/// Summary description for ConferenceOrders
/// </summary>
/// 
namespace ns_ConferenceList
{
    public partial class ConferenceList : System.Web.UI.Page
    {
        #region Protected Members

        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.DropDownList lstListType;
        //protected System.Web.UI.WebControls.DropDownList lstCalendar;//Added for FB 1428//ZD 101388 Commented
        protected System.Web.UI.WebControls.Label lblConfTypeLabel;
        protected System.Web.UI.WebControls.DataGrid dgConferenceList;
        protected System.Web.UI.HtmlControls.HtmlInputHidden helpPage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtPublic;
        //protected System.Web.UI.HtmlControls.HtmlInputHidden txtCOMConfigPath; ZD 100263_Nov11
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtSearchType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtConferenceSearchType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtSortBy;
        //protected System.Web.UI.HtmlControls.HtmlInputHidden txtASPILConfigPath; ZD 100263_Nov11
        protected System.Web.UI.WebControls.Label lblHeader;
        protected System.Web.UI.WebControls.Label lblTimezone;
        protected System.Web.UI.WebControls.Label lblNoConferences;
        protected System.Web.UI.WebControls.Table tblPage;
        protected System.Web.UI.HtmlControls.HtmlTable tblLists;
        protected System.Web.UI.HtmlControls.HtmlTableRow trGoToLobby;
        protected System.Web.UI.HtmlControls.HtmlInputHidden txtSelectionCount;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnJoinID; // FB 1934
        //Recurrence Fixes to display the insatances count as tooltip
        protected string totalInstancesCnt = "Recurring Conference";
        protected String isCustomEdit = ""; //Code added for FB 1391
        Boolean initload = true;//Code added for p2p Status
        protected System.Web.UI.WebControls.CheckBox Refreshchk;//Code added for p2p Status

        protected System.Web.UI.HtmlControls.HtmlTableCell PublicRSS; //RSS Feed Changes
        protected System.Web.UI.HtmlControls.HtmlTableCell PrivateRSS; //RSS Feed Changes

        protected System.Web.UI.HtmlControls.HtmlTableRow OrgRow; //organization module
        protected System.Web.UI.WebControls.DropDownList drpOrgs; //organization module

        protected System.Web.UI.WebControls.CheckBox chkAllSilo; //FB 2274
        protected System.Web.UI.HtmlControls.HtmlTableCell tdchkSilo; 
        protected System.Web.UI.HtmlControls.HtmlTableCell tdOrgName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnChkSilo; //FB 2382

        protected System.Web.UI.HtmlControls.HtmlTableRow PrintRow; //FB 2670        
        //protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAscendingOrder;//FB 2822 ZD 101333
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSortingOrder;
        bool isSearchConf = false; //FB 2763
		//protected System.Web.UI.HtmlControls.HtmlTableRow trlstCalendar;//FB 2968//ZD 101388 Commented
        //FB 2639 - Search Start
        string Filtertype = "";
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnFiltertype;
        //FB 2639 - Search End
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstrname;//ZD 100642
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnConfid; //ZD 101315 - Extend Conf
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMins;//ZD 101315 - Extend Conf
		//ZD 101233 Starts
        protected int hasView = 0;
        protected int hasEdit = 0;
        protected int hasDelete = 0;
        protected int hasManage = 0;
        protected int hasClone = 0;
        protected int hasMCUInfo = 0;
        protected int hasExtendTime = 0;
		//ZD 101233 End
        //ZD 101597
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEditForm; 
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTempID; 
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEditID;

        protected int enableCloudInstallation = 0;//ZD 101719
        protected int enableWaitList = 0;//ZD 102532
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnBrowserLang;//ZD 103174

        
        #endregion

        #region Private variables

        myVRMNet.NETFunctions obj;
        ns_InXML.InXML objInXML;
        static int totalPages;
        static int pageNo;
        DataSet ds = new DataSet();
        ns_MyVRMNet.vrmConfStatus confStatus;
        ns_Logger.Logger log;
        String tformat = "hh:mm tt";
        protected String language = ""; //FB 1830
        String SortingOrder = "0";//FB 2822
        bool isViewUser = false;//FB 1522
        public int VMRType = 0, isVMR = 0; //FB 2550
        public int Sortorders = 0;//103901



        DataTable dtCustom = new DataTable(); //FB 2670
        MyVRMNet.LoginManagement loginMgmt = null;

        public ConferenceList()
        {
            //
            // TODO: Add constructor logic here
            //
            obj = new myVRMNet.NETFunctions();
            objInXML = new ns_InXML.InXML();
            log = new ns_Logger.Logger();
            confStatus = new ns_MyVRMNet.vrmConfStatus();
            loginMgmt = new MyVRMNet.LoginManagement();//ZD 101846
        }
        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                //ZD 101714 //ZD 103531 - Start
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "fr-CA";                
            }
            else
            {
                if (Request.QueryString["lang"] != null)
                {
                    string browserlang = Request.QueryString["lang"];                    
                    Session["browserlang"] = Request.QueryString["lang"];
                    if (browserlang.Contains("en-US") || browserlang.Contains("en"))
                    {                        
                        UICulture = "en";
                        Culture = "en";
                        HttpContext.Current.Session.Add("languageID", "1");
                        Session["TranslationText"] = Cache["EnglishTransText"];
                    }
                    if (browserlang.Contains("fr-CA") || browserlang.Contains("fr"))
                    {                        
                        UICulture = "fr-CA";
                        Culture = "fr-CA";
                        HttpContext.Current.Session.Add("languageID", "5");                        
                        Session["TranslationText"] = Cache["FrenchTransText"];
                    }
                    if (browserlang.Contains("es") || browserlang.Contains("es-US"))
                    {                        
                        UICulture = "es-US";
                        Culture = "es-US";
                        HttpContext.Current.Session.Add("languageID", "3");
                        Session["TranslationText"] = Cache["SpanishTransText"];
                    }
                }
                else
                {
                    UICulture = "en";
                    Culture = "en";
                    HttpContext.Current.Session.Add("languageID", "1");
                    Session["TranslationText"] = Cache["EnglishTransText"];
                }
            }
            base.InitializeCulture();
			//ZD 103531 - End
        }
        #endregion

        #region Web Form Designer generated code
        
        override protected void OnInit(EventArgs e)
        {
            //
            // CODEGEN: This call is required by the ASP.NET Web Form Designer.
            //
            //InitializeComponent();
            //base.OnInit(e);
        }

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            //            this.dgConferenceList.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.GetInstances);
            //            this.dgConferenceApproveList.ItemCommand += new System.Web.UI.WebControls.DataGridCommandEventHandler(this.GetInstances);
            //            this.Load += new System.EventHandler(this.Page_Load);
        }

        //FB 1607 Start
        void dgConferenceList_ItemCreated(object sender, DataGridItemEventArgs e) 
        {
            if (e.Item.ItemType == ListItemType.Header)
                ((Label)e.Item.FindControl("lblDtTimeHeader")).Text = obj.GetTranslatedText("Time Remaining"); //FB 2272
            if(e.Item.FindControl("btnSortDateTime") != null)
                ((LinkButton)e.Item.FindControl("btnSortDateTime")).Visible = false;
        }
        //FB 1607 End

        #endregion

        #region Page_Load
        private void Page_Load(object sender, System.EventArgs e)
        {
            string confChkArg = string.Empty;
            try
            {
                // ZD 100263 Starts
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheckNoSession(Request.Url.AbsoluteUri.ToLower());

                loginMgmt.SetSiteThemes();//ZD 101846
                //if (Session["Admin"] != null)
                //    if (Session["Admin"].ToString().Equals("0"))
                //        Response.Redirect("ShowError.aspx");

                ListItem itemToRemove = null;//ZD 101233

                if (Request.QueryString["t"] != null)
                    confChkArg = Request.QueryString["t"].ToString().Trim();
                if (confChkArg != "4" && Session["roomCascadingControl"] == null)
                    Response.Redirect("genlogin.aspx");
                //if (confChkArg != "7" && confChkArg != "3") // ZD 101233
                    //confChkArg = "";

                //if (Session["isExpressUserAdv"] != null)
                //    if (Session["isExpressUserAdv"].ToString().Equals("1") && confChkArg == "7")
                //        Response.Redirect("ShowError.aspx");

                obj.AccessConformityCheck("conferencelist.aspx?t=" + confChkArg);
                //obj.AccessConformityCheck("conferencelist.aspx");
                // ZD 100263 Ends


				//FB 2382 Starts
                if (Request.UrlReferrer == null)
                    Session["CrossSilo"] = "0";

                if (Session["CrossSilo"] != null && (Session["CrossSilo"].ToString() == "1" || hdnChkSilo.Value == "1"))
                    chkAllSilo.Checked = true;
                else
                    chkAllSilo.Checked = false;

                //FB 2639 - Search Start
                if (hdnFiltertype.Value != "")
                    Filtertype = hdnFiltertype.Value;
                //FB 2639 - Search End

                if(hdnChkSilo.Value == "0")
                    chkAllSilo.Checked = false;
                //FB 2382 Ends

                //FB 1607 Start
                if (Request.QueryString["t"] != null)
                      if(Request.QueryString["t"].ToString() == "2")
                           dgConferenceList.ItemCreated += new DataGridItemEventHandler(dgConferenceList_ItemCreated);
                //FB 1607 End
                //FB 1830
                if (Session["language"] == null)
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
                
                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                Session["FormatDateType"] = ((Session["FormatDateType"] == null) ? "MM/dd/yyyy" : Session["FormatDateType"]);//FB 1906
                Session["timeZoneDisplay"] = ((Session["timeZoneDisplay"] == null) ? "1" : Session["timeZoneDisplay"]);
                //tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");//FB 2588
                //FB 2588 STARTS
                if (Session["timeFormat"].ToString().Equals("0"))
                    tformat = "HH:mm";
                else if (Session["timeFormat"].ToString().Equals("1"))
                    tformat = "hh:mm tt";
                else if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";
                //FB 2588 ENDS

                //ZD 102532
                if (Session["EnableWaitList"] != null && Session["EnableWaitList"].ToString() != "")
                {
                    int.TryParse(Session["EnableWaitList"].ToString(), out enableWaitList);
                }

                //FB 1428
                if (Application["Client"] == null)
                    Application["Client"] = "";
                //Response.Write(Session["userEmail"].ToString());
                errLabel.Visible = false;

                //code added for RSS --- Start
                PublicRSS.Visible = false;
                PrivateRSS.Visible = true;

                if (Request.QueryString["frm"] != null) //FB 2763
                    if (Request.QueryString["frm"].ToString().Equals("1"))
                    {
                        isSearchConf = true;
                        trGoToLobby.Visible = true;
                    }

                if (Request.QueryString["hf"] != null)
                    if (Request.QueryString["hf"].ToString().Equals("1"))
                    {
                        PublicRSS.Visible = true;
                    }
                //code added for RSS -- End
                isViewUser = obj.ViewUserRole();//FB 1522
                if (hdnSortingOrder.Value == "1")
                    SortingOrder = "1";
                else
                    SortingOrder = "0";

                //FB 2968 START
				//ZD 101388 Commented Starts
                /*if (Request.QueryString["t"] != null)
                    if (Request.QueryString["t"].ToString().Equals("1"))
                    {
                        if (Session["isExpressUser"] != null && Session["isExpressManage"] != null)//FB 2968
                        {
                            if (Session["isExpressUser"].ToString() == "1" && Session["isExpressManage"].ToString() == "0")
                            {
                                itemToRemove = lstCalendar.Items.FindByValue("1");//ZD 101233
                                if (itemToRemove != null)
                                    lstCalendar.Items.Remove(itemToRemove);

                                itemToRemove = lstCalendar.Items.FindByValue("3");
                                if (itemToRemove != null)
                                    lstCalendar.Items.Remove(itemToRemove);

                                lstCalendar.Enabled = false;  //ZD 101022
                            }

                        }
                        if (Session["hasCalendar"] != null) //FB 3055
                            if (Session["hasCalendar"].ToString().ToLower().Trim() == "false")
                                lstCalendar.Enabled = false; //ZD 101022
                    }*/
				//ZD 101388 Commented End
                //FB 2968 END


                //ZD 101233 Starts
                itemToRemove = lstListType.Items.FindByValue("2");
                if (itemToRemove != null && Session["hasOngoing"] != null && Session["hasOngoing"].ToString() == "0")
                    lstListType.Items.Remove(itemToRemove);
                itemToRemove = lstListType.Items.FindByValue("3");
                if (itemToRemove != null && Session["hasReservations"] != null && Session["hasReservations"].ToString() == "0")
                    lstListType.Items.Remove(itemToRemove);
                itemToRemove = lstListType.Items.FindByValue("4");
                if (itemToRemove != null && Session["hasPublic"] != null && Session["hasPublic"].ToString() == "0")
                    lstListType.Items.Remove(itemToRemove);
                itemToRemove = lstListType.Items.FindByValue("5");
                if (itemToRemove != null && Session["hasPending"] != null && Session["hasPending"].ToString() == "0")
                    lstListType.Items.Remove(itemToRemove);
                itemToRemove = lstListType.Items.FindByValue("6");
                if (itemToRemove != null && Session["hasApproval"] != null && Session["hasApproval"].ToString() == "0")
                    lstListType.Items.Remove(itemToRemove);
                itemToRemove = lstListType.Items.FindByValue("7");
                if (itemToRemove != null && Session["hasConferenceSupport"] != null && Session["hasConferenceSupport"].ToString() == "0")
                    lstListType.Items.Remove(itemToRemove);
                itemToRemove = lstListType.Items.FindByValue("8");
                if (itemToRemove != null && Session["hasOnMCU"] != null && Session["hasOnMCU"].ToString() == "0")
                    lstListType.Items.Remove(itemToRemove);
                //ZD 101233 End
                //ZD 102532
                itemToRemove = lstListType.Items.FindByValue("11");
                if (itemToRemove != null && Session["hasWaitList"] != null && Session["hasWaitList"].ToString() == "0")
                    lstListType.Items.Remove(itemToRemove);

                if (!IsPostBack)
                {
                    /* Fogbugz case number 32 */
                    if (Session["userID"] == null)
                        Session.Add("userID", "11");
                    if (Session["admin"] == null)
                        Session.Add("admin", "2");
                    if (Session["systemDate"] == null)
                    {
                        Session.Add("systemDate", DateTime.Now.ToString("MM/dd/yyyy"));
                        Session.Add("systemTime", DateTime.Now.ToString(tformat));
                    }
                    /* Case 32 changes end here */
                    
                    OrgRow.Attributes.Add("Style", "Display:None"); //organization module

                    hdnConfid.Value = ""; //ZD 101315 - Extend Conf
                    hdnMins.Value = ""; //ZD 101315 - Extend Conf

                    if (Request.QueryString["hf"] != null)
                        if (Request.QueryString["hf"].ToString().Equals("1"))
                        {
                            tblLists.Visible = false;
                            PublicRSS.Visible = true; //RSS Change
                            PrivateRSS.Visible = false; //RSS Change
                            Session.Add("userID", "11");
                            Application.Add("COM_ConfigPath", "C:\\VRMSchemas_v1.8.3\\COMConfig.xml");//ZD 100263_Nov11
                            Application.Add("MyVRMServer_ConfigPath", "C:\\VRMSchemas_v1.8.3\\");//ZD 100263_Nov11
                            //FB 2027
                            //obj.GetSystemDateTime(Application["COM_ConfigPath"].ToString());
                            obj.GetSystemDateTime(Application["MyVRMServer_ConfigPath"].ToString());
                            
                            if(Session["timezoneDisplay"].ToString() == "1")
                                lblTimezone.Visible = true;
                            lblTimezone.Text = (String)Session["systemTimeZone"];   // FB 1627
                            trGoToLobby.Visible = true;

                            OrgRow.Attributes.Add("Style", "Display:Block"); //organization module
                            obj.BindOrganizationNames(drpOrgs);
                            Session["OrganizationID"] = drpOrgs.SelectedValue;
                        }
                    PrintRow.Visible = false; //FB 2670
                    if (Request.QueryString["t"] != null)
                    {
                        switch (Request.QueryString["t"].ToString())
                        {
                            case "1":
                                helpPage.Value = "88";
                                txtPublic.Value = "";
                                BindDataFromSearch();
                                chkAllSilo.Visible = false; //FB 2274
                                tdchkSilo.Visible = false;
                                PrintRow.Visible = true; //FB 2670
                                break;
                            case "2": // Ongoing
                                helpPage.Value = "110";
                                txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled + "," + ns_MyVRMNet.vrmConfStatus.Ongoing; //ZD 100093
                                txtPublic.Value = "";
                                txtConferenceSearchType.Value = "1";
                                chkAllSilo.Visible = true; //FB 2274
                                tdchkSilo.Visible = true;
                                Filtertype = ns_MyVRMNet.vrmSearchFilterType.Ongoing; //FB 2639 - Search
                                BindReservations(txtConferenceSearchType.Value);
                                lblHeader.Text = obj.GetTranslatedText("Ongoing Conferences"); //FB 2570
                                break;
                            case "3": // Reservations
                                helpPage.Value = "110";
                                txtPublic.Value = "";
                                txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled; //ZD 100036
                                //txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled + "," + ns_MyVRMNet.vrmConfStatus.OnMCU;
                                txtConferenceSearchType.Value = "0";
                                Filtertype = ns_MyVRMNet.vrmSearchFilterType.Reservation;//FB 2639 - Search
                                BindReservations(txtConferenceSearchType.Value);
                                lblHeader.Text = obj.GetTranslatedText("Reservations");//2570
                                break;
                            case "4": // Public
                                if (Session["organizationID"] == null)
                                    Session["organizationID"] = "11"; //default conference
                                
                                PrivateRSS.Visible = false; //RSS Change
                                
                                helpPage.Value = "110";
                                txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled; // fogbugz case 32 //ZD 100036
                                //txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled + "," + ns_MyVRMNet.vrmConfStatus.OnMCU; // fogbugz case 32
                                txtPublic.Value = "1";
                                txtConferenceSearchType.Value = "";
                                Filtertype = ns_MyVRMNet.vrmSearchFilterType.Public;//FB 2639 - Search
                                BindReservations(txtConferenceSearchType.Value);
                                chkAllSilo.Visible = false; //FB 2274
                                tdchkSilo.Visible = false;
                                lblHeader.Text = obj.GetTranslatedText("Public Conferences");//2570
                                break;
                            case "5": // Pending
                                helpPage.Value = "110";
                                txtPublic.Value = "";
                                txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Pending;
                                txtConferenceSearchType.Value = "";
                                Filtertype = ns_MyVRMNet.vrmSearchFilterType.Pending;//FB 2639 - Search
                                BindReservations(txtConferenceSearchType.Value);
                                chkAllSilo.Visible = false; //FB 2274
                                tdchkSilo.Visible = false;
                                lblHeader.Text = obj.GetTranslatedText("Pending Conferences");//FB 2570
                                break;
                            case "6": // Approval
                                helpPage.Value = "110";
                                txtPublic.Value = "";
                                txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Pending;
                                txtConferenceSearchType.Value = "";
                                Filtertype = ns_MyVRMNet.vrmSearchFilterType.ApprovalPending;//FB 2639 - Search
                                BindReservations(txtConferenceSearchType.Value);
                                chkAllSilo.Visible = false; //FB 2274
                                tdchkSilo.Visible = false;
                                lblHeader.Text = obj.GetTranslatedText("Approval Pending");//FB 2570
                                break;
                            //FB 2501 VNOC Start
                            case "7": //Conference Support
                                helpPage.Value = "110";
                                txtPublic.Value = "";
                                txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled + "," + ns_MyVRMNet.vrmConfStatus.OnMCU + "," + ns_MyVRMNet.vrmConfStatus.Ongoing;
                                txtConferenceSearchType.Value = "";//FB 2639 - Search
                                Filtertype = ns_MyVRMNet.vrmSearchFilterType.Congiere;//FB 2639 - Search
                                BindReservations(txtConferenceSearchType.Value);
                                lblHeader.Text = obj.GetTranslatedText("Conference Support");//FB 2570//FB 2844 to change VNOC to concierge conference //ZD 101092
                                break;
                            //FB 2501 VNOC End
                            case "8": // On MCU //ZD 100036
                                helpPage.Value = "110";
                                txtPublic.Value = "";
                                txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.OnMCU;
                                txtConferenceSearchType.Value = "1";
                                Filtertype = ns_MyVRMNet.vrmSearchFilterType.OnMCU;
                                BindReservations(txtConferenceSearchType.Value);
                                lblHeader.Text = obj.GetTranslatedText("On MCU");
                                break;
                            case "11": // Wait List //ZD 102532
                                helpPage.Value = "110";
                                txtPublic.Value = "";
                                txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.WaitList; 
                                txtConferenceSearchType.Value = "0";
                                Filtertype = ns_MyVRMNet.vrmSearchFilterType.WaitList;
                                BindReservations(txtConferenceSearchType.Value);
                                lblHeader.Text = obj.GetTranslatedText("Wait List Conferences");
                                break;
                            default:
                                errLabel.Text = obj.GetTranslatedText("Illegal Search");//FB 1830 - Translation
                                break;
                        }
                    }
                    if (Request.QueryString["m"] != null)
                        if (Request.QueryString["m"].ToString().Equals("1"))
                        {
                            errLabel.Text = obj.GetTranslatedText("Operation Successful!");//FB 1830 - Translation
                            errLabel.Visible = true;
                        }
                }
                else
                {
                   
                }
                //Added for FB 1428 Start
                if (Application["Client"].ToString().ToUpper() == "MOJ")
                {
                    lblConfTypeLabel.Text = "Hearing Filter";
                    lblHeader.Text = "List of Hearing";
                    //lstCalendar.Items[1].Text = "Room Hearing Calendar";//ZD 101388 Commented
                    //lstCalendar.Items[2].Text = "Personal Hearing Calendar";
                }
				//FB 2392
                if (Session["EnablePublicRooms"] != null)
                {
                    if (Session["EnablePublicRooms"].ToString() == "1") //FB 2594
                    {
                        itemToRemove = lstListType.Items.FindByValue("6");//ZD 101233
                        if (itemToRemove != null)
                            lstListType.Items.Remove(itemToRemove);
                    }
                }
                //Added for FB 1428 End
                //ZD 101388 - Starts - Commented
                //FB 2670 Starts
                //if (Session["admin"].ToString() != "2" && Session["admin"].ToString() != "3")
                //{
                //    itemToRemove  = lstListType.Items.FindByValue("7");//ZD 101233
                //    if (itemToRemove != null)
                //        lstListType.Items.Remove(itemToRemove);
                //}
                //FB 2670 Ends
                //ZD 101388 - End - Commented
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "PageLoad: " + ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("Page_Load:" + ex.Message);//ZD 100263
            }

        }
        #endregion

        #region BindDataFromSearch
        /// <summary>
        /// BindDataFromSearch
        /// </summary>
        protected void BindDataFromSearch()
        {
            try
            {
                String pageNoS = "1";
                if (Request.QueryString["pageNo"] != null)
                    pageNoS = Request.QueryString["pageNo"].ToString().Trim();
                XmlDocument xmldocInXML = new XmlDocument();
                //Response.Write(obj.Transfer(Session["inXML"].ToString()));
                xmldocInXML.LoadXml(Session["inXML"].ToString());
                String pageNoInInXML = xmldocInXML.SelectSingleNode("//SearchConference/PageNo").InnerText;
                String sortBy = xmldocInXML.SelectSingleNode("//SearchConference/SortBy").InnerText;
                String SortingValue = xmldocInXML.SelectSingleNode("//SearchConference/SortingOrder").InnerText;//FB 2822
                Session["inXML"] = Session["inXML"].ToString().Replace("<PageNo>" + pageNoInInXML + "</PageNo>", "<PageNo>" + pageNoS + "</PageNo>");
                if (Request.QueryString["sortBy"] != null)
                {
                    if (Request.QueryString["sortBy"].ToString().Equals(txtSortBy.Value) || txtSortBy.Value.Equals(""))
                        txtSortBy.Value = Request.QueryString["sortBy"].ToString();
                    else
                        txtSortBy.Value = txtSortBy.Value;
                } //ZD 103901
                if (Request.QueryString["sortorder"] != null)
                {
                    if (Sortorders == 1)
                        hdnSortingOrder.Value = hdnSortingOrder.Value;
                    else
                        hdnSortingOrder.Value = Request.QueryString["sortorder"].ToString();
                    Sortorders = 0;
                }
                if (!txtSortBy.Value.Trim().Equals(""))
                    Session["inXML"] = Session["inXML"].ToString().Replace("<SortBy>" + sortBy + "</SortBy>", "<SortBy>" + txtSortBy.Value + "</SortBy>");

                Session["inXML"] = Session["inXML"].ToString().Replace("<SortingOrder>" + SortingValue + "</SortingOrder>", "<SortingOrder>" + SortingOrder + "</SortingOrder>");//FB 2822
                
                String outXML = obj.CallMyVRMServer("SearchConference", Session["inXML"].ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                //Response.Write(obj.Transfer(Session["inXML"].ToString()));
                //Response.Write("<hr>" + obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") < 0)
                {
                    dgConferenceList.DataSource = null;//FB 2274
                    dgConferenceList.DataBind(); //FB 2274
                    lstListType.Visible = false;
                    lblConfTypeLabel.Visible = false;
                    PrivateRSS.Visible = false; //RSS Change
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
                    LoadConferenceList(nodes, dgConferenceList, null);//ZD 100642
                    if (nodes.Count > 0)
                    {
                        ShowHide();//FB 2822
                        Label lblTemp = new Label();
                        DataGridItem dgFooter = (DataGridItem)dgConferenceList.Controls[0].Controls[dgConferenceList.Controls[0].Controls.Count - 1];
                        lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                        //Response.Write(xmldoc.SelectSingleNode("//SearchConference/TotalRecords").InnerText);
                        lblTemp.Text = xmldoc.SelectSingleNode("//SearchConference/TotalRecords").InnerText;



                        totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//SearchConference/TotalPages").InnerText);
                        pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//SearchConference/PageNo").InnerText);
                        if (totalPages > 1)
                        {
                            //  Request.QueryString.Remove("pageNo");
                            string qString = Request.QueryString.ToString();
                            if (Request.QueryString["pageNo"] != null)
                                qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                            if (Request.QueryString["m"] != null)
                                qString = qString.Replace("&m=" + Request.QueryString["m"].ToString(), "");
                            obj.DisplayPaging(totalPages, pageNo, tblPage, "ConferenceList.aspx?" + qString);
                            DisplaySort();
                        }
                        foreach (DataGridItem dgi in dgConferenceList.Items)
                        {
                            if ((LinkButton)dgi.FindControl("btnViewDetails") != null)
                                ((LinkButton)dgi.FindControl("btnViewDetails")).Attributes.Add("onclick", "javascript:viewconf('" + dgi.Cells[0].Text + "');return false;");
                        }
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                helpPage.Value = "88";
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = "BindData: " + ex.StackTrace;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindDataFromSearch:" + ex.Message);//ZD 100263
            }
        }
        #endregion

        #region BindReservations
        /// <summary>
        /// BindReservations
        /// </summary>
        /// <param name="SearchType"></param>
        protected void BindReservations(String SearchType)
        {
            try
            {
                //FB 2274  Starts
                objInXML.isAllSilo = false;

                //ZD 101233 Starts
                int filterType = 0;
                if (Request.QueryString["t"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["t"].ToString()))
                    int.TryParse(Request.QueryString["t"].ToString(), out filterType);
                hasView = 0;
                hasEdit = 0;
                hasDelete = 0;
                hasManage = 0;
                hasClone = 0;
                hasMCUInfo = 0;
                hasExtendTime = 0;
                if (filterType > 1)
                {
                    obj.CheckConferenceRights(filterType, ref hasView, ref hasManage, ref hasExtendTime, ref hasMCUInfo, ref hasEdit, ref hasDelete, ref hasClone);

                    ViewState["hasView"] = hasView.ToString();
                    ViewState["hasManage"] = hasManage.ToString();
                    ViewState["hasExtendTime"] = hasExtendTime.ToString();
                    ViewState["hasMCUInfo"] = hasMCUInfo.ToString();
                    ViewState["hasEdit"] = hasEdit.ToString();
                    ViewState["hasDelete"] = hasDelete.ToString();
                    ViewState["hasClone"] = hasClone.ToString();
                }
                //ZD 101233 End

                if (Session["UsrCrossAccess"] != null)
                {
                    if (chkAllSilo.Checked && Session["organizationID"].ToString() == "11" && Session["UsrCrossAccess"].ToString() == "1")
                        objInXML.isAllSilo = true;
                }
                if (Session["UsrCrossAccess"] != null)
                {
                    if (Session["UsrCrossAccess"].ToString().Equals("1") && Session["organizationID"].ToString() == "11" && Session["OrganizationsLimit"].ToString().Trim()!= "1")
                    {
                        chkAllSilo.Visible = true;
                        tdchkSilo.Visible = true;
                    }
                    else
                    {
                        chkAllSilo.Visible = false;
                        tdchkSilo.Visible = false;
                    }
                }
                //FB 2274 ends
                lstListType.Visible = true;

                //FB 2968 START
                //ZD 101388 Commented Starts
                /*if (Session["isExpressUser"] != null && Session["isExpressManage"] != null)//FB 1779
                {
                    if (Session["isExpressUser"].ToString() == "1" && Session["isExpressManage"].ToString() == "0")
                    {
                        //FB 2585 Start
                        ListItem itemToRemove = lstCalendar.Items.FindByValue("1");
                        if (itemToRemove != null)
                            lstCalendar.Items.Remove(itemToRemove);

                        itemToRemove = lstCalendar.Items.FindByValue("3");
                        if (itemToRemove != null)
                            lstCalendar.Items.Remove(itemToRemove);
                        //FB 2585 End
                        //lstListType.Enabled = false; //ZD 100819
                        lstCalendar.Enabled = false;//ZD 101022
                    }
                        
                }*/
                //ZD 101388 Commented End


                //ZD 100819 Start

                //if (Session["isExpressUser"] != null && Session["isExpressManage"] != null && Session["isExpressUserAdv"] != null)//FB 1779
                //{
                //    if (Session["isExpressUser"].ToString() == "1" && Session["isExpressManage"].ToString() == "1" && Session["isExpressUserAdv"].ToString() == "0")
                //        lstListType.Enabled = false;
                //}
                //FB 2968 END
                //ZD 101388 Commented Starts
                /*if (Session["isExpressUser"] != null && Session["isExpressManage"] != null && Session["isExpressUserAdv"] != null)//FB 1779
                {
                    if ((Session["isExpressUser"].ToString() == "1" || Session["isExpressManage"].ToString() == "1") && Session["isExpressUserAdv"].ToString() == "0")
                    {
                        ListItem itemToRemove = lstListType.Items.FindByValue("8");
                        if (itemToRemove != null)
                            lstListType.Items.Remove(itemToRemove);
                        itemToRemove = lstListType.Items.FindByValue("4");
                        if (itemToRemove != null)
                            lstListType.Items.Remove(itemToRemove);
                        itemToRemove = lstListType.Items.FindByValue("5");
                        if (itemToRemove != null)
                            lstListType.Items.Remove(itemToRemove);
                        itemToRemove = lstListType.Items.FindByValue("6");
                        if (itemToRemove != null)
                            lstListType.Items.Remove(itemToRemove);
                        itemToRemove = lstListType.Items.FindByValue("7");
                        if (itemToRemove != null)
                            lstListType.Items.Remove(itemToRemove);
                    }
                }*/
                //ZD 101388 Commented End

                //ZD 100819 End

                lstListType.ClearSelection();
                lstListType.Items.FindByValue(Request.QueryString["t"].ToString()).Selected = true;
                //lblHeader.Text = lstListType.SelectedItem.Text.Split(' ')[1]; // "Reservation(s)";' //FB 2570 Starts
                //if (lstListType.SelectedItem.Text.Split(' ').Length > 1)
                //    lblHeader.Text = obj.GetTranslatedText(lstListType.SelectedItem.Text.Split(' ')[1]);
                //else
                //    lblHeader.Text = obj.GetTranslatedText(lstListType.SelectedItem.Text);

                if (!SearchType.Equals("0"))
                {
                //    //Added for FB 1428 Start
                //    if (Application["Client"].ToString().ToUpper() == "MOJ")
                //        lblHeader.Text += obj.GetTranslatedText(" Hearing");
                //    else
                //        //Added for FB 1428 End
                //        lblHeader.Text += obj.GetTranslatedText(" Conferences");//FB 1830 - Translation
                }
                else SearchType = ""; //FB 2570 Ends
                //String inXML = "<login><userID>" + Session["userID"].ToString() + "</userID></login>";
                //String outXML = obj.CallCOM("GetAccountFuture", inXML, Application["COM_ConfigPath"].ToString());
                String PageNo = "1";
                if (Request.QueryString["pageNo"] != null)
                    PageNo = Request.QueryString["pageNo"].ToString();

                //Response.Write("<br>here" + txtSortBy.Value);
                if (Request.QueryString["sortBy"] != null)
                {
                    if (Request.QueryString["sortBy"].ToString().Equals(txtSortBy.Value) || txtSortBy.Value.Equals(""))
                        txtSortBy.Value = Request.QueryString["sortBy"].ToString();
                    else
                        txtSortBy.Value = txtSortBy.Value;
                }
                //ZD 103901
                if (Request.QueryString["sortorder"] != null)
                {
                    if (Sortorders == 1)
                        hdnSortingOrder.Value = hdnSortingOrder.Value;
                    else
                       hdnSortingOrder.Value = Request.QueryString["sortorder"].ToString();

                    SortingOrder = hdnSortingOrder.Value;
                    Sortorders = 0;
                }
                //Response.Write(txtSortBy.Value);
                if (txtSortBy.Value.Equals(""))
                    txtSortBy.Value = "3";

                hdnFiltertype.Value = Filtertype; //FB 2639_Search

                String inXML = objInXML.SearchConference(Session["userID"].ToString(), txtSearchType.Value, "", "", "", "", SearchType, "", "", "", "", txtPublic.Value, "1", "", PageNo, txtSortBy.Value, "1", "", "", "", SortingOrder,"", false, Filtertype,""); //Custom Attribute Fixes //FB 2632//FB 2694 //FB 2870 //FB 3006 //FB 2639 - Search TIK# 100037
                if (Request.QueryString["t"] != null)
                    if (Request.QueryString["t"].ToString().Equals("2") || Request.QueryString["t"].ToString().Equals("8")) //ZD 100036
                        inXML = objInXML.SearchConference(Session["userID"].ToString(), txtSearchType.Value, "", "", "", "", SearchType, "", "", "", "", txtPublic.Value, "1", "", PageNo, txtSortBy.Value, "0", "", "", "", SortingOrder, "", false, Filtertype,"");    //Custom Attribute Fixes //FB 2632//FB 2694 //FB 2870 //FB 3006 //FB 2639 - Search TIK# 100037
                // fogbugz case 295 Saima Code Change to display all public recurring conference instances at one
                if (Request.QueryString["hf"] != null)
                    if (Request.QueryString["hf"].ToString().Equals("1"))
                        inXML = objInXML.SearchConference(Session["userID"].ToString(), txtSearchType.Value, "", "", "", "", SearchType, "", "", "", "", txtPublic.Value, "1", "", PageNo, txtSortBy.Value, "1", "", "", "", SortingOrder, "", false, Filtertype,"");    //Custom Attribute Fixes //FB 2632//FB 2694 //FB 2870 //FB 3006 //FB 2639 - Search TIK# 100037
                //FB 2501 VNOC Start
                if (Request.QueryString["t"] != null)
                    if (Request.QueryString["t"].ToString().Equals("7"))
                        inXML = objInXML.SearchConference(Session["userID"].ToString(), txtSearchType.Value, "", "", "", "", SearchType, "", "", "", "", txtPublic.Value, "3", "", PageNo, txtSortBy.Value, "1", "", "", "", SortingOrder, "", false, Filtertype,""); //FB 2632 //FB 2694 //FB 2870 //FB 3006 //FB 2639 - Search TIK# 100037
                //FB 2501 VNOC End
                //String outXML = "<SearchConference><Conferences><Conference><ConferenceID>494,1</ConferenceID><ConferenceUniqueID>1939</ConferenceUniqueID><ConferenceName>sss</ConferenceName><ConferenceType>7</ConferenceType><ConferenceDateTime>4/20/2008 7:50 PM</ConferenceDateTime><ConferenceDuration>60</ConferenceDuration><ConferenceStatus>5</ConferenceStatus><OpenForRegistration>1</OpenForRegistration><ConferenceHost>spr spr</ConferenceHost><IsHost>0</IsHost><IsParticipant>1</IsParticipant><IsRecur>0</IsRecur><Location></Location></Conference><Conference><ConferenceID>495,1</ConferenceID><ConferenceUniqueID>1940</ConferenceUniqueID><ConferenceName>pub conf</ConferenceName><ConferenceType>7</ConferenceType><ConferenceDateTime>4/20/2008 7:58 PM</ConferenceDateTime><ConferenceDuration>60</ConferenceDuration><ConferenceStatus>5</ConferenceStatus><OpenForRegistration>0</OpenForRegistration><ConferenceHost>helen Anderson</ConferenceHost><IsHost>0</IsHost><IsParticipant>1</IsParticipant><IsRecur>0</IsRecur><Location></Location></Conference></Conferences><Timezone>Eastern Time </Timezone><PageNo>1</PageNo><TotalPages>1</TotalPages><TotalRecords>2</TotalRecords><SortBy>3</SortBy></SearchConference>"; // obj.CallMyVRMServer("SearchConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("SearchConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //outXML = "<SearchConference><Conferences><Conference><ConferenceID>1077,1</ConferenceID><ConferenceUniqueID>5705</ConferenceUniqueID><ConferenceName>test saima 1</ConferenceName><ConferenceType>2</ConferenceType><ConferenceDateTime>12/29/2008 3:00 PM</ConferenceDateTime><ConferenceDuration>60</ConferenceDuration><ConferenceStatus>1</ConferenceStatus><OpenForRegistration>0</OpenForRegistration><ConferenceHost>VRM Administrator</ConferenceHost><IsHost>1</IsHost><IsParticipant>0</IsParticipant><IsRecur>0</IsRecur><Location><Selected><ID>1</ID><Name>AK A ITC R1015</Name></Selected><Selected><ID>2</ID><Name>AK ITC R1006</Name></Selected></Location><ApprovalPending><Entities><Level>1</Level><Entity><Name>AK A ITC R1015</Name><ID>1</ID><Message></Message></Entity><Entity><Name>AK ITC R1006</Name><ID>2</ID><Message></Message></Entity></Entities></ApprovalPending></Conference><Conference><ConferenceID>1071,1</ConferenceID><ConferenceUniqueID>5677</ConferenceUniqueID><ConferenceName>test recur for alex again</ConferenceName><ConferenceType>2</ConferenceType><ConferenceDateTime>12/30/2008 12:00 PM</ConferenceDateTime><ConferenceDuration>15</ConferenceDuration><ConferenceStatus>1</ConferenceStatus><OpenForRegistration>0</OpenForRegistration><ConferenceHost>VRM Administrator</ConferenceHost><IsHost>1</IsHost><IsParticipant>0</IsParticipant><IsRecur>1</IsRecur><Location><Selected><ID>2</ID><Name>AK ITC R1006</Name></Selected></Location><ApprovalPending><Entities><Level>1</Level><Entity><Name>AK ITC R1006</Name><ID>2</ID><Message></Message></Entity><Entity><Name>AK ITC R1006</Name><ID>2</ID><Message></Message></Entity></Entities></ApprovalPending></Conference><Conference><ConferenceID>1056,1</ConferenceID><ConferenceUniqueID>5630</ConferenceUniqueID><ConferenceName>ET SCA1</ConferenceName><ConferenceType>2</ConferenceType><ConferenceDateTime>1/13/2009 10:00 AM</ConferenceDateTime><ConferenceDuration>60</ConferenceDuration><ConferenceStatus>1</ConferenceStatus><OpenForRegistration>0</OpenForRegistration><ConferenceHost>VRM Administrator</ConferenceHost><IsHost>1</IsHost><IsParticipant>0</IsParticipant><IsRecur>0</IsRecur><Location><Selected><ID>73</ID><Name>ROOMWithAppr</Name></Selected></Location><ApprovalPending><Entities><Level>1</Level><Entity><Name>ROOMWithAppr</Name><ID>73</ID><Message></Message></Entity></Entities></ApprovalPending></Conference><Conference><ConferenceID>1051,3</ConferenceID><ConferenceUniqueID>5618</ConferenceUniqueID><ConferenceName>Test Recur 1066</ConferenceName><ConferenceType>2</ConferenceType><ConferenceDateTime>2/18/2009 3:00 PM</ConferenceDateTime><ConferenceDuration>60</ConferenceDuration><ConferenceStatus>1</ConferenceStatus><OpenForRegistration>0</OpenForRegistration><ConferenceHost>VRM Administrator</ConferenceHost><IsHost>1</IsHost><IsParticipant>0</IsParticipant><IsRecur>1</IsRecur><Location><Selected><ID>73</ID><Name>ROOMWithAppr</Name></Selected></Location><ApprovalPending><Entities><Level>1</Level><Entity><Name>ROOMWithAppr</Name><ID>73</ID><Message></Message></Entity></Entities></ApprovalPending></Conference><Conference><ConferenceID>982,1</ConferenceID><ConferenceUniqueID>5255</ConferenceUniqueID><ConferenceName>SCTEA Meeting clone</ConferenceName><ConferenceType>2</ConferenceType><ConferenceDateTime>12/5/2009 9:00 AM</ConferenceDateTime><ConferenceDuration>60</ConferenceDuration><ConferenceStatus>1</ConferenceStatus><OpenForRegistration>0</OpenForRegistration><ConferenceHost>Alex Duncan</ConferenceHost><IsHost>0</IsHost><IsParticipant>0</IsParticipant><IsRecur>0</IsRecur><Location></Location><ApprovalPending><Entities><Level>1</Level><Entity><Name>AK A ITC R1015</Name><ID>1</ID><Message></Message></Entity></Entities></ApprovalPending></Conference><Conference><ConferenceID>1078,1</ConferenceID><ConferenceUniqueID>5706</ConferenceUniqueID><ConferenceName>lekjewkr</ConferenceName><ConferenceType>2</ConferenceType><ConferenceDateTime>12/24/2012 3:00 PM</ConferenceDateTime><ConferenceDuration>60</ConferenceDuration><ConferenceStatus>1</ConferenceStatus><OpenForRegistration>0</OpenForRegistration><ConferenceHost>VRM Administrator</ConferenceHost><IsHost>1</IsHost><IsParticipant>0</IsParticipant><IsRecur>0</IsRecur><Location><Selected><ID>1</ID><Name>AK A ITC R1015</Name></Selected><Selected><ID>2</ID><Name>AK ITC R1006</Name></Selected></Location><ApprovalPending><Entities><Level>1</Level><Entity><Name>AK A ITC R1015</Name><ID>1</ID><Message></Message></Entity><Entity><Name>AK ITC R1006</Name><ID>2</ID><Message></Message></Entity></Entities></ApprovalPending></Conference><Conference><ConferenceID>1079,1</ConferenceID><ConferenceUniqueID>5707</ConferenceUniqueID><ConferenceName>lksjdlkfjlksdf</ConferenceName><ConferenceType>2</ConferenceType><ConferenceDateTime>12/24/2013 3:00 PM</ConferenceDateTime><ConferenceDuration>60</ConferenceDuration><ConferenceStatus>1</ConferenceStatus><OpenForRegistration>0</OpenForRegistration><ConferenceHost>VRM Administrator</ConferenceHost><IsHost>1</IsHost><IsParticipant>0</IsParticipant><IsRecur>0</IsRecur><Location><Selected><ID>1</ID><Name>AK A ITC R1015</Name></Selected><Selected><ID>2</ID><Name>AK ITC R1006</Name></Selected></Location><ApprovalPending><Entities><Level>1</Level><Entity><Name>AK A ITC R1015</Name><ID>1</ID><Message></Message></Entity><Entity><Name>AK ITC R1006</Name><ID>2</ID><Message></Message></Entity></Entities></ApprovalPending></Conference></Conferences><Timezone>Eastern Time </Timezone><PageNo>1</PageNo><TotalPages>1</TotalPages><TotalRecords>7</TotalRecords><SortBy>3</SortBy></SearchConference>";
                
                if (Session["inXML"] == null)
                    Session.Add("inXML", inXML);
                else
                    Session["inxML"] = inXML;
                if (outXML.IndexOf("<error>") < 0)
                {
                    dgConferenceList.DataSource = null;//FB 2274
                    dgConferenceList.DataBind();
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    //XmlNodeList nodes = xmldoc.SelectNodes("//account/conferences/conference");
                    Session["systemDate"] = DateTime.Parse(xmldoc.SelectSingleNode("//SearchConference/UserCurrentTime").InnerText).ToShortDateString();// FB 1848
                    Session["systemTime"] = DateTime.Parse(xmldoc.SelectSingleNode("//SearchConference/UserCurrentTime").InnerText).ToShortTimeString();// FB 1848
                    XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences/Conference");
                    LoadConferenceList(nodes, dgConferenceList, null);//ZD 100642
                    if (nodes.Count > 0)
                    {
                        if (dgConferenceList.Items.Count > 0)
                        {
                            ShowHide();//FB 2822
                            Label lblTemp = new Label();
                            DataGridItem dgFooter = (DataGridItem)dgConferenceList.Controls[0].Controls[dgConferenceList.Controls[0].Controls.Count - 1];
                            lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
                            lblTemp.Text = xmldoc.SelectSingleNode("//SearchConference/TotalRecords").InnerText;
                            totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//SearchConference/TotalPages").InnerText);
                            pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//SearchConference/PageNo").InnerText);

                            string qString = Request.QueryString.ToString();

                            if (totalPages > 1)
                            {
                                qString = Request.QueryString.ToString();
                                if (Request.QueryString["pageNo"] != null)
                                    qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                                if (Request.QueryString["sortBy"] != null)
                                    qString = qString.Replace("&sortBy=" + Request.QueryString["sortBy"].ToString(), "");
                                if (Request.QueryString["sortorder"] != null) //103901
                                    qString = qString.Replace("&sortorder=" + Request.QueryString["sortorder"].ToString(), "");
                                if (Request.QueryString["m"] != null)
                                    qString = qString.Replace("&m=" + Request.QueryString["m"].ToString(), "");
                                qString += "&sortBy=" + txtSortBy.Value + "&sortorder=" + hdnSortingOrder.Value; //103901

                                tblPage.Rows.Clear();
                                obj.DisplayPaging(totalPages, pageNo, tblPage, "ConferenceList.aspx?" + qString);
                                DisplaySort();
                                ShowHide(); //ZD 103901

                            }
                            else
                            {
                                qString = Request.QueryString.ToString();
                                if (Request.QueryString["sortorder"] != null) //103901
                                    qString = qString.Replace("&sortorder=" + Request.QueryString["sortorder"].ToString(), "");
                                qString += "&sortBy=" + txtSortBy.Value + "&sortorder=" + hdnSortingOrder.Value; //103901
                                DisplaySort();
                                ShowHide(); //ZD 103901
                            }
                            foreach (DataGridItem dgi in dgConferenceList.Items)
                            {
								//ZD 101233 Starts
                                if (hasView == 0 && (LinkButton)dgi.FindControl("btnViewDetails") != null)
                                    ((LinkButton)dgi.FindControl("btnViewDetails")).Visible = false;
                                if (hasManage == 0 && (LinkButton)dgi.FindControl("btnManage") != null)
                                    ((LinkButton)dgi.FindControl("btnManage")).Visible = false;
                                if (hasExtendTime == 0 && (LinkButton)dgi.FindControl("btnExtendtime") != null)
                                    ((LinkButton)dgi.FindControl("btnExtendtime")).Visible = false;
                                if (hasMCUInfo == 0 && (LinkButton)dgi.FindControl("btnMCUDetails") != null)
                                    ((LinkButton)dgi.FindControl("btnMCUDetails")).Visible = false;
                                if (hasEdit == 0 && (LinkButton)dgi.FindControl("btnEdit") != null)
                                    ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                                if (hasClone == 0 && (LinkButton)dgi.FindControl("btnClone") != null)
                                    ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                                if (hasDelete == 0 && (LinkButton)dgi.FindControl("btnDelete") != null)
                                    ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;
								//ZD 101233 End

                                if ((LinkButton)dgi.FindControl("btnViewDetails") != null)
                                    ((LinkButton)dgi.FindControl("btnViewDetails")).Attributes.Add("onclick", "javascript:viewconf('" + dgi.Cells[0].Text + "');return false;");

                                if ((LinkButton)dgi.FindControl("btnExtendtime") != null)
                                    ((LinkButton)dgi.FindControl("btnExtendtime")).OnClientClick = "javascript:ShowExtendconf('" + dgi.Cells[0].Text + "');return false;"; //ZD 101315-Extend conf
                                                              
                                if (((LinkButton)dgi.FindControl("btnApprovalStatus")) != null)
                                {
                                    string splconfname = ((Label)dgi.FindControl("lblConfName")).Text;
                                    splconfname = splconfname.Replace("'", "�");
                                    ((LinkButton)dgi.FindControl("btnApprovalStatus")).Attributes.Add("onclick", "javascript:viewapprovalstatus('" + dgi.Cells[0].Text + "','" + splconfname + "','" + ((Label)dgi.FindControl("lblDateTime")).Text + "');return false;"); //FB 2321 Ends
                                }
								//FB 2448 - Starts
                                if (((LinkButton)dgi.FindControl("btnMCUDetails")) != null)
                                {
                                    //Label isVMR = (Label)dgi.FindControl("lblisVMR");
                                    //if (isVMR != null)
                                        //if (isVMR.Text.Trim() == "0" || isVMR.Text.Trim() == "2") //ZD 100522
                                         ((LinkButton)dgi.FindControl("btnMCUDetails")).Attributes.Add("onclick", "javascript:viewconfMCUinfo('" + dgi.Cells[0].Text + "');return false;");
                                 
                                }
								//FB 2448 - End
                                if (chkAllSilo.Checked && Session["organizationID"].ToString() == "11" && Session["UsrCrossAccess"].ToString() == "1")//FB 2274
                                {
                                    if (((Label)dgi.FindControl("lblOrgName")) != null)
                                    {
                                        if (Session["organizationName"] != null)
                                        {
                                            if (Session["organizationName"].ToString() != "")
                                            {
                                                if (Session["organizationName"].ToString() != ((Label)dgi.FindControl("lblOrgName")).Text)
                                                {
                                                    if (((LinkButton)dgi.FindControl("btnClone")) != null)
                                                        ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("bindReservations: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindApproval - Not Used
        //protected void BindApproval()
        //{
        //    try
        //    {
        //        lstListType.Visible = true;
        //        lstListType.ClearSelection();
        //        lstListType.Items.FindByValue("6").Selected = true;
        //        String inXML = objInXML.SearchConference(Session["userID"].ToString(), ns_MyVRMNet.vrmConfStatus.Pending, "", "", "", "", "", "", "", "", "", "1", "", "1", "3", "1");
        //        log.Trace("SearchConference for approval pending Inxml: " + inXML);
        //        String outXML = obj.CallMyVRMServer("SearchConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());
        //        log.Trace("SearchConference for approval pending Outxml: " + outXML);
        //        XmlDocument xmldoc = new XmlDocument();
        //        xmldoc.LoadXml(outXML);
        //        int nodesCount = xmldoc.SelectNodes("//SearchConference/Conferences/Conference").Count;
        //        if (nodesCount > 0)
        //        {
        //            XmlNodeList nodes = xmldoc.SelectNodes("//SearchConference/Conferences");
        //            LoadConferenceList(nodes, dgConferenceList);
        //            Label lblTemp = new Label();
        //            DataGridItem dgFooter = (DataGridItem)dgConferenceList.Controls[0].Controls[dgConferenceList.Controls[0].Controls.Count - 1];
        //            lblTemp = (Label)dgFooter.FindControl("lblTotalRecords");
        //            lblTemp.Text = nodesCount.ToString();
        //        }
        //        else
        //        {
        //            lblNoConferences.Visible = true;
        //            dgConferenceApproveList.Visible = false;
        //        }
        //        foreach (DataGridItem dgi in dgConferenceList.Items) //fogbugz case 184
        //        {
        //            if ((LinkButton)dgi.FindControl("btnViewDetails") != null)
        //                ((LinkButton)dgi.FindControl("btnViewDetails")).Attributes.Add("onclick", "javascript:viewconf('" + dgi.Cells[0].Text + "');return false;");
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        errLabel.Visible = true;
        //        errLabel.Text = ex.StackTrace + " : " + ex.Message;
        //    }
        //}
        #endregion

        #region LoadConferenceList
        /// <summary>
        /// LoadConferenceList
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="dgList"></param>
        protected void LoadConferenceList(XmlNodeList nodes, DataGrid dgList, DataGrid dgConfList)
        {
            try
            {
                DateTime lblStart = new DateTime(); //ZD 100995
                XmlTextReader xtr;
                ds = new DataSet();
                DataGridItem dgiSingleConf = null; //ZD 100642
                foreach (XmlNode node in nodes)
                {
                    if (node.InnerXml.Trim().IndexOf("<Selected>") <= 0)
                    {
                        XmlNode nodeLoc = node.SelectSingleNode("//SearchConference/Conferences/Conference/Location");
                        nodeLoc.InnerXml = "<Selected><ID>-1</ID><Name>Other</Name><isVMR>0</isVMR></Selected>";//FB 2550
                    }
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.Auto);
                }
                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    {
                        if (!dv.Table.Columns.Contains("Locations"))
                            dv.Table.Columns.Add("Locations");
                        if (!dv.Table.Columns.Contains("ConferenceType"))
                            dv.Table.Columns.Add("ConferenceType");
                        if (!ds.Tables[0].Columns.Contains("ConferenceTypeDescription"))
                            ds.Tables[0].Columns.Add("ConferenceTypeDescription");
                        if (!ds.Tables[0].Columns.Contains("StartMode"))//FB 2501
                            ds.Tables[0].Columns.Add("StartMode");
                        if (!ds.Tables[0].Columns.Contains("ConferenceSupport"))//ZD 101092
                            ds.Tables[0].Columns.Add("ConferenceSupport");

                        string confTYpe = "2";

                        foreach (DataRow dr in dv.Table.Rows)
                        {
                            confTYpe = dr["ConferenceType"].ToString();
                            if (dr["ConferenceType"].ToString() == "2" && dr["isOBTP"].ToString() == "1")
                                confTYpe = "9";
                            switch (confTYpe)
                            {
                                case ns_MyVRMNet.vrmConfType.AudioOnly:
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("Audio-Only");
                                    break;
                                case ns_MyVRMNet.vrmConfType.AudioVideo:
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("Audio/Video");
                                    break;
                                case ns_MyVRMNet.vrmConfType.P2P:
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("Point to Point");
                                    break;
                                case ns_MyVRMNet.vrmConfType.RoomOnly:
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("Room Only");
                                    break;
                                    //FB 2694 Start
                                case ns_MyVRMNet.vrmConfType.HotDesking:
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("Hotdesking");
                                    break;
                                //FB 2694 End
                                case ns_MyVRMNet.vrmConfType.OBTP: //ZD 100513
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("OBTP");
                                    break;
                                default:
                                    dr["ConferenceTypeDescription"] = obj.GetTranslatedText("Undefined");
                                    break;
                            }
                            dr["ConferenceName"] = dr["ConferenceName"].ToString().Replace("{P}", "<sup><a style='font-size:medium; color:Red'>*</a></sup>"); //FB 1920

                            //ZD 101092
                            String strConfSupp = "";
                            if (dr["OnSiteAVSupport"].ToString() == "1")
                                strConfSupp += obj.GetTranslatedText("On-Site A/V Support") + "; ";

                            if (dr["MeetandGreet"].ToString() == "1")
                                strConfSupp += obj.GetTranslatedText("Meet and Greet") + "; ";

                            if (dr["CallMonitoring"].ToString() == "1")
                                strConfSupp += obj.GetTranslatedText("Call Monitoring") + "; ";

                            if (dr["DedicatedVNOC"].ToString() != "")
                                strConfSupp += obj.GetTranslatedText("Dedicated VNOC Operator") + ": " + dr["DedicatedVNOC"].ToString();
                            else if(strConfSupp != "")
                                strConfSupp = strConfSupp.Remove(strConfSupp.Length - 2);

                            dr["ConferenceSupport"] = strConfSupp;
                        }
                        
                    }
                    dt = dv.Table;
                    //foreach (DataColumn dr in dt.Columns)
                    //    Response.Write("<br>" + dt.Columns.Count + " : " +  dr.ColumnName);
                    dgList.DataSource = dt;
                    dgList.DataBind();
                    CustomTable();//FB 2670
                    foreach (DataGridItem dgi in dgList.Items)
                    {
                        DataRow dr = dtCustom.NewRow(); //FB 2670
                        //ZD 100642 Starts
                        if (dgConfList != null)
                        {
                            foreach (DataGridItem dgiconf in dgConfList.Items)
                            {
                                
                                if (dgi.Cells[0].Text.ToString().Split(',')[0] == dgiconf.Cells[0].Text.ToString())
                                    dgiSingleConf = dgiconf;
                            }
                        }
                        //ZD 100642 Ends
                        Label lblTemp = (Label)dgi.FindControl("lblOwner");
                        lblTemp.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                        if (dgi.Cells[1].Text.Trim().Equals("1"))
                            lblTemp.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                        dr["Owner"] = lblTemp.Text; //FB 2670
                        lblTemp = (Label)dgi.FindControl("lblAttendee");
                        lblTemp.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                        if (dgi.Cells[2].Text.Trim().Equals("1"))
                            lblTemp.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                        dr["Attendee"] = lblTemp.Text;//FB 2670
                        Int32 Duration = Int32.Parse(dgi.Cells[3].Text.Trim());
                        lblTemp = (Label)dgi.FindControl("lblDuration");
                        //Response.Write("<br>" + Duration.ToString());
                        if (((Duration / 60) / 24) > 0)
                            lblTemp.Text = ((Duration / 60) / 24) + "days ";
                        if ((Duration / 60) > 0)
                            lblTemp.Text = (Duration / 60) + " hrs ";//ZD 100528
                        if ((Duration % 60) > 0)
                            lblTemp.Text += (Duration % 60) + " mins";//ZD 100528
                        dr["Duration"] = lblTemp.Text; //FB 2670

                        lblTemp = (Label)dgi.FindControl("lblDateTime");
                        //Response.Write(lblTemp.Text);
                        DateTime UPDT = DateTime.Parse(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());
                        DateTime dtRec = DateTime.Parse(dgi.Cells[8].Text);
                        TimeSpan ts = UPDT.Subtract(dtRec);
                        String StartWithin = "";
                       
                        if (HttpContext.Current.Session["FormatDateType"] == null || HttpContext.Current.Session["EmailDateFormat"] == null) //ZD 100995
                            DateTime.TryParse(lblTemp.Text,out lblStart);

                        ////ZD 101044 - Starts
                        //if (Math.Abs(ts.TotalMinutes) > Duration) //FB 2894
                        //{
                            //Code added by offshore for FBIssue 1073 -- start
                            //StartWithin = lblTemp.Text;
                            if (HttpContext.Current.Session["FormatDateType"] != null && HttpContext.Current.Session["EmailDateFormat"] != null) //ZD 100995
                                StartWithin = myVRMNet.NETFunctions.GetFormattedDate(lblTemp.Text) + "<br>" + myVRMNet.NETFunctions.GetFormattedTime(Convert.ToDateTime(lblTemp.Text).ToShortTimeString(), Session["timeFormat"].ToString());
                            else
                                StartWithin = lblStart.ToShortDateString() + "<br>" + lblStart.ToShortTimeString();

                            if (Request.QueryString["t"].ToString().Equals("2"))
                            {
                                StartWithin = (Duration - Math.Abs(ts.Minutes)).ToString() + obj.GetTranslatedText(" mins");
                                lblTemp.ForeColor = System.Drawing.Color.Red;
                            }

                            lblTemp.Text = StartWithin;
                        //}
                        ////Code added by offshore for FBIssue 1073 -- end
                        //else
                        //{
                        //    StartWithin += Math.Abs(ts.Minutes) + obj.GetTranslatedText(" mins"); //ZD 100528
                        //    if (dtRec > UPDT)
                        //        StartWithin += " " + obj.GetTranslatedText(" remaining");
                        //    else
                        //        StartWithin += " " + obj.GetTranslatedText(" ago");
                        //}
                        //lblTemp.Text = StartWithin;
                        //if (lblTemp.Text.IndexOf(obj.GetTranslatedText(" ago")) > 0)
                        //    lblTemp.ForeColor = System.Drawing.Color.Red;
                        //if (lblTemp.Text.IndexOf(obj.GetTranslatedText(" remaining")) > 0)
                        //    lblTemp.ForeColor = System.Drawing.Color.Green;
                        ////ZD 101044 - Starts

                        if (Request.QueryString["t"].ToString().Equals("6"))
                        {
                            DataGrid dgResponse = (DataGrid)dgi.FindControl("dgResponse");
                            dgResponse.DataSource = getApprovalDataSource(dgi.Cells[7].Text.ToString(), dgi, dgiSingleConf); //ZD 100642
                            dgResponse.DataBind();
                        }
                        //Code added for FB 1191
                        if ((LinkButton)dgi.FindControl("btnViewDetails") != null)
                            ((LinkButton)dgi.FindControl("btnViewDetails")).Attributes.Add("onclick", "javascript:viewconf('" + dgi.Cells[0].Text + "');return false;");
                        //Code added for FB 1191
                        //FB 2448 - Starts
                        if (((LinkButton)dgi.FindControl("btnMCUDetails")) != null)
                        {
                            //Label isVMR = (Label)dgi.FindControl("lblisVMR");
                            //if (isVMR != null)
                                //if (isVMR.Text.Trim() == "0" || isVMR.Text.Trim() == "2") //ZD 100522
                                    ((LinkButton)dgi.FindControl("btnMCUDetails")).Attributes.Add("onclick", "javascript:viewconfMCUinfo('" + dgi.Cells[0].Text + "');return false;");
                        }
                        //FB 2448 - End
                        if (!(dgi.Cells[26].Text.Trim().Equals("0"))) //ZD 102997
                        {
                            if ((LinkButton)dgi.FindControl("btnExtendtime") != null)
                                ((LinkButton)dgi.FindControl("btnExtendtime")).Attributes.Add("style", "visibility: hidden");//ZD 102997
                        }

                        /* ** Code added for P2P status * ***/
                        Label Temptype = (Label)dgi.FindControl("lblConferenceType"); //FB 2670 Moved from If Condition
                        dr["Type"] = Temptype.Text;

                        if (Request.QueryString["t"] != null)
                        {
                            if (Request.QueryString["t"].ToString().Equals("2"))
                            {
                                dgList.Columns[11].Visible = true;
                                

                                //dgi.Cells[11].Width = 20; FB 2272
                                //dgi.Cells[11].HorizontalAlign = HorizontalAlign.Left; FB 2272
                                dgi.Cells[11].Text = obj.GetTranslatedText("Status being Updated.");//FB 1830 - Translation
                                dgi.Cells[11].Attributes.Add("nowrap","nowrap"); // FB 2272
                                dgi.Cells[11].Attributes.Add("style", "display:inline"); // FB 2272
                                dgi.Cells[11].Attributes.Add("style", "direction:ltr"); // FB 2272

                                if (Temptype != null)
                                {
                                    //FB 1824 start
                                    //if (Temptype.Text == "Point to Point" && initload == false)
                                    //    switch(GetP2pStatus(dgi.Cells[0].Text))
                                    if (Temptype.Text != obj.GetTranslatedText("Room Only") && initload == false)//FB 2272
                                    {
                                        switch (GetConfOnlineStatus(dgi.Cells[0].Text)) 
                                        {
                                            case ns_MyVRMNet.vrmEndPointConnectionSatus.disconnect:
                                                dgi.Cells[11].Text = obj.GetTranslatedText("Disconnected");//FB 1830 - Translation
                                                dgi.Cells[11].BackColor = System.Drawing.Color.Red;
                                                dgi.Cells[11].Font.Bold = true;
                                                break;
                                            case ns_MyVRMNet.vrmEndPointConnectionSatus.Connecting:
                                                dgi.Cells[11].Text = obj.GetTranslatedText("Connecting");//FB 1830 - Translation
                                                dgi.Cells[11].BackColor = System.Drawing.Color.Yellow;
                                                dgi.Cells[11].Font.Bold = true;
                                                break;
                                            case ns_MyVRMNet.vrmEndPointConnectionSatus.Connected:
                                                dgi.Cells[11].Text = obj.GetTranslatedText("Connected");//FB 1830 - Translation
                                                dgi.Cells[11].BackColor = System.Drawing.Color.LimeGreen;
                                                dgi.Cells[11].Font.Bold = true;
                                                break;
                                            case ns_MyVRMNet.vrmEndPointConnectionSatus.Online:
                                                dgi.Cells[11].Text = obj.GetTranslatedText("Online");//FB 1830 - Translation
                                                dgi.Cells[11].BackColor = System.Drawing.Color.FromArgb(32, 108, 255);
                                                dgi.Cells[11].Font.Bold = true;
                                                break;
                                            case ns_MyVRMNet.vrmEndPointConnectionSatus.PartiallyConnected: //FB 1824
                                                dgi.Cells[11].Text = obj.GetTranslatedText("Partially Connected");
                                                dgi.Cells[11].BackColor = System.Drawing.Color.Yellow;
                                                dgi.Cells[11].Font.Bold = true;
                                                break;
                                            case "-1":
                                                dgi.Cells[11].Text = obj.GetTranslatedText("Status not available.");//FB 1830 - Translation
                                                break;

                                            //FB 1824 end   
                                        }
                                    }
                                }
                            }

                            //ZD 101719 START
                            if (Session["EnableCloudInstallation"] != null)
                                int.TryParse(Session["EnableCloudInstallation"].ToString().Trim(), out enableCloudInstallation);

                            if (enableCloudInstallation == 1)
                            {
                                dgList.Columns[11].Visible = false;
                            }
                            //ZD 101719 END
                            
                            if (Request.QueryString["t"].ToString().Equals("1")) //TCK  #100037
                            {
								//ZD 101233 Starts
                                lblTemp = null;
                                int confStatus = 0, isPublicConf = 0, isConfSupport = 0;
                                int.TryParse(dgi.Cells[4].Text, out confStatus);
                                int.TryParse(dgi.Cells[24].Text, out isPublicConf);
                                lblTemp = (Label)dgi.FindControl("lblConferenceSupport");

                                if (lblTemp != null && !string.IsNullOrWhiteSpace(lblTemp.Text))
                                    isConfSupport = 1;

                                int filterType = 0;
                                hasView = 0;
                                hasEdit = 0;
                                hasDelete = 0;
                                hasManage = 0;
                                hasClone = 0;
                                hasMCUInfo = 0;
                                hasExtendTime = 0;

                                int hasReservations = 0;
                                int hasPublic = 0;
                                int hasConferenceSupport = 0;

                                if (Session["hasReservations"] != null && Session["hasReservations"].ToString() == "1")
                                    hasReservations = 1;

                                if (Session["hasPublic"] != null && Session["hasPublic"].ToString() == "1")
                                    hasPublic = 1;

                                if (Session["hasConferenceSupport"] != null && Session["hasConferenceSupport"].ToString() == "1")
                                    hasConferenceSupport = 1;

                                //ZD 102783
                                if (hasReservations == 1 && confStatus == 0) //Reservation
                                    filterType = 3;
                                else if (hasPublic == 1 && confStatus == 0 && isPublicConf == 1)  //Public
                                    filterType = 4;
                                else if (hasConferenceSupport == 1 && confStatus == 0 && isConfSupport == 1) //ConfSupport
                                    filterType = 7;
                                else if (confStatus == 1) //Pending
                                    filterType = 5;
                                else if (confStatus == 6) //On MCU
                                    filterType = 8;
                                else if (confStatus == 5) //Ongoing
                                    filterType = 2;
                                else if (confStatus == 3 || confStatus == 9) //Terminated or Deleted
                                    filterType = 9;
                                else if (confStatus == 7) //Completed
                                    filterType = 10;
                                else if (confStatus == 2) //Wait List-ZD102532
                                    filterType = 11;
                                else
                                    filterType = 3; //if (confStatus == 0 && isPublicConf == 0)//Reservation with Puclic or ConfSupport or without both

                                obj.CheckConferenceRights(filterType, ref hasView, ref hasManage, ref hasExtendTime, ref hasMCUInfo, ref hasEdit, ref hasDelete, ref hasClone);

                                if (hasView == 0 && (LinkButton)dgi.FindControl("btnViewDetails") != null)
                                    ((LinkButton)dgi.FindControl("btnViewDetails")).Visible = false;
                                if (hasManage == 0 && (LinkButton)dgi.FindControl("btnManage") != null)
                                    ((LinkButton)dgi.FindControl("btnManage")).Visible = false;
                                if (hasExtendTime == 0 && (LinkButton)dgi.FindControl("btnExtendtime") != null)
                                    ((LinkButton)dgi.FindControl("btnExtendtime")).Visible = false;
                                if (hasMCUInfo == 0 && (LinkButton)dgi.FindControl("btnMCUDetails") != null)
                                    ((LinkButton)dgi.FindControl("btnMCUDetails")).Visible = false;
                                if (hasEdit == 0 && (LinkButton)dgi.FindControl("btnEdit") != null)
                                    ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                                if (hasClone == 0 && (LinkButton)dgi.FindControl("btnClone") != null)
                                    ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                                if (hasDelete == 0 && (LinkButton)dgi.FindControl("btnDelete") != null)
                                    ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;
								//ZD 101233 End
                                switch (dgi.Cells[4].Text)
                                {
                                    case ns_MyVRMNet.vrmConfStatus.Deleted:
                                        //if (((LinkButton)dgi.FindControl("btnClone")) != null)
                                        //    ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                                        if (((LinkButton)dgi.FindControl("btnDelete")) != null)
                                            ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;
                                        if (((LinkButton)dgi.FindControl("btnEdit")) != null)
                                            ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                                        break;
                                    case ns_MyVRMNet.vrmConfStatus.Terminated:
                                        if (((LinkButton)dgi.FindControl("btnClone")) != null)
                                            ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                                        if (((LinkButton)dgi.FindControl("btnDelete")) != null)
                                            ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;
                                        if (((LinkButton)dgi.FindControl("btnEdit")) != null)
                                            ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                                        break;
                                    case ns_MyVRMNet.vrmConfStatus.OnMCU:
                                        if (((LinkButton)dgi.FindControl("btnClone")) != null)
                                            ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                                        //ZD 100036 Starts
                                        if (((LinkButton)dgi.FindControl("btnDelete")) != null)
                                        {
                                            if (dgi.Cells[22].Text.Equals("1") && hasDelete == 1) //Synchronous//ZD 101233
                                                ((LinkButton)dgi.FindControl("btnDelete")).Visible = true;
                                            else
                                                ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;
                                        }
                                        if (((LinkButton)dgi.FindControl("btnEdit")) != null)
                                        {
                                            if (dgi.Cells[22].Text.Equals("1") && hasEdit == 1) //ZD 101233
                                                ((LinkButton)dgi.FindControl("btnEdit")).Visible = true;
                                            else
                                                ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                                        }
                                        //ZD 100036 End
                                        break;
                                }

                            }
                        }
                        /* ** Code added fro P2P status * ***/
                        //FB 2501 Starts
                        Label lblconfmode = (Label)dgi.FindControl("lblConferenceMode");
                        if(lblconfmode.Text == "0")
                            lblconfmode.Text = obj.GetTranslatedText("Automatic");
                        else
                            lblconfmode.Text = obj.GetTranslatedText("Manual");
                        //FB 2501 Ends
                        //FB 2670 - Start
                        dr["Conf Mode"] = lblconfmode.Text;
                        dr["Unique ID"] = ((Label)dgi.FindControl("lblUniqueID")).Text;
                        dr["Conference Name"] = ((Label)dgi.FindControl("lblConfName")).Text;
                        dr["Silo Name"] = dgi.Cells[9].Text;
                        dr["Conference Date/Time"] = dgi.Cells[8].Text;
                        dtCustom.Rows.Add(dr); 
                    }
                    Session["PrintTable"] = dtCustom;
                    Session["titleString"] = "Conference List";
                    //FB 2670 - End
                    lblNoConferences.Visible = false; //organization fix
                }
                else
                    //Added for FB 1428 Start
                {
                    if (Application["Client"].ToString().ToUpper() == "MOJ")
                    {
                        lblNoConferences.Text = "No hearing found.";
                        lblNoConferences.Visible = true;
                    }
                    //Added for FB 1428 End
                    lblNoConferences.Visible = true;
                }//Added for FB 1428
            }
            catch (Exception ex)
            {
                log.Trace("conferenceList: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region SortGrid
        /// <summary>
        /// SortGrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SortGrid(Object sender, CommandEventArgs e)
        {
            try
            {
                txtSortBy.Value = e.CommandArgument.ToString();
                Sortorders = 1;//103901
                if (Request.QueryString["t"].ToString().Equals("1"))
                    BindDataFromSearch();
                else
                    BindReservations(txtConferenceSearchType.Value);
                //FB 2274 Starts
                if (Request.QueryString["t"] != null)
                {
                    switch (Request.QueryString["t"].ToString())
                    {
                        case "1":
                            chkAllSilo.Visible = false;
                            tdchkSilo.Visible = false;
                            break;
                        case "2": // Ongoing //ZD 101388
                            if (Session["UsrCrossAccess"] != null && Session["UsrCrossAccess"].ToString().Equals("1") && Session["organizationID"].ToString() == "11" && Session["OrganizationsLimit"].ToString().Trim() != "1")
                            {
                                //ZD 103062 start
                                chkAllSilo.Visible = true;
                                tdchkSilo.Visible = true;
                            }
                            else
                            {
                                chkAllSilo.Visible = false;
                                tdchkSilo.Visible = false;
                                //ZD 103062 End
                            }
                            break;
                        case "3": // Reservations
                            break;
                        case "4": // Public
                            chkAllSilo.Visible = false; 
                            tdchkSilo.Visible = false;
                            break;
                        case "5": // Pending
                            chkAllSilo.Visible = false; 
                            tdchkSilo.Visible = false;
                            break;
                        case "6": // Approval
                            chkAllSilo.Visible = false; 
                            tdchkSilo.Visible = false;
                            break;
                    }
                }
                //FB 2274 Ends
                DisplaySort();
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text += ex.StackTrace + " : " + ex.Message;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SortGrid:" + ex.Message);//ZD 100263
            }
        }
        #endregion

		//FB 2822 - Start
        #region DisplaySort
        /// <summary>
        /// DisplaySort
        /// </summary>
        protected void DisplaySort()
        {
            DataGridItem dgi = (DataGridItem)dgConferenceList.Controls[0].Controls[0];
            switch (txtSortBy.Value)
            {
                case "1":
                    //((Image)dgi.FindControl("imgSortID")).Visible = true;
                    //((Image)dgi.FindControl("imgSortName")).Visible = false;
                    //((Image)dgi.FindControl("imgSortDateTime")).Visible = false;
                    ((HtmlTableCell)dgi.FindControl("tdID")).Attributes.Add("style", "text-decoration:underline");
                    ((HtmlTableCell)dgi.FindControl("tdName")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdDateTime")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("td9")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdOrgName")).Attributes.Add("style", "text-decoration:");
                    break;
                case "2":
                    //((Image)dgi.FindControl("imgSortID")).Visible = false;
                    //((Image)dgi.FindControl("imgSortDateTime")).Visible = false; 
                    //((Image)dgi.FindControl("imgSortName")).Visible = true;
                    ((HtmlTableCell)dgi.FindControl("tdName")).Attributes.Add("style", "text-decoration:underline");
                    ((HtmlTableCell)dgi.FindControl("tdID")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdDateTime")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("td9")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdOrgName")).Attributes.Add("style", "text-decoration:");
                    break;
                case "3":
                    //((Image)dgi.FindControl("imgSortID")).Visible = false;
                    //((Image)dgi.FindControl("imgSortName")).Visible = false;
                    //((Image)dgi.FindControl("imgSortDateTime")).Visible = true;
                    ((HtmlTableCell)dgi.FindControl("tdName")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdID")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdDateTime")).Attributes.Add("style", "text-decoration:underline");
                    ((HtmlTableCell)dgi.FindControl("td9")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdOrgName")).Attributes.Add("style", "text-decoration:");
                    break;
                case "4":
                    ((HtmlTableCell)dgi.FindControl("tdName")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdID")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdDateTime")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("td9")).Attributes.Add("style", "text-decoration:underline");
                    ((HtmlTableCell)dgi.FindControl("tdOrgName")).Attributes.Add("style", "text-decoration:");
                    break;
                case "5":
                    ((HtmlTableCell)dgi.FindControl("tdName")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdID")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdDateTime")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("td9")).Attributes.Add("style", "text-decoration:");
                    ((HtmlTableCell)dgi.FindControl("tdOrgName")).Attributes.Add("style", "text-decoration:underline");                    
                    break;      
            }

        }
        #endregion
		//FB 2822 - End

        #region GetJLocations
        /// <summary>
        /// GetJLocations
        /// </summary>
        /// <param name="conference_Id"></param>
        /// <param name="conferenceType"></param>
        /// <returns></returns>
        protected DataView GetJLocations(String conference_Id, String conferenceType)
        {
            try
            {
                DataView dvConference = new DataView();
                DataView dvLocation = new DataView();
                DataView dvRooms = new DataView();
                int rowcount = 0, isVMRRoom=0; //FB 2550
                Int32.TryParse(conference_Id, out rowcount); //FB 2550
                if (Request.QueryString["hf"] == "1")
                {
                    if (ds.Tables.Count > 1)
                    {
                        dvConference = ds.Tables["Conference"].DefaultView;
                        //FB 2550 Starts
                        Int32.TryParse(dvConference[rowcount]["VMRType"].ToString(), out VMRType);
                        Int32.TryParse(dvConference[rowcount]["isVMR"].ToString(), out isVMR);
						//if (isVMR == 1) 
                        if (isVMR >0) //FB 2620
                        {
                            if (VMRType == 2)
                            {
                                if (ds.Tables.Contains("Location"))
                                {
                                    dvLocation = ds.Tables["Location"].DefaultView;
                                    dvLocation.RowFilter = "Conference_Id='" + Int32.Parse(conference_Id) + "'";
                                    if (ds.Tables.Contains("Selected"))
                                    {
                                        dvRooms = ds.Tables["Selected"].DefaultView;
                                        dvRooms.RowFilter = "Location_Id='" + Int32.Parse(dvLocation[0]["Location_Id"].ToString()) + "'";
                                        for (int i = 0; i < dvRooms.Count; i++)
                                        {
                                            isVMRRoom = 0;
                                            Int32.TryParse(dvRooms[i]["isVMR"].ToString(), out isVMRRoom);
                                            if (isVMRRoom == 0)
                                            {
                                                dvRooms[i].Row.Delete();
                                                i = i - 1;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (ds.Tables.Contains("Location"))
                            {
                                dvLocation = ds.Tables["Location"].DefaultView;
                                dvLocation.RowFilter = "Conference_Id='" + Int32.Parse(conference_Id) + "'";
                                if (ds.Tables.Contains("Selected"))
                                {
                                    dvRooms = ds.Tables["Selected"].DefaultView;
                                    dvRooms.RowFilter = "Location_Id='" + Int32.Parse(dvLocation[0]["Location_Id"].ToString()) + "'";
                                    //[Vivek] Code change as a fix for 295
                                    if (conferenceType != ns_MyVRMNet.vrmConfType.RoomOnly)
                                    {
                                        DataRow dr;
                                        dr = dvRooms.Table.NewRow();
                                        dr["Location_Id"] = dvLocation[0]["Location_Id"].ToString();
                                        dr["ID"] = "-1";
                                        dr["Name"] = obj.GetTranslatedText("Other");//ZD 103531
                                        dr["isVMR"] = "0";
                                        dvRooms.Table.Rows.Add(dr);
                                    }
                                }
                            }
                        }
                    } //FB 2550 Ends
                    else
                    {
                        return null;
                    }
                }
                
                return dvRooms;
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GetJLocations:" + ex.Message);//ZD 100263
                return null;
            }
        }
        #endregion

        #region getApprovalDataSource
        protected DataView getApprovalDataSource(String Conference_Id, DataGridItem conflist, DataGridItem SingleConf)
        {
            try
            {
                string confid = "", TZ = "", serType = "", cloudConf = "", ConfType = "";
                int isVMR = 0, confDuration = 0;
                Label lblVMR = null;
                DateTime StartDateTime = DateTime.Now, EndDateTIme = DateTime.Now;

                DataView dvConference = new DataView();
                DataView dvApprovalPending = new DataView();
                DataView dvEntities = new DataView();
                DataView dvEntity = new DataView();
                if (ds.Tables.Count > 1 && Request.QueryString["t"].ToString().Equals("6"))
                {

                    dvConference = ds.Tables["Conference"].DefaultView;
                    dvApprovalPending = ds.Tables["ApprovalPending"].DefaultView;
                    dvEntities = ds.Tables["Entities"].DefaultView;

                    dvConference.RowFilter = "Conference_Id='" + Int32.Parse(Conference_Id) + "'";
                    dvApprovalPending.RowFilter = "Conference_Id='" + Int32.Parse(Conference_Id) + "'";
                    dvEntities.RowFilter = "ApprovalPending_Id='" + Int32.Parse(dvApprovalPending[0]["ApprovalPending_Id"].ToString()) + "'";

                    //Response.Write(dvEntities[0]["Entities_Id"].ToString() + " : " + Conference_Id + " : " + dvApprovalPending[0]["ApprovalPending_Id"].ToString());
                    //Response.End(); 
                    if (!ds.Tables["Entity"].Columns.Contains("EntityTypeID"))
                        ds.Tables["Entity"].Columns.Add("EntityTypeID");
                    if (!ds.Tables["Entity"].Columns.Contains("EntityTypeName"))
                        ds.Tables["Entity"].Columns.Add("EntityTypeName");
                    //ZD 100642 Starts
                    if (!ds.Tables["Entity"].Columns.Contains("EntityParams")) 
                        ds.Tables["Entity"].Columns.Add("EntityParams");

                 
                    confid = conflist.Cells[0].Text;
					//ZD 100718 Starts
                    DateTime ConfBufferStartTime = DateTime.MinValue;
                    DateTime ConfBufferEndTime = DateTime.MinValue;
                    DateTime.TryParse(conflist.Cells[20].Text.ToString(), out ConfBufferStartTime);
                    DateTime.TryParse(conflist.Cells[21].Text.ToString(), out ConfBufferEndTime);
					//ZD 100718 End
                    lblVMR = (Label)conflist.FindControl("lblisVMR");
                    if (lblVMR != null)
                        int.TryParse(lblVMR.Text.Trim(), out isVMR);

                    ConfType = conflist.Cells[15].Text;
                    cloudConf = conflist.Cells[16].Text;
                    TZ = conflist.Cells[17].Text;
                    serType = conflist.Cells[18].Text;
                    
                    if (serType == "0") //ZD 101426
                        serType = "-1";
					//ZD 100718
                    String strSrc = "RoomSearch.aspx?rmsframe=11&hf=1&confID=&stDate=" + ConfBufferStartTime.ToString() + "&enDate=" + ConfBufferEndTime.ToString() +
                    "&tzone=" + TZ + "&serType=" + serType + "&isVMR=" + isVMR + "&CloudConf=" + cloudConf + "&immediate=0&frm=frmConferenceSetUp&ConfType=" + ConfType;

                    if (ConfType == "7")
                        strSrc += "&dedVid=";

                    if (ConfType == "4")
                        strSrc += "&isTel=";
                    //ZD 100642 Ends
                    DataGrid dgResponse = null, dgResponseAll = null;
                    Label lblName = null;
                    Label lblID = null, lblRoomiDs = null;
                    foreach (DataRow dr in ds.Tables["Entity"].Rows)
                    {
                        dr["EntityTypeID"] = dvEntities[0]["Level"].ToString();
                        switch (dvEntities[0]["Level"].ToString())
                        {
                            case "1": dr["EntityTypeName"] = obj.GetTranslatedText("Room");
                                dr["EntityParams"] = strSrc; //ZD 100642
                                if (SingleConf != null)
                                {
                                    dgResponse = (DataGrid)SingleConf.FindControl("dgResponse");
                                    dgResponseAll = (DataGrid)conflist.FindControl("dgResponse");
                                    //ZD 100642 Start
                                    foreach (DataGridItem dgiResponse in dgResponse.Items)
                                    {
                                        lblName = (Label)dgiResponse.FindControl("lblEntityName");
                                        lblID = (Label)dgiResponse.FindControl("lblEntityID");
                                        lblRoomiDs = (Label)dgiResponse.FindControl("lblActualEntityID");
                                        if (dr["ID"].ToString() == lblRoomiDs.Text.ToString())
                                        {
                                            dr["Name"] = lblName.Text;
                                            dr["ID"] = lblID.Text;
                                        }
                                    }
                                    //ZD 100642 End
                                }
                                break; //ZD 100288
                            case "2": dr["EntityTypeName"] = obj.GetTranslatedText("MCU");
                                //dr["EntityParams"] = strSrc; //ZD 100718
                                dr["EntityParams"] = "Bridgelist.aspx?hd=" + dr["ID"].ToString(); //ZD 100718
                                if (SingleConf != null)
                                {
                                    dgResponse = (DataGrid)SingleConf.FindControl("dgResponse");
                                    dgResponseAll = (DataGrid)conflist.FindControl("dgResponse");
                                    //ZD 100718 Start
                                    foreach (DataGridItem dgiResponse in dgResponse.Items)
                                    {
                                        lblName = (Label)dgiResponse.FindControl("lblEntityName");
                                        lblID = (Label)dgiResponse.FindControl("lblEntityID");
                                        lblRoomiDs = (Label)dgiResponse.FindControl("lblActualEntityID");
                                        if (dr["ID"].ToString() == lblRoomiDs.Text.ToString())
                                        {
                                            dr["Name"] = lblName.Text;
                                            dr["ID"] = lblID.Text;
                                        }
                                    }
                                    //ZD 100718 End
                                }
                                break;
                            case "3": dr["EntityTypeName"] = obj.GetTranslatedText("Department"); break;
                            case "4": dr["EntityTypeName"] = obj.GetTranslatedText("System"); break;
                            default: dr["EntityTypeName"] = obj.GetTranslatedText("None"); break;
                        }
                    }

                    dvEntity = ds.Tables["Entity"].DefaultView;
                    dvEntity.RowFilter = "Entities_Id='" + Int32.Parse(dvEntities[0]["Entities_Id"].ToString()) + "'";
                }
                return dvEntity;
            }
            catch (Exception ex)
            {
                log.Trace("getApprovalDataSource: " + ex.StackTrace + " : " + ex.Message);
                return null;
            }
        }
        #endregion

        #region getInstancesDataSource
        protected DataView getInstancesDataSource(String instance_Id)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();

                DataView dvInstance = new DataView();
                DataView dvPartyInfo = new DataView();
                DataView dvApprove = new DataView();
                DataView dvRooms = new DataView();
                DataView dvRoom = new DataView();
                if (ds.Tables.Count > 1)
                {
                    //foreach (DataTable dt in ds.Tables)
                    //{
                    //    Response.Write("<br><br>" + dt.TableName);
                    //    foreach (DataColumn dc in dt.Columns)
                    //        Response.Write("<br>" + dc.ColumnName);
                    //}
                    dvInstance = ds.Tables["instance"].DefaultView;
                    dvPartyInfo = ds.Tables["partyInfo"].DefaultView;
                    dvApprove = ds.Tables["approve"].DefaultView;

                    dvInstance.RowFilter = "instance_Id='" + Int32.Parse(instance_Id) + "'";
                    dvPartyInfo.RowFilter = "instance_Id='" + Int32.Parse(instance_Id) + "'";
                    dvApprove.RowFilter = "partyInfo_Id='" + Int32.Parse(dvPartyInfo[0]["partyInfo_Id"].ToString()) + "'";
                    dvRooms.RowFilter = "approve_Id='" + Int32.Parse(dvApprove[0]["approve_Id"].ToString()) + "'";

                    switch (dvApprove[0]["level"].ToString())
                    {
                        case "1":
                            dvRooms = ds.Tables["rooms"].DefaultView;
                            dvRooms.RowFilter = "approve_Id='" + Int32.Parse(dvApprove[0]["approve_Id"].ToString()) + "'";
                            if (!ds.Tables["room"].Columns.Contains("EntityTypeID"))
                                ds.Tables["room"].Columns.Add("EntityTypeID");
                            if (!ds.Tables["room"].Columns.Contains("EntityTypeName"))
                                ds.Tables["room"].Columns.Add("EntityTypeName");
                            foreach (DataRow dr in ds.Tables["room"].Rows)
                            {
                                dr["EntityTypeID"] = dvApprove[0]["level"].ToString();
                                dr["EntityTypeName"] = obj.GetTranslatedText("Room");
                            }
                            dvRoom = ds.Tables["room"].DefaultView;
                            dvRoom.RowFilter = "rooms_Id='" + Int32.Parse(dvRooms[0]["rooms_Id"].ToString()) + "'";
                            break;
                        case "2":
                            dvRooms = ds.Tables["MCUs"].DefaultView;
                            dvRooms.RowFilter = "approve_Id='" + Int32.Parse(dvApprove[0]["approve_Id"].ToString()) + "'";
                            if (!ds.Tables["MCU"].Columns.Contains("EntityTypeID"))
                                ds.Tables["MCU"].Columns.Add("EntityTypeID");
                            if (!ds.Tables["MCU"].Columns.Contains("EntityTypeName"))
                                ds.Tables["MCU"].Columns.Add("EntityTypeName");
                            foreach (DataRow dr in ds.Tables["MCU"].Rows)
                            {
                                dr["EntityTypeID"] = dvApprove[0]["level"].ToString();
                                dr["EntityTypeName"] = obj.GetTranslatedText("MCU");
                            }
                            dvRoom = ds.Tables["MCU"].DefaultView;
                            dvRoom.RowFilter = "MCUs_Id='" + Int32.Parse(dvRooms[0]["MCUs_Id"].ToString()) + "'";
                            break;
                        case "3":
                            break;
                        case "4":
                            dvRooms = ds.Tables["systems"].DefaultView;
                            dvRooms.RowFilter = "approve_Id='" + Int32.Parse(dvApprove[0]["approve_Id"].ToString()) + "'";
                            if (!ds.Tables["system"].Columns.Contains("EntityTypeID"))
                                ds.Tables["system"].Columns.Add("EntityTypeID");
                            if (!ds.Tables["system"].Columns.Contains("EntityTypeName"))
                                ds.Tables["system"].Columns.Add("EntityTypeName");
                            if (!ds.Tables["system"].Columns.Contains("ID"))
                                ds.Tables["system"].Columns.Add("ID");
                            if (!ds.Tables["system"].Columns.Contains("name"))
                                ds.Tables["system"].Columns.Add("name");
                            foreach (DataRow dr in ds.Tables["system"].Rows)
                            {
                                dr["EntityTypeID"] = dvApprove[0]["level"].ToString();
                                dr["EntityTypeName"] = obj.GetTranslatedText("System");
                            }
                            dvRoom = ds.Tables["system"].DefaultView;
                            dvRoom.RowFilter = "systems_Id='" + Int32.Parse(dvRooms[0]["systems_Id"].ToString()) + "'";
                            break;
                    }
                }
                return dvRoom;
            }
            catch (Exception ex)
            {
                log.Trace("getApprovalDataSource: " + ex.StackTrace + " : " + ex.Message);
                return null;
            }
        }
        #endregion

        #region ChangeListType
        protected void ChangeListType(Object sender, EventArgs e)
        {
            try
            {
                //ZD 101388 Commented
                //if (Session["isExpressUser"] != null)//FB 1779
                //{
                Session["CrossSilo"] = 0; //FB 2382

                //if (Session["isExpressUser"].ToString() == "1" && Session["isExpressUserAdv"].ToString() == "0") // commented for ZD 100819
                //    Response.Redirect("ConferenceList.aspx?t=3");
                //else
                Response.Redirect("ConferenceList.aspx?t=" + lstListType.SelectedValue);
                //}

            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ChangeListType:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        //FB 2274
        #region CheckAllSilo
        protected void CheckAllSilo(Object sender, EventArgs e)
        {
            try
            {	
				//FB 2382 Starts
                if (chkAllSilo.Checked)
                    Session["CrossSilo"] = "1";
                else
                    Session["CrossSilo"] = "0";
                //FB 2382 Ends
				//FB 2639 - Search Start
                if(txtConferenceSearchType.Value == "1")
                    Filtertype = Filtertype = ns_MyVRMNet.vrmSearchFilterType.Ongoing;
                else if (txtConferenceSearchType.Value == "0")
                    Filtertype = Filtertype = ns_MyVRMNet.vrmSearchFilterType.Reservation;
                else
                    Filtertype = Filtertype = ns_MyVRMNet.vrmSearchFilterType.Congiere;
				//FB 2639 - Search End

                BindReservations(txtConferenceSearchType.Value.ToString());
            }
            catch (Exception ex)
            {
                log.Trace("CheckAllSilo" + ex.Message);
            }
        }
        #endregion

        #region ApproveConference
        /// <summary>
        /// ApproveConference
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ApproveConference(Object sender, EventArgs e)
        {
            try
            {
                //FB Case 680 Saima this if condition is introduced to avoid approve conference getting
                // called if no conference has been selected.

                if (txtSelectionCount.Value != "0" && txtSelectionCount.Value != "")
                {
                    String inXML = "";
                    inXML += "<approveConference>";
                    inXML += obj.OrgXMLElement();//Organization Module Fixes
                    inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                    inXML += " <language>"+ language +"</language>";//FB 1830
                    //ZD 101123 Starts
                    if (Application["Exchange"].ToString().ToUpper() == "YES")
                        inXML += "<isExchange>1</isExchange>";
                    else
                        inXML += "<isExchange>0</isExchange>";
                    //ZD 101123 Ends
                    inXML += "  <conferences>";
                    foreach (DataGridItem dgi in dgConferenceList.Items)
                    {
                        DataGrid dgInstances = (DataGrid)dgi.FindControl("dgInstanceList");
                        if (dgInstances.Items.Count > 0 && dgInstances.Visible.Equals(true))
                        {
                            foreach (DataGridItem dgiInstance in dgInstances.Items)
                            {
                                inXML += "<conference>";
                                //Code added for FB 1701 Start
                                //inXML += GenerateConferenceInXML(dgiInstance);
                                inXML += GenerateConferenceInXML(dgiInstance, "I");
                                //Code added for FB 1701 End
                                inXML += "</conference>";
                            }
                        }
                        else
                        {
                            inXML += "<conference>";
                            //Code added for FB 1701 Start
                            //inXML += GenerateConferenceInXML(dgi);
                            inXML += GenerateConferenceInXML(dgi, "A");
                            //Code added for FB 1701 End
                            inXML += "</conference>";
                        }
                    }
                    inXML += "  </conferences>";
                    inXML += "</approveConference>";
                    log.Trace("SetApproveConference Inxml: " + inXML);

                    if (inXML.IndexOf("<error>") < 0)
                    {
                        String outXML = obj.CallMyVRMServer("SetApproveConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                        log.Trace("SetApproveConference Outxml: " + outXML);
                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
                        else
                        {
                            /*outXML = obj.CallMyVRMServer("CheckApprovalEntity", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 1830
                            log.Trace("CheckApprovalEntity Outxml: " + outXML); //FB 1830
                            outXML = obj.CallCommand("CreateParticipantICALOnApproval", inXML); //FB 1782
                            //FB 1643 -Fix
                            outXML = obj.CallCommand("CreateCiscoICALOnApproval", inXML);
                            log.Trace("CreateCiscoICALOnApproval Outxml: " + outXML);
                            if (outXML.IndexOf("<error>") >= 0)
                            {
                                errLabel.Text = obj.ShowErrorMessage(outXML);
                                errLabel.Visible = true;
                            }*/
                            Response.Redirect(String.Format("ConferenceList.aspx?m=1&t={0}", Request.QueryString["t"]));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ApproveConference:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region GenerateConferenceInXML
        protected String GenerateConferenceInXML(DataGridItem dgi, String instanceType)//Code added for FB 1701
        {
            try
            {
                DataGrid dgResponse = (DataGrid)dgi.FindControl("dgResponse");
                String inXML = "";
                inXML += "<confID>" + dgi.Cells[0].Text + "</confID>";
                inXML += "<instanceType>" + instanceType + "</instanceType>";//Code added for FB 1701
                inXML += "<partyInfo>";
                inXML += "  <response>";
                inXML += "      <level>" + ((Label)dgResponse.Items[0].FindControl("lblEntityTypeID")).Text + "</level>";
                switch (((Label)dgResponse.Items[0].FindControl("lblEntityTypeID")).Text)
                {
                    case "1":
                        inXML += "<rooms>";
                        foreach (DataGridItem dgiResponse in dgResponse.Items)
                        {
                            inXML += "  <room>";
                            inXML += "      <ID>" + ((Label)dgiResponse.FindControl("lblEntityID")).Text + "</ID>";
                            //ZD 100642_11jan2014 Start
                            if (((Label)dgiResponse.FindControl("lblEntityID")).Text.Trim() != ((Label)dgiResponse.FindControl("lblActualEntityID")).Text.Trim())
                            {
                                inXML += "      <PreviousID>" + ((Label)dgiResponse.FindControl("lblActualEntityID")).Text + "</PreviousID>";
                                inXML += "      <decision>" + GetDecision(dgiResponse) + "</decision>";
                            }
                            else if (((RadioButton)dgiResponse.FindControl("rdAlternate")).Checked)
                            {
                                inXML += "      <decision>0</decision>"; // Undecided
                            }
                            else
                                inXML += "      <decision>" + GetDecision(dgiResponse) + "</decision>";

                            //ZD 100642_11jan2014 End

                            inXML += "      <message>" + ((TextBox)dgiResponse.FindControl("txtMessage")).Text + "</message>";
                            inXML += "  </room>";
                        }
                        inXML += "</rooms>";
                        break;
                    case "2":
                        inXML += "<MCUs>";
                        foreach (DataGridItem dgiResponse in dgResponse.Items)
                        {
                            inXML += "  <MCU>";
                            inXML += "      <ID>" + ((Label)dgiResponse.FindControl("lblEntityID")).Text + "</ID>";
                            //ZD 100718 Start
                            if (((Label)dgiResponse.FindControl("lblEntityID")).Text.Trim() != ((Label)dgiResponse.FindControl("lblActualEntityID")).Text.Trim())
                                inXML += "      <PreviousID>" + ((Label)dgiResponse.FindControl("lblActualEntityID")).Text + "</PreviousID>";
                            //ZD 100718 End
                            inXML += "      <decision>" + GetDecision(dgiResponse) + "</decision>";
                            inXML += "      <message>" + ((TextBox)dgiResponse.FindControl("txtMessage")).Text + "</message>";
                            inXML += "  </MCU>";
                        }
                        inXML += "</MCUs>";
                        break;
                    case "3":
                        inXML += "<departments>";
                        foreach (DataGridItem dgiResponse in dgResponse.Items)
                        {

                            inXML += "  <department>";
                            inXML += "      <ID>" + ((Label)dgiResponse.FindControl("lblEntityID")).Text + "</ID>";
                            inXML += "      <decision>" + GetDecision(dgiResponse) + "</decision>";
                            inXML += "      <message>" + ((TextBox)dgiResponse.FindControl("txtMessage")).Text + "</message>";
                            inXML += "  </department>";
                        }
                        inXML += "</departments>";
                        break;
                    case "4":
                        inXML += "<systems>";
                        foreach (DataGridItem dgiResponse in dgResponse.Items)
                        {
                            inXML += "  <system>";
                            inXML += "      <decision>" + GetDecision(dgiResponse) + "</decision>";
                            inXML += "      <message>" + ((TextBox)dgiResponse.FindControl("txtMessage")).Text + "</message>";
                            inXML += "  </system>";
                        }
                        inXML += "</systems>";
                        break;
                }
                inXML += "  </response>";
                inXML += "</partyInfo>";
                return inXML;
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GenerateConferenceInXML:" + ex.Message);//ZD 100263
                return "<error></error>";
            }
        }
        #endregion

        #region GetDecision
        protected String GetDecision(DataGridItem dgi)
        {
            try
            {
                if (((RadioButton)dgi.FindControl("rdUndecided")).Checked)
                    return "0";
                if (((RadioButton)dgi.FindControl("rdApprove")).Checked)
                    return "1";
                if (((RadioButton)dgi.FindControl("rdDeny")).Checked)
                    return "2";
                if (((RadioButton)dgi.FindControl("rdAlternate")).Checked) //ZD 100642
                    return "1"; // Approve - Alternate
                return "0";
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                //errLabel.Text = ex.StackTrace;
                //return "error decision";
                log.Trace(ex.StackTrace + "error decision:" + ex.Message);
                errLabel.Text  = obj.ShowSystemMessage();//FB 1881
                return obj.ShowSystemMessage();
            }
        }
        #endregion

        #region EditConference
        protected void EditConference(Object sender, DataGridCommandEventArgs e)
        {
            
            
            try
            {
                int isExpress = 0, isAdvancedForm = 0, isAdmin = 0; //ZD 102200
                bool isExpressUser = false; //ZD 101233

                //ZD 102200 Starts
                if (Session["hasConference"] != null)
                    int.TryParse(Session["hasConference"].ToString(), out isAdvancedForm);
                if (Session["hasExpConference"] != null)
                    int.TryParse(Session["hasExpConference"].ToString(), out isExpress);
                if (Session["admin"] != null)
                    int.TryParse(Session["admin"].ToString(), out isAdmin);
                //ZD 102200 Ends

                if(Session["isExpressUser"] != null && Session["isExpressUserAdv"] != null && Session["isExpressManage"] != null)
                {
                    if (Session["isExpressUser"].ToString() == "1" || Session["isExpressUserAdv"].ToString() == "1" || Session["isExpressManage"].ToString() == "1")
                        isExpressUser = true;
                }
                //Response.Write(sender.GetType().ToString() + " : " + ((LinkButton)e.CommandSource).Text);
                //Response.End();
                //ZD 101597
                if (e.CommandSource.GetType().Name == "LinkButton" && ((LinkButton)e.CommandSource).Text.IndexOf(obj.GetTranslatedText("Delete") + " &") >= 0)//FB 1133 //|| ((LinkButton)e.CommandSource).Text.IndexOf("Supprimer &") >= 0
                {//FB 1133
                    //Added for the Editing future recurring conferences    
                    DeleteConference(sender, e);//FB 1133
                }//FB 1133
                else if ((e.CommandSource.GetType().Name == "LinkButton" && ((LinkButton)e.CommandSource).Text.IndexOf(obj.GetTranslatedText("Edit")) >= 0)
                    || hdnEditForm.Value != "")//ZD 101597     //FB 1133 //|| ((LinkButton)e.CommandSource).Text.IndexOf("�diter") >= 0
                {//FB 1133

                    string redirectPage = "ExpressConference.aspx?t=";//FB 1779 //ZD 101233
                    //ZD 101597
                    if (hdnEditForm.Value != "")
                        Session.Add("ConfID", hdnEditID.Value);
                    else
                        Session.Add("ConfID", e.Item.Cells[0].Text);
                    
                   
                    //ZD 101597 //ZD 102200 Starts
                    if (isAdvancedForm == 1 && isExpress == 1)
                    {
                        if (isAdmin > 0)
                        {
                            if (hdnEditForm.Value == "1")
                                Response.Redirect("ConferenceSetup.aspx?t=");
                            else if (hdnEditForm.Value == "2")
                                Response.Redirect("ExpressConference.aspx?t=");
                        }
                        else
                        {
                            if (e.Item.Cells[23].Text == "0" && !isExpressUser)
                                redirectPage = "ConferenceSetup.aspx?t=";
                        }
                    }
                    else if (isAdvancedForm == 1)
                        Response.Redirect("ConferenceSetup.aspx?t=");
                    else if (isExpress == 1)
                        Response.Redirect("ExpressConference.aspx?t=");
                    //ZD 101233 END //ZD 102200 Ends
                    hdnEditForm.Value = "";
                    hdnEditID.Value = "";
                    Response.Redirect("~/en/" + redirectPage); //FB 1779, FB 1830
                }//FB 1133
                else
                    GetInstances(sender, e);
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("EditConference:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region DeleteConference
        protected void DeleteConference(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                // ZD 100433 START
                string inXML = "";
                string outXML = "";

                if (e.Item.Cells[4] != null && e.Item.Cells[4].Text == ns_MyVRMNet.vrmConfStatus.OnMCU)
                {
                    inXML = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><conferenceID>" + e.Item.Cells[0].Text + "</conferenceID><FromService>0</FromService>"; //ZD 100959
                    if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                        inXML += "<isExchange>1</isExchange>";
                    //ZD 100924 Start
                    else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                        inXML += "<isExchange>2</isExchange>";
                    else
                        inXML += "<isExchange>0</isExchange>";
                    //ZD 100924 End
                    inXML += "</login>";

                    outXML = obj.CallCOM2("TerminateConference", inXML, Application["RTC_ConfigPath"].ToString());

                 }
                // ZD 100433 END

                inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <delconference>";
                inXML += "      <conference>";
                inXML += "          <confID>" + e.Item.Cells[0].Text + "</confID>";
                inXML += "          <reason></reason>";
                inXML += "      </conference>";
                inXML += "  </delconference>";
                inXML += "</login>";
                //Response.Write(obj.Transfer(inXML));
                //Response.End();
                outXML = obj.CallMyVRMServer("DeleteConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                log.Trace("DeleteConference - " + outXML);
                //Response.Write("<br>" + obj.Transfer(outXML));
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    switch (Request.QueryString["t"].ToString())
                    {
                        case "1":
                            BindDataFromSearch();
                            break;
                        case "2":
                            BindReservations("5");
                            break;
                        case "3":
                            BindReservations("0");
                            break;
                        case "4":
                        case "11"://ZD 102532
                            BindReservations("");
                            break;
                        case "5":
                        case "6":
                            BindReservations("1");
                            break;
                        default:
                            errLabel.Text = obj.GetTranslatedText("Illegal Search");//FB 1830 - Translation
                            break;
                    }
                }
                else
                {
                    //FB 2363 - Start
                    if (Application["External"].ToString() != "")
                    {
                        String inEXML = "";
                        inEXML = "<SetExternalScheduling>";
                        inEXML += "<confID>" + e.Item.Cells[0].Text + "</confID>";
                        inEXML += "</SetExternalScheduling>";

                        String outExml = obj.CallCommand("SetExternalScheduling", inEXML);
                    }
                    //FB 2363 - End
                    bool isWaitListConference = false;
                    if (e.Item.Cells[4] != null && e.Item.Cells[4].Text == ns_MyVRMNet.vrmConfStatus.WaitList)
                        isWaitListConference = true;

                    if (!isWaitListConference)
                    {
                        //FB 1782 - Starts
                        String inxml = "<DeleteParticipantICAL>";
                        inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                        inxml += "<ConfID>" + e.Item.Cells[0].Text + "</ConfID>";
                        //FB 2457 Exchange Round trip starts
                        if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "YES")
                            inxml += "<isExchange>1</isExchange>";
                        //ZD 100924 Start
                        else if (Application["Exchange"] != null && Application["Exchange"].ToString().ToUpper() == "MOD")
                            inxml += "<isExchange>2</isExchange>";
                        else
                            inxml += "<isExchange>0</isExchange>";
                        //ZD 100924 End
                        //FB 2457 Exchange Round trip starts
                        inxml += "</DeleteParticipantICAL>";
                        obj.CallCommand("DeleteParticipantICAL", inxml);
                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
                        //FB 1782 - Ends
                        //FB 1602 Cisco ICAl Fix -- Start
                        inxml = "<DeleteCiscoICAL>";
                        inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                        inxml += "<ConfID>" + e.Item.Cells[0].Text + "</ConfID>";
                        inxml += "</DeleteCiscoICAL>";
                        obj.CallCommand("DeleteCiscoICAL", inxml);
                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
                        //FB 1602 Cisco ICAl Fix -- End
                        Session["CalendarMonthly"] = null;//FB 1850
                        inXML = "";
                        inXML += "<login>";
                        inXML += obj.OrgXMLElement();//Organization Module Fixes
                        inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                        inXML += "  <ConferenceID>" + e.Item.Cells[0].Text + "</ConferenceID>";
                        inXML += "  <WorkorderID>0</WorkorderID>";
                        inXML += "</login>";
                        //Response.Write(obj.Transfer(inXML));
                        outXML = obj.CallMyVRMServer("DeleteWorkOrder", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
                    }

                    if (((LinkButton)e.CommandSource).Text.IndexOf(obj.GetTranslatedText("Delete") + " &") >= 0)//FB 1133 //|| ((LinkButton)e.CommandSource).Text.IndexOf("Supprimer &") >= 0
                    {//FB 1133
                        CloneConference(sender, e);//FB 1133
                    }//FB 1133
                    else//FB 1133
                    {//FB 1133
                        String qString = "m=1&t=" + Request.QueryString["t"].ToString();
                        if (Request.QueryString["pageNo"] != null)
                            qString += "&pageNo=" + Request.QueryString["pageNo"].ToString();
                        //ZD 101673 start
                        if (isSearchConf == true)
                            qString += "&frm=1";
                        //ZD 101673 End
                        Response.Redirect("ConferenceList.aspx?" + qString);
                    }//FB 1133
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("DeleteConference:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region CloneConference
        protected void CloneConference(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                Session.Add("ConfID", e.Item.Cells[0].Text);
                Response.Redirect("ConferenceSetup.aspx?t=o");
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("CloneConference:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region ManageConference
        protected void ManageConference(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                if (e.CommandArgument.ToString().Equals("1"))
                {
                    Session.Add("ConfID", e.Item.Cells[0].Text);
                    Response.Redirect("ManageConference.aspx?t=");
                }
                else if (e.CommandArgument.ToString().Equals("2"))
                    JoinConference(e);
                else if (e.CommandArgument.ToString().Equals("3"))
                    SubmitJoin(e);
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ManageConference:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion
        //FB 1934 - Start - Modified codes
        #region JoinConference
        protected void JoinConference(DataGridCommandEventArgs e)
        {
            Boolean isInstance = false;
            Boolean istblVis = false;
            try
            {
                //[Vivek] Code change as a fix for 295
                DataGrid dgMain = (DataGrid)e.Item.Parent.Parent.Parent.FindControl(e.Item.NamingContainer.ID); // (DataGrid)e.Item.Parent.Parent;

                if (e.Item.NamingContainer.ID == "dgInstanceList")
                {
                    dgMain = dgConferenceList;
                    isInstance = true;
                }

                foreach (DataGridItem dgi in dgMain.Items)
                {
                    Table tblJoin = ((Table)dgi.FindControl("tblJoin"));

                    if (isInstance)
                    {
                        DataGrid dgIns = (DataGrid)dgi.FindControl(e.Item.NamingContainer.ID);
                        if (dgIns != null)
                        {
                            foreach (DataGridItem dgi1 in dgIns.Items)
                            {
                                if (istblVis == true)
                                    break;
                                if (dgi1.Equals(e.Item))
                                {
                                    tblJoin.Visible = true;
                                    istblVis = true;
                                    hdnJoinID.Value = e.Item.Cells[0].Text;
                                    //FB 2550 Starts
                                    RadioButtonList rdRooms = (RadioButtonList)(tblJoin.FindControl("rdJRooms"));
                                    Label lblHdTxt = (Label)(tblJoin.FindControl("lblHdTxt"));
                                    Label lblRooms = (Label)(tblJoin.FindControl("lblRooms"));
                                    lblRooms.Visible = true;
                                    lblHdTxt.Text = obj.GetTranslatedText("Please provide the information below and select a room to attend this conference.");//ZD 103531
                                    if ((e.Item.Cells[13].Text == "1") && (e.Item.Cells[14].Text != "2"))
                                    {
                                        if (rdRooms.Items.Count.Equals(0))
                                        {
                                            lblRooms.Visible = false;
                                            lblHdTxt.Text = obj.GetTranslatedText("Please provide the information below to attend this conference.");//ZD 103531
                                        }
                                    }
                                    //FB 2550 Ends
                                    
                                }
                                else
                                    tblJoin.Visible = false;
                            }
                        }
                    }
                    else
                    {
                        if (tblJoin != null)
                        {
                            if (dgi.Equals(e.Item))
                            {
                                tblJoin.Visible = true;
                                hdnJoinID.Value = e.Item.Cells[0].Text;
                                //FB 2550 Starts
                                RadioButtonList rdRooms = (RadioButtonList)(tblJoin.FindControl("rdJRooms"));
                                Label lblHdTxt = (Label)(tblJoin.FindControl("lblHdTxt"));
                                Label lblRooms = (Label)(tblJoin.FindControl("lblRooms"));
                                lblRooms.Visible = true;
                                lblHdTxt.Text = obj.GetTranslatedText("Please provide the information below and select a room to attend this conference.");//ZD 103531
                                if ((e.Item.Cells[13].Text == "1") && (e.Item.Cells[14].Text != "2"))
                                {
                                    if (rdRooms.Items.Count.Equals(0))
                                    {
                                        lblRooms.Visible = false;
                                        lblHdTxt.Text = obj.GetTranslatedText("Please provide the information below to attend this conference.");//ZD 103531  
                                    }
                                }
                                //FB 2550 Ends
                            }
                            else
                                tblJoin.Visible = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("JoinConference:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion
        //FB 1934 End
        #region ChangeJoinOption
        protected void ChangeJoinOption(Object sender, EventArgs e)
        {
            RadioButtonList rdJoin = (RadioButtonList)sender;
            Table tblJoin = new Table();
            foreach (DataGridItem dgi in dgConferenceList.Items)
            {
                if (((Table)dgi.FindControl("tblJoin")).Visible.Equals(true))
                    tblJoin = (Table)dgi.FindControl("tblJoin");
            }
            switch (rdJoin.SelectedValue.ToString())
            {
                case "-1":
                    ((Table)tblJoin.FindControl("tblEA")).Visible = true;
                    break;
                default:
                    ((Table)tblJoin.FindControl("tblEA")).Visible = false;
                    //((Table)tblJoin.FindControl("tblRA")).Visible = true;
                    //RadioButtonList rdRooms = (RadioButtonList)(tblJoin.FindControl("rdJRooms"));
                    //if (rdRooms.Items.Count.Equals(0))
                    //    ((Label)tblJoin.FindControl("lblJNoRooms")).Visible = true;
                    break;
            }
        }
        #endregion

        #region SubmitJoin
        protected void SubmitJoin(DataGridCommandEventArgs e)
        {
            try
            {
                if (IsValidSubmit(e))
                {
                    //FB 2027 start
                    StringBuilder inXML = new StringBuilder();
                    //String inXML = "";
                    inXML.Append("<login>");
                    inXML.Append(obj.OrgXMLElement());//Organization Module Fixes
                    inXML.Append("  <userFirstName>" + ((TextBox)e.Item.FindControl("txtJFirstName")).Text + "</userFirstName>");
                    inXML.Append("  <userLastName>" + ((TextBox)e.Item.FindControl("txtJLastName")).Text + "</userLastName>");
                    inXML.Append("  <userEmail>" + ((TextBox)e.Item.FindControl("txtJEmailAddress")).Text + "</userEmail>");
                    inXML.Append("  <userPassword>123</userPassword>");
                    inXML.Append("</login>");
                    String outXML = obj.CallMyVRMServer("GuestRegister", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                    //String outXML = obj.CallCOM("GuestRegister", inXML, Application["COM_ConfigPath"].ToString());
                    if (outXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        StringBuilder inXML1 = new StringBuilder();
                        inXML1.Append("<login>");
                        inXML1.Append(obj.OrgXMLElement());//Organization Module Fixes
                        inXML1.Append("<userID>" + xmldoc.SelectSingleNode("//GuestRegister/userID").InnerText + "</userID>");
                        //inXML += "<confID>" + e.Item.Cells[0].Text + "</confID>"; //FB 1934
                        inXML1.Append("<confID>" + hdnJoinID.Value + "</confID>"); //FB 1934
                        String pI = "1";
                        if (!((RadioButtonList)e.Item.FindControl("rdJRooms")).SelectedValue.Equals("-1"))
                            pI = "2";
                        inXML1.Append("<partyInvite>" + pI + "</partyInvite>");
                        inXML1.Append("<videoProtocol>" + ((DropDownList)e.Item.FindControl("lstJProtocol")).SelectedValue + "</videoProtocol>");
                        inXML1.Append("<IPISDNAddress>" + ((TextBox)e.Item.FindControl("txtJAddress")).Text + "</IPISDNAddress>");
                        inXML1.Append("<connectionType>" + ((DropDownList)e.Item.FindControl("lstJConnectionType")).SelectedValue + "</connectionType>");
                        if (((CheckBox)e.Item.FindControl("chkJIsOutSide")).Checked)
                            inXML1.Append("<isOutside>1</isOutside>");
                        else
                            inXML1.Append("<isOutside>0</isOutside>");
                        //FB 2550 Starts
                        inXML1.Append("<partyNotify>1</partyNotify>");
                        inXML1.Append("<PublicVMRParty>1</PublicVMRParty>");
                        //FB 2550 Ends
                        inXML1.Append("<locationID>" + ((RadioButtonList)e.Item.FindControl("rdJRooms")).SelectedValue + "</locationID>");
                        inXML1.Append("</login>");
                        //outXML = obj.CallCOM("SetDynamicUser", inXML1.ToString(), Application["COM_ConfigPath"].ToString()); 
                        outXML = obj.CallMyVRMServer("SetDynamicUser", inXML1.ToString(), Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027
                        //FB 2027 End
                        //inXML = "<SetConferenceEndpoint>";
                        //inXML += "  <UserID>" + xmldoc.SelectSingleNode("//GuestRegister/userID").InnerText + "</UserID>";
                        //inXML += "  <ConfID>" + e.Item.Cells[0].Text + "</ConfID>";
                        //inXML += "  <Endpoint>";
                        //inXML += "	  <EndpointID>new</EndpointID>";
                        //inXML += "	  <EndpointName></EndpointName>"; // <!-- if type is R then this would be the roomname -->
                        //inXML += "	  <EndpointLastName></EndpointLastName>";
                        //inXML += "	  <EndpointEmail></EndpointEmail>";
                        //inXML += "	  <ProfileID>1</ProfileID>";
                        //inXML += "	  <Type>U</Type>";
                        //inXML += "	  <EncryptionPreferred>0</EncryptionPreferred>";
                        //inXML += "	  <AddressType>1</AddressType>";
                        //inXML += "	  <Address>12.12.12.12</Address>";
                        //inXML += "	  <URL></URL>";
                        //inXML += "	  <IsOutside>0</IsOutside>";
                        //inXML += "	  <ConnectionType>1</ConnectionType>";
                        //inXML += "	  <VideoEquipment>17</VideoEquipment>";
                        //inXML += "	  <LineRate>384</LineRate>";
                        //inXML += "	  <Bridge>1</Bridge>";
                        //inXML += "	  <Connection>3</Connection>";
                        //inXML += "	  <Protocol>1</Protocol>";
                        //inXML += "  </Endpoint>";
                        //inXML += "</SetConferenceEndpoint>";

                        if (outXML.IndexOf("<error>") < 0)
                        {
                            //ZD 103531 - Start
                            if (Session["browserlang"] != null)
                                Response.Redirect("ConferenceList.aspx?t=4&hf=1&m=1&lang=" + Session["browserlang"].ToString());
                            else
                                Response.Redirect("ConferenceList.aspx?t=4&hf=1&m=1&lang=en-US");
                            //ZD 103531 - End                            
                        }
                        else
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
                    }
                    else
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SubmitJoin:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region IsValidSubmit
        protected bool IsValidSubmit(DataGridCommandEventArgs e)
        {
            try
            {
                String pattern = @"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){3}$";
                Regex check = new Regex(pattern);
                //RadioButtonList rdJoin = (RadioButtonList)e.Item.FindControl("rdJoin");
                TextBox txtJAddress = (TextBox)e.Item.FindControl("txtJAddress");
                RadioButtonList rdJRooms = (RadioButtonList)e.Item.FindControl("rdJRooms");
                DropDownList lstJProtocol = (DropDownList)e.Item.FindControl("lstJProtocol");
                String IPToValidate = txtJAddress.Text;
                String errMsg = "";
                if (rdJRooms.SelectedValue.Equals("-1") && lstJProtocol.SelectedValue.Equals("IP") && !check.IsMatch(IPToValidate, 0))
                    errMsg = obj.GetTranslatedText("Invalid IP Address.");

                //Response.Write("'" + rdJoin.SelectedValue + " : " + errMsg.Trim() + "'");
                //Response.End();
                pattern = @"[1-9][0-9]+";
                check = new Regex(pattern);
                if (rdJRooms.SelectedValue.Equals("-1") && lstJProtocol.SelectedValue.Equals("ISDN") && !check.IsMatch(IPToValidate, 0))
                    errMsg = obj.GetTranslatedText("Invalid ISDN Address");
                //Response.Write(rdJRooms.SelectedValue);
                //Response.End();
                if (!rdJRooms.SelectedValue.Equals("-1"))
                    if (rdJRooms.Items.Count.Equals(0))
                        errMsg = obj.GetTranslatedText("You cannot attend this conference as a room attendee since there are no rooms associated with this conference");
                    else
                        if (rdJRooms.SelectedValue.Equals(""))
                        {
                            if ((e.Item.Cells[13].Text == "1") && (e.Item.Cells[14].Text == "2"))   //FB 2550 Starts
                                errMsg = obj.GetTranslatedText("Please select a room to join this conference as a Virtual Meeting Room attendee."); //ZD 100806
                            else
                                errMsg = obj.GetTranslatedText("Please select a room to join this conference as a room attendee.");
                        }
                if ((e.Item.Cells[13].Text == "1") && (e.Item.Cells[14].Text != "2")) 
                    errMsg = "";
                //FB 2550  Ends
                errLabel.Text = errMsg;
                errLabel.Visible = true;
                if (errMsg.Equals(""))
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("IsValidSubmit:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
                return false;
            }
        }
        #endregion

        #region CancelJoin
        protected void CancelJoin(Object sender, EventArgs e)
        {
            //ZD 103531 - Start
            if (Session["browserlang"] != null)
                Response.Redirect("ConferenceList.aspx?t=4&hf=1&m=1&lang=" + Session["browserlang"].ToString());
            else
                Response.Redirect("ConferenceList.aspx?t=4&hf=1&m=1&lang=en-US");
            //ZD 103531 - End                       
        }
        #endregion

        #region GetInstances
        protected void GetInstances(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                if (((DataGrid)e.Item.FindControl("dgInstanceList")).Visible.Equals(false))
                {
                    DataGrid dgConfList = dgConferenceList;
                    DataGrid dgInstanceList = (DataGrid)e.Item.FindControl("dgInstanceList");
                    //if (dgInstanceList.Items.Count <= 0)
                    //{

                        String inXML = Session["InXML"].ToString();
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(inXML);
                        inXML = inXML.Replace("<ConferenceID>" + xmldoc.SelectSingleNode("//SearchConference/ConferenceID").InnerText + "</ConferenceID>", "<ConferenceID>" + e.Item.Cells[0].Text + "</ConferenceID>");
                        inXML = inXML.Replace("<RecurrenceStyle>" + xmldoc.SelectSingleNode("//SearchConference/RecurrenceStyle").InnerText + "</RecurrenceStyle>", "<RecurrenceStyle>0</RecurrenceStyle>");
                        inXML = inXML.Replace("<PageNo>" + xmldoc.SelectSingleNode("//SearchConference/PageNo").InnerText + "</PageNo>", "<PageNo>1</PageNo>");
                        log.Trace("GetInstances Inxml SearchConference: " + inXML);
                        String outXML = obj.CallMyVRMServer("SearchConference", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                        log.Trace("GetInstances Outxml SearchConference: " + outXML);

                        xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);

                        //Recurrence Fixes to display the total instances count as tooltip - Start 

                        totalInstancesCnt += " (" + xmldoc.SelectSingleNode("//SearchConference/TotalRecords").InnerText + " instances)";
                        if (((Image)e.Item.FindControl("imgRecur")).Visible.Equals(true))
                        {
                            Image recImage = (Image)e.Item.FindControl("imgRecur");
                            //Added for FB 1428 Start
                            if (Application["Client"].ToString().ToUpper() == "MOJ")
                            {
                                recImage.ToolTip = "Recurring Hearing" + " (" + xmldoc.SelectSingleNode("//SearchConference/TotalRecords").InnerText + " instances)";
                            }
                            else
                                //Added for FB 1428 End
                            recImage.ToolTip = totalInstancesCnt;
                        }
                        //Recurrence Fixes - End

                        LoadConferenceList(xmldoc.SelectNodes("//SearchConference/Conferences/Conference"), dgInstanceList, dgConferenceList); //ZD 100642

                        foreach (DataGridItem dgi in dgInstanceList.Items)
                        {
                            //ZD 101233 Starts
                            if (ViewState["hasView"] != null && ViewState["hasView"].ToString() == "0" && (LinkButton)dgi.FindControl("btnViewDetails") != null)
                                ((LinkButton)dgi.FindControl("btnViewDetails")).Visible = false;
                            if (ViewState["hasManage"] != null && ViewState["hasManage"].ToString() == "0" && (LinkButton)dgi.FindControl("btnManage") != null)
                                ((LinkButton)dgi.FindControl("btnManage")).Visible = false;
                            if (ViewState["hasExtendTime"] != null && ViewState["hasExtendTime"].ToString() == "0" && (LinkButton)dgi.FindControl("btnExtendtime") != null)
                                ((LinkButton)dgi.FindControl("btnExtendtime")).Visible = false;
                            if (ViewState["hasMCUInfo"] != null && ViewState["hasMCUInfo"].ToString() == "0" && (LinkButton)dgi.FindControl("btnMCUDetails") != null)
                                ((LinkButton)dgi.FindControl("btnMCUDetails")).Visible = false;
                            if (ViewState["hasEdit"] != null && ViewState["hasEdit"].ToString() == "0" && (LinkButton)dgi.FindControl("btnEdit") != null)
                                ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                            if (ViewState["hasClone"] != null && ViewState["hasClone"].ToString() == "0" && (LinkButton)dgi.FindControl("btnClone") != null)
                                ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                            if (ViewState["hasDelete"] != null && ViewState["hasDelete"].ToString() == "0" && (LinkButton)dgi.FindControl("btnDelete") != null)
                                ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;
                            //ZD 101233 End

                            //ZD 101388 Commented Starts
                            /*if (Session["isExpressUser"] != null)//FB 1779
                            {
                                if (Session["isExpressUser"].Equals("1"))
                                {
                                    if (((LinkButton)dgi.FindControl("btnClone")) != null)
                                    {
                                        ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                                    }
                                    if (((LinkButton)dgi.FindControl("btnManage")) != null)
                                    {
                                        ((LinkButton)dgi.FindControl("btnManage")).Visible = true; //ZD 100819
                                    }
                                }

                                
                            }*/
                            //FB 1522 - Start
                            /*if (isViewUser) //if (menuset != "1")
                            {
                                if (Session["isExpressUser"] != null)
                                {
                                    if (Session["isExpressUser"].Equals("0"))
                                    {
                                        if (((LinkButton)dgi.FindControl("btnClone")) != null)
                                        {
                                            ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                                        }
                                        if (((LinkButton)dgi.FindControl("btnDelete")) != null)
                                        {
                                            ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;
                                        }
                                        if (((LinkButton)dgi.FindControl("btnEdit")) != null)
                                        {
                                            ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                                        }
                                        if (((LinkButton)dgi.FindControl("btnManage")) != null)
                                        {
                                            ((LinkButton)dgi.FindControl("btnManage")).Visible = false;
                                        }
                                    }
                                }
                            }*/
                            //ZD 101388 Commented End
                            //FB 1522 - End
                            if (chkAllSilo.Checked && Session["organizationID"].ToString() == "11" && Session["UsrCrossAccess"].ToString() == "1")//FB 2274
                            {
                                if (((Label)dgi.FindControl("lblOrgName")) != null)
                                {
                                    if (Session["organizationName"] != null)
                                    {
                                        if (Session["organizationName"].ToString() != "")
                                        {
                                            if (Session["organizationName"].ToString() != ((Label)dgi.FindControl("lblOrgName")).Text)
                                            {
                                                if (((LinkButton)dgi.FindControl("btnClone")) != null)
                                                    ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                                            }
                                        }
                                    }
                                }
                            }
                            //FB 2501 Starts
							//ZD 101388 Commented Starts
                            /*if (lstListType.SelectedValue.Equals("7")) 
                            {
                                if (((LinkButton)dgi.FindControl("btnClone")) != null)
                                {
                                    ((LinkButton)dgi.FindControl("btnClone")).Visible = false;
                                }
                                if (((LinkButton)dgi.FindControl("btnDelete")) != null)
                                {
                                    ((LinkButton)dgi.FindControl("btnDelete")).Visible = false;
                                }
                                if (((LinkButton)dgi.FindControl("btnEdit")) != null)
                                {
                                    ((LinkButton)dgi.FindControl("btnEdit")).Visible = false;
                                }
                            }*/
							//ZD 101388 Commented End
                            //FB 2501 Ends


                            Label lblTemp = (Label)dgi.FindControl("lblOwner");
                            lblTemp.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                            if (dgi.Cells[1].Text.Trim().Equals("1"))
                                lblTemp.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                            lblTemp = (Label)dgi.FindControl("lblAttendee");
                            lblTemp.Text = obj.GetTranslatedText("No");//FB 1830 - Translation
                            if (dgi.Cells[2].Text.Trim().Equals("1"))
                                lblTemp.Text = obj.GetTranslatedText("Yes");//FB 1830 - Translation
                            Int32 Duration = Int32.Parse(dgi.Cells[3].Text.Trim());
                            lblTemp = (Label)dgi.FindControl("lblDuration");
                            lblTemp.Text = "";
                            //Response.Write("<br>" + Duration.ToString());
                            if (((Duration / 60) / 24) > 0)
                                lblTemp.Text = ((Duration / 60) / 24) + "days ";
                            if ((Duration / 60) > 0)
                                lblTemp.Text = (Duration / 60) + " hrs "; //ZD 100528
                            if ((Duration % 60) > 0)
                                lblTemp.Text += (Duration % 60) + " mins"; //ZD 100528

                            lblTemp = (Label)dgi.FindControl("lblDateTime");
                            if (!lblTemp.Text.Contains(obj.GetTranslatedText(" mins"))) //FB 1146 FB 2272
                            {//FB 1146
                                DateTime UPDT = DateTime.Parse(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());
                                //Code changed for FB Issue 1073 -- Start
                                //DateTime dtRec = DateTime.Parse(lblTemp.Text);
                                String strDate = lblTemp.Text.Replace("<br>", " ");
                                string Date, Time; //FB 2588
                                Date = strDate.Split(' ')[0];
                                Time = strDate.Split(' ')[1];
                                if (strDate.Contains("Z")) //FB 2588
                                    strDate = myVRMNet.NETFunctions.GetDefaultDate(Date) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(Time);
                                
                                if (strDate.IndexOf(':') < 0)
                                    strDate = strDate.Insert((strDate.Length - 2), ":");

                                //ZD 100995 //ZDLatest
                                DateTime dtRec;
                                if (Session["EmailDateFormat"] != null && Session["EmailDateFormat"].ToString() == "1")
                                    dtRec = DateTime.Parse(strDate);
                                else
                                    dtRec = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(strDate));

                                //Code changed for FB Issue 1073 -- END
                                TimeSpan ts = UPDT.Subtract(dtRec);
                                String StartWithin = "";
                                //if (Math.Abs(ts.TotalMinutes) > 59)
                                StartWithin = lblTemp.Text;

                                if (Request.QueryString["t"].ToString().Equals("2"))
                                {
                                    StartWithin = (Duration - Math.Abs(ts.Minutes)).ToString() + obj.GetTranslatedText(" mins");
                                    lblTemp.ForeColor = System.Drawing.Color.Red;
                                }

                                lblTemp.Text = StartWithin;

                                //else
                                //{
                                //    StartWithin += Math.Abs(ts.Minutes) + " mins"; //ZD 100528
                                //    if (dtRec > UPDT)
                                //        StartWithin += " remaining";
                                //    else
                                //        StartWithin += " ago";
                                //}
                                //lblTemp.Text = StartWithin;
                                //if (lblTemp.Text.IndexOf(obj.GetTranslatedText(" ago")) > 0)
                                //    lblTemp.ForeColor = System.Drawing.Color.Red;
                                //if (lblTemp.Text.IndexOf(obj.GetTranslatedText(" remaining")) > 0)
                                //    lblTemp.ForeColor = System.Drawing.Color.Green;
                            }//FB 1146
                        }
                    //}
                    e.Item.FindControl("dgInstanceList").Visible = true;
                    if (e.Item.FindControl("tblInstances") != null)
                    {
                        e.Item.FindControl("tblInstances").Visible = true;
                        ((Table)(e.Item.FindControl("tblInstances"))).BorderWidth = 1;

                        
                    }
                    dgConferenceList.SelectedIndex = e.Item.ItemIndex;
                    //dgConferenceApproveList.SelectedIndex = e.Item.ItemIndex;
                    foreach (DataGridItem dgi in dgConferenceList.Items)
                    {
                        if (!dgi.Equals(e.Item))
                        {
                            DataGrid dgTemp = (DataGrid)dgi.FindControl("dgInstanceList");
                            dgTemp.Visible = false;
                            

                            Table tblTemp = (Table)dgi.FindControl("tblInstances");
                            tblTemp.BorderWidth = 0;
                            tblTemp.Visible = false;
                        }

                        
                       
                    }
                    //foreach (DataGridItem dgi in dgConferenceList.Items)
                    //{
                    //    DataGrid dgTemp = (DataGrid)dgi.FindControl("dgResponse");
                    //    dgTemp.Visible = true;
                    //    if (!dgi.Equals(e.Item))
                    //    {
                    //        dgTemp = (DataGrid)dgi.FindControl("dgInstanceList");
                    //        dgTemp.Visible = false;
                    //        dgTemp = (DataGrid)dgi.FindControl("dgResponse");
                    //        dgTemp.Visible = true;
                    //    }
                    //    else
                    //    {
                    //        dgTemp = (DataGrid)dgi.FindControl("dgResponse");
                    //        dgTemp.Visible = false;
                    //    }
                    //}
                }
                else
                {
                    e.Item.FindControl("dgInstanceList").Visible = false;
                    Table tblTemp = (Table)e.Item.FindControl("tblInstances");
                    tblTemp.BorderWidth = 0;
                    tblTemp.Visible = false;
                    if (Request.QueryString["t"].ToString().Equals("6"))
                        e.Item.FindControl("dgResponse").Visible = true;
                }
                String qString = Request.QueryString.ToString();

                if (Request.QueryString["pageNo"] != null)
                    qString = qString.Replace("&pageNo=" + Request.QueryString["pageNo"].ToString(), "");
                if (Request.QueryString["sortBy"] != null)
                    qString = qString.Replace("&sortBy=" + Request.QueryString["sortBy"].ToString(), "");
                if (Request.QueryString["sortorder"] != null) //103901
                    qString = qString.Replace("&sortorder=" + Request.QueryString["sortorder"].ToString(), "");
                if (Request.QueryString["m"] != null)
                    qString = qString.Replace("&m=" + Request.QueryString["m"].ToString(), "");
                //qString += "&sortBy=" + txtSortBy.Value;
                qString += "&sortBy=" + txtSortBy.Value + "&sortorder=" + SortingOrder; //103901

                obj.DisplayPaging(totalPages, pageNo, tblPage, "ConferenceList.aspx?" + qString);
                if (!Request.QueryString["t"].ToString().Equals("6"))
                    DisplaySort();
            }
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace + " : " + ex.Message;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GetInstances:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region GetInstancesDataTable
        /// <summary>
        /// GetInstancesDataTable
        /// </summary>
        /// <param name="nodes"></param>
        /// <param name="confID"></param>
        /// <param name="confName"></param>
        /// <param name="owner"></param>
        /// <param name="attendee"></param>
        /// <param name="duration"></param>
        /// <param name="ConferenceStatus"></param>
        /// <param name="ConferenceType"></param>
        /// <param name="OpenForRegistration"></param>
        /// <returns></returns>
        protected DataTable GetInstancesDataTable(XmlNodeList nodes, String confID, String confName, String owner, String attendee, String duration, String ConferenceStatus, String ConferenceType, String OpenForRegistration)
        {
            try
            {
                XmlTextReader xtr;
                //Response.Write(obj.Transfer(nodes.Item(0).InnerXml));
                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.Auto);
                }
                DataView dv;
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    if (Request.QueryString["t"].ToString().Equals("6"))
                        dv = new DataView(ds.Tables[1]);
                    dt = dv.Table;
                    if (!dt.Columns.Contains("confID")) dt.Columns.Add("confID");
                    if (!dt.Columns.Contains("confName")) dt.Columns.Add("confName");
                    if (!dt.Columns.Contains("owner")) dt.Columns.Add("owner");
                    if (!dt.Columns.Contains("partyInvite")) dt.Columns.Add("partyInvite");
                    if (!dt.Columns.Contains("Duration")) dt.Columns.Add("Duration");
                    if (!dt.Columns.Contains("ConferenceStatus")) dt.Columns.Add("ConferenceStatus");
                    if (!dt.Columns.Contains("ConferenceDateTime")) dt.Columns.Add("ConferenceDateTime");
                    if (!dt.Columns.Contains("ConferenceTypeDescription")) dt.Columns.Add("ConferenceTypeDescription");
                    if (!dt.Columns.Contains("ConferenceType")) dt.Columns.Add("ConferenceType");
                    if (!dt.Columns.Contains("IsHost")) dt.Columns.Add("IsHost");
                    if (!dt.Columns.Contains("OpenForRegistration")) dt.Columns.Add("OpenForRegistration");
                    if (!dt.Columns.Contains("Conference_Id")) dt.Columns.Add("Conference_Id");
                    if (!dt.Columns.Contains("StartMode")) dt.Columns.Add("StartMode");//FB 2501
                    //foreach (DataRow dr in dt.Rows)
                    for (int i = dt.Rows.Count - 1; i >= 0; i--)
                    {
                        DataRow dr = dt.Rows[i];
                        if (Request.QueryString["t"].ToString().Equals("6"))
                        {
                            if (dr["pending"].ToString().Equals("1"))
                            {
                                //Response.Write("Series ID: " + dr["seriesID"]);
                                dr["confID"] = confID + "," + dr["seriesID"];
                                dr["confName"] = confName;
                                dr["owner"] = owner;
                                dr["partyInvite"] = attendee;
                                dr["Duration"] = duration;
                                dr["ConferenceTypeDescription"] = ConferenceType;
                                dr["OpenForRegistration"] = OpenForRegistration;
                            }
                            else
                                dt.Rows.RemoveAt(i);
                        }
                        else
                        {
                            dr["Conference_Id"] = confID;
                            dr["confID"] = confID + "," + dr["seriesID"];
                            dr["confName"] = confName;
                            dr["owner"] = owner;
                            dr["partyInvite"] = attendee;
                            int dur = Int32.Parse(dr["durationMin"].ToString());
                            String strDuration = "";
                            //Response.Write((dur % 60));
                            if (dur / (60 * 24) > 0)
                                strDuration = (dur / (60 * 24)) + " days ";
                            if ((dur / 60) > 0)
                                strDuration += (dur / 60) + " hrs ";
                            if (dur % 60 > 0)
                                strDuration += (dur % 60) + " mins";

                            dr["Duration"] = strDuration;  //duration;
                            dr["IsHost"] = "0";
                            if (owner.ToUpper().Equals("YES"))
                                dr["IsHost"] = "1";
                            dr["ConferenceDateTime"] = dr["startDate"] + " " + dr["startHour"] + ":" + dr["startMin"] + " " + dr["startSet"];
                            dr["ConferenceDateTime"] =  myVRMNet.NETFunctions.GetFormattedTime(dr["ConferenceDateTime"].ToString(), Session["timeFormat"].ToString());
                            dr["ConferenceTypeDescription"] = ConferenceType;
                            dr["OpenForRegistration"] = OpenForRegistration;
                            DateTime UPDT = DateTime.Parse(Session["systemDate"].ToString() + " " + Session["systemTime"].ToString());
                            DateTime dtRec = DateTime.Parse(dr["ConferenceDateTime"].ToString());
                            TimeSpan ts = UPDT.Subtract(dtRec);
                            //dr["ConferenceDateTime"] = "";
                            if (Math.Abs(ts.TotalMinutes) > 59)
                                dr["ConferenceDateTime"] = dr["ConferenceDateTime"];
                            else
                            {
                                //if (Math.Abs(ts.Days) > 0)
                                //    dr["StartWithin"] += Math.Abs(ts.Days) + " day ";
                                //if (Math.Abs(ts.Hours) > 0)
                                //    dr["StartWithin"] += Math.Abs(ts.Hours) + " hr ";
                                dr["ConferenceDateTime"] += Math.Abs(ts.Minutes) + " mins";  //ZD 100528
                                if (dtRec > UPDT)
                                    dr["ConferenceDateTime"] += " remaining";
                                else
                                    dr["ConferenceDateTime"] += " ago";
                            }
                        }
                    }
                }
                return dt;
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GetInstancesDataTable:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
                return null;
            }
        }
        #endregion

        #region BindRowsDeleteMessage
        /// <summary>
        /// BindRowsDeleteMessage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    //Added for FB 1425 MOJ End
                    if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                        btnTemp.Attributes.Add("onclick", "if (confirm('Are you sure you want to delete this hearing?')) return true; else {DataLoading('0'); return false;}");
                    else //Added for FB 1425 MOJ End
                        btnTemp.Attributes.Add("onclick", "if (confirm('" + obj.GetTranslatedText("Are you sure you want to delete this conference?") + "')) return true; else {DataLoading('0'); return false;}");//FB japnese


                    //ZD 101597
                    btnTemp = (LinkButton)e.Item.FindControl("btnEdit");
                    if (Int32.Parse(Session["admin"].ToString()) > 0)
                        btnTemp.Attributes.Add("onclick", "javascript:return fnSelectForm('" + row["ConferenceID"].ToString() + "');");
                    else
                        btnTemp.Attributes.Add("onclick", "javascript:DataLoading(1);");

                    
                }
                //Response.Write(e.Item.Cells[0].Text); //.Equals(dgConferenceList));
                //if(e.Item.Parent.Parent.Equals(dgConferenceList))
                //    InterpretRole(sender, e);
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace + " : " + ex.Message;
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("BindRowsDeleteMessage:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        /* *** Recurrence Fixes - EDit, Delete & Clone All - Start *** */
        #region InterpretRole
        /// <summary>
        /// InterpretRole
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void InterpretRole(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.SelectedItem) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    //Added for FB 1428 Start
                    if (Application["Client"].ToString().ToUpper() == "MOJ")
                    {
                        Image recImage = (Image)e.Item.FindControl("imgRecur");
                        recImage.ToolTip = "Recurring Hearing";
                    }
                    //Added for FB 1428 End
                    //Recurrence Conferences Handling
                    if (e.Item.Cells[5].Text.Equals("1")) // && !lstListType.SelectedValue.Equals("2")) 
                    {
                        e.Item.Cells[0].Text = e.Item.Cells[0].Text.Split(',')[0];

                        //Delete All Instances - Completed instances will not be deleted
                        LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                        btnTemp.Text += "<br>";
                        btnTemp.Text += obj.GetTranslatedText("All");

                        //Clone All Instances
                        btnTemp = (LinkButton)e.Item.FindControl("btnClone"); //FB 865
                        btnTemp.Text += "<br>";
                        btnTemp.Text += obj.GetTranslatedText("All");

                        //Edit All Instances                        
                        btnTemp = (LinkButton)e.Item.FindControl("btnEdit");
                        btnTemp.Text += "<br>";
                        btnTemp.Text += obj.GetTranslatedText("All");

                        /* ***Code added for FB 1391 -- Start *** */

                        isCustomEdit = "N";

                        string recOutxml = obj.CallMyVRMServer("GetIfDirty", "<confID>" + e.Item.Cells[0].Text + "</confID>", Application["MyVRMServer_ConfigPath"].ToString());


                        if (recOutxml != "")
                        {
                            if (recOutxml.IndexOf("<error>") < 0)
                            {
                                if (recOutxml.Contains("1"))
                                    isCustomEdit = "Y";
                            }
                        }


                        btnTemp.Attributes.Add("onclick", "javascript:return CustomEditAlert('" + isCustomEdit + "','" + e.Item.Cells[0].Text + "')"); //ZD 101597

                        /* ***Code added for FB 1391 -- End *** */


                        /* *** Commented Delete & Clone All since Edit All feature for some instances in past is provided  *** */
                        /*
                        //Delete & Clone All
                        if (e.Item.Cells[4].Text.Equals("7")) //FB 1133
                        {
                            btnTemp.Text = "Delete &<br>Clone All";
                            btnTemp.Attributes.Add("onclick", "if (confirm('Are you sure you want to delete and Clone this conference?')) return true; else {DataLoading('0'); return false;}");
                        }
                        else//FB 1133
                            btnTemp.Text += "<br>All";//FB 1133
                        */

                        btnTemp = (LinkButton)e.Item.FindControl("btnManage");
                        btnTemp.Text += "<br>";
                        btnTemp.Text += obj.GetTranslatedText("All");

                        //ALLBUGS-88 - Start
                        LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");
                        Button temp = (Button)e.Item.FindControl("Temp");
                        hdnTempID.Value = temp.ClientID;
                        if (Int32.Parse(Session["admin"].ToString()) > 0)
                            btnEdit.Attributes.Add("onclick", "javascript:return fnSelectForm('" + e.Item.Cells[0].Text + "');");
                        //ALLBUGS-88 - End
                    }
                    else   //Normal Conference Handling
                    {
                        if ((lstListType.SelectedValue.Equals("2") && !Request.QueryString["t"].ToString().Equals("1")) || lstListType.SelectedValue.Equals("8")) //Ongoing Conferences //FB 2694 //ZD 100036
                        {
                            ((Image)e.Item.FindControl("imgRecur")).Visible = false;
                            LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                            if (e.Item.Cells[22].Text.Equals("1")&& lstListType.SelectedValue.Equals("8") && hasDelete == 1) //ZD 100959 //ZD 101226 //ZD 101233
                                btnTemp.Visible = true;
                            else
                                btnTemp.Visible = false;
                            btnTemp = (LinkButton)e.Item.FindControl("btnClone");
                            btnTemp.Visible = false;
                            btnTemp = (LinkButton)e.Item.FindControl("btnEdit");
                            if (e.Item.Cells[22].Text.Equals("1") && lstListType.SelectedValue.Equals("8") && hasEdit == 1) //If Synchronous conf-Edit and Delete = True //ZD 100959 //ZD 101226//ZD 101233
                                btnTemp.Visible = true;
                            else
                                btnTemp.Visible = false;
                            //ZD 101597
                            if (Int32.Parse(Session["admin"].ToString()) > 0)
                                btnTemp.Attributes.Add("onclick", "javascript:return fnSelectForm('" + e.Item.Cells[0].Text + "');");

                            btnTemp = (LinkButton)e.Item.FindControl("btnMCUDetails"); //FB 2448 start
                            //ZD 101233 Starts
                            if (hasMCUInfo >= 1)
                            {
                                btnTemp.Visible = true;
                                btnTemp.Enabled = true;
                            }
                            else
                            {
                                btnTemp.Visible = false;
                                //ZD 101233 End
                                ///Label isVMR = (Label)e.Item.FindControl("lblisVMR");
                                // if (isVMR != null)
                                // if (isVMR.Text.Trim() == "1" || isVMR.Text.Trim() == "3") //ZD 100522
                                // if (Convert.ToInt32(isVMR.Text.Trim()) > 0)// FB 2620
                                btnTemp.Enabled = false;
                            } //FB 2448 end

                            //ZD 101719 START
                            if (Session["EnableCloudInstallation"] != null)
                                int.TryParse(Session["EnableCloudInstallation"].ToString().Trim(), out enableCloudInstallation);

                            if (enableCloudInstallation == 1)
                            {
                                btnTemp.Visible = true;
                                ((LinkButton)e.Item.FindControl("btnMCUDetails")).Visible = false;
                            }
                            //ZD 101719 END

                            if ((lstListType.SelectedValue.Equals("8"))) //ZD 101315-Extend conf
                            {
                                btnTemp = (LinkButton)e.Item.FindControl("btnExtendtime"); 
                                btnTemp.Visible = false;
                            }

                        }
                        else if (e.Item.Cells[4].Text.Equals("7"))  //Past Conferences
                        {
                            LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                            btnTemp.Visible = false;
                            btnTemp = (LinkButton)e.Item.FindControl("btnEdit");
                            btnTemp.Visible = false;

                            //ZD 101597
                            if (Int32.Parse(Session["admin"].ToString()) > 0)
                                btnTemp.Attributes.Add("onclick", "javascript:return fnSelectForm('" + e.Item.Cells[0].Text + "');");

                            /* *** Following code block commented for FB 1469 - start *** */
                            //btnTemp = (LinkButton)e.Item.FindControl("btnClone");
                            //btnTemp.Visible = false;
                            /* *** Following code block commented for FB 1469 - end *** */
                        }//FB 2694 Starts
                        else if (e.Item.Cells[4].Text.Equals("5")) //Ongoing Conference //FB 2694
                        {
                            ((Image)e.Item.FindControl("imgRecur")).Visible = false;
                            LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                            btnTemp.Visible = false;
                            btnTemp = (LinkButton)e.Item.FindControl("btnClone");
                            btnTemp.Visible = false;
                            btnTemp = (LinkButton)e.Item.FindControl("btnEdit");
                            btnTemp.Visible = false;
                        }//FB 2694 End

                        //ZD 101597
                        LinkButton btnEdit = (LinkButton)e.Item.FindControl("btnEdit");
                        Button temp = (Button)e.Item.FindControl("Temp");
                        hdnTempID.Value = temp.ClientID;
                        if (Int32.Parse(Session["admin"].ToString()) > 0)
                            btnEdit.Attributes.Add("onclick", "javascript:return fnSelectForm('" + e.Item.Cells[0].Text + "');");
                    }
					//ZD 101388 Commented Starts
                    /*if (Session["isExpressUser"] != null)//FB 1779
                    {
                        if (Session["isExpressUser"].ToString() == "1") 
                        {
                            LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnClone");
                            btnTemp.Visible = false;
                            btnTemp = (LinkButton)e.Item.FindControl("btnManage");
                            btnTemp.Visible = true; //ZD 100819                         

                            if (Session["isExpressUserAdv"].ToString() == "1")
                            {
                                if (Request.QueryString["t"] != null)
                                {
                                    if (Request.QueryString["t"].ToString() == "2")
                                    {
                                        btnTemp.Visible = true;

                                    }
                                }
                            }
                            //ZD 101315 Start
                            if (Request.QueryString["t"] != null)
                            {
                                if (Request.QueryString["t"].ToString() == "2")
                                {
                                    btnTemp = (LinkButton)e.Item.FindControl("btnExtendtime");
                                    btnTemp.Visible = true; 
                                }
                            }
                            //ZD 101315 End
                        }

                        
                    }
                    //FB 1522 - Start
                    if (isViewUser) //if (menuset != "1")
                    {
                        if (Session["isExpressUser"] != null)
                        {
                            if (Session["isExpressUser"].Equals("0"))
                            {
                                LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnClone");
                                btnTemp.Visible = false;
                                btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                                btnTemp.Visible = false;
                                btnTemp = (LinkButton)e.Item.FindControl("btnEdit");
                                btnTemp.Visible = false;
                                btnTemp = (LinkButton)e.Item.FindControl("btnManage");
                                btnTemp.Visible = false;
                                btnTemp = (LinkButton)e.Item.FindControl("btnExtendtime"); //ZD 101315
                                btnTemp.Visible = false;
                            }
                        }
                    }
                    //FB 1522 - End

                    //FB 2501 Starts
                    if (lstListType.SelectedValue.Equals("7"))
                    {
                        LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnClone");
                        btnTemp.Visible = false;
                        btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                        btnTemp.Visible = false;
                        btnTemp = (LinkButton)e.Item.FindControl("btnEdit");
                        btnTemp.Visible = false;

                    }*/
                    //FB 2501 Ends
					//ZD 101388 Commented End
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("InterpretRole:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion
        /* *** Recurrence Fixes - EDit, Delete & Clone All - End *** */

        #region SetButtonProperties
        /// <summary>
        /// SetButtonProperties
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SetButtonProperties(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    if (e.Item.Cells[0].Text.IndexOf(",") >= 0)
                    {
                        e.Item.Cells[0].Text = e.Item.Cells[0].Text.Split(',')[0];
                        ((Label)e.Item.FindControl("lblDateTime")).Visible = false;
                        ((LinkButton)e.Item.FindControl("btnGetInstances")).Visible = true;
                        //((DataGrid)e.Item.FindControl("dgResponse")).Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("SetButtonProperties:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion
        
        #region GoToLobby
        /// <summary>
        /// GoToLobby
        /// Fogbugz case 158
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void GoToLobby(Object sender, EventArgs e)
        {
            try
            {                
                if (Application["loginPage"] != null)
                {
                    Response.Redirect(Application["loginPage"].ToString());
                }
                else if(isSearchConf) //FB 2763
                    Response.Redirect("SearchConferenceInputParameters.aspx");
                else
                    Response.Redirect("~/en/genlogin.aspx?lang=" + hdnBrowserLang.Value, false);//ZD 103174
                    //Response.Redirect("~/en/genlogin.aspx"); //Login Management //FB 1830
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region P2P status
        /// <summary>
        /// P2P status
        /// </summary>
        /// <param name="confid"></param>
        /// <returns></returns>
        protected String GetP2pStatus(String confid)
        {
            String stus = "";
            XmlDocument doc = null;
            try
            {
                if (confid != "")
                {
                    String inXML = "";
                    inXML += "<GetTerminalStatus>"; //Code changed for Blue status
                    inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                    inXML += "  <confID>" + confid + "</confID>";
                    inXML += "  <endpointID></endpointID>";
                    inXML += "  <terminalType></terminalType>";
                    inXML += "</GetTerminalStatus>"; //Code changed for Blue status
                    //Response.Write(obj.Transfer(inXML));
                    // <login> <userID>11</userID> <confID>15</confID> <endpointID></endpointID> <terminalType></terminalType> <displayLayout>01</displayLayout></login>
                    //Code changed for Blue statusString outXML = obj.CallCOM2("GetP2PConfStatus", inXML, Application["RTC_ConfigPath"].ToString());
                    String outXML = obj.CallCOM2("GetTerminalStatus", inXML, Application["RTC_ConfigPath"].ToString());//Code changed for Blue status
                    errLabel.Visible = true;
                    if (outXML.IndexOf("<error>") >= 0)
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                    else
                    {
                        doc = new XmlDocument();
                        doc.LoadXml(outXML);
                        if (doc.SelectSingleNode("GetTerminalStatus/connectionStatus") != null) //Code changed for Blue status
                            stus = doc.SelectSingleNode("GetTerminalStatus/connectionStatus").InnerText;  //Code changed for Blue status
                    }
                }

            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GetP2pStatus:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }

            return stus;
        }

        protected void Updatelist(Object sender, EventArgs e) 
        {
            try
            {
                //Refresh Fix Start

                Session.Add("systemDate", DateTime.Now.ToString("MM/dd/yyyy")); 
                Session.Add("systemTime", DateTime.Now.ToString(tformat));  

                if (Request.QueryString["t"] != null)
                {
                    switch (Request.QueryString["t"].ToString())
                    {

                        case "2": // Ongoing
                            Refreshchk.Checked = true;
                            helpPage.Value = "110";
                            txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled + "," + ns_MyVRMNet.vrmConfStatus.Ongoing; //ZD 100093
                            txtPublic.Value = "";
                            txtConferenceSearchType.Value = "1";
                            initload = false;
                            Filtertype = ns_MyVRMNet.vrmSearchFilterType.Ongoing; //FB 2639 - Search
                            BindReservations(txtConferenceSearchType.Value);
                            break;
                        case "3": // Reservations
                            Refreshchk.Checked = true;
                            helpPage.Value = "110";
                            txtPublic.Value = "";
                            txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled; //ZD 100036
                            //txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled + "," + ns_MyVRMNet.vrmConfStatus.OnMCU;
                            Filtertype = ns_MyVRMNet.vrmSearchFilterType.Reservation;//FB 2639 - Search
                            txtConferenceSearchType.Value = "0";
                            BindReservations(txtConferenceSearchType.Value);
                            break;
                        case "8": // OnMCU  //ZD 100036
                            Refreshchk.Checked = true;
                            helpPage.Value = "110";
                            txtPublic.Value = "";
                            txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.OnMCU;
                            Filtertype = ns_MyVRMNet.vrmSearchFilterType.OnMCU;
                            txtConferenceSearchType.Value = "0";
                            BindReservations(txtConferenceSearchType.Value);
                            break;
                        default:
                            errLabel.Text = obj.GetTranslatedText("Illegal Search");//FB 1830 - Translation
                            break;
                    }
                }
                //Refresh Fix End
                //FB 1985 - Starts
                if (Application["Client"].ToString().ToUpper() == "DISNEY")
                {
                    if (Request.QueryString["t"] != null)
                    {
                        switch (Request.QueryString["t"].ToString())
                        {
                            case "4": // Public
                                Refreshchk.Checked = true;
                                PrivateRSS.Visible = false;
                                helpPage.Value = "110";
                                txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled; //ZD 100036
                                //txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Scheduled + "," + ns_MyVRMNet.vrmConfStatus.OnMCU;
                                txtPublic.Value = "1";
                                txtConferenceSearchType.Value = "";
                                Filtertype = ns_MyVRMNet.vrmSearchFilterType.Public;//FB 2639 - Search
                                BindReservations(txtConferenceSearchType.Value);
                                break;
                            case "5": // Pending
                                Refreshchk.Checked = true;
                                helpPage.Value = "110";
                                txtPublic.Value = "";
                                txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Pending;
                                txtConferenceSearchType.Value = "";
                                Filtertype = ns_MyVRMNet.vrmSearchFilterType.Pending;//FB 2639 - Search
                                BindReservations(txtConferenceSearchType.Value);
                                break;
                            case "6": // Approval
                                Refreshchk.Checked = true;
                                helpPage.Value = "110";
                                txtPublic.Value = "";
                                txtSearchType.Value = ns_MyVRMNet.vrmConfStatus.Pending;
                                txtConferenceSearchType.Value = "";
                                Filtertype = ns_MyVRMNet.vrmSearchFilterType.ApprovalPending;//FB 2639 - Search
                                BindReservations(txtConferenceSearchType.Value);
                                break;
                            default:
                                errLabel.Text = "Illegal Search";
                                break;
                        }
                    }
                }
                //FB 1985 - End
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("Updatelist:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        #endregion

        //RSS Feed -- Start

        #region PublicRSSFeed
        protected void RSSFeed(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("RSSGenerator.aspx?m=p&OrgID=" + drpOrgs.SelectedValue);   //RSS Fix
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        #region PrivateRSSFeed
        protected void PrivateRSSFeed(object sender, EventArgs e)
        {
            try
            {
                //RSS Fix -- Start
                String orgID = "";
                if (Session["organizationID"] != null)
                {
                    if (Session["organizationID"].ToString() != "")
                    {
                        orgID = Session["organizationID"].ToString();
                    }
                }
                //ZD 100599 Starts
                if (Request.UserAgent.ToString().ToLower().Contains("chrome"))
                {
                    string script = "<script type='text/javascript'>window.open('" + "RSSGenerator.aspx?OrgID=" + orgID + "')</script>";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "RSS", script, false);
                }
                else            
                        Response.Redirect("RSSGenerator.aspx?OrgID=" + orgID, false);
                //ZD 100599 Ends

               
                

                //RSS Fix -- End
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }
        #endregion

        //Rss Feed -- End

        #region OrgIndexChanged
        //Organization Module
        protected void OrgIndexChanged(Object sender, EventArgs e)
        {
            try
            {
                Session["OrganizationID"] = drpOrgs.SelectedValue;
                dgConferenceList.DataSource = null;
                dgConferenceList.DataBind();

                BindReservations("");
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("OrgIndexChanged:" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }

        #endregion


        //Method added for FB 1824
        
        #region GetConfOnlineStatus
        /// <summary>
        /// GetConfOnlineStatus
        /// </summary>
        /// <param name="confID"></param>
        /// <returns></returns>
        protected string GetConfOnlineStatus(String confID)
        {
            int status = 0, EPStatus = 0;
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                String inXML = "<login>";
                inXML += obj.OrgXMLElement();
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <confID>" + confID + "</confID>";
                inXML += "</login>";
                log.Trace("ConferenceList_GetTerminalControl: " + inXML);
                //String outXML = obj.CallCOM("GetTerminalControl", inXML, Application["COM_ConfigPath"].ToString());
                String outXML = obj.CallMyVRMServer("GetTerminalControl", inXML, Application["MyVRMServer_ConfigPath"].ToString());//FB 2027(GetTerminalControl)
                log.Trace("ConferenceList_GetTerminalControl: " + outXML);
                
                if (outXML.IndexOf("<error>") < 0)
                {
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodesEP = xmldoc.SelectNodes("//terminalControl/confInfo/terminals/terminal");
                    Boolean isPartialConnected = false; //FB 1824
                    for(int i =0; i < nodesEP.Count; i++)
                    {
                        int.TryParse(nodesEP[i].SelectSingleNode("status").InnerText.Trim(), out EPStatus);
                        if (i == 0)
                            int.TryParse(nodesEP[i].SelectSingleNode("status").InnerText.Trim(), out status);

                        if (EPStatus < status)
                            status = EPStatus;

                        if (EPStatus == 2) //FB 1824
                            isPartialConnected = true;
                    }
                    //FB 1824
                    if (isPartialConnected && status == 0)
                        status = 4;
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("GetConfOnlineStatus" + ex.Message);
                //errLabel.Text = obj.ShowSystemMessage();ZD 100263
                errLabel.Visible = true;
            }
            return status.ToString();
        }
        #endregion

        //FB 2670 - Start
        #region ExportExcel

        protected void ExportExcel(object sender, EventArgs e)
        {
            try
            {
                DataSet objDs = new DataSet();
                if (Session["PrintTable"] != null)
                {
                    dtCustom = (DataTable)Session["PrintTable"];
                    dtCustom.TableName = "ConferenceList";
                    objDs.Tables.Add(dtCustom);
                }

                String DestDirectory = Server.MapPath("..");
                DestDirectory += "/" + Session["Language"].ToString();
                String dFile = "ConferenceList.xls";

                myVRM.ExcelXmlWriter.ExcelUtil exlUtil = new myVRM.ExcelXmlWriter.ExcelUtil();
                Hashtable loglist = new Hashtable();
                loglist.Add("ConferenceList", "Conference List");
                exlUtil.CreateXLFile(loglist, DestDirectory + "\\" + dFile, objDs);

                String[] pathAry = Request.ServerVariables["URL"].Split('/');
                String redURL = "http://" + Request.ServerVariables["http_host"];

                for (Int32 i = 0; i < pathAry.Length - 1; i++)
                {
                    if (pathAry[i].Trim() != "")
                        redURL += "/" + pathAry[i].Trim();
                }

                redURL = redURL + "/" + dFile;
                Response.Redirect(redURL);

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region ExportPDF

        protected void ExportPDF(object sender, EventArgs e)
        {
            try
            {
                if (Session["PrintTable"] != null)
                {
                    dtCustom = (DataTable)Session["PrintTable"];
                    dtCustom.TableName = "ConferenceList";
                }

                GridView GridView1 = new GridView();
                GridView1.AllowPaging = false;
                GridView1.DataSource = dtCustom;
                GridView1.DataBind();

                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=ConferenceList.pdf");
                Response.Cache.SetCacheability(HttpCacheability.NoCache);
                StringWriter sw = new StringWriter();
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                GridView1.RenderControl(hw);
                StringReader sr = new StringReader(sw.ToString());
                iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 10f, 10f, 10f, 0f);
                iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
                iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
                pdfDoc.Open();
                htmlparser.Parse(sr);
                pdfDoc.Close();
                Response.Write(pdfDoc);
                Response.End();

            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace);
            }
        }

        #endregion

        #region CustomTable

        protected DataTable CustomTable()
        {
            try
            {
                dtCustom = new DataTable();

                dtCustom.Columns.Add("Unique ID");
                dtCustom.Columns.Add("Conference Name");
                dtCustom.Columns.Add("Silo Name");
                dtCustom.Columns.Add("Conference Date/Time");
                dtCustom.Columns.Add("Type");
                dtCustom.Columns.Add("Conf Mode");
                dtCustom.Columns.Add("Duration");
                dtCustom.Columns.Add("Owner");
                dtCustom.Columns.Add("Attendee");

                return dtCustom;
            }
            catch (Exception ex)
            {
                //errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("CustomTable:" + ex.Message);//ZD 100263
                return null;
            }
        }

        #endregion

        //FB 2670 - End

        //FB 2822 - Start
        protected void ShowHide()
        {
            try
            {        
                DataGridItem dgi = (DataGridItem)dgConferenceList.Controls[0].Controls[0];
                if (hdnSortingOrder.Value == "1")// && hdnAscendingOrder.Value == "1") ZD 101333
                {
                    ((Label)dgi.FindControl("spnAscendingID")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingConfName")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingSiloName")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingConfDate")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingConfMode")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingIDIE")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingConfNameIE")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingConfDateIE")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingSiloNameIE")).Visible = false;
                    ((Label)dgi.FindControl("spnAscendingConfModeIE")).Visible = false;

                    ((Label)dgi.FindControl("spnDescendingID")).Visible = true;
                    ((Label)dgi.FindControl("spnDescendingConfName")).Visible = true;
                    ((Label)dgi.FindControl("spnDescendingSiloName")).Visible = true;
                    ((Label)dgi.FindControl("spnDescendingConfDate")).Visible = true;
                    ((Label)dgi.FindControl("spnDescendingConfMode")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingIDIE")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingConfNameIE")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingConfDateIE")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingSiloNameIE")).Visible = true;
                    ((Label)dgi.FindControl("spnDescndingConfModeIE")).Visible = true;
                    //hdnAscendingOrder.Value = "0"; ZD 101333                   
                }
                else
                {
                    ((Label)dgi.FindControl("spnAscendingID")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingConfName")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingSiloName")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingConfDate")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingConfMode")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingIDIE")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingConfNameIE")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingConfDateIE")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingSiloNameIE")).Visible = true;
                    ((Label)dgi.FindControl("spnAscendingConfModeIE")).Visible = true;

                    ((Label)dgi.FindControl("spnDescendingID")).Visible = false;
                    ((Label)dgi.FindControl("spnDescendingConfName")).Visible = false;
                    ((Label)dgi.FindControl("spnDescendingSiloName")).Visible = false;
                    ((Label)dgi.FindControl("spnDescendingConfDate")).Visible = false;
                    ((Label)dgi.FindControl("spnDescendingConfMode")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingIDIE")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingConfNameIE")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingConfDateIE")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingSiloNameIE")).Visible = false;
                    ((Label)dgi.FindControl("spnDescndingConfModeIE")).Visible = false;
                    //hdnAscendingOrder.Value = "1";  ZD 101333                  
                }
            }            
            catch (Exception ex)
            {
               // errLabel.Text = ex.StackTrace;ZD 100263
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ShowHide:" + ex.Message);//ZD 100263
                
            }
        }
        //FB 2822 - End

        // ZD 100288
        public string fnGetTranslatedText(string par)
        {
            return obj.GetTranslatedText(par);
        }

        //ZD 100642 Strats
        #region ShowAlternate
        protected void ShowAlternate(Object sender, EventArgs e)
        {
            try
            {
                DataGridItem clickedRow = ((LinkButton)sender).NamingContainer as DataGridItem;
               
                string  AlternateName = "";
                int  altrenateID = 0;
                String[] roomsSelcted = null;
                String[] DoublePlusDelim = { "++" };

                if (locstrname != null)
                    if (locstrname.Value != "")
                    {
                        roomsSelcted = locstrname.Value.Split(DoublePlusDelim, StringSplitOptions.RemoveEmptyEntries);
                        for (int i = 0; i < roomsSelcted.Length; i++)
                        {
                            if (roomsSelcted[i].Trim() == "")
                                continue;

                            int.TryParse(roomsSelcted[i].Split('|')[0].ToString(), out altrenateID);
                            AlternateName = roomsSelcted[i].Split('|')[1];
                        }
                    }

                Label lblName = (Label)clickedRow.FindControl("lblEntityName");
                Label lblID = (Label)clickedRow.FindControl("lblEntityID");
                Label lblAlterRoomID = (Label)clickedRow.FindControl("lblAlterRoomID");
                
                if (AlternateName != "")
                {
                    lblAlterRoomID.Text = lblID.Text + "," + altrenateID; //Original RoomID, Alternate Room ID
                    lblName.Text = AlternateName;
                    lblID.Text = altrenateID.ToString();
                    ((RadioButton)clickedRow.FindControl("rdUndecided")).Enabled = false;
                    ((RadioButton)clickedRow.FindControl("rdApprove")).Enabled = false;
                    ((RadioButton)clickedRow.FindControl("rdDeny")).Enabled = false;

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "DisableOption", "disableOption();", true);//ZD 100718
                }

                return;

            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                errLabel.Visible = true;
                log.Trace("ShowAlternate:" + ex.Message);
            }
        }
        #endregion
        
        #region ResetConference
        /// <summary>
        /// ResetConference
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ResetConference(Object sender, EventArgs e)
        {
            try
            {
                Response.Redirect("ConferenceList.aspx?t=6");
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();
                log.Trace("ResetConference:" + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion
        //ZD 100642 Ends

        //ZD 101315 - Extend Time Start
        #region SubmitExtendTime
        /// <summary>
        /// SubmitExtendTime
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void SubmitExtendTime(Object sender, EventArgs e)
        {
            StringBuilder inXML = new StringBuilder();
            string outXML = "";
            try
            {
                if (hdnConfid.Value == "" || hdnMins.Value == "")
                    return;

                inXML.Append("<login>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<confInfo>");
                inXML.Append("<confID>" + hdnConfid.Value + "</confID>");
                inXML.Append("<retry>0</retry>");
                inXML.Append("<extendEndTime>" + hdnMins.Value + "</extendEndTime>");
                inXML.Append("</confInfo>");
                inXML.Append("</login>");

                outXML = obj.CallMyVRMServer("CheckForExtendTimeConflicts", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    outXML = obj.CallCOM2("SetTerminalControl", inXML.ToString(), HttpContext.Current.Application["RTC_ConfigPath"].ToString());

                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        errLabel.Visible = true;
                    }
                    else
                    {
                        outXML = obj.CallMyVRMServer("SetTerminalControl", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                        if (outXML.IndexOf("<error>") >= 0)
                        {
                            errLabel.Text = obj.ShowErrorMessage(outXML);
                            errLabel.Visible = true;
                        }
                    }
                }
                hdnConfid.Value = "";
                hdnMins.Value = "";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Updatelist", "setTimeout('refresh();',100);", true);               
            }
            catch (Exception ex)
            {
                hdnConfid.Value = "";
                hdnMins.Value = "";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Updatelist", "setTimeout('refresh();',100);", true);       
                log.Trace("SubmitExtendTime:" + ex.Message);
            }
        }
        #endregion
        //ZD 101315 - Extend Time End
		//ZD 101597
        protected void EditConf(Object sender, EventArgs e)
        {
            DataGridCommandEventArgs ea = new DataGridCommandEventArgs(dgConferenceList.Items[0], dgConferenceList, new CommandEventArgs("Edit", new object())); 
            EditConference(new object(), ea);

        }
    }
}
