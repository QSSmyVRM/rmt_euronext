/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;
using System.Text;//ZD 100621
using DevExpress.Web.ASPxPager;
using System.Collections.Generic;
using System.Data.Sql;
using System.Data.SqlTypes;
using System.Data.SqlClient;
using System.Data.Common;

namespace ns_MyVRM
{
    public partial class en_RoomSearch : System.Web.UI.Page
    {
        myVRMNet.NETFunctions obj;
        myVRMNet.ImageUtil imageUtilObj = null;
        ns_Logger.Logger log;
        DataRow[] filtrs = null;
        DataTable dtSel = null;
        DataTable dtDisp = null;
        DataSet DSdisp = null;
        ArrayList rmlist = null;

        DataSet ds = null;
        public String confID = "";
        public String duration = "";
        public String Parentframe = "";
        String outXML = "";
        String outXMLDept = "";
        String tzone = "";
        string immediate = "0"; //FB 2534
        protected String format = "MM/dd/yyyy";
        String tformat = "hh:mm tt";
        XmlTextReader txtrd = null;
        String roomEdit = "N";
        protected String favRooms = "";
        protected String GuestRooms = "";//FB 2426
        String pathName = "";
        string filePath = ""; //Image Project
        byte[] imgArray = null;
        protected String language = "";//FB 1830
        protected String EnableRoomServiceType = "";//FB 2219
        protected String VMR = ""; //FB 2448
        protected string roomVMR = "0"; //FB 2448
        protected int Cloud = 0, pageIndex = 0; //FB 2262 //FB 2599 //ZD 101175
        string Tier1ID = ""; //FB 2637
        string confOrgID = ""; //FB 2646
        protected string HotdeskingRooms = ""; //FB 2694
        protected string ConfType = ""; //FB 2694
        const string PageSizeSessionKey = "ed5e843d-cff7-47a7-815e-832923f7fb09";
        protected int CloudConferencing = 0, favor = 0; //FB 2717 //ZD 102481
        StringBuilder inXML = new StringBuilder(); //ZD 100621
        protected int enableWaitList = 0;
        protected string busyRooms = "";
        protected int isRecurConference = 0;
        //Enyim.Caching.MemcachedClient memClient = null;//for mem cache ZD 104482
        //protected int memcacheEnabled = 0;//ZD 103496 ZD 104482
        protected string hf = "";//ZD 104482 
		protected int RoomViewType = 1;//ZD 104586
        string searchType = ""; //ZD 102916

        # region prviate DataMember

        protected DevExpress.Web.ASPxGridView.ASPxGridView grid;
        protected DevExpress.Web.ASPxGridView.ASPxGridView grid2;
        protected DevExpress.Web.ASPxPager.ASPxPager ASPxPager1; //ZD 101175
        protected DevExpress.Web.ASPxPager.ASPxPager ASPxPager2; //ZD 101175

        protected System.Web.UI.WebControls.DataGrid SelectedGrid;

        protected System.Web.UI.WebControls.Label LblError;
        //protected System.Web.UI.WebControls.Label lblViewType; //ZD 101240
        protected System.Web.UI.WebControls.Label vidLbl;
        protected System.Web.UI.WebControls.Label nvidLbl;
        protected System.Web.UI.WebControls.Label vmrvidLbl;
        protected System.Web.UI.WebControls.Label ROHotdeskingRooms;
        protected System.Web.UI.WebControls.Label VCHotdeskingRooms;
        protected System.Web.UI.WebControls.Label ttlVCHotdeskingRooms;
        protected System.Web.UI.WebControls.Label ttlROHotdeskingRooms;
        protected System.Web.UI.WebControls.Label Label1;
        protected System.Web.UI.WebControls.Label ttlnvidLbl;
        protected System.Web.UI.WebControls.Label lblVMRRooms;
        protected System.Web.UI.WebControls.Label tntvmrrooms;
        protected System.Web.UI.WebControls.Label lblPublicRoom;
        protected System.Web.UI.WebControls.Label ttlPublicLbl;
        protected System.Web.UI.WebControls.Label totalNumber;
        protected System.Web.UI.WebControls.Label lblTotalRecords;//ZD 101175
        protected System.Web.UI.WebControls.Label lblTotalRecords1;//ZD 101175

        protected System.Web.UI.WebControls.CheckBox chkHotdesking;
        protected System.Web.UI.WebControls.CheckBox Available;
        protected System.Web.UI.WebControls.CheckBox chkIsVMR;
        protected System.Web.UI.WebControls.CheckBox chkGuestRooms;
        protected System.Web.UI.WebControls.CheckBox chkFavourites;
        protected System.Web.UI.WebControls.CheckBox MediaNone;
        protected System.Web.UI.WebControls.CheckBox MediaAudio;
        protected System.Web.UI.WebControls.CheckBox MediaVideo;
        protected System.Web.UI.WebControls.CheckBox PhotosOnly;
        protected System.Web.UI.WebControls.CheckBox HandiCap;

        protected System.Web.UI.WebControls.CheckBoxList AVlist;

        protected System.Web.UI.WebControls.DropDownList DrpDwnListView;
        protected System.Web.UI.WebControls.DropDownList DrpActDct;
        protected System.Web.UI.WebControls.DropDownList lstCountry;
        protected System.Web.UI.WebControls.DropDownList lstStates;
        protected System.Web.UI.WebControls.DropDownList lstStates2;
        protected System.Web.UI.WebControls.DropDownList lstStates3;
        protected System.Web.UI.WebControls.DropDownList DrpRecordsperPage;//ZD 101175

        protected System.Web.UI.HtmlControls.HtmlTableRow trDateFromTo;
        protected System.Web.UI.HtmlControls.HtmlTableRow TrRoomAvaible;
        protected System.Web.UI.HtmlControls.HtmlTableRow trActDct;
        protected System.Web.UI.HtmlControls.HtmlTableRow TrLicense;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAvlChk;
        protected System.Web.UI.HtmlControls.HtmlTableRow DetailsView;
        protected System.Web.UI.HtmlControls.HtmlTableRow ListView;

        protected System.Web.UI.HtmlControls.HtmlTableCell TDSelectedRoom;

        protected System.Web.UI.WebControls.RegularExpressionValidator regRoomStartTime;
        protected System.Web.UI.WebControls.RegularExpressionValidator regRoomEndTime;

        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnVMRRoomadded;
        protected System.Web.UI.HtmlControls.HtmlInputHidden locstr;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Tierslocstr;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnServiceType;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTimeZone;
        protected System.Web.UI.HtmlControls.HtmlInputHidden selectedlocframe;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnView;
        protected System.Web.UI.HtmlControls.HtmlInputHidden addroom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDelRoom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnDelRoomID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEditroom;
        protected System.Web.UI.HtmlControls.HtmlInputHidden cmd;
        protected System.Web.UI.HtmlControls.HtmlInputHidden helpPage;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRoomIDs;
        protected System.Web.UI.HtmlControls.HtmlInputHidden IsSettingsChange;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCapacityH;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnCapacityL;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAV;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMedia;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnLoc;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnName;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnZipCode;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnAvailable;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnStartTime;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEndTime;


        protected MetaBuilders.WebControls.ComboBox confRoomStartTime;
        protected MetaBuilders.WebControls.ComboBox confRoomEndTime;

        protected System.Web.UI.WebControls.TextBox txtRoomDateFrom;
        protected System.Web.UI.WebControls.TextBox txtRoomDateTo;
        protected System.Web.UI.WebControls.TextBox TxtNameSearch;
        protected System.Web.UI.WebControls.TextBox txtZipCode;
        protected System.Web.UI.WebControls.TextBox txtTier1; //ZD 101175
        protected System.Web.UI.WebControls.TextBox txtTier2;//ZD 101175
        protected System.Web.UI.WebControls.TextBox txtRoomName;//ZD 101175


        protected System.Web.UI.HtmlControls.HtmlTableRow trHotdesking; //FB 2717
        protected System.Web.UI.HtmlControls.HtmlButton btnClose; //ZD 100642
        //protected System.Web.UI.WebControls.CheckBox ChkUserRoomViewType;//ZD 100621 //ZD 102123
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnImportErr;  //ZD 100619
        //ZD 101225 Starts
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton Av_and;
        protected System.Web.UI.HtmlControls.HtmlInputRadioButton Av_or;
        //ZD 101225 Ends
        //ZD 101098 START
        protected System.Web.UI.WebControls.Label lbliControlRooms;
        protected System.Web.UI.WebControls.Label tntiControlRooms;
        protected System.Web.UI.WebControls.Label icontrolvidlbl;
        //ZD 101098 END
        string VMRdialOUTID = "";//ZD 100522
        protected System.Web.UI.HtmlControls.HtmlInputHidden SelectedRoomNameValue; //ZD 101175
        //ZD 101918 start
        protected System.Web.UI.HtmlControls.HtmlInputText txtDsearchtier1;
        protected System.Web.UI.HtmlControls.HtmlInputText txtDsearchtier2;
        protected System.Web.UI.HtmlControls.HtmlInputText txtDsearchRoomname;
        protected System.Web.UI.HtmlControls.HtmlInputText txtDsearchMaxCapacity;
        protected System.Web.UI.HtmlControls.HtmlInputText txtLsearchtier1;
        protected System.Web.UI.HtmlControls.HtmlInputText txtLsearchtier2;
        protected System.Web.UI.HtmlControls.HtmlInputText txtLsearchRoomname;
        protected System.Web.UI.HtmlControls.HtmlInputText txtLsearchMaxCapacity;
        //ZD 101918 End
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnTierIDlocStr; //ZD 102458
        //ZD 104482
        protected System.Web.UI.HtmlControls.HtmlTableRow trRmName;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCapacity;
        protected System.Web.UI.HtmlControls.HtmlTableRow trCountry;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMedia;
        protected System.Web.UI.HtmlControls.HtmlTableRow trPhoto;
        protected System.Web.UI.HtmlControls.HtmlTableRow trHandi;
		protected System.Web.UI.HtmlControls.HtmlInputHidden hdnChgViewType;//ZD 104586
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSelRoomID;//ZD 102916
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSelectionVal;//ZD 102916
        

        #endregion

        const string PageSizeSession = "ed5e843d-cff7-47a7-815e-832923f7fb10";

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            imageUtilObj = new myVRMNet.ImageUtil();
            String stDate = "";
            String enDate = "";
            String serType = "";

            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("RoomSearch.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                LblError.Visible = false;
                LblError.Text = "";

                grid.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Standard;
                grid.Settings.ShowVerticalScrollBar = true;
                grid.Settings.VerticalScrollableHeight = 450;

                grid2.Settings.VerticalScrollBarStyle = GridViewVerticalScrollBarStyle.Standard;
                grid2.Settings.ShowVerticalScrollBar = true;
                grid2.Settings.VerticalScrollableHeight = 450;

                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");

                if (Session["timeFormat"].ToString() == "2") //FB 2588
                    tformat = "HHmmZ";

                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() != "")
                        format = Session["FormatDateType"].ToString();
                }

                if (tformat == "HH:mm")
                {
                    regRoomEndTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                    regRoomEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                    regRoomStartTime.ValidationExpression = "[0-2][0-9]:[0-5][0-9]";
                    regRoomStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HH:mm)");
                }
                else if (tformat == "HHmmZ")
                {
                    regRoomEndTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                    regRoomEndTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                    regRoomStartTime.ValidationExpression = "[0-2][0-9][0-5][0-9][Z|z]";
                    regRoomStartTime.ErrorMessage = obj.GetTranslatedText("Invalid Time (HHmmZ)");
                }

                if (Request.QueryString["frm"] != null)
                    Parentframe = Request.QueryString["frm"].ToString().Trim();

                //ZD 102916
                if (Parentframe.ToLower() != "frmconferencesetup")
                {
                    if (Session["PartyDatatable"] != null)
                        Session.Remove("PartyDatatable");  
                }

                if (Session["DtDisp"] != null)
                {
                    //ZD 104482 - Starts
                    DataTable dtd = (DataTable)Session["DtDisp"];
                    if (dtd.Columns.Contains("ImageName") )
                    {
                        if (Parentframe == "RoomFloor")
                        {
                            if ((Session["FloorPlanRoomIds"] != null && Session["FloorPlanRoomIds"].ToString() != "") || (Session["floorPlanTiers"] != null && Session["floorPlanTiers"].ToString() != ""))
                                fnFilterRooms(ref dtd);
                        }

                        grid.DataSource = dtd;
                        grid.DataBind();

                        grid2.DataSource = dtd;
                        grid2.DataBind();
                    }
                    //ZD 104482 - End
                }

                if (Session["systemTimezoneID"] != null)
                {
                    if (Session["systemTimezoneID"].ToString() != "")
                    {
                        tzone = Session["systemTimezoneID"].ToString();
                    }
                }

                if (Session["language"] == null)//FB 1830
                    Session["language"] = "en";
                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();

                if (Session["EnableRoomServiceType"] == null)//FB 2219 
                    Session["EnableRoomServiceType"] = "0";
                if (Session["EnableRoomServiceType"].ToString() != "")
                    EnableRoomServiceType = Session["EnableRoomServiceType"].ToString();

                //FB 2599 Start
                if (Session["Cloud"] != null) //FB 2262 
                {
                    if (Session["Cloud"].ToString() != "")
                        Int32.TryParse(Session["Cloud"].ToString(), out Cloud);
                }
                //FB 2599 End

                if (Session["EnableWaitList"] != null && !string.IsNullOrEmpty(Session["EnableWaitList"].ToString()))
                    int.TryParse(Session["EnableWaitList"].ToString(), out enableWaitList);
                /*commented for ZD 104482
                //ZD 103496
                if (Session["MemcacheEnabled"] != null && !string.IsNullOrEmpty(Session["MemcacheEnabled"].ToString()))
                    int.TryParse(Session["MemcacheEnabled"].ToString(), out memcacheEnabled);
                //ZD 104482 */
                if (Request.QueryString["hf"] != null)
                    hf = Request.QueryString["hf"].ToString();

                //FB 2694
                searchType = "";
                if (Request.QueryString["type"] != null)
                    searchType = Request.QueryString["type"].ToString().Trim();

                if (searchType.ToLower() == "h")
                {
                    chkHotdesking.Checked = true;
                    chkHotdesking.Enabled = false;

                    Available.Enabled = false;
                    chkIsVMR.Enabled = false;
                    chkGuestRooms.Enabled = false;
                    chkFavourites.Enabled = false;
                }

                //FB 2646 Starts
                if (Request.QueryString["conforgID"] != null)
                {
                    confOrgID = Request.QueryString["conforgID"].ToString();
                    Session.Add("multisiloOrganizationID", confOrgID);
                }
                //FB 2646 Ends
               

                if (Request.QueryString["rmEdit"] != null)
                    roomEdit = Request.QueryString["rmEdit"].ToString().Trim();

                if (roomEdit == "Y")
                {
                    Session["multisiloOrganizationID"] = null; //FB 2274
                    TrLicense.Attributes.Add("style", "display:block"); //ZD 101175
                    trActDct.Attributes.Add("style", "display:block");
                }

                if (Parentframe == "frmCalendarRoom" || roomEdit == "Y" )
                {
                    TrRoomAvaible.Attributes.Add("style", "display:none;");
                    trAvlChk.Attributes.Add("style", "display:none;");
                    trDateFromTo.Attributes.Add("style", "display:none;");
                }

                if (roomEdit == "N") //FB 2426 //FB 2448
                {
                    chkGuestRooms.Attributes.Add("style", "display:none");
                    chkIsVMR.Attributes.Add("style", "display:none");
                }
                //FB 2717 start
                /*//FB 2599 start
                if (Cloud == 1) //FB 2262
                {
                    lblViewType.Visible = false;
                    DrpDwnListView.Visible = false;
                    chkGuestRooms.Enabled = false;
                    chkIsVMR.Enabled = false;
                    chkIsVMR.Checked = true;
                }
                //FB 2599 End*/

                if (Request.QueryString["CloudConf"] != null)
                {
                    int.TryParse(Request.QueryString["CloudConf"].ToString(), out CloudConferencing);
                }

                if (Cloud == 1 && CloudConferencing == 1)
                {
                    chkHotdesking.Checked = false;
                    trHotdesking.Attributes.Add("style", "display:none;");
                    //chkIsVMR.Enabled = false;
                    //chkIsVMR.Checked = true;
                }
                //FB 2717 End

                if (Request.QueryString["ConfType"] != null)//FB 2694
                {
                    if (Request.QueryString["ConfType"] != "")
                        ConfType = Request.QueryString["ConfType"].ToString();
                }

                //ZD 100602 Starts
                if (Request.QueryString["hdnLnkBtnId"] != null && Request.QueryString["hdnLnkBtnId"].ToString() == "1")
                {
                    MediaNone.Checked = false;
                    MediaNone.Enabled = false;
                }
                //ZD 100602 End

                if (Request.QueryString["immediate"] != null) //Latest issue
                    immediate = Request.QueryString["immediate"].ToString();

                //ZD 102963
                if (Request.QueryString["isRecurrence"] != null)
                    int.TryParse(Request.QueryString["isRecurrence"].ToString(), out isRecurConference);

                if (!IsPostBack)
                {                   
                    //ZD 100621 start
                    GetRoomViewType();

                    if (Request.QueryString["rmsframe"] != null)
                    {
                        selectedlocframe.Value += Request.QueryString["rmsframe"].ToString().Trim();

                        if (Request.QueryString["isVMR"] != null)//FB 2448
                        {
                            if (Request.QueryString["isVMR"] != "")
                                roomVMR = Request.QueryString["isVMR"].ToString();
                        }

                        String selrooms = "";


                        if (selectedlocframe.Value != "")
                        {
                            foreach (String s in selectedlocframe.Value.Split(','))
                            {
                                if (s != "")
                                {
                                    if (selrooms == "")
                                        selrooms = s.Trim();
                                    else
                                        selrooms += "," + s.Trim();
                                }
                            }
                        }

                        selectedlocframe.Value = selrooms;
                    }

                    //ZD 102123 - Start
                    if (Request.QueryString["View"] != null)
                    {
                        DrpDwnListView.SelectedValue = Request.QueryString["View"].ToString();
                        hdnChgViewType.Value = "1";//ZD 104586
                    }
                    //ZD 104586 - Start
                    else
                    {
                        bool expressForm = false;
                        if (Request.QueryString["frm"] != null && Request.QueryString["frm"].ToString() == "frmExpress")
                        {
                            expressForm = true;
                        }
                        if (Request.QueryString["hf"] != null && Request.QueryString["hf"].ToString() == "1" && expressForm == false) // ZD 102123
                        {
                            if (DrpDwnListView.Items.FindByValue("3") != null)
                                DrpDwnListView.Items.Remove(DrpDwnListView.Items.FindByValue("3"));
                        }
                        else if (ConfType != "" && ConfType != ns_MyVRMNet.vrmConfType.HotDesking)
                        {
                            if (DrpDwnListView.Items.FindByValue("3") != null)
                                DrpDwnListView.Items.Remove(DrpDwnListView.Items.FindByValue("3"));
                        }
                        else if (RoomViewType == 2)
                        {
                            string ViewtypeURL = Request.Url.AbsoluteUri;
                            ViewtypeURL = ViewtypeURL.Replace("RoomSearch.aspx", "RoomSearchHotdesking.aspx");                            
                            ViewtypeURL = ViewtypeURL.Replace("rmsframe=", "");
                            ViewtypeURL += "&rmsframe= " + selectedlocframe.Value; 
                            Response.Redirect(ViewtypeURL);                           
                        }
                    }
                    //ZD 104586 - End
                    //if (ChkUserRoomViewType.Checked)
                    //    DrpDwnListView.SelectedValue = "2";
                    //else
                    //    DrpDwnListView.SelectedValue = "1";
                    //ZD 102123 - End

                    //if (Session["roomViewType"] != null) //FB 1577
                    //{
                    //    if (Session["roomViewType"].ToString() != "")
                    //    {
                    //        DrpDwnListView.SelectedValue = Session["roomViewType"].ToString();
                    //    }
                    //}
                    //ZD 100621 end
                    
                    confRoomStartTime.Items.Clear();
                    confRoomEndTime.Items.Clear();
                    obj.BindTimeToListBox(confRoomStartTime, true, true);
                    obj.BindTimeToListBox(confRoomEndTime, true, true);

                    if (selectedlocframe.Value != "" || selectedlocframe.Value != " ")
                        GetselectedLocations(); //ZD 100175
                    else
                        selectedlocframe.Value = "";

                    if (Request.QueryString["confID"] != null)
                    {
                        confID = Request.QueryString["confID"].ToString();
                        //ZD 102963 Starts
                        if (enableWaitList == 1 && ConfType == ns_MyVRMNet.vrmConfType.HotDesking && isRecurConference == 0)
                        {
                            Available.Checked = false;
                            Available.Enabled = false;
                        }
                        else
                        {
                            Available.Checked = true;
                            Available.Enabled = false;
                        }
                        //ZD 102963 End
                    }

                    if (Request.QueryString["stDate"] != null)
                        stDate = Request.QueryString["stDate"].ToString();

                    if (Request.QueryString["enDate"] != null)
                        enDate = Request.QueryString["enDate"].ToString();

                    if (Request.QueryString["serType"] != null)//FB 2219
                    {
                        if (Request.QueryString["serType"] != "")
                            serType = Request.QueryString["serType"].ToString();

                        if (serType != "")
                            hdnServiceType.Value = serType;
                    }

                    //if (Request.QueryString["immediate"] != null) //FB 2534 Commented for Latest issue
                    //    immediate = Request.QueryString["immediate"].ToString();

                    DateTime dStart, dEnd;
                    DateTime.TryParse(stDate, out dStart);
                    DateTime.TryParse(enDate, out dEnd);

                    if (dStart == DateTime.MinValue || dEnd == DateTime.MinValue)
                    {
                        dStart = DateTime.Now;
                        dEnd = dStart.AddMinutes(60);
                    }

                    txtRoomDateFrom.Text = dStart.ToString(format);//ZD 100998
                    txtRoomDateTo.Text = dEnd.ToString(format);//ZD 100998

                    confRoomStartTime.Text = dStart.ToString(tformat);
                    confRoomEndTime.Text = dEnd.ToString(tformat);


                    if (Request.QueryString["tzone"] != null)
                    {
                        if (Request.QueryString["tzone"].ToString() != "")
                            tzone = Request.QueryString["tzone"].ToString();

                        //FB 1796
                        if (tzone != "")
                            hdnTimeZone.Value = tzone;
                    }

                    if (roomVMR == "1")//FB 2448
                        chkIsVMR.Attributes.Add("style", "display:block");
                    //ZD 104482 - Start
                    //BindAv();
                    //ZD 104882                   
                    obj.GetCountryCodes(lstCountry);

                    if (lstCountry.Items.FindByValue("-1") != null)
                        lstCountry.Items.FindByValue("-1").Selected = true;

                    BindStates();
                   
                    //ZD 104482 End
                    GetAllData(null, null);//ZD 101175
                }

                if (roomEdit == "Y")
                    TDSelectedRoom.Attributes.Add("style", "display:none");

                //ZD 100642 Start
                if (Request.QueryString["hdnLnkBtnId"] != null && Request.QueryString["hdnLnkBtnId"].ToString() != "1") //ZD 100602
                {
                    string lnkbtnid = Request.QueryString["hdnLnkBtnId"].ToString();
                    btnClose.Attributes.Add("onclick", "javascript:ClosePopup();window.parent.fnTriggerFromPopup('" + lnkbtnid + "')");
                }
                if (Request.QueryString["hdnLnkBtnId"] != null)
                {
                    trDateFromTo.Attributes.Add("style", "display:none;");
                }
                //ZD 100642 End
                //ZD 102123 - Start //ZD 104586 - Start
                //bool expressForm = false;
                //if (Request.QueryString["frm"] != null && Request.QueryString["frm"].ToString() == "frmExpress")
                //{
                //    expressForm = true;
                //}
                //if (Request.QueryString["hf"] != null && Request.QueryString["hf"].ToString() == "1" && expressForm == false) // ZD 102123
                //{
                //    if (DrpDwnListView.Items.FindByValue("3") != null)
                //        DrpDwnListView.Items.Remove(DrpDwnListView.Items.FindByValue("3"));
                //}
                //else if (confID != "" && ConfType != "")
                //{
                //    if(ConfType != ns_MyVRMNet.vrmConfType.HotDesking)
                //    {
                //        if (DrpDwnListView.Items.FindByValue("3") != null)
                //            DrpDwnListView.Items.Remove(DrpDwnListView.Items.FindByValue("3"));
                //    }
                //}

                //ZD 102123 - End //ZD 104586 - End
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);

            }

        }
        #endregion

        #region Rooms Attribute

        protected void SetRoomAttributes(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if ((e.Item.ItemType.Equals(ListItemType.Item)) || (e.Item.ItemType.Equals(ListItemType.AlternatingItem)))
                {
                    //ZD 102358
                    DataRowView row = e.Item.DataItem as DataRowView;

                    HyperLink lnkRoom = (HyperLink)e.Item.FindControl("btnViewDetails");
                    if (lnkRoom != null)
                        lnkRoom.Attributes.Add("onclick", "javascript:chkresources('" + e.Item.Cells[0].Text + "');");

                    /* HyperLink lnkRooms = (HyperLink)e.Item.FindControl("btnEdit");
                     if (lnkRooms != null)
                         lnkRooms.Attributes.Add("onclick", "javascript:EditRoom('" + e.Item.Cells[0].Text + "');");
                     if (roomEdit == "Y")
                         lnkRooms.Attributes.Add("style", "display:block");*/

                    //ZD 102358
                    String tierID = "";
                    tierID = row["Tier1ID"].ToString() + ":" + row["Tier2ID"].ToString() + ":" + e.Item.Cells[0].Text.Trim();

                    HtmlImage imageDel = (HtmlImage)e.Item.FindControl("ImageDel");

                    if (imageDel != null)
                        imageDel.Attributes.Add("onclick", "javascript:Delroms('" + e.Item.Cells[0].Text.Trim() + "','" + tierID + "');");//ZD 102358


                    //ZD 102916 - Start
                    HtmlImage ImgRoomAttendee = (HtmlImage)e.Item.FindControl("ImgRoomAttendee");
                    if (ImgRoomAttendee != null)
                        ImgRoomAttendee.Attributes.Add("style", "display:none");

                    if ((Session["PartyToRoom"] != null) && (Session["PartyToRoom"].ToString() == "1"))
                    {

                        if (Parentframe.ToLower() == "frmconferencesetup")
                        {
                            if (ImgRoomAttendee != null)
                            {
                                ImgRoomAttendee.Attributes.Add("onclick", "javascript:return getPartyList('" + e.Item.Cells[0].Text + "');");
                                ImgRoomAttendee.Attributes.Add("style", "display:block");
                            }

                        }
                        else
                        {
                            if (ImgRoomAttendee != null)
                                ImgRoomAttendee.Attributes.Add("style", "display:none");
                        }

                        if (ConfType == "8")
                            ImgRoomAttendee.Attributes.Add("style", "display:none");
                    }
                    //ZD 102916 - End
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        #endregion

        #region Bind AV List

        private void BindAv()
        {

            String itemInXML = "";
            String itemOutXML = "<ItemList></ItemList>";
            XmlDocument xmldoc = null;
            try
            {


                if (!IsPostBack)
                {
                    if (Session["roomModule"].ToString() == "1")
                    {
                        itemInXML = "<login><userID>11</userID>" + obj.OrgXMLElement() + "<Type>1</Type>" + obj.OrgXMLElement() + "</login>";
                        itemOutXML = obj.CallMyVRMServer("GetItemsList", itemInXML, Application["MyVRMServer_ConfigPath"].ToString());
                    }
                    xmldoc = new XmlDocument();
                    xmldoc.LoadXml(itemOutXML);

                    AVlist.Items.Add(new ListItem(obj.GetTranslatedText("None"), "None")); //ZD 101344

                    if (AVlist.Items.FindByText(obj.GetTranslatedText("None")) != null)
                        AVlist.Items.FindByText(obj.GetTranslatedText("None")).Selected = true;

                    XmlNodeList nodes = xmldoc.SelectNodes("//ItemList/Item");
                    foreach (XmlNode node in nodes)
                    {
                        if (node.SelectSingleNode("Name") != null)
                            AVlist.Items.Add(new ListItem(node.SelectSingleNode("Name").InnerText, node.SelectSingleNode("Name").InnerText));//ZD 101344

                        if (AVlist.Items.FindByText(node.SelectSingleNode("Name").InnerText) != null)
                            AVlist.Items.FindByText(node.SelectSingleNode("Name").InnerText).Selected = true;
                    }

                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        private void BindStates()
        {
            try
            {
                if (hdnLoc.Value == "0")
                {
                    hdnLoc.Value = "1";

                    if (lstCountry.Items.FindByValue("-1") != null)
                        lstCountry.Items.FindByValue("-1").Text = obj.GetTranslatedText("Any");

                    lstStates.Enabled = true;
                    lstStates2.Enabled = true;
                    lstStates3.Enabled = true;

                    obj.GetCountryStatesSearch(lstStates, lstCountry.SelectedValue);
                    obj.GetCountryStatesSearch(lstStates2, lstCountry.SelectedValue);
                    obj.GetCountryStatesSearch(lstStates3, lstCountry.SelectedValue);

                    if (lstStates.Items.FindByValue("-1") != null)
                    {
                        lstStates.Items.FindByValue("-1").Text = obj.GetTranslatedText("Any");
                        lstStates.Items.FindByValue("-1").Selected = true;
                    }
                    if (lstStates2.Items.FindByValue("-1") != null)
                    {
                        lstStates2.Items.FindByValue("-1").Text = obj.GetTranslatedText("Any");
                        lstStates2.Items.FindByValue("-1").Selected = true;
                    }
                    if (lstStates3.Items.FindByValue("-1") != null)
                    {
                        lstStates3.Items.FindByValue("-1").Text = obj.GetTranslatedText("Any");
                        lstStates3.Items.FindByValue("-1").Selected = true;
                    }
                }


            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        private void CreateTable()
        {
            try
            {
                dtSel = new DataTable();
                dtSel.Columns.Add("RoomID");
                dtSel.Columns.Add("RoomName");
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }

        }

        protected void ASPxGridView1_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                //ZD 104482 - Starts				                
                string RoomiconPath = "../image/RoomIcon/";
                if (e.RowType == DevExpress.Web.ASPxGridView.GridViewRowType.Data)
                {
                    DataRow row = grid.GetDataRow(e.VisibleIndex);

                    if (row != null)
                    {
                        if (row["Capacity"].ToString() == "")
                            row["Capacity"] = 0;
                        else
                            row["Capacity"] = int.Parse(row["Capacity"].ToString());

                        string Videoaval = row["Video"].ToString();
                        if (Videoaval == "2")
                            row["Video"] = obj.GetTranslatedText("Video");
                        else if (Videoaval == "1")
                            row["Video"] = obj.GetTranslatedText("Audio");
                        else
                            row["Video"] = obj.GetTranslatedText("None");

                        string ApprovalReq = row["ApprovalReq"].ToString();
                        if (ApprovalReq == "Yes")
                            row["ApprovalReq"] = obj.GetTranslatedText("Yes");
                        else
                            row["ApprovalReq"] = obj.GetTranslatedText("No");

                        string RoomImage = row["ImageName"].ToString();
                        string ImageWebPath = row["ImageWebPath"].ToString();
                        //string rImage = "";

                        //if (RoomImage == "[None]" || RoomImage == "")
                        //{
                        //    rImage = "../" + Session["language"].ToString() + "/image/noimage.gif";
                        //}
                        //else
                        //{
                        //    rImage = ImageWebPath + RoomImage;
                        //}
                        row["ImageName"] = RoomImage.Replace("{0}", Session["language"].ToString());
                        string rmIconTypeID = row["RoomIconTypeId"].ToString();
                        if (rmIconTypeID == ns_MyVRMNet.vrmAttributeType.AudioOnly)
                        {
                            row["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.AudioOnly;
                            row["ImageName1"] = obj.GetTranslatedText("Audio");
                        }
                        else if (rmIconTypeID == ns_MyVRMNet.vrmAttributeType.Video)
                        {
                            row["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.Video;
                            row["ImageName1"] = obj.GetTranslatedText("Video");
                        }
                        else if (rmIconTypeID == ns_MyVRMNet.vrmAttributeType.RoomOnly)
                        {
                            row["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.RoomOnly;
                            row["ImageName1"] = obj.GetTranslatedText("Media Type is None");
                        }
                        else if (rmIconTypeID == ns_MyVRMNet.vrmAttributeType.Telepresence)
                        {
                            row["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.Telepresence;
                            row["ImageName1"] = obj.GetTranslatedText("Telepresence");
                        }
                        else if (rmIconTypeID == ns_MyVRMNet.vrmAttributeType.HotdeskingAudio)
                        {
                            row["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.HotdeskingAudio;
                            row["ImageName1"] = obj.GetTranslatedText("Hotdesking Audio");
                        }
                        else if (rmIconTypeID == ns_MyVRMNet.vrmAttributeType.HotdeskingVideo)
                        {
                            row["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.HotdeskingVideo;
                            row["ImageName1"] = obj.GetTranslatedText("Hotdesking Video");
                        }
                        else if (rmIconTypeID == ns_MyVRMNet.vrmAttributeType.GuestVideo)
                        {
                            row["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.GuestVideo;
                            row["ImageName1"] = obj.GetTranslatedText("GuestVideo");
                        }
                    }
                    //ZD 104482 - ENd
                    if (e.KeyValue != null)
                    {
                        string RoomT1Name = "";//ZD 101175

                        HyperLink lnkRoom = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "btnViewDetailsDev") as HyperLink;
                        if (lnkRoom != null)
                            lnkRoom.Attributes.Add("onclick", "javascript:return chkresource('" + e.KeyValue.ToString() + "');"); //ZD 100420
                        //ZD 102963 Starts
                        int isBusyRoom = 0;
                        if (busyRooms != "" && Parentframe != "RoomFloor")
                        {
                            string[] busyRoomList = busyRooms.Split(',');
                            var busyRoomId = busyRoomList.Where(rooms => rooms == e.KeyValue.ToString()).ToList();
                            if (busyRoomId != null && busyRoomId.Count > 0)
                                isBusyRoom = 1;
                        }
                        //ZD 102963 End
                        HyperLink lblh = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "Hyper1") as HyperLink;

                        //FB 2637 Starts
                        Tier1ID = "0";
                        if (Parentframe.ToLower() == "frmcalendarroom" || Parentframe.ToLower() == "frmconferencesetup" || Parentframe.ToLower() == "frmexpress" || Parentframe.ToLower() == "roomfloor") //ZD 102358 //ZD 104586
                            Tier1ID = e.GetValue("Tier1ID").ToString() + ":" + e.GetValue("Tier2ID").ToString();
                        //ZD 104482 
                        if (e.GetValue("RoomName") != null)
                            RoomT1Name = e.GetValue("RoomName").ToString();

                        //ZD 100522 Starts
                        VMRdialOUTID = "";
                        if (Parentframe == "frmConferenceSetUp" || Parentframe == "frmExpress")
                            VMRdialOUTID = e.GetValue("VMRDialOutLoc").ToString();
                        //ZD 100522 Ends

                        /*
                        0-> RoomId
                        1-> TierId 1 : TierId 2
                        2-> VMRdialOUTID
                        3-> RoomName
                        4-> isBusyRoom
                        5-> Conference Type
                        */
                        if (RoomT1Name.Contains("'"))
                        {
                            RoomT1Name = RoomT1Name.Replace("'", "�");
                        }


                        string roomsArg = e.KeyValue.ToString() + ";" + Tier1ID + ";" + VMRdialOUTID + ";" + RoomT1Name + ";" + isBusyRoom + ";" + ConfType;

                        if (lblh != null)
                        {
                            // ZD 102963
                            lblh.Attributes.Add("onclick", "Javascript:Addroms('" + roomsArg + "')");
                            lblh.ForeColor = System.Drawing.Color.Green;
                        }
                        //ZD 102963 End
                        HyperLink lblDel = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "DelRoom") as HyperLink;

                        if (lblDel != null)
                            lblDel.Attributes.Add("onclick", "Javascript:delRoom('" + e.KeyValue.ToString() + "');");

                        HyperLink lblEdit = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "Editroom") as HyperLink;

                        if (lblEdit != null)
                            lblEdit.Attributes.Add("onclick", "Javascript:EditRoom('" + e.KeyValue.ToString() + "')");

                        //FB 2426 Start
                        HyperLink lblimport = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "importRoom") as HyperLink;

                        if (lblimport != null)
                            lblimport.Attributes.Add("onclick", "Javascript:ImportRoom('" + e.KeyValue.ToString() + "')");

                        HyperLink lblExtDel = ((DevExpress.Web.ASPxGridView.ASPxGridView)sender).FindRowTemplateControl(e.VisibleIndex, "DelGuestRoom") as HyperLink;

                        if (lblExtDel != null)
                            lblExtDel.Attributes.Add("onclick", "Javascript:delGuestRoom('" + e.KeyValue.ToString() + "')");
                        //FB 2599 Start
                        //FB 2717 Vidyo Integration Start
                        //if (Cloud == 1 || Session["admin"].ToString() == "3") //FB 2262 //FB 2670
                        if (Session["admin"].ToString() == "3") //FB 2262 //FB 2670
                        {
                            /*if (Cloud == 1)
                            {
                                lblEdit.Attributes.Remove("onClick");
                                lblEdit.Enabled = false;
                                lblEdit.Style.Add("cursor", "default");
                                lblEdit.ForeColor = System.Drawing.Color.Gray;
                            }*/
                            //else //FB 2717 Vidyo Integration End
                            lblEdit.Text = obj.GetTranslatedText("View");

                            //lblDel.Attributes.Remove("onClick");
                            //lblExtDel.Attributes.Remove("onClick");



                            //lblDel.Style.Add("cursor", "default");
                            //lblExtDel.Style.Add("cursor", "default");

                            //lblDel.ForeColor = System.Drawing.Color.Gray;
                            //lblExtDel.ForeColor = System.Drawing.Color.Gray;
                            //ZD 100263
                            lblDel.Visible = false;
                            lblExtDel.Visible = false;
                        }
                        else
                        {
                            lblDel.ForeColor = System.Drawing.Color.Red;
                            lblExtDel.ForeColor = System.Drawing.Color.Red;
                            //ZD 102963
                            if (isBusyRoom == 1)
                                lblh.ForeColor = System.Drawing.Color.Red;
                            else
                                lblh.ForeColor = System.Drawing.Color.Green;
                        }
                        //FB 2599 End

                        if (Request.QueryString["rmEdit"] != null)//ZD 101175
                            roomEdit = Request.QueryString["rmEdit"].ToString().Trim();

                        if (lblEdit != null && lblDel != null && lblh != null && lblimport != null && lblExtDel != null)
                        {

                            if (roomEdit == "Y" && chkGuestRooms.Checked == false)
                            {
                                lblh.Attributes.Add("style", "display:none");
                                lblimport.Attributes.Add("style", "display:none");
                                lblExtDel.Attributes.Add("style", "display:none");
                            }
                            else if (chkGuestRooms.Checked == true)
                            {
                                lblh.Attributes.Add("style", "display:none");
                                lblDel.Attributes.Add("style", "display:none");
                                lblEdit.Attributes.Add("style", "display:none");
                                lblimport.Attributes.Add("style", "display:"); // ZD 103880
                                lblExtDel.Attributes.Add("style", "display:"); // ZD 103880
                            }
                            else
                            {
                                chkGuestRooms.Attributes.Add("style", "display:none");
                                lblDel.Attributes.Add("style", "display:none");
                                lblEdit.Attributes.Add("style", "display:none");
                                lblimport.Attributes.Add("style", "display:none");
                                lblExtDel.Attributes.Add("style", "display:none");
                            }
                            //FB 2426 End
                            if (GetDeactRooms(e.KeyValue.ToString()))
                            {
                                lblDel.Text = obj.GetTranslatedText("Activate");
                                lblDel.Attributes.Add("onclick", "Javascript:ActivateRoom('" + e.KeyValue.ToString() + "')");
                                lblDel.Style.Add("color", "Green");
                                //lblEdit.Attributes.Add("style", "display:none");Code added for FB 1600
                                //FB 2670
                                if (Session["admin"].ToString() == "3")
                                {
                                    //lblDel.Attributes.Remove("onClick");

                                    //lblDel.Style.Add("cursor", "default");
                                    //lblDel.Style.Add("color", "Gray");
                                    //lblDel.ForeColor = System.Drawing.Color.Gray;
                                    //ZD 100263
                                    lblDel.Visible = false;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);

            }
        }


        public String GetAvailableRooms()
        {
            String rets = "11";
            myVRMNet.NETFunctions func = new myVRMNet.NETFunctions();

            try
            {
                string inxmls = "<conferenceTime>";
                inxmls += func.OrgXMLElement();
                inxmls += "<userID>" + Session["userID"].ToString() + "</userID>";
                confID = "0";

                if (Request.QueryString["confID"] != null) // FB 1798
                {
                    confID = Request.QueryString["confID"].ToString();//Code added for FB 1727
                    if (confID == "" || confID.ToUpper() == "NEW")
                        confID = "0";
                }

                inxmls += "<confID>" + confID + "</confID>";


                DateTime dStart = DateTime.MinValue;
                DateTime dEnd = DateTime.MinValue;
                try
                {
                    dStart = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtRoomDateFrom.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confRoomStartTime.Text)); //FB 2588
                    dEnd = DateTime.Parse(myVRMNet.NETFunctions.GetDefaultDate(txtRoomDateTo.Text) + " " + myVRMNet.NETFunctions.ChangeTimeFormat(confRoomEndTime.Text)); //FB 2588
                }
                catch (Exception dt)
                {
                    throw new Exception(func.GetTranslatedText("Please enter a valid date time."));
                }
                //FB 1796
                if (hdnTimeZone.Value != "")
                    tzone = hdnTimeZone.Value;

                TimeSpan ts = dEnd.Subtract(dStart);
                inxmls += "		<startDate>" + myVRMNet.NETFunctions.GetDefaultDate(txtRoomDateFrom.Text) + "</startDate>";
                inxmls += "		<startHour>" + dStart.Hour + "</startHour>";
                inxmls += "		<startMin>" + dStart.Minute + "</startMin>";
                inxmls += "		<startSet>" + dStart.ToString("tt") + "</startSet>";
                inxmls += "		<timeZone>" + tzone + "</timeZone>";
                inxmls += "		<immediate>" + immediate + "</immediate>"; //FB 2534

                Double durationMin = ts.TotalMinutes;
                inxmls += "		<durationMin>" + durationMin.ToString() + "</durationMin>";
                //FB 2634
                //FB 2594 Starts
                //string isPublicEP = "0";
                //if (Session["EnablePublicRooms"].ToString() == "1")
                //    isPublicEP = "1";
                //inxmls += "		<enablePublicRoom>" + isPublicEP + "</enablePublicRoom>";
                //FB 2594 Ends
                inxmls += "<ConfType>" + ConfType + "</ConfType>";
                inxmls += "</conferenceTime>";


                String outxmls = obj.CallMyVRMServer("GetBusyRooms", inxmls, Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(outxmls);

                XmlNode busy = doc.SelectSingleNode("roomIDs");

                if (busy != null)
                {
                    if (busy.InnerText != "")
                        rets = busy.InnerText;

                }

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
            func = null;
            return rets;
        }



        #endregion

        #region Get Selective rooms
        //ZD 101175 Start
        public void GetselectedLocations()
        {
            HttpContext.Current.Session["locstr"] = null; //ZD 101175
            DataTable dtselected = null;
            string selrooms = "";
            string selinXML = "", selOutXML = "";
            DataSet ds1 = new DataSet();
            myVRMNet.NETFunctions func = new myVRMNet.NETFunctions();
            try
            {
                if (selectedlocframe.Value != "")
                {
                    foreach (String s in selectedlocframe.Value.Split(','))
                    {
                        if (s != "")
                        {
                            if (selrooms == "")
                                selrooms = s.Trim();
                            else
                                selrooms += "," + s.Trim();
                        }
                    }
                }

                if (!string.IsNullOrEmpty(selrooms))
                {
                    selinXML = "<SelectedRooms>" + func.OrgXMLElement() + "<RoomID>" + selrooms + "</RoomID></SelectedRooms>";
                    selOutXML = obj.CallMyVRMServer("SelectedRooms", selinXML, Application["MyVRMServer_ConfigPath"].ToString());
                    if (selOutXML.IndexOf("<error>") >= 0)
                    {
                        LblError.Text = obj.ShowErrorMessage(selOutXML);
                        LblError.Visible = true;
                        return;
                    }
                    else
                    {
                        XmlDocument xmldoc = new XmlDocument();
                        xmldoc.LoadXml(selOutXML);
                        XmlNodeList nodes = xmldoc.SelectNodes("//Rooms/Room");
                        ds1 = new DataSet();

                        foreach (XmlNode node in nodes)
                        {
                            txtrd = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds1.ReadXml(txtrd, XmlReadMode.InferSchema);
                        }

                        if (ds1.Tables.Count > 0)
                        {

                            if (ds1.Tables[0] != null)
                            {
                                Tierslocstr.Value = "";
                                hdnTierIDlocStr.Value = ""; //ZD 102358
                                locstr.Value = "";
                                dtselected = ds1.Tables[0].Clone();
                                foreach (DataRow rw in ds1.Tables[0].Rows)
                                {
                                    if (Tierslocstr.Value == "")
                                        Tierslocstr.Value = rw["Tier1Name"].ToString() + " > " + rw["Tier2Name"].ToString() + " > " + rw["RoomName"].ToString();
                                    else
                                        Tierslocstr.Value += "�" + rw["Tier1Name"].ToString() + " > " + rw["Tier2Name"].ToString() + " > " + rw["RoomName"].ToString(); //ZD 104000

                                    if (hdnTierIDlocStr.Value == "")
                                        hdnTierIDlocStr.Value = rw["Tier1ID"].ToString() + ":" + rw["Tier2ID"].ToString() + ":" + rw["RoomID"].ToString() + ":D";
                                    else
                                        hdnTierIDlocStr.Value += "�" + rw["Tier1ID"].ToString() + ":" + rw["Tier2ID"].ToString() + ":" + rw["RoomID"].ToString() + ":D"; //ZD 104000

                                    if (locstr.Value == "")
                                        locstr.Value = rw["RoomID"].ToString() + "|" + rw["RoomName"].ToString();
                                    else
                                        locstr.Value += "++" + rw["RoomID"].ToString() + "|" + rw["RoomName"].ToString();

                                    dtselected.ImportRow(rw);
                                }
                            }
                        }
                        else
                            dtselected = new DataTable();

                        HttpContext.Current.Session["locstr"] = locstr.Value;
                        SelectedRoomNameValue.Value = locstr.Value.Replace("++", "�"); //ALT + 147
                        HttpContext.Current.Session["Tierslocstr"] = Tierslocstr.Value;
                        SelectedGrid.DataSource = dtselected;
                        SelectedGrid.DataBind();


                    }
                }
                else
                {
                    dtselected = new DataTable();
                    SelectedRoomNameValue.Value = "";
                    HttpContext.Current.Session["locstr"] = null;
                    HttpContext.Current.Session["Tierslocstr"] = null;
                    SelectedGrid.DataSource = dtselected;
                    SelectedGrid.DataBind();
                }
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }
        //ZD 101175 End
        public Boolean GetDeactRooms(String rmids)
        {
            String deacRooms = "";
            Boolean retrn = false;
            try
            {
                myVRMNet.NETFunctions func = new myVRMNet.NETFunctions();
                String inXMLDept = "<login>";
                inXMLDept += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXMLDept += func.OrgXMLElement();
                inXMLDept += "</login>";
                if (rmlist == null)
                {

                    String outXML = func.CallMyVRMServer("GetDeactivatedRooms", inXMLDept, Application["MyVRMServer_ConfigPath"].ToString());

                    XmlDocument docdata = new XmlDocument();
                    docdata.LoadXml(outXML);

                    XmlNode favlist = docdata.SelectSingleNode("roomIDs");
                    if (favlist != null)
                        deacRooms = favlist.InnerText;

                    if (deacRooms != "")
                    {
                        rmlist = new ArrayList();
                        foreach (String s in deacRooms.Split(','))
                        {
                            if (s != "")
                                rmlist.Add(s);

                        }
                    }
                }

                if (rmlist != null)
                    retrn = rmlist.Contains(rmids);

                func = null;

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }

            return retrn;

        }

        #endregion

        #region Delete Room - Submit Button Event Handler
        /// <summary>
        /// To Delete Room - Delete Click Button
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnDeleteRoom_Click()
        {
            String sDelMnLoc = selectedlocframe.Value.Trim();
            String sRmId = "";
            String inDelXML = "";
            String outDelXML = "";
            String cmdDelRM = "DeleteRoom";
            XmlDocument XmlDel = null;
            XmlNodeList delnodes = null;
            XmlNode srch = null;
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            XmlDocument createRooms = null;
            XmlNodeList roomList = null;
            string actvteID = "";

            try
            {
                myVRMNet.NETFunctions func = new myVRMNet.NETFunctions();
                if (hdnDelRoomID.Value != "")
                {
                    XmlDocument XmlDoc = new XmlDocument();
                    if (hdnDelRoom.Value == func.GetTranslatedText("Activate"))
                    {
                        cmdDelRM = "ActiveRoom";
                        actvteID = "0";
                    }
                    else if (hdnDelRoom.Value == func.GetTranslatedText("Deactivate"))
                    {
                        cmdDelRM = "DeleteRoom";
                        actvteID = "1";
                    }
                    //FB 2426 Start
                    else if (hdnDelRoom.Value == func.GetTranslatedText("Import"))
                    {
                        cmdDelRM = "SetGuesttoNormalRoom";
                        actvteID = "0";
                    }
                    else if (hdnDelRoom.Value == func.GetTranslatedText("Delete"))
                    {
                        cmdDelRM = "DeleteGuestRoom";
                        actvteID = "1"; //ZD 103496
                    }
                    //FB 2426 End
                    inDelXML += "<login>";
                    inDelXML += "<userID>" + Session["userID"].ToString() + "</userID>";
                    inDelXML += func.OrgXMLElement();
                    inDelXML += "<roomID>" + hdnDelRoomID.Value + "</roomID>";
                    inDelXML += "</login>";
                    hdnImportErr.Value = "0"; //ZD 100619
                    //ZD 100522 Start
                    XmlDel = new XmlDocument();
                    outDelXML = obj.CallMyVRMServer(cmdDelRM, inDelXML, Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027

                    if (outDelXML.IndexOf("<error>") >= 0)
                    {
                        LblError.Text = obj.ShowErrorMessage(outDelXML); //FB 1577
                        LblError.Visible = true;
                        //ZD 100619 Starts
                        if (cmdDelRM == "SetGuesttoNormalRoom")
                            hdnImportErr.Value = "1";
                        //ZD 100619 Ends
                        return;
                    }
                    if (cmdDelRM == "DeleteRoom")
                    {
                        XmlDel = new XmlDocument();
                        outDelXML = obj.CallCOM2("TerminateVMRConference", inDelXML, Application["RTC_ConfigPath"].ToString());
                        if (outDelXML.IndexOf("<error>") >= 0)
                        {
                            LblError.Text = obj.ShowErrorMessage(outDelXML);
                            LblError.Visible = true;
                            //return;
                        }
                    }

                    if (outDelXML.IndexOf("<UserStatus>") > 0) //FB 1644
                    {
                        XmlDocument xmld = new XmlDocument();
                        xmld.LoadXml(outDelXML);

                        LblError.Text = "The Assistant-In-Charge of \"" + xmld.SelectSingleNode("success/AssistantInchargeName").InnerText//FB 2974
                                    + "\" is not found in the Active User List.";
                        LblError.Visible = true;
                        return;
                    }

                    /* Coomented for ZD 104482
                    //ZD 103496 Starts
                    #region Memcache
                    DataTable dtaddTable = null;
                    DataRow[] RoomOldRows = null;
                    DataSet dsCache = null;
                    DataView dv = null;
                    DataTable dtNewTable = null;
                    bool bRet = false;
                    if (memcacheEnabled == 1)
                    {
                        using (memClient = new Enyim.Caching.MemcachedClient())
                        {
                            dtNewTable = new DataTable();
                            obj.GetTablefromCache(ref memClient, ref dsCache);

                            if (dsCache.Tables.Count > 0)
                                dtaddTable = dsCache.Tables[0];


                            bRet = obj.GetAllRoomsInfo(hdnDelRoomID.Value, "0", ref dtNewTable);

                            if (!bRet || dtNewTable.Rows.Count <= 0)
                            {
                                //dtTable = memClient.Get<DataTable>("myVRMRoomsDatatable");
                                obj.GetAllRoomsInfo("", "0", ref dtaddTable, ref dsCache);
                            }
                            RoomOldRows = dtaddTable.Select("Roomid = '" + hdnDelRoomID.Value + "'");

                            if (RoomOldRows != null && RoomOldRows.Count() > 0)
                            {
                                dtaddTable.Rows.Remove(RoomOldRows[0]);
                                dtaddTable.Rows.Add(dtNewTable.Rows[0].ItemArray);
                            }
                            else
                                dtaddTable.Rows.Add(dtNewTable.Rows[0].ItemArray);


                            dv = new DataView(dtaddTable);
                            dv.Sort = "Tier1Name,Tier2Name,RoomName ASC";
                            dtaddTable = dv.ToTable();                          
                            obj.AddRoomTabletoCache(ref memClient, ref dsCache);
                            //memClient.Store(Enyim.Caching.Memcached.StoreMode.Set, "myVRMRoomsDatatable", dtTable);

                        }
                    }

                    #endregion
                    //ZD 103496 End
                    */
                    rmlist = null;
                    func = null;
                    hdnDelRoom.Value = "";
                    GetAllData(null, null);//ZD 101175

                }

                hdnDelRoomID.Value = "";//FB 1830


            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);


            }
            finally
            {
                hdnDelRoomID.Value = "";
            }
        }

        #endregion

        #region BindCountry

        protected void BindCountry(Object sender, EventArgs e)
        {
            try
            {
                BindStates();
                /*Type t = typeof(Button);
                object[] p = new object[1];
                p[0] = EventArgs.Empty;
                MethodInfo m = t.GetMethod("OnClick", BindingFlags.NonPublic | BindingFlags.Instance);
                m.Invoke(btnRefreshRooms, p);*/
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }

        #endregion

        #region Custom Call

        protected void Grid_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            grid.DataBind();
        }

        protected void Grid2_CustomCallback(object sender, ASPxGridViewCustomCallbackEventArgs e)
        {
            grid2.DataBind();
        }

        protected void Grid_DataBound(object sender, EventArgs e)
        {
            grid.JSProperties["cpPageCount"] = grid.PageCount;
        }

        protected void Grid2_DataBound(object sender, EventArgs e)
        {

            grid2.JSProperties["cpPageCount"] = grid2.PageCount;
        }

        #endregion

        #region Change View


        private void changeView()
        {
            try
            {
                //ZD 104586 - Start
                if(hdnChgViewType.Value == "1")
                    SetRoomViewType();

                DetailsView.Attributes.Add("style", "display:none");
                ListView.Attributes.Add("style", "display:none");

                if (DrpDwnListView.SelectedValue == "2")
                    DetailsView.Attributes.Add("style", "display:block");
                else if (DrpDwnListView.SelectedValue == "1")
                    ListView.Attributes.Add("style", "display:block");
                else
                {

                    string ViewtypeURL = Request.Url.AbsoluteUri;
                    ViewtypeURL = ViewtypeURL.Replace("RoomSearch.aspx", "RoomSearchHotdesking.aspx");
                    ViewtypeURL = ViewtypeURL.Replace("rmsframe=", "");
                    ViewtypeURL += "&rmsframe= " + selectedlocframe.Value;
                    Response.Redirect(ViewtypeURL);                 
                }
                //ZD 104586 - End
            }
            catch (Exception ex)
            { }
        }

        #endregion

        #region ViewChanges
        /// <summary>
        /// Changing the View type
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        protected void DrpDwnListView_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {	//ZD 100621 start
                //Session.Remove("roomViewType");

                //if (Session["roomViewType"] == null)
                //{
                //    Session.Add("roomViewType", DrpDwnListView.SelectedValue);
                //}
                //else
                //    Session["roomViewType"] = DrpDwnListView.SelectedValue;
                //ZD  100621 End
                changeView();
                hdnChgViewType.Value = "0";//ZD 104586

            }
            catch (Exception ex)
            {
                log.Trace("ListViewChange: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region WriteToFile
        private void WriteToFile(string roomid, String path, string RoomImageID)
        {
            byte[] myData; string imageString = "";//ZD 101611
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                XmlDocument XmlImages = null;
                if (roomid != "" && path != "")
                {
                    String inXML = "<GetImages>";//FB 1756
                    inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                    inXML += obj.OrgXMLElement();
                    inXML += "  <RoomID>" + roomid + "</RoomID>";
                    inXML += "</GetImages>"; //FB 1756

                    XmlImages = new XmlDocument();
                    String outImgXML = obj.CallCommand("GetImages", inXML);

                    if (outImgXML.IndexOf("<error>") < 0)
                    {
                        XmlImages.LoadXml(outImgXML);

                        XmlNode imgbytes = XmlImages.SelectSingleNode("//GetImages/Image");

                        // Create a file
                        FileStream newFile = new FileStream(path, FileMode.Create);

                        if (imgbytes != null)
                        {
                            if (imgbytes.InnerText != "")
                            {
                                imgArray = imageUtilObj.ConvertBase64ToByteArray(imgbytes.InnerText);

                                //ZD 101611 start
                                System.Drawing.Image image = System.Drawing.Image.FromStream(new System.IO.MemoryStream(imgArray));
                                if (image.Width > 250 || image.Height > 250)
                                {
                                    int roomImgWidth = 250;
                                    int roomImgHeight = 250;
                                    myData = obj.Imageresize(image.Width, image.Height, roomImgWidth, roomImgHeight, image);
                                    imageString = imageUtilObj.ConvertByteArrToBase64(myData);
                                    SetRoomImage(imageString, RoomImageID);
                                    newFile.Write(myData, 0, myData.Length);
                                }

                                else
                                {
                                    newFile.Write(imgArray, 0, imgArray.Length);
                                }
                                // Write data to the file
                                //newFile.Write(imgArray, 0, imgArray.Length);

                                // Close file
                                newFile.Close();
                                newFile.Dispose();
                                newFile = null;
                                imgArray = null;
                                imgbytes = null;

                            }
                        }

                    }


                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 100621  start
        protected void ChangeRoomViewType(object sender, EventArgs e)
        {

            try
            {
                //ZD 102123 - Start
                //if (ChkUserRoomViewType.Checked)
                //{                    
                //    DrpDwnListView.SelectedValue = "2";
                //}
                //else
                //{                   
                //    DrpDwnListView.SelectedValue = "1";
                //}
                //ZD 102123 - End

                SetRoomViewType();
                changeView();
            }
            catch (Exception ex)
            { }
        }

        //ZD 100621  start
        private string GetRoomViewType()
        {
            try
            {
                string outXML = "";
                //int RoomViewType = 0; //ZD 104586
                inXML.Append("<GetUserRoomViewType>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("</GetUserRoomViewType>");
                outXML = obj.CallMyVRMServer("GetUserRoomViewType", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNode node = (XmlNode)xmldoc.DocumentElement;
                    node = xmldoc.SelectSingleNode("//GetUserRoomViewType/RoomViewType");
                    if (node != null)
                    {
                        int.TryParse(node.InnerText.Trim(), out RoomViewType);
                        //ZD 102123 - Start
                        //if (RoomViewType.Equals(1))
                        //    ChkUserRoomViewType.Checked = true;
                        //else
                        //    ChkUserRoomViewType.Checked = false;
						//ZD 104586 - Start
                        if (RoomViewType.Equals(0))
                            DrpDwnListView.SelectedValue = "2"; 
                        else
                            DrpDwnListView.SelectedValue = "1";
                        //ZD 104586 - End
                        //ZD 102123 - End
                    }
                    else
                    {
                        DrpDwnListView.SelectedValue = "1";
                        //ChkUserRoomViewType.Checked = true; //ZD 102123
                    }
                    //ZD 101175 Start
                    node = xmldoc.SelectSingleNode("//GetUserRoomViewType/RoomRecordsView");
                    if (node != null)
                    {
                        DrpRecordsperPage.SelectedValue = node.InnerText.Trim();
                    }
                    else
                        DrpRecordsperPage.SelectedValue = "20";
                    //ZD 101175 End
                    return outXML;
                }
                return outXML;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return "";
            }

        }
        private void SetRoomViewType()
        {
            try
            {
                string outxml = "";
                inXML = new StringBuilder();
                inXML.Append("<SetUserRoomViewType>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                //ZD 102123 - Start
                //if (ChkUserRoomViewType.Checked)
                //    inXML.Append("<RoomViewType>1</RoomViewType>");
                //else
                //    inXML.Append("<RoomViewType>0</RoomViewType>");

                if (DrpDwnListView.SelectedValue == "1")//ZD 101966
                    inXML.Append("<RoomViewType>1</RoomViewType>");
                else if (DrpDwnListView.SelectedValue == "2")
                    inXML.Append("<RoomViewType>0</RoomViewType>");
                else if (DrpDwnListView.SelectedValue == "3")
                    inXML.Append("<RoomViewType>2</RoomViewType>");
                //ZD 102123 - End

                inXML.Append("<RoomRecordsView>" + DrpRecordsperPage.SelectedValue + "</RoomRecordsView>"); //ZD 101175

                inXML.Append("</SetUserRoomViewType>");
                outxml = obj.CallMyVRMServer("SetUserRoomViewType", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }


        //ZD 100621  End

        //ZD 101175 Start

        #region Paging and Filtering
        /// <summary>
        /// Paging and Filtering
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ASPxPager_PageIndexChanged(object sender, EventArgs e)
        {
            ASPxPager pager = sender as ASPxPager;
            pageIndex = pager.PageIndex;

            GetAllData(null, null);

        }

        protected void ASPxPager_PageIndexChanged2(object sender, EventArgs e)
        {
            ASPxPager pager = sender as ASPxPager;
            pageIndex = pager.PageIndex;

            GetAllData(null, null);

        }
        #endregion

        #region DrpRecordsperPage
        /// <summary>
        /// DrpRecordsperPage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void DrpRecordsperPage_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GetAllData(null, null);
            }
            catch (Exception ex)
            {
                log.Trace("ListViewChange: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetAllDatas
        /// <summary>
        /// Get Rooms Information
        /// </summary>
        public void GetAllData(object sender, EventArgs e)
        {
            myVRMNet.NETFunctions func = new myVRMNet.NETFunctions();
            XmlWriter xWriter = null;
            XmlWriterSettings xSettings = new XmlWriterSettings();
            StringBuilder RoomXML = new StringBuilder();
            string selQuery = "", mediaQry = "", stateQuery = "", avalRoomIDs = "", AVItemNone = "0";
            string avItemQueryAnd = "", avItemQueryAndorOr = "", rmName = "", Department = "";
            //XmlDocument deptdoc = null;//ZD 103496
            DataView dv = null;
            DataTable dt = new DataTable();
            string inXMLDept = ""; //ZD 102481
            //string Tier1Qry = "", Tier2Qry = "", RoomNameQry = "", MaxCapQry = ""; //ZD 101918//ZD 103496
            string RoomiconPath = "../image/RoomIcon/"; //ZD 103569 
            XmlDocument xmldoc = null;
            XmlNodeList nodes = null;
            String roomXML = "";
            string Lsearchtier1 = "", Lsearchtier2 = "", LsearchRoomname = "", DsearchRoomname = "", Dsearchtier1 = "", Dsearchtier2 = "", Tier1 = "", Tier2 = "", RoomName = "";//ZD 104000
            try
            {
                #region Fetch Room Departments

                //ZD 102481 Starts
                inXMLDept = "<login>";
                inXMLDept += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXMLDept += func.OrgXMLElement();
                inXMLDept += "</login>";

                //outXMLDept = obj.CallMyVRMServer("GetRoomsDepts", inXMLDept, Application["MyVRMServer_ConfigPath"].ToString());
                //if (Session["UserdeptXML"] == null)
                //{
                //    Session.Add("UserdeptXML", outXMLDept);
                //}
                //else
                //    Session["UserdeptXML"] = outXMLDept;
                //ZD 102481 Ends
                #endregion

                if (hdnDelRoom.Value != "" && hdnDelRoom.Value != "0")
                {
                    btnDeleteRoom_Click();
                    hdnDelRoom.Value = "";
                }

                //ZD 102916 - Start
                if (hdnSelectionVal.Value == "1" || hdnSelectionVal.Value == "2")
                {
                    UpdatePartyList(null, null);
                    hdnSelectionVal.Value = "0";
                }
                //ZD 102916 - End


                #region GetAllRoomData

                if (addroom.Value == "1")
                {
                    addroom.Value = "0";
                    GetselectedLocations();
                }
                else
                {
                    #region memacahe code commented
                    /* Commented for ZD 104482
                    //ZD 103496 Starts
                    if (memcacheEnabled == 1)
                    {
                        #region Selection Query

                        if (Av_and.Checked)
                            avItemQueryAndorOr = "and";
                        else
                            avItemQueryAndorOr = "or";

                        if (Request.QueryString["isTel"] != null)
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " isTelePresence = '0'";
                        }
                        
                        if (Available.Checked)
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " RoomID NOT IN  (" + GetAvailableRooms() + ")";
                        }//ZD 102963
                        else if (ConfType == ns_MyVRMNet.vrmConfType.HotDesking && enableWaitList == 1 && isRecurConference == 0 && !Available.Checked)
                        {
                            busyRooms = GetAvailableRooms();
                        }
                       
                        if (Session["DedicatedVideo"].ToString() != "1")
                        {
                            if (Request.QueryString["dedVid"] != null)
                            {
                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                selQuery += " DedicatedVideo = '0'";

                            }
                        }

                        
                        if (favRooms == "0")
                            chkFavourites.Checked = false;

                        //ZD 102481 Starts
                        favor = 0;
                        if (chkFavourites.Checked)
                            favor = 1;
                        //ZD 102481 Ends

                        //ZD 103496 Starts
                        if (DrpActDct != null)
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " Disabled = '" + DrpActDct.SelectedValue + "'";
                        }

                        int RoomOrgId = 0;
                        if (Session["organizationID"] != null && Session["organizationID"].ToString().Trim() != "")
                            int.TryParse(Session["organizationID"].ToString().Trim(), out RoomOrgId);

                        if (Session["multisiloOrganizationID"] != null && Session["multisiloOrganizationID"].ToString().Trim() != "")
                        {
                            int multiOrg = 0;
                            int.TryParse(Session["multisiloOrganizationID"].ToString().Trim(), out multiOrg);
                            if (multiOrg > 11)
                                RoomOrgId = multiOrg;
                        }

                        if (RoomOrgId >= 11)
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " orgId = '" + RoomOrgId + "'";
                        }
                        //ZD 103496 End

                        if (GuestRooms == "0")
                            chkGuestRooms.Checked = false;

                        if (chkGuestRooms.Checked)
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " Extroom = '1'";

                            if (!(Session["admin"].ToString().Trim() == "2"))
                            {
                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                selQuery += " LoginUserId = '" + Session["userID"].ToString() + "'";
                            }

                        }
                        else
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " Extroom = '0'";
                        }

                        if (HotdeskingRooms == "0")
                            chkHotdesking.Checked = false;

                        if (ConfType == "8")
                        {
                            chkHotdesking.Checked = true;
                            chkHotdesking.Enabled = false;
                        }

                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        if (chkHotdesking.Checked)
                        {
                            selQuery += " RoomCategory = '4'";
                        }
                        else
                        {
                            selQuery += " RoomCategory NOT IN (4) ";
                        }

                        if (VMR == "0")
                            chkIsVMR.Checked = true;

                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        if (chkIsVMR.Checked)
                        {
                            selQuery += " IsVMR = '1'";
                            if (Cloud == 1 && CloudConferencing == 1)
                            {
                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                selQuery += " RoomCategory = '5' and OwnerID = '" + Session["userID"].ToString() + "'";
                            }
                        }
                        else
                        {
                            selQuery += " IsVMR = '0'";
                        }

                        if (Cloud <= 0 && CloudConferencing <= 0)
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " RoomCategory not in (5) ";
                        }

                        string isPublicEP = "0";
                        if (Session["EnablePublicRooms"].ToString() == "1")
                            isPublicEP = "1";

                        if (isPublicEP == "0")
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " isPublic = '0'";
                        }

                        if (txtTier1 != null && txtTier1.Text != "")
                        {
                            if (txtTier1.Text.Contains("'")) //ZD 104000 start
                            {
                                Tier1 = txtTier1.Text.Replace("'", "''");

                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                selQuery += " Tier1Name Like  '%" + Tier1 + "%' ";
                            }
                            else
                            {
                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                selQuery += " Tier1Name Like  '%" + txtTier1.Text + "%' ";
                            }
                        }
                        if (txtTier2 != null && txtTier2.Text != "")
                        {
                            if (txtTier2.Text.Contains("'"))
                            {
                                Tier2 = txtTier2.Text.Replace("'", "''");

                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                selQuery += " Tier2Name Like  '%" + Tier2 + "%' ";
                            }
                            else
                            {
                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                selQuery += " Tier2Name Like  '%" + txtTier2.Text + "%' ";
                            }

                        }
                        if (txtRoomName != null && txtRoomName.Text != "")
                        {
                            if (txtRoomName.Text.Contains("'"))
                            {
                                RoomName = txtRoomName.Text.Replace("'", "''");

                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                selQuery += " RoomName Like  '%" + RoomName + "%' ";
                            }//ZD Latest
                            else
                            {
                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                selQuery += " RoomName Like  '%" + txtRoomName.Text + "%' ";
                            }
                        }
                        //ZD 104000 End
                        if (hdnName.Value == "1")
                        {
                            selQuery = ((selQuery == "") ? " RoomName like '%" + TxtNameSearch.Text + "%'" : selQuery += " and RoomName like '%" + TxtNameSearch.Text + "%'");//ZD Latest
                        }
                        else
                        {
                            if (hdnCapacityL.Value != "" || hdnCapacityH.Value != "")
                            {
                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                selQuery += " MaximumCapacity >= " + hdnCapacityL.Value;

                                if (hdnCapacityH.Value != "")
                                    selQuery += " and MaximumCapacity <= " + hdnCapacityH.Value;
                            }

                            if (ConfType != "" && ConfType != "8" && chkHotdesking.Checked)
                            {
                                mediaQry = " Video = '2' ";
                            }
                            else
                            {
                                if (MediaNone.Checked)
                                    mediaQry += " Video = '0' ";

                                mediaQry = ((mediaQry == "" || !MediaAudio.Checked) ? mediaQry : mediaQry += " or ");

                                if (MediaAudio.Checked)
                                    mediaQry += " Video = '1' ";

                                mediaQry = ((mediaQry == "" || !MediaVideo.Checked) ? mediaQry : mediaQry += " or ");

                                if (MediaVideo.Checked)
                                    mediaQry += " Video = '2' ";

                            }
                            mediaQry = "( " + mediaQry + " )";

                            selQuery = ((selQuery == "") ? mediaQry : selQuery + " and " + mediaQry);
                            if (hdnZipCode.Value == "1")
                            {
                                selQuery += " and ZipCode Like  '%" + txtZipCode.Text + "%' ";

                            }
                            else
                            {
                                if (lstCountry.SelectedValue != "-1")
                                    selQuery += " and Country = " + lstCountry.SelectedValue;

                                if (lstStates.SelectedValue != "-1")
                                    stateQuery += " State = " + lstStates.SelectedValue;

                                stateQuery = ((stateQuery == "" || lstStates2.SelectedValue == "-1") ? stateQuery : stateQuery += " or ");

                                if (lstStates2.SelectedValue != "-1")
                                    stateQuery += " State = " + lstStates2.SelectedValue;

                                stateQuery = ((stateQuery == "" || lstStates3.SelectedValue == "-1") ? stateQuery : stateQuery += " or ");

                                if (lstStates3.SelectedValue != "-1")
                                    stateQuery += " State = " + lstStates3.SelectedValue;

                                stateQuery = ((stateQuery == "") ? stateQuery : "( " + stateQuery + " )");

                                selQuery = ((stateQuery == "") ? selQuery : selQuery + " and " + stateQuery);
                            }

                            if (avalRoomIDs != "" && Available.Checked)
                                selQuery += " and ( RoomID in ( " + avalRoomIDs + " ) )";

                            if (PhotosOnly.Checked)
                                selQuery += " and  ( Imagetype <> '') "; //Image Project

                            //ZD 101918 start
                            if (txtLsearchtier1 != null && txtLsearchtier1.Value != "")
                            {
                                if (txtLsearchtier1.Value.Contains("'")) //ZD 104000 start
                                {
                                    Lsearchtier1 = txtLsearchtier1.Value.Replace("'", "''");

                                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                    selQuery += " Tier1Name Like  '%" + Lsearchtier1 + "%' ";
                                }
                                else
                                {
                                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                    selQuery += " Tier1Name Like  '%" + txtLsearchtier1.Value + "%' ";
                                }
                            }
                            if (txtLsearchtier2 != null && txtLsearchtier2.Value != "")
                            {
                                if (txtLsearchtier2.Value.Contains("'"))
                                {
                                    Lsearchtier2 = txtLsearchtier2.Value.Replace("'", "''");

                                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                    selQuery += " Tier2Name Like  '%" + Lsearchtier2 + "%' ";
                                }
                                else
                                {
                                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                    selQuery += " Tier2Name Like  '%" + txtLsearchtier2.Value + "%' ";
                                }
                            }
                            if (txtLsearchRoomname != null && txtLsearchRoomname.Value != "")
                            {
                                if (txtLsearchRoomname.Value.Contains("'"))
                                {
                                    LsearchRoomname = txtLsearchRoomname.Value.Replace("'", "''");

                                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                    selQuery += " RoomName Like  '%" + LsearchRoomname + "%' ";
                                }
                                else
                                {
                                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                    selQuery += " RoomName Like  '%" + txtLsearchRoomname.Value + "%' ";
                                }
                            }

                            //ZD 104000 End
                            if (txtLsearchMaxCapacity != null && txtLsearchMaxCapacity.Value != "")
                            {
                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                selQuery += " MaximumCapacity =" + txtLsearchMaxCapacity.Value;
                            }

                            if (txtDsearchRoomname != null && txtDsearchRoomname.Value != "")
                            {
                                if (txtDsearchRoomname.Value.Contains("'")) //ZD 104000 start
                                {
                                    DsearchRoomname = txtDsearchRoomname.Value.Replace("'", "''");

                                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                    selQuery += " RoomName Like  '%" + DsearchRoomname + "%' ";
                                }
                                else
                                {
                                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                    selQuery += " RoomName Like  '%" + txtDsearchRoomname.Value + "%' ";
                                }
                            }

                            if (txtDsearchtier1 != null && txtDsearchtier1.Value != "")
                            {
                                if (txtDsearchtier1.Value.Contains("'"))
                                {
                                    Dsearchtier1 = txtDsearchtier1.Value.Replace("'", "''");

                                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                    selQuery += " Tier1Name Like  '%" + Dsearchtier1 + "%' ";
                                }
                                else
                                {
                                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                    selQuery += " Tier1Name Like  '%" + txtDsearchtier1.Value + "%' ";
                                }
                            }
                            if (txtDsearchtier2 != null && txtDsearchtier2.Value != "")
                            {
                                if (txtDsearchtier2.Value.Contains("'"))
                                {
                                    Dsearchtier2 = txtDsearchtier2.Value.Replace("'", "''");

                                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                    selQuery += " Tier2Name Like  '%" + Dsearchtier2 + "%' ";
                                }
                                else
                                {
                                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                    selQuery += " Tier2Name Like  '%" + txtDsearchtier2.Value + "%' ";
                                }
                            }
                            //ZD 104000 End
                            if (txtDsearchMaxCapacity != null && txtDsearchMaxCapacity.Value != "")
                            {
                                selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                selQuery += " MaximumCapacity =" + txtDsearchMaxCapacity.Value;
                            }
                            //ZD 101918 End

                            if (hdnAV.Value == "1")
                            {
                                foreach (ListItem lstItem in AVlist.Items)
                                {
                                    //ZD 101344 - Start
                                    rmName = lstItem.Value.Trim();

                                    if (lstItem.Value.Trim().Split(' ').Length > 1)
                                        rmName = lstItem.Value.Trim().Split(' ')[0] + lstItem.Value.Trim().Split(' ')[1];
                                    //ZD 101344 - End

                                    if (lstItem.Selected)
                                    {
                                        if (lstItem.Text != "None")
                                        {
                                            if (avItemQueryAnd == "")
                                                avItemQueryAnd = " name = '" + lstItem.Text + "' ";
                                            else
                                                avItemQueryAnd += avItemQueryAndorOr + " name = '" + lstItem.Text + "' ";
                                        }
                                        else
                                            AVItemNone = "1";
                                    }
                                }
                            }

                            if (HandiCap.Checked)
                                selQuery += " and Handicappedaccess = '1' ";
                        }

                        #endregion

                        #region Memcache

                        DataTable dtTable = null;
                        using (memClient = new Enyim.Caching.MemcachedClient())// for meme cache demo
                        {
                            try
                            {

                                roomXML = myVRMNet.NETFunctions.DecompressString(memClient.Get<String>("myVRMRoomsDatatable"));                             

                                if (String.IsNullOrEmpty(roomXML))
                                {
                                    obj.GetAllRoomsInfo("", DrpActDct.SelectedValue, ref roomXML);
                                    if (String.IsNullOrEmpty(roomXML))
                                    {
                                        LblError.Text = obj.ShowSystemMessage();
                                        LblError.Visible = true;
                                        return;
                                    }
                                    else
                                    {
                                        memClient.Store(Enyim.Caching.Memcached.StoreMode.Set, "myVRMRoomsDatatable", myVRMNet.NETFunctions.CompressString(roomXML));
                                    }
                                    
                                }

                                if (!String.IsNullOrEmpty(roomXML))
                                {
                                    dtTable = new DataTable();
                                    ds = new DataSet();
                                    xmldoc = new XmlDocument();

                                    xmldoc.LoadXml(roomXML);

                                    txtrd = new XmlTextReader(xmldoc.OuterXml, XmlNodeType.Document, new XmlParserContext(null, null, null, XmlSpace.None));
                                    ds.ReadXml(txtrd, XmlReadMode.InferSchema);

                                    dv = new DataView(ds.Tables[0]);
                                    dv.Sort = "Tier1Name,Tier2Name,RoomName ASC";
                                    dtTable = dv.ToTable();
                                }

                            }
                            catch (Exception ey)
                            {
                                log.Trace("Error Message myVRMRoomsDatatable: " + ey.Message + " InnerException: " + ey.InnerException + "StackTrace: " + ey.StackTrace);
                            }
                        }
                        if (dtTable == null || dtTable.Rows.Count == 0)
                            dt = new DataTable();

                        lblTotalRecords.Text = "0";
                        lblTotalRecords1.Text = "0";
                        if (dtTable != null && dtTable.Rows.Count > 0)
                        {
                            //ZD 104482
                            //dtDisp = dtTable.Clone();
                            dtDisp = dtTable;

                            pathName = "../en/image/room/";

                            int displayrecordCount = 0;
                            int.TryParse(DrpRecordsperPage.SelectedValue, out displayrecordCount);

                            string deptIds = "";
                            string[] deptIdList = null;
                            string deptQuery = "";
                            if (Session["UserDeptList"] != null && Session["UserDeptList"].ToString() != "")
                            {
                                deptIds = Session["UserDeptList"].ToString();
                                deptIdList = deptIds.Split(',');
                            }

                            if (!(Session["admin"].ToString().Trim() == "2" || Session["admin"].ToString().Trim() == "3"))
                            {
                                deptQuery = "";
                                if (deptIdList != null && deptIdList.Count() > 0)
                                {
                                    for (int i = 0; i < deptIdList.Count(); i++)
                                    {
                                        if (deptQuery == "")
                                            deptQuery += "( RoomDeptId like '%," + deptIdList[i] + ",%'";
                                        else
                                            deptQuery += " or RoomDeptId like '%," + deptIdList[i] + ",%'";
                                    }
                                    deptQuery += ")";
                                }
                                else
                                    deptQuery += " RoomDeptId = '' ";
                                
                                selQuery += " and " + deptQuery;
                            }

                            string preferredRoomIds = "";
                             deptQuery = "";
                             if (Session["UserPreferedRoom"] != null && Session["UserPreferedRoom"].ToString() != "")
                             {
                                 preferredRoomIds = Session["UserPreferedRoom"].ToString();
                             }

                             if (favor == 1 )
                             {
                                 if (!string.IsNullOrEmpty(preferredRoomIds) && preferredRoomIds != "0")
                                 {
                                     selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                     selQuery += " RoomID  IN  (" + preferredRoomIds + ")";
                                 }
                                 else
                                 {
                                     selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                                     selQuery += " RoomID  IN  ('')";
                                 }

                             }

                            if (hdnAV.Value == "1")
                            {
                                string avItemQuery = "";
                                foreach (ListItem lstItem in AVlist.Items)
                                {
                                    if (lstItem.Selected)
                                    {
                                        if (lstItem.Text != "None")
                                        {
                                            if (avItemQuery == "")
                                                avItemQuery = " ( AVItemList like '%�" + lstItem.Text + "�%'";
                                            else
                                                avItemQuery += avItemQueryAndorOr + " AVItemList like '%�" + lstItem.Text + "�%' ";
                                        }
                                        else
                                        {
                                            if (avItemQuery == "")
                                                avItemQuery = "( AVItemList = '' ";
                                            else
                                                avItemQuery += " AVItemList = '' ";
                                        }
                                    }
                                }
                                if (avItemQuery != "")
                                    selQuery += " and " + avItemQuery + ") ";
                            }
                            log.Trace("selQuery:" + selQuery);
                            selQuery = selQuery.Replace("l.", "");//ZD 104482
                            DataRow[] totalRoomRows = dtTable.Select(selQuery);
                            DataRow[] RoomRows = totalRoomRows.Skip(displayrecordCount * pageIndex).Take(displayrecordCount).ToArray();
                            
                            int totalReords = totalRoomRows.Count();
                            lblTotalRecords.Text = totalReords.ToString();
                            lblTotalRecords1.Text = totalReords.ToString();

                            int ttlPages = (int)(totalReords / displayrecordCount);
                            if (totalReords % displayrecordCount > 0)
                                ttlPages++;
                            ASPxPager1.ItemCount = ttlPages;
                            ASPxPager2.ItemCount = ttlPages;

                            log.Trace("RoomRows:" + totalReords);

                            filePath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\";

                            string Videoaval = "", ApprovalReq = "", RoomImage = "", RoomImageID = "", rImage = "" ; // AttributeImage = "", Image1 = ""; //ZD 103569
                            //ZD 104482 -- Start - Commented following and handled in HTMLRowCreated
                            
                           
                            foreach (DataRow rw in RoomRows)
                            {
                                Videoaval = rw["Video"].ToString();

                                if (rw["MaximumCapacity"].ToString() == "")
                                    rw["MaximumCapacity"] = 0;
                                else
                                    rw["MaximumCapacity"] = int.Parse(rw["MaximumCapacity"].ToString());

                                if (Videoaval == "2") //Code changed for FB 1744  //ZD 101344 start
                                    rw["Video"] = obj.GetTranslatedText("Video");
                                else if (Videoaval == "1")
                                    rw["Video"] = obj.GetTranslatedText("Audio");
                                else
                                    rw["Video"] = obj.GetTranslatedText("None");//ZD 101344 End

                                //ZD 101344 start
                                ApprovalReq = rw["ApprovalReq"].ToString();
                                if (ApprovalReq == "Yes")
                                    rw["ApprovalReq"] = obj.GetTranslatedText("Yes");
                                else
                                    rw["ApprovalReq"] = obj.GetTranslatedText("No");
                                // ZD 101344 End

                                //Image Project starts ...

                                 RoomImage = rw["ImageName"].ToString();
                                 string ImageWebPath = rw["ImageWebPath"].ToString();//string RoomImageID = rw["ImageID"].ToString();
                                 rImage = "";

                                if (RoomImage == "[None]" || RoomImage == "")
                                {
                                    rImage = "../" + Session["language"].ToString() + "/image/noimage.gif"; //ZD 101344
                                }
                                else
                                {
                                    rImage = ImageWebPath + RoomImage; 
                                    //if (!File.Exists(filePath + RoomImage)) //Image Project
                                    //    File.Delete(filePath + RoomImage);  //ZD 101611

                                    //WriteToFile(rw["RoomID"].ToString(), filePath + RoomImage, RoomImageID); //ZD 101611

                                }
                                rw["ImageName"] = rImage;

                                //AttributeImage = ""; //ZD 103569 start

                                //if (rw.Table.Columns["AttributeImage"] != null)
                                //    AttributeImage = rw["AttributeImage"].ToString();

                                //Image1 = ""; 

                                //if (rw["RoomIconTypeId"].ToString() != "")
                                //{
                                //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.AudioOnly.ToString()) //RoomIconTypeId - Attribute Type ID for Room Types.
                                //        Image1 = "Audio.jpg";

                                //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.VideoOnly.ToString())
                                //        Image1 = "Video.jpg";

                                //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.RoomOnly.ToString())
                                //        Image1 = "Room.jpg";

                                //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.Telepresence.ToString())
                                //        Image1 = "Telepresence.jpg";

                                //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.HotdeskingAudio.ToString())
                                //        Image1 = "HotdeskingAudio.jpg";

                                //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.HotdeskingVideo.ToString())
                                //        Image1 = "HotdeskingVideo.jpg";

                                //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.GuestVideo.ToString())
                                //        Image1 = "GuestVideo.jpg";

                                //    if (AttributeImage != "")
                                //    {
                                //        imgArray = imageUtilObj.ConvertBase64ToByteArray(AttributeImage);
                                //        FileStream newFile = new FileStream(filePath + Image1, FileMode.Create);

                                //        newFile.Write(imgArray, 0, imgArray.Length);

                                //        newFile.Close();
                                //        newFile.Dispose();
                                //        newFile = null;
                                //        imgArray = null;
                                //        AttributeImage = null;
                                //    }
                                //} //ZD 103569 End

                                dtDisp.ImportRow(rw);
                            }
                           
                            dv = new DataView(dtDisp);

                            dv.Sort = "Tier1Name,Tier2Name,RoomName ASC";

                            if (Session["DtDisp"] != null)
                                Session["DtDisp"] = dv.ToTable();
                            else
                                Session.Add("DtDisp", dv.ToTable());

                            dt = new DataTable();
                            dt = (DataTable)Session["DtDisp"];

                            
                            System.Data.DataColumn ImagePath = new System.Data.DataColumn("ImgRoomIcon", typeof(System.String));
                            ImagePath.DefaultValue = "";
                            dt.Columns.Add(ImagePath);

                            System.Data.DataColumn ImageName1 = new System.Data.DataColumn("ImageName1", typeof(System.String));
                            ImageName1.DefaultValue = "";
                            dt.Columns.Add(ImageName1);

                            //ZD 104482 -- Start - Commented following and handled in HTMLRowCreated
                            /*
                            //ZD 103569 start 

                            DataRow[] Audio = dt.Select("RoomIconTypeId ='" + ns_MyVRMNet.vrmAttributeType.AudioOnly + "'");
                            if (Audio.Count() > 0)
                            {
                                for (int i = 0; i < Audio.Count(); i++)
                                {
                                    Audio[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.AudioOnly;
                                    Audio[i]["ImageName1"] = obj.GetTranslatedText("Audio");

                                }
                            }

                            DataRow[] Video = dt.Select("RoomIconTypeId = '" + ns_MyVRMNet.vrmAttributeType.Video + "'");
                            if (Video.Count() > 0)
                            {
                                for (int i = 0; i < Video.Count(); i++)
                                {
                                    Video[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.Video;
                                    Video[i]["ImageName1"] = obj.GetTranslatedText("Video");

                                }
                            }

                            DataRow[] Room = dt.Select("RoomIconTypeId ='" + ns_MyVRMNet.vrmAttributeType.RoomOnly + "'");
                            if (Room.Count() > 0)
                            {
                                for (int i = 0; i < Room.Count(); i++)
                                {
                                    Room[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.RoomOnly;
                                    Room[i]["ImageName1"] = obj.GetTranslatedText("Media Type is None");

                                }
                            }

                            DataRow[] Telepresence = dt.Select("RoomIconTypeId ='" + ns_MyVRMNet.vrmAttributeType.Telepresence + "'");
                            if (Telepresence.Count() > 0)
                            {
                                for (int i = 0; i < Telepresence.Count(); i++)
                                {
                                    Telepresence[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.Telepresence;
                                    Telepresence[i]["ImageName1"] = obj.GetTranslatedText("Telepresence");
                                }
                            }

                            DataRow[] HotdeskingAudio = dt.Select("RoomIconTypeId = '" + ns_MyVRMNet.vrmAttributeType.HotdeskingAudio + "'");
                            if (HotdeskingAudio.Count() > 0)
                            {
                                for (int i = 0; i < HotdeskingAudio.Count(); i++)
                                {
                                    HotdeskingAudio[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.HotdeskingAudio;
                                    HotdeskingAudio[i]["ImageName1"] = obj.GetTranslatedText("Hotdesking Audio");
                                }
                            }

                            DataRow[] HotdeskingVideo = dt.Select("RoomIconTypeId ='" + ns_MyVRMNet.vrmAttributeType.HotdeskingVideo + "'");
                            if (HotdeskingVideo.Count() > 0)
                            {
                                for (int i = 0; i < HotdeskingVideo.Count(); i++)
                                {
                                    HotdeskingVideo[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.HotdeskingVideo;
                                    HotdeskingVideo[i]["ImageName1"] = obj.GetTranslatedText("Hotdesking Video");
                                }
                            }

                            DataRow[] GuestVideo = dt.Select("RoomIconTypeId ='"+ ns_MyVRMNet.vrmAttributeType.GuestVideo +"'");
                            if (GuestVideo.Count() > 0)
                            {
                                for (int i = 0; i < GuestVideo.Count(); i++)
                                {
                                    GuestVideo[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.GuestVideo;
                                    GuestVideo[i]["ImageName1"] = obj.GetTranslatedText("GuestVideo");
                                }
                            }

                            //ZD 103569 End
                             
                            //ZD 104482 -- End
                            // ZD 102123 Starts
                            if (Parentframe == "RoomFloor")
                            {
                                if ((Session["FloorPlanRoomIds"] != null && Session["FloorPlanRoomIds"].ToString() != "") || (Session["floorPlanTiers"] != null && Session["floorPlanTiers"].ToString() != ""))
                                    fnFilterRooms(ref dt);
                            }
                            // ZD 102123 Ends
                            

                            grid.DataSource = dt;
                            grid.DataBind();

                            grid2.DataSource = dt;
                            grid2.DataBind();
                            changeView();

                        }
                        else
                        {
                            DataTable dtNone = new DataTable();
                            if (Session["DtDisp"] != null)
                                Session["DtDisp"] = dtNone;
                            else
                                Session.Add("DtDisp", dtNone);

                            grid.DataSource = dt;
                            grid.DataBind();

                            grid2.DataSource = dt;
                            grid2.DataBind();
                            changeView();
                        }

                        #endregion
                    }
                    else
                    {
                    */
                    #endregion

                    #region Selection Query

                    if (Av_and.Checked)
                        avItemQueryAndorOr = "and";
                    else
                        avItemQueryAndorOr = "or";

                    if (Request.QueryString["isTel"] != null)
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " isTelePresence = 0";

                    }

                    if (Available.Checked)
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " l.RoomID NOT IN  (" + GetAvailableRooms() + ")";
                    }
                    else if (ConfType == ns_MyVRMNet.vrmConfType.HotDesking && enableWaitList == 1 && isRecurConference == 0 && !Available.Checked)
                    {
                        busyRooms = GetAvailableRooms();
                    }


                    if (Session["DedicatedVideo"].ToString() != "1")
                    {
                        if (Request.QueryString["dedVid"] != null)
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " DedicatedVideo = 0";

                        }
                    }

                    if (favRooms == "0")
                        chkFavourites.Checked = false;

                    favor = 0;
                    if (chkFavourites.Checked)
                        favor = 1;

                    if (GuestRooms == "0")
                        chkGuestRooms.Checked = false;

                    if (chkGuestRooms.Checked)
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " Extroom = 1";

                        if (!(Session["admin"].ToString().Trim() == "2"))
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " adminId = " + Session["userID"].ToString();
                        }
                    }
                    else
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " Extroom = 0";
                    }

                    if (HotdeskingRooms == "0")
                        chkHotdesking.Checked = false;

                    if (ConfType == "8")
                    {
                        chkHotdesking.Checked = true;
                        chkHotdesking.Enabled = false;
                    }

                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                    if (chkHotdesking.Checked)
                    {
                        selQuery += " RoomCategory = 4";
                    }
                    else
                    {
                        selQuery += " RoomCategory NOT IN (4) ";
                    }


                    if (VMR == "0")
                        chkIsVMR.Checked = true;

                    selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                    if (chkIsVMR.Checked)
                    {
                        selQuery += " IsVMR = 1";

                        if (Cloud == 1 && CloudConferencing == 1)
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " RoomCategory = 5 and OwnerID = " + Session["userID"].ToString();
                        }
                    }
                    else
                    {
                        selQuery += " IsVMR = 0";
                    }

                    if (Cloud <= 0 && CloudConferencing <= 0)
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " RoomCategory not in (5) ";
                    }

                    string isPublicEP = "0";
                    if (Session["EnablePublicRooms"].ToString() == "1")
                        isPublicEP = "1";

                    if (isPublicEP == "0")
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " isPublic = 0";
                    }

                    if (txtTier1 != null && txtTier1.Text != "")
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " l.TopTier Like  '%" + txtTier1.Text + "%' ";
                    }
                    if (txtTier2 != null && txtTier2.Text != "")
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " l.MiddleTier Like  '%" + txtTier2.Text + "%' ";
                    }
                    if (txtRoomName != null && txtRoomName.Text != "")
                    {
                        selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                        selQuery += " l.Name Like  '%" + txtRoomName.Text + "%' ";
                    }


                    if (hdnName.Value == "1")
                    {
                        selQuery = ((selQuery == "") ? " l.Name like '%" + TxtNameSearch.Text + "%'" : selQuery += " and l.Name like '%" + TxtNameSearch.Text + "%'");

                    }
                    else
                    {
                        if (hdnCapacityL.Value != "" || hdnCapacityH.Value != "")
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " Capacity �= " + hdnCapacityL.Value;

                            if (hdnCapacityH.Value != "")
                                selQuery += " and Capacity �= " + hdnCapacityH.Value;
                        }


                        if (ConfType != "" && ConfType != "8" && chkHotdesking.Checked)
                        {
                            mediaQry = " VideoAvailable = 2 ";
                        }
                        else
                        {
                            if (MediaNone.Checked)
                                mediaQry += " VideoAvailable = 0 ";

                            mediaQry = ((mediaQry == "" || !MediaAudio.Checked) ? mediaQry : mediaQry += " or ");

                            if (MediaAudio.Checked)
                                mediaQry += " VideoAvailable = 1 ";

                            mediaQry = ((mediaQry == "" || !MediaVideo.Checked) ? mediaQry : mediaQry += " or ");

                            if (MediaVideo.Checked)
                                mediaQry += " VideoAvailable = 2 ";

                        }
                        if (mediaQry != "")
                        {
                            mediaQry = "( " + mediaQry + " )";

                            selQuery = ((selQuery == "") ? mediaQry : selQuery + " and " + mediaQry);
                        }

                        if (hdnZipCode.Value == "1")
                        {
                            selQuery += " and ZipCode Like  '%" + txtZipCode.Text + "%' ";

                        }
                        else
                        {

                            if (lstCountry.SelectedValue != "-1")
                                selQuery += " and Country = " + lstCountry.SelectedValue;

                            if (lstStates.SelectedValue != "-1")
                                stateQuery += " l.State = " + lstStates.SelectedValue;

                            stateQuery = ((stateQuery == "" || lstStates2.SelectedValue == "-1") ? stateQuery : stateQuery += " or ");

                            if (lstStates2.SelectedValue != "-1")
                                stateQuery += " l.State = " + lstStates2.SelectedValue;

                            stateQuery = ((stateQuery == "" || lstStates3.SelectedValue == "-1") ? stateQuery : stateQuery += " or ");

                            if (lstStates3.SelectedValue != "-1")
                                stateQuery += " l.State = " + lstStates3.SelectedValue;

                            stateQuery = ((stateQuery == "") ? stateQuery : "( " + stateQuery + " )");

                            selQuery = ((stateQuery == "") ? selQuery : selQuery + " and " + stateQuery);
                        }

                        if (PhotosOnly.Checked)
                            selQuery += " and  ( RoomImage �� '') ";


                        if (avalRoomIDs != "" && Available.Checked)
                            selQuery += " and ( l.RoomID in ( " + avalRoomIDs + " ) )";


                        if (txtLsearchtier1 != null && txtLsearchtier1.Value != "")
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " l.TopTier Like  '%" + txtLsearchtier1.Value + "%' ";
                        }
                        if (txtLsearchtier2 != null && txtLsearchtier2.Value != "")
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " l.MiddleTier Like  '%" + txtLsearchtier2.Value + "%' ";
                        }
                        if (txtLsearchRoomname != null && txtLsearchRoomname.Value != "")
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " l.Name Like  '%" + txtLsearchRoomname.Value + "%' ";
                        }
                        if (txtLsearchMaxCapacity != null && txtLsearchMaxCapacity.Value != "")
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " Capacity =" + txtLsearchMaxCapacity.Value;
                        }

                        if (txtDsearchRoomname != null && txtDsearchRoomname.Value != "")
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " l.Name Like  '%" + txtDsearchRoomname.Value + "%' ";
                        }

                        if (txtDsearchtier1 != null && txtDsearchtier1.Value != "")
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " l.TopTier Like  '%" + txtDsearchtier1.Value + "%' ";
                        }
                        if (txtDsearchtier2 != null && txtDsearchtier2.Value != "")
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " l.MiddleTier Like  '%" + txtDsearchtier2.Value + "%' ";
                        }

                        if (txtDsearchMaxCapacity != null && txtDsearchMaxCapacity.Value != "")
                        {
                            selQuery = ((selQuery == "") ? selQuery : selQuery += " and ");
                            selQuery += " Capacity =" + txtDsearchMaxCapacity.Value;
                        }

                        /* Commented for ZD 104482
                        if (hdnAV.Value == "1")
                        {
                            foreach (ListItem lstItem in AVlist.Items)
                            {
                                rmName = lstItem.Value.Trim();

                                if (lstItem.Value.Trim().Split(' ').Length > 1)
                                    rmName = lstItem.Value.Trim().Split(' ')[0] + lstItem.Value.Trim().Split(' ')[1];

                                if (lstItem.Selected)
                                {
                                    if (lstItem.Text != "None")
                                    {
                                        if (avItemQueryAnd == "")
                                            avItemQueryAnd = " name = '" + lstItem.Text + "' ";
                                        else
                                            avItemQueryAnd += avItemQueryAndorOr + " name = '" + lstItem.Text + "' ";
                                    }
                                    else
                                        AVItemNone = "1";
                                }
                            }
                        }
                        */
                        if (HandiCap.Checked)
                            selQuery += " and Handicappedaccess = 1 ";
                    }

                    #endregion

                    #region Without Memcache
                    xSettings = new XmlWriterSettings();
                    xSettings.OmitXmlDeclaration = true;
                    using (xWriter = XmlWriter.Create(RoomXML, xSettings))
                    {

                        xWriter.WriteStartElement("GetAllRoomsBasicInfo");
                        xWriter.WriteElementString("UserID", Session["userID"].ToString());
                        xWriter.WriteString(func.OrgXMLElement());
                        xWriter.WriteElementString("RoomID", "");
                        xWriter.WriteElementString("isMemCached", "0");
                        xWriter.WriteStartElement("SearchCriteria");
                        xWriter.WriteElementString("selQuery", selQuery);
                        xWriter.WriteElementString("Disabled", DrpActDct.SelectedValue);
                        xWriter.WriteElementString("Favorites", favor.ToString());//ZD 102481
                        xWriter.WriteElementString("Department", Department);
                        xWriter.WriteElementString("isEnableAVItem", hdnAV.Value);
                        xWriter.WriteElementString("ListAVItem", avItemQueryAnd);
                        xWriter.WriteElementString("AVItemNone", AVItemNone);
                        xWriter.WriteElementString("avItemQuery", avItemQueryAndorOr);
                        xWriter.WriteFullEndElement();

                        xWriter.WriteElementString("pageNo", pageIndex.ToString());
                        xWriter.WriteElementString("MaxRecords", DrpRecordsperPage.SelectedValue);
                        xWriter.WriteFullEndElement();
                        xWriter.Flush();
                    }

                    RoomXML = RoomXML.Replace("&lt;", "<")
                                   .Replace("&gt;", ">");

                    outXML = obj.CallMyVRMServer("GetAllRoomsInfoOpt", RoomXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());//ZD 104482
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        LblError.Text = obj.ShowErrorMessage(outXML);
                        LblError.Visible = true;
                        return;
                    }
                    else
                    {
                        xmldoc = new XmlDocument();
                        xmldoc.LoadXml(outXML);
                        nodes = xmldoc.SelectNodes("//Rooms/Room");
                        ds = new DataSet();

                        foreach (XmlNode node in nodes)
                        {
                            txtrd = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds.ReadXml(txtrd, XmlReadMode.InferSchema);
                        }

                        if (ds.Tables.Count > 0)
                        {
                            int pageNo = Convert.ToInt32(xmldoc.SelectSingleNode("//Rooms/pageNo").InnerText);
                            int totalPages = Convert.ToInt32(xmldoc.SelectSingleNode("//Rooms/totalPages").InnerText);
                            lblTotalRecords.Text = xmldoc.SelectSingleNode("//Rooms/TotalRecords").InnerText;
                            lblTotalRecords1.Text = xmldoc.SelectSingleNode("//Rooms/TotalRecords").InnerText;
                            ASPxPager1.ItemCount = totalPages;
                            ASPxPager2.ItemCount = totalPages;
                        }
                        else
                        {
                            lblTotalRecords.Text = "0";
                            lblTotalRecords1.Text = "0";
                            ASPxPager1.ItemCount = 0;
                            ASPxPager2.ItemCount = 0;
                        }
                    }

                    if (ds.Tables.Count == 0)
                        dt = new DataTable();

                    if (ds.Tables.Count > 0)
                    {
                        dtDisp = ds.Tables[0].Clone();
                        pathName = "../en/image/room/";

                        filePath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\";
                        //ZD 104482 -- Start - Commented following, Handled in HTMLRowCreated
                        dtDisp = ds.Tables[0];
                        /*
                        foreach (DataRow rw in ds.Tables[0].Rows)
                        {
                            string Videoaval = rw["Video"].ToString();

                            if (rw["MaximumCapacity"].ToString() == "")
                                rw["MaximumCapacity"] = 0;
                            else
                                rw["MaximumCapacity"] = int.Parse(rw["MaximumCapacity"].ToString());


                            if (Videoaval == "2") //Code changed for FB 1744  //ZD 101344 start
                                rw["Video"] = obj.GetTranslatedText("Video");
                            else if (Videoaval == "1")
                                rw["Video"] = obj.GetTranslatedText("Audio");
                            else
                                rw["Video"] = obj.GetTranslatedText("None");//ZD 101344 End

                            //ZD 101344 start
                            string ApprovalReq = rw["ApprovalReq"].ToString();
                            if (ApprovalReq == "Yes")
                                rw["ApprovalReq"] = obj.GetTranslatedText("Yes");
                            else
                                rw["ApprovalReq"] = obj.GetTranslatedText("No");
                            // ZD 101344 End

                            //Image Project starts ...

                            string RoomImage  = rw["ImageName"].ToString();
                            string ImageWebPath = rw["ImageWebPath"].ToString();//string RoomImageID = rw["ImageID"].ToString();
                            string rImage = "";

                            if (RoomImage == "[None]" || RoomImage == "")
                            {
                                rImage = "../" + Session["language"].ToString() + "/image/noimage.gif"; //ZD 101344
                            }
                            else
                            {
                                rImage = ImageWebPath + RoomImage; //ZD 103569 start
                                //rImage = pathName + RoomImage;
                                //if (!File.Exists(filePath + RoomImage)) //Image Project
                                //    File.Delete(filePath + RoomImage);  //ZD 101611

                                //WriteToFile(rw["RoomID"].ToString(), filePath + RoomImage, RoomImageID); //ZD 101611

                            }
                            rw["ImageName"] = rImage; //ZD 103569 End
                                
                            //string AttributeImage = "";

                            //if (rw.Table.Columns["AttributeImage"] != null)
                            //    AttributeImage = rw["AttributeImage"].ToString();

                            // string Image1 = "";

                            //if (rw["RoomIconTypeId"].ToString() != "")
                            //{
                            //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.AudioOnly.ToString()) //RoomIconTypeId - Attribute Type ID for Room Types.
                            //        Image1 = "Audio.jpg";

                            //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.VideoOnly.ToString())
                            //        Image1 = "Video.jpg";

                            //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.RoomOnly.ToString())
                            //        Image1 = "Room.jpg";

                            //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.Telepresence.ToString())
                            //        Image1 = "Telepresence.jpg";

                            //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.HotdeskingAudio.ToString())
                            //        Image1 = "HotdeskingAudio.jpg";

                            //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.HotdeskingVideo.ToString())
                            //        Image1 = "HotdeskingVideo.jpg";

                            //    if (rw["RoomIconTypeId"].ToString() == ns_MyVRMNet.vrmAttributeType.GuestVideo.ToString())
                            //        Image1 = "GuestVideo.jpg";

                            //    if (AttributeImage != "")
                            //    {
                            //        imgArray = imageUtilObj.ConvertBase64ToByteArray(AttributeImage);
                            //        FileStream newFile = new FileStream(filePath + Image1, FileMode.Create);

                            //        newFile.Write(imgArray, 0, imgArray.Length);

                            //        newFile.Close();
                            //        newFile.Dispose();
                            //        newFile = null;
                            //        imgArray = null;
                            //        AttributeImage = null;
                            //    }
                            //}

                            dtDisp.ImportRow(rw);
                        }
                        */
                        //ZD 104482 -- End

                        dv = new DataView(dtDisp);

                        dv.Sort = "Tier1Name,Tier2Name,RoomName ASC";

                        if (Session["DtDisp"] != null)
                            Session["DtDisp"] = dv.ToTable();
                        else
                            Session.Add("DtDisp", dv.ToTable());

                        dt = new DataTable();
                        dt = (DataTable)Session["DtDisp"];

                        System.Data.DataColumn ImagePath = new System.Data.DataColumn("ImgRoomIcon", typeof(System.String));
                        ImagePath.DefaultValue = "";
                        dt.Columns.Add(ImagePath);

                        System.Data.DataColumn ImageName1 = new System.Data.DataColumn("ImageName1", typeof(System.String));
                        ImageName1.DefaultValue = "";
                        dt.Columns.Add(ImageName1);

                        //ZD 104482 -- Start - Commented following, Handled in HTMLRowCreated
                        /*
                        //ZD 103569 start
                        DataRow[] Audio = dt.Select("RoomIconTypeId ='" + ns_MyVRMNet.vrmAttributeType.AudioOnly + "'");
                        if (Audio.Count() > 0)
                        {
                            for (int i = 0; i < Audio.Count(); i++)
                            {
                                Audio[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.AudioOnly;
                                Audio[i]["ImageName1"] = obj.GetTranslatedText("Audio");

                            }
                        }

                        DataRow[] Video = dt.Select("RoomIconTypeId ='" + ns_MyVRMNet.vrmAttributeType.Video + "'");
                        if (Video.Count() > 0)
                        {
                            for (int i = 0; i < Video.Count(); i++)
                            {
                                Video[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.Video;
                                Video[i]["ImageName1"] = obj.GetTranslatedText("Video");

                            }
                        }

                        DataRow[] Room = dt.Select("RoomIconTypeId= '" + ns_MyVRMNet.vrmAttributeType.RoomOnly + "'");
                        if (Room.Count() > 0)
                        {
                            for (int i = 0; i < Room.Count(); i++)
                            {
                                Room[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.RoomOnly;
                                Room[i]["ImageName1"] = obj.GetTranslatedText("Media Type is None");

                            }
                        }

                        DataRow[] Telepresence = dt.Select("RoomIconTypeId = '"+ ns_MyVRMNet.vrmAttributeType.Telepresence + "'");
                        if (Telepresence.Count() > 0)
                        {
                            for (int i = 0; i < Telepresence.Count(); i++)
                            {
                                Telepresence[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.Telepresence;
                                Telepresence[i]["ImageName1"] = obj.GetTranslatedText("Telepresence");
                            }
                        }

                        DataRow[] HotdeskingAudio = dt.Select("RoomIconTypeId = '" + ns_MyVRMNet.vrmAttributeType.HotdeskingAudio + "'");
                        if (HotdeskingAudio.Count() > 0)
                        {
                            for (int i = 0; i < HotdeskingAudio.Count(); i++)
                            {
                                HotdeskingAudio[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.HotdeskingAudio;
                                HotdeskingAudio[i]["ImageName1"] = obj.GetTranslatedText("Hotdesking Audio");
                            }
                        }

                        DataRow[] HotdeskingVideo = dt.Select("RoomIconTypeId ='" + ns_MyVRMNet.vrmAttributeType.HotdeskingVideo + "'");
                        if (HotdeskingVideo.Count() > 0)
                        {
                            for (int i = 0; i < HotdeskingVideo.Count(); i++)
                            {
                                HotdeskingVideo[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.HotdeskingVideo;
                                HotdeskingVideo[i]["ImageName1"] = obj.GetTranslatedText("Hotdesking Video");
                            }
                        }

                        DataRow[] GuestVideo = dt.Select("RoomIconTypeId ='" + ns_MyVRMNet.vrmAttributeType.GuestVideo + "'");
                        if (GuestVideo.Count() > 0)
                        {
                            for (int i = 0; i < GuestVideo.Count(); i++)
                            {
                                GuestVideo[i]["ImgRoomIcon"] = RoomiconPath + ns_MyVRMNet.vrmAttributeType.GuestVideo;
                                GuestVideo[i]["ImageName1"] = obj.GetTranslatedText("GuestVideo");
                            }
                        }
                        */
                        //ZD 103569 End
                        //ZD 104482 -- End

                        // ZD 102123 Starts
                        if (Parentframe == "RoomFloor")
                        {
                            if ((Session["FloorPlanRoomIds"] != null && Session["FloorPlanRoomIds"].ToString() != "") || (Session["floorPlanTiers"] != null && Session["floorPlanTiers"].ToString() != ""))
                                fnFilterRooms(ref dt);
                        }
                        // ZD 102123 Ends

                        grid.DataSource = dt;
                        grid.DataBind();

                        grid2.DataSource = dt;
                        grid2.DataBind();
                        changeView();

                    }
                    else
                    {
                        DataTable dtNone = new DataTable();
                        if (Session["DtDisp"] != null)
                            Session["DtDisp"] = dtNone;
                        else
                            Session.Add("DtDisp", dtNone);

                        grid.DataSource = dt;
                        grid.DataBind();

                        grid2.DataSource = dt;
                        grid2.DataBind();
                        changeView();

                    }
                    #endregion
                    //}//Commneted for ZD 104482
                    //ZD 103496 End
                }

                #endregion

                #region License update

                string outXMLlicen = obj.CallMyVRMServer("GetRoomLicenseDetails", inXMLDept, Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmld = new XmlDocument();
                xmld.LoadXml(outXMLlicen);

                XmlNode nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxNonvideoRemaining");

                if (nde != null)
                    nvidLbl.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVideoRemaining");

                if (nde != null)
                    vidLbl.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVMRRemaining");

                if (nde != null)
                    vmrvidLbl.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxNonvideo");

                if (nde != null)
                {
                    if (nde.InnerText != "" && nvidLbl.Text != "")
                    {
                        ttlnvidLbl.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(nvidLbl.Text)).ToString();
                    }

                }

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVideo");

                if (nde != null)
                {
                    if (nde.InnerText != "" && vidLbl.Text != "")
                    {
                        totalNumber.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(vidLbl.Text)).ToString();
                    }
                }

                //ZD 101098 START
                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxiControlRemaining");

                if (nde != null)
                    icontrolvidlbl.Text = nde.InnerText;
                //ZD 101098 END

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVMR");

                if (nde != null)
                {
                    if (nde.InnerText != "" && vmrvidLbl.Text != "")
                    {
                        tntvmrrooms.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(vmrvidLbl.Text)).ToString();
                    }
                }
                //ZD 101098 START- iControl Room Calculation
                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxiControl");

                if (nde != null)
                {
                    if (nde.InnerText != "" && icontrolvidlbl.Text != "")
                    {
                        tntiControlRooms.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(icontrolvidlbl.Text)).ToString();
                    }
                }
                //ZD 101098 END- iControl Room Calculation

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxPublicRoom");

                if (nde != null)
                {
                    if (nde.InnerText != "")
                        ttlPublicLbl.Text = nde.InnerText;
                }
                lblPublicRoom.Visible = false;
                ttlPublicLbl.Visible = false;
                if (Session["EnablePublicRooms"].ToString() == "1")
                {
                    lblPublicRoom.Visible = true;
                    ttlPublicLbl.Visible = true;
                }

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxROHotdeskingRemaining");
                if (nde != null)
                    ROHotdeskingRooms.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxROHotdesking");

                if (nde != null)
                {
                    if (nde.InnerText != "" && ROHotdeskingRooms.Text != "")
                    {
                        ttlROHotdeskingRooms.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(ROHotdeskingRooms.Text)).ToString();
                    }
                }
                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVCHotdeskingRemaining");
                if (nde != null)
                    VCHotdeskingRooms.Text = nde.InnerText;

                nde = xmld.SelectSingleNode("RoomLicenseDetails/MaxVCHotdesking");

                if (nde != null)
                {
                    if (nde.InnerText != "" && VCHotdeskingRooms.Text != "")
                    {
                        ttlVCHotdeskingRooms.Text = (Int32.Parse(nde.InnerText) - Int32.Parse(VCHotdeskingRooms.Text)).ToString();
                    }
                }

                nde = null;

                #endregion

            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
            finally
            {
                if (txtrd != null)
                {
                    txtrd.Close();
                }
                txtrd = null;
                func = null;

            }
        }
        #endregion


        //ZD 101611 start
        #region SetRoomImage
        /// <summary>
        /// SetRoomImage
        /// </summary>
        /// <param name="imageString"></param>
        /// <param name="fname"></param>
        /// <param name="ImageID"></param>
        protected void SetRoomImage(string imageString, string ImageID)
        {
            StringBuilder inXML = new StringBuilder();
            try
            {
                inXML.Append("<SetRoomImage>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<ImageID>" + ImageID + "</ImageID>");
                inXML.Append("<ActualImage>" + imageString + "</ActualImage>");
                inXML.Append("</SetRoomImage>");
                String outxml = obj.CallMyVRMServer("setRoomResizeImage", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetRoomProfile Outxml: " + outxml);
            }
            catch (Exception ex)
            {
                log.Trace("SetRoomImage: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 101611 End

        //ZD 101918 Start
        #region searchresult
        /// <summary>
        /// searchresult
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void searchresult(object sender, EventArgs e)
        {
            try
            {
                GetAllData(null, null);
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace(ex.StackTrace + " Room search error : " + ex.Message);
            }
        }
        #endregion
        //ZD 101918 End

        public string fnGetTranslatedText(string par)
        {
            return obj.GetTranslatedText(par);
        }

        // ZD 102123 Starts
        public void fnFilterRooms(ref DataTable dt)
        {
            string[] selTier = null;
            if (Session["SelectedTierName"] != null && Session["SelectedTierName"].ToString() != "")
            {
                selTier = Session["SelectedTierName"].ToString().Split(',');
            }

            string roomIds = "";
            if (Session["FloorPlanRoomIds"] != null && Session["FloorPlanRoomIds"].ToString() != "")
            {
                string qry = "";
                roomIds = Session["FloorPlanRoomIds"].ToString();
                DataView dv = new DataView(dt);
                qry = "RoomID not in (" + roomIds + ")";

                if (selTier != null)
                {
                    qry += " and Tier1ID = " + selTier[0] + "  and Tier2ID = " + selTier[1];
                }

                dv.RowFilter = qry;

                //dv.RowStateFilter = DataViewRowState.CurrentRows;
                //dv.RowFilter = "RoomID not in (15)";
                dt = dv.ToTable();
            }

            /*
            string roomId = "";
            int n = dt.Rows.Count;
            for (int i = 0; i < n; i++)
            {
                roomId = dt.Rows[i]["RoomID"].ToString();
                if (Session["FloorPlanRoomIds"].ToString().IndexOf(roomId) > -1)
                {
                    dt.Rows[i].Delete();
                    i--;
                    n--;
                }
            }
            */

            else
            {
                if (Session["floorPlanTiers"] != null)
                {
                    string tierInUse = Session["floorPlanTiers"].ToString();
                    string[] tierInUseList = tierInUse.Split(new string[] { "~~" }, StringSplitOptions.None);

                    string[] tierSubList = null;
                    int n2 = tierInUseList.Length;
                    string qStr = "";

                    for (int i = 0; i < n2; i++)
                    {
                        tierSubList = tierInUseList[i].Split(new string[] { "||" }, StringSplitOptions.None);


                        //if (dt.Rows[i]["Tier1ID"].ToString() == tierSubList[0] && dt.Rows[i]["Tier2ID"].ToString() == tierSubList[1])
                        //{

                        //    dt.Rows[i].Delete();
                        //    i--;
                        //    n2--;
                        //}


                        if (qStr != "")
                            qStr += " and ";
                        //qStr += "Tier1ID <> " + tierSubList[0] + " and Tier2ID <> " + tierSubList[1];
                        qStr += "Tier2ID <> " + tierSubList[1];

                    }


                    DataView dv = new DataView(dt);
                    dv.RowFilter = qStr;

                    //dv.RowStateFilter = DataViewRowState.CurrentRows;
                    //dv.RowFilter = "RoomID not in (15)";
                    dt = dv.ToTable();
                }
            }

        }
        // ZD 102123 Ends

        //ZD 102916 Start
        #region UpdatePartyList
        /// <summary>
        /// UpdatePartyList
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void UpdatePartyList(object sender, EventArgs e)
        {
            try
            {
                DataTable dt = new DataTable();   
                DataRow[] partyList = null;

                if (Session["PartyDatatable"] != null)
                {
                    dt = (DataTable)Session["PartyDatatable"];

                    if (dt.Rows.Count > 0)
                    {
                        if (hdnSelectionVal.Value == "1")
                        {
                            partyList = dt.Select("RoomID ='" + hdnSelRoomID.Value + "'");
                            if (partyList.Count() > 0)
                            {
                                for (int i = 0; i < partyList.Count(); i++)
                                {
                                    partyList[i]["RoomID"] = "";//Removed RoomID all existing Assigned Rooms
                                    partyList[i]["Assigned"] = "0";
                                }
                            }
                        }
                        else if (hdnSelectionVal.Value == "2")
                        {
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                dt.Rows[j]["RoomID"] = "";
                                dt.Rows[j]["Assigned"] = "0";
                            }
                        }
                    }
                    Session["PartyDatatable"] = dt;
                }                
            }
            catch (Exception ex)
            {
                LblError.Visible = true;
                LblError.Text = obj.ShowSystemMessage();
                log.Trace(ex.StackTrace + " UpdatePartyList : " + ex.Message);
            }
        }
        #endregion
        //ZD 102916 End


    }
}
