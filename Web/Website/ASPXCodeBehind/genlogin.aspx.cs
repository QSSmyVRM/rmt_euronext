﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using System.IO;
using System.Management;
using System.Management.Instrumentation;
using System.Threading;//ZD 100152
using Enyim;//ZD 103496

namespace ns_Login
{
    public partial class genlogin : System.Web.UI.Page
    {
        /* Login management - Codes changed for FB 1820 */
        /*SetApplicationVariables - method removed as it is not used anywhere */

        #region Private Data Members
        /// <summary>
        /// Declaring Data Members
        /// </summary>
        const String loginPage = "../en/genlogin.aspx";
        const String loginPageTitle = "VRM - Video Conferencing Made Simple !";
        const String company_logo = "../image/company-logo/SiteLogo.jpg"; //FB Icon
        const String mainTopImg = "../en/image/vrmtopleft.GIF";
        const String bgColor = "#FFEBCE";
        const String popUpbgColor = "#FFEBCE";

        protected Boolean wizard_enable = false;
        protected String ind = "";
        protected String WHO_USE = "";
        protected Boolean blockNSFF = false;
        protected Boolean corBroswer = true;
        //protected System.Web.UI.HtmlControls.HtmlGenericControl CompanyTagLine; //FB 2719
        protected System.Web.UI.HtmlControls.HtmlInputText UserName;
        protected System.Web.UI.HtmlControls.HtmlInputPassword UserPassword;
        protected System.Web.UI.HtmlControls.HtmlTableCell lblViewPublicConf;//FB 2858
        protected System.Web.UI.HtmlControls.HtmlTableCell lblForgotPassword;
        //protected System.Web.UI.HtmlControls.HtmlInputHidden hdnscreenres; //ZD 100157 //ZD 100335
       
        MyVRMNet.LoginManagement loginMgmt = null;
        myVRMNet.NETFunctions obj = null;
        ns_Logger.Logger log = null;
        protected System.Web.UI.WebControls.Label errLabel;
        int versionNo = 0;
        public String companyInfo = "";
        String language = "en"; //FB 1830

        protected System.Web.UI.WebControls.CheckBox RememberMe;//FB 2009
        bool isloginFailed = false; //FB 2027 GetHome
        public string exist = ""; // FB 2779 // ZD 103581
        string macID = ""; //ZD 100263
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnURL;  //ZD 101477
        protected System.Web.UI.HtmlControls.HtmlTableRow trReqUsrAcc;//ZD 101846
        //Enyim.Caching.MemcachedClient memClient = null;//ZD 103496 ZD 104482
        //protected int memcacheEnabled = 0;//ZD 103496 ZD 104482
        #endregion

        public string mailextn = "";//FB 1943

        # region Constructor
        /// <summary>
        /// Public Constructor 
        /// </summary>
        public genlogin()
        {
            obj = new myVRMNet.NETFunctions();
            loginMgmt = new MyVRMNet.LoginManagement();
            log = new ns_Logger.Logger();

        }
        #endregion

        //ZD 103174 satrt
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            //ZD 103174 - Start
            if (Request.QueryString["lang"] == null)
            {
                Session["curReq"] = Request.Url.AbsoluteUri;
                Response.Redirect("~/default.aspx");
            }
            //ZD 103174 - End
            if (Request.QueryString["lang"] != null)
            {
                string browserlang = Request.QueryString["lang"];
                Session["browserlang"] = Request.QueryString["lang"];
                if (browserlang.Contains("en-US") || browserlang.Contains("en"))
                {
                    UICulture = "en";
                    Culture = "en";
                }
                if (browserlang.Contains("fr-CA") || browserlang.Contains("fr"))
                {
                    UICulture = "fr-CA";
                    Culture = "fr-CA";
                }
                if (browserlang.Contains("es") || browserlang.Contains("es-US"))
                {
                    UICulture = "es-US";
                    Culture = "es-US";
                }
            }
            else
            {
                UICulture = "en";
                Culture = "en";
            }
            base.InitializeCulture();
        }
        #endregion
        //ZD 103174 End
        //protected override void InitializeCulture()
        //{
        //        UICulture = "en-US";
        //        Culture = "en-US";
        //        base.InitializeCulture();
        //}
        #region Page Load Event handler
        /// <summary>
        /// Page Load Event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Session.Clear();// ZD 100263
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.URLConformityCheckNoSession(Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                //FB 1633 start
                loginMgmt.SetSitePaths(); 
                loginMgmt.SetSiteThemes();
                //FB 1633 end
                BindProcessorDetails();//ZD 100263
                mailextn = loginMgmt.mailextn;//FB 1943
                //ZD 100335 start
                //Session["hdnscreenresoul"] = 1024;
                //if (hdnscreenres != null)//ZD 100157
                //    Session["hdnscreenresoul"] = hdnscreenres.Value;
                if (Request.Cookies["hdnscreenres"] != null)
                    Session["hdnscreenresoul"] = Request.Cookies["hdnscreenres"].Value;
                //ZD 100335 End
                if (Session["ViewPublicConfs"] != null) //FB 2858
                {
                    if (Session["ViewPublicConfs"].ToString() == "2")
                    {
                        lblViewPublicConf.Visible = false;
                        lblForgotPassword.Attributes.Add("style", "text-align:center");//ZD 102227                        
                    }
                }

                //ZD 101846 - STart
                if (Session["ReqUsrAcc"] != null)
                {
                    if (Session["ReqUsrAcc"].ToString() == "0")
                    {
                        trReqUsrAcc.Visible = false;                        
                    }
                }
                //ZD 101846 - End
                // FB 2779 Starts // ZD 103581 - Start
                if(File.Exists(Server.MapPath("..\\image\\company-logo\\LoginBackground.jpg")))
                {
                    exist = "wide";
                }
                if (File.Exists(Server.MapPath("..\\image\\company-logo\\LoginBackgroundNormal.jpg")))
                {
                    exist += "normal";
                }
                // FB 2779 Ends // ZD 103581 - End
                // FB 2779 Ends
                /* commented for ZD 104482
				//ZD 103496 starts
                if (Session["MemcacheEnabled"] != null && Session["MemcacheEnabled"].ToString() != "")
                    int.TryParse(Session["memcacheEnabled"].ToString(), out memcacheEnabled);
                 */
				//ZD 103496 End
                if (!IsPostBack)
                {
                    /* commented for ZD 104482
                    //ZD 103496
                    if (memcacheEnabled == 1)
                        SetCache();
                    */
                    //ZD 103174 - Start
                    string loginbrowserlang = "";
                    if(Session["loginbrowserlang"] != null)
                        loginbrowserlang = Session["loginbrowserlang"].ToString();
                    //ZD 103174 - End
                    errLabel.Text = "";
                    if (Request.QueryString["m"] == "1")
                    {
                        string themeVal = Session["ThemeType"].ToString(); // FB 2719
                        Session.Abandon();
                        Session.RemoveAll();
                        Session["ThemeType"] = themeVal; // FB 2719
                        //errLabel.Text = "You have successfully logged out of myVRM.";//ZD 103174
                        errLabel.Visible = true;
                    }
                    //ZD 100386 - Starts
                    if (Request.QueryString["m"] == "2")
                    {
                        string themeVal = Session["ThemeType"].ToString(); // FB 2719
                        Session.Abandon();
                        Session.RemoveAll();
                        Session["ThemeType"] = themeVal; // FB 2719
                       // errLabel.Text = "Your session expired. Please sign in to continue...";//ZD 103174
                        errLabel.Visible = true;
                    }
                    //ZD 100386 - End //ZD 103174 satrt
                    if (loginbrowserlang != "")
                        Session["loginbrowserlang"] = loginbrowserlang;

					//ZD 103531 - Start
                    string browserlangid = "0";
                    String inxml = "";
                    String outXML = "";

                    if (Request.QueryString["lang"] != null)
                    {
                        string browserlang = Request.QueryString["lang"];
                        Session["browserlang"] = Request.QueryString["lang"];
                        if (browserlang.Contains("en-US") || browserlang.Contains("en"))
                        {
                            HttpContext.Current.Session.Add("languageID", "1");
                            browserlangid = "1";
                        }
                        if (browserlang.Contains("fr-CA") || browserlang.Contains("fr"))
                        {
                            HttpContext.Current.Session.Add("languageID", "5");
                            browserlangid = "5";
                        }
                        if (browserlang.Contains("es") || browserlang.Contains("es-US"))
                        {
                            HttpContext.Current.Session.Add("languageID", "3");
                            browserlangid = "3";
                        }
                    }

                    inxml = "<login><browserlangid>" + browserlangid + "</browserlangid></login>";
                    outXML = obj.CallCommand("GetLanguageTexts", inxml);

                    if (HttpContext.Current.Session["TranslationText"] == null)
                        HttpContext.Current.Session.Add("TranslationText", outXML);
                    else
                        HttpContext.Current.Session["TranslationText"] = outXML;

                    if (browserlangid == "5")
                    {
                        if (Cache["FrenchTransText"] == null)
                            Cache["FrenchTransText"] = Session["TranslationText"].ToString();
                    }
                    else if (browserlangid == "3")
                    {
                        if (Cache["SpanishTransText"] == null)
                            Cache["SpanishTransText"] = Session["TranslationText"].ToString();
                    }
                    else
                    {
                        if (Cache["EnglishTransText"] == null)
                            Cache["EnglishTransText"] = Session["TranslationText"].ToString();
                    }


                    if (Request.QueryString["m"] == "1")
                        errLabel.Text = obj.GetTranslatedText("You have successfully logged out of RMT.");
                    else if (Request.QueryString["m"] == "2")
                        errLabel.Text = obj.GetTranslatedText("Your session expired. Please sign in to continue...");                    
                
					//ZD 103174 End //ZD 103531 - End

                    if (loginMgmt.errMessage.IndexOf("<error>") >= 0)
                    {
                        errLabel.Text = obj.ShowErrorMessage(loginMgmt.errMessage);
                        errLabel.Visible = true;
                    }
                    //CompanyTagLine.InnerText = loginMgmt.companyTagline; //FB 2719
                }

                loginMgmt.HeaderASP();
                loginMgmt.readaspconfigASP();
                //loginMgmt.cookiedectASP();  Remember me
                

                if (File.Exists(Application["SchemaPath"].ToString() + "\\company.ifo"))
                {
                    companyInfo = File.ReadAllText(Application["SchemaPath"].ToString() + "\\company.ifo");
                }
                if (Session["versionNo"] != null)
                {
                    Int32.TryParse(Session["versionNo"].ToString().Trim(), out versionNo);
                }

                if (Session["browser"] != null)
                {
                    if (Session["browser"].ToString().Trim().ToLower() == "internet explorer")
                    {
                        if (versionNo > 5.0)
                        {
                            corBroswer = true;
                        }
                    }

                    if (Session["browser"].ToString().Trim().ToLower() == "netscape")
                    {
                        if (versionNo > 5)
                        {
                            if (blockNSFF)
                                corBroswer = false;
                            else
                                corBroswer = true;
                        }
                    }

                    if (Session["browser"].ToString().Trim().ToLower() == "firefox")
                    {
                        if (versionNo >= 1)
                        {
                            if (blockNSFF)
                                corBroswer = false;
                            else
                                corBroswer = true;
                        }
                    }

                    if (Session["browser"].ToString().Trim().ToLower() == "opera")
                    {
                        corBroswer = true;
                    }
                }

                if (Session["wizard_enable"] != null)
                    if (Session["wizard_enable"].ToString().Equals("1"))
                        wizard_enable = true;

                if (Session["WHO_USE"] != null)
                    WHO_USE = Session["WHO_USE"].ToString();


                //ZD 100152 Starts
                if (!string.IsNullOrEmpty(Request.QueryString["G"]))
                {
                    if(Request.QueryString["G"].ToString()=="f")
                        errLabel.Text = obj.GetTranslatedText("Invalid user Authentication");
                    else
                        errLabel.Text = obj.GetTranslatedText("Google Authentication Failed");
                }
                //ZD 100152 Ends
                //FB 1628 START
                if (Request.QueryString["id"] != null && Request.QueryString["req"] != null && Request.QueryString["tp"] != null)
                {
                    if (Request.QueryString["id"].ToString() != "" && Request.QueryString["req"].ToString() != "" && Request.QueryString["tp"].ToString() != "")
                    {
                        try
                        {
                            String idStr = Request.QueryString["id"].ToString();
                            String reqStr = Request.QueryString["req"].ToString();
                            String tpStr = Request.QueryString["tp"].ToString();
                            //ZD 100663
                            String rmStr = "";
                            if(Request.QueryString["r"] != null)
                                rmStr = Request.QueryString["r"].ToString();
                            String rmID = "";
                            if (Request.QueryString["rID"] != null)
                                rmID = Request.QueryString["rID"].ToString();

                            if (Session["req"] == null && Session["id"] == null)
                            {
                                Session.Add("req", Request.QueryString["req"]);
                                Session.Add("id", Request.QueryString["id"]);
                            }
                            else
                            {
                                Session["req"] = Request.QueryString["req"].ToString();
                                Session["id"] = Request.QueryString["id"].ToString();
                            }

                            Session["userID"] = "11";//Hard Corded to fetch the System Time Zone
                            Session["organizationID"] = "11";//default organization ID

                            //FB 2616 Start
                            if (Request.QueryString["tp"].ToString() == "M")
                                Response.Redirect("ManageConference.aspx?t=&confid=" + idStr + "&req=" + reqStr + "&tp=" + tpStr + "");

                            else if (Request.QueryString["tp"].ToString() == "CM")
                                Response.Redirect("MonitorMCU.aspx?id=" + idStr + "&req=" + reqStr + "&tp=" + tpStr + "");

                            else if (Request.QueryString["tp"].ToString() == "P2P")
                                Response.Redirect("point2point.aspx?id=" + idStr + "&req=" + reqStr + "&tp=" + tpStr + "");
                            else if (Request.QueryString["tp"].ToString() == "CR") //ZD 100663
                            {
                                if (Request.QueryString["rl"] != null && Request.QueryString["rl"].ToString() == "0")
                                    Response.Redirect("ExpressConference.aspx?id=" + idStr + "&req=" + reqStr + "&t=&tp=" + tpStr);
                                else
                                    Response.Redirect("ConferenceSetup.aspx?id=" + idStr + "&req=" + reqStr + "&t=&tp=" + tpStr);
                            }
                            else if (Request.QueryString["tp"].ToString() == "DR" && Request.QueryString["cu"] != null && Request.QueryString["cu"].ToString() == "1") //ZD 100718
                            {
                                Response.Redirect("ConferenceChangeRequest.aspx?id=" + idStr + "&req=" + reqStr + "&tp=" + tpStr
                                 + "&t=hf&r=" + rmStr + "&rID=" + rmID + "&cu=1");
                            }
                            else if (Request.QueryString["tp"].ToString() == "DR")
                                Response.Redirect("ConferenceChangeRequest.aspx?id=" + idStr + "&req=" + reqStr + "&tp=" + tpStr
                                    + "&t=hf&r=" + rmStr + "&rID=" + rmID);
                            else
                                Response.Redirect("ResponseConference.aspx?id=" + idStr + "&req=" + reqStr + "&tp=" + tpStr + "&t=hf");
                            //FB 2616 End
                        }
                        catch (System.Threading.ThreadAbortException) { }

                    }
                }

                //FB 1628 END

                RemembermeLogin();

                GetQueryStrings();
            }
            catch (ThreadAbortException) //ZD 100152
            {
                // User was not yet authenticated and is being forwarded to the authorization page.
               
            }
            catch (Exception ex)
            {
                log.Trace("Page Load: "+ ex.Message);
            }
        }

        #endregion

        #region BrowswerDetect
        /// <summary>
        /// BrowswerDetect
        /// </summary>
        private void BrowserDetect()
        {
            try
            {
				Session["macid"] = macID; //ZD 100263
                Session["ip"] = Request.ServerVariables["REMOTE_ADDR"];
                Session["referrer"] = Request.ServerVariables["HTTP_REFERER"];
                if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("WINDOWS"))
                    Session["os"] = "Windows";
                else if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MAC"))
                    Session["os"] = "Mac";
                else if (Request.ServerVariables["HTTP_USER_AGENT"].ToUpper().ToUpper().Contains("LINUX"))
                    Session["os"] = "Linux";
                else
                    Session["os"] = "Other";

                if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MOZILLA"))
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("MSIE"))
                    {
                        Session["browser"] = "Internet Explorer";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("NETSCAPE"))
                    {
                        Session["browser"] = "Netscape";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }

                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("FIREFOX"))
                    {
                        Session["browser"] = "Firefox";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("SAFARI"))
                    {
                        Session["browser"] = "Firefox";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("CHROME"))
                    {
                        Session["browser"] = "Firefox";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    if (Session["browser"] == null)
                        Session["browser"] = "Undetectable";
                    if (Session["versionNo"] == null)
                        Session["versionNo"] = "Undetectable";
                    if (Session["version"] == null)
                        Session["version"] = "Undetectable";
                }
                else
                {
                    if (Request.ServerVariables["HTTP_USER_AGENT"].ToString().ToUpper().Contains("OPERA"))
                    {
                        Session["browser"] = "Opera";
                        Session["versionNo"] = Request.Browser.MajorVersion;
                        Session["version"] = Request.Browser.Version;
                    }
                    else
                    {
                        Session["browser"] = "Not Mozilla";
                        Session["version"] = "Undetectable";
                    }
                }
            }
            catch (Exception ex)
            {
                //log.trace
                log.Trace(ex.StackTrace + ex.Message);

            }
        }
        #endregion
        
        #region GetQueryStrings
        /// <summary>
        /// GetQueryStrings
        /// </summary>
        private void GetQueryStrings()
        {
            Int32 domain = 0;
            String user = "";
            String strDomain = "";//101812
            try
            {
                Session.Add("c", "0");
                //Session.Add("guestlogin", ""); - This variable is not used in the system
                
                if (Request.QueryString["sct"] != null)
                    if (Request.QueryString["sct"] != "")
                    {
                        Session["c"] = Request.QueryString["sct"];
                    }
                ind = Session["c"].ToString();

                if (Application["CosignEnable"].ToString() == "1") //Check for cosign environment
                {
                    if (Request.ServerVariables["HTTP_REMOTE_USER"] != null) //FB 1820
                    {
                        loginMgmt.queyStraVal = Request.ServerVariables["HTTP_REMOTE_USER"].ToString();
                        
                        log.Trace("HTTP_REMOTE_USER: " + loginMgmt.queyStraVal);

                        loginMgmt.qStrPVal = "pwd";
                        loginMgmt.windwsAuth = true;
                        login();
                    }
                }
                else if (Application["ssoMode"] != null && Application["ssoMode"].ToString().ToUpper() == "YES")    
                {
                    if (Request.ServerVariables["AUTH_USER"] != null)
                    {
                        log.Trace("Complete AUTH_USER: " + Request.ServerVariables["AUTH_USER"].ToString()); //FB 1820
                        domain = (Request.ServerVariables["AUTH_USER"]).ToLower().IndexOf("\\", 0) + 1;
                        user = ((Request.ServerVariables["AUTH_USER"]).ToLower()).Substring(domain);
                        strDomain = ((Request.ServerVariables["AUTH_USER"]).ToLower()).Substring(0, (Request.ServerVariables["AUTH_USER"]).ToLower().IndexOf("\\", 0));// 101812
                        log.Trace("AUTH_domain: " + strDomain); //FB 1820
                        log.Trace("AUTH_USER: "+user); //FB 1820
                        loginMgmt.queyStraVal = user;
                        loginMgmt.qStrPVal = "pwd";
                        loginMgmt.qStrIpVal = macID; //ZD 100263
                        loginMgmt.windwsAuth = true;
                        loginMgmt.qStrDomainVal = strDomain;//101812
                        login();
                    }
                }
                else
                {  
                    String orgId = "";
                    if (Request.QueryString["OrgID"] != null)
                    {
                        if (Request.QueryString["OrgID"].ToString() != "")
                        {
                            orgId = Request.QueryString["OrgID"].ToString();
                        }
                    }
                    //RSS Fix -- End

                    //FB 1830
                    if (Session["language"] != null)
                    {
                        if (Session["language"].ToString() != "")
                            language = Session["language"].ToString();
                    }

                    if (Request.QueryString["rss"] != null)//Rss Feed
                    {
                        if (Request.QueryString["rss"] != "")
                        {
                            if (Session["userID"].ToString() != "")
                            {
                                Response.Redirect("../en/ManageConference.aspx?t=&confid=" + Request.QueryString["rss"] + "&OrgID=" + orgId); //ZD 101022
                                //Response.Redirect("../" + language + "/ManageConference.aspx?t=&confid=" + Request.QueryString["rss"] + "&OrgID=" + orgId);
                            }
                        }
                    }
                }

            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace("GetQueryStrings: " + ex.Message); //FB 1820
            }
        }

        #endregion

        #region Submit Event Handlers

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            try
            {
                /*
                if (UserName.Value.ToString().Trim().IndexOf("@") <= 0)
                {
                    errLabel.Text = "Please enter Email"; // FB 2310
                    return;
                }
                */
                if (isloginFailed)//FB 2027 GetHome
                    return;

                loginMgmt = new MyVRMNet.LoginManagement();
                loginMgmt.queyStraVal = UserName.Value.ToString().Trim();

                Session["LoginUserName"] = UserName.Value.ToString().Trim(); // FB 1725
                loginMgmt.qStrPVal = UserPassword.Value.ToString().Trim();
                loginMgmt.windwsAuth = false;
                loginMgmt.qStrIpVal = macID; //ZD 100263
                login();
            }
            catch (System.Threading.ThreadAbortException)
            {
            }
            catch (Exception ex)
            {
                log.Trace("btnSubmit_Click: "+ ex.Message);
            }
        }

        #endregion

        protected void checkSession()
        {

        }

        #region Login
        //MEthod modified for FB 1820
        private void login()
        {
            string outXML = "";
            XmlDocument errDoc = null; //ZD 100152
            errLabel.Text = ""; // ZD 103954
            errLabel.Visible = false; // ZD 103954
            int errno = 0, enablePassRule = 0;// ZD 103954
            string browserlang = Request.QueryString["lang"];
            try
            {
                outXML = loginMgmt.GetHomeCommand();
                if (outXML != "")
                {
                    if (outXML.IndexOf("<error>") >= 0)
                    {
                        // ZD 103954 Start
                        Session.Remove("EnablePasswordRule");
                        errDoc = new XmlDocument();
                        errDoc.LoadXml(outXML);
                        if (errDoc.SelectSingleNode("error/message") != null)
                        {
                            errLabel.Text = errDoc.SelectSingleNode("error/message").InnerText.Trim();
                            errLabel.Visible = true;
                            isloginFailed = true;
                        }
                        XmlNode errcode = errDoc.SelectSingleNode("error/errorCode");
                        int.TryParse(errcode.InnerXml.Trim(), out errno);
                        if (errno == 725)
                        {
                            errcode = errDoc.SelectSingleNode("error/passwordrule");
                            if (errcode != null)
                                int.TryParse(errcode.InnerXml.Trim(), out enablePassRule);
                            Session.Add("EnablePasswordRule", enablePassRule);
                            Response.Redirect("PasswordReset.aspx?lang=" + browserlang);
                        }
                        return;
                    }
                }
				//FB 1830 // ZD 103954 End
                if(Session["language"] != null)
                {
                    if (Session["language"].ToString() != "")
                        language = Session["language"].ToString();
                }

                language = "en"; //ZD 101022

                //RSS Fix-Single Sign On -- Start
                String orgID = "";
                if (Request.QueryString["OrgID"] != null)
                {
                    if (Request.QueryString["OrgID"].ToString() != "")
                    {
                        orgID = Request.QueryString["OrgID"].ToString();
                    }
                }
                //RSS Fix -- End
                //ZD 101477 Starts
                Session["RequrstURL"] = "";
                if (hdnURL != null && hdnURL.Value != "")
                    Session["RequrstURL"] = hdnURL.Value;
                //ZD 101477 Ends
                if (Request.QueryString["rss"] != null)//Rss Feed
                {
                    if (Request.QueryString["rss"] != "")
                    {
                        if (Session["userID"].ToString() != "")
                        {
                            Response.Redirect("ManageConference.aspx?t=&confid=" + Request.QueryString["rss"] + "&OrgID=" + orgID);
                        }
                    }
                }
                //Single Sign On -- End

                if (Session["NavLobby"] == null) // To avoid the alert message on refreshing lobby page
                    Session["NavLobby"] = "1";

                // //FB 2009
                if (Session["outXML"] != null) //ZD 100263
                    outXML = Session["outXML"].ToString();

                if (RememberMe.Checked)
                {
                    XmlDocument xdoc = new XmlDocument();
                    xdoc.LoadXml(outXML);
                    if (xdoc.SelectNodes("enEmailID") != null)
                        Response.Cookies["VRMuser"]["act"] = xdoc.SelectSingleNode("user/globalInfo/enEmailID").InnerText.Trim();
                    if (xdoc.SelectNodes("enUserPWD") != null)
                        Response.Cookies["VRMuser"]["pwd"] = xdoc.SelectSingleNode("user/globalInfo/enUserPWD").InnerText.Trim();
                    if (xdoc.SelectNodes("enIPAddress") != null)
                        Response.Cookies["VRMuser"]["macid"] = xdoc.SelectSingleNode("user/globalInfo/enMacID").InnerText.Trim();
                    Response.Cookies["VRMuser"].Expires = DateTime.Now.AddDays(365);
                    Response.Cookies["VRMuser"].HttpOnly = true; // ZD 100263
                }
                //FB 2009

                //ZD 100152 Starts
                outXML = HttpContext.Current.Session["outXML"].ToString();
                errDoc = new XmlDocument();

                errDoc.LoadXml(outXML);
                int PushtoGoogle = 0;
                XmlNode node = errDoc.SelectSingleNode("user/GoogleInfo/PushtoGoogle");
                if (node != null)
                    int.TryParse(errDoc.SelectSingleNode("user/GoogleInfo/PushtoGoogle").InnerXml, out PushtoGoogle);
                
                //FB 1779 start
                if (Session["LandingPageLink"] != null && Session["LandingPageLink"].ToString() != "") // ZD 100172
                {
                    if (Session["UserID"].ToString().Equals("11"))
                        Session["adminAlert"] = "1";
                }

                if (PushtoGoogle == 1)
                    Response.Redirect("~/en/GoogleAuth.aspx"); //ZD 101022
                //Response.Redirect("~/" + language + "/GoogleAuth.aspx");

                if (Session["isExpressUser"] != null)
                {
                    if (Session["isExpressUser"].ToString() == "1")
                    {
                        if (Session["LandingPageLink"] != null && Session["LandingPageLink"].ToString() != "" && obj.checkURLSessionStatus(Session["LandingPageLink"].ToString())) // ZD 100172
                        {
                            Response.Redirect("~/en/" + Session["LandingPageLink"].ToString()); //ZD 101022
                        }
                        else
                            Response.Redirect("~/en/ExpressConference.aspx?t=n"); //FB 1830//ZD 101022
                    }
                }
                //FB 1779 end
                
				//FB 1830

               
                 
               
				if (Session["LandingPageLink"] != null && Session["LandingPageLink"].ToString() != "" && obj.checkURLSessionStatus(Session["LandingPageLink"].ToString())) // ZD 100172
                    Response.Redirect("~/en/" + Session["LandingPageLink"].ToString());//ZD 101022
                else
                    Response.Redirect("~/en/SettingSelect2.aspx?c=GH"); //ZD 101022
                //ZD 100152 Ends
                //Response.Redirect("~/" + language + "/lobby.aspx"); //LOBBY
            }
            catch (System.Threading.ThreadAbortException)
            { }
            catch (Exception ex)
            {
                log.Trace("login: "+ex.Message);
            }
        }
        #endregion

        #region Remember me
        /// <summary>
        /// FB 2009
        /// </summary>
        private void RemembermeLogin()
        {
            try
            {
                if (Request.Cookies["VRMuser"] != null)
                {
                    if (Request.Cookies["VRMuser"].HasKeys)
                    {
                        if (Request.Cookies["VRMuser"]["macid"] == null) // ZD 100263 Starts
                            return;


                        if (Request.Cookies["VRMuser"]["act"] != null)
                            if (Request.Cookies["VRMuser"]["act"] != "")
                                loginMgmt.qStrEnUnVal = Request.Cookies["VRMuser"]["act"];

                        if (Request.Cookies["VRMuser"]["pwd"] != null)
                            if (Request.Cookies["VRMuser"]["pwd"] != "")
                                loginMgmt.qStrEnPwVal = Request.Cookies["VRMuser"]["pwd"];

                        if (Request.Cookies["VRMuser"]["macid"] != null)
                            if (Request.Cookies["VRMuser"]["macid"] != "")
                                loginMgmt.qStrEnIpVal = Request.Cookies["VRMuser"]["macid"];

                        /*
                        if (UserName.Value.ToString().Trim().IndexOf("@") <= 0)
                        {
                            UserName.Value = "";
                            return;
                        }

                        if (UserPassword.Value.ToString().Trim() == "")
                        {
                            UserPassword.Value = "";
                            return;
                        }
                        */

                        //loginMgmt = new MyVRMNet.LoginManagement();
                        loginMgmt.queyStraVal = UserName.Value.ToString().Trim();

                        Session["LoginUserName"] = loginMgmt.qStrEnUnVal; // FB 1725
                        //loginMgmt.qStrPVal = UserPassword.Value.ToString().Trim();
                        
                        loginMgmt.qStrIpVal = macID; // ZD 100263 Ends
                        loginMgmt.windwsAuth = false;
                        login();
                    }
                }

            }
            catch (Exception ex)
            {

                log.Trace("Rememberme: " + ex.Message);
            }
        }

        #endregion

        //ZD 100263 Starts
        #region Bind Processor

        public void BindProcessorDetails()
        {
            ManagementObjectSearcher objCS;
            try
            {
                objCS = null;
                objCS = new ManagementObjectSearcher("SELECT * FROM Win32_NetworkAdapter");

                foreach (ManagementObject objmgmt in objCS.Get())
                {
                    if (objmgmt.Properties["AdapterTypeID"].Value != null)//Ethernet 802.3
                        if (objmgmt.Properties["AdapterTypeID"].Value.ToString() == "0")
                            macID += "," + objmgmt.Properties["MACAddress"].Value.ToString();
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                errLabel.Visible = true;
            }
        }
        #endregion
        //ZD 100263 Starts

        /* commented for ZD 104482
        //ZD 103496 Starts
        #region SetCache
        /// <summary>
        /// SetCache
        /// </summary>
        private void SetCache()
        {
            DataTable myVRMRoomsDatatable = null;
            String roomXML = "";
             XmlDocument xmldoc = new XmlDocument();
            XmlNodeList nodes = null;
            DataSet ds = null;
            XmlTextReader txtrd = null;
            DataTable dtTable = null;
            string roomInXML1 = "";
            string outXML = "";
            DataView dv = null;
            StringWriter txtWriter = null;
            try
            {
                using (memClient = new Enyim.Caching.MemcachedClient())
                {
                    roomXML = myVRMNet.NETFunctions.DecompressString(memClient.Get<String>("myVRMRoomsDatatable"));


                    if (String.IsNullOrEmpty(roomXML))
                    {
                        roomInXML1 =
                             "<GetAllRoomsBasicInfo><UserID>11</UserID><organizationID>11</organizationID><multisiloOrganizationID>0</multisiloOrganizationID><RoomID /><isMemCached>1</isMemCached><SearchCriteria>"
                             + "<selQuery>RoomID NOT IN (11) and ( VideoAvailable = 0 or VideoAvailable = 1 or VideoAvailable = 2 )</selQuery>"
                             + "<Disabled>0</Disabled><Favorites>0</Favorites><Department /><isEnableAVItem>0</isEnableAVItem><ListAVItem /><AVItemNone>0</AVItemNone><avItemQuery>or</avItemQuery>"
                             + "</SearchCriteria><pageNo>0</pageNo><MaxRecords>0</MaxRecords></GetAllRoomsBasicInfo>";

                        outXML = obj.CallMyVRMServer("GetAllRoomsInfoforCache", roomInXML1.ToString(), Application["MyVRMServer_ConfigPath"].ToString());

                        xmldoc.LoadXml(outXML);

                        if (ds == null)
                            ds = new DataSet();

                        txtrd = new XmlTextReader(xmldoc.OuterXml, XmlNodeType.Document, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(txtrd, XmlReadMode.InferSchema);

                        dv = new DataView(ds.Tables[0]);
                        dv.Sort = "Tier1Name,Tier2Name,RoomName ASC";
                        txtWriter = new StringWriter();
                        ds.WriteXml(txtWriter);
                        roomXML = txtWriter.ToString();
                        if (!String.IsNullOrEmpty(roomXML))
                        {

                            memClient.Store(Enyim.Caching.Memcached.StoreMode.Set, "myVRMRoomsDatatable", myVRMNet.NETFunctions.CompressString(roomXML));

                        }
                        else
                            log.Trace("Error no rooms are wriiten");
                        
                    }

                   
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + "Inner" + ex.InnerException + "Stac" + ex.StackTrace);
            }
            finally
            {
                txtWriter = null;
            }
        }
        #endregion
        //ZD 103496 End
         * */
    }
}
