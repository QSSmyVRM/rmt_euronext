/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Collections.Generic;

using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxGridView.Export.Helper;
using DevExpress.Web.ASPxClasses;
using DevExpress.Web.ASPxEditors;
using DevExpress.Web.ASPxHtmlEditor;
using DevExpress.Web.ASPxPopupControl;
using DevExpress.Web.ASPxMenu;
using DevExpress.XtraPrinting;
using DevExpress.XtraCharts.Native;
using DevExpress.XtraCharts;
using DevExpress.Utils;
                

/// <summary>
/// Summary description for SuperAdministrator.
/// </summary>

public partial class HDConferenceList : System.Web.UI.Page
{
    #region protected Members
    
    protected System.Web.UI.WebControls.Label errLabel;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnValue;
    protected DevExpress.Web.ASPxGridView.ASPxGridView MainGrid;
    protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEditForm; //ZD 101597     
    
    #endregion

    #region protected data members 

    private myVRMNet.NETFunctions obj;
    private ns_Logger.Logger log;

    protected int orgId = 11;
    protected XmlDocument xmlDoc = null;
    protected DataSet ds = null;
    protected DataSet dr = null;
    protected DataTable rptTable = new DataTable();
    protected string format = "dd/MM/yyyy";
    protected string tformat = "hh:mm tt";
    protected string tmzone = "";
    protected string organizationID = "";
    protected int usrID = 11;
    bool isSearchConf = false; //FB 2763
    protected System.Web.UI.WebControls.Button btnCancel; //FB 2763
    bool isExpressUser = false; //ZD 101388
    #endregion

    #region Constructor 
    public HDConferenceList()
    {
        obj = new myVRMNet.NETFunctions();
        log = new ns_Logger.Logger();
    }

    #endregion

    //ZD 101022
    #region InitializeCulture
    protected override void InitializeCulture()
    {
        if (Session["UserCulture"] != null)
        {
            UICulture = Session["UserCulture"].ToString();
            Culture = Session["UserCulture"].ToString();
            base.InitializeCulture();
        }
    }
    #endregion
    
    #region Methods Executed on Page Load 

    private void Page_Load(object sender, System.EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("HDConferenceList.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            if (Session["userID"] != null)
            {
                if (Session["userID"].ToString() != "")
                    int.TryParse(Session["userID"].ToString(), out usrID);
            }

            if (Session["FormatDateType"] != null)
            {
                if (Session["FormatDateType"].ToString() != "")
                    format = Session["FormatDateType"].ToString();
            }

            if (Session["timeFormat"] != null)
            {
                if (Session["timeFormat"].ToString() != "")
                    if (Session["timeFormat"].ToString() == "0")
                        tformat = "HH:mm";
            }


            if (Session["isExpressUser"] != null && Session["isExpressUserAdv"] != null && Session["isExpressManage"] != null)
            {
                if (Session["isExpressUser"].ToString() == "1" || Session["isExpressUserAdv"].ToString() == "1" || Session["isExpressManage"].ToString() == "1")
                    isExpressUser = true;
            }

            if (Request.QueryString["frm"] != null) //FB 2763
                if (Request.QueryString["frm"].ToString().Equals("1"))
                {
                    isSearchConf = true;
                    btnCancel.Visible = true;
                }
           
            //if (!IsPostBack)
                BindData();
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("Page_Load" + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;            
        }
    }

    #endregion

    #region BindData 

    private void BindData()
    {
        try
        {
            XmlDocument xmlDoc = new XmlDocument();
            string outXML = obj.CallMyVRMServer("SearchConference", Session["inXML"].ToString(), Application["MyVRMServer_ConfigPath"].ToString());
            if (outXML.IndexOf("<error>") < 0)
            {
                xmlDoc.LoadXml(outXML);
                ds = new DataSet();
                dr = new DataSet();

                ds.ReadXml(new XmlNodeReader(xmlDoc));

                if (ds.Tables.Count > 0)
                {
                    CreateTable();

                    try
                    {
                        MainGrid.DataSource = rptTable;
                        MainGrid.DataBind();
                    }
                    catch (Exception ex)
                    {
                        log.Trace(ex.StackTrace + " : " + ex.Message);
                    }
                }
            }
            else
            {
                errLabel.Text = obj.ShowErrorMessage(outXML);
                errLabel.Visible = true;
            }
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("BindData" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();
            errLabel.Visible = true;            
        }
    }

    #endregion

    #region MainGrid_HtmlRowCreated

    protected void MainGrid_HtmlRowCreated(object sender, DevExpress.Web.ASPxGridView.ASPxGridViewTableRowEventArgs e)
    {
        try
        {
            string CN = "Confirmation <br>Number", RS = "Reservation <br>Start Date/Time";
            //CN = CN.Replace("<", "&lt;").Replace(">", "&gt;");//ZD 100288
            //RS = RS.Replace("<", "&lt;").Replace(">", "&gt;");
            CN = obj.GetTranslatedText("Confirmation") + "<br>" + obj.GetTranslatedText("Number");
            RS = obj.GetTranslatedText("Reservation") + "<br>" + obj.GetTranslatedText("Start Date/Time");
            //CN = obj.GetTranslatedText(CN);
            //CN = CN.Replace("&lt;", "<").Replace("&gt;", ">");
            //RS = obj.GetTranslatedText(RS);
            //RS = RS.Replace("&lt;", "<").Replace("&gt;", ">");

            GridViewDataColumn colDuration = (GridViewDataColumn)MainGrid.Columns[obj.GetTranslatedText("Duration")];

            // ZD 100263 Starts
            if (Session["roomCascadingControl"] != null && Session["roomCascadingControl"].ToString().Equals("1"))
                if (colDuration == null)
                {
                    log.Trace("Page: HDConferenceList.aspx.cs, Function: MainGrid_HtmlRowCreated, Reason: GridViewDataColumn colDuration is null");
                    Response.Redirect("ShowError.aspx");
                }
            // ZD 100263 Ends
            //ZD 101597
            if (colDuration != null)
                colDuration.Width = Unit.Percentage(8);
            GridViewDataColumn colConfirmationNumber = (GridViewDataColumn)MainGrid.Columns[obj.GetTranslatedText("Confirmation") + "<br>" + obj.GetTranslatedText("Number")];//ZD 100288
            colConfirmationNumber.Width = Unit.Percentage(9);
            GridViewDataColumn colReservationDate = (GridViewDataColumn)MainGrid.Columns[obj.GetTranslatedText("Reservation") + "<br>" + obj.GetTranslatedText("Start Date/Time")];
            colReservationDate.Width = Unit.Percentage(15);
            GridViewDataColumn colRequestor = (GridViewDataColumn)MainGrid.Columns[obj.GetTranslatedText("Requestor")];
            colRequestor.Width = Unit.Percentage(13);
            GridViewDataColumn colHost = (GridViewDataColumn)MainGrid.Columns[obj.GetTranslatedText("Host")];
            colHost.Width = Unit.Percentage(13);
            GridViewDataColumn colRooms = (GridViewDataColumn)MainGrid.Columns[obj.GetTranslatedText("Rooms")];
            colRooms.Width = Unit.Percentage(15);//ZD 100288
            GridViewDataColumn colR = (GridViewDataColumn)MainGrid.Columns[obj.GetTranslatedText("Actions")];
            colR.Width = Unit.Percentage(28);

            if (e.RowType != DevExpress.Web.ASPxGridView.GridViewRowType.Data) return;
            string colValue = "";
            //ZD 102783
            string isHost = "", isExpressConf = ""; //ZD 101233
            int confStatus = 0;
            GridViewDataColumn colID = (GridViewDataColumn)MainGrid.Columns["ID"];
            if (colID != null)
            {
                colValue = MainGrid.GetRowValues(e.VisibleIndex, MainGrid.Columns["ID"].ToString()).ToString();
                colID.Visible = false;
            }
            
            GridViewDataColumn colIsHost = (GridViewDataColumn)MainGrid.Columns[obj.GetTranslatedText("Ishost")];
            if (colIsHost != null)
            {
                isHost = MainGrid.GetRowValues(e.VisibleIndex, MainGrid.Columns[obj.GetTranslatedText("Ishost")].ToString()).ToString();
                colIsHost.Visible = false;
            }

            GridViewDataColumn colStatus = (GridViewDataColumn)MainGrid.Columns[obj.GetTranslatedText("Status")];
            if (colStatus != null)
            {	//ZD 102783
                int.TryParse(MainGrid.GetRowValues(e.VisibleIndex, MainGrid.Columns[obj.GetTranslatedText("Status")].ToString()).ToString(), out confStatus);
                colStatus.Visible = false;
            }
            //ZD 101233 Starts
            GridViewDataColumn colisExpressConf = (GridViewDataColumn)MainGrid.Columns["Express"];
            if (colisExpressConf != null)
            {
                isExpressConf = MainGrid.GetRowValues(e.VisibleIndex, MainGrid.Columns["Express"].ToString()).ToString();
                colisExpressConf.Visible = false;
            }

            if (colRooms != null)
            {
                int index = colRooms.Index;
                //e.Row.Cells[index].Wrap = false;
                string rmText = MainGrid.GetRowValues(e.VisibleIndex, obj.GetTranslatedText("Rooms")).ToString(); //ZD 100288
                e.Row.Cells[colRooms.Index].Text = rmText.Replace(",", "<br>");
            }
            
            if (colR != null)
            {
                 int hasView = 0;
                 int hasEdit = 0;
                 int hasDelete = 0;
                 int hasManage = 0;
                 int hasClone = 0;
                 int hasMCUInfo = 0;
                 int hasExtendTime = 0;
                 int filterType = 0;
                 //ZD 102783 Starts
                 int hasReservations = 0;

                 if (Session["hasReservations"] != null && Session["hasReservations"].ToString() == "1")
                     hasReservations = 1;

                 if (hasReservations == 1 && confStatus == 0) //Reservation
                     filterType = 3;
                 else if (confStatus == 1) //Pending
                     filterType = 5;
                 else if (confStatus == 6) //On MCU
                     filterType = 8;
                 else if (confStatus == 5) //Ongoing
                     filterType = 2;
                 else if (confStatus == 3 || confStatus == 9) //Terminated or Deleted
                     filterType = 9;
                 else if (confStatus == 7) //Completed
                     filterType = 10;
                 else if (confStatus == 2) //Wait List
                     filterType = 11;
                 else
                     filterType = 3; //if (confStatus == 0 && isPublicConf == 0)//Reservation with Puclic or ConfSupport or without both
                 //ZD 102783 end
                obj.CheckConferenceRights(filterType, ref hasView, ref hasManage, ref hasExtendTime, ref hasMCUInfo, ref hasEdit, ref hasDelete, ref hasClone);


                int index = colR.Index;
                string rCellText = MainGrid.GetRowValues(e.VisibleIndex, obj.GetTranslatedText("Actions")).ToString(); //ZD 100288
                LinkButton lnkView = new LinkButton();
                if (hasView == 1)
                {
                    lnkView.ID = "View" + e.VisibleIndex + "_" + index;
                    //ZD 101028 start
                    lnkView.Attributes.Add("name", colValue);
                    lnkView.Attributes.Add("onclick", "javascript:return fnInvoke(this);");
                    //lnkView.Attributes.Add("ondblclick", "javascript:return fnOpen('V','" + colValue + "');return false;");//ZD 101028 end
                    lnkView.Style.Add("Cursor", "Hand");
                    lnkView.Text = obj.GetTranslatedText("View") + e.KeyValue;
                    e.Row.Cells[index].Controls.Add(lnkView);
                }

                if ((Request.QueryString["hf"] != "1") && isHost == "1" || (int.Parse(Session["admin"].ToString()) > 0))
                {
                    e.Row.Cells[index].Wrap = false;
                    e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;"));
                    if (confStatus != Convert.ToInt16(ns_MyVRMNet.vrmConfStatus.Ongoing))
                    {
                        if (hasClone == 1)
                        {
                            LinkButton lnkClone = new LinkButton();
                            lnkClone.ID = "Clone" + e.VisibleIndex + "_" + index;
                            lnkClone.Attributes.Add("onclick", "javascript:fnOpen('C','" + colValue + "')");
                            lnkClone.Style.Add("Cursor", "Hand");
                            lnkClone.Text = obj.GetTranslatedText("Clone");
                            lnkClone.Click += new EventHandler(lnkClone_Click);
                            e.Row.Cells[index].Controls.Add(lnkClone);
                        }
                    }

                    if (confStatus != Convert.ToInt16(ns_MyVRMNet.vrmConfStatus.Completed) && confStatus != Convert.ToInt16(ns_MyVRMNet.vrmConfStatus.Ongoing) && confStatus != Convert.ToInt16(ns_MyVRMNet.vrmConfStatus.Deleted) && confStatus != Convert.ToInt16(ns_MyVRMNet.vrmConfStatus.Terminated)) //TICK  #100037 
                    {
                        if (hasDelete == 1)
                        {

                            e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;"));
                            LinkButton lnkDelete = new LinkButton();
                            lnkDelete.ID = "Delete" + e.VisibleIndex + "_" + index;
                            lnkDelete.Attributes.Add("onclick", "javascript:fnOpen('D','" + colValue + "')");
                            lnkDelete.Style.Add("Cursor", "Hand");
                            lnkDelete.Text = obj.GetTranslatedText("Delete");
                            lnkDelete.Click += new EventHandler(lnkDelete_Click);
                            e.Row.Cells[index].Controls.Add(lnkDelete);
                        }

                        if (hasEdit == 1)
                        {
                            e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;"));
                            LinkButton lnkEdit = new LinkButton();
                            lnkEdit.ID = "Edit" + e.VisibleIndex + "_" + index;
                            //ZD 101597
                            lnkEdit.Attributes.Add("onclick", "javascript:return fnOpen('E','" + colValue + "')");
                            lnkEdit.Style.Add("Cursor", "Hand");
                            lnkEdit.Text = obj.GetTranslatedText("Edit");

                            
                            if (Int32.Parse(Session["admin"].ToString()) <= 0)
                            {
                                if (isExpressConf == "0" && !isExpressUser)
                                    lnkEdit.Click += new EventHandler(lnkEdit_Click);
                                else
                                    lnkEdit.Click += new EventHandler(lnkExpressEdit_Click);
                            }
							else
								lnkEdit.Click += new EventHandler(lnkEdit_Click);//ZD 102200
                            e.Row.Cells[index].Controls.Add(lnkEdit);
                        }
                    }

                    if (hasManage == 1)
                    {
                        e.Row.Cells[index].Controls.Add(new LiteralControl("&nbsp;"));
                        LinkButton lnkManage = new LinkButton();
                        lnkManage.ID = "Manage" + e.VisibleIndex + "_" + index;
                        lnkManage.Attributes.Add("onclick", "javascript:fnOpen('M','" + colValue + "')");
                        lnkManage.Style.Add("Cursor", "Hand");
                        lnkManage.Text = obj.GetTranslatedText("Manage");
                        lnkManage.Click += new EventHandler(lnkManage_Click);
                        e.Row.Cells[index].Controls.Add(lnkManage);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            errLabel.Visible = true;
            errLabel.Text = obj.ShowSystemMessage();//ZD 100263
            log.Trace(ex.StackTrace + " MainGrid_HtmlRowCreated: " + ex.Message);
        }
    }

    void lnkDelete_Click(object sender, EventArgs e)
    {
        DeleteConference(null, null);
    }

    void lnkClone_Click(object sender, EventArgs e)
    {
        Session["ConfID"] = hdnValue.Value;
        Response.Redirect("ConferenceSetup.aspx?t=o");
    }

    protected void lnkEdit_Click(object sender, EventArgs e)
    {
        Session["ConfID"] = hdnValue.Value;
        //ZD 101233 START
        int isExpress = 0, isAdvancedForm = 0, isAdmin = 0;//ZD 102200
        if (Session["hasConference"] != null)
            int.TryParse(Session["hasConference"].ToString(), out isAdvancedForm);
        if (Session["hasExpConference"] != null)
            int.TryParse(Session["hasExpConference"].ToString(), out isExpress);
        if (Session["admin"] != null)
            int.TryParse(Session["admin"].ToString(), out isAdmin);
        //ZD 102200 Ends
        //ZD 101597 //ZD 102200 Starts
        if (isAdvancedForm == 1 && isExpress == 1)
        {
            if (isAdmin > 0)
            {
                if (hdnEditForm.Value == "1")
                    Response.Redirect("ConferenceSetup.aspx?t=");
                else if (hdnEditForm.Value == "2")
                    Response.Redirect("ExpressConference.aspx?t=");
            }
            else
            {
                if (isAdvancedForm == 1)
                    Response.Redirect("ConferenceSetup.aspx?t=");
                else if (isExpress == 1)
                    Response.Redirect("ExpressConference.aspx?t=");
            }
        }
        else if (isAdvancedForm == 1)
            Response.Redirect("ConferenceSetup.aspx?t=");
        else if (isExpress == 1)
            Response.Redirect("ExpressConference.aspx?t=");
        //ZD 101233 END //ZD 102200 Ends

        hdnEditForm.Value = "";
    }

    void lnkExpressEdit_Click(object sender, EventArgs e)
    {
        Session["ConfID"] = hdnValue.Value;
        Response.Redirect("ExpressConference.aspx?t=");
    }


    void lnkManage_Click(object sender, EventArgs e)
    {
        Session["ConfID"] = hdnValue.Value;
        Response.Redirect("ManageConference.aspx?t=");
    }

    #endregion

    #region DeleteConference
    protected void DeleteConference(Object sender, DataGridCommandEventArgs e)
    {
        try
        {
            if (hdnValue.Value == "")
                return;

            string inXML = "";
            inXML += "<login>";
            inXML += obj.OrgXMLElement();
            inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
            inXML += "  <delconference>";
            inXML += "      <conference>";
            inXML += "          <confID>" + hdnValue.Value + "</confID>";
            inXML += "          <reason></reason>";
            inXML += "      </conference>";
            inXML += "  </delconference>";
            inXML += "</login>";
            string outXML = obj.CallMyVRMServer("DeleteConference", inXML, Application["MyVRMServer_ConfigPath"].ToString()); 
            log.Trace("DeleteConference - " + outXML);
            if (outXML.IndexOf("<error>") >= 0)
            {
                errLabel.Text = obj.ShowErrorMessage(outXML);
                errLabel.Visible = true;
                BindData();
            }
            else
            {
                if (Application["External"].ToString() != "")
                {
                    string inEXML = "";
                    inEXML = "<SetExternalScheduling>";
                    inEXML += "<confID>" + hdnValue.Value + "</confID>";
                    inEXML += "</SetExternalScheduling>";

                    string outExml = obj.CallCommand("SetExternalScheduling", inEXML);
                }

                string inxml = "<DeleteParticipantICAL>";
                inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "<ConfID>" + hdnValue.Value + "</ConfID>";
                inxml += "</DeleteParticipantICAL>";
                obj.CallCommand("DeleteParticipantICAL", inxml);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }

                inxml = "<DeleteCiscoICAL>";
                inxml += "<UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "<ConfID>" + hdnValue.Value + "</ConfID>";
                inxml += "</DeleteCiscoICAL>";
                obj.CallCommand("DeleteCiscoICAL", inxml);
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }

                Session["CalendarMonthly"] = null;
                inXML = "";
                inXML += "<login>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + Session["userID"].ToString() + "</userID>";
                inXML += "  <ConferenceID>" + hdnValue.Value + "</ConferenceID>";
                inXML += "  <WorkorderID>0</WorkorderID>";
                inXML += "</login>";

                outXML = obj.CallMyVRMServer("DeleteWorkOrder", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    string qString = "m=1&t=" + Request.QueryString["t"].ToString();
                    if (Request.QueryString["pageNo"] != null)
                        qString += "&pageNo=" + Request.QueryString["pageNo"].ToString();
                    //ZD 101673 start
                    if (isSearchConf == true)
                        qString += "&frm=1";
                    //ZD 101673 End
                    Response.Redirect("HDConferenceList.aspx?" + qString);
                }
            }
        }
        catch (Exception ex)
        {
            //ZD 100263
            log.Trace("DeleteConference" + ex.StackTrace);
            errLabel.Text = obj.ShowSystemMessage();            
            errLabel.Visible = true;
        }
    }
    #endregion

    #region CreateTable 

    private void CreateTable()
    {
        try
        {
            string CN = "Confirmation <br/>Number", RS = "Reservation <br/>Start Date/Time";
            //CN = CN.Replace("<", "&lt;").Replace(">", "&gt;");//ZD 100288
            //RS = RS.Replace("<", "&lt;").Replace(">", "&gt;");
            //CN = obj.GetTranslatedText(CN);
            //CN = CN.Replace("&lt;", "<").Replace("&gt;", ">");
            //RS = obj.GetTranslatedText(RS);
            //RS = RS.Replace("&lt;", "<").Replace("&gt;", ">");
            CN = obj.GetTranslatedText("Confirmation") + "<br>" + obj.GetTranslatedText("Number");
            RS = obj.GetTranslatedText("Reservation") + "<br>" + obj.GetTranslatedText("Start Date/Time");

            rptTable = new DataTable();
            rptTable.Columns.Add(obj.GetTranslatedText("Confirmation") + "<br>" + obj.GetTranslatedText("Number"), typeof(String));
            rptTable.Columns.Add(obj.GetTranslatedText("Reservation") + "<br>" + obj.GetTranslatedText("Start Date/Time"), typeof(String));
            rptTable.Columns.Add(obj.GetTranslatedText("Duration"), typeof(String));
            rptTable.Columns.Add(obj.GetTranslatedText("Requestor"), typeof(String));
            rptTable.Columns.Add(obj.GetTranslatedText("Host"), typeof(String));
            rptTable.Columns.Add(obj.GetTranslatedText("Rooms"), typeof(String));
            rptTable.Columns.Add(obj.GetTranslatedText("Actions"), typeof(String));

            DataTable detailsTable = null;
            if (ds.Tables.Count > 2)
                detailsTable = ds.Tables[2];

            if (detailsTable != null)
            {
                for (int i = 0; i < detailsTable.Rows.Count; i++)
                {
                    if (i == 0)
                    {
                        rptTable.Columns.Add("ID", typeof(String));
                        rptTable.Columns.Add(obj.GetTranslatedText("Ishost"), typeof(String));
                        rptTable.Columns.Add(obj.GetTranslatedText("Status"), typeof(String));
                        rptTable.Columns.Add("Express", typeof(String));
                    }
                    DataRow dataRow = null;

                    dataRow = rptTable.NewRow();

                    if (detailsTable.Rows[0][1].ToString() == "")
                        break;

                    DateTime startDate;
                    dataRow[0] = detailsTable.Rows[i]["ConferenceUniqueID"].ToString();
                    startDate = DateTime.Parse(detailsTable.Rows[i]["ConferenceDateTime"].ToString());
                    dataRow[1] = startDate.ToString(format) + " " + startDate.ToString(tformat);
                    dataRow[2] = detailsTable.Rows[i]["ConferenceDuration"].ToString() + " mins";//ZD 100528
                    dataRow[3] = detailsTable.Rows[i]["ConferenceRequestor"].ToString();
                    dataRow[4] = detailsTable.Rows[i]["ConferenceHost"].ToString();
                    dataRow[5] = detailsTable.Rows[i]["LocationList"].ToString().Replace("~", ", "); //<br />");
                    //dataRow[6] - Actions
                    dataRow[7] = detailsTable.Rows[i]["ConferenceID"].ToString();
                    dataRow[8] = detailsTable.Rows[i]["Ishost"].ToString();
                    dataRow[9] = detailsTable.Rows[i]["ConferenceStatus"].ToString();
                    dataRow[10] = detailsTable.Rows[i]["isExpressConference"].ToString(); //ZD 101233

                    rptTable.Rows.Add(dataRow);
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace("CreateTable " + ex.Message);
            errLabel.Text = obj.ShowSystemMessage();
        }
    }

    #endregion
    
    //FB 2763
    #region GoBack
    /// <summary>
    /// GoToLobby
    /// Fogbugz case 158
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void GoBack(Object sender, EventArgs e)
    {
        try
        {
            if (Application["loginPage"] != null)
            {
                Response.Redirect(Application["loginPage"].ToString());
            }
            else if (isSearchConf) //FB 2763
                Response.Redirect("SearchConferenceInputParameters.aspx");
           
        }
        catch (Exception ex)
        {
            log.Trace(ex.Message + " : " + ex.StackTrace);
        }
    }
    #endregion
}