/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100889
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Text;

namespace ns_MyVRM
{
    public partial class SelRoomResources : System.Web.UI.Page
    {
        protected HtmlTableRow trRoomName;
        protected HtmlTableRow trTier;
        protected HtmlTableRow trFloorRoom;
        protected HtmlTableRow trAddress;
        protected HtmlTableRow trPDir;
        protected HtmlTableRow trMapLink;
        protected HtmlTableRow trTz;
        protected HtmlTableRow trPhoneNo;
        protected HtmlTableRow trCapacity;
        //protected HtmlTableRow trSTB;//ZD 101563
        //protected HtmlTableRow trTTB;
        protected HtmlTableRow trProjector;
        protected HtmlTableRow trRImg;
        protected HtmlTableRow trCF;
        protected HtmlTableRow trAIC;
        protected HtmlTableRow trMap1;
        protected HtmlTableRow trSecurity1;
        protected HtmlTableRow trMisc1;
        protected HtmlTableRow trPriAppr;
        protected HtmlTableRow trEPT;
        protected HtmlTableRow trDept;
        protected HtmlTableRow trExternalNumber; //FB 2448
        protected HtmlTableRow trInternalNumber; //FB 2448

        protected Label lblHeading;
        protected Label multicodec; //FB 2400

        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        String RoomId;
        Int32 RoomCnt = 0;
        myVRMNet.ImageUtil imageUtilObj = null;
        byte[] imgArray = null;
        protected String language = "";//FB 1830
        bool isVMR = false; //FB 2448
        bool RoomCategory = false; //FB 2694
        bool isHotdeskingRoom = false; //ALLBUGS-23

        public SelRoomResources()
        {
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            imageUtilObj = new myVRMNet.ImageUtil(); //Image Project
        }

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                base.InitializeCulture();
            }
        }
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("roomresourcecomparesel.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

                //FB 1830 - Starts
                if (Session["language"] == null)
                    Session["language"] = "en";

                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();

                //FB 1830 - End
                if (!IsPostBack) //if page loads for the first time
                {
                    if (Request.QueryString["rms"] != null)
                        RoomId = Request.QueryString["rms"].ToString().Trim();
                    
                    BindData();
                }
                /* *** Code added for FB 1425 QA Bug -Start *** */

                if (Session["timeZoneDisplay"] != null)
                {
                    if (Session["timeZoneDisplay"].ToString() == "0")
                        trTz.Attributes.Add("style", "display:none");

                }

                /* *** Code added for FB 1425 QA Bug -End *** */
            }
            catch (Exception ex)
            {
                log.Trace("PageLoad: " + ex.StackTrace + " : " + ex.Message);
                //Response.Write(ex.Message);
            }
        }

        private DataTable BuildTableSchema()
        {
            DataTable dt = null;
            try
            {
                dt = new DataTable("RoomDetails");
                dt.Columns.Add(new DataColumn("RoomName", typeof(String)));
                dt.Columns.Add(new DataColumn("RoomPhoneNumber", typeof(String)));
                dt.Columns.Add(new DataColumn("RoomFloor", typeof(String)));
                dt.Columns.Add(new DataColumn("Projector", typeof(String)));
                dt.Columns.Add(new DataColumn("MaximumCapacity", typeof(String)));
                //dt.Columns.Add(new DataColumn("SetupTime", typeof(String)));//ZD 101563
                //dt.Columns.Add(new DataColumn("TeardownTime", typeof(String)));
                dt.Columns.Add(new DataColumn("AssistantInchargeName", typeof(String)));
                dt.Columns.Add(new DataColumn("Address", typeof(String)));
                dt.Columns.Add(new DataColumn("ParkingDirections", typeof(String)));
                dt.Columns.Add(new DataColumn("MapLink", typeof(String)));
                dt.Columns.Add(new DataColumn("TimezoneName", typeof(String)));
                dt.Columns.Add(new DataColumn("RoomText", typeof(String)));
                dt.Columns.Add(new DataColumn("Map1", typeof(String)));
                dt.Columns.Add(new DataColumn("Map2", typeof(String)));
                dt.Columns.Add(new DataColumn("Security1", typeof(String)));
                dt.Columns.Add(new DataColumn("Security2", typeof(String)));
                dt.Columns.Add(new DataColumn("Misc1", typeof(String)));
                dt.Columns.Add(new DataColumn("Misc2", typeof(String)));
                dt.Columns.Add(new DataColumn("Approver1Name", typeof(String)));
                dt.Columns.Add(new DataColumn("Approver2Name", typeof(String)));
                dt.Columns.Add(new DataColumn("Approver3Name", typeof(String)));
                dt.Columns.Add(new DataColumn("Tier", typeof(String)));
                dt.Columns.Add(new DataColumn("CatererFacility", typeof(String)));
                dt.Columns.Add(new DataColumn("Endpoint", typeof(String)));
                dt.Columns.Add(new DataColumn("Departments", typeof(String)));
                dt.Columns.Add(new DataColumn("GooleMap", typeof(String))); //Maps
                dt.Columns.Add(new DataColumn("YahooMap", typeof(String))); //Maps
                dt.Columns.Add(new DataColumn("BingMap", typeof(String))); //Maps
                dt.Columns.Add(new DataColumn("IsVMR", typeof(String))); //FB 2448 start
                dt.Columns.Add(new DataColumn("InternalNumber", typeof(String))); 
                dt.Columns.Add(new DataColumn("ExternalNumber", typeof(String))); //FB 2448 start
                dt.Columns.Add(new DataColumn("RoomCategory", typeof(String)));  //FB 2694
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
            return dt;
        }

        private void GetRoomProfileXML(String RoomID, ref DataTable dtRD)
        {
            //DataTable dtRoomDetails = null;
            try
            {

                String inXML = "<GetRoomProfile>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <RoomID>" + RoomID + "</RoomID>";
                inXML += "</GetRoomProfile>";


                log.Trace("GetRoomProfile inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("GetRoomProfile", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                //outXML = "<GetRoomProfile><RoomID>74</RoomID><RoomName>ROOMWithAppr74</RoomName><RoomPhoneNumber>521-524-5215</RoomPhoneNumber><MaximumCapacity>5</MaximumCapacity><MaximumConcurrentPhoneCalls>10</MaximumConcurrentPhoneCalls><SetupTime>20</SetupTime><TeardownTime>15</TeardownTime><AssistantInchargeID>297</AssistantInchargeID><AssistantInchargeName>room assistant</AssistantInchargeName><MultipleAssistantEmails>ROOMWithApprAsst11@myvrm.com;ROOMWithApprAsst12@myvrm.com</MultipleAssistantEmails><Tier1ID>4</Tier1ID><Tier1Name>Florence</Tier1Name><Tier2ID>46</Tier2ID><Tier2Name>Other</Tier2Name><CatererFacility>1</CatererFacility><DynamicRoomLayout>1</DynamicRoomLayout><Projector>1</Projector><Video>0</Video><Floor>12</Floor><RoomNumber>16</RoomNumber><StreetAddress1>Test Street Address1</StreetAddress1><StreetAddress2>Test Street Address2</StreetAddress2><City>City</City><State>0</State><StateName></StateName><ZipCode>11590</ZipCode><Country>1</Country><CountryName>Afghanistan</CountryName><MapLink>Maplink</MapLink><ParkingDirections>Parking Directions</ParkingDirections><AdditionalComments></AdditionalComments><TimezoneID>0</TimezoneID><TimezoneName></TimezoneName><Longitude>37.0625</Longitude><Latitude>-95.677068</Latitude><Images><Map1></Map1><Map2></Map2><Security1></Security1><Security2></Security2><Misc1></Misc1><Misc2></Misc2></Images><Approvers><Approver1ID>305</Approver1ID><Approver1Name>room approver  </Approver1Name><Approver2ID>300</Approver2ID><Approver2Name>Saima Ahmad  </Approver2Name><Approver3ID>11</Approver3ID><Approver3Name>VRM Administrator  </Approver3Name></Approvers><EndpointID>-1</EndpointID><EndpointName></EndpointName><EndpointIP></EndpointIP><RoomImage>room1,room2</RoomImage><Custom1></Custom1><Custom2></Custom2><Custom3></Custom3><Custom4></Custom4><Custom5></Custom5><Custom6></Custom6><Custom7></Custom7><Custom8></Custom8><Custom9></Custom9><Custom10></Custom10><Departments><Department><ID>2</ID><Name>Hershey Med</Name><SecurityKey></SecurityKey></Department><Department><ID>3</ID><Name>Coop Extension </Name><SecurityKey></SecurityKey></Department><Department><ID>4</ID><Name>PSU General University Access</Name><SecurityKey></SecurityKey></Department></Departments></GetRoomProfile>";

                log.Trace("GetRoomProfile outxml: " + outXML);
                
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                DataRow drRD = dtRD.NewRow();
                
                drRD["RoomName"] =  xmldoc.SelectSingleNode("//GetRoomProfile/RoomName").InnerText.Trim();
                drRD["RoomPhoneNumber"] = xmldoc.SelectSingleNode("//GetRoomProfile/RoomPhoneNumber").InnerText.Trim();
                String FlRoom = xmldoc.SelectSingleNode("//GetRoomProfile/Floor").InnerText.Trim();
                if(FlRoom.Length > 0) 
                    FlRoom += "/" ;
                FlRoom += xmldoc.SelectSingleNode("//GetRoomProfile/RoomNumber").InnerText.Trim();
                drRD["RoomFloor"] = FlRoom;
                drRD["MaximumCapacity"] = xmldoc.SelectSingleNode("//GetRoomProfile/MaximumCapacity").InnerText.Trim();
                //drRD["SetupTime"] = xmldoc.SelectSingleNode("//GetRoomProfile/SetupTime").InnerText.Trim();//ZD 101563
                //drRD["TeardownTime"] = xmldoc.SelectSingleNode("//GetRoomProfile/TeardownTime").InnerText.Trim();
                drRD["AssistantInchargeName"] = xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeName").InnerText.Trim();
                String Address = "";

                //Code changed /modified for Maps --- Start
                if (xmldoc.SelectSingleNode("//GetRoomProfile/StreetAddress1").InnerText.Trim().Length > 0)
                {
                    Address += xmldoc.SelectSingleNode("//GetRoomProfile/StreetAddress1").InnerText.Trim();
                }
                if (xmldoc.SelectSingleNode("//GetRoomProfile/StreetAddress2").InnerText.Trim().Length > 0)
                {
                    if (Address != "")
                       Address += "," + xmldoc.SelectSingleNode("//GetRoomProfile/StreetAddress2").InnerText.Trim();
                    else
                        Address = xmldoc.SelectSingleNode("//GetRoomProfile/StreetAddress2").InnerText.Trim();
                }
                if (xmldoc.SelectSingleNode("//GetRoomProfile/City").InnerText.Trim().Length > 0)
                {
                    if(Address != "")
                        Address += "," + xmldoc.SelectSingleNode("//GetRoomProfile/City").InnerText.Trim();
                    else
                        Address = xmldoc.SelectSingleNode("//GetRoomProfile/City").InnerText.Trim();
                }
                if (xmldoc.SelectSingleNode("//GetRoomProfile/StateName").InnerText.Trim().Length > 0)
                {
                    if (Address != "")
                        Address += "," + xmldoc.SelectSingleNode("//GetRoomProfile/StateName").InnerText.Trim();
                    else
                        Address = xmldoc.SelectSingleNode("//GetRoomProfile/StateName").InnerText.Trim();
                }
                if (xmldoc.SelectSingleNode("//GetRoomProfile/CountryName").InnerText.Trim().Length > 0)
                {
                    if (Address != "")
                        Address += "," + xmldoc.SelectSingleNode("//GetRoomProfile/CountryName").InnerText.Trim();
                    else
                        Address = xmldoc.SelectSingleNode("//GetRoomProfile/CountryName").InnerText.Trim();
                    
                }
                if (xmldoc.SelectSingleNode("//GetRoomProfile/ZipCode").InnerText.Trim().Length > 0)
                {
                    if (Address != "")
                        Address += "," + xmldoc.SelectSingleNode("//GetRoomProfile/ZipCode").InnerText.Trim();
                    else
                        Address = xmldoc.SelectSingleNode("//GetRoomProfile/ZipCode").InnerText.Trim();
                       
                    
                }
                drRD["Address"] = Address;
                Address = Address.Replace(" ", "%20");
                String Googleurl = "", Yahoourl = "", Bingurl = "";

                if (Address != "")
                    Googleurl = "http://maps.google.com/maps?f=q&source=s_q&hl=en&geocode=&q=" + Address;
                else
                    Googleurl = "N";

                drRD["GooleMap"] = "<a href='javascript:void(0);' onclick=javascript:GetDirection('" + Googleurl + "');>Google " + obj.GetTranslatedText("Map") + "</a>";
              
                if (Address != "")
                    Yahoourl = "http://maps.yahoo.com/maps_result?ard=1&q1=" + Address + "&zoom=6";
                else
                    Yahoourl = "N";

                drRD["YahooMap"] = "<a href='javascript:void(0);' onclick=javascript:GetDirection('" + Yahoourl + "');>Yahoo " + obj.GetTranslatedText("Map") + "</a>";

                //http://bing.com/maps/default.aspx?where1=11025 old country road,westbury,Ny 11590&cp=43.901683~-69.522416&lvl=12&style=r&v=2
                if (Address != "")
                    Bingurl = "http://bing.com/maps/default.aspx?where1=" + Address + "&cp=43.901683~-69.522416&lvl=12&style=r&v=2";
                else
                    Bingurl = "N";

                drRD["BingMap"] = "<a href='javascript:void(0);' onclick=javascript:GetDirection('" + Bingurl + "');>Bing " + obj.GetTranslatedText("Map") + "</a>";
               
              
                //Code changed /modified for Maps --- End

                String PDir = xmldoc.SelectSingleNode("//GetRoomProfile/ParkingDirections").InnerText.Trim();
                PDir = PDir.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&").Replace("&#37", "%");
                //code added for FB 146 - start
                if (PDir.Length > 0)
                {
                    if ((PDir.Contains("www") || PDir.Contains("http")))
                    {
                        if (PDir.StartsWith("www"))
                            PDir = "http://" + PDir;

                        PDir = "<a href='" + PDir + "' target='_blank'>Click to see Parking Directions</a>";
                    }
                }
                //code added for FB 146 - end
                else
                    PDir = obj.GetTranslatedText("Not Specified"); //FB 2272
                drRD["ParkingDirections"] = PDir;
                String Maplink = xmldoc.SelectSingleNode("//GetRoomProfile/MapLink").InnerText.Trim();
                Maplink = Maplink.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&").Replace("&#37", "%");
                if (Maplink.Length > 0)
                    Maplink = "<a href='" + Maplink + "' target='_blank'>"+obj.GetTranslatedText("Click to see Map")+"</a>"; //ZD 100622
                else
                    Maplink = obj.GetTranslatedText("N/A");
                drRD["MapLink"] = Maplink;
                drRD["TimezoneName"] = xmldoc.SelectSingleNode("//GetRoomProfile/TimezoneName").InnerText.Trim();

                /*String RoomImage = xmldoc.SelectSingleNode("//GetRoomProfile/RoomImage").InnerText.Trim();
                String rImage = "";
                String roomText = "";
                int i = 0;
                if (RoomImage.Trim().Length > 0)
                {
                    if (RoomImage.IndexOf(',') > 0)
                    {
                        String[] arrRmImages = RoomImage.Split(',');
                        foreach (String strImage in arrRmImages)
                        {
                            if (strImage != "[None]") // FB 1200
                            {
                                rImage = strImage.Trim() + ".jpg";
                                if (roomText == "")
                                {
                                    roomText = "<a href='image/room/" + rImage + "' target='_blank' onMouseOver=\"toggleDiv('roomdiv" + RoomCnt.ToString() + i.ToString() + "',1);\" onMouseOut=\"toggleDiv('roomdiv" + RoomCnt.ToString() + i.ToString() + "',0);\" >" + rImage + "</a>";
                                    roomText += "<div class='hdiv' id='roomdiv" + RoomCnt.ToString() + i.ToString() + "'><img src='image/room/" + rImage + "'/></div>";
                                }
                                else
                                {
                                    roomText += ", " + "<a href='image/room/" + rImage + "' target='_blank' onMouseOver=\"toggleDiv('roomdiv" + RoomCnt.ToString() + i.ToString() + "',1);\" onMouseOut=\"toggleDiv('roomdiv" + RoomCnt.ToString() + i.ToString() + "',0);\" >" + rImage + "</a>";
                                    roomText += "<div class='hdiv' id='roomdiv" + RoomCnt.ToString() + i.ToString() + "'><img src='image/room/" + rImage + "'/></div>";
                                }
                                i++;
                            } // FB 1200
                        }
                    }
                    else
                    {
                        if (RoomImage != "[None]") // FB 1200
                        {
                            rImage = RoomImage.Trim() + ".jpg";
                            roomText = "<a href='image/room/" + rImage + "' target='_blank' onMouseOver=\"toggleDiv('roomdiv" + RoomCnt.ToString() + i.ToString() + "',1);\" onMouseOut=\"toggleDiv('roomdiv" + RoomCnt.ToString() + i.ToString() + "',0);\" >" + rImage + "</a>";
                            roomText += "<div class='hdiv' id='roomdiv" + RoomCnt.ToString() + i.ToString() + "'><img src='image/room/" + rImage + "'/></div>";
                        }// FB 1200
                    }
                }*/
               XmlNodeList rmImgNodes = xmldoc.SelectNodes("//GetRoomProfile/RoomImages/ImageDetails");

                if (rmImgNodes != null)
                    drRD["RoomText"] = LoadRoomImageGrid(rmImgNodes);

                //drRD["RoomText"] = roomText;
                string Map1  = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Map1").InnerText.Trim();
                string MapText = "", imgData = "";//ZD 103569 start
                if (Map1.Length > 0)
                {
                    imgData = "";
                    if(xmldoc.SelectSingleNode("//GetRoomProfile/Images/Map1Image") != null)
                        imgData = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Map1Image").InnerText.Trim();
                    if (!string.IsNullOrEmpty(imgData))
                    {
                        MapText = "<a href='" + imgData + Map1 + "' target='_blank' onMouseOver=\"toggleDiv('Mapdiv" + RoomCnt.ToString() + "1',1);\" onMouseOut=\"toggleDiv('Mapdiv" + RoomCnt.ToString() + "1',0);\" >Map 1</a>";
                        MapText += "<div class='hdiv' id='Mapdiv" + RoomCnt.ToString() + "1'><img src='"+imgData+ Map1 + "'/></div>";
                    }
                }
                string Map2 = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Map2").InnerText.Trim();
                if (Map2.Length > 0 && Map1.Length > 0)
                {
                     imgData = "";
                    if (xmldoc.SelectSingleNode("//GetRoomProfile/Images/Map2Image") != null)
                        imgData = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Map2Image").InnerText.Trim();
                    if (!string.IsNullOrEmpty(imgData))
                    {

                        MapText += ", <a href='" + imgData + Map2 + "' target='_blank' onMouseOver=\"toggleDiv('Mapdiv" + RoomCnt.ToString() + "2',1);\" onMouseOut=\"toggleDiv('Mapdiv" + RoomCnt.ToString() + "2',0);\" >Map 2</a>";
                        MapText += "<div class='hdiv' id='Mapdiv" + RoomCnt.ToString() + "2'><img src='" + imgData  + Map2 + "'/></div>";
                    }
                }
                else if (Map2.Length > 0 && Map1.Length < 1)
                {
                    imgData = "";
                    if (xmldoc.SelectSingleNode("//GetRoomProfile/Images/Map2Image") != null)
                        imgData = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Map2Image").InnerText.Trim();
                    if (!string.IsNullOrEmpty(imgData))
                    {
                        MapText += "<a href='" + imgData + Map2 + "' target='_blank' onMouseOver=\"toggleDiv('Mapdiv" + RoomCnt.ToString() + "2',1);\" onMouseOut=\"toggleDiv('Mapdiv" + RoomCnt.ToString() + "2',0);\" >Map 2</a>";
                        MapText += "<div class='hdiv' id='Mapdiv" + RoomCnt.ToString() + "2'><img src='" + imgData  +Map2 + "'/></div>";
                    }
                }
                drRD["Map1"] = MapText; 

                /*String Security1 = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security1").InnerText.Trim();
                String SecurityText = "";
                if (Security1.Length > 0)
                {
                    //Image Project START
                    String imgData = "";
                    String pathName = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\image\\SecurityBadge\\" + Session["userID"].ToString(); //FB 1830

                    if (xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security1Image") != null)
                        imgData = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security1Image").InnerText.Trim();

                    if (!Directory.Exists(pathName))
                        System.IO.Directory.CreateDirectory(pathName);

                    Security1 = Security1 + ".jpg";

                    pathName = pathName + "\\" + Security1;

                    if (File.Exists(pathName ))
                        File.Delete(pathName);

                    imgArray = imageUtilObj.ConvertBase64ToByteArray(imgData);
                    WriteToFile((pathName), ref imgArray);

                    //Image Project END
                    //SecurityText = "<a href='../image/SecurityBadge/" + Session["userID"].ToString() + "/" + Security1 + "' target='_blank' onMouseOver=\"toggleDiv('Secdiv" + RoomCnt.ToString() + "1',1);\" onMouseOut=\"toggleDiv('Secdiv" + RoomCnt.ToString() + "1',0);\" >" + Security1 + "</a>";//FB 2136
                    //SecurityText += "<div class='hdiv' id='Secdiv" + RoomCnt.ToString() + "1'><img src='../image/SecurityBadge/" + Session["userID"].ToString() + "/" + Security1 + "'/></div>"; //FB 2136
                }*/

                /* //FB 2136 start
                String Security2 = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security2").InnerText.Trim();
                if (Security2.Length > 0 && Security1.Length > 0)
                {
                    //Image Project START
                    String imgData = "";
                    String pathName = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\upload\\"; //FB 1830

                    if (xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security2Image") != null)
                        imgData = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security2Image").InnerText.Trim();

                    if (!File.Exists(pathName + Security2))
                    {
                        imgArray = imageUtilObj.ConvertBase64ToByteArray(imgData);
                        WriteToFile((pathName + Security2), ref imgArray);
                    }

                    //Image Project END
                    SecurityText += ", <a href='../en/upload/" + Security2 + "' target='_blank' onMouseOver=\"toggleDiv('Secdiv" + RoomCnt.ToString() + "2',1);\" onMouseOut=\"toggleDiv('Secdiv" + RoomCnt.ToString() + "2',0);\" >Security 2</a> ";
                    SecurityText += "<div class='hdiv' id='Secdiv" + RoomCnt.ToString() + "2'><img src='../en/upload/" + Security2 + "'/></div>";
                }
                else if (Security2.Length > 0 && Security1.Length < 1)
                {

                    //Image Project START
                    String imgData = "";
                    String pathName = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\upload\\"; //FB 1830

                    if (xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security2Image") != null)
                        imgData = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security2Image").InnerText.Trim();

                    if (!File.Exists(pathName + Map2))
                    {
                        imgArray = imageUtilObj.ConvertBase64ToByteArray(imgData);
                        WriteToFile((pathName + Security2), ref imgArray);
                    }
                    //Image Project END

                    SecurityText = "<a href='../en/upload/" + Security2 + "' target='_blank' onMouseOver=\"toggleDiv('Secdiv" + RoomCnt.ToString() + "2',1);\" onMouseOut=\"toggleDiv('Secdiv" + RoomCnt.ToString() + "2',0);\" >Security 2</a> ";
                    SecurityText += "<div class='hdiv' id='Secdiv" + RoomCnt.ToString() + "2'><img src='../en/upload/" + Security2 + "'/></div>";
                }*/ //FB 2136 end

                //drRD["Security1"] = SecurityText;

                string Misc1 = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Misc1").InnerText.Trim();
                string MiscText = "";
                if (Misc1.Length > 0)
                {
                   
                     imgData = "";
                    if (xmldoc.SelectSingleNode("//GetRoomProfile/Images/Misc1Image") != null)
                        imgData = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Misc1Image").InnerText.Trim();
                    if (!string.IsNullOrEmpty(imgData))
                    {
                        MiscText = "<a href='" + imgData + Misc1 + "' target='_blank' onMouseOver=\"toggleDiv('Attdiv" + RoomCnt.ToString() + "1',1);\" onMouseOut=\"toggleDiv('Attdiv" + RoomCnt.ToString() + "1',0);\" >Misc. Attachments 1</a>";
                        MiscText += "<div class='hdiv' id='Attdiv" + RoomCnt.ToString() + "1'><img src='" + imgData + Misc1 + "'/></div>";
                    }
                }
                String Misc2 = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Misc2").InnerText.Trim();
                if (Misc2.Length > 0 && Misc2.Length > 0)
                {
                     imgData = "";
                    if (xmldoc.SelectSingleNode("//GetRoomProfile/Images/Misc2Image") != null)
                        imgData = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Misc2Image").InnerText.Trim();
                    if (!string.IsNullOrEmpty(imgData))
                    {
                        MiscText += ", <a href='" + imgData + Misc2 + "' target='_blank' onMouseOver=\"toggleDiv('Attdiv" + RoomCnt.ToString() + "2',1);\" onMouseOut=\"toggleDiv('Attdiv" + RoomCnt.ToString() + "2',0);\" >Misc. Attachments 2</a>";
                        MiscText += "<div class='hdiv' id='Attdiv" + RoomCnt.ToString() + "2'><img src='" + imgData + Misc2 + "'/></div>";
                    }
                }
                else if (Misc2.Length > 0 && Misc2.Length < 1)
                {
                    imgData = "";
                    if (xmldoc.SelectSingleNode("//GetRoomProfile/Images/Misc2Image") != null)
                        imgData = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Misc2Image").InnerText.Trim();
                    if (!string.IsNullOrEmpty(imgData))
                    {
                        MiscText += "<a href='" + imgData + Misc2 + "' target='_blank' onMouseOver=\"toggleDiv('Attdiv" + RoomCnt.ToString() + "2',1);\" onMouseOut=\"toggleDiv('Attdiv" + RoomCnt.ToString() + "2',0);\" >Misc. Attachments 2</a>";
                        MiscText += "<div class='hdiv' id='Attdiv" + RoomCnt.ToString() + "2'><img src='" + imgData + Misc2 + "'/></div>";
                    }
                }

                drRD["Misc1"] = MiscText; //ZD 103569  End 

                String Approver1 = xmldoc.SelectSingleNode("//GetRoomProfile/Approvers/Approver1Name").InnerText.Trim();
                String Approver2 = xmldoc.SelectSingleNode("//GetRoomProfile/Approvers/Approver2Name").InnerText.Trim();
                if(Approver1.Length > 0 && Approver2.Length > 0)
                    Approver2 = Approver1 + ", " + Approver2;
                else if(Approver1.Length > 0) //Added to Handle Approver
                    Approver2 = Approver1;//Added to Handle Approver

                String Approver3 = xmldoc.SelectSingleNode("//GetRoomProfile/Approvers/Approver3Name").InnerText.Trim();
                if (Approver2.Length > 0 && Approver3.Length > 0)
                    Approver3 = Approver2 + ", " + Approver3;
                else if (Approver2.Length > 0 && Approver3.Length < 1)
                    Approver3 = Approver2;

                drRD["Approver1Name"] = Approver3;

                String Projector = xmldoc.SelectSingleNode("//GetRoomProfile/Projector").InnerText.Trim();
                if (Projector == "1")
                    drRD["Projector"] = obj.GetTranslatedText("Yes");
                else
                    drRD["Projector"] = obj.GetTranslatedText("No");

                String Tier = xmldoc.SelectSingleNode("//GetRoomProfile/Tier1Name").InnerText.Trim();
                Tier += " > " + xmldoc.SelectSingleNode("//GetRoomProfile/Tier2Name").InnerText.Trim();
                drRD["Tier"] = Tier;
                String Caterer = xmldoc.SelectSingleNode("//GetRoomProfile/CatererFacility").InnerText.Trim();
                if (Caterer == "1")
                    Caterer = obj.GetTranslatedText("Yes");
                else
                    Caterer = obj.GetTranslatedText("No");

                drRD["CatererFacility"] = Caterer;

                String endPointID = xmldoc.SelectSingleNode("//GetRoomProfile/EndpointID").InnerText.Trim();
                String endPointName = xmldoc.SelectSingleNode("//GetRoomProfile/EndpointName").InnerText.Trim();
                String endPointAddr = xmldoc.SelectSingleNode("//GetRoomProfile/EndpointIP").InnerText.Trim();
                String eptText = "";

                //FB 2400 start
                String adds = "", MulAdds="";
                XmlNodeList nodes = null;
                nodes = xmldoc.SelectNodes("//GetRoomProfile/MultiCodec");
                foreach (XmlNode node1 in nodes)
                {
                    adds = "<table>";
                    XmlNodeList nodess = node1.SelectNodes("Address");
                    foreach (XmlNode snode in nodess)
                        adds += "<tr><td>" + snode.InnerText + "</td></tr>";

                    adds += "</table>";
                }

                if (xmldoc.SelectSingleNode("//GetRoomProfile/isTelePresEndPoint") != null)
                {
                    if (xmldoc.SelectSingleNode("//GetRoomProfile/isTelePresEndPoint").InnerText.Trim() == "1")
                    {
                        MulAdds = "<a style='color:Blue' target='_blank' onMouseOver=\"ShowHide('multiCodecPopUp',1);\" onMouseOut=\"ShowHide('multiCodecPopUp',0);\">"+ obj.GetTranslatedText("More") +" ...</a>";
                        multicodec.Text += "<div id='multiCodecAdd'>" + adds + "</div>";
                    }
                }

                if (endPointName.Trim().Length > 0)
                    eptText = "<a href='javascript:void(0);' onclick=javascript:viewEndpoint('" + endPointID + "');>" + endPointName + "</a> (" + endPointAddr + ")" + MulAdds;
                drRD["Endpoint"] = eptText;

                nodes = xmldoc.SelectNodes("//GetRoomProfile/Departments/Department"); //FB 2400 End
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                String Departments = "";
                foreach (DataTable dt in ds.Tables)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (Departments.Trim() == "")
                            Departments += dr["Name"].ToString().Trim();
                        else
                            Departments += ", " + dr["Name"].ToString().Trim();
                    }
                }
                drRD["Departments"] = Departments.Trim();

                //FB 2448 start
                drRD["IsVMR"] = "0";
                
                if (xmldoc.SelectSingleNode("//GetRoomProfile/IsVMR") != null)
                    drRD["IsVMR"] = xmldoc.SelectSingleNode("//GetRoomProfile/IsVMR").InnerText.Trim();

                drRD["InternalNumber"] = "";
                if (xmldoc.SelectSingleNode("//GetRoomProfile/InternalNumber") != null)
                    drRD["InternalNumber"] = xmldoc.SelectSingleNode("//GetRoomProfile/InternalNumber").InnerText.Trim();

                drRD["ExternalNumber"] = "";
                if (xmldoc.SelectSingleNode("//GetRoomProfile/ExternalNumber") != null)
                    drRD["ExternalNumber"] = xmldoc.SelectSingleNode("//GetRoomProfile/ExternalNumber").InnerText.Trim();

                isVMR = false;
                if (drRD["IsVMR"].ToString().Trim() == "1")
                    isVMR = true;

                //FB 2448 end
				 //FB 2694 Start
                drRD["RoomCategory"] = "0";

                if (xmldoc.SelectSingleNode("//GetRoomProfile/RoomCategory") != null)
                    drRD["RoomCategory"] = xmldoc.SelectSingleNode("//GetRoomProfile/RoomCategory").InnerText.Trim();
                
                RoomCategory = false;
                if (drRD["RoomCategory"].ToString().Trim() != "4")
                    RoomCategory = true;
                //ALLBUGS-23
                if (drRD["RoomCategory"].ToString().Trim() == "4")
                    isHotdeskingRoom = true;
				 //FB 2694 End
                dtRD.Rows.Add(drRD);
                RoomCnt++;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BuildRoomDetails(DataTable dtRoomDetails)
        {
            HtmlTableCell td = null;
            Int32 i = 0;
            try
            {
                if (dtRoomDetails != null)
                {
                    foreach (DataRow dr in dtRoomDetails.Rows)
                    {
                        if (i < 1)
                        {
                            td = new HtmlTableCell();
                            td.Attributes.Add("class", "tableHeader");
                            td.Width = Unit.Percentage(23).ToString();
                            td.InnerHtml = obj.GetTranslatedText("Room Name"); //ZD 100425
                            //td.InnerHtml = "&nbsp;";
                            trRoomName.Cells.Add(td);
                            //window dressing start
                            if (isVMR)
                            {
                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Assistant-In-Charge") + "</b></font>";
                                trAIC.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("InternalNumber") + "</b></font>";
                                trExternalNumber.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("ExternalNumber") + "</b></font>";
                                trInternalNumber.Cells.Add(td);

                                //FB 2994 Start //ZD 100522
                                //td = new HtmlTableCell();
                                //td.Attributes.Add("class", "tableHeader");
                                //td.Align = "left";
                                //td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Room Images") + "</b></font>";
                                //trRImg.Cells.Add(td);
                                //FB 2994 End
                            }
                            else
                            {

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Top > Middle Tier") + "</b></font>";
                                trTier.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Floor / Room Number") + "</b></font>";
                                trFloorRoom.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Address and Directions") + "</b></font>"; //Maps
                                trAddress.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Parking Directions") + "</b></font>";
                                trPDir.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Map Link") + "</b></font>";
                                trMapLink.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Time Zone") + "</b></font>";
                                trTz.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Room Phone Number") + "</b></font>";
                                trPhoneNo.Cells.Add(td);
								 //FB 2694 Start
                                //if (RoomCategory)//ZD 104844
                                //{
                                    td = new HtmlTableCell();
                                    td.Attributes.Add("class", "tableHeader");
                                    td.Align = "left";
                                    td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Maximum Capacity") + "</b></font>";
                                    trCapacity.Cells.Add(td);
									//ZD 101563
                                    //td = new HtmlTableCell();
                                    //td.Attributes.Add("class", "tableHeader");
                                    //td.Align = "left";
                                    //td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Setup Time Buffer") + "</b></font>";
                                    //trSTB.Cells.Add(td);

                                    //td = new HtmlTableCell();
                                    //td.Attributes.Add("class", "tableHeader");
                                    //td.Align = "left";
                                    //td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("TearDown Time Buffer") + "</b></font>";
                                    //trTTB.Cells.Add(td);
                                //}
                                    //FB 2694 End //ZD 104844
                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Projector Available") + "</b></font>";
                                trProjector.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Room Images") + "</b></font>";
                                trRImg.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Catering Menus") + "</b></font>"; // FB 2570
                                trCF.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Assistant-In-Charge") + "</b></font>";//FB 2974
                                trAIC.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Map Images") + "</b></font>";
                                trMap1.Cells.Add(td);
								 //FB 2694 Start
                                if (RoomCategory)
                                {
                                    //td = new HtmlTableCell();
                                    //td.Attributes.Add("class", "tableHeader");
                                    //td.Align = "left";
                                    //td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Security Images") + "</b></font>";
                                    //trSecurity1.Cells.Add(td);


                                    td = new HtmlTableCell();
                                    td.Attributes.Add("class", "tableHeader");
                                    td.Align = "left";
                                    td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Misc. Attachments") + "</b></font>";
                                    trMisc1.Cells.Add(td);
                                }
								 //FB 2694 End
                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Approvers") + "</b></font>";
                                trPriAppr.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Endpoint") + "</b></font>";
                                trEPT.Cells.Add(td);

                                td = new HtmlTableCell();
                                td.Attributes.Add("class", "tableHeader");
                                td.Align = "left";
                                td.InnerHtml = "<font face='verdana' size='2'><b>" + obj.GetTranslatedText("Departments") + "</b></font>";
                                trDept.Cells.Add(td);
                            }
                        }
                        //Maps Changes - Start
                        td = new HtmlTableCell();
                        td.Align = "Center";
                        td.Attributes.Add("class", "tableHeader");
                        td.ColSpan = 2;
                        //Window Dressing
                        td.InnerHtml = "<font face='verdana' size='2'>" + dr["RoomName"].ToString().Trim() + "</font>";
                        trRoomName.Cells.Add(td);

                        if (isVMR)
                        {
                            #region NonVMRFieldsdisplay
                            trTier.Style.Value = "display:none;";
                            trFloorRoom.Style.Value = "display:none;";
                            trAddress.Style.Value = "display:none;";
                            trAddress.Style.Value = "display:none;";
                            trPDir.Style.Value = "display:none;";
                            trMapLink.Style.Value = "display:none;";
                            trTz.Style.Value = "display:none;";
                            trPhoneNo.Style.Value = "display:none;";
                            trCapacity.Style.Value = "display:none;";
                            //trSTB.Style.Value = "display:none;";//ZD 101563
                            //trTTB.Style.Value = "display:none;";
                            trProjector.Style.Value = "display:none;";
                            trRImg.Style.Value = "display:none;"; //FB 2994 //ZD 100522
                            trCF.Style.Value = "display:none;";
                            trMap1.Style.Value = "display:none;";
                            trSecurity1.Style.Value = "display:none;";
                            trMisc1.Style.Value = "display:none;";
                            trPriAppr.Style.Value = "display:none;";
                            trEPT.Style.Value = "display:none;";
                            trDept.Style.Value = "display:none;";
                            #endregion

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "#f1f1f1";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["AssistantInchargeName"].ToString().Trim() + "</font>";
                            trAIC.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "white";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["InternalNumber"].ToString().Trim() + "</font>";
                            trExternalNumber.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "white";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["ExternalNumber"].ToString().Trim() + "</font>";
                            trInternalNumber.Cells.Add(td);

                            //FB 2994 Start
                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "#f1f1f1";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = dr["RoomText"].ToString().Trim();
                            trRImg.Cells.Add(td);
                            //FB 2994 End
                        }
                        else
                        {
							 //FB 2694 Start
                            if (!RoomCategory)
                            {
                                //trCapacity.Style.Value = "display:none;";//ZD 104844
                                //trSTB.Style.Value = "display:none;";//ZD 101563
                                //trTTB.Style.Value = "display:none;";
                                trSecurity1.Style.Value = "display:none;";
                                trMisc1.Style.Value = "display:none;";
                            }
							 //FB 2694 End
                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "white";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["Tier"].ToString().Trim() + "</font>";
                            trTier.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "#f1f1f1";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["RoomFloor"].ToString().Trim() + "</font>";
                            trFloorRoom.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            // td.BgColor = "white";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["Address"].ToString().Trim() + "</font>";
                            trAddress.Cells.Add(td);
                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.NoWrap = true;
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["GooleMap"].ToString().Trim() + "</font>";
                            td.InnerHtml += "&nbsp;&nbsp;<font face='verdana' size='2' >" + dr["YahooMap"].ToString().Trim() + "</font>";
                            td.InnerHtml += "&nbsp;&nbsp;<font face='verdana' size='2' >" + dr["BingMap"].ToString().Trim() + "</font>";
                            trAddress.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "#f1f1f1";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = dr["ParkingDirections"].ToString().Trim();
                            trPDir.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "white";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = dr["MapLink"].ToString().Trim();
                            trMapLink.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "#f1f1f1";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["TimezoneName"].ToString().Trim() + "</font>";
                            trTz.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "white";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["RoomPhoneNumber"].ToString().Trim() + "</font>";
                            trPhoneNo.Cells.Add(td);
							 //FB 2694 Start
                            //if (RoomCategory) //ZD 104844 
                            //{
                                td = new HtmlTableCell();
                                td.Align = "left";
                                td.VAlign = "top";
                                td.ColSpan = 2;
                                //td.BgColor = "#f1f1f1";
                                td.Attributes.Add("class", "tableBody");
                                td.InnerHtml = "<font face='verdana' size='2' >" + dr["MaximumCapacity"].ToString().Trim() + "</font>";
                                trCapacity.Cells.Add(td);
								//ZD 101563
                                //td = new HtmlTableCell();
                                //td.Align = "left";
                                //td.VAlign = "top";
                                //td.ColSpan = 2;
                                ////td.BgColor = "white";
                                //td.Attributes.Add("class", "tableBody");
                                //td.InnerHtml = "<font face='verdana' size='2' >" + dr["SetupTime"].ToString().Trim() + "</font>";
                                //trSTB.Cells.Add(td);

                                //td = new HtmlTableCell();
                                //td.Align = "left";
                                //td.VAlign = "top";
                                //td.ColSpan = 2;
                                ////td.BgColor = "#f1f1f1";
                                //td.Attributes.Add("class", "tableBody");
                                //td.InnerHtml = "<font face='verdana' size='2' >" + dr["TeardownTime"].ToString().Trim() + "</font>";
                                //trTTB.Cells.Add(td);
                                //} //ZD 104844
							 //FB 2694 End
                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "white";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + obj.GetTranslatedText(dr["Projector"].ToString().Trim()) + "</font>";
                            trProjector.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "#f1f1f1";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = dr["RoomText"].ToString().Trim();
                            trRImg.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "white";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + obj.GetTranslatedText(dr["CatererFacility"].ToString().Trim()) + "</font>";
                            trCF.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "#f1f1f1";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["AssistantInchargeName"].ToString().Trim() + "</font>";
                            trAIC.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "white";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["Map1"].ToString().Trim() + "</font>";
                            trMap1.Cells.Add(td);
							 //FB 2694 Start
                            if (RoomCategory)
                            {
                                //td = new HtmlTableCell();
                                //td.Align = "left";
                                //td.VAlign = "top";
                                //td.ColSpan = 2;
                                ////td.BgColor = "white";
                                //td.Attributes.Add("class", "tableBody");
                                //td.InnerHtml = "<font face='verdana' size='2' >" + dr["Security1"].ToString().Trim() + "</font>";
                                //trSecurity1.Cells.Add(td);


                                td = new HtmlTableCell();
                                td.Align = "left";
                                td.VAlign = "top";
                                td.ColSpan = 2;
                                //td.BgColor = "white";
                                td.Attributes.Add("class", "tableBody");
                                td.InnerHtml = "<font face='verdana' size='2' >" + dr["Misc1"].ToString().Trim() + "</font>";
                                trMisc1.Cells.Add(td);
                            }
							 //FB 2694 End
                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "white";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["Approver1Name"].ToString().Trim() + "</font>";
                            trPriAppr.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "#f1f1f1";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = dr["Endpoint"].ToString().Trim();
                            trEPT.Cells.Add(td);

                            td = new HtmlTableCell();
                            td.Align = "left";
                            td.VAlign = "top";
                            td.ColSpan = 2;
                            //td.BgColor = "white";
                            td.Attributes.Add("class", "tableBody");
                            td.InnerHtml = "<font face='verdana' size='2' >" + dr["Departments"].ToString().Trim() + "</font>";
                            trDept.Cells.Add(td);

                        }
                        i++;
                        //window dressing end
                        //Maps Changes - end

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindData()
        {
            String[] arrRoomIds = null;
            DataTable dtRoomDetails = null; 
            try
            {
                
                dtRoomDetails = BuildTableSchema();
                if (RoomId.Trim().IndexOf(',') > -1)
                {
                    arrRoomIds = RoomId.Trim().Split(',');
                    foreach (String rmID in arrRoomIds)
                    {
                        if(rmID.Trim().Length > 0)
                            GetRoomProfileXML(rmID.Trim(), ref dtRoomDetails);
                    }
                }
                else
                {
                    
                    GetRoomProfileXML(RoomId.Trim(), ref dtRoomDetails);
                }
                BuildRoomDetails(dtRoomDetails);
                if (RoomCnt > 1)
                //Added for FB 1428 Start
                {
                    if (Application["Client"].ToString().ToUpper() == "MOJ")
                        lblHeading.Text = "Compare Hearing Room Resource";
                    else
                        //Added for FB 1428 End
                        lblHeading.Text = obj.GetTranslatedText("Compare Conference Room Resource");//FB 1830 - Translation
                }//Added for FB 1428
                else
                //Added for FB 1428 Start
                {
                    if (Application["Client"].ToString().ToUpper() == "MOJ")
                        lblHeading.Text = "Hearing Room Resource";
                    else if (isHotdeskingRoom)//ALLBUGS-23
                        lblHeading.Text = obj.GetTranslatedText("Hotdesking Room Resource");
                    else
                        //Added for FB 1428 End
                        lblHeading.Text = obj.GetTranslatedText("Conference Room Resource");//FB 1830 - Translation
                }//Added for FB 1428
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region LoadRoomImageGrid - Added for Image Project Edit Mode

        private String LoadRoomImageGrid(XmlNodeList rmImgNodes)
        {
            string roomText ="";
            string rImage ="";
            int RoomCnt =0, i =0;
            string ImagePath = ""; //ImageID = ""; //byte[] myData; string imageString = "";string errMsg = ""; string imgData = "";//ZD 101611
            try
            {
                string fName;
                string fileext = "";
                string pathName = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en/image/room/"; //FB 1830
                string phyPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\"; //FB 1830
                foreach (XmlNode xnode in rmImgNodes)
                {
                    RoomCnt++;
                   
                    fName = xnode.SelectSingleNode("ImageName").InnerText;
                    fileext = xnode.SelectSingleNode("Imagetype").InnerText;
                    //imgData = xnode.SelectSingleNode("Image").InnerText;
                    ImagePath = xnode.SelectSingleNode("ImageWebPath").InnerText;
                    //ZD 103569 start
                    //imgArray = imageUtilObj.ConvertBase64ToByteArray(imgData);
                    //if (imgArray != null) //FB 1598
                    //{
                    //    if (File.Exists((phyPath + fName)))
                    //        File.Delete((phyPath + fName));

                    //    //ZD 101611 start
                    //    System.Drawing.Image image = System.Drawing.Image.FromStream(new System.IO.MemoryStream(imgArray));
                    //    if (image.Width > 250 || image.Height > 250)
                    //    {
                    //        int roomImgWidth = 250;
                    //        int roomImgHeight = 250;
                    //        myData = obj.Imageresize(image.Width, image.Height, roomImgWidth, roomImgHeight, image);
                    //        imageString = imageUtilObj.ConvertByteArrToBase64(myData);
                    //        SetRoomImage(imageString,ImageID);
                    //        WriteToFile((phyPath + fName), ref myData);
                    //    }
                    //    else
                    //        WriteToFile((phyPath + fName), ref imgArray);
                    //    //ZD 101611 start
                    //}
                    rImage = "";
                    if (fName != "[None]" && !string.IsNullOrEmpty(ImagePath)) // FB 1200
                    {
                       // rImage = pathName + fName;//ZD 103569
                        rImage = ImagePath+ fName;
                        if (roomText == "")
                        {
                            roomText = "<a href='" + rImage + "' target='_blank' onMouseOver=\"toggleDiv('roomdiv" + RoomCnt.ToString() + i.ToString() + "',1);\" onMouseOut=\"toggleDiv('roomdiv" + RoomCnt.ToString() + i.ToString() + "',0);\" >" + fName + "</a>";
                            roomText += "<div class='hdiv' id='roomdiv" + RoomCnt.ToString() + i.ToString() + "'><img src='" + rImage + "'/></div>";
                        }
                        else
                        {
                            roomText += ", " + "<a href='" + rImage + "' target='_blank' onMouseOver=\"toggleDiv('roomdiv" + RoomCnt.ToString() + i.ToString() + "',1);\" onMouseOut=\"toggleDiv('roomdiv" + RoomCnt.ToString() + i.ToString() + "',0);\" >" + fName + "</a>";
                            roomText += "<div class='hdiv' id='roomdiv" + RoomCnt.ToString() + i.ToString() + "'><img src='" + rImage + "'/></div>";
                        }
                        i++;
                    } 
                    //ZD 103569 End
                }

            }
            catch (Exception ex)
            {
                log.Trace("LoadRoomProfile: " + ex.Message + " : " + ex.StackTrace);
            }

            return roomText;
        }
        #endregion

        #region WriteToFile
        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //ZD 101611 start
        #region SetRoomImage
        /// <summary>
        /// SetRoomImage
        /// </summary>
        /// <param name="imageString"></param>
        /// <param name="fname"></param>
        /// <param name="ImageID"></param>
        protected void SetRoomImage(string imageString,string ImageID)
        {
           StringBuilder inXML = new StringBuilder();
            try
            {
                inXML.Append("<SetRoomImage>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<ImageID>" + ImageID + "</ImageID>");
                inXML.Append("<ActualImage>" + imageString + "</ActualImage>");
                inXML.Append("</SetRoomImage>");
                String outxml = obj.CallMyVRMServer("setRoomResizeImage", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetRoomProfile Outxml: " + outxml);
            }
            catch (Exception ex)
            {
                log.Trace("SetRoomImage: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 101611 End


    }
}


#region Commented 
//String inXML = "<GetRoomProfile>";
//inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
//inXML += "  <RoomID>" + RoomId + "</RoomID>";
//inXML += "</GetRoomProfile>";


//log.Trace("GetRoomProfile inxml: " + inXML);
//String outXML = obj.CallMyVRMServer("GetRoomProfile", inXML, Application["MyVRMServer_ConfigPath"].ToString());
////outXML = "<GetRoomProfile><RoomID>74</RoomID><RoomName>ROOMWithAppr74</RoomName><RoomPhoneNumber>521-524-5215</RoomPhoneNumber><MaximumCapacity>5</MaximumCapacity><MaximumConcurrentPhoneCalls>10</MaximumConcurrentPhoneCalls><SetupTime>20</SetupTime><TeardownTime>15</TeardownTime><AssistantInchargeID>297</AssistantInchargeID><AssistantInchargeName>room assistant</AssistantInchargeName><MultipleAssistantEmails>ROOMWithApprAsst11@myvrm.com;ROOMWithApprAsst12@myvrm.com</MultipleAssistantEmails><Tier1ID>5</Tier1ID><Tier1Name>Greenville</Tier1Name><Tier2ID>47</Tier2ID><Tier2Name>Other</Tier2Name><CatererFacility>1</CatererFacility><DynamicRoomLayout>1</DynamicRoomLayout><Projector>1</Projector><Video>1</Video><Floor>12</Floor><RoomNumber>16</RoomNumber><StreetAddress1>Test Street Address1</StreetAddress1><StreetAddress2>Test Street Address2</StreetAddress2><City>City</City><State>5</State><StateName>CA</StateName><ZipCode>11590</ZipCode><Country>225</Country><CountryName>United States</CountryName><MapLink>Maplink</MapLink><ParkingDirections>Parking Directions</ParkingDirections><AdditionalComments></AdditionalComments><TimezoneID>2</TimezoneID><TimezoneName>Alaska</TimezoneName><Longitude>37.0625</Longitude><Latitude>-95.677068</Latitude><Images><Map1></Map1><Map2></Map2><Security1></Security1><Security2></Security2><Misc1></Misc1><Misc2></Misc2></Images><Approvers><Approver1ID>305</Approver1ID><Approver1Name>room approver  </Approver1Name><Approver2ID>300</Approver2ID><Approver2Name>Saima Ahmad  </Approver2Name><Approver3ID>11</Approver3ID><Approver3Name>VRM Administrator  </Approver3Name></Approvers><EndpointID>46</EndpointID><EndpointName>SCC LRC 12</EndpointName><EndpointIP>167.7.144.169</EndpointIP><RoomImage>room1, room2</RoomImage><Custom1></Custom1><Custom2></Custom2><Custom3></Custom3><Custom4></Custom4><Custom5></Custom5><Custom6></Custom6><Custom7></Custom7><Custom8></Custom8><Custom9></Custom9><Custom10></Custom10><Departments><Department><ID>2</ID><Name>Hershey Med</Name><SecurityKey></SecurityKey></Department><Department><ID>3</ID><Name>Coop Extension </Name><SecurityKey></SecurityKey></Department><Department><ID>4</ID><Name>PSU General University Access</Name><SecurityKey></SecurityKey></Department></Departments></GetRoomProfile>";
//log.Trace("GetRoomProfile outxml: " + outXML);
//XmlDocument xmldoc = new XmlDocument();
//xmldoc.LoadXml(outXML);

//lblRoomName.Text = xmldoc.SelectSingleNode("//GetRoomProfile/RoomName").InnerText.Trim();
//lblPhoneNo.Text = xmldoc.SelectSingleNode("//GetRoomProfile/RoomPhoneNumber").InnerText.Trim();
//lblFlRoom.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Floor").InnerText.Trim() + " / " + xmldoc.SelectSingleNode("//GetRoomProfile/RoomNumber").InnerText.Trim();
//lblCapacity.Text = xmldoc.SelectSingleNode("//GetRoomProfile/MaximumCapacity").InnerText.Trim();
//lblSTB.Text = xmldoc.SelectSingleNode("//GetRoomProfile/SetupTime").InnerText.Trim();
//lblTTB.Text = xmldoc.SelectSingleNode("//GetRoomProfile/TeardownTime").InnerText.Trim();
//lblAssInCharge.Text = xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeName").InnerText.Trim();
//String Address = "";
//if (xmldoc.SelectSingleNode("//GetRoomProfile/StreetAddress1").InnerText.Trim().Length > 0)
//    Address += xmldoc.SelectSingleNode("//GetRoomProfile/StreetAddress1").InnerText.Trim() + ", ";
//if(xmldoc.SelectSingleNode("//GetRoomProfile/StreetAddress2").InnerText.Trim().Length > 0)
//    Address += xmldoc.SelectSingleNode("//GetRoomProfile/StreetAddress2").InnerText.Trim() + ", ";
//if (xmldoc.SelectSingleNode("//GetRoomProfile/City").InnerText.Trim().Length > 0)
//    Address += xmldoc.SelectSingleNode("//GetRoomProfile/City").InnerText.Trim() + ", ";
//if (xmldoc.SelectSingleNode("//GetRoomProfile/StateName").InnerText.Trim().Length > 0)
//    Address += xmldoc.SelectSingleNode("//GetRoomProfile/StateName").InnerText.Trim() + ", ";
//if (xmldoc.SelectSingleNode("//GetRoomProfile/CountryName").InnerText.Trim().Length > 0)
//    Address += xmldoc.SelectSingleNode("//GetRoomProfile/CountryName").InnerText.Trim() + ", ";
//if (xmldoc.SelectSingleNode("//GetRoomProfile/ZipCode").InnerText.Trim().Length > 0)
//    Address += xmldoc.SelectSingleNode("//GetRoomProfile/ZipCode").InnerText.Trim();
//lblAddress.Text = Address;
//lblPDir.Text = xmldoc.SelectSingleNode("//GetRoomProfile/ParkingDirections").InnerText.Trim();
//lblMapLink.Text = xmldoc.SelectSingleNode("//GetRoomProfile/MapLink").InnerText.Trim();
//lblTZ.Text = xmldoc.SelectSingleNode("//GetRoomProfile/TimezoneName").InnerText.Trim();

//String RoomImage = xmldoc.SelectSingleNode("//GetRoomProfile/RoomImage").InnerText.Trim();
//String rImage = "";
//String roomText = "";
//if (RoomImage.Trim().Length > 0)
//{
//    if (RoomImage.IndexOf(',') > 0)
//    {
//        String[] arrRmImages = RoomImage.Split(',');
//        foreach (String strImage in arrRmImages)
//        {
//            rImage = strImage.Trim() + ".jpg";
//            if (roomText == "")
//                roomText = "<a href='image/room/" + rImage + "' target='_blank'>" + rImage + "</a>";
//            else
//                roomText += ", " + "<a href='image/room/" + rImage + "' target='_blank'>" + rImage + "</a>";
//        }
//    }
//}
//tdRoomImg.InnerHtml = roomText;
//lblMap1.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Map1").InnerText.Trim();
//lblMap2.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Map2").InnerText.Trim();
//lblSecurity1.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security1").InnerText.Trim();
//lblSecurity2.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security2").InnerText.Trim();
//lblMiscAttach1.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Misc1").InnerText.Trim();
//lblMiscAttach2.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Misc2").InnerText.Trim();
//lblPrimaryAppr.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Approvers/Approver1Name").InnerText.Trim();
//lblSecAppr1.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Approvers/Approver2Name").InnerText.Trim();
//lblSecAppr2.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Approvers/Approver3Name").InnerText.Trim();
//lblTier.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Tier1Name").InnerText.Trim();
//lblTier.Text += " > " + xmldoc.SelectSingleNode("//GetRoomProfile/Tier2Name").InnerText.Trim();
//lblCatering.Text = xmldoc.SelectSingleNode("//GetRoomProfile/CatererFacility").InnerText.Trim();
//if (lblCatering.Text == "1")
//    lblCatering.Text = "Yes";
//else
//    lblCatering.Text = "No";

//String endPointID = xmldoc.SelectSingleNode("//GetRoomProfile/EndpointID").InnerText.Trim();
//String endPointName = xmldoc.SelectSingleNode("//GetRoomProfile/EndpointName").InnerText.Trim();
//String endPointAddr = xmldoc.SelectSingleNode("//GetRoomProfile/EndpointIP").InnerText.Trim();
//String eptText = "";
//if(endPointName.Trim().Length > 0)
//    eptText = "<a href='javascript:void(0);' onclick=javascript:viewEndpoint('" + endPointID + "');>" + endPointName + "</a> (" + endPointAddr + ")";
//tdEPT.InnerHtml = eptText;

//XmlNodeList nodes = xmldoc.SelectNodes("//GetRoomProfile/Departments/Department");
//XmlTextReader xtr;
//DataSet ds = new DataSet();

//foreach (XmlNode node in nodes)
//{
//    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
//    ds.ReadXml(xtr, XmlReadMode.InferSchema);
//}
//String Departments = "";
//foreach (DataTable dt in ds.Tables)
//{
//    foreach (DataRow dr in dt.Rows)
//    {
//        if(Departments.Trim() == "")
//            Departments += dr["Name"].ToString().Trim();
//        else
//            Departments += ", " + dr["Name"].ToString().Trim();
//    }
//}
//lblDept.Text = Departments.Trim();
#endregion