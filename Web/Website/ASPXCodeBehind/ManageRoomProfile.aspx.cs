/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml;
using System.IO;
using System.Text;
using System.Collections.Specialized;//ZD 100263
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using System.Drawing;


// This class contains definition for all room profile attributes
// The methods in this class contain all operations performed on Room Profile

namespace ns_MyVRM
{
    public partial class Room : System.Web.UI.Page
    {
        #region Private Members

        ns_Logger.Logger log;
        myVRMNet.NETFunctions obj;
        string rmImg = "";
        protected String dtFormatType = "MM/dd/yyyy";
        String tformat = "hh:mm tt";

        String fName; //Image Project
        string fileext;
        String pathName;
        myVRMNet.ImageUtil imageUtilObj = null;
        byte[] imgArray = null;
        DataTable roomImageDt;
        string sessionKey = "";
        protected String language = "";//FB 1830
        //protected Enyim.Caching.MemcachedClient memClient = null;//ZD 103496 //ZD 104482
        //protected int memcacheEnabled = 0;//ZD 103496
        #endregion

        #region protected Members
        protected System.Web.UI.WebControls.Label lblTitle;
        protected System.Web.UI.WebControls.Label errLabel;
        protected System.Web.UI.WebControls.TextBox txtRoomName;
        protected System.Web.UI.WebControls.TextBox txtRoomEmail; // FB 2342
        protected System.Web.UI.WebControls.TextBox txtRoomUID; //ZD 100196
        protected System.Web.UI.WebControls.RequiredFieldValidator reqName;
        protected System.Web.UI.WebControls.TextBox txtRoomPhone;
        protected System.Web.UI.WebControls.TextBox txtMaximumCapacity;
        protected System.Web.UI.WebControls.RangeValidator CapacityValidator;
        protected System.Web.UI.WebControls.TextBox txtMaxConcurrentCalls;
        //protected System.Web.UI.WebControls.TextBox txtSetupTime; //ZD 101563
        //protected System.Web.UI.WebControls.TextBox txtTeardownTime;
        protected System.Web.UI.WebControls.DropDownList lstProjector;
        protected System.Web.UI.WebControls.DropDownList lstVideo;
        protected System.Web.UI.WebControls.DropDownList lstDynamicRoomLayout;
        protected System.Web.UI.WebControls.ListBox lstRoomImage;
        protected System.Web.UI.WebControls.TextBox Assistant;
        protected System.Web.UI.WebControls.RequiredFieldValidator AssistantValidator;
        protected System.Web.UI.WebControls.TextBox txtMultipleAssistant;
        protected System.Web.UI.WebControls.DropDownList CatererList;
        protected System.Web.UI.WebControls.DropDownList lstTopTier;
        protected System.Web.UI.WebControls.DropDownList lstMiddleTier;
        protected System.Web.UI.WebControls.TextBox txtFloor;
        protected System.Web.UI.WebControls.TextBox txtRoomNumber;
        protected System.Web.UI.WebControls.TextBox txtStreetAddress1;
        protected System.Web.UI.WebControls.TextBox txtStreetAddress2;
        protected System.Web.UI.WebControls.TextBox txtCity;
        protected System.Web.UI.WebControls.DropDownList lstCountries;
        protected System.Web.UI.WebControls.DropDownList lstStates;
        protected System.Web.UI.WebControls.TextBox txtZipCode;
        protected System.Web.UI.WebControls.TextBox txtParkingDirections;
        protected System.Web.UI.WebControls.TextBox txtAdditionalComments;
        protected System.Web.UI.WebControls.TextBox txtMapLink;
        protected System.Web.UI.WebControls.DropDownList lstTimezone;
        protected System.Web.UI.WebControls.TextBox txtLongitude;
        protected System.Web.UI.WebControls.TextBox txtLatitude;
        protected System.Web.UI.WebControls.Label lblUploadMap1;
        protected System.Web.UI.WebControls.Button btnRemoveMap1;
        protected System.Web.UI.WebControls.Label hdnUploadMap1;
        protected System.Web.UI.WebControls.Label lblUploadMap2;
        protected System.Web.UI.WebControls.Button btnRemoveMap2;
        protected System.Web.UI.WebControls.Label hdnUploadMap2;
        //protected System.Web.UI.WebControls.Label lblUploadSecurity1; // FB 2136
        //protected System.Web.UI.WebControls.Button btnRemoveSecurity1; // FB 2136
        //protected System.Web.UI.WebControls.Label hdnUploadSecurity1; // FB 2136
        //protected System.Web.UI.WebControls.Label lblUploadSecurity2; // FB 2136
        //protected System.Web.UI.WebControls.Button btnRemoveSecurity2; // FB 2136
        //protected System.Web.UI.WebControls.Label hdnUploadSecurity2; // FB 2136
        protected System.Web.UI.WebControls.Label lblUploadMisc1;
        protected System.Web.UI.WebControls.Button btnRemoveMisc1;
        protected System.Web.UI.WebControls.Label hdnUploadMisc1;
        protected System.Web.UI.WebControls.Label lblUploadMisc2;
        protected System.Web.UI.WebControls.Button btnRemoveMisc2;
        protected System.Web.UI.WebControls.Label hdnUploadMisc2;
        protected System.Web.UI.WebControls.Button btnUploadFiles;
        protected System.Web.UI.WebControls.TextBox Approver0;
        protected System.Web.UI.WebControls.TextBox Approver1;
        protected System.Web.UI.WebControls.TextBox Approver2;
        protected System.Web.UI.WebControls.DropDownList lstEndpoint;
        protected System.Web.UI.WebControls.Button btnEndpointDetails;
        protected System.Web.UI.WebControls.ListBox DepartmentList;
		//ZD 100420
        //protected System.Web.UI.WebControls.Button btnReset;
        protected System.Web.UI.HtmlControls.HtmlButton btnReset;
        protected System.Web.UI.WebControls.Button btnSubmitAddNew;
        //protected System.Web.UI.HtmlControls.HtmlButton btnSubmitAddNew;
        protected System.Web.UI.WebControls.Button btnSubmit;
        //protected System.Web.UI.HtmlControls.HtmlButton btnSubmit;
		//ZD 100420
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnMultipleDept;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRoomID;
        protected System.Web.UI.HtmlControls.HtmlInputFile fleMap1;
        protected System.Web.UI.HtmlControls.HtmlInputFile fleMap2;
        //protected System.Web.UI.HtmlControls.HtmlInputFile fleSecurity1; // FB 2136
        //protected System.Web.UI.HtmlControls.HtmlInputFile fleSecurity2; // FB 2136
        protected System.Web.UI.HtmlControls.HtmlInputFile fleMisc1;
        protected System.Web.UI.HtmlControls.HtmlInputFile fleMisc2;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Approver0ID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Approver1ID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Approver2ID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden AssistantID;
        protected System.Web.UI.HtmlControls.HtmlInputHidden AssistantName;

        /* *** Code added for FB 1425 QA Bug -Start *** */
        protected System.Web.UI.HtmlControls.HtmlTableCell TzTD1;
        protected System.Web.UI.HtmlControls.HtmlTableCell TzTD2;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdntzone;
        protected System.Web.UI.HtmlControls.HtmlGenericControl spnConf;
        /* *** Code added for FB 1425 QA Bug -End *** */
        protected System.Web.UI.WebControls.Label lblMUser;
        protected System.Web.UI.WebControls.Label lblMdate;
        protected System.Web.UI.WebControls.DropDownList DrpHandi;
        protected System.Web.UI.WebControls.TextBox txtEndpoint;   //Endpoint Search
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnEPID;  //Endpoint Search

        protected System.Web.UI.HtmlControls.HtmlInputFile roomfileimage; //Image Project
        //protected System.Web.UI.WebControls.Button BtnUploadRmImg; //ZD 100420
        protected System.Web.UI.HtmlControls.HtmlButton BtnUploadRmImg; //ZD 100420
        protected System.Web.UI.WebControls.DataGrid dgItems;
        protected System.Web.UI.HtmlControls.HtmlButton btnUploadImages; //Image Project//ZD 100420
        protected myVRMWebControls.ImageControl Map1ImageCtrl;
        protected myVRMWebControls.ImageControl Map2ImageCtrl;
        //protected myVRMWebControls.ImageControl Sec1ImageCtrl; // FB 2136
        //protected myVRMWebControls.ImageControl Sec2ImageCtrl; // FB 2136
        protected myVRMWebControls.ImageControl Misc1ImageCtrl;
        protected myVRMWebControls.ImageControl Misc2ImageCtrl;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Map1ImageDt;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Map2ImageDt;
        //protected System.Web.UI.HtmlControls.HtmlInputHidden Sec1ImageDt; // FB 2136
        //protected System.Web.UI.HtmlControls.HtmlInputHidden Sec2ImageDt; // FB 2136
        protected System.Web.UI.HtmlControls.HtmlInputHidden Misc1ImageDt;
        protected System.Web.UI.HtmlControls.HtmlInputHidden Misc2ImageDt;
        protected System.Web.UI.WebControls.DropDownList DrpisVIP;
        protected System.Web.UI.WebControls.DropDownList DrpisTelepresence;//FB 2170
        protected System.Web.UI.WebControls.DropDownList DrpServiceType;//FB 2219
        //protected System.Web.UI.WebControls.DropDownList drpSecImgList; //FB 2136
        //protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSelecOption; //FB 2136
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnSecSelection; //FB 2136
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox Chkdedicatedvideo;//FB 2334
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox Chkdedpresentcodec;//FB 2390
        protected System.Web.UI.WebControls.TextBox txtAVOnsiteSupportEmail;//FB 2415
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnisEPTelePresence; //FB 2400

        //FB 2694 Starts
        //protected System.Web.UI.HtmlControls.HtmlTableRow trCallCapacity;//ZD 104844
        //protected System.Web.UI.HtmlControls.HtmlTableRow trsetupteardown; //ZD 101563      
        protected System.Web.UI.HtmlControls.HtmlTableRow trVIP;
        protected System.Web.UI.HtmlControls.HtmlTableRow trServiceType;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDedication;
        protected System.Web.UI.HtmlControls.HtmlTableRow trAVEmail;
        protected System.Web.UI.HtmlControls.HtmlTableRow trLatitude;
        protected System.Web.UI.HtmlControls.HtmlTableRow trDynamicRoomLayout;
        //protected System.Web.UI.HtmlControls.HtmlTableRow trSecurityImage;//ZD 101271-Commented
        protected System.Web.UI.HtmlControls.HtmlTableRow trMisc1;
        protected System.Web.UI.HtmlControls.HtmlTableRow trMisc2;
        protected System.Web.UI.WebControls.Label lblProjector;        
        //FB 2694 Ends
        bool hasReports = false;//ZD 100664
        protected System.Web.UI.WebControls.DataGrid dgChangedHistory;//ZD 100664
        protected System.Web.UI.HtmlControls.HtmlTableRow trHistory; //ZD 100664
        //ZD 100619 Starts
        protected System.Web.UI.HtmlControls.HtmlTableCell lblVIP;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdVIP;
        protected System.Web.UI.WebControls.TextBox txtContactEmail;
        protected System.Web.UI.WebControls.TextBox txtContactPhone;
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnRoomCategory;
        protected System.Web.UI.HtmlControls.HtmlInputHidden AssistantEmail;
        //ZD 100619 Ends
		//ZD 101098 START
        protected System.Web.UI.HtmlControls.HtmlTableRow triControlRoom;
        protected System.Web.UI.HtmlControls.HtmlInputCheckBox chkiControlRoom;
        protected System.Web.UI.WebControls.CheckBox ChkSecure;//ZD 101244
        protected System.Web.UI.WebControls.TextBox txtsecurityemail;//ZD 101244
        //ZD 101098 END
        protected System.Web.UI.HtmlControls.HtmlInputHidden hdnHelpReq; //ZD 102168
        protected System.Web.UI.WebControls.ListBox lstHelpReqEmail;
        //ZD 104844 - Start
        protected System.Web.UI.HtmlControls.HtmlTableCell tdlblMaxConcurrentCalls;
        protected System.Web.UI.HtmlControls.HtmlTableCell tdMaxConcurrentCalls;
        //ZD 104844 - End
        #endregion

        #region Constructor
        public Room()
        {
            log = new ns_Logger.Logger();
            obj = new myVRMNet.NETFunctions();
            imageUtilObj = new myVRMNet.ImageUtil(); //Image Project
        }
        #endregion

        //ZD 101022
        #region InitializeCulture
        protected override void InitializeCulture()
        {
            if (Session["UserCulture"] != null)
            {
                UICulture = Session["UserCulture"].ToString();
                Culture = Session["UserCulture"].ToString();
                //ZD 101714
                if (Session["UserCulture"].ToString() == "fr-CA")
                    Culture = "en-US";
                base.InitializeCulture();
            }
        }
        #endregion

        #region Page_Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (obj == null)
                    obj = new myVRMNet.NETFunctions();
                obj.AccessandURLConformityCheck("ManageRoomProfile.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263
                if (Session["FormatDateType"] != null)
                {
                    if (Session["FormatDateType"].ToString() == "")
                        Session["FormatDateType"] = "MM/dd/yyyy";
                    else
                        dtFormatType = Session["FormatDateType"].ToString();
                }

                //FB 1830 - Starts
                if (Session["language"] == null)
                    Session["language"] = "en";

                if (Session["language"].ToString() != "")
                    language = Session["language"].ToString();
                //FB 1830 - End

                Session["timeFormat"] = ((Session["timeFormat"] == null) ? "1" : Session["timeFormat"]);
                tformat = (Session["timeFormat"].ToString().Equals("0") ? "HH:mm" : "hh:mm tt");
				//ZD 100664 Start
                if (Session["timeFormat"].ToString().Equals("2"))
                    tformat = "HHmmZ";
				//ZD 100664 End

                //ZD 103496 commented for ZD 104452
                //if (Session["MemcacheEnabled"] != null && !string.IsNullOrEmpty(Session["MemcacheEnabled"].ToString()))
                //    int.TryParse(Session["MemcacheEnabled"].ToString(), out memcacheEnabled);


                /* *** Code added for FB 1425 QA Bug -Start *** */

                if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                {
                    TzTD1.Attributes.Add("style", "display:none");
                    TzTD2.Attributes.Add("style", "display:none");
                    spnConf.InnerText = "Hearing Room Approvers";


                    if (hdntzone.Value == "")
                    {
                        lstTimezone.SelectedValue = "31";
                        //String timezne = obj.GetSystemTimeZone(Application["COM_ConfigPath"].ToString());
                        String timezne = obj.GetSystemTimeZone(Application["MyVRMServer_ConfigPath"].ToString());
                        if (timezne != "")
                        {
                            lstTimezone.ClearSelection();
                            lstTimezone.SelectedValue = timezne;
                        }
                    }
                }

                //if(Session["RoomEP"] != null)
                //Code added for Endpoint Search
                //if (hdnEPID.Value != "")
                //    txtEndpoint.Text = lstEndpoint.Items.FindByValue(hdnEPID.Value.Trim()).Text;

                /* *** Code added for FB 1425 QA Bug -End *** */

                /*if (hdnSelecOption.Value != "")
                {
                    String[] secArry = hdnSelecOption.Value.Split('|');

                    for (Int32 s = 0; s < secArry.Length; s++)
                    {
                        if (secArry[s] != "")
                            drpSecImgList.Items.Add(new ListItem(secArry[s].Split(',')[1], secArry[s].Split(',')[0]));
                    }

                    if (hdnSecSelection.Value != "")
                        drpSecImgList.SelectedIndex = drpSecImgList.Items.IndexOf(drpSecImgList.Items.FindByValue(hdnSecSelection.Value));
                }*/

                if (Request.QueryString["pageid"] != null) //FB 2694
                    ShowHideHotdeskingRoomContent();

                //ZD 100664 start
                string[] mary = Session["sMenuMask"].ToString().Split('-');
                string[] mmary = mary[1].Split('+');
                string[] ccary = mary[0].Split('*');
                int topMenu = Convert.ToInt32(ccary[1]);
                int adminMenu = Convert.ToInt32(mmary[10].Split('*')[1]); // Convert.ToInt32(mmary[1].Split('*')[1]); ZD 101233 103095

                if (Convert.ToBoolean(topMenu & 16))
                {
                    hasReports = Convert.ToBoolean(adminMenu & 4);
                }

                if (Session["admin"] != null)
                {
                    if (Session["admin"].ToString().Equals("2") && (hasReports == true))
                    {
                        trHistory.Attributes.Add("Style", "visibility:visible;");
                    }
                    else
                    {
                        trHistory.Attributes.Add("Style", "visibility:hidden;");
                    }
                }
                //ZD 100664 End

                if (!IsPostBack) //if page loads for the first time
                {
                    if (Request.QueryString["rID"] != null)
                        hdnRoomID.Value = Request.QueryString["rID"].ToString();
                    else
                        hdnRoomID.Value = "new";
                    if (hdnRoomID.Value.ToLower().Equals("new"))//FB 2694
                    {
                        if (Request.QueryString["pageid"] != null)
                            lblTitle.Text = obj.GetTranslatedText("Create New Hotdesking") + " " + lblTitle.Text; //ZD 100288
                        else
                            lblTitle.Text = obj.GetTranslatedText("Create New") + " " + lblTitle.Text;//FB 1830 - Translation //ZD 100288
                    }
                    else
                    {
                        if (Request.QueryString["pageid"] != null)
                            lblTitle.Text = obj.GetTranslatedText("Edit Hotdesking") + " " + lblTitle.Text;//ZD 100288
                        else
                            lblTitle.Text = obj.GetTranslatedText("Edit") + " " + lblTitle.Text;//FB 1830 - Translation //ZD 100288
                    }

                    Assistant.Attributes.Add("readonly", "");
                    Approver0.Attributes.Add("readonly", "");
                    Approver1.Attributes.Add("readonly", "");
                    Approver2.Attributes.Add("readonly", "");
                    
                    BindData();
                    //BindSecurityImages(); // FB 2136
                }
                else // if page refreshes after a post back
                {
                }

                //FB 2670
                if (Session["admin"].ToString() == "3")
                {
                    
                    // btnSubmit.ForeColor = System.Drawing.Color.Gray; // FB 2796

                    
                    //btnSubmitAddNew.ForeColor = System.Drawing.Color.Gray;// FB 2796
                    //btnSubmitAddNew.Attributes.Add("Class", "btndisable");// FB 2796
                    //btnSubmit.Attributes.Add("Class", "btndisable");// FB 2796

                    if (Request.QueryString["pageid"] != null)// FB 2694
                        lblTitle.Text = obj.GetTranslatedText("View Hotdesking Room Details");
                    else
                        lblTitle.Text = obj.GetTranslatedText("View Room Details");
                    //ZD 100263
                    btnSubmit.Visible = false;
                    btnSubmitAddNew.Visible = false;
                    btnReset.Visible = false;
                }
                // FB 2796 Starts 
                else
                {
                    btnSubmitAddNew.Attributes.Add("Class", "altLongBlueButtonFormat");
                    btnSubmit.Attributes.Add("Class", "altLongBlueButtonFormat");
                }
                // FB 2796 End

                //ZD 100664
                if (hdnRoomID.Value == "new")
                    trHistory.Attributes.Add("Style", "visibility:hidden;");
                if (AssistantEmail.Value != "") //ZD 100619
                    txtContactEmail.Text = AssistantEmail.Value;

                txtsecurityemail.Enabled = false;//ZD 101244

            }
            catch (Exception ex)
            {
                log.Trace("PageLoad: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //FB 2694 Starts
        #region ShowHideHotdeskingRoomContent
        /// <summary>
        /// ShowHideHotdeskingRoomContent
        /// </summary>
        protected void ShowHideHotdeskingRoomContent()
        {
            //ZD 104844 - Start
            //trCallCapacity.Visible= false;
            tdlblMaxConcurrentCalls.Visible = false;
            tdMaxConcurrentCalls.Visible = false;
            //ZD 104844 - End
            //trsetupteardown.Visible= false;//ZD 101563 
            tdVIP.Visible = false; //ZD 100619
            lblVIP.Visible = false; //ZD 100619

            trServiceType.Visible= false;
            trDedication.Visible= false;
            trAVEmail.Visible= false;
            lblProjector.Visible= false;
            lstProjector.Visible = false;
            trLatitude.Visible = false;
            trDynamicRoomLayout.Visible = false;
            //trSecurityImage.Visible = false;//ZD 101271-Commented
            trMisc1.Visible = false;
            trMisc2.Visible = false;
            triControlRoom.Visible = false;//ZD 101098
        }
        #endregion
        //FB 2694 Ends

        // FB 2136 Start
        /*#region BindSecurityImages
        /// <summary>
        /// BindSecurityImages
        /// </summary>
        protected void BindSecurityImages()
        {
            try
            {
                StringBuilder inXML = new StringBuilder();

                inXML.Append("<GetSecImages>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("</GetSecImages>");

                log.Trace("GetSecImages InXML: " + inXML);
                String outXML = obj.CallMyVRMServer("GetSecImages", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetSecImages OutXML: " + outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetSecImages/badge");
                obj.LoadList(drpSecImgList, nodes, "badgeid", "badgename");
            }
            catch (Exception e)
            {
                log.Trace("BindSecurityImages: " + e.Message);
            }
        }
        #endregion*/
        // FB 2136 End

        #region BindData
        //Binds Room Profile to controls for the first time
        protected void BindData()
        {
            try
            {
                String inXML = "";
                inXML += "<GetLocations>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "</GetLocations>";
                log.Trace("getLocations InXML: " + inXML);
                String outXML = obj.CallMyVRMServer("GetLocations", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("getLocations OutXML: " + outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetLocations/Location");
                    obj.LoadList(lstTopTier, nodes, "ID", "Name");
                }
                obj.GetCountryCodes(lstCountries);
                try
                {
                    if (hdnRoomID.Value == "new")
                    {
                        lstCountries.Items.FindByValue("225").Selected = true; // United States
                        UpdateStates(null, null);                        
                    }
                    else
                        lstCountries.Items.FindByValue("1").Selected = true;
                }
                catch (Exception ex1) { }

                lstStates.Items.Clear();
                obj.GetCountryStates(lstStates, lstCountries.SelectedValue);

                if(hdnRoomID.Value == "new")
                    lstStates.Items.FindByValue("-1").Selected = true; // // ZD 100486 Default to Please select... Correspondong to the United States as Country

                LoadEndpoints();
                LoadDepartments();

                String tzID = "26";
                if (Session["systemTimezoneID"] != null)
                    tzID = Session["systemTimezoneID"].ToString();

                obj.GetTimezones(lstTimezone, ref tzID);
                //obj.BindServiceType(DrpServiceType);//FB 2219 //ZD 102159

                DirectoryInfo imgFiles = new DirectoryInfo(Server.MapPath("Image/room"));
                ArrayList imgList = new ArrayList();

                foreach (FileInfo file in imgFiles.GetFiles("*.jpg"))
                    imgList.Add(file.ToString().Replace(".jpg", ""));

                lstRoomImage.DataSource = imgList;
                lstRoomImage.DataBind();
                lstRoomImage.Items.Insert(0, new ListItem("[None]", ""));

                if (!hdnRoomID.Value.ToLower().Equals("new"))
                    LoadRoomProfile();
                
            }
            catch (Exception ex)
            {
                log.Trace("BindData: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region UpdateMiddleTiers
        protected void UpdateMiddleTiers(Object sender, EventArgs e)
        {
            try
            {
                String inXML = "";
                inXML += "<GetLocations2>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <Tier1ID>" + lstTopTier.SelectedValue + "</Tier1ID>";
                inXML += "</GetLocations2>";
                String outXML = obj.CallMyVRMServer("GetLocations2", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetLocations2 outxml: " + outXML);
                
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//GetLocations2/Location");
                    obj.LoadList(lstMiddleTier, nodes, "ID", "Name");
                }
                //ZD 101244 Start
                String inxml = "";
                inxml += "<GetLocations>";
                inxml += obj.OrgXMLElement();//Organization Module Fixes
                inxml += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "  <Tier1ID>" + lstTopTier.SelectedValue + "</Tier1ID>";
                inxml += "</GetLocations>";
                String outxml = obj.CallMyVRMServer("GetLocationsTier1", inxml, Application["MyVRMServer_ConfigPath"].ToString());
                if (outxml.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);
                    if (xmldoc.SelectSingleNode("//GetLocations/Secure") != null && xmldoc.SelectSingleNode("//GetLocations/Secure").InnerText == "1")
                        ChkSecure.Enabled = false;
                    else
                        ChkSecure.Enabled = true;
                }
                //ZD 101244 End
            }
            catch (Exception ex)
            {
                log.Trace("UpdateMiddleTiers: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region LoadEndpoints
        protected void LoadEndpoints()
        {
            try
            {
                //FB 2594 Starts
                int isPublicEP = 0;
                if (Session["EnablePublicRooms"] != null)
                    if (Session["EnablePublicRooms"].ToString() == "1")
                        isPublicEP = 1;
                //FB 2594 Ends
                String inXML = "";
                inXML += "<SearchEndpoint>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <EndpointName></EndpointName>";
                inXML += "  <EndpointType></EndpointType>";
                inXML += "  <PageNo></PageNo>";
                inXML += "  <PublicEndpoint>" + isPublicEP + "</PublicEndpoint>"; //FB 2594
                inXML += "</SearchEndpoint>";
                log.Trace("SearchEndpoint Inxml: " + inXML);


                String outXML = obj.CallMyVRMServer("SearchEndpoint", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SearchEndpoint outxml: " + outXML);

                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//SearchEndpoint/Endpoints/Endpoint");
                    if (nodes.Count > 0)
                        obj.LoadList(lstEndpoint, nodes, "ID", "Name");
                }
                else
                {
                    /* *** Code added for FB 1425 QA Bug -Start *** */

                    if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                            errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                    }
                    else
                        /* *** Code added for FB 1425 QA Bug -End *** */
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("LoadEndpoints: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region LoadDepartments
        protected void LoadDepartments()
        {
            try
            {
                String inXML = "";
                inXML += "<GetDepartmentsForLocation>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "</GetDepartmentsForLocation>";
                log.Trace("GetDepartmentsForLocation Inxml: " + inXML);

                String outXML = obj.CallMyVRMServer("GetDepartmentsForLocation", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetDepartmentsForLocation outxml: " + outXML);
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    hdnMultipleDept.Value = xmldoc.SelectSingleNode("//GetDepartmentsForLocation/multiDepartment").InnerText;
                    if (hdnMultipleDept.Value == "1")
                    {
                        XmlNodeList nodes = xmldoc.SelectNodes("//GetDepartmentsForLocation/departments/department");
                        if (nodes.Count > 0)
                            obj.LoadList(DepartmentList, nodes, "id", "name");
                    }
                }
                else
                {
                    /* *** Code added for FB 1425 QA Bug -Start *** */

                    if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                            errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                    }
                    else
                        /* *** Code added for FB 1425 QA Bug -End *** */
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("LoadEndpoints: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region LoadRoomProfile
        protected void LoadRoomProfile()
        {
            DateTime deactDate = DateTime.MinValue;
            try
            {
                string inXML = "<GetRoomProfile>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inXML += "  <RoomID>" + hdnRoomID.Value + "</RoomID>";
                inXML += "</GetRoomProfile>";
                
                string outXML = obj.CallMyVRMServer("GetRoomProfile", inXML, Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                //FB 2136 start
                /*if (xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security1") != null)
                {
                    string selSecImage = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security1").InnerText.Trim();
                    string selSecImageId = xmldoc.SelectSingleNode("//GetRoomProfile/Images/Security1ImageId").InnerText.Trim();
                    if (selSecImageId != "")
                    {
                        try
                        {
                            drpSecImgList.SelectedValue = selSecImageId;
                           // hdnSelecOption.Value = selSecImage + "," + selSecImageId;
                        }
                        catch { }
                        //Valli
                        //string[] tmpImgArray = null;
                        //tmpImgArray = selSecImage.Split('_');
                        //drpSecImgList.SelectedValue = tmpImgArray[0].Substring(8, tmpImgArray[0].Length - 8);
                        //tmpImgArray = selSecImage.Split('.');
                        //hdnSelecOption.Value = tmpImgArray[0];
                    }
                }*/
                //FB 2136 end

                //ZD 100664 Start
                XmlNodeList changedHistoryNodes = xmldoc.SelectNodes("//GetRoomProfile/LastModifiedDetails/LastModified");
                LoadHistory(changedHistoryNodes);
                //ZD 100664 End

                txtRoomName.Text = xmldoc.SelectSingleNode("//GetRoomProfile/RoomName").InnerText;
                txtRoomEmail.Text = xmldoc.SelectSingleNode("//GetRoomProfile/RoomQueue").InnerText; // FB 2342
                txtRoomUID.Text = xmldoc.SelectSingleNode("//GetRoomProfile/RoomUID").InnerText; //ZD 100196
                hdnRoomID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/RoomID").InnerText;
                txtRoomPhone.Text = xmldoc.SelectSingleNode("//GetRoomProfile/RoomPhoneNumber").InnerText;
                txtRoomNumber.Text = xmldoc.SelectSingleNode("//GetRoomProfile/RoomNumber").InnerText;
                txtFloor.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Floor").InnerText;
                txtMaximumCapacity.Text = xmldoc.SelectSingleNode("//GetRoomProfile/MaximumCapacity").InnerText;
                txtMaxConcurrentCalls.Text = xmldoc.SelectSingleNode("//GetRoomProfile/MaximumConcurrentPhoneCalls").InnerText;
                txtMultipleAssistant.Text = xmldoc.SelectSingleNode("//GetRoomProfile/MultipleAssistantEmails").InnerText;
                //txtSetupTime.Text = xmldoc.SelectSingleNode("//GetRoomProfile/SetupTime").InnerText; //ZD 101563
                //txtTeardownTime.Text = xmldoc.SelectSingleNode("//GetRoomProfile/TeardownTime").InnerText;
				if(xmldoc.SelectSingleNode("//GetRoomProfile/RoomCategory") != null) //ZD 100619
                	hdnRoomCategory.Value = xmldoc.SelectSingleNode("//GetRoomProfile/RoomCategory").InnerText;

                //FB 1644
                if (xmldoc.SelectSingleNode("//GetRoomProfile/UserStaus") != null)
                {
                    if (xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeName") != null)//FB 2519
                        errLabel.Text = "The Assistant-In-Charge \"" + xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeName").InnerText + "\" is not found in Active User List.";//FB 2974
                    errLabel.Visible = true;
                }
                else
                {
                    AssistantID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeID").InnerText;
                    Assistant.Text = xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeName").InnerText;
					if(xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeEmail") != null) //ZD 100619
                    	txtContactEmail.Text = xmldoc.SelectSingleNode("//GetRoomProfile/AssistantInchargeEmail").InnerText;
                }
				if(xmldoc.SelectSingleNode("//GetRoomProfile/GuestContactPhone") != null) //ZD 100619
                	txtContactPhone.Text = xmldoc.SelectSingleNode("//GetRoomProfile/GuestContactPhone").InnerText;
                txtStreetAddress1.Text = xmldoc.SelectSingleNode("//GetRoomProfile/StreetAddress1").InnerText;
                txtStreetAddress2.Text = xmldoc.SelectSingleNode("//GetRoomProfile/StreetAddress2").InnerText;
                txtCity.Text = xmldoc.SelectSingleNode("//GetRoomProfile/City").InnerText;
                txtZipCode.Text = xmldoc.SelectSingleNode("//GetRoomProfile/ZipCode").InnerText;
                txtParkingDirections.Text = xmldoc.SelectSingleNode("//GetRoomProfile/ParkingDirections").InnerText.Replace("&gt;", ">").Replace("&lt;", "<").Replace("&amp;", "&").Replace("&#37", "%");
                txtMapLink.Text = xmldoc.SelectSingleNode("//GetRoomProfile/MapLink").InnerText.Replace("&gt;",">").Replace("&lt;","<").Replace("&amp;","&").Replace("&#37","%") ;
                txtAdditionalComments.Text = xmldoc.SelectSingleNode("//GetRoomProfile/AdditionalComments").InnerText;
                txtLongitude.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Longitude").InnerText;
                txtLatitude.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Latitude").InnerText;
                Approver0ID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/Approvers/Approver1ID").InnerText;
                Approver0.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Approvers/Approver1Name").InnerText;
                Approver1ID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/Approvers/Approver2ID").InnerText;
                Approver1.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Approvers/Approver2Name").InnerText;
                Approver2ID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/Approvers/Approver3ID").InnerText;
                Approver2.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Approvers/Approver3Name").InnerText;

                XmlNodeList nodes = xmldoc.SelectNodes("//GetRoomProfile/Departments/Department");
                Int32 c = nodes.Count;
                for (Int32 d = 0; d < nodes.Count; d++)
                {
                    foreach (ListItem item in DepartmentList.Items)
                    {
                        if (c == 0)
                            break;
                        if (item.Value == nodes[d].SelectSingleNode("ID").InnerText)
                        {
                            item.Selected = true;
                            c--;
                            break;
                        }
                    }
                }
            
                
                lstTopTier.SelectedValue = xmldoc.SelectSingleNode("//GetRoomProfile/Tier1ID").InnerText;
                UpdateMiddleTiers(new Object(), new EventArgs());
                lstMiddleTier.SelectedValue = xmldoc.SelectSingleNode("//GetRoomProfile/Tier2ID").InnerText;

                lstProjector.SelectedValue = xmldoc.SelectSingleNode("//GetRoomProfile/Projector").InnerText;
                lstVideo.SelectedValue = xmldoc.SelectSingleNode("//GetRoomProfile/Video").InnerText;
                //DrpServiceType.SelectedValue = xmldoc.SelectSingleNode("//GetRoomProfile/ServiceType").InnerText;//FB 2219 //ZD 102159
                if (xmldoc.SelectSingleNode("//GetRoomProfile/DedicatedVideo").InnerText == "1")//FB 2334
                {
                    Chkdedicatedvideo.Checked = true;
                }
                else
                {
                    Chkdedicatedvideo.Checked = false;
                }
                //FB 2390 Start
                if (xmldoc.SelectSingleNode("//GetRoomProfile/DedicatedCodec").InnerText == "1")//FB 2334
                {
                    Chkdedpresentcodec.Checked = true;
                }
                else
                {
                    Chkdedpresentcodec.Checked = false;
                }
                //FB 2390 End
                txtAVOnsiteSupportEmail.Text = xmldoc.SelectSingleNode("//GetRoomProfile/AVOnsiteSupportEmail").InnerText;//FB 2415
                lstDynamicRoomLayout.SelectedValue = xmldoc.SelectSingleNode("//GetRoomProfile/DynamicRoomLayout").InnerText;
                hdnisEPTelePresence.Value = xmldoc.SelectSingleNode("//GetRoomProfile/isTelePresEndPoint").InnerText; //FB 2400
                 
                CatererList.SelectedValue = xmldoc.SelectSingleNode("//GetRoomProfile/CatererFacility").InnerText;
				
				 //ZD 101098 START
                if (xmldoc.SelectSingleNode("//GetRoomProfile/iControlRoom").InnerText == "1")
                    chkiControlRoom.Checked = true;
                else
                    chkiControlRoom.Checked = false;
                //ZD 101098 END

                //ZD 101244 start
                UpdateCheckSecuredetails(new Object(), new EventArgs());
                if (xmldoc.SelectSingleNode("//GetRoomProfile/Secure") != null && xmldoc.SelectSingleNode("//GetRoomProfile/Secure").InnerText == "1")
                    ChkSecure.Checked = true;
                else
                    ChkSecure.Checked = false;
                if (xmldoc.SelectSingleNode("//GetRoomProfile/Securityemail") != null)
                    txtsecurityemail.Text = xmldoc.SelectSingleNode("//GetRoomProfile/Securityemail").InnerText;

                if (xmldoc.SelectSingleNode("//GetRoomProfile/UsersSecurityemail") != null) //ZD 102168
                {
                    hdnHelpReq.Value = xmldoc.SelectSingleNode("//GetRoomProfile/UsersSecurityemail").InnerText.Trim();
                    //ZD 102365 Commenting Starts
                    //String[] HelpReq = xmldoc.SelectSingleNode("//GetRoomProfile/UsersSecurityemail").InnerText.Trim().Split(';');
                    //for (int i = 0; i < HelpReq.Length; i++)
                    //{
                    //    if (HelpReq[i].Trim() != "")
                    //    {
                    //        ListItem item = new ListItem();
                    //        item.Text = HelpReq[i];
                    //        item.Attributes.Add("title", HelpReq[i]);
                    //        lstHelpReqEmail.Items.Add(item);
                    //    }
                    //}
                    //ZD 102365 Commenting Ends
                }
                //ZD 101244 end

               
         
                //Image Project code starts here...
                rmImg = "";
                XmlNodeList rmImgNodes = xmldoc.SelectNodes("//GetRoomProfile/RoomImages/ImageDetails");

                if(rmImgNodes != null)
                    LoadRoomImageGrid(rmImgNodes);

                //String[] rmImgSplit = null;

                //if (rmImg.IndexOf(',') > 0)
                //{
                //    rmImgSplit = rmImg.Split(',');
                //    Int32 j = rmImgSplit.Length;
                //    for (Int32 i = 0; i <= rmImgSplit.Length - 1; i++)
                //    {
                //        if (j == 0)
                //            break;
                //        foreach (ListItem item in lstRoomImage.Items)
                //        {
                //            if (item.Text == rmImgSplit[i].ToString())
                //            {
                //                item.Selected = true;
                //                j--;
                //                break;
                //            }
                //        }
                //    }
                //}
                //else
                //    lstRoomImage.SelectedValue = rmImg;



                // Get Top and Middle Tiers 
                try
                {
                    lstTopTier.ClearSelection();
                    lstTopTier.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/Tier1ID").InnerText).Selected = true;
                    if ((lstTopTier.Items.Count > 1) && (lstTopTier.SelectedIndex > 0))
                    {
                        UpdateMiddleTiers(new Object(), new EventArgs());
                        if (lstMiddleTier.Items.Count > 0)
                        {
                            lstMiddleTier.ClearSelection();
                            lstMiddleTier.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/Tier2ID").InnerText).Selected = true;
                        }
                    }


                    if (xmldoc.SelectSingleNode("//GetRoomProfile/LastModifiedDate") != null)
                    {
                        DateTime.TryParse(xmldoc.SelectSingleNode("//GetRoomProfile/LastModifiedDate").InnerText, out deactDate);

                        if (deactDate != DateTime.MinValue)
                            lblMdate.Text = myVRMNet.NETFunctions.GetFormattedDate(deactDate.ToString()) + " " + deactDate.ToString(tformat); //ZD 100995
                    }
                    lblMUser.Text = xmldoc.SelectSingleNode("//GetRoomProfile/LastModififeduserName").InnerText;


                }
                catch (Exception ex1) { }


                try
                {
                    DrpHandi.ClearSelection();

                    if(DrpHandi.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/Handicappedaccess").InnerText) != null)
                        DrpHandi.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/Handicappedaccess").InnerText).Selected = true;
                }
                catch (Exception ex1) { }

                try
                {
                    DrpisVIP.ClearSelection();

                    if (DrpisVIP.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/isVIP").InnerText) != null)
                        DrpisVIP.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/isVIP").InnerText).Selected = true;
                }
                catch (Exception ex1) { }

                //Get Countries and States
                try
                {
                    lstCountries.ClearSelection();
                    lstCountries.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/Country").InnerText).Selected = true;
                    if ((lstCountries.Items.Count > 1) && (lstCountries.SelectedIndex > 0))
                    {
                        lstStates.Items.Clear();
                        obj.GetCountryStates(lstStates, lstCountries.SelectedValue);
                        if (lstStates.Items.Count > 0)
                        {
                            lstStates.ClearSelection();
                            lstStates.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/State").InnerText).Selected = true;
                        }
                    }
                }
                catch (Exception ex1) { }

                //Get selected Timezone for the room
                try
                {
                    lstTimezone.ClearSelection();
                    lstTimezone.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/TimezoneID").InnerText).Selected = true;
                }
                catch (Exception ex1) { }

                //Get Endpoint for the room
                try
                {
                    lstEndpoint.ClearSelection();
                    lstEndpoint.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/EndpointID").InnerText).Selected = true;
                    //code added for Endpoint Search - Start
                    txtEndpoint.Text = "";
                    txtEndpoint.Text = lstEndpoint.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/EndpointID").InnerText).Text;
                    hdnEPID.Value = xmldoc.SelectSingleNode("//GetRoomProfile/EndpointID").InnerText;
                    //code added for Endpoint Search - End
                }
                catch (Exception ex1) { }

                //Get Image Files links
                //Image Project code starts here...

                XmlNode oimgNode = xmldoc.SelectSingleNode("//GetRoomProfile/Images");
                if (oimgNode != null)
                {
                    if (oimgNode.ChildNodes.Count > 0)
                    {
                        DisplayOtherImages(oimgNode);
                    }
                }
                //Image Project code ends here...

                hdntzone.Value = "1";//Code added for  FB 1425 QA Bug

                //FB 2170
                try
                {
                    DrpisTelepresence.ClearSelection();

                    if (DrpisTelepresence.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/isTelepresence").InnerText) != null)
                        DrpisTelepresence.Items.FindByValue(xmldoc.SelectSingleNode("//GetRoomProfile/isTelepresence").InnerText).Selected = true;
                }
                catch (Exception ex1) { }
                
            }
            catch (Exception ex)
            {
                log.Trace("LoadRoomProfile: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region LoadRoomImageGrid - Added for Image Project Edit Mode

        private void LoadRoomImageGrid(XmlNodeList rmImgNodes)
        { 
            //byte[] myData; string imageString; //ZD 101611 //ZD 103569 start
            //string ImageID = ""; 
            string ImagePath="";
            try
            {
                roomImageDt = new DataTable();
                roomImageDt.Columns.Add(new DataColumn("ImageName"));
                roomImageDt.Columns.Add(new DataColumn("Image"));
                roomImageDt.Columns.Add(new DataColumn("Imagetype"));
                roomImageDt.Columns.Add(new DataColumn("ImagePath"));

                //string imgData = "";
                //pathName = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en/image/room/";//FB 1830
                //pathName = "../en/image/room/";//FB 1830 
                
                //foreach (XmlNode xnode in rmImgNodes)
                //{
                //    fName = xnode.SelectSingleNode("ImageName").InnerText;
                //    fileext = xnode.SelectSingleNode("Imagetype").InnerText;
                //    //imgData = xnode.SelectSingleNode("Image").InnerText;
                //    ImageID = xnode.SelectSingleNode("ImageID").InnerText;//ZD 101611

                    //if (!File.Exists(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString())  + "\\image\\room\\" + fName)) //FB 1830
                    //{
                    //    File.Delete((Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString())  + "\\image\\room\\"+ fName));//FB 1830
                    //}

                    //imgArray = imageUtilObj.ConvertBase64ToByteArray(imgData);
                    //ZD 101611 start
                    //System.Drawing.Image image = System.Drawing.Image.FromStream(new System.IO.MemoryStream(imgArray));
                    //if (image.Width > 250 || image.Height > 250)
                    //{
                    //    int roomImgWidth = 250;
                    //    int roomImgHeight = 250;
                    //    myData = obj.Imageresize(image.Width, image.Height, roomImgWidth, roomImgHeight, image);
                    //    imageString = imageUtilObj.ConvertByteArrToBase64(myData);
                    //    SetRoomImage(imageString,ImageID);
                    //    WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName, ref myData);
                    //    //SetRoomProfile();
                    //}
                    //else
                    //{
                    //    WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\" + fName, ref imgArray);//FB 1830
                    //}
                    //ZD 101611 End
                    //WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\image\\room\\"+ fName, ref imgArray);//FB 1830

                    foreach (XmlNode xnode in rmImgNodes)
                    {
                    fName = xnode.SelectSingleNode("ImageName").InnerText;
                    fileext = xnode.SelectSingleNode("Imagetype").InnerText;
                    ImagePath = xnode.SelectSingleNode("ImageWebPath").InnerText; ;
                    //imgArray = imageUtilObj.ConvertBase64ToByteArray(imgData);

                    DataRow dr = roomImageDt.NewRow();

                    dr["ImageName"] = fName;
                    dr["Imagetype"] = fileext;
                    dr["Image"] = "";
                    dr["ImagePath"] = ImagePath + fName; //ZD 103569 End

                    roomImageDt.Rows.Add(dr);
                }
                if (roomImageDt.Rows.Count > 0)
                    dgItems.DataSource = roomImageDt;
                else
                    dgItems.DataSource = null;

                dgItems.DataBind();
                dgItems.Visible = true;

            }
            catch (Exception ex)
            {
                log.Trace("LoadRoomProfile: " + ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        #region Display Room Other Images - Added for Image Project Edit Mode
        /// <summary>
        /// Display Room Other Images - Added for Image Project Edit Mode
        /// </summary>
        /// <param name="imgNode"></param>
        private void DisplayOtherImages(XmlNode imgNode)
        {
            XmlNode node = null;
            string imgString = "";
            try
            {
                imgString = "";
                fName = "";
                node = imgNode.SelectSingleNode("Map1");
                if (node != null)
                    fName = node.InnerText;

                node = imgNode.SelectSingleNode("Map1Image");
                if (node != null)
                    imgString = node.InnerText;

                hdnUploadMap1.Text = fName;
                Map1ImageDt.Value = imgString;
                //imgArray = imageUtilObj.ConvertBase64ToByteArray(imgString);//ZD 103569
                LoadOtherImage(1);

                imgString = "";
                fName = "";
                node = imgNode.SelectSingleNode("Map2");
                if (node != null)
                    fName = node.InnerText;

                node = imgNode.SelectSingleNode("Map2Image");
                if (node != null)
                    imgString = node.InnerText;

                hdnUploadMap2.Text = fName;
                Map2ImageDt.Value = imgString;

                // imgArray = imageUtilObj.ConvertBase64ToByteArray(imgString); //ZD 103569
                LoadOtherImage(2);

                // FB 2136 Starts
                //imgString = "";
                //fName = "";
                //node = imgNode.SelectSingleNode("Security1");
                //if (node != null)
                //    fName = node.InnerText;

                //node = imgNode.SelectSingleNode("Security1Image");
                //if (node != null)
                //    imgString = node.InnerText;

                //hdnUploadSecurity1.Text = fName;
                //Sec1ImageDt.Value = imgString;

                //imgArray = imageUtilObj.ConvertBase64ToByteArray(imgString); 
                //LoadOtherImage(3);

                //imgString = "";
                //fName = "";
                //node = imgNode.SelectSingleNode("Security2");
                //if (node != null)
                //    fName = node.InnerText;

                //node = imgNode.SelectSingleNode("Security2Image");
                //if (node != null)
                //    imgString = node.InnerText;

                //hdnUploadSecurity2.Text = fName;
                //Sec2ImageDt.Value = imgString;

                //imgArray = imageUtilObj.ConvertBase64ToByteArray(imgString);
                //LoadOtherImage(4);
                // FB 2136 End

                imgString = "";
                fName = "";
                node = imgNode.SelectSingleNode("Misc1");
                if (node != null)
                    fName = node.InnerText;

                node = imgNode.SelectSingleNode("Misc1Image");
                if (node != null)
                    imgString = node.InnerText;

                hdnUploadMisc1.Text = fName;
                Misc1ImageDt.Value = imgString;

                //imgArray = imageUtilObj.ConvertBase64ToByteArray(imgString); //ZD 103569
                LoadOtherImage(5);

                imgString = "";
                fName = "";
                node = imgNode.SelectSingleNode("Misc2");
                if (node != null)
                    fName = node.InnerText;

                node = imgNode.SelectSingleNode("Misc2Image");
                if (node != null)
                    imgString = node.InnerText;

                hdnUploadMisc2.Text = fName;
                Misc2ImageDt.Value = imgString;

                //imgArray = imageUtilObj.ConvertBase64ToByteArray(imgString); //ZD 103569
                LoadOtherImage(6);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        private void LoadOtherImage(int cmd)
        {
            try
            {
                if (fName != "")
                    fileext = fName.Substring(fName.LastIndexOf(".") + 1);

                switch (cmd)
                {
                    case 1:
                        {
                            if (string.IsNullOrEmpty(Map1ImageDt.Value) && string.IsNullOrEmpty(fName))//imgArray == null) //ZD 103569 start
                            {
                                hdnUploadMap1.Text = "";
                                Map1ImageDt.Value = "";
                                Map1ImageCtrl.Visible = false;
                                fleMap1.Visible = true;
                                btnRemoveMap1.Visible = false;
                            }
                            //if (imgArray.Length <= 0)
                            //{
                            //    hdnUploadMap1.Text = "";
                            //    Map1ImageDt.Value = "";
                            //    Map1ImageCtrl.Visible = false;
                            //    fleMap1.Visible = true;
                            //    btnRemoveMap1.Visible = false;
                            //}

                            //MemoryStream ms1 = new MemoryStream(imgArray, 0, imgArray.Length);
                            //ms1.Write(imgArray, 0, imgArray.Length);
                            else
                            {
                                if (fileext.ToLower() == "gif")
                                    Map1ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                                else
                                    Map1ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                                Map1ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                                if (!string.IsNullOrEmpty(Map1ImageDt.Value) && !string.IsNullOrEmpty(fName))
                                {
                                    Bitmap bitMapImageMap1 = new System.Drawing.Bitmap(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + Map1ImageDt.Value + fName);
                                    Map1ImageCtrl.Bitmap = bitMapImageMap1;
                                }
                                //Map1ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);
                                //ImageSource imageSource = new BitmapImage(new Uri(@"C:\northwindimages\king.bmp"));
                                //Map1ImageCtrl.Bitmap.Ur = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromFile(I);
                                //Bitmap myBmp = new Bitmap("path here");
                                //Map1ImageCtrl.src
                                //Map1ImageCtrl.ResolveClientUrl

                                imgArray = null;

                                Map1ImageCtrl.Visible = true;
                                fleMap1.Visible = false;
                                btnRemoveMap1.Visible = true;
                            }
                            break;
                        }
                    case 2:
                        {
                            if (string.IsNullOrEmpty(Map2ImageDt.Value) && string.IsNullOrEmpty(fName))
                            {
                                hdnUploadMap2.Text = "";
                                Map2ImageDt.Value = "";
                                Map2ImageCtrl.Visible = false;
                                fleMap2.Visible = true;
                                btnRemoveMap2.Visible = false;
                            }
                            //if (imgArray.Length <= 0)
                            //{
                            //    hdnUploadMap2.Text = "";
                            //    Map2ImageDt.Value = "";
                            //    Map2ImageCtrl.Visible = false;
                            //    fleMap2.Visible = true;
                            //    btnRemoveMap2.Visible = false;
                            //}

                            //MemoryStream ms1 = new MemoryStream(imgArray, 0, imgArray.Length);
                            //ms1.Write(imgArray, 0, imgArray.Length);
                            else
                            {
                                if (fileext.ToLower() == "gif")
                                    Map2ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                                else
                                    Map2ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                                Map2ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                                // Map2ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);
                                if (!string.IsNullOrEmpty(Map2ImageDt.Value) && !string.IsNullOrEmpty(fName))
                                {
                                    Bitmap bitMapImageMap2 = new System.Drawing.Bitmap(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + Map2ImageDt.Value + fName);
                                    Map2ImageCtrl.Bitmap = bitMapImageMap2;
                                }

                                imgArray = null;

                                Map2ImageCtrl.Visible = true;
                                fleMap2.Visible = false;
                                btnRemoveMap2.Visible = true;
                            }
                            break;
                        }
                    //FB 2136 Starts
                    //case 3:
                    //    {
                    //        if (imgArray == null)
                    //        {
                    //            hdnUploadSecurity1.Text = "";
                    //            Sec1ImageDt.Value = "";
                    //            Sec1ImageCtrl.Visible = false;
                    //            fleSecurity1.Visible = true;
                    //            btnRemoveSecurity1.Visible = false;
                    //        }
                    //        if (imgArray.Length <= 0)
                    //        {
                    //            hdnUploadSecurity1.Text = "";
                    //            Sec1ImageDt.Value = "";
                    //            Sec1ImageCtrl.Visible = false;
                    //            fleSecurity1.Visible = true;
                    //            btnRemoveSecurity1.Visible = false;
                    //        }

                    //        MemoryStream ms1 = new MemoryStream(imgArray, 0, imgArray.Length);
                    //        ms1.Write(imgArray, 0, imgArray.Length);

                    //        if (fileext.ToLower() == "gif")
                    //            Sec1ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                    //        else
                    //            Sec1ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                    //        Sec1ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                    //        Sec1ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);

                    //        imgArray = null;

                    //        Sec1ImageCtrl.Visible = true;
                    //        fleSecurity1.Visible = false;
                    //        btnRemoveSecurity1.Visible = true;
                    //        break;
                    //    }
                    //case 4:
                    //    {
                    //        if (imgArray == null)
                    //        {
                    //            hdnUploadSecurity2.Text = "";
                    //            Sec2ImageDt.Value = "";
                    //            Sec2ImageCtrl.Visible = false;
                    //            fleSecurity2.Visible = true;
                    //            btnRemoveSecurity2.Visible = false;
                    //        }
                    //        if (imgArray.Length <= 0)
                    //        {
                    //            hdnUploadSecurity2.Text = "";
                    //            Sec2ImageDt.Value = "";
                    //            Sec2ImageCtrl.Visible = false;
                    //            fleSecurity2.Visible = true;
                    //            btnRemoveSecurity2.Visible = false;
                    //        }

                    //        MemoryStream ms1 = new MemoryStream(imgArray, 0, imgArray.Length);
                    //        ms1.Write(imgArray, 0, imgArray.Length);

                    //        if (fileext.ToLower() == "gif")
                    //            Sec2ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                    //        else
                    //            Sec2ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                    //        Sec2ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                    //        Sec2ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);

                    //        imgArray = null;

                    //        Sec2ImageCtrl.Visible = true;
                    //        fleSecurity2.Visible = false;
                    //        btnRemoveSecurity2.Visible = true;
                    //        break;
                    //    }
                    // FB 2136 Ends
                    case 5:
                        {
                            if (string.IsNullOrEmpty(Misc1ImageDt.Value) && string.IsNullOrEmpty(fName))
                            {
                                hdnUploadMisc1.Text = "";
                                Misc1ImageDt.Value = "";
                                Misc1ImageCtrl.Visible = false;
                                fleMisc1.Visible = true;
                                btnRemoveMisc1.Visible = false;
                            }
                            //if (imgArray.Length <= 0)
                            //{
                            //    hdnUploadMisc1.Text = "";
                            //    Misc1ImageDt.Value = "";
                            //    Misc1ImageCtrl.Visible = false;
                            //    fleMisc1.Visible = true;
                            //    btnRemoveMisc1.Visible = false;
                            //}

                           // MemoryStream ms1 = new MemoryStream(imgArray, 0, imgArray.Length);
                            //ms1.Write(imgArray, 0, imgArray.Length);
                            else
                            {
                                if (fileext.ToLower() == "gif")
                                    Misc1ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                                else
                                    Misc1ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                                Misc1ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                                //Misc1ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);
                                if (!string.IsNullOrEmpty(Misc1ImageDt.Value) && !string.IsNullOrEmpty(fName))
                                {
                                    Bitmap bitMapImageMisc1 = new System.Drawing.Bitmap(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + Misc1ImageDt.Value +fName);
                                    Misc1ImageCtrl.Bitmap = bitMapImageMisc1;
                                }

                                imgArray = null;

                                Misc1ImageCtrl.Visible = true;
                                fleMisc1.Visible = false;
                                btnRemoveMisc1.Visible = true;
                            }
                            break;
                        }
                    case 6:
                        {
                            if (string.IsNullOrEmpty(Misc2ImageDt.Value) && string.IsNullOrEmpty(fName))
                            {
                                hdnUploadMisc2.Text = "";
                                Misc2ImageDt.Value = "";
                                Misc2ImageCtrl.Visible = false;
                                fleMisc2.Visible = true;
                                btnRemoveMisc2.Visible = false;
                            }
                            //if (imgArray.Length <= 0)
                            //{
                            //    hdnUploadMisc2.Text = "";
                            //    Misc2ImageDt.Value = "";
                            //    Misc2ImageCtrl.Visible = false;
                            //    fleMisc2.Visible = true;
                            //    btnRemoveMisc2.Visible = false;
                            //}

                           // MemoryStream ms1 = new MemoryStream(imgArray, 0, imgArray.Length);
                            //ms1.Write(imgArray, 0, imgArray.Length);
                            else
                            {
                                if (fileext.ToLower() == "gif")
                                    Misc2ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                                else
                                    Misc2ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                                Misc2ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                                // Misc2ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);
                                if (!string.IsNullOrEmpty(Misc2ImageDt.Value) && !string.IsNullOrEmpty(fName))
                                {
                                    Bitmap bitMapImageMisc2 = new System.Drawing.Bitmap(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + Misc2ImageDt.Value + fName);
                                    Misc2ImageCtrl.Bitmap = bitMapImageMisc2;
                                }
                                imgArray = null;

                                Misc2ImageCtrl.Visible = true;
                                fleMisc2.Visible = false;
                                btnRemoveMisc2.Visible = true;
                            } //ZD 103569  End
                            break;
                       }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region ResetRoomProfile
        protected void ResetRoomProfile(Object sender, EventArgs e)
        {
            try
            {
                //FB 2694 - Start
                if(Request.QueryString["pageid"] != "Hotdesking")
                    Response.Redirect("ManageRoomProfile.aspx?rID=" + hdnRoomID.Value +"&cal=2");
                else
                    Response.Redirect("ManageRoomProfile.aspx?cal=2&rID=" + hdnRoomID.Value +"&pageid=Hotdesking");
                //FB 2694 - End

            }
            catch (Exception ex)
            {
                log.Trace("ResetRoomProfile" + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region SubmitRoomProfile
        protected void SubmitRoomProfile(Object sender, EventArgs e)
        {
            try
            {
                String roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();//FB 1830

                if (SetRoomProfile())
                {
                    //Response.Redirect("dispatcher/admindispatcher.asp?cmd=ManageConfRoom&m=1"); //Login Management
                    Response.Redirect("manageroom.aspx?hf=&m=1&pub=&d=&comp=&f=&frm=");//Login Management
                }
                else
                {
                    if (obj == null)
                        obj = new myVRMNet.NETFunctions();


                    if (File.Exists(roomxmlPath))
                    {
                        if (obj.WaitForFile(roomxmlPath))
                            File.Delete(roomxmlPath);
                    }
                }


            }
            catch (System.Threading.ThreadAbortException) { }//Login Management
            catch (Exception ex)
            {
                log.Trace("SubmitRoomProfile" + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region SetRoomProfile
        protected bool SetRoomProfile()
        {
            XmlDocument createRooms = null;
            XmlDocument loadRooms = null;
            String roomxmlPath = "";//Room search
            XmlNodeList roomList = null;
            string roomId = "";
            XmlNode ndeRoomID = null;
            
            
            try
            {
                String inXML = "";
                inXML += "<SetRoomProfile>";
                inXML += obj.OrgXMLElement();//Organization Module Fixes
                inXML += "<UserID>" + Session["userID"].ToString() + "</UserID>";                
                inXML += "<RoomID>" + hdnRoomID.Value + "</RoomID>";
                inXML += "<RoomName>" + txtRoomName.Text + "</RoomName>";
                inXML += "<RoomPhoneNumber>" + txtRoomPhone.Text + "</RoomPhoneNumber>";
                if (txtMaximumCapacity.Text == "")
                    txtMaximumCapacity.Text = "0";
                inXML += "<MaximumCapacity>" + txtMaximumCapacity.Text + "</MaximumCapacity>";
                if (txtMaxConcurrentCalls.Text == "")
                    txtMaxConcurrentCalls.Text = "0";
                inXML += "<MaximumConcurrentPhoneCalls>" + txtMaxConcurrentCalls.Text + "</MaximumConcurrentPhoneCalls>";
				//ZD 101563
                //if (txtSetupTime.Text == "")
                //    txtSetupTime.Text = "0";
                //inXML += "<SetupTime>" + txtSetupTime.Text + "</SetupTime>";
                //if (txtTeardownTime.Text == "")
                //    txtTeardownTime.Text = "0";
                //inXML += "<TeardownTime>" + txtTeardownTime.Text + "</TeardownTime>";
                inXML += "<AssistantInchargeID>" + AssistantID.Value + "</AssistantInchargeID>";
                inXML += "<AssistantInchargeName>" + Assistant.Text + "</AssistantInchargeName>"; //ZD 100619
                inXML += "<AssistantInchargeEmail>" + txtContactEmail.Text + "</AssistantInchargeEmail>"; //ZD 100619
                inXML += "<GuestContactPhone>" + txtContactPhone.Text + "</GuestContactPhone>"; //ZD 100619
                inXML += "<MultipleAssistantEmails>" + txtMultipleAssistant.Text + "</MultipleAssistantEmails>";
                inXML += "<Tier1ID>" + lstTopTier.SelectedValue + "</Tier1ID>";
                inXML += "<Tier2ID>" + lstMiddleTier.SelectedValue + "</Tier2ID>";
                inXML += "<Floor>" + txtFloor.Text + "</Floor>";
                inXML += "<RoomNumber>" + txtRoomNumber.Text + "</RoomNumber>";
                inXML += "<StreetAddress1>" + txtStreetAddress1.Text + "</StreetAddress1>";
                inXML += "<StreetAddress2>" + txtStreetAddress2.Text + "</StreetAddress2>";
                inXML += "<City>" + txtCity.Text + "</City>";
                inXML += "<State>" + lstStates.SelectedValue + "</State>";
                inXML += "<ZipCode>" + txtZipCode.Text + "</ZipCode>";
                inXML += "<Country>" + lstCountries.SelectedValue + "</Country>";
                inXML += "<Handicappedaccess>" + DrpHandi.SelectedValue + "</Handicappedaccess>";
                inXML += "<isVIP>" + DrpisVIP.SelectedValue + "</isVIP>";
                inXML += "<isTelepresence>" + DrpisTelepresence.SelectedValue + "</isTelepresence>";//FB 2170
                inXML += "<RoomQueue>" + txtRoomEmail.Text + "</RoomQueue>";//FB 2342
                inXML += "<RoomUID>" + txtRoomUID.Text + "</RoomUID>";//ZD 100196
                inXML += "<MapLink>" + txtMapLink.Text.Replace(">","&gt;").Replace("<","&lt;").Replace("&","&amp;").Replace("%","&#37") + "</MapLink>";
                inXML += "<ParkingDirections>" + txtParkingDirections.Text.Replace(">", "&gt;").Replace("<", "&lt;").Replace("&", "&amp;").Replace("%", "&#37") + "</ParkingDirections>";
                inXML += "<AdditionalComments>" + txtAdditionalComments.Text + "</AdditionalComments>";
                inXML += "<TimezoneID>" + lstTimezone.SelectedValue + "</TimezoneID>";
                inXML += "<Longitude>" + txtLongitude.Text + "</Longitude>";
                inXML += "<Latitude>" + txtLatitude.Text + "</Latitude>";
                inXML += "<Approvers>";
                inXML += "  <Approver1ID>" + Approver0ID.Value + "</Approver1ID>";
                inXML += "  <Approver1Name></Approver1Name>";
                inXML += "  <Approver2ID>" + Approver1ID.Value + "</Approver2ID>";
                inXML += "  <Approver2Name></Approver2Name>";
                inXML += "  <Approver3ID>" + Approver2ID.Value + "</Approver3ID>";
                inXML += "  <Approver3Name></Approver3Name>";
                inXML += "</Approvers>";
                //Endpoint Search -- Start
                //inXML += "<EndpointID>" + lstEndpoint.SelectedValue + "</EndpointID>";
                if (txtEndpoint.Text.Trim() != "")
                    inXML += "<EndpointID>" + hdnEPID.Value + "</EndpointID>";
                else
                    inXML += "<EndpointID></EndpointID>";
                //Endpoint Search -- End
                inXML += "<Custom1></Custom1>";
                inXML += "<Custom2></Custom2>";
                inXML += "<Custom3></Custom3>";
                inXML += "<Custom4></Custom4>";
                inXML += "<Custom5></Custom5>";
                inXML += "<Custom6></Custom6>";
                inXML += "<Custom7></Custom7>";
                inXML += "<Custom8></Custom8>";
                inXML += "<Custom9></Custom9>";
                inXML += "<Custom10></Custom10>";
                //FB 2694 Starts //FB 2065 - Start
                int RoomCategory = 1;
                //ZD 100619 Start
                if (hdnRoomCategory.Value != "")
                    int.TryParse(hdnRoomCategory.Value, out RoomCategory);
                
                if (Request.QueryString["pageid"] != null)
                {
                    RoomCategory = (int)myVRMNet.NETFunctions.RoomCategory.HotdeskingRoom;
                    inXML += "<RoomCategory>" + RoomCategory + "</RoomCategory>";
                }//ZD 100619 Starts
                else if (RoomCategory == 3)
                {
                    RoomCategory = (int)myVRMNet.NETFunctions.RoomCategory.GuestRoom;
                    inXML += "<RoomCategory>" + RoomCategory + "</RoomCategory>";
                }//ZD 100619 End
                
                else if (chkiControlRoom.Checked ==true)//ZD 101098
                {
                    RoomCategory = (int)myVRMNet.NETFunctions.RoomCategory.iControl;
                    inXML += "<RoomCategory>6</RoomCategory>";
                }
                
                else
                {
                    RoomCategory = (int)myVRMNet.NETFunctions.RoomCategory.NormalRoom;
                    inXML += "<RoomCategory>" + RoomCategory + "</RoomCategory>";
                }
                
                //FB 2694 Ends //FB 2065 - End

                inXML += "<Projector>" + lstProjector.SelectedValue + "</Projector>";
                inXML += "<Video>" + lstVideo.SelectedValue + "</Video>";
                inXML += "<DynamicRoomLayout>" + lstDynamicRoomLayout.SelectedValue + "</DynamicRoomLayout>";
                inXML += "<CatererFacility>" + CatererList.SelectedValue + "</CatererFacility>";
                //ZD 102159
                //inXML += "<ServiceType>" + DrpServiceType.SelectedValue + "</ServiceType>";//FB 2219
                inXML += "<ServiceType>-1</ServiceType>";//FB 2219
                //FB 2334 Start
                if(Chkdedicatedvideo.Checked == true)
                    inXML += "<DedicatedVideo>1</DedicatedVideo>";
                else
                    inXML += "<DedicatedVideo>0</DedicatedVideo>";
                //FB 2334 End
                //FB 2390 Start
                if(Chkdedpresentcodec.Checked == true)
                    inXML += "<DedicatedCodec>1</DedicatedCodec>";
                else
                    inXML += "<DedicatedCodec>0</DedicatedCodec>";
                //FB 2390 End
                inXML += "<IsVMR>0</IsVMR>"; //FB 2448 start
                inXML += "<InternalNumber></InternalNumber>";
                inXML += "<ExternalNumber></ExternalNumber>";  //FB 2448 end
                inXML += "<AVOnsiteSupportEmail>" + txtAVOnsiteSupportEmail.Text + "</AVOnsiteSupportEmail>";//FB 2415
                inXML += "<isPublic>0</isPublic>"; //FB 2594
				
                //ZD 101098 START
                if (chkiControlRoom.Checked == true)
                    inXML += "<iControlRoom>1</iControlRoom>";
                else
                    inXML += "<iControlRoom>0</iControlRoom>";
                //ZD 101098 END
                inXML += "<Departments>";
                foreach (ListItem item in DepartmentList.Items)
                {
                    if (item.Selected)
                        inXML += "<Department><ID>" + item.Value + "</ID><Name>" + item.Text + "</Name><SecurityKey></SecurityKey></Department>";
                }

                inXML += "</Departments>";
                
                rmImg = "";//ZD 103569 start
                pathName = "../image/room/";
                inXML += "<RoomImages>"; //Image Project start
                foreach (DataGridItem dgi in dgItems.Items)
                {
                     if (rmImg == "")
                         rmImg = dgi.Cells[0].Text.Trim();
                     else
                         rmImg += "," + dgi.Cells[0].Text.Trim();
                     //inXML += "<Image>";
                     //inXML += "<ActualImage>" + dgi.Cells[1].Text.Trim() + "</ActualImage>";
                     //inXML += "</Image>";
                }
                inXML += "</RoomImages>";
                inXML += "<RoomImageName>" + rmImg + "</RoomImageName>"; 
                if(!string.IsNullOrEmpty(rmImg))
                    inXML += "<RoomImagePath>" + pathName + "</RoomImagePath>";
                //ZD 103569 End

                inXML += "<Images>";

                inXML += "  <Map1>" + hdnUploadMap1.Text + "</Map1>";
                inXML += "  <Map1Image>" + Map1ImageDt.Value.Trim() + "</Map1Image>";

                inXML += "  <Map2>" + hdnUploadMap2.Text + "</Map2>";
                inXML += "  <Map2Image>" + Map2ImageDt.Value.Trim() + "</Map2Image>";

                //FB 2136 start
                //string selImageName = "";
                //string selImageId = "";

                //selImageName = drpSecImgList.SelectedItem.Text;
                //selImageId = drpSecImgList.SelectedValue.Trim();

                //string[] selImage = hdnSelecOption.Value.ToString().Split(',');
                //if (selImage.Length > 1)
                //{
                //    selImageName = selImage[0];
                //    selImageId = selImage[1];

                //    if (selImage[0] != null)
                //        //if (selImage[0].Trim() == "")
                //        {
                //            selImageName = drpSecImgList.SelectedItem.Text;
                //            selImageId = drpSecImgList.SelectedValue.Trim();
                //        }
                //}

                //inXML += "  <Security1>" + selImageName + "</Security1>"; //Image Name
                //inXML += "  <Security1ImageId>" + selImageId + "</Security1ImageId>"; //Image ID

                //inXML += "  <Security2>" + hdnUploadSecurity2.Text + "</Security2>";
                //inXML += "  <Security2Image>" + Sec2ImageDt.Value.Trim() + "</Security2Image>";
                //FB 2136 end

                inXML += "  <Misc1>" + hdnUploadMisc1.Text + "</Misc1>";
                inXML += "  <Misc1Image>" + Misc1ImageDt.Value.Trim() + "</Misc1Image>";

                inXML += "  <Misc2>" + hdnUploadMisc2.Text + "</Misc2>";
                inXML += "  <Misc2Image>" + Misc2ImageDt.Value.Trim() + "</Misc2Image>";

                inXML += "</Images>";
                //FB 2065 - Start  //ZD 103569 start
                if (DrpisTelepresence.SelectedValue == "1")
                    inXML += "<RoomIconTypeId>" + ns_MyVRMNet.vrmAttributeType.Telepresence + "</RoomIconTypeId>";
                else if (lstVideo.SelectedValue == "0" && (RoomCategory == 1 || RoomCategory == 6)) //ZD 104733
                    inXML += "<RoomIconTypeId>" + ns_MyVRMNet.vrmAttributeType.RoomOnly + "</RoomIconTypeId>";
                else if (lstVideo.SelectedValue == "1" && DrpisTelepresence.SelectedValue == "0" && (RoomCategory == 1 || RoomCategory == 6)) //ZD 104733
                    inXML += "<RoomIconTypeId>" + ns_MyVRMNet.vrmAttributeType.AudioOnly + "</RoomIconTypeId>";
                else if (lstVideo.SelectedValue == "1" && DrpisTelepresence.SelectedValue == "0" && RoomCategory == 4)
                    inXML += "<RoomIconTypeId>" + ns_MyVRMNet.vrmAttributeType.HotdeskingAudio + "</RoomIconTypeId>";
                else if (lstVideo.SelectedValue == "2" && DrpisTelepresence.SelectedValue == "0" && (RoomCategory == 1 || RoomCategory == 6)) //ZD 104733
                    inXML += "<RoomIconTypeId>" + ns_MyVRMNet.vrmAttributeType.Video + "</RoomIconTypeId>";
                else if (lstVideo.SelectedValue == "2" && DrpisTelepresence.SelectedValue == "0" && RoomCategory == 4)
                    inXML += "<RoomIconTypeId>" + ns_MyVRMNet.vrmAttributeType.HotdeskingVideo + "</RoomIconTypeId>";
                else if (RoomCategory == 3) //ZD 100619
                    inXML += "<RoomIconTypeId>" + ns_MyVRMNet.vrmAttributeType.GuestVideo + "</RoomIconTypeId>";
                else
                    inXML += "<RoomIconTypeId></RoomIconTypeId>";

                //FB 2065 - End 103569 End
                //ZD 101244 start
                string sectemp = "0"; //ZD 102168
                if (!string.IsNullOrEmpty(hdnHelpReq.Value))
                {
                    sectemp = "1";
                    inXML += "<UsersSecurityemail>" + hdnHelpReq.Value + "</UsersSecurityemail>";
                }
                else
                    inXML += "<UsersSecurityemail></UsersSecurityemail>";

                inXML += "<Securityemail>" + txtsecurityemail.Text + "</Securityemail>";

                string temp = "0";
                if (ChkSecure.Checked == true) //ZD 102168
                    temp ="1";
                if (sectemp == "1" || temp == "1")
                    temp = "1";
                inXML += "<Secure>" + temp + "</Secure>"; //ZD 102168
                //ZD 101244 End

                inXML += "</SetRoomProfile>";
                log.Trace("SetRoomProfile Inxml: " + inXML);
                String outXML = obj.CallMyVRMServer("SetRoomProfile", inXML, Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetRoomProfile Outxml: " + outXML);

                if (outXML.IndexOf("<error>") >= 0)
                {
                    /* *** Code added for FB 1425 QA Bug -Start *** */

                    if (Application["Client"].ToString().ToUpper().Equals(ns_MyVRMNet.vrmClient.MOJ))
                    {
                        errLabel.Text = obj.ShowErrorMessage(outXML);
                        if (errLabel.Text.ToString().ToUpper().Contains("CONFERENCES"))
                            errLabel.Text = errLabel.Text.ToString().Replace("conferences", "hearings");
                    }
                    else
                    /* *** Code added for FB 1425 QA Bug -End *** */
                    errLabel.Text = obj.ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                    return false;
                }
                else
                {
                    string innerXML = "";
                    bool newroom = true;
                    /* code commented for ZD 104482
                    //ZD 103496 Starts                    
                    
                    DataTable dtTable = null;
                    XmlNode node = null;
                    string disabled = "0";

                    loadRooms = new XmlDocument();
                    loadRooms.LoadXml(outXML);

                    ndeRoomID = loadRooms.SelectSingleNode("Rooms/Room/RoomID");
                    if (ndeRoomID != null)
                        roomId = ndeRoomID.InnerText;
                    node = loadRooms.SelectSingleNode("Rooms/Room/Disabled");
                    if (node != null)
                        disabled = node.InnerText.Trim();

                    

                    #region Memcache
                    DataTable dtaddTable = null;
                    DataRow[] RoomOldRows = null;
                    DataSet dsCache = null;
                    DataView dv = null;
                    DataTable dtNewTable = null;
                    bool bRet = false;
                    if (memcacheEnabled == 1 )
                    {
                        using (memClient = new Enyim.Caching.MemcachedClient())
                        {
                            dtNewTable = new DataTable();
                            obj.GetTablefromCache(ref memClient, ref dsCache);

                            if (dsCache.Tables.Count > 0)
                                dtaddTable = dsCache.Tables[0];

                            bRet = obj.GetAllRoomsInfo(roomId, disabled, ref dtNewTable);

                            if (!bRet || dtNewTable.Rows.Count <= 0)
                            {
                                //dtTable = memClient.Get<DataTable>("myVRMRoomsDatatable");
                                obj.GetAllRoomsInfo("", "0", ref dtaddTable, ref dsCache);
                            }
                            RoomOldRows = dtaddTable.Select("Roomid = '" + roomId + "'");

                            if (RoomOldRows != null && RoomOldRows.Count() > 0)
                            {
                                dtaddTable.Rows.Remove(RoomOldRows[0]);
                                dtaddTable.ImportRow(dtNewTable.Rows[0]);
                            }
                            else
                                dtaddTable.ImportRow(dtNewTable.Rows[0]);


                            dv = new DataView(dtaddTable);
                            dv.Sort = "Tier1Name,Tier2Name,RoomName ASC";
                            dtaddTable = dv.ToTable();

                            obj.AddRoomTabletoCache(ref memClient, ref dsCache);
                            //memClient.Store(Enyim.Caching.Memcached.StoreMode.Set, "myVRMRoomsDatatable", dtTable);

                        }
                    }

                    #endregion

                    //ZD 103496 End*/

                    roomxmlPath = Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\" + Session["RoomXmlPath"].ToString();//FB 1830
                    if (File.Exists(roomxmlPath))
                    {
                        loadRooms = new XmlDocument();
                        loadRooms.LoadXml(outXML);
                        ndeRoomID = loadRooms.SelectSingleNode("Rooms/Room/RoomID");
                        if (ndeRoomID != null)
                            roomId = ndeRoomID.InnerText;
                        if (roomId != "")
                        {
                            createRooms = new XmlDocument();
                            if (!obj.WaitForFile(roomxmlPath))
                            {
                                return false;
                            }
                            else
                                createRooms.Load(roomxmlPath);

                            roomList = createRooms.SelectNodes("Rooms/Room");
                            ndeRoomID = loadRooms.SelectSingleNode("Rooms/Room");
                            innerXML = ndeRoomID.InnerXml;
                            if (roomList != null)
                            {
                                for (int a = 0; a < roomList.Count; a++)
                                {
                                    if (roomList[a].SelectSingleNode("RoomID").InnerText.Trim() == roomId.Trim())
                                    {
                                        newroom = false;
                                        if (ndeRoomID != null)
                                            roomList[a].InnerXml = ndeRoomID.InnerXml;
                                    }
                                        
                                }

                                if (newroom)
                                {
                                    if (innerXML != "")
                                    {
                                        ndeRoomID = createRooms.CreateElement("Room");
                                        ndeRoomID.InnerXml = innerXML;
                                        createRooms.SelectSingleNode("Rooms").AppendChild(ndeRoomID);
                                    }
                                }

                                if (File.Exists(roomxmlPath))
                                {
                                    if (obj.WaitForFile(roomxmlPath))
                                    {
                                        File.Delete(roomxmlPath);
                                        createRooms.Save(roomxmlPath);
                                    }
                                }
                            }

                        }

                    }
                    return true;
                }
               
            }
            catch (Exception ex)
            {
                log.Trace("SetRoomProfile: " + ex.Message + " : " + ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region SubmitAddNewRoomProfile
        protected void SubmitAddNewRoomProfile(Object sender, EventArgs e)
        {
            try
            {
                String roomxmlPath = HttpContext.Current.Request.MapPath(".").ToString() + "\\" + Session["RoomXmlPath"].ToString();
                //FB 2694 - Start
                if (SetRoomProfile())
                {
                    if (Request.QueryString["pageid"] == "Hotdesking")
                        Response.Redirect("ManageRoomProfile.aspx?rID=new&cal=2&pageid=Hotdesking");
                    else
                        Response.Redirect("ManageRoomProfile.aspx?rID=new&cal=2");
                }
                //FB 2694 - End
                else
                {
                    if (obj == null)
                        obj = new myVRMNet.NETFunctions();


                    if (File.Exists(roomxmlPath))
                    {
                        if (obj.WaitForFile(roomxmlPath))
                            File.Delete(roomxmlPath);
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace("SubmitAddNewRoomProfile" + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region RemoveFile1
        protected void RemoveFile1(Object sender, CommandEventArgs e)
        {
            try
            {
                Label lblTemp = new Label();
                HtmlInputFile inTemp = new HtmlInputFile();
                Label lblFname = new Label();
                Label lblHdnName = new Label();
                Button btnTemp = new Button();
                switch (e.CommandArgument.ToString())
                {
                    case "1":
                        lblTemp = hdnUploadMap1;
                        inTemp = fleMap1;
                        lblFname = lblUploadMap1;
                        lblHdnName = hdnUploadMap1;
                        btnTemp = btnRemoveMap1;
                        break;
                    case "2":
                        lblTemp = hdnUploadMap2;
                        inTemp = fleMap2;
                        lblFname = lblUploadMap2;
                        lblHdnName = hdnUploadMap2;
                        btnTemp = btnRemoveMap2;
                        break;
                    // FB 2136 Starts
                    //case "3":
                    //    lblTemp = hdnUploadSecurity1;
                    //    inTemp = fleSecurity1;
                    //    lblFname = lblUploadSecurity1;
                    //    lblHdnName = hdnUploadSecurity1;
                    //    btnTemp = btnRemoveSecurity1;
                    //    break;
                    //case "4":
                    //    lblTemp = hdnUploadSecurity2;
                    //    inTemp = fleSecurity2;
                    //    lblFname = lblUploadSecurity2;
                    //    lblHdnName = hdnUploadSecurity2;
                    //    btnTemp = btnRemoveSecurity2;
                    //    break;
                    // FB 2136 Ends
                    case "5":
                        lblTemp = hdnUploadMisc1;
                        inTemp = fleMisc1;
                        lblFname = lblUploadMisc1;
                        lblHdnName = hdnUploadMisc1;
                        btnTemp = btnRemoveMisc1;
                        break;
                    case "6":
                        lblTemp = hdnUploadMisc2;
                        inTemp = fleMisc2;
                        lblFname = lblUploadMisc2;
                        lblHdnName = hdnUploadMisc2;
                        btnTemp = btnRemoveMisc2;
                        break;
                }
                //Response.Write("hdnUpload" + e.CommandArgument.ToString());
                if (lblTemp.Text != "")
                {
                    //Response.Write(e.CommandArgument + " : " + lblTemp.Text);
//                    FileInfo fi = new FileInfo(lblTemp.Text);
                    FileInfo fi = new FileInfo(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en"+"\\upload\\" + lblTemp.Text);//FB 1830
                    if (hdnRoomID.Value == "new")
                    {
                        if (fi.Exists)
                            fi.Delete();
                    }
                    inTemp.Visible = true;
                    inTemp.Disabled = false;
                    lblFname.Visible = false;
                    lblFname.Text = "";
                    lblHdnName.Visible = false;
                    lblHdnName.Text = "";
                    btnTemp.Visible = false;
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region RemoveFile
        protected void RemoveFile(Object sender, CommandEventArgs e)
        {
            try
            {
                Label lblTemp = new Label();
                HtmlInputFile inTemp = new HtmlInputFile();
                Label lblFname = new Label();
                Label lblHdnName = new Label();
                Button btnTemp = new Button();
                HtmlInputHidden tempHdn = new HtmlInputHidden(); 
                myVRMWebControls.ImageControl tempImg = new myVRMWebControls.ImageControl();
                switch (e.CommandArgument.ToString())
                {
                    case "1":
                        lblTemp = hdnUploadMap1;
                        inTemp = fleMap1;
                        lblFname = lblUploadMap1;
                        lblHdnName = hdnUploadMap1;
                        btnTemp = btnRemoveMap1;
                        tempImg = Map1ImageCtrl;
                        tempHdn = Map1ImageDt;
                        break;
                    case "2":
                        lblTemp = hdnUploadMap2;
                        inTemp = fleMap2;
                        lblFname = lblUploadMap2;
                        lblHdnName = hdnUploadMap2;
                        btnTemp = btnRemoveMap2;
                        tempImg = Map2ImageCtrl;
                        tempHdn = Map2ImageDt;
                        break;
                    // FB 2136 Starts
                    //case "3":
                    //    lblTemp = hdnUploadSecurity1;
                    //    inTemp = fleSecurity1;
                    //    lblFname = lblUploadSecurity1;
                    //    lblHdnName = hdnUploadSecurity1;
                    //    btnTemp = btnRemoveSecurity1;
                    //    tempImg = Sec1ImageCtrl;
                    //    tempHdn = Sec1ImageDt;
                    //    break;
                    //case "4":
                    //    lblTemp = hdnUploadSecurity2;
                    //    inTemp = fleSecurity2;
                    //    lblFname = lblUploadSecurity2;
                    //    lblHdnName = hdnUploadSecurity2;
                    //    btnTemp = btnRemoveSecurity2;
                    //    tempImg = Sec2ImageCtrl;
                    //    tempHdn = Sec2ImageDt;
                    //    break;
                    // FB 2136 Ends
                    case "5":
                        lblTemp = hdnUploadMisc1;
                        inTemp = fleMisc1;
                        lblFname = lblUploadMisc1;
                        lblHdnName = hdnUploadMisc1;
                        btnTemp = btnRemoveMisc1;
                        tempImg = Misc1ImageCtrl;
                        tempHdn = Misc1ImageDt;
                        break;
                    case "6":
                        lblTemp = hdnUploadMisc2;
                        inTemp = fleMisc2;
                        lblFname = lblUploadMisc2;
                        lblHdnName = hdnUploadMisc2;
                        btnTemp = btnRemoveMisc2;
                        tempImg = Misc2ImageCtrl;
                        tempHdn = Misc2ImageDt;
                        break;
                }

                if (lblTemp.Text != "")
                {
                    tempImg.Dispose();
                    tempImg.Visible = false;
                    inTemp.Visible = true;
                    lblFname.Visible = false;
                    lblFname.Text = "";
                    lblHdnName.Visible = false;
                    lblHdnName.Text = "";
                    btnTemp.Visible = false;
                    tempHdn.Value = "";
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region UploadFiles
        protected void UploadFiles(Object sender, EventArgs e)
        {
            try
            {
                //Response.Write((FileUpload1.Value.LastIndexOf("\\") + 1) + " : " + (FileUpload1.Value.Length - 1));
                String fName;
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData;
                String errMsg = "";
                String fPath = "";
                fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/en/") + 3) + "/Upload/";//FB 1830
                if (!fleMap1.Value.Equals(""))
                {
                    fName = Path.GetFileName(fleMap1.Value);
                    //ZD 100263 Starts
                    if (!CheckFileWhiteList(fName))
                    {
                        errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                        errLabel.Visible = true;
                        return;
                    }
                    //ZD 100263 End
                    myFile = fleMap1.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (nFileLen <= 10000000)
                    {
                        //WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en"+ "\\upload\\" + fName, ref myData); //FB 1830
                        
                        lblUploadMap1.Text = "<a href='" + fPath + fName +"' target='_blank'>" + fName + "</a>";
                        hdnUploadMap1.Text = fName;
                        fleMap1.Visible = false;
                        lblUploadMap1.Visible = true;
                        btnRemoveMap1.Visible = true;
                    }
                    else
                        errMsg += "Map 1 attachment is greater than 10MB. File has not been uploaded.";
                }
                if (!fleMap2.Value.Equals(""))
                {
                    fName = Path.GetFileName(fleMap2.Value);
                    //ZD 100263 Starts
                    if (!CheckFileWhiteList(fName))
                    {
                        errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                        errLabel.Visible = true;
                        return;
                    }
                    //ZD 100263 End
                    myFile = fleMap2.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (fleMap2.Value.Equals(fleMap1.Value))
                        errMsg += "Map 2 attachment has already been uploaded in Map 1.";
                    else
                        if (nFileLen <= 10000000)
                        {
                            //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                            WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\upload\\" + fName, ref myData);//FB 1830

                            lblUploadMap2.Text = "<a href='" + fPath + fName +"' target='_blank'>" + fName + "</a>";
                            hdnUploadMap2.Text = fName;
                            fleMap2.Visible = false;
                            lblUploadMap2.Visible = true;
                            btnRemoveMap2.Visible = true;
                        }
                        else
                            errMsg += "Map 2 attachment is greater than 10MB. File has not been uploaded.";
                }
                // FB 2136 Starts
                //if (!fleSecurity1.Value.Equals(""))
                //{
                //    fName = Path.GetFileName(fleSecurity1.Value);
                //    myFile = fleSecurity1.PostedFile;
                //    nFileLen = myFile.ContentLength;
                //    myData = new byte[nFileLen];
                //    myFile.InputStream.Read(myData, 0, nFileLen);
                //    if (nFileLen <= 10000000)
                //    {
                //        //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                //        WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\upload\\" + fName, ref myData);//FB 1830
                //        lblUploadSecurity1.Text = "<a href='" + fPath + fName +"' target='_blank'>" + fName + "</a>";
                //        hdnUploadSecurity1.Text = fName;
                //        fleSecurity1.Visible = false;
                //        lblUploadSecurity1.Visible = true;
                //        btnRemoveSecurity1.Visible = true;
                //    }
                //    else
                //        errMsg += "Security 1 attachment is greater than 10MB. File has not been uploaded.";
                //}
                //if (!fleSecurity2.Value.Equals(""))
                //{
                //    fName = Path.GetFileName(fleSecurity2.Value);
                //    myFile = fleSecurity2.PostedFile;
                //    nFileLen = myFile.ContentLength;
                //    myData = new byte[nFileLen];
                //    myFile.InputStream.Read(myData, 0, nFileLen);
                //    if (fleSecurity2.Value.Equals(fleSecurity1.Value))
                //        errMsg += "Security 2 attachment has already been uploaded in Security 1.";
                //    else
                //        if (nFileLen <= 10000000)
                //        {
                //            //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                //            WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\upload\\" + fName, ref myData);//FB 1830
                //            lblUploadSecurity2.Text = "<a href='" + fPath + fName +"' target='_blank'>" + fName + "</a>";
                //            hdnUploadSecurity2.Text = fName;
                //            fleSecurity2.Visible = false;
                //            lblUploadSecurity2.Visible = true;
                //            btnRemoveSecurity2.Visible = true;
                //        }
                //        else
                //            errMsg += "Security 2 attachment is greater than 10MB. File has not been uploaded.";
                //}
                // FB 2136 Ends
                if (!fleMisc1.Value.Equals(""))
                {
                    fName = Path.GetFileName(fleMisc1.Value);
                    //ZD 100263 Starts
                    if (!CheckFileWhiteList(fName))
                    {
                        errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                        errLabel.Visible = true;
                        return;
                    }
                    //ZD 100263 End
                    myFile = fleMisc1.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (nFileLen <= 10000000)
                    {
                        //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                        WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\upload\\" + fName, ref myData);//FB 1830
                        lblUploadMisc1.Text = "<a href='" + fPath + fName +"' target='_blank'>" + fName + "</a>";
                        hdnUploadMisc1.Text = fName;
                        fleMisc1.Visible = false;
                        lblUploadMisc1.Visible = true;
                        btnRemoveMisc1.Visible = true;
                    }
                    else
                        errMsg += "Misc 1 attachment is greater than 10MB. File has not been uploaded.";
                }
                if (!fleMisc2.Value.Equals(""))
                {
                    fName = Path.GetFileName(fleMisc2.Value);
                    //ZD 100263 Starts
                    if (!CheckFileWhiteList(fName))
                    {
                        errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                        errLabel.Visible = true;
                        return;
                    }
                    //ZD 100263 End
                    //Response.Write(HttpContext.Current.Request.MiscPath(".").ToString() + "\\" + fName);
                    myFile = fleMisc2.PostedFile;
                    nFileLen = myFile.ContentLength;
                    //Response.Write(nFileLen.ToString());
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    if (fleMisc2.Value.Equals(fleMisc1.Value))
                        errMsg += "Misc 2 attachment has already been uploaded in Misc 1.";
                    else
                        if (nFileLen <= 10000000)
                        {
                            //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                            WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\en" + "\\upload\\" + fName, ref myData);//FB 1830
                            lblUploadMisc2.Text = "<a href='" + fPath + fName +"' target='_blank'>" + fName + "</a>";
                            hdnUploadMisc2.Text = fName;
                            fleMisc2.Visible = false;
                            lblUploadMisc2.Visible = true;
                            btnRemoveMisc2.Visible = true;
                        }
                        else
                            errMsg += "Misc 2 attachment is greater than 10MB. File has not been uploaded.";
                }
                errLabel.Text = errMsg;
                errLabel.Visible = true;
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files." + ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region WriteToFile
        private void WriteToFile(string strPath, ref byte[] Buffer)
        {
            try
            {
                // Create a file
                FileStream newFile = new FileStream(strPath, FileMode.Create);

                // Write data to the file
                newFile.Write(Buffer, 0, Buffer.Length);

                // Close file
                newFile.Close();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetUploadedFiles
        protected void GetUploadedFiles(XmlNode node, int cmd)
        {
            try
            {
                    if (!node.InnerText.Equals(""))
                    {
                        String fPath = node.InnerText;
                        if (node.InnerText != "")
                        {
                            //fPath = fPath.Replace("\\", "/");
                            //int startIndex = fPath.IndexOf("/en/");
                            //int len = fPath.Length - 1;
                            ////Response.Write(fPath + " : " + startIndex + " : " + len);
                            //fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/en/") + 3) + fPath.Substring(startIndex + 3);
                            fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/en/") + 3) + "/Upload/" + node.InnerText;//FB 1830
                            switch (cmd)
                            {
                                case 1:
                                    lblUploadMap1.Text = "<a href='" + fPath + "' target='_blank'>" + getUploadFilePath(node.InnerText) + "</a>";
                                    //lblUploadMap1.Text = "<a href='" + fPath + "' target='_blank' onMouseOver=\"toggleDiv('roomdiv" + cmd + "',1);\" onMouseOut=\"toggleDiv('roomdiv" + cmd + "',0);\" >" + node.InnerText + "</a>";
                                    //lblUploadMap1.Text += "<div class='hdiv' id='roomdiv" + cmd + "'><img src='" + fPath + "'/></div>";
                                    
                                    hdnUploadMap1.Text = node.InnerText;
                                    btnRemoveMap1.Visible = true;
                                    fleMap1.Visible = false;
                                    lblUploadMap1.Visible = true;
                                    break;
                                case 2:
                                    lblUploadMap2.Text = "<a href='" + fPath + "' target='_blank'>" + getUploadFilePath(node.InnerText) + "</a>";
                                    //lblUploadMap2.Text = "<a href='" + fPath + "' target='_blank' onMouseOver=\"toggleDiv('roomdiv" + cmd + "',1);\" onMouseOut=\"toggleDiv('roomdiv" + cmd + "',0);\" >" + node.InnerText + "</a>";
                                    //lblUploadMap2.Text += "<div class='hdiv' id='roomdiv" + cmd + "'><img src='" + fPath + "'/></div>";
                                    hdnUploadMap2.Text = node.InnerText;
                                    btnRemoveMap2.Visible = true;
                                    fleMap2.Visible = false;
                                    lblUploadMap2.Visible = true;
                                    break;
                                // FB 2136 Starts
                                //case 3:
                                //    lblUploadSecurity1.Text = "<a href='" + fPath + "' target='_blank'>" + getUploadFilePath(node.InnerText) + "</a>";
                                //    //lblUploadSecurity1.Text = "<a href='" + fPath + "' target='_blank' onMouseOver=\"toggleDiv('roomdiv" + cmd + "',1);\" onMouseOut=\"toggleDiv('roomdiv" + cmd + "',0);\" >" + node.InnerText + "</a>";
                                //    //lblUploadSecurity1.Text += "<div class='hdiv' id='roomdiv" + cmd + "'><img src='" + fPath + "'/></div>";
                                //    hdnUploadSecurity1.Text = node.InnerText;
                                //    btnRemoveSecurity1.Visible = true;
                                //    fleSecurity1.Visible = false;
                                //    lblUploadSecurity1.Visible = true;
                                //    break;
                                //case 4:
                                //    lblUploadSecurity2.Text = "<a href='" + fPath + "' target='_blank'>" + getUploadFilePath(node.InnerText) + "</a>";
                                //    //lblUploadSecurity2.Text = "<a href='" + fPath + "' target='_blank' onMouseOver=\"toggleDiv('roomdiv" + cmd + "',1);\" onMouseOut=\"toggleDiv('roomdiv" + cmd + "',0);\" >" + node.InnerText + "</a>";
                                //    //lblUploadSecurity2.Text += "<div class='hdiv' id='roomdiv" + cmd + "'><img src='" + fPath + "'/></div>";
                                //    hdnUploadSecurity2.Text = node.InnerText;
                                //    btnRemoveSecurity2.Visible = true;
                                //    fleSecurity2.Visible = false;
                                //    lblUploadSecurity2.Visible = true;
                                //    break;
                                // FB 2136 Ends
                                case 5:
                                    lblUploadMisc1.Text = "<a href='" + fPath + "' target='_blank'>" + getUploadFilePath(node.InnerText) + "</a>";
                                    //lblUploadMisc1.Text = "<a href='" + fPath + "' target='_blank' onMouseOver=\"toggleDiv('roomdiv" + cmd + "',1);\" onMouseOut=\"toggleDiv('roomdiv" + cmd + "',0);\" >" + node.InnerText + "</a>";
                                    //lblUploadMisc1.Text += "<div class='hdiv' id='roomdiv" + cmd + "'><img src='" + fPath + "'/></div>";
                                    hdnUploadMisc1.Text = node.InnerText;
                                    btnRemoveMisc1.Visible = true;
                                    fleMisc1.Visible = false;
                                    lblUploadMisc1.Visible = true;
                                    break;
                                case 6:
                                    lblUploadMisc2.Text = "<a href='" + fPath + "' target='_blank'>" + getUploadFilePath(node.InnerText) + "</a>";
                                    //lblUploadMisc2.Text = "<a href='" + fPath + "' target='_blank' onMouseOver=\"toggleDiv('roomdiv" + cmd + "',1);\" onMouseOut=\"toggleDiv('roomdiv" + cmd + "',0);\" >" + node.InnerText + "</a>";
                                    //lblUploadMisc2.Text += "<div class='hdiv' id='roomdiv" + cmd + "'><img src='" + fPath + "'/></div>";
                                    hdnUploadMisc2.Text = node.InnerText;
                                    btnRemoveMisc2.Visible = true;
                                    fleMisc2.Visible = false;
                                    lblUploadMisc2.Visible = true;
                                    break;
                            }
                        }
                    }

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region getUploadFilePath
        protected string getUploadFilePath(string fpn)
        {
            String fPath = String.Empty;
            if (fpn.Equals(""))
                fPath = "";
            else
            {
                char[] splitter = { '\\' };
                String[] fa = fpn.Split('\\');
                if (fa.Length.Equals(0))
                    fPath = "";
                else
                    fPath = fa[fa.Length - 1];
            }
            return fPath;
        }
        #endregion

        #region UpdateStates
        protected void UpdateStates(Object sender, EventArgs e)
        {
            try
            {
                lstStates.Items.Clear();
                if (!lstCountries.SelectedValue.Equals("-1"))
                    obj.GetCountryStates(lstStates, lstCountries.SelectedValue);
            }
            catch (Exception ex)
            {
                log.Trace("UpdateStates: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        // Image Project code starts...

        #region UploadRoomImage
        /// <summary>
        /// UploadRoomImage
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UploadRoomImage(Object sender, EventArgs e)
        {
            try
            {
                sessionKey = "RoomImg" + Session.SessionID;
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData = null;
                String errMsg = "";
                string imageString = "";
                System.Drawing.Image img = null; //ZD 101611
                errLabel.Text = "";
                if (roomImageDt == null)
                {
                    roomImageDt = new DataTable();
                    roomImageDt.Columns.Add(new DataColumn("ImageName"));
                    roomImageDt.Columns.Add(new DataColumn("Image"));
                    roomImageDt.Columns.Add(new DataColumn("Imagetype"));
                    roomImageDt.Columns.Add(new DataColumn("ImagePath"));
                }
                FillRoomDataTable();

                if (!roomfileimage.Value.Equals(""))
                {
                    fName = Path.GetFileName(roomfileimage.Value);

                    //ZD 100263 Starts
                    if (!CheckFileWhiteList(fName))
                    {
                        errLabel.Text = obj.GetTranslatedText("File type is invalid. Please select a new file and try again.");
                        errLabel.Visible = true;
                        return;
                    }
                    //ZD 100263 End

                    myFile = roomfileimage.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    
                    fileext = Path.GetExtension(roomfileimage.Value);

                    //pathName = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en/image/room/" + fName;//FB 1830
                    //pathName = "../en/image/room/" + fName;//FB 1830 //ZD 103569
                    pathName = "../image/room/";
                    if (File.Exists(pathName))
                        errMsg += "Room image already exists.";
                    else
                    {
                        //pathName = Path.GetFullPath(roomfileimage.Value);
                        myFile.InputStream.Read(myData, 0, nFileLen);
                        //ZD 101611 start
                        img = System.Drawing.Image.FromStream(myFile.InputStream);
                        if (img.Width > 250 || img.Height > 250)
                        {
                            errLabel.Text = obj.GetTranslatedText("Room image width and height should not exceed 250 pixels.");
                            errLabel.Visible = true;
                            throw new Exception(errLabel.Text);
                        }
                        //ZD 101611 End
                        if (nFileLen <= 5000000)
                        {
                            //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\image\\room\\" + fName, ref myData);
                            if (!Directory.Exists(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\room"))
                            {
                                Directory.CreateDirectory(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\room");
                            }
                            WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\image\\room\\" + fName, ref myData);//FB 1830

                            imageString = imageUtilObj.ConvertByteArrToBase64(myData);

                            DataRow dr = roomImageDt.NewRow();
                            dr["ImageName"] = fName;
                            dr["Imagetype"] = fileext;
                            dr["Image"] = "";
                            dr["ImagePath"] = pathName + fName;

                            roomImageDt.Rows.Add(dr);

                            dgItems.DataSource = roomImageDt;
                            dgItems.DataBind();

                            dgItems.Visible = true;
                        }
                        else
                            errMsg += "Room image is greater than 5MB. File has not been uploaded.";
                    }
                }
                
                if(errMsg != "")
                {
                    errLabel.Text = errMsg;
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files." + ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion

        #region FillRoomDataTable

        private void FillRoomDataTable()
        {
            try
            {
                foreach (DataGridItem dgi in dgItems.Items)
                {
                    DataRow dr = roomImageDt.NewRow();

                    dr["ImageName"] = dgi.Cells[0].Text;
                    dr["Imagetype"] = dgi.Cells[2].Text;
                    dr["Image"] = dgi.Cells[1].Text;
                    dr["ImagePath"] = dgi.Cells[3].Text;

                    roomImageDt.Rows.Add(dr);
                }
             }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("FillRoomDatatable" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }

        }
        #endregion

        #region RemoveImage
        protected void RemoveImage(Object sender, DataGridCommandEventArgs e)
        {
            try
            {
                string imgName = dgItems.Items[e.Item.ItemIndex].Cells[0].Text;
                
                roomImageDt = new DataTable();
                roomImageDt.Columns.Add(new DataColumn("ImageName"));
                roomImageDt.Columns.Add(new DataColumn("Image"));
                roomImageDt.Columns.Add(new DataColumn("Imagetype"));
                roomImageDt.Columns.Add(new DataColumn("ImagePath"));

                foreach (DataGridItem dgi in dgItems.Items)
                {
                    if (!dgi.ItemIndex.Equals(e.Item.ItemIndex))
                    {
                        DataRow dr = roomImageDt.NewRow();

                        dr["ImageName"] = dgi.Cells[0].Text;
                        dr["Imagetype"] = dgi.Cells[2].Text;
                        dr["Image"] = dgi.Cells[1].Text;
                        dr["ImagePath"] = dgi.Cells[3].Text;
                        
                        roomImageDt.Rows.Add(dr);
                    }
                }
                
                if (roomImageDt.Rows.Count > 0)
                    dgItems.DataSource = roomImageDt;
                else
                    dgItems.DataSource = null;

                dgItems.DataBind();
                dgItems.Visible = true;
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("RemoveImage" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region BindRowsDeleteMessage
        protected void BindRowsDeleteMessage(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    LinkButton btnTemp = (LinkButton)e.Item.FindControl("btnDelete");
                    btnTemp.Attributes.Add("onclick", "return confirm('" + obj.GetTranslatedText("Are you sure you want to delete this Item?") + "')"); //FB japnese
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("BindRowDeleteMsg" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region BindImages - on item databound not used
        /// <summary>
        /// BindImages - on item databound
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void BindImages(Object sender, DataGridItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType.Equals(ListItemType.Item) || e.Item.ItemType.Equals(ListItemType.AlternatingItem))
                {
                    DataRowView row = e.Item.DataItem as DataRowView;

                    
                    
                    string imCont = "";

                    if (row["ImageName"] != null)
                        fName = row["ImageName"].ToString().Trim();

                    //if (fName != "")
                    //    fileext = fName.Substring(fName.LastIndexOf(".") + 1);

                    if (row["Imagetype"] != null)
                        fileext = row["Imagetype"].ToString().Trim();

                    if (row["Image"] != null)
                        imCont = row["Image"].ToString().Trim();

                    if (imCont != "")
                    {
                        imgArray = imageUtilObj.ConvertBase64ToByteArray(imCont);
                        myVRMWebControls.ImageControl imgCtrl = (myVRMWebControls.ImageControl)e.Item.FindControl("itemImage");

                        MemoryStream ms = new MemoryStream(imgArray, 0, imgArray.Length);
                        ms.Write(imgArray, 0, imgArray.Length);

                        if (fileext == ns_MyVRMNet.vrmImageFormattype.gif)
                            imgCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                        else
                            imgCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                        imgCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                        //imgCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms);
                        imgCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(ms);

                        imCont = null;
                        imgCtrl.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = obj.ShowSystemMessage();//ZD 100263
                log.Trace("BindImages" + ex.Message);//ZD 100263
                errLabel.Visible = true;
            }
        }
        #endregion

        #region UploadOtherImages
        /// <summary>
        /// UploadOtherImages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void UploadOtherImages(Object sender, EventArgs e)
        {
            try
            {
                String fName;
                HttpPostedFile myFile;
                int nFileLen;
                byte[] myData = null;
                String errMsg = "";
                String fPath = "";
                string fileExtn = "";
                System.Drawing.Image img = null;//ZD 102700
                fPath = Request.Url.ToString().Substring(0, Request.Url.ToString().IndexOf("/" + language + "/") + 1) + "en/Upload/";//FB 1830
                if (!fleMap1.Value.Equals(""))
                {
                    fName = Path.GetFileName(fleMap1.Value);
                    myFile = fleMap1.PostedFile;
                    nFileLen = myFile.ContentLength;

                    if (fName != "")
                        fileExtn = fName.Substring(fName.LastIndexOf(".") + 1);

                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    //ZD 102700 - Start 
                    img = System.Drawing.Image.FromStream(myFile.InputStream);
                    if (img.Width > 250 || img.Height > 250)
                    {
                        errLabel.Text = obj.GetTranslatedText("Map 1 attachment width and height should not exceed 250 pixels.");
                        errLabel.Visible = true;
                        throw new Exception(errLabel.Text);
                    }
                    //ZD 102700 - End
                    if (nFileLen <= 5000000)
                    {
                        if (!Directory.Exists(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\room"))
                        {
                            Directory.CreateDirectory(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\room");
                        }
                        WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\image\\Room\\" + fName, ref myData);
                        //lblUploadMap1.Text = fName;
                        //lblUploadMap1.Text = "<a href='" + fPath + fName + "' target='_blank'>" + fName + "</a>";

                        hdnUploadMap1.Text = fName;
                        Map1ImageDt.Value = "../image/Room/"; 
                        //Map1ImageDt.Value = imageUtilObj.ConvertByteArrToBase64(myData);

                        MemoryStream ms1 = new MemoryStream(myData, 0, myData.Length);
                        ms1.Write(myData, 0, myData.Length);

                        if (fileExtn.ToLower() == "gif")
                            Map1ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                        else
                            Map1ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                        Map1ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;

                        //BitmapImage image = new BitmapImage(new Uri("/MyProject;component/Images/down.png", UriKind.Relative));
                        //(System.Drawing.Bitmap) bitMapImage = new System.Drawing.Bitmap(HttpContext.Current.Server.MapPath("a.jpg"));
                         
                        //Map1ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);
                        //Map1ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms1);
                        //ms1.Close();

                        Bitmap bitMapImageMap1 = new System.Drawing.Bitmap(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + Map1ImageDt.Value + fName);
                        Map1ImageCtrl.Bitmap = bitMapImageMap1;

                        myData = null;
                        Map1ImageCtrl.Visible = true;
                        fleMap1.Visible = false;
                        //lblUploadMap1.Visible = true;
                        btnRemoveMap1.Visible = true;
                    }
                    else
                        errMsg += "Map 1 attachment is greater than 5MB. File has not been uploaded.";
                }
                if (!fleMap2.Value.Equals(""))
                {
                    fName = Path.GetFileName(fleMap2.Value);
                    myFile = fleMap2.PostedFile;
                    nFileLen = myFile.ContentLength;

                    if (fName != "")
                        fileExtn = fName.Substring(fName.LastIndexOf(".") + 1);

                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    //ZD 102700 - Start 
                    img = System.Drawing.Image.FromStream(myFile.InputStream);
                    if (img.Width > 250 || img.Height > 250)
                    {
                        errLabel.Text = obj.GetTranslatedText("Map 2 attachment width and height should not exceed 250 pixels.");
                        errLabel.Visible = true;
                        throw new Exception(errLabel.Text);
                    }
                    //ZD 102700 - End
                    if (fleMap2.Value.Equals(fleMap1.Value))
                        errMsg += "Map 2 attachment has already been uploaded in Map 1.";
                    else
                        if (nFileLen <= 5000000)
                        {
                            if (!Directory.Exists(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\room"))
                            {
                                Directory.CreateDirectory(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\room");
                            }
                            WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\image\\Room\\" + fName, ref myData);
                            //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                            //lblUploadMap2.Text = fName;
                            //lblUploadMap2.Text = "<a href='" + fPath + fName + "' target='_blank'>" + fName + "</a>";
                            hdnUploadMap2.Text = fName;
                            Map2ImageDt.Value = "../image/Room/";
                            //Map2ImageDt.Value = imageUtilObj.ConvertByteArrToBase64(myData);

                            MemoryStream ms2 = new MemoryStream(myData, 0, myData.Length);
                            ms2.Write(myData, 0, myData.Length);

                            if (fileExtn.ToLower() == "gif")
                                Map2ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                            else
                                Map2ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                            Map2ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                            //Map2ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms);
                            //Map2ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Image.FromStream(ms2);
                            Bitmap bitMapImageMap2 = new System.Drawing.Bitmap(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + Map2ImageDt.Value + fName);
                            Map2ImageCtrl.Bitmap = bitMapImageMap2;
                            //ms2.Close();
                            myData = null;
                            Map2ImageCtrl.Visible = true;
                            fleMap2.Visible = false;
                            lblUploadMap2.Visible = true;
                            btnRemoveMap2.Visible = true;
                        }
                        else
                            errMsg += "Map 2 attachment is greater than 5MB. File has not been uploaded.";
                }
                // FB 2136 Starts
                //if (!fleSecurity1.Value.Equals(""))
                //{
                //    fName = Path.GetFileName(fleSecurity1.Value);
                //    if (fName != "")
                //        fileExtn = fName.Substring(fName.LastIndexOf(".") + 1);

                //    myFile = fleSecurity1.PostedFile;
                //    nFileLen = myFile.ContentLength;
                //    myData = new byte[nFileLen];
                //    myFile.InputStream.Read(myData, 0, nFileLen);
                //    if (nFileLen <= 5000000)
                //    {
                //        hdnUploadSecurity1.Text = fName;
                //        Sec1ImageDt.Value = "";
                //        Sec1ImageDt.Value = imageUtilObj.ConvertByteArrToBase64(myData);

                //        MemoryStream ms3 = new MemoryStream(myData, 0, myData.Length);
                //        ms3.Write(myData, 0, myData.Length);

                //        if (fileExtn.ToLower() == "gif")
                //            Sec1ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                //        else
                //            Sec1ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                //        Sec1ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                //        Sec1ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms3);
                //        //ms3.Close();
                //        myData = null;
                //        Sec1ImageCtrl.Visible = true;
                //        fleSecurity1.Visible = false;
                //        lblUploadSecurity1.Visible = true;
                //        btnRemoveSecurity1.Visible = true;
                //    }
                //    else
                //        errMsg += "Security 1 attachment is greater than 5MB. File has not been uploaded.";
                //}
                //if (!fleSecurity2.Value.Equals(""))
                //{
                //    fName = Path.GetFileName(fleSecurity2.Value);
                //    if (fName != "")
                //        fileExtn = fName.Substring(fName.LastIndexOf(".") + 1);

                //    myFile = fleSecurity2.PostedFile;
                //    nFileLen = myFile.ContentLength;
                //    myData = new byte[nFileLen];
                //    myFile.InputStream.Read(myData, 0, nFileLen);
                //    if (fleSecurity2.Value.Equals(fleSecurity1.Value))
                //        errMsg += "Security 2 attachment has already been uploaded in Security 1.";
                //    else
                //        if (nFileLen <= 5000000)
                //        {
                //            //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                //            //lblUploadSecurity2.Text = fName;
                //            //lblUploadSecurity2.Text = "<a href='" + fPath + fName + "' target='_blank'>" + fName + "</a>";
                //            hdnUploadSecurity2.Text = fName;
                //            Sec2ImageDt.Value = "";
                //            Sec2ImageDt.Value = imageUtilObj.ConvertByteArrToBase64(myData);

                //            MemoryStream ms4 = new MemoryStream(myData, 0, myData.Length);
                //            ms4.Write(myData, 0, myData.Length);

                //            if (fileExtn.ToLower() == "gif")
                //                Sec2ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                //            else
                //                Sec2ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                //            Sec2ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                //            Sec2ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms4);
                //            //ms4.Close();
                //            myData = null;
                //            Sec2ImageCtrl.Visible = true;
                //            fleSecurity2.Visible = false;
                //            lblUploadSecurity2.Visible = true;
                //            btnRemoveSecurity2.Visible = true;
                //        }
                //        else
                //            errMsg += "Security 2 attachment is greater than 5MB. File has not been uploaded.";
                //}
                // FB 2136 Ends
                if (!fleMisc1.Value.Equals(""))
                {
                    fName = Path.GetFileName(fleMisc1.Value);
                    if (fName != "")
                        fileExtn = fName.Substring(fName.LastIndexOf(".") + 1);

                    myFile = fleMisc1.PostedFile;
                    nFileLen = myFile.ContentLength;
                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    //ZD 102700 - Start 
                    img = System.Drawing.Image.FromStream(myFile.InputStream);
                    if (img.Width > 250 || img.Height > 250)
                    {
                        errLabel.Text = obj.GetTranslatedText("Misc 1 attachment width and height should not exceed 250 pixels.");
                        errLabel.Visible = true;
                        throw new Exception(errLabel.Text);
                    }
                    //ZD 102700 - End
                    if (nFileLen <= 5000000)
                    {
                        if (!Directory.Exists(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\room"))
                        {
                            Directory.CreateDirectory(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\room");
                        }
                        WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\image\\Room\\" + fName, ref myData);
                        //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                        //lblUploadMisc1.Text = fName;
                        //lblUploadMisc1.Text = "<a href='" + fPath + fName + "' target='_blank'>" + fName + "</a>";
                        hdnUploadMisc1.Text = fName;
                        Misc1ImageDt.Value = "../image/Room/";
                        //Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\image\\Room\\" + fName;
                        //Misc1ImageDt.Value = imageUtilObj.ConvertByteArrToBase64(myData);

                        MemoryStream ms5 = new MemoryStream(myData, 0, myData.Length);
                        ms5.Write(myData, 0, myData.Length);

                        if (fileExtn.ToLower() == "gif")
                            Misc1ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                        else
                            Misc1ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                        Misc1ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                        //Misc1ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms5);
                        Bitmap bitMapImageMisc1 = new System.Drawing.Bitmap(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + Misc1ImageDt.Value + fName);
                        Misc1ImageCtrl.Bitmap = bitMapImageMisc1;
                        //ms5.Close();
                        myData = null;
                        Misc1ImageCtrl.Visible = true;
                        fleMisc1.Visible = false;
                        lblUploadMisc1.Visible = true;
                        btnRemoveMisc1.Visible = true;
                    }
                    else
                        errMsg += "Misc 1 attachment is greater than 5MB. File has not been uploaded.";
                }
                if (!fleMisc2.Value.Equals(""))
                {
                    fName = Path.GetFileName(fleMisc2.Value);
                    if (fName != "")
                        fileExtn = fName.Substring(fName.LastIndexOf(".") + 1);

                    myFile = fleMisc2.PostedFile;
                    nFileLen = myFile.ContentLength;

                    myData = new byte[nFileLen];
                    myFile.InputStream.Read(myData, 0, nFileLen);
                    //ZD 102700 - Start 
                    img = System.Drawing.Image.FromStream(myFile.InputStream);
                    if (img.Width > 250 || img.Height > 250)
                    {
                        errLabel.Text = obj.GetTranslatedText("Misc 2 attachment width and height should not exceed 250 pixels.");
                        errLabel.Visible = true;
                        throw new Exception(errLabel.Text);
                    }
                    //ZD 102700 - End
                    if (fleMisc2.Value.Equals(fleMisc1.Value))
                        errMsg += "Misc 2 attachment has already been uploaded in Misc 1.";
                    else
                        if (nFileLen <= 5000000)
                        {
                            if (!Directory.Exists(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\room"))
                            {
                                Directory.CreateDirectory(HttpContext.Current.Request.MapPath("..").ToString() + "\\image\\" + "\\room");
                            }
                            WriteToFile(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + "\\image\\Room\\" + fName, ref myData);
                            //WriteToFile(HttpContext.Current.Request.MapPath(".").ToString() + "\\upload\\" + fName, ref myData);
                            //lblUploadMisc2.Text = fName;
                            //lblUploadMisc2.Text = "<a href='" + fPath + fName + "' target='_blank'>" + fName + "</a>";
                            hdnUploadMisc2.Text = fName;
                            Misc2ImageDt.Value = "../image/Room/";
                            //Misc2ImageDt.Value = imageUtilObj.ConvertByteArrToBase64(myData);

                            MemoryStream ms6 = new MemoryStream(myData, 0, myData.Length);
                            ms6.Write(myData, 0, myData.Length);

                            if (fileExtn.ToLower() == "gif")
                                Misc2ImageCtrl.ImageType = myVRMWebControls.ImageType.Gif;
                            else
                                Misc2ImageCtrl.ImageType = myVRMWebControls.ImageType.Jpeg;

                            Misc2ImageCtrl.PersistenceType = myVRMWebControls.Persistence.Cache;
                           // Misc2ImageCtrl.Bitmap = (System.Drawing.Bitmap)System.Drawing.Bitmap.FromStream(ms6);
                            Bitmap bitMapImageMisc2 = new System.Drawing.Bitmap(Directory.GetParent(HttpContext.Current.Request.MapPath(".").ToString()) + Misc2ImageDt.Value +  fName);
                            Misc2ImageCtrl.Bitmap = bitMapImageMisc2;
                            //ms6.Close();
                            myData = null;
                            Misc2ImageCtrl.Visible = true;
                            fleMisc2.Visible = false;
                            lblUploadMisc2.Visible = true;
                            btnRemoveMisc2.Visible = true;
                        }
                        else
                            errMsg += "Misc 2 attachment is greater than 5MB. File has not been uploaded.";
                }
                if (errLabel.Text == "")
                {
                    errLabel.Text = errMsg;
                    errLabel.Visible = true;
                }
            }
            catch (Exception ex)
            {
                log.Trace("Error in uploading files." + ex.StackTrace + " : " + ex.Message);
                errLabel.Visible = true;
            }
        }
        #endregion
                
        // Image Project code ends...

        //Code added for Endpoint Search - Start

        #region BindEndpoint
        protected void BindEndpoint(Object sender, EventArgs e)
        {
            try
            {
                if (hdnEPID.Value != "")
                    txtEndpoint.Text = lstEndpoint.Items.FindByValue(hdnEPID.Value.Trim()).Text;
                
                Session.Remove("EndpointXML"); //FB 1886
            }
            catch (Exception ex)
            {
                log.Trace("ResetRoomProfile" + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //Code added for Endpoint Search - Start


        //ZD 100263 Starts
        #region CheckFileWhiteList
        /// <summary>
        /// CheckFileWhiteList
        /// </summary>
        /// <param name="actualImageName"></param>
        /// <returns></returns>
        private bool CheckFileWhiteList(string actualImageName)
        {
            try
            {
                string filextn = "";
                string[] roomNameList = null;
                List<string> strExtension = null;
                bool filecontains = false;

                if (actualImageName != "")
                {
                    roomNameList = actualImageName.Split(',');
                    for (int k = 0; k < roomNameList.Length; k++)
                    {
                        filextn = roomNameList[k].Split('.')[roomNameList[k].Split('.').Length - 1];
                        strExtension = new List<string> { "jpg", "jpeg", "png", "bmp", "gif" };
                        filecontains = strExtension.Contains(filextn, StringComparer.OrdinalIgnoreCase);
                        if (!filecontains)
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                log.Trace("CheckFileWhiteList" + ex.Message);
                return false;
            }
        }
        #endregion
        //ZD 100263 End

        //ZD 101026 Starts
        #region LoadHistory
        /// <summary>
        /// LoadHistory
        /// </summary>
        /// <param name="changedHistoryNodes"></param>
        /// <returns></returns>
        private bool LoadHistory(XmlNodeList changedHistoryNodes)
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            try
            {
                DataTable dtable = new DataTable();
                if (!dtable.Columns.Contains("ModifiedUserId"))
                    dtable.Columns.Add("ModifiedUserId");
                if (!dtable.Columns.Contains("ModifiedDateTime"))
                    dtable.Columns.Add("ModifiedDateTime");
                if (!dtable.Columns.Contains("ModifiedUserName"))
                    dtable.Columns.Add("ModifiedUserName");
                if (!dtable.Columns.Contains("Description"))
                    dtable.Columns.Add("Description");

                if (changedHistoryNodes != null && changedHistoryNodes.Count > 0)
                {
                    dtable = obj.LoadDataTable(changedHistoryNodes, null);
                    DateTime ModifiedDateTime = DateTime.Now;

                    for (Int32 i = 0; i < dtable.Rows.Count; i++)
                    {
                        DateTime.TryParse(dtable.Rows[i]["ModifiedDateTime"].ToString(), out ModifiedDateTime);
                        dtable.Rows[i]["ModifiedDateTime"] = myVRMNet.NETFunctions.GetFormattedDate(ModifiedDateTime.Date.ToString()) + " " + ModifiedDateTime.ToString(tformat);

                        dtable.Rows[i]["Description"] = obj.GetTranslatedText(dtable.Rows[i]["Description"].ToString());
                    }
                }
                dgChangedHistory.DataSource = dtable;
                dgChangedHistory.DataBind();

            }
            catch (Exception ex)
            {
                log.Trace("LoadHistory" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion
        //ZD 101026 End
        //ZD 101244 Start
        #region UpdateSecuredetails
        protected void UpdateSecuredetails(Object sender, EventArgs e)
        {
            try
            {
                String inxml = "";
                inxml += "<GetLocations>";
                inxml += obj.OrgXMLElement();//Organization Module Fixes
                inxml += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "  <Tier1ID>" + lstTopTier.SelectedValue + "</Tier1ID>";
                inxml += "</GetLocations>";
                String outxml = obj.CallMyVRMServer("GetLocationsTier1", inxml, Application["MyVRMServer_ConfigPath"].ToString());
                if (outxml.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);
                    if (xmldoc.SelectSingleNode("//GetLocations/Secure") != null && xmldoc.SelectSingleNode("//GetLocations/Secure").InnerText == "1")
                    {
                        ChkSecure.Checked = true;
                        ChkSecure.Enabled = false;
                    }
                    else
                    {
                        ChkSecure.Enabled = true;
                        ChkSecure.Checked = false;
                    }               
                    if (xmldoc.SelectSingleNode("//GetLocations/Secure") != null && xmldoc.SelectSingleNode("//GetLocations/Secure").InnerText == "1")
                    {
                        if (xmldoc.SelectSingleNode("//GetLocations/Securityemail") != null && xmldoc.SelectSingleNode("//GetLocations/Secure").InnerText == "1")
                        {
                            txtsecurityemail.Text = xmldoc.SelectSingleNode("//GetLocations/Securityemail").InnerText;
                        }
                    }
                    else
                        txtsecurityemail.Text = "";
                }
                log.Trace("GetLocationsTier1 outxml: " + outxml);
            }
            catch (Exception ex)
            {
                log.Trace("UpdateSecuredetails: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region UpdateCheckSecuredetails
        protected void UpdateCheckSecuredetails(Object sender, EventArgs e)
        {
            try
            {
                String inxml = "";
                inxml += "<GetLocations>";
                inxml += obj.OrgXMLElement();//Organization Module Fixes
                inxml += "  <UserID>" + Session["userID"].ToString() + "</UserID>";
                inxml += "  <Tier2ID>" + lstMiddleTier.SelectedValue + "</Tier2ID>";
                inxml += "</GetLocations>";
                String outxml = obj.CallMyVRMServer("GetLocationsTier2", inxml, Application["MyVRMServer_ConfigPath"].ToString());
                if (outxml.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outxml);
                    if (xmldoc.SelectSingleNode("//GetLocations/Secure") != null && xmldoc.SelectSingleNode("//GetLocations/Secure").InnerText == "1")
                    {
                        ChkSecure.Checked = true;
                        ChkSecure.Enabled = false;
                    }
                    else
                    {
                        ChkSecure.Enabled = true;
                        ChkSecure.Checked = false;
                    }
                    if (xmldoc.SelectSingleNode("//GetLocations/Secure") != null && xmldoc.SelectSingleNode("//GetLocations/Secure").InnerText == "1")
                    {
                        if (xmldoc.SelectSingleNode("//GetLocations/Securityemail") != null && xmldoc.SelectSingleNode("//GetLocations/Secure").InnerText == "1")
                        {
                            txtsecurityemail.Text = xmldoc.SelectSingleNode("//GetLocations/Securityemail").InnerText;
                        }
                    }
                    else
                        txtsecurityemail.Text = "";
                   
                }
                log.Trace("GetLocationsTier2 outxml: " + outxml);
            }
            catch (Exception ex)
            {
                log.Trace("UpdateCheckSecuredetails: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 101244 End

        //ZD 101611 start
        #region SetRoomImage
        /// <summary>
        /// SetRoomImage
        /// </summary>
        /// <param name="imageString"></param>
        /// <param name="fname"></param>
        /// <param name="ImageID"></param>
        protected void SetRoomImage(string imageString,string ImageID)
        {
            StringBuilder inXML = new StringBuilder();
            try
            {
                inXML.Append("<SetRoomImage>");
                inXML.Append(obj.OrgXMLElement());
                inXML.Append("<userID>" + Session["userID"].ToString() + "</userID>");
                inXML.Append("<ImageID>" + ImageID + "</ImageID>");
                inXML.Append("<ActualImage>" + imageString + "</ActualImage>");
                inXML.Append("</SetRoomImage>");
                String outxml = obj.CallMyVRMServer("setRoomResizeImage", inXML.ToString(), Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("SetRoomProfile Outxml: " + outxml);
            }
            catch (Exception ex)
            {
                log.Trace("SetRoomImage: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 101611 End
    }
}