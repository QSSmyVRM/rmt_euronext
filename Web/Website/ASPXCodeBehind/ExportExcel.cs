﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Runtime.InteropServices;
using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.IO;
using System.Diagnostics;
using System.Drawing;


    public class ExportExcel : System.Web.UI.Page
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Form1"/> class.
        /// </summary>
        public ExportExcel()
        {
        }
      
        /// <summary>
        /// Generates the report.
        /// </summary>
        public void GenerateReport(DataTable dt, String sheetName, string strNote, string heading, string filePath)
        {
            try
            {
                using (ExcelPackage p = new ExcelPackage())
                {
                    //set the workbook properties and add a default sheet in it
                    //SetWorkbookProperties(p);
                    //Create a sheet
                    ExcelWorksheet ws = CreateSheet(p, sheetName);
                    
                    //Merging cells and create a center heading for out table
                    int rowIndex = 1;
                    if (heading != "")
                    {                        
                        ws.Cells[1, 1].Value = heading;
                        ws.Cells[1, 1, 1, dt.Columns.Count].Merge = true;
                        ws.Cells[1, 1, 1, dt.Columns.Count].Style.Font.Bold = true;
                        ws.Cells[1, 1, 1, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        rowIndex++;
                    }

                    if (strNote != "")
                        CreateNoteHeaderORFooter(ws, ref rowIndex, dt, strNote);

                    CreateHeader(ws, ref rowIndex, dt);
                    CreateData(ws, ref rowIndex, dt);
                    
                    

                    //AddComment(ws, 5, 10, "Zeeshan Umar's Comments", "Zeeshan Umar");
                    //AddCustomShape(ws, 10, 7, eShapeStyle.Ellipse, "Text inside Ellipse.");

                    //Generate A File with Random name
                    Byte[] bin = p.GetAsByteArray();
                    //string file = Request.MapPath(".") +"\\Image\\"+ fileName + ".xlsx";
                    File.WriteAllBytes(filePath, bin);

                    ////These lines will open it in Excel
                    //ProcessStartInfo pi = new ProcessStartInfo(filePath);
                    //Process.Start(pi);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static ExcelWorksheet CreateSheet(ExcelPackage p, string sheetName)
        {
            p.Workbook.Worksheets.Add(sheetName);
            ExcelWorksheet ws = p.Workbook.Worksheets[1];
            ws.Name = sheetName; //Setting Sheet's name
            ws.Cells.Style.Font.Size = 11; //Default font size for whole sheet
            ws.Cells.Style.Font.Name = "Calibri"; //Default Font name for whole sheet

            return ws;
        }

        /// <summary>
        /// Sets the workbook properties and adds a default sheet.
        /// </summary>
        /// <param name="p">The p.</param>
        /// <returns></returns>
        private static void SetWorkbookProperties(ExcelPackage p)
        {
            //Here setting some document properties
            p.Workbook.Properties.Author = "Zeeshan Umar";
            p.Workbook.Properties.Title = "EPPlus Sample";

            
        }

        private static void CreateHeader(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {            
            int colIndex = 1;
            rowIndex++;
            foreach (DataColumn dc in dt.Columns) //Creating Headings
            {
                var cell = ws.Cells[rowIndex, colIndex];

                //Setting the background color of header cells to Gray
                var fill = cell.Style.Fill;
                fill.PatternType = ExcelFillStyle.Solid;
                fill.BackgroundColor.SetColor(Color.Gray);

                //Setting Top/left,right/bottom borders.
                var border = cell.Style.Border;
                border.Bottom.Style = border.Top.Style = border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;

                //Setting Value in cell
                cell.Value = dc.ColumnName;

                double cellSize = ws.Cells[colIndex, colIndex].Worksheet.Column(colIndex).Width;
                double proposedCellSize = cell.Value.ToString().Length * 1.3;
                if (cellSize <= proposedCellSize)
                {
                    ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = proposedCellSize;
                }

                colIndex++;
            }
           
        }

        private static void CreateData(ExcelWorksheet ws, ref int rowIndex, DataTable dt)
        {
            int colIndex=0;
            foreach (DataRow dr in dt.Rows) // Adding Data into rows
            {
                colIndex = 1;
                rowIndex++;

                foreach (DataColumn dc in dt.Columns)
                {
                    var cell = ws.Cells[rowIndex, colIndex];

                    //Setting Value in cell
                    cell.Value = dr[dc.ColumnName];

                    //Setting borders of cell
                    var border = cell.Style.Border;
                    border.Left.Style = border.Right.Style = ExcelBorderStyle.Thin;
                    if(dc.ColumnName.ToLower() == "time zone" || dc.ColumnName.ToLower() == "email")
                        ws.Cells[rowIndex, colIndex].Worksheet.Column(colIndex).Width = cell.Value.ToString().Length * 1.3;
                    colIndex++;
                }
            }
        }

        private static void CreateNoteHeaderORFooter(ExcelWorksheet ws, ref int rowIndex, DataTable dt, string strNote)
        {
            //rowIndex++;
            ws.Cells[rowIndex, 1].Value = strNote;
            //ws.Cells[rowIndex, 1, rowIndex, dt.Columns.Count].Merge = true;
            ws.Cells[rowIndex, 1, rowIndex, dt.Columns.Count].Style.Font.Bold = true;
            ws.Cells[rowIndex, 1, rowIndex, dt.Columns.Count].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
            ws.Cells[rowIndex, 1, rowIndex, dt.Columns.Count].Style.VerticalAlignment = ExcelVerticalAlignment.Justify;
            ws.Cells[rowIndex, 1, rowIndex + 4, dt.Columns.Count].Merge = true;            

            rowIndex = rowIndex + 4 ;

            //int colIndex = 0;
            //foreach (DataColumn dc in dt.Columns) //Creating Formula in footers
            //{
            //    if (colIndex > 0)
            //        break;
            //    colIndex++;
            //    var cell = ws.Cells[rowIndex, colIndex];

            //    //Setting Sum Formula
            //    //cell.Formula = "Sum(" + ws.Cells[3, colIndex].Address + ":" + ws.Cells[rowIndex - 1, colIndex].Address + ")";
            //    cell.Value = strFoorter;
            //    //Setting Background fill color to Gray
            //    cell.Style.Fill.PatternType = ExcelFillStyle.Solid;
            //    cell.Style.Fill.BackgroundColor.SetColor(Color.Gray);
            //}
        }

        /// <summary>
        /// Adds the custom shape.
        /// </summary>
        /// <param name="ws">Worksheet</param>
        /// <param name="colIndex">Column Index</param>
        /// <param name="rowIndex">Row Index</param>
        /// <param name="shapeStyle">Shape style</param>
        /// <param name="text">Text for the shape</param>
        private static void AddCustomShape(ExcelWorksheet ws, int colIndex, int rowIndex, eShapeStyle shapeStyle, string text)
        {
            ExcelShape shape = ws.Drawings.AddShape("cs" + rowIndex.ToString() + colIndex.ToString(), shapeStyle);
            shape.From.Column = colIndex;
            shape.From.Row = rowIndex;
            shape.From.ColumnOff = Pixel2MTU(5);
            shape.SetSize(100, 100);
            shape.RichText.Add(text);
        }

        /// <summary>
        /// Adds the image in excel sheet.
        /// </summary>
        /// <param name="ws">Worksheet</param>
        /// <param name="colIndex">Column Index</param>
        /// <param name="rowIndex">Row Index</param>
        /// <param name="filePath">The file path</param>
        private static void AddImage(ExcelWorksheet ws, int columnIndex, int rowIndex, string filePath)
        {
            //How to Add a Image using EP Plus
            Bitmap image = new Bitmap(filePath);
            ExcelPicture picture = null;
            if (image != null)
            {
                picture = ws.Drawings.AddPicture("pic" + rowIndex.ToString() + columnIndex.ToString(), image);
                picture.From.Column = columnIndex;
                picture.From.Row = rowIndex;
                picture.From.ColumnOff = Pixel2MTU(2); //Two pixel space for better alignment
                picture.From.RowOff = Pixel2MTU(2);//Two pixel space for better alignment
                picture.SetSize(100, 100);
            }
        }

        /// <summary>
        /// Adds the comment in excel sheet.
        /// </summary>
        /// <param name="ws">Worksheet</param>
        /// <param name="colIndex">Column Index</param>
        /// <param name="rowIndex">Row Index</param>
        /// <param name="comments">Comment text</param>
        /// <param name="author">Author Name</param>
        private static void AddComment(ExcelWorksheet ws, int colIndex, int rowIndex, string comment, string author)
        {
            //Adding a comment to a Cell
            var commentCell = ws.Cells[rowIndex, colIndex];
            commentCell.AddComment(comment, author);
        }

        /// <summary>
        /// Pixel2s the MTU.
        /// </summary>
        /// <param name="pixels">The pixels.</param>
        /// <returns></returns>
        public static int Pixel2MTU(int pixels)
        {
            int mtus = pixels * 9525;
            return mtus;
        }
       
    }

