﻿/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml;
using DevExpress.Web;
using DevExpress.Web.ASPxGridView;
using DevExpress.Web.ASPxEditors;


public partial class en_VNOCparticipantlist : System.Web.UI.Page
{
    #region protected Members
    protected DevExpress.Web.ASPxGridView.ASPxGridView grid;
    protected System.Web.UI.WebControls.Label lblVNOC;
    string partystring = "", confvnoc = "";
    int crossSilo = 0; //FB 2766
    int ConfOrg = 11; //FB 2764
    #endregion

    myVRMNet.NETFunctions obj;
    ns_Logger.Logger log;

    //ZD 101022
    #region InitializeCulture
    protected override void InitializeCulture()
    {
        UICulture = Session["UserCulture"].ToString();
        Culture = Session["UserCulture"].ToString();
        base.InitializeCulture();
    }
    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (obj == null)
                obj = new myVRMNet.NETFunctions();
            obj.AccessandURLConformityCheck("VNOCparticipantlist.aspx", Request.Url.AbsoluteUri.ToLower()); // ZD 100263

            obj = new myVRMNet.NETFunctions();
            log = new ns_Logger.Logger();
            grid.Settings.ShowFilterRow = true;

            if (Request.QueryString["cvnoc"] != null)
                confvnoc = Request.QueryString["cvnoc"].ToString().Trim();

            if (Request.QueryString["frm"] != null) //FB 2766
                if (Request.QueryString["frm"].ToString().Equals("1"))
                    crossSilo = 1;

            int.TryParse(Session["OrganizationID"].ToString(), out ConfOrg);
            if (Request.QueryString["ConfOrg"] != null) //FB 2764
                int.TryParse(Request.QueryString["ConfOrg"].ToString().Trim(), out ConfOrg);

            if (Request.QueryString["partys"] != null)
                partystring = Request.QueryString["partys"].ToString().Trim().Replace("@@", "++");

            lblVNOC.Attributes.Add("style", "display:none");
            if ((Session["SiteCongSupport"].ToString() == "1" && (ConfOrg > 11)) || crossSilo == 1) //FB 2764 //FB 2820
                lblVNOC.Attributes.Add("style", "display:block");

            

            SetVNOCParticipant();
        }
        catch (Exception ex)
        {
            log.Trace(ex.ToString());
        }

    }

    public DataTable populateVNOCusers()
    {
        string inXML1 = "";
        string outXML1 = "";
        XmlDocument xmldoc = null;
        DataTable dt = null;
        try
        {
            inXML1 = "<login>" + obj.OrgXMLElement() + "<userID>" + Session["userID"].ToString() + "</userID><isCrossSilo>" + crossSilo + "</isCrossSilo></login>"; //FB 2766
            outXML1 = obj.CallMyVRMServer("GetVNOCUserList", inXML1, Application["COM_ConfigPath"].ToString());
            xmldoc = new XmlDocument();
            dt = new DataTable();
            if (outXML1.IndexOf("Error") <= 0)
            {
                xmldoc.LoadXml(outXML1);
                XmlNodeList nodes = xmldoc.SelectNodes("//users");
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                if (ds.Tables.Count > 0)
                {
                    dt = ds.Tables[0];

                    if (!dt.Columns.Contains("ifrmDetails"))
                        dt.Columns.Add("ifrmDetails");

                    foreach (DataRow dr in dt.Rows)
                        dr["ifrmDetails"] = dr["userID"].ToString() + "|" + dr["firstName"].ToString() + "|" + dr["lastName"].ToString() + "|" + dr["userEmail"].ToString();

                    Session["dtVNOC"] = dt;
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
        return dt;
    }

    protected void SetVNOCParticipant()
    {
        try
        {
            grid.DataSource = populateVNOCusers();
            grid.DataBind();

        }
        catch (Exception ex)
        {
            log.Trace(ex.ToString());
        }
    }

    protected void ASPxGridView1_DataBound(object sender, EventArgs e)
    {
        bool isuserpresent = false;
        try
        {
            string[] delimiter = { "!!" };
            string[] delimiter1 = { "||" };
            if (!IsPostBack)
            {
                if (partystring != "")
                {
                    string[] partys = partystring.Split(delimiter1, StringSplitOptions.RemoveEmptyEntries);

                    for (int j = 0; j < partys.Length; j++)
                    {
                        isuserpresent = false;
                        string temp = partys[j].ToString();
                        int tmpUserId = 0;
                        if (temp != "")
                        {
                            for (int i = 0; i < this.grid.VisibleRowCount; i++)
                            {
                                tmpUserId = 0;
                                Int32.TryParse(temp.Split(delimiter, StringSplitOptions.RemoveEmptyEntries)[0], out tmpUserId); //FB 1888
                                if (this.grid.GetRowValues(i, "userID") != null)
                                {

                                    if (Convert.ToInt32(this.grid.GetRowValues(i, "userID")) == tmpUserId)
                                    {
                                        this.grid.Selection.SelectRow(i);
                                        isuserpresent = true;
                                    }
                                }
                            }
                        }
                    }
                }
                GridRowCommand();
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.ToString());
        }

    }

    protected void GridRowCommand()
    {
        try
        {

            DataTable dtx = (DataTable)Session["dtVNOC"];
            int cnt = dtx.Rows.Count;

            foreach (DataRow dr in dtx.Rows)
            {
                if (confvnoc.IndexOf(dr["userID"].ToString()) > -1)
                {
                    grid.Selection.SetSelection(dtx.Rows.IndexOf(dr), true);
                }
            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.ToString());
        }
    }

    protected void HtmlDataCellPrepared(object sender, ASPxGridViewTableDataCellEventArgs e)
    {
        try
        {
            string Fname = "";
            if (e.DataColumn.FieldName == "firstName")
            {
                if (e.CellValue.ToString().IndexOf('*') > -1)
                {
                    string[] str = e.CellValue.ToString().Split(' ');
                    for (int i = 0; i < str.Length; i++)
                    {
                        if (i == str.Length - 1)
                            Fname += "<span style='color:red'> " + str[i] + "</span>";
                        else
                        {
                            if (i == 0)
                                Fname += str[i];
                            else
                                Fname += " " + str[i];
                        }
                    }
                    e.Cell.Text = Fname;
                }

            }
        }
        catch (Exception ex)
        {
            log.Trace(ex.ToString());
        }

    }

}
