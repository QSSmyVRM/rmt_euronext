/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 //ZD 100886
using System;
using System.Data;
using System.Xml;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using System.Configuration;
using System.Xml.Schema;
using System.Web.Security;
using System.Web.Util;
using System.Linq;//Added for FB 1712
using System.Xml.Linq;
//ZD 100152 Starts
using Google.Apis.Calendar.v3;
using Google.Apis.Calendar.v3.Data;
//ZD 100152 Ends
using System.Drawing;//ZD 101611
using System.Drawing.Imaging;//ZD 101611
using System.Drawing.Drawing2D;
using System.Globalization;
using System.Threading;//ZD 101611
using System.IO.Compression;



namespace myVRMNet
{
    /// <summary>
    /// This is a generic class which has common functions that are used across the website.
    /// Created by: Saima Ahmad
    /// </summary>
    public class NETFunctions
    {
        #region Data Members
        /// <summary>
        /// Data Members
        /// </summary>

        private string errXML;
        public Label errLabel;
        public ns_Logger.Logger log;

        public const int defaultOrgID = 11; //Organization Module Fixes
        private string organizationID;
        private string orgXMLElement = "<organizationID>11</organizationID>";

        public int remainingRooms = 0;
        public int remainingMCUs = 0;
        public int remainingUsers = 0;
        public int remExchangeUsers = 0;
        public int remDominoUsers = 0;
        public int remMobileUsers = 0; //FB 1979
        public int remWebexUsers = 0; //ZD 100221
        public int remBJNUsers = 0, MaxBJNUsers = 0; //ZD 103550
        public int remainingVRooms = 0;
        public int remainingNVRooms = 0;
        public int remainingEndPoints = 0;
        public int remainingFacilities = 0;
        public int remainingCatering = 0;
        public int remainingHouseKeeping = 0;
        public int remainingAPI = 0;
        //public int remainingPC = 0; //FB 2347 //FB 2693
        public int remainingEnchancedMCUs = 0;//FB 2486
        public Int32 OrgSetupTime = 0, OrgTearDownTime = 0, EnableBufferZone = 0; //FB 2398
        public bool isBufferChecked = false; //FB 2398
        //FB 2426 Start
        public int remainingExtRooms = 0; 
        public int remainingGstRoomPerUser = 0;
        //FB 2426 End
        public int remainingVMRRooms = 0;//FB 2586
        public int maxOrganizations = 1; //FB 1639
        public int enableCloud = 0; //FB 2262  //FB 2599
        public int enablePublicRooms = 0; //FB 2645
        public int remainingPCUsers, remainingJabber, remainingLync, remainingVidtel, remainingVidyo = 0; //FB 2693
        public string remainingBJ;//ZD 103550

        private int loginUsrLanguage = 1; //FB 1881
        public int remainingVCHotRooms = 0, remainingROHotRooms = 0; //2694
        public int remainingAdvReport = 0; //FB 2593
        public int remainingiControlRooms = 0;//ZD 101098

        string[] urlAccess = new string[] { "%3c", "%3e", "java", "script", "embed", "iframe", "canvas", "<", ">", "style", "alert", "prompt", "window", "document", "object", "confirm", "location", "document", "history", "src", "href", "meta", "layer", "link", "img", "html", "body", "applet" }; // 100263
        #endregion

        #region Transfer
        /// <summary>
        /// Transfer
        /// </summary>
        /// <param name="xml"></param>
        /// <returns></returns>

        public string Transfer(string xml)
        {
            return xml.Replace(">", "&gt;").Replace("<", "&lt;");
        }
        #endregion

        #region NETFunctions
        /// <summary>
        /// NETFunctions
        /// </summary>
        public NETFunctions()
        {
            //ZD 101714
            if (HttpContext.Current.Session != null && HttpContext.Current.Session["UserCulture"] != null)
            {
                if (HttpContext.Current.Session["UserCulture"].ToString() == "fr-CA")
                {
                    CultureInfo currentCulture;
                    currentCulture = new System.Globalization.CultureInfo("en-US");
                    Thread.CurrentThread.CurrentCulture = currentCulture;
                }
            }

            log = new ns_Logger.Logger();
            errXML = "<error><message>A system error has occurred. Please contact your myVRM system administrator and give them the following error code: </message><level>E</level><errorCode>444</errorCode></error>";//ZD 102432
            errXML = GetErrorMessage(200); //FB 1881
            //GetOrganisationID();
            #region Added for Front-end Facade Layer
            if (NetCmds == null || COMCmds == null || RTCCommands == null)
            {
                GetNetCommands();
                GetCOMCommands();
                GetRTCCommands();
            }
            #endregion
        }
        #endregion

        #region CreateFilePath
        /// <summary>
        /// CreateFilePath
        /// </summary>
        /// <param name="directoryPath"></param>
        /// <param name="filePath"></param>
        /// <param name="xmlStream"></param>
        private void CreateFilePath(string directoryPath, string filePath, string xmlStream)
        {
            // Create the inXML/outXML directory if it doesn't exists
            if (!Directory.Exists(directoryPath))
            {
                // Create the inXML/outXML directory
                DirectoryInfo dir = Directory.CreateDirectory(directoryPath);
            }
            // Create the inXML/outXML file if it doesn't exists.
            if (!File.Exists(filePath))
            {
                // Create the outXML file.
                using (FileStream fs = File.Create(filePath)) { }
            }
            if (!HttpContext.Current.Application["debugMode"].ToString().Equals("ASP"))
            {
                // Open the stream and write to the inXML/outXML file.
                using (FileStream fs = File.Open(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite))
                {
                    Byte[] info = new UTF8Encoding(true).GetBytes(xmlStream);

                    // Add some information to the inXML/outXML file.
                    fs.Write(info, 0, info.Length);
                }
            }
        }
        #endregion

        #region GetICALInfo
        /// <summary>
        /// GetICALInfo
        /// </summary>
        /// <param name="instr"></param>
        /// <returns></returns>
        public string GetICALInfo(string instr)
        {
            string recurpattern = "";
            string[] recurArray = instr.Split('#');
            string[] recurArray0 = recurArray[0].Split('&');
            string[] recurArray1 = recurArray[1].Split('&');
            string[] recurArray2 = recurArray[2].Split('&');
            HttpContext.Current.Response.Write(recurArray1[0]);
            switch (recurArray1[0])
            {
                case "1":
                    recurpattern += "FREQ=DAILY;";
                    recurpattern += "INTERVAL=" + recurArray1[2] + ";";
                    break;
                case "2":
                    recurpattern += "FREQ=DAILY;";
                    recurpattern += "INTERVAL=" + recurArray1[3] + ";";
                    break;
                case "3":
                    recurpattern += "FREQ=MONTHLY;";
                    switch (recurArray1[5])
                    {
                        case "1":
                            recurpattern += "INTERVAL=" + recurArray1[7] + ";" + "BYMONTHDAY=" + recurArray1[6] + ";";
                            break;
                        case "2":
                            recurpattern += "INTERVAL=" + recurArray1[10] + ";" + "BYDAY=" + recurArray1[9] + ";";
                            break;
                        //                        default:
                        //                            HttpContext.Current.Response.Write("An error had occured in Monthly recurring pattern");
                    }
                    break;
                case "4":
                    recurpattern += "FREQ=YEARLY;";
                    switch (recurArray1[11])
                    {
                        case "1":
                            recurpattern += "INTERVAL=1;BYMONTHDAY=" + recurArray1[13] + ";BYMONTH=" + recurArray1[12] + ";";
                            break;
                        case "2":
                            recurpattern += "INTERVAL=1;BYDAY=" + getMonthWeekDay(recurArray1[15]) + ";BYMONTH=" + recurArray1[16] + ";BYSETPOS=" + getMonthWeekSet(recurArray1[14]) + ";";
                            break;
                    }
                    break;
            }
            switch (recurArray2[1])
            {
                case "1":
                case "2":
                    recurpattern += "COUNT=" + recurArray2[2] + ";";
                    break;
                case "3":
                    recurpattern += "UNTIL=" + Convert.ToDateTime(recurArray2[3]).ToString("YYYYMMDD") + "T000000Z" + ";";
                    break;
            }

            return recurpattern;
        }
        #endregion

        #region getWeekDay
        /// <summary>
        /// getWeekDay
        /// </summary>
        /// <param name="weekdayno"></param>
        /// <returns></returns>
        public String getWeekDay(String weekdayno)
        {
            switch (weekdayno)
            {
                case "1":
                    return "SU";
                case "2":
                    return "MO";
                case "3":
                    return "TU";
                case "4":
                    return "WE";
                case "5":
                    return "TH";
                case "6":
                    return "FR";
                case "7":
                    return "SA";
            }
            return "";
        }
        #endregion

        #region getMonthWeekSet
        /// <summary>
        /// getMonthWeekSet
        /// </summary>
        /// <param name="monthweeksetno"></param>
        /// <returns></returns>
        public String getMonthWeekSet(String monthweeksetno)
        {
            switch (monthweeksetno)
            {
                case "1":
                    return "1";
                case "2":
                    return "2";
                case "3":
                    return "3";
                case "4":
                    return "4";
                case "5":
                    return "-1";
            }
            return "";
        }
        #endregion

        #region getMonthWeekDay
        /// <summary>
        /// getMonthWeekDay
        /// </summary>
        /// <param name="monthweekdayno"></param>
        /// <returns></returns>
        public String getMonthWeekDay(String monthweekdayno)
        {
            switch (monthweekdayno)
            {
                case "1":
                    return "SU,MO,TU,WE,TH,FR,SA";
                case "2":
                    return "MO,TU,WE,TH,FR";
                case "3":
                    return "SU,SA";
                default:
                    int t = Int32.Parse(monthweekdayno) - 3;
                    return getWeekDay(t.ToString());
            }
            //return "";
        }
        #endregion

        #region AppendRecur
        /// <summary>
        /// AppendRecur
        /// </summary>
        /// <param name="instr"></param>
        /// <returns></returns>
        public string AppendRecur(string instr, string bufferxml, string timezone)//Code added for  Fb 1728
        {
            
            int TotalDur=0; //FB 2398
            string recurXML = "";
            string[] recurArray = instr.Split('#');
            string[] recurArray0 = recurArray[0].Split('&');
            string[] recurArray1 = recurArray[1].Split('&');
            string[] recurArray2 = recurArray[2].Split('&');

            //FB 2398 start
            Int32.TryParse(recurArray0[4], out TotalDur);
            DateTime BuffSetup = DateTime.Parse(GetDefaultDate(recurArray2[0]) + " " + recurArray0[1] + ":" + recurArray0[2]+" "+recurArray0[3]);
            //FB 2634
            //if (!isBufferChecked && EnableBufferZone == 1)
            //{
            //    BuffSetup = BuffSetup.AddMinutes(-OrgSetupTime);
            //    TotalDur += OrgSetupTime + OrgTearDownTime;
            //}

            recurXML += "<appointmentTime>";

            /* Code changed for Fb 1728*/

            if (HttpContext.Current.Session["timeZoneDisplay"] != null) //FB 1425 
            {
                if (HttpContext.Current.Session["timeZoneDisplay"].ToString() == "0")
                {
                    recurArray0[0] = timezone;
                }
            }
            //if (Int32.Parse(recurArray0[1]) > 12)
            //{
            //    recurArray0[1] = Convert.ToString(Int32.Parse(recurArray0[1]) - 12);
            //    recurArray0[3] = "PM";
            //}
            //if (Int32.Parse(recurArray0[1]) == 0)
            //{
            //    recurArray0[1] = "12";
            //    recurArray0[3] = "AM";
            //}
            /* *** Code added for FB 1425 * ***/

            recurXML += "   <timeZone>" + recurArray0[0] + "</timeZone>";
            recurXML += "   <startHour>" + BuffSetup.ToString("hh") + "</startHour>";
            recurXML += "   <startMin>" + BuffSetup.ToString("mm") + "</startMin>";
            recurXML += "   <startSet>" + BuffSetup.ToString("tt") + "</startSet>";
            recurXML += "   <durationMin>" + TotalDur + "</durationMin>"; //FB 2398 end
            recurXML += bufferxml;   //buffer zone
            recurXML += "</appointmentTime>";
            recurXML += "<recurrencePattern>";
            recurXML += "   <recurType>" + recurArray1[0] + "</recurType>";
            switch (recurArray1[0])
            {
                case "1":
                    recurXML += "<dailyType>" + recurArray1[1] + "</dailyType>";
                    recurXML += "<dayGap>" + recurArray1[2] + "</dayGap>";
                    break;
                case "2":
                    recurXML += "<weekGap>" + recurArray1[3] + "</weekGap>";
                    recurXML += "<weekDay>" + recurArray1[4] + "</weekDay>";
                    break;
                case "3":
                    recurXML += "<monthlyType>" + recurArray1[5] + "</monthlyType>";
                    switch (recurArray1[5])
                    {
                        case "1":
                            recurXML += "<monthDayNo>" + recurArray1[6] + "</monthDayNo>";
                            recurXML += "<monthGap>" + recurArray1[7] + "</monthGap>";
                            break;
                        case "2":
                            recurXML += "<monthWeekDayNo>" + recurArray1[8] + "</monthWeekDayNo>";
                            recurXML += "<monthWeekDay>" + recurArray1[9] + "</monthWeekDay>";
                            recurXML += "<monthGap>" + recurArray1[10] + "</monthGap>";
                            break;
                        //                        default:
                        //                            HttpContext.Current.Response.Write("An error had occured in Monthly recurring pattern");
                    }
                    break;
                case "4":
                    recurXML += "<yearlyType>" + recurArray1[11] + "</yearlyType>";
                    switch (recurArray1[11])
                    {
                        case "1":
                            recurXML += "<yearMonth>" + recurArray1[12] + "</yearMonth>";
                            recurXML += "<yearMonthDay>" + recurArray1[13] + "</yearMonthDay>";
                            break;
                        case "2":
                            recurXML += "<yearMonthWeekDayNo>" + recurArray1[14] + "</yearMonthWeekDayNo>";
                            recurXML += "<yearMonthWeekDay>" + recurArray1[15] + "</yearMonthWeekDay>";
                            recurXML += "<yearMonth>" + recurArray1[16] + "</yearMonth>";
                            break;
                        //                        default:
                        //                            HttpContext.Current.Response.Write("An error had occured in Yearly recurring pattern");
                    }
                    break;
                case "5":
                    recurXML += "<startDates>";
                    for (int i = 0; i < recurArray2.Length; i++)
                    {
                        /* *** Code added by Offshore for DateFormat String FB Issue 1073 -- Start *** */

                        recurXML += "<startDate>" + GetDefaultDate(recurArray2[i]) + "</startDate>";

                        /* *** Code added by Offshore for DateFormat String FB Issue 1073 -- End *** */
                    }
                    recurXML += "";
                    recurXML += "</startDates>";
                    break;
                //                default:
                //                    HttpContext.Current.Response.Write("An error had occured in Custom Dates recurring pattern");
            }

            if (!recurArray1[0].Equals("5"))
            {
                recurXML += "   <recurrenceRange>";
                /* *** Code added by Offshore for DateFormat String FB Issue 1073 -- Start *** */

                recurXML += "<startDate>" + GetDefaultDate(recurArray2[0]) + "</startDate>";

                /* *** Code added by Offshore for DateFormat String FB Issue 1073 -- End *** */

                recurXML += "<endType>" + recurArray2[1] + "</endType>";
                switch (recurArray2[1])
                {
                    case "1":
                    case "2":
                        recurXML += "<occurrence>" + recurArray2[2] + "</occurrence>";
                        break;
                    case "3":
                        /* *** Code added by Offshore for DateFormat String FB Issue 1073 -- Start *** */

                        recurXML += "<endDate>" + GetDefaultDate(recurArray2[3]) + "</endDate>";

                        /* *** Code added by Offshore for DateFormat String FB Issue 1073 -- End *** */
                        break;
                    //                    default:
                    //                        HttpContext.Current.Response.Write("An error had occured in Last recurring pattern");
                }
                recurXML += "   </recurrenceRange>";
            }
            recurXML += "</recurrencePattern>";

            return recurXML;
        }
        #endregion

        #region AssembleRecur
        /// <summary>
        /// AssembleRecur
        /// </summary>
        /// <param name="instr"></param>
        /// <param name="tzstr"></param>
        /// <returns></returns>

        public string AssembleRecur(string instr, string tzstr)
        {
            XmlDocument RecurXmlDoc = new XmlDocument();
            RecurXmlDoc.LoadXml(instr);
            try
            {
                string timeZoneNo = RecurXmlDoc.SelectSingleNode("/recurstr/timeZone").InnerText;
                string startHour = RecurXmlDoc.SelectSingleNode("/recurstr/startHour").InnerText;
                string startMin = RecurXmlDoc.SelectSingleNode("/recurstr/startMin").InnerText;
                string startSet = RecurXmlDoc.SelectSingleNode("/recurstr/startSet").InnerText;
                string durationMin = RecurXmlDoc.SelectSingleNode("/recurstr/durationMin").InnerText;
                string at_str = timeZoneNo + "&" + startHour + "&" + startMin + "&" + startSet + "&" + durationMin;

                string recurType = RecurXmlDoc.SelectSingleNode("/recurstr/recurType").InnerText;
                string rt_str = "";
                string rr_str = "";
                rt_str = rt_str + recurType + "&";
                //Response.Write("recurType: " + recurType);
                switch (recurType)
                {
                    case "1":
                        string dailyType = RecurXmlDoc.SelectSingleNode("/recurstr/dailyType").InnerText;
                        rt_str += dailyType + "&";
                        switch (dailyType)
                        {
                            case "1":
                                string dayGap = RecurXmlDoc.SelectSingleNode("/recurstr/dayGap").InnerText;
                                rt_str += dayGap;
                                break;
                            case "2":
                                rt_str += "-1";
                                break;
                            default:

                                break;

                        }
                        rt_str += "&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";
                        break;
                    case "2":
                        string weekGap = RecurXmlDoc.SelectSingleNode("/recurstr/weekGap").InnerText;
                        string weekDay1 = RecurXmlDoc.SelectSingleNode("/recurstr/weekDay").InnerText;
                        rt_str += "-1&-1&" + weekGap + "&" + weekDay1 + "&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&-1";
                        break;
                    case "3":
                        string monthlyType = RecurXmlDoc.SelectSingleNode("/recurstr/monthlyType").InnerText;
                        string monthGap = "";
                        rt_str += "-1&-1&-1&-1&" + monthlyType + "&";
                        switch (monthlyType)
                        {
                            case "1":
                                string monthDayNo = RecurXmlDoc.SelectSingleNode("/recurstr/monthDayNo").InnerText;
                                monthGap = RecurXmlDoc.SelectSingleNode("/recurstr/monthGap").InnerText;
                                rt_str += monthDayNo + "&" + monthGap + "&-1&-1&-1";
                                break;
                            case "2":
                                string monthWeekDayNo = RecurXmlDoc.SelectSingleNode("/recurstr/monthWeekDayNo").InnerText;
                                string monthWeekDay = RecurXmlDoc.SelectSingleNode("/recurstr/monthWeekDay").InnerText;
                                monthGap = RecurXmlDoc.SelectSingleNode("/recurstr/monthGap").InnerText;
                                rt_str += "-1&-1&" + monthWeekDayNo + "&" + monthWeekDay + "&" + monthGap;
                                break;
                            default:
                                break;
                        }
                        rt_str += "&-1&-1&-1&-1&-1&-1";
                        break;
                    case "4":
                        string yearlyType = RecurXmlDoc.SelectSingleNode("/recurstr/yearlyType").InnerText;
                        string yearMonth = "";
                        rt_str += "-1&-1&-1&-1&-1&-1&-1&-1&-1&-1&" + yearlyType + "&";
                        switch (yearlyType)
                        {
                            case "1":
                                yearMonth = RecurXmlDoc.SelectSingleNode("/recurstr/yearMonth").InnerText;
                                string yearMonthDay = RecurXmlDoc.SelectSingleNode("/recurstr/yearMonthDay").InnerText;
                                rt_str += yearMonth + "&" + yearMonthDay + "&-1&-1&-1";
                                break;
                            case "2":
                                string yearMonthWeekDayNo = RecurXmlDoc.SelectSingleNode("/recurstr/yearMonthWeekDayNo").InnerText;
                                string yearMonthWeekDay = RecurXmlDoc.SelectSingleNode("/recurstr/yearMonthWeekDay").InnerText;
                                yearMonth = RecurXmlDoc.SelectSingleNode("/recurstr/yearMonth").InnerText;
                                /* *** Recurrence Fixes - Yearly Pattern - Start *** */
                                rt_str += "-1&-1&" + yearMonthWeekDayNo + "&" + yearMonthWeekDay + "&" + yearMonth;
                                /* *** Recurrence Fixes - Yearly Pattern - End *** */
                                break;
                            default:
                                break;
                        }
                        break;
                    case "5":
                        rt_str = "5";
                        rr_str = "";
                        XmlNodeList sds = RecurXmlDoc.SelectNodes("/recurstr/startDates/startDate");
                        for (int k = 0; k < sds.Count; k++)
                            rr_str += sds[k].InnerText + "&";
                        rr_str = rr_str.Substring(0, rr_str.Length - 1);
                        break;
                    default:
                        break;
                }

                if (recurType != "5")
                {
                    string startDate = RecurXmlDoc.SelectSingleNode("/recurstr/startDate").InnerText;
                    string endType = RecurXmlDoc.SelectSingleNode("/recurstr/endType").InnerText;

                    rr_str += startDate + "&" + endType + "&";
                    switch (endType)
                    {
                        case "1":
                            rr_str += "-1&-1";
                            break;
                        case "2":
                            string occurrence = RecurXmlDoc.SelectSingleNode("/recurstr/occurrence").InnerText;
                            rr_str += occurrence + "&-1";
                            break;

                        case "3":
                            string endDate = RecurXmlDoc.SelectSingleNode("/recurstr/endDate").InnerText;
                            rr_str += "-1&" + endDate;
                            break;
                        default:
                            break;
                    }
                }
                string recur = at_str + "#" + rt_str + "#" + rr_str;
                recur += "|" + aGetTimeZoneName(1, timeZoneNo, tzstr);
                return recur;
            }
            catch (Exception ex)
            {
                //HttpContext.Current.Response.Write(ex.StackTrace);
                HttpContext.Current.Response.Write(errXML);
                log.Trace(ex.StackTrace + "AssembleRecur :Error in Assembly function. " + ex.Message);
                return errXML;//FB 1881
                //return "<error>Error in Assembly function</error>";
            }
        }
        #endregion

        #region aGetTimeZoneName
        /// <summary>
        /// aGetTimeZoneName
        /// </summary>
        /// <param name="pos"></param>
        /// <param name="tzID"></param>
        /// <param name="str"></param>
        /// <returns></returns>
        public string aGetTimeZoneName(int pos, string tzID, string str)
        {
            string selectedTimezoneName = "";
            XmlDocument xmldoc = new XmlDocument();
            xmldoc.LoadXml(str);
            XmlNodeList nodes = xmldoc.SelectNodes("/TimeZone/timezone");
            for (int i = 0; i < nodes.Count; i++)
            {
                if (nodes[i].SelectSingleNode("timezoneID").InnerText.Equals(tzID))//Code for Fb 1728
                    selectedTimezoneName = nodes[i].SelectSingleNode("timezoneName").InnerText;
            }
            return selectedTimezoneName;
        }
        #endregion

        #region GetSystemDateTime
        /// <summary>
        /// GetSystemDateTime
        /// </summary>
        /// <param name="configpath"></param>
        public void GetSystemDateTime(string configpath)
        {
            try
            {
                string resXML;
                string inputXML = "<login><userID>" + HttpContext.Current.Session["userID"] + "</userID></login>";

                if (HttpContext.Current.Application["debugMode"].ToString().ToUpper() != "ASP")
                {
                    //FB 2027
                    //web_com_v18_Net.Com com = new web_com_v18_Net.Com();
                    //resXML = com.comCentral("GetSystemDateTime", inputXML, configpath);
                    resXML = CallMyVRMServer("GetSystemDateTime", inputXML, configpath);
                    //com = null; //Login Management

                    if (resXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldocdt = new XmlDocument();
                        xmldocdt.LoadXml(resXML);
                        HttpContext.Current.Session["systemDate"] = xmldocdt.SelectSingleNode("//systemDateTime/systemDate").InnerText;
                        HttpContext.Current.Session["systemTime"] = xmldocdt.SelectSingleNode("//systemDateTime/systemTime").InnerText;
                        string systemTimeZone = xmldocdt.SelectSingleNode("//systemDateTime/systemTimeZone").InnerText;

                        HttpContext.Current.Session.Remove("systemTimezoneID");//Login Management
                        HttpContext.Current.Session.Add("systemTimezoneID", systemTimeZone);

                        XmlNodeList nodes = xmldocdt.SelectNodes("//systemDateTime/timezones/timezone");

                        foreach (XmlNode node in nodes)
                        {
                            if (node.SelectSingleNode("timezoneID").InnerText.Trim().Equals(systemTimeZone))
                                HttpContext.Current.Session["systemTimeZone"] = node.SelectSingleNode("timezoneName").InnerText;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
        }
        #endregion

        /* *** Code added for FB 1425 QA Bug -Start *** */

        #region GetSystemTimeZone
        /// <summary>
        /// GetSystemTimeZone
        /// </summary>
        /// <param name="configpath"></param>
        public String GetSystemTimeZone(string configpath)
        {
            String timezoneId = "";
            try
            {
                string resXML;
                string inputXML = "<login><userID></userID></login>";

                if (HttpContext.Current.Application["debugMode"].ToString().ToUpper() != "ASP")
                {
                    //FB 2027
                    //web_com_v18_Net.Com com = new web_com_v18_Net.Com();                    
                    //resXML = com.comCentral("GetSystemDateTime", inputXML, configpath);
                    resXML = CallMyVRMServer("GetSystemDateTime", inputXML, configpath);
                    if (resXML.IndexOf("<error>") < 0)
                    {
                        XmlDocument xmldocdt = new XmlDocument();
                        xmldocdt.LoadXml(resXML);
                        timezoneId = xmldocdt.SelectSingleNode("//systemDateTime/systemTimeZone").InnerText;
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
                //HttpContext.Current.Response.Write(ex.StackTrace);
            }

            return timezoneId;
        }
        #endregion

        /* *** Code added for FB 1425 QA Bug -Start *** */

        //ZD 100152 Starts

        #region GetEncrpytedText
        /// <summary>
        /// GetEncrpytedText
        /// </summary>
        /// <param name="_configpath"></param>
        /// <param name="_Data"></param>
        /// <returns></returns>
        public string GetEncrpytedText(string _configpath, string _Data)
        {
            string _inXML = "", _outXML = "", _Returnvalue = ""; ;
            XmlDocument _docs = null;
            try
            {
                _inXML = "<System><Cipher>" + _Data.Trim() + "</Cipher></System>";

                _outXML = CallMyVRMServer("GetEncrpytedText", _inXML, _configpath);

                if (_outXML.IndexOf("<error>") < 0)
                {
                    _docs = new XmlDocument();
                    _docs.LoadXml(_outXML);
                    XmlNode nde = _docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        _Returnvalue = nde.InnerText;
                }

                return _Returnvalue;

            }
            catch (Exception ex)
            {
                log.Trace("" + ex.StackTrace);
                return "";
            }
        }

        #endregion

        #region GetDecryptText
        /// <summary>
        /// GetDecryptText
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public string GetDecryptText(string _configpath, string _Data)
        {
            string _inXML = "", _outXML = "", _Returnvalue = ""; ;
            XmlDocument docs = null;
            try
            {
                _inXML = "<System><Cipher>" + _Data.Trim() + "</Cipher></System>";

                _outXML = CallMyVRMServer("GetDecrpytedText", _inXML, _configpath);

                if (_outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(_outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        _Returnvalue = nde.InnerText;
                }
                return _Returnvalue;

            }
            catch (Exception ex)
            {
                log.Trace("" + ex.StackTrace);
                return "";
            }
        }
        #endregion
        //ZD 100152 Ends

        #region  GetMyVRMUserName
        /// <summary>
        /// GetMyVRMUserName
        /// </summary>
        /// <param name="userID"></param>
        /// <returns></returns>
        public string GetMyVRMUserName(string userID)
        {
            string userName = "";
            try
            {
                // GetOldUser] inxml : <login><userID>11</userID><user><userID>12</userID></user></login>
                string inxml = "<login>" + OrgXMLElement() + "<userID>11</userID><user><userID>" + userID + "</userID></user></login>";//Organization Module Fixes
                //FB 2027 Start
                string outXML = CallMyVRMServer("GetOldUser", inxml, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //string outXML = CallCOM("GetOldUser", inxml, HttpContext.Current.Application["COM_ConfigPath"].ToString());
                //FB 2027 End
                //HttpContext.Current.Response.Write(Transfer(outXML
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                userName = xmldoc.SelectSingleNode("/oldUser/userName/firstName").InnerText + " " + xmldoc.SelectSingleNode("/oldUser/userName/lastName").InnerText;
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
            }
            return userName;
        }
        #endregion

        #region MenuMaskCheckLobby
        /// <summary>
        /// MenuMaskCheckLobby
        /// </summary>
        public void MenuMaskCheckLobby()
        {
        }
        #endregion

        #region ShowErrorMessage
        /// <summary>
        /// ShowErrorMessage
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public string ShowErrorMessage(string xmlString)
        {
            try
            {
                string errMSG = "";
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(xmlString);
                errMSG = GetTranslatedText(xmlDOC.SelectSingleNode("//error/message").InnerText); //ZD 100288
                if (xmlDOC.SelectSingleNode("//error/errorCode") != null)
                    errMSG += " " + GetTranslatedText("Error Code") + " : " + xmlDOC.SelectSingleNode("//error/errorCode").InnerText; //ZD 100288 //ZD 101714
                return errMSG;
            }
            catch (Exception ex)
            {
                log.Trace(xmlString);
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return errXML;//FB 1881
                //return "Error 122: Please contact your VRM Administrator";
            }
        }
        #endregion

        #region GetErrorLevel
        /// <summary>
        /// GetErrorLevel
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public string GetErrorLevel(string xmlString)
        {
            string errMSG = "";
            XmlDocument xmlDOC = new XmlDocument();
            xmlDOC.LoadXml(xmlString);
            errMSG = xmlDOC.SelectSingleNode("/error/level").InnerText;
            return errMSG;
        }
        #endregion

        #region getUploadFilePath
        /// <summary>
        /// getUploadFilePath
        /// </summary>
        /// <param name="fpn"></param>
        /// <returns></returns>
        public string getUploadFilePath(string fpn)
        {
            string fPath = String.Empty;
            if (fpn.Equals(""))
                fPath = "";
            else
            {
                char[] splitter = { '/' };
                string[] fa = fpn.Split(splitter[0]);
                if (fa.Length.Equals(0))
                    fPath = "";
                else
                    fPath = fa[fa.Length - 1];
            }
            return fPath;
        }
        #endregion

        #region DisplayPaging
        /// <summary>
        /// DisplayPaging
        /// </summary>
        /// <param name="totalPages"></param>
        /// <param name="currentPage"></param>
        /// <param name="tblPage"></param>
        /// <param name="targetPage"></param>
        public void DisplayPaging(int totalPages, int currentPage, Table tblPage, string targetPage)
        {
            try
            {
                tblPage.Rows.Clear();
                tblPage.Visible = true;
                TableRow tr = new TableRow();
                TableCell tc;
                tblPage.CellPadding = 2;
                tc = new TableCell();
                tc.Text = GetTranslatedText("Page(s):");//FB 1830 - Translation
                tr.Cells.Add(tc);
                int startPage = 0;
                int endPage = 10;
                /*
                if ( (currentPage > startPage) && () )
                    startPage = startPage - 10;
                for (int i = 0; i < totalPages / 10; i++)
                {

                }*/
                if (currentPage < 5)
                    startPage = 1;
                else
                    startPage = currentPage - 4;
                endPage = startPage + 9;
                if (endPage > totalPages)
                    endPage = totalPages;

                if (endPage - 10 < 0)
                    startPage = 1;
                else
                    startPage = endPage - 9;

                tc = new TableCell();
                tc.Text = "<a href='" + targetPage + "&pageNo=1'>" + GetTranslatedText("First") + "</a>";//FB 2272
                tr.Cells.Add(tc);

                tc = new TableCell();
                if ((startPage == 1) || (currentPage == -1))
                    tc.Text = "<img src='image/prevroomgray.gif' border='0' width='15' height='15' style='cursor: point;' title='Already on first page'>";
                else
                    tc.Text = "<a href='" + targetPage + "&pageNo=" + (startPage - 1) + "'><img src='image/prevroom.gif' border='0' width='15' height='15' style='cursor: hand;' title='previous page'></a>";

                tr.Cells.Add(tc);
                tc = new TableCell();
                for (int i = startPage; i <= endPage; i++)
                {
                    if (i == currentPage)
                        tc.Text += i.ToString() + " ";
                    else
                    {
                        string qString = HttpContext.Current.Request.QueryString.ToString();
                        //qString = qString.Replace("pageNo=" + HttpContext.Current.Request.QueryString["pageNo"].ToString(), "pageNo=" + i.ToString());
                        tc.Text += " <a href='" + targetPage + "&pageNo=" + i + "'>" + i.ToString() + "</a> ";
                    }
                }
                tr.Cells.Add(tc);
                tc = new TableCell();
                //HttpContext.Current.Response.Write(currentPage + " : " + totalPages);
                if ((endPage >= totalPages) || (currentPage == -1))
                    tc.Text = "<img src='image/prevroomgray.gif' border='0' width='15' height='15' style='cursor: point;' title='Already on last page'>";
                else
                    tc.Text = "<a href='" + targetPage + "&pageNo=" + (endPage + 1) + "'><img src='image/nextroom.gif' border='0' width='15' height='15' style='cursor: hand;' title='next page'></a>";
                tr.Cells.Add(tc);
                tc = new TableCell();
                tc.Text = "<a href='" + targetPage + "&pageNo=" + totalPages + "'>" + GetTranslatedText("Last") + "</a>";//FB 2272
                tr.Cells.Add(tc);
                tblPage.Rows.Add(tr);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetYear
        /// <summary>
        /// GetYear
        /// </summary>
        /// <param name="fromDate"></param>
        /// <returns></returns>
        public int GetYear(String fromDate)
        {
            try
            {
                return (Int32.Parse(fromDate.Split('/')[2]));
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return DateTime.Now.Year;
            }
        }
        #endregion

        #region GetMonth
        /// <summary>
        /// GetMonth
        /// </summary>
        /// <param name="fromDate"></param>
        /// <returns></returns>
        public int GetMonth(String fromDate)
        {
            try
            {
                return (Int32.Parse(fromDate.Split('/')[0]));
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return DateTime.Now.Month;
            }
        }
        #endregion

        #region GetDay
        /// <summary>
        /// GetDay
        /// </summary>
        /// <param name="fromDate"></param>
        /// <returns></returns>
        public int GetDay(String fromDate)
        {
            try
            {
                return (Int32.Parse(fromDate.Split('/')[1]));
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return DateTime.Now.Day;
            }
        }
        #endregion

        #region GetHour
        /// <summary>
        /// GetHour
        /// </summary>
        /// <param name="fromTime"></param>
        /// <returns></returns>
        public int GetHour(String fromTime)
        {
            try
            {
                int hr = Int32.Parse(fromTime.Split(' ')[0].Split(':')[0]);
                if (fromTime.Split(' ')[1].ToUpper().Equals("PM") && (hr != 12))
                    hr += 12;
                if (fromTime.Split(' ')[1].ToUpper().Equals("AM") && (hr == 12))
                    hr -= 12;
                return hr;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return DateTime.Now.Day;
            }
        }
        #endregion

        #region GetMinute
        /// <summary>
        /// GetMinute
        /// </summary>
        /// <param name="fromTime"></param>
        /// <returns></returns>
        public int GetMinute(String fromTime)
        {
            try
            {
                return (Int32.Parse(fromTime.Split(' ')[0].Split(':')[1]));
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return DateTime.Now.Day;
            }
        }
        #endregion

        #region GetTimeSet
        /// <summary>
        /// GetTimeSet
        /// </summary>
        /// <param name="fromTime"></param>
        /// <returns></returns>
        public String GetTimeSet(String fromTime)
        {
            try
            {
                return (fromTime.Split(' ')[1].ToUpper());
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + "Error : " + ex.Message);
                return errXML;//FB 1881
                //return "Error";
            }
        }
        #endregion

        #region BindVideoEquipment
        /// <summary>
        /// BindVideoEquipment
        /// </summary>
        /// <param name="sender"></param>
        public void BindVideoEquipment(DropDownList sender)
        {
            try
            {
                String inXML = "<GetVideoEquipment>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetVideoEquipment>";//Organization Module Fixes
                String outXML;
                outXML = CallMyVRMServer("GetVideoEquipment", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//VideoEquipment/Equipment");
                //if (nodes.Count > 0)
                {
                    LoadList(sender, nodes, "VideoEquipmentID", "EquipmentDisplayName");//ZD 100736
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //ZD 100736 - Start
        #region BindManufacturer
        /// <summary>
        /// BindManufacturer
        /// </summary>
        /// <param name="sender"></param>
        public void BindManufacturer(DropDownList sender)
        {
            try
            {
                String inXML = "<GetManufacturer>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetManufacturer>";
                String outXML;
                outXML = CallMyVRMServer("GetManufacturer", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
               
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//Manufacturer/Equipment");
               
                LoadList(sender, nodes, "ManufacturerID", "ManufacturerName");
               
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 100736 - End

        //ZD 100736 START
        #region GetManufacturerModel
        /// <summary>
        /// GetManufacturerModel
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="strEquipmentCode"></param>
        public void GetManufacturerModel(DropDownList lstList, String strEquipmentCode)
        {
            try
            {
                String inXML = "<GetManufacturerModel><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>" + OrgXMLElement() + "<ManufacturerID>" + strEquipmentCode + "</ManufacturerID></GetManufacturerModel>";
                String outXML = CallMyVRMServer("GetManufacturerModel", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetManufacturerModel outxml: " + outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetManufacturerModel/EndpointModel");
                LoadList(lstList, nodes, "VideoEquipmentID", "EquipmentDisplayName");
            }
            catch (Exception ex)
            {
                log.Trace("GetManufacturerModel: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 100736 END

        #region BindLineRate
        /// <summary>
        /// BindLineRate
        /// </summary>
        /// <param name="sender"></param>
        public void BindLineRate(DropDownList sender)
        {
            try
            {
                String inXML = "<GetLineRate>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetLineRate>";//Organization Module Fixes
                String outXML;
                //if (HttpContext.Current.Session["LineRates"] == null)
                //{
                outXML = CallMyVRMServer("GetLineRate", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //   HttpContext.Current.Session.Add("LineRates", outXML);
                // }
                //else
                //    outXML = HttpContext.Current.Session["LineRates"].ToString();

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//LineRate/Rate");
                //if (nodes.Count > 0)
                {
                    LoadList(sender, nodes, "LineRateID", "LineRateName");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindAddressType
        /// <summary>
        /// BindAddressType
        /// </summary>
        /// <param name="sender"></param>
        public void BindAddressType(DropDownList sender)
        {
            try
            {
                String inXML = "<GetAddressType>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetAddressType>";//Organization Module Fixes
                String outXML;
                //if (HttpContext.Current.Session["AddressTypes"] == null)
                //{
                outXML = CallMyVRMServer("GetAddressType", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //    HttpContext.Current.Session.Add("AddressTypes", outXML);
                //}
                //else
                //    outXML = HttpContext.Current.Session["AddressTypes"].ToString();

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetAddressType/AddressType/Type");
                //if (nodes.Count > 0)
                {
                    LoadList(sender, nodes, "ID", "Name");
                }
            }
            catch (Exception ex)
            {
                log.Trace("GetAddressType: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region  BindVideoMode
        /// <summary>
        /// BindVideoMode
        /// </summary>
        /// <param name="sender"></param>
        public void BindVideoMode(DropDownList sender)
        {
            try
            {
                String inXML = "<GetVideoModes>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetVideoModes>";//Organization Module Fixes
                String outXML;
                //if (HttpContext.Current.Session["VideoModes"] == null)
                {
                    outXML = CallMyVRMServer("GetVideoModes", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                    //HttpContext.Current.Response.Write(Transfer(outXML));
                    //outXML = "<GetVideoModes><VideoModes><VideoMode><ID>1</ID><Name>Switched VC</Name></VideoMode><VideoMode><ID>2</ID><Name>Transcoding &amp; Continous Presence</Name></VideoMode></VideoModes></GetVideoModes>";
                    //   HttpContext.Current.Session.Add("VideoModes", outXML);
                }
                //else
                //    outXML = HttpContext.Current.Session["VideoModes"].ToString();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetVideoModes/VideoMode");
                //if (nodes.Count > 0)
                {
                    LoadList(sender, nodes, "ID", "Name");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindVideoCodecs
        /// <summary>
        /// BindVideoCodecs
        /// </summary>
        /// <param name="sender"></param>
        public void BindVideoCodecs(DropDownList sender)
        {
            try
            {
                String inXML = "<GetVideoCodecs>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetVideoCodecs>";//Organization Module Fixes
                String outXML;
                //if (HttpContext.Current.Session["VideoCodecs"] == null)
                //{
                outXML = CallMyVRMServer("GetVideoCodecs", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //HttpContext.Current.Response.Write(Transfer(outXML));
                //outXML = "<GetVideoCodecs><VideoCodecs><VideoCodec><ID>1</ID><Name>IP Address</Name></VideoCodec><VideoCodec><ID>2</ID><Name>H323 ID</Name></VideoCodec><VideoCodec><ID>3</ID><Name>E.164</Name></VideoCodec><VideoCodec><ID>4</ID><Name>ISDN Phone number</Name></VideoCodec></VideoCodecs></GetVideoCodecs>";
                //   HttpContext.Current.Session.Add("VideoCodecs", outXML);
                //}
                //else
                //    outXML = HttpContext.Current.Session["VideoCodecs"].ToString();

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetVideoCodecs/VideoCodec");
                //if (nodes.Count > 0)
                LoadList(sender, nodes, "ID", "Name");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindAudioCodecs
        /// <summary>
        /// BindAudioCodecs
        /// </summary>
        /// <param name="sender"></param>
        public void BindAudioCodecs(DropDownList sender)
        {
            try
            {
                String inXML = "<GetAudioCodecs>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetAudioCodecs>";//Organization Module Fixes
                String outXML;
                //if (HttpContext.Current.Session["AudioCodecs"] == null)
                //{
                outXML = CallMyVRMServer("GetAudioCodecs", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //HttpContext.Current.Response.Write("<br>AudioCodecs" + Transfer(outXML));
                //outXML = "<GetAudioCodecs><AudioCodecs><AudioCodec><ID>1</ID><Name>IP Address</Name></AudioCodec><AudioCodec><ID>2</ID><Name>H323 ID</Name></AudioCodec><AudioCodec><ID>3</ID><Name>E.164</Name></AudioCodec><AudioCodec><ID>4</ID><Name>ISDN Phone number</Name></AudioCodec></AudioCodecs></GetAudioCodecs>";
                //    HttpContext.Current.Session.Add("AudioCodecs", outXML);
                //}
                //else
                //{
                //    outXML = HttpContext.Current.Session["AudioCodecs"].ToString();
                // }
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetAudioCodecs/AudioCodec");
                //if (nodes.Count > 0)
                {
                    LoadList(sender, nodes, "ID", "Name");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindEndpointDetails
        /// <summary>
        /// BindEndpointDetails
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="EPID"></param>
        public void BindEndpointDetails(DropDownList sender, String EPID)
        {
            try
            {
                String inXML = "<EndpointDetails>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID><EndpointID>" + EPID + "</EndpointID></EndpointDetails>";//Organization Module Fixes
                String outXML = CallMyVRMServer("GetEndpointDetails", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//EndpointDetails/Endpoint/Profiles/Profile");
                //if (nodes.Count > 0)
                {
                    LoadList(sender, nodes, "ProfileID", "ProfileName");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindBridges
        /// <summary>
        /// BindBridges
        /// </summary>
        /// <param name="sender"></param>
        public void BindBridges(DropDownList sender)
        {
            try
            {
                String inXML = "<GetBridges>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetBridges>";//Organization Module Fixes
                String outXML = CallMyVRMServer("GetBridges", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //HttpContext.Current.Response.Write(Transfer(inXML) + "<hr>" + Transfer(outXML));
                //String outXML = "<Bridges><Bridge><BridgeID>1</BridgeID><BridgeName>Expedite</BridgeName></Bridge><Bridge><BridgeID>2</BridgeID><BridgeName>ExpediteV</BridgeName></Bridge><Bridge><BridgeID>3</BridgeID><BridgeName>Umiami</BridgeName></Bridge></Bridges>";
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//Bridges/Bridge");
                //if (nodes.Count > 0)
                {
                    LoadList(sender, nodes, "BridgeID", "BridgeName");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //FB 2593
        #region GetAllBridges
        /// <summary>
        /// GetAllBridges
        /// </summary>
        /// <param name="sender"></param>
        public void GetAllBridges(ref String outXML, String type)
        {
            try
            {
                String inXML = "<GetBridges>" + OrgXMLElement() + "<SearchType>" + type + "</SearchType><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetBridges>";//Organization Module Fixes
                outXML = CallMyVRMServer("GetBridges", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindMediaTypes
        /// <summary>
        /// BindMediaTypes
        /// </summary>
        /// <param name="sender"></param>
        public void BindMediaTypes(DropDownList sender)
        {
            try
            {
                String inXML = "<GetMediaTypes>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetMediaTypes>";//Organization Module Fixes
                String outXML = CallMyVRMServer("GetMediaTypes", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = "<GetMediaTypes><Type><ID>1</ID><Name>None</Name> </Type><Type><ID>2</ID><Name>Audio-only</Name> </Type><Type><ID>3</ID><Name>Audio,Video</Name> </Type></GetMediaTypes>";
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetMediaTypes/Type");
                //if (nodes.Count > 0)
                {
                    LoadList(sender, nodes, "ID", "Name");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindVideoProtocols
        /// <summary>
        /// BindVideoProtocols
        /// </summary>
        /// <param name="sender"></param>
        public void BindVideoProtocols(DropDownList sender)
        {
            try
            {
                String inXML = "<GetVideoProtocols>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetVideoProtocols>";//Organization Module Fixes
                String outXML = CallMyVRMServer("GetVideoProtocols", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = "<GetVideoProtocols><Protocol><ID>1</ID><Name>IP</Name> </Protocol><Protocol><ID>2</ID><Name>ISDN</Name> </Protocol><Protocol><ID>3</ID><Name>SIP</Name> </Protocol></GetVideoProtocols>";
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetVideoProtocols/Protocol");
                //if (nodes.Count > 0)
                {
                    LoadList(sender, nodes, "ID", "Name");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetDeliveryTypes
        /// <summary>
        /// GetDeliveryTypes
        /// </summary>
        /// <param name="sender"></param>
        public void GetDeliveryTypes(DropDownList sender)
        {
            try
            {
                String inXML = "<GetDeliveryTypes>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetDeliveryTypes>";//Organization Module Fixes
                String outXML;
                //if (HttpContext.Current.Session["DeliveryTypes"] == null)
                //{
                outXML = CallMyVRMServer("GetDeliveryTypes", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //outXML = "<GetDeliveryTypes><Type><ID>1</ID><Name>Delivery and Pickup</Name></Type><Type><ID>2</ID><Name>Delivery Only</Name></Type><Type><ID>3</ID><Name>Pickup Only</Name></Type><Type><ID>4</ID><Name>Equipment Pickup</Name></Type></GetDeliveryTypes>";
                //    HttpContext.Current.Session.Add("DeliveryTypes", outXML);
                //<DeliveryTypes><Type><ID>1</ID><Name>Delivery and Pickup</Name></Type><Type><ID>2</ID><Name>Delivery Only</Name></Type><Type><ID>3</ID><Name>Pick up only</Name></Type><Type><ID>4</ID><Name>Equipment pickup</Name></Type></DeliveryTypes>
                //}
                //else
                //    outXML = HttpContext.Current.Session["DeliveryTypes"].ToString();
                //HttpContext.Current.Response.Write(Transfer(outXML));
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//DeliveryTypes/Type");
                //if (nodes.Count > 0)
                {
                    LoadList(sender, nodes, "ID", "Name");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindDialingOptions
        /// <summary>
        /// BindDialingOptions
        /// </summary>
        /// <param name="sender"></param>
        public void BindDialingOptions(DropDownList sender)
        {
            try
            {
                String inXML = "<GetDialingOption><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>" + OrgXMLElement() + "</GetDialingOption>";//Organization Module Fixes
                String outXML;
                //if (HttpContext.Current.Session["DialingOptions"] == null)
                //{
                outXML = CallMyVRMServer("GetDialingOptions", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //outXML = "<GetDialingOptions><DialingOption><ID>1</ID><Name>Dial-in to MCU</Name></DialingOption><DialingOption><ID>2</ID><Name>Dial-out from MCU</Name></DialingOption><DialingOption><ID>3</ID><Name>Direct Dialing from MCU</Name></DialingOption></GetDialingOptions>";
                //    HttpContext.Current.Session.Add("GetDialingOptions", outXML);
                //<DeliveryTypes><Type><ID>1</ID><Name>Delivery and Pickup</Name></Type><Type><ID>2</ID><Name>Delivery Only</Name></Type><Type><ID>3</ID><Name>Pick up only</Name></Type><Type><ID>4</ID><Name>Equipment pickup</Name></Type></DeliveryTypes>
                //}
                //else
                //    outXML = HttpContext.Current.Session["GetDialingOptions"].ToString();
                //HttpContext.Current.Response.Write(Transfer(outXML));
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetDialingOption/DialingOption");
                //if (nodes.Count > 0)
                {
                    LoadList(sender, nodes, "ID", "Name");
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region BindServiceType
        /// <summary>
        /// BindServiceType FB 2219
        /// </summary>
        /// <param name="sender"></param>
        public void BindServiceType(DropDownList sender)
        {
            try
            {
                String inXML = "<GetServiceType>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetServiceType>";
                String outXML = CallMyVRMServer("GetServiceType", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetServiceType/ServiceType");
                LoadList(sender, nodes, "ID", "Name");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //FB 2136 - Starts
        /*#region BindSecurityBadgeType
        /// <summary>
        /// BindSecurityBadgeType
        /// </summary>
        /// <param name="sender"></param>
        public void BindSecurityBadgeType(DropDownList sender)
        {
            try
            {
                String inXML = "<GetSecurityBadgeType>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetSecurityBadgeType>";
                String outXML = CallMyVRMServer("GetSecurityBadgeType", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetSecurityBadgeType/SecurityBadgeType");
                drpLoadList(sender, nodes, "ID", "Name");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion*/
        //FB 2136 - End

        #region GetTimezones
        /// <summary>
        /// GetTimezones
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="sel"></param>
        public void GetTimezones(DropDownList sender, ref String sel)
        {
            String userID = "11";//Code added for Error 200
            try
            {

                if (HttpContext.Current.Session["userID"] != null)//Code added for Error 200
                    userID = HttpContext.Current.Session["userID"].ToString();//Code added for Error 200

                String inXML = "<GetTimezones><UserID>" + userID + "</UserID>" + OrgXMLElement() + "</GetTimezones>";//Code added for Error 200 //Organization Module Fixes
                String outXML;
                //if (HttpContext.Current.Session["Timezones"] == null)
                {
                    outXML = CallMyVRMServer("GetTimezones", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                    //outXML = "<timezones><selected>26</selected><timezone><timezoneID>26</timezoneID><timezoneName>Eastern Standard Time</timezoneName></timezone></timezones>";
                    HttpContext.Current.Session.Add("Timezones", outXML);
                }
                //else
                //    outXML = HttpContext.Current.Session["Timezones"].ToString();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                if (xmldoc.SelectSingleNode("//Timezones/selected") != null)//Code added for Error 200
                    sel = xmldoc.SelectSingleNode("//Timezones/selected").InnerText;//Code added for Error 200
                XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");
                if (nodes.Count > 0)
                {
                    /* Code Modified For FB 1453- Start */
                    //LoadList(sender, nodes, "timezoneID", "timezoneName");
                    LoadTimeZoneList(sender, nodes, "timezoneID", "timezoneName");
                    /* Code Modified For FB 1453- Start */
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetGroups
        /// <summary>
        /// GetGroups
        /// </summary>
        /// <param name="sender"></param>
        public void GetGroups(DropDownList sender)
        {
            try
            {
                String inXML = "<login>" + OrgXMLElement() + "<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID><groupID></groupID><sortBy>1</sortBy></login>";//Organization Module Fixes
                //FB 2027
                //String outXML = CallCOM("GetGroup", inXML, HttpContext.Current.Application["COM_ConfigPath"].ToString()); //FB 1598
                String outXML = CallMyVRMServer("GetGroup", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString()); //FB 1598

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//groups/group");
                LoadList(sender, nodes, "groupID", "groupName");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        /// <summary>
        /// GetGroups
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="proxyid"></param>
        public void GetGroups(DropDownList sender, string proxyid) //FB 1598
        {
            try
            {
                String inXML = "<login>" + OrgXMLElement() + "<userID>" + proxyid + "</userID><groupID></groupID><sortBy>1</sortBy></login>";//Organization Module Fixes
                //FB 2027 
                //String outXML = CallCOM("GetGroup", inXML, HttpContext.Current.Application["COM_ConfigPath"].ToString());
                String outXML = CallMyVRMServer("GetGroup", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//groups/group");
                LoadList(sender, nodes, "groupID", "groupName");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetUserRoles
        /// <summary>
        /// GetUserRoles
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="col1"></param>
        /// <param name="col2"></param>
        public void GetUserRoles(DropDownList sender, String col1, String col2)
        {
            try
            {
                //Organization Module Fixes
                String inXML = "<login>" + OrgXMLElement() + "<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID><foodModuleEnable>" + HttpContext.Current.Session["foodModule"].ToString()+
                               "</foodModuleEnable><roomModuleEnable>" + HttpContext.Current.Session["roomModule"].ToString() + "</roomModuleEnable>"+
                               "<hkModuleEnable>" + HttpContext.Current.Session["hkModule"].ToString() + "</hkModuleEnable></login>"; //FB 2027
                String outXML = CallMyVRMServer("GetUserRoles", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString()); //FB 2027

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//userRole/roles/role");
                LoadList(sender, nodes, col1, col2);
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetSearchTemplateList
        /// <summary>
        /// GetSearchTemplateList
        /// </summary>
        /// <param name="sender"></param>
        public void GetSearchTemplateList(DropDownList sender)
        {
            try
            {
                String inXML = "<login>" + OrgXMLElement() + "<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID></login>";//Organization Module Fixes
                //FB 2027
                String outXML = CallMyVRMServer("GetSearchTemplateList", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //String outXML = CallCOM("GetSearchTemplateList", inXML, HttpContext.Current.Application["COM_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//getSearchTemplateList/searchTemplates/searchTemplate");
                LoadList(sender, nodes, "ID", "name");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetManageDepartment
        /// <summary>
        /// GetManageDepartment
        /// </summary>
        /// <param name="sender"></param>
        public void GetManageDepartment(ListBox sender)
        {
            try
            {
                String inXML = "<login><userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>" + OrgXMLElement() + "</login>";//Organization Module Fixes
                String outXML = CallMyVRMServer("GetManageDepartment", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//getManageDepartment/departments/department");
                LoadList(sender, nodes, "id", "name");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
		//FB 2047	
        public void GetAllDepartments(ref String outXml)
        {
            try
            {
                String inXML = "<login><userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID></login>";
                outXml = CallMyVRMServer("GetAllDepartments", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        public void GetAllRoomsList(ref String outXml)
        {
            try
            {
                String inXML = "<login><userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID></login>";
                outXml = CallMyVRMServer("GetAllRoomsList", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region GetCateringServices
        /// <summary>
        /// GetCateringServices,Check Box List
        /// </summary>
        /// <param name="sender"></param>
        public void GetCateringServices(CheckBoxList sender)
        {
            try
            {
                String inXML = "<GetCateringServices>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetCateringServices>";//Organization Module Fixes
                //String outXML = "<CateringServices><Service><ID>1</ID><Name>Breakfast</Name></Service><Service><ID>2</ID><Name>Lunch</Name></Service><Service><ID>3</ID><Name>Snack</Name></Service><Service><ID>4</ID><Name>Dinner</Name></Service><Service><ID>5</ID><Name>Coffee Service</Name></Service></CateringServices>";
                String outXML = CallMyVRMServer("GetCateringServices", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                log.Trace("GetCateringServices: " + outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//CateringServices/Service");
                LoadList(sender, nodes, "ID", "Name");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetCateringServices
        /// <summary>
        /// GetCateringServices , Drop Down List
        /// </summary>
        /// <param name="sender"></param>
        public void GetCateringServices(DropDownList sender)
        {
            try
            {
                String inXML = "<GetCateringServices>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetCateringServices>";//Organization Module Fixes
                //String outXML = "<CateringServices><Service><ID>1</ID><Name>Breakfast</Name></Service><Service><ID>2</ID><Name>Lunch</Name></Service><Service><ID>3</ID><Name>Snack</Name></Service><Service><ID>4</ID><Name>Dinner</Name></Service><Service><ID>5</ID><Name>Coffee Service</Name></Service></CateringServices>";
                String outXML = CallMyVRMServer("GetCateringServices", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//CateringServices/Service");
                LoadList(sender, nodes, "ID", "Name");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetMCUCards
        /// <summary>
        /// GetMCUCards
        /// </summary>
        /// <returns></returns>
        public String GetMCUCards()
        {
            try
            {
                String inXML = "<GetMCUCards>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetMCUCards>";//Organization Module Fixes
                String outXML = CallMyVRMServer("GetMCUCards", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //                outXML = "<MCUCards><MCUCard><ID>1</ID><Name>Card 1</Name></MCUCard><MCUCard><ID>2</ID><Name>Card 2</Name></MCUCard><MCUCard><ID>3</ID><Name>Card 3</Name></MCUCard></MCUCards>";
                return outXML;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return "<error></error>";
            }
        }
        #endregion

        #region LoadList
        /// <summary>
        /// LoadList
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="nodes"></param>
        /// <param name="col1"></param>
        /// <param name="col2"></param>
        public void LoadList(DropDownList lstList, XmlNodeList nodes, String col1, String col2)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();
                String adds = ""; //FB 2602
                XmlNodeList multicode = null;

                foreach (XmlNode node in nodes)
                {
					//FB 2602 - Start
                    adds = "";
                    if (node.SelectNodes("//MultiCodec/Address") != null)
                    {
                        multicode = node.SelectNodes("MultiCodec/Address");
                        for (int i = 0; i < multicode.Count; i++)
                        {
                            if (multicode[i] != null)
                                if (multicode[i].InnerText.Trim() != "")
                                {
                                    if (i > 0)
                                        adds += "�";
                                    
                                    adds += multicode[i].InnerText;
                                }
                        }
                        
                        if (multicode.Count > 0)
                            node.SelectSingleNode("MultiCodec").InnerText = adds;
                    }
					//FB 2602 - End               
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    //Response.Write(ds.Tables[0].Columns[0].ColumnName);
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
					//ZD 104091 - Start
                    if (dt.Columns.Contains("Name") || dt.Columns.Contains("Familyid"))//FB 2272 
                        foreach (DataRow drr in dt.Rows)
                        {
                            if (dt.Columns.Contains("Name"))
                            {
                                if (!(((lstList.ID.IndexOf("lstTopTier") >= 0) || (lstList.ID.IndexOf("lstMiddleTier") >= 0)) && ((HttpContext.Current.Request.Url.ToString().IndexOf("ManageRoomProfile") >= 0) || (HttpContext.Current.Request.Url.ToString().IndexOf("mainadministrator") >= 0)))) //ZD 100288
                                    drr["Name"] = GetTranslatedText(drr["Name"].ToString());
                            }
                            if (dt.Columns.Contains("Familyid"))
                            {
                                if (lstList.ID == "DIVideoEquip") //for DataImport //ZD 104091
                                {
                                    drr["EquipmentDisplayName"] = drr["EquipmentDisplayName"].ToString().ToLower();
                                    drr["VideoEquipmentID"] = drr["VideoEquipmentID"].ToString() + "|" + drr["Familyid"].ToString();
                                }
                            }
                        }
					//ZD 104091 - End
                    DataRow dr = dt.NewRow();
                    dr[0] = "-1";
                    dr[1] = GetTranslatedText("Please select...");//FB 1830 - Translation
                    if (lstList.ID != null) // FB 1860
                    {
                        if ((lstList.ID.IndexOf("lstBridge") >= 0) && (HttpContext.Current.Request.Url.ToString().IndexOf("ConferenceSetup") >= 0 || HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("expressconference") >= 0))  //ZD 100619 //ZD 101178
                            dr[1] = GetTranslatedText("Auto select...");//FB 1830 - Translation
                        if ((lstList.ID.IndexOf("lstTemplates") >= 0) && (HttpContext.Current.Request.Url.ToString().IndexOf("ConferenceSetup") >= 0)) //Added for Template List
                            dr[1] = GetTranslatedText("None...");//FB 1830 - Translation  
                        if ((lstList.ID.IndexOf("lstMCUProfile") >= 0) && (HttpContext.Current.Request.Url.ToString().IndexOf("ConferenceSetup") >= 0)) //FB 2839
                            dr[1] = GetTranslatedText("None");//FB 1830 - Translation 
                        if (((lstList.ID.IndexOf("txtProfileID") >= 0) || (lstList.ID.IndexOf("drpPoolOrder") >= 0)) && (HttpContext.Current.Request.Url.ToString().IndexOf("BridgeDetails") >= 0)) //FB 2876
                            dr[1] = GetTranslatedText("None");
                        if ((lstList.ID.IndexOf("drpPoolOrder") >= 0) && (HttpContext.Current.Request.Url.ToString().IndexOf("ConferenceSetup") >= 0 || HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("expressconference") >= 0)) //ZD 104256
                            dr[1] = GetTranslatedText("None");
                        if ((lstList.ID.IndexOf("lstGuestBridges") >= 0) && (HttpContext.Current.Request.Url.ToString().IndexOf("ConferenceSetup") >= 0 || HttpContext.Current.Request.Url.ToString().ToLower().IndexOf("expressconference") >= 0)) //ZD 100619 //ZD 101178
                            dr[1] = GetTranslatedText("Auto select...");
                        dt.Rows.InsertAt(dr, 0);
                        if ((lstList.ID.IndexOf("lstVideoEquipment") >= 0) && (HttpContext.Current.Request.Url.ToString().IndexOf("EditEndpoint") >= 0)) //ZD 100736
                            dt.Rows.RemoveAt(0);
                        if ((lstList.ID.IndexOf("lstBridges") >= 0) && (HttpContext.Current.Request.Url.ToString().IndexOf("EditEndpoint") >= 0)) //ZD 100040
                            dt.Rows.RemoveAt(0);
                    }
                }
                else
                {
                    dv = new DataView();
                    dt = new DataTable();
                    dt.Columns.Add(col1);
                    dt.Columns.Add(col2);
                    DataRow dr = dt.NewRow();
                    dr[0] = "-1";
                    dr[1] = GetTranslatedText("No Items...");//FB 1830 - Translation
                    dt.Rows.InsertAt(dr, 0);
                }
                //ZD 103723 START
                if (lstList.ID != null)
                {
                    if ((lstList.ID.IndexOf("lstVideoEquipment") >= 0)) 
                    {
                        dt.DefaultView.Sort = "EquipmentDisplayName ASC";
                    }
                }
                //ZD 103723 END
                //ZD 100040 start
                if ((lstList.ID.IndexOf("lstBridges") >= 0) && (HttpContext.Current.Request.Url.ToString().IndexOf("EditEndpoint") >= 0))//ZD 100040
                {
                    dt.Columns.Add("MCUNameandtype");
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        if (dt.Rows[i]["Virtual"].ToString() == "1")
                            dt.Rows[i]["MCUNameandtype"] = dt.Rows[i]["BridgeName"] + " " + "(" + GetTranslatedText("Virtual") + ")";
                        else
                            dt.Rows[i]["MCUNameandtype"] = dt.Rows[i]["BridgeName"] + " " + "(" + GetTranslatedText("Standalone") + ")";
                    }

                    if (dt.Rows.Count > 0)
                    {
                        DataRow[] drVirtual = dt.Select("Virtual=1");
                        DataRow[] drStandard = dt.Select("Virtual=0");
                        if(drVirtual != null && drVirtual.Length > 0)
                            drVirtual.OrderBy(row => row.Field<String>("BridgeName"));
                        if (drStandard != null && drStandard.Length > 0)
                            drStandard.OrderBy(row => row.Field<String>("BridgeName"));
                        try
                        {
                            if (drStandard != null && drVirtual != null)
                                dt = drStandard.Union(drVirtual).CopyToDataTable();

                        }
                        catch (Exception e)
                        {
                            
                           
                        }
                        
                    }

                    DataRow dr = dt.NewRow();
                    dr[0] = "-1";
                    dr[8] = GetTranslatedText("Please select..."); //ZD 104256
                    dt.Rows.InsertAt(dr, 0);

                } //ZD 100040 End
                lstList.DataSource = dt;
                lstList.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion
        //Code Added for FB 1712 - Starts
        public void LoadCountryList(DropDownList lstList, XmlNodeList nodes, String col1, String col2)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }

                //DataTable dt = new DataTable();
                //dt = ds.Tables[0].Clone();

                //string conditionForPriorityCountry = "ID in (38,137,225)";
                //string conditionForRemainingCountry = "ID not in (38,137,225)";
                //string orderByPriorityCountry = "Name DESC";
                //string orderByRemainingCountry = "Name ASC";

                //var result = (from pc in ds.Tables[0].Select(conditionForPriorityCountry, orderByPriorityCountry) select pc).
                //    Union(from rc in ds.Tables[0].Select(conditionForRemainingCountry, orderByRemainingCountry) select rc);

                //foreach (var item in result)
                //{
                //    dt.ImportRow(item);
                //}
                //ds.Tables[0].DefaultView.Sort = "Name ASC";
                //FB 2657 START
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].NewRow();
                    dr["ID"] = "-1";
                    dr["Name"] = GetTranslatedText("Please select...");//FB 1830 -Translation
                    ds.Tables[0].Rows.InsertAt(dr, 0);
                }
                else
                {
                    ds.Tables[0].Columns.Add(col1);
                    ds.Tables[0].Columns.Add(col2);
                    DataRow dr = ds.Tables[0].NewRow();
                    dr["ID"] = "-1";
                    dr["Name"] = GetTranslatedText("No Items...");//FB 1830 -Translation
                    ds.Tables[0].Rows.InsertAt(dr, 0);
                }

                lstList.DataSource = ds.Tables[0];
                lstList.DataBind();
                //FB 2657 END

             
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        //Code Added for FB 1712 - End

        #region LoadList
        /// <summary>
        /// LoadList
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="nodes"></param>
        /// <param name="col1"></param>
        /// <param name="col2"></param>
        public void LoadList(ListBox lstList, XmlNodeList nodes, String col1, String col2)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv = new DataView();
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                {
                    //Response.Write(ds.Tables[0].Columns[0].ColumnName);
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                }
                lstList.DataSource = dt;
                lstList.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion

        #region LoadList
        /// <summary>
        /// LoadList
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="nodes"></param>
        /// <param name="col1"></param>
        /// <param name="col2"></param>
        public void LoadList(CheckBoxList lstList, XmlNodeList nodes, String col1, String col2)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv = new DataView();
                DataTable dt = new DataTable();
                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    if (dt.Columns.Contains("Name"))//FB 2272
                        foreach (DataRow dr in dt.Rows)
                            dr["Name"] = GetTranslatedText(dr["Name"].ToString());

                    lstList.DataSource = ds.Tables[0];
                    lstList.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace("LoadList: " + ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion

        #region IsValidMCUForMPI
        /// <summary>
        /// IsValidMCUForMPI
        /// </summary>
        /// <param name="BridgeID"></param>
        /// <param name="errMsg"></param>
        /// <returns></returns>
        public bool IsValidMCUForMPI(String BridgeID, ref String errMsg)
        {
            try
            {
                String inXML = "";
                inXML += "<login>";
                inXML += OrgXMLElement();//Organization Module Fixes
                inXML += "  <userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>";
                inXML += "  <bridgeID>" + BridgeID + "</bridgeID>";
                inXML += "</login>";

                String outXML = CallMyVRMServer("GetOldBridge", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    int bridgeType = Int32.Parse(xmldoc.SelectSingleNode("//bridge/bridgeType").InnerText);
                    XmlNodeList nodes = xmldoc.SelectNodes("//bridge/bridgeTypes/type");
                    XmlNodeList nodesMPI = xmldoc.SelectNodes("//bridge/bridgeDetails/MPIServices/MPIService");
                    //Response.Write(bridgeType + " : " + nodes.Count + " : " + nodes[bridgeType - 1].SelectSingleNode("interfaceType").InnerText);
                    if (nodes[bridgeType - 1].SelectSingleNode("interfaceType").InnerText.Equals(ns_MyVRMNet.vrmMCUInterfaceType.Polycom)
                        && nodesMPI.Count > 0)
                        return true;
                }
                return false;

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                return false;
            }
        }
        #endregion

        //Code added by offshore to extract the selected room details - FB Issue No 412- start

        //Code added by offshore to extract the selected room details - FB Issue No 412- end

        //FB 1133 Added to avoid empty labels        

        #region GetProperValue
        /// <summary>
        /// Method to return N/A if the input string is empty
        /// If the input String has the value then it will return the input string
        /// </summary>
        /// <param name="InString"></param>
        /// <returns></returns>
        public String GetProperValue(String InString)
        {
            String OutString = "";
            try
            {
                if (InString.Trim().Length > 0)
                    OutString = InString.Trim();
                else
                    OutString = GetTranslatedText("N/A"); //ZD 100288
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                OutString = GetTranslatedText("N/A"); //ZD 100288
            }
            return OutString;
        }
        #endregion

        #region GetCountryCodes
        /// <summary>
        /// GetCountryCodes
        /// </summary>
        /// <param name="lstList"></param>
        public void GetCountryCodes(DropDownList lstList)
        {
            try
            {
                String inXML = "<GetCountryCodes><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>" + OrgXMLElement() + "</GetCountryCodes>";//Organization Module Fixes
                String outXML = CallMyVRMServer("GetCountryCodes", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //outXML = "<GetCountryCodes><Country><ID>225</ID><Name>United States</Name></Country><Country><ID>137</ID><Name>Mexico</Name></Country><Country><ID>38</ID><Name>Canada</Name></Country><Country><ID>238</ID><Name>Zimbabwe</Name></Country><Country><ID>237</ID><Name>Zambia</Name></Country><Country><ID>236</ID><Name>Yemen</Name></Country><Country><ID>235</ID><Name>Western Sahara</Name></Country><Country><ID>234</ID><Name>Wallis and Futuna</Name></Country><Country><ID>233</ID><Name>Virgin Islands, U.S.</Name></Country><Country><ID>232</ID><Name>Virgin Islands, British</Name></Country><Country><ID>231</ID><Name>Viet Nam</Name></Country><Country><ID>230</ID><Name>Venezuela</Name></Country><Country><ID>229</ID><Name>Vanuatu</Name></Country><Country><ID>228</ID><Name>Uzbekistan</Name></Country><Country><ID>227</ID><Name>Uruguay</Name></Country><Country><ID>226</ID><Name>United States Minor Outlying Islands</Name></Country><Country><ID>224</ID><Name>United Kingdom</Name></Country><Country><ID>223</ID><Name>United Arab Emirates</Name></Country><Country><ID>222</ID><Name>Ukraine</Name></Country><Country><ID>221</ID><Name>Uganda</Name></Country><Country><ID>220</ID><Name>Tuvalu</Name></Country><Country><ID>219</ID><Name>Turks and Caicos Islands</Name></Country><Country><ID>218</ID><Name>Turkmenistan</Name></Country><Country><ID>217</ID><Name>Turkey</Name></Country><Country><ID>216</ID><Name>Tunisia</Name></Country><Country><ID>215</ID><Name>Trinidad and Tobago</Name></Country><Country><ID>214</ID><Name>Tonga</Name></Country><Country><ID>213</ID><Name>Tokelau</Name></Country><Country><ID>212</ID><Name>Togo</Name></Country><Country><ID>211</ID><Name>Timor-leste</Name></Country><Country><ID>210</ID><Name>Thailand</Name></Country><Country><ID>209</ID><Name>Tanzania, United Republic of</Name></Country><Country><ID>208</ID><Name>Tajikistan</Name></Country><Country><ID>207</ID><Name>Taiwan, Province of China</Name></Country><Country><ID>206</ID><Name>Syrian Arab Republic</Name></Country><Country><ID>205</ID><Name>Switzerland</Name></Country><Country><ID>204</ID><Name>Sweden</Name></Country><Country><ID>203</ID><Name>Swaziland</Name></Country><Country><ID>202</ID><Name>Svalbard and Jan Mayen</Name></Country><Country><ID>201</ID><Name>Suriname</Name></Country><Country><ID>200</ID><Name>Sudan</Name></Country><Country><ID>199</ID><Name>Sri Lanka</Name></Country><Country><ID>198</ID><Name>Spain</Name></Country><Country><ID>197</ID><Name>South Georgia and The South Sandwich Islands</Name></Country><Country><ID>196</ID><Name>South Africa</Name></Country><Country><ID>195</ID><Name>Somalia</Name></Country><Country><ID>194</ID><Name>Solomon Islands</Name></Country><Country><ID>193</ID><Name>Slovenia</Name></Country><Country><ID>192</ID><Name>Slovakia</Name></Country><Country><ID>191</ID><Name>Singapore</Name></Country><Country><ID>190</ID><Name>Sierra Leone</Name></Country><Country><ID>189</ID><Name>Seychelles</Name></Country><Country><ID>188</ID><Name>Serbia and Montenegro</Name></Country><Country><ID>187</ID><Name>Senegal</Name></Country><Country><ID>186</ID><Name>Saudi Arabia</Name></Country><Country><ID>185</ID><Name>Sao Tome and Principe</Name></Country><Country><ID>184</ID><Name>San Marino</Name></Country><Country><ID>183</ID><Name>Samoa</Name></Country><Country><ID>182</ID><Name>Saint Vincent and The Grenadines</Name></Country><Country><ID>181</ID><Name>Saint Pierre and Miquelon</Name></Country><Country><ID>180</ID><Name>Saint Lucia</Name></Country><Country><ID>179</ID><Name>Saint Kitts and Nevis</Name></Country><Country><ID>178</ID><Name>Saint Helena</Name></Country><Country><ID>177</ID><Name>Rwanda</Name></Country><Country><ID>176</ID><Name>Russian Federation</Name></Country><Country><ID>175</ID><Name>Romania</Name></Country><Country><ID>174</ID><Name>Reunion</Name></Country><Country><ID>173</ID><Name>Qatar</Name></Country><Country><ID>172</ID><Name>Puerto Rico</Name></Country><Country><ID>171</ID><Name>Portugal</Name></Country><Country><ID>170</ID><Name>Poland</Name></Country><Country><ID>169</ID><Name>Pitcairn</Name></Country><Country><ID>168</ID><Name>Philippines</Name></Country><Country><ID>167</ID><Name>Peru</Name></Country><Country><ID>166</ID><Name>Paraguay</Name></Country><Country><ID>165</ID><Name>Papua New Guinea</Name></Country><Country><ID>164</ID><Name>Panama</Name></Country><Country><ID>163</ID><Name>Palestinian Territory, Occupied</Name></Country><Country><ID>162</ID><Name>Palau</Name></Country><Country><ID>161</ID><Name>Pakistan</Name></Country><Country><ID>160</ID><Name>Oman</Name></Country><Country><ID>159</ID><Name>Norway</Name></Country><Country><ID>158</ID><Name>Northern Mariana Islands</Name></Country><Country><ID>157</ID><Name>Norfolk Island</Name></Country><Country><ID>156</ID><Name>Niue</Name></Country><Country><ID>155</ID><Name>Nigeria</Name></Country><Country><ID>154</ID><Name>Niger</Name></Country><Country><ID>153</ID><Name>Nicaragua</Name></Country><Country><ID>152</ID><Name>New Zealand</Name></Country><Country><ID>151</ID><Name>New Caledonia</Name></Country><Country><ID>150</ID><Name>Netherlands Antilles</Name></Country><Country><ID>149</ID><Name>Netherlands</Name></Country><Country><ID>148</ID><Name>Nepal</Name></Country><Country><ID>147</ID><Name>Nauru</Name></Country><Country><ID>146</ID><Name>Namibia</Name></Country><Country><ID>145</ID><Name>Myanmar</Name></Country><Country><ID>144</ID><Name>Mozambique</Name></Country><Country><ID>143</ID><Name>Morocco</Name></Country><Country><ID>142</ID><Name>Montserrat</Name></Country><Country><ID>141</ID><Name>Mongolia</Name></Country><Country><ID>140</ID><Name>Monaco</Name></Country><Country><ID>139</ID><Name>Moldova, Republic of</Name></Country><Country><ID>138</ID><Name>Micronesia, Federated States of</Name></Country><Country><ID>136</ID><Name>Mayotte</Name></Country><Country><ID>135</ID><Name>Mauritius</Name></Country><Country><ID>134</ID><Name>Mauritania</Name></Country><Country><ID>133</ID><Name>Martinique</Name></Country><Country><ID>132</ID><Name>Marshall Islands</Name></Country><Country><ID>131</ID><Name>Malta</Name></Country><Country><ID>130</ID><Name>Mali</Name></Country><Country><ID>129</ID><Name>Maldives</Name></Country><Country><ID>128</ID><Name>Malaysia</Name></Country><Country><ID>127</ID><Name>Malawi</Name></Country><Country><ID>126</ID><Name>Madagascar</Name></Country><Country><ID>125</ID><Name>Macedonia, The Former Yugoslav Republic of</Name></Country><Country><ID>124</ID><Name>Macao</Name></Country><Country><ID>123</ID><Name>Luxembourg</Name></Country><Country><ID>122</ID><Name>Lithuania</Name></Country><Country><ID>121</ID><Name>Liechtenstein</Name></Country><Country><ID>120</ID><Name>Libyan Arab Jamahiriya</Name></Country><Country><ID>119</ID><Name>Liberia</Name></Country><Country><ID>118</ID><Name>Lesotho</Name></Country><Country><ID>117</ID><Name>Lebanon</Name></Country><Country><ID>116</ID><Name>Latvia</Name></Country><Country><ID>115</ID><Name>Lao People's Democratic Republic</Name></Country><Country><ID>114</ID><Name>Kyrgyzstan</Name></Country><Country><ID>113</ID><Name>Kuwait</Name></Country><Country><ID>112</ID><Name>Korea, Republic of</Name></Country><Country><ID>111</ID><Name>Korea, Democratic People's Republic of</Name></Country><Country><ID>110</ID><Name>Kiribati</Name></Country><Country><ID>109</ID><Name>Kenya</Name></Country><Country><ID>108</ID><Name>Kazakhstan</Name></Country><Country><ID>107</ID><Name>Jordan</Name></Country><Country><ID>106</ID><Name>Japan</Name></Country><Country><ID>105</ID><Name>Jamaica</Name></Country><Country><ID>104</ID><Name>Italy</Name></Country><Country><ID>103</ID><Name>Israel</Name></Country><Country><ID>102</ID><Name>Ireland</Name></Country><Country><ID>101</ID><Name>Iraq</Name></Country><Country><ID>100</ID><Name>Iran, Islamic Republic of</Name></Country><Country><ID>99</ID><Name>Indonesia</Name></Country><Country><ID>98</ID><Name>India</Name></Country><Country><ID>97</ID><Name>Iceland</Name></Country><Country><ID>96</ID><Name>Hungary</Name></Country><Country><ID>95</ID><Name>Hong Kong</Name></Country><Country><ID>94</ID><Name>Honduras</Name></Country><Country><ID>93</ID><Name>Holy See (Vatican City State)</Name></Country><Country><ID>92</ID><Name>Heard Island and Mcdonald Islands</Name></Country><Country><ID>91</ID><Name>Haiti</Name></Country><Country><ID>90</ID><Name>Guyana</Name></Country><Country><ID>89</ID><Name>Guinea-bissau</Name></Country><Country><ID>88</ID><Name>Guinea</Name></Country><Country><ID>87</ID><Name>Guatemala</Name></Country><Country><ID>86</ID><Name>Guam</Name></Country><Country><ID>85</ID><Name>Guadeloupe</Name></Country><Country><ID>84</ID><Name>Grenada</Name></Country><Country><ID>83</ID><Name>Greenland</Name></Country><Country><ID>82</ID><Name>Greece</Name></Country><Country><ID>81</ID><Name>Gibraltar</Name></Country><Country><ID>80</ID><Name>Ghana</Name></Country><Country><ID>79</ID><Name>Germany</Name></Country><Country><ID>78</ID><Name>Georgia</Name></Country><Country><ID>77</ID><Name>Gambia</Name></Country><Country><ID>76</ID><Name>Gabon</Name></Country><Country><ID>75</ID><Name>French Southern Territories</Name></Country><Country><ID>74</ID><Name>French Polynesia</Name></Country><Country><ID>73</ID><Name>French Guiana</Name></Country><Country><ID>72</ID><Name>France</Name></Country><Country><ID>71</ID><Name>Finland</Name></Country><Country><ID>70</ID><Name>Fiji</Name></Country><Country><ID>69</ID><Name>Faroe Islands</Name></Country><Country><ID>68</ID><Name>Falkland Islands (Malvinas)</Name></Country><Country><ID>67</ID><Name>Ethiopia</Name></Country><Country><ID>66</ID><Name>Estonia</Name></Country><Country><ID>65</ID><Name>Eritrea</Name></Country><Country><ID>64</ID><Name>Equatorial Guinea</Name></Country><Country><ID>63</ID><Name>El Salvador</Name></Country><Country><ID>62</ID><Name>Egypt</Name></Country><Country><ID>61</ID><Name>Ecuador</Name></Country><Country><ID>60</ID><Name>Dominican Republic</Name></Country><Country><ID>59</ID><Name>Dominica</Name></Country><Country><ID>58</ID><Name>Djibouti</Name></Country><Country><ID>57</ID><Name>Denmark</Name></Country><Country><ID>56</ID><Name>Czech Republic</Name></Country><Country><ID>55</ID><Name>Cyprus</Name></Country><Country><ID>54</ID><Name>Cuba</Name></Country><Country><ID>53</ID><Name>Croatia</Name></Country><Country><ID>52</ID><Name>Costa Rica</Name></Country><Country><ID>51</ID><Name>Cook Islands</Name></Country><Country><ID>50</ID><Name>Congo, The Democratic Republic of The</Name></Country><Country><ID>49</ID><Name>Congo</Name></Country><Country><ID>48</ID><Name>Comoros</Name></Country><Country><ID>47</ID><Name>Colombia</Name></Country><Country><ID>46</ID><Name>Cocos (Keeling) Islands</Name></Country><Country><ID>45</ID><Name>Christmas Island</Name></Country><Country><ID>44</ID><Name>China</Name></Country><Country><ID>43</ID><Name>Chile</Name></Country><Country><ID>42</ID><Name>Chad</Name></Country><Country><ID>41</ID><Name>Central African Republic</Name></Country><Country><ID>40</ID><Name>Cayman Islands</Name></Country><Country><ID>39</ID><Name>Cape Verde</Name></Country><Country><ID>37</ID><Name>Cameroon</Name></Country><Country><ID>36</ID><Name>Cambodia</Name></Country><Country><ID>35</ID><Name>Burundi</Name></Country><Country><ID>34</ID><Name>Burkina Faso</Name></Country><Country><ID>33</ID><Name>Bulgaria</Name></Country><Country><ID>32</ID><Name>Brunei Darussalam</Name></Country><Country><ID>31</ID><Name>British Indian Ocean Territory</Name></Country><Country><ID>30</ID><Name>Brazil</Name></Country><Country><ID>29</ID><Name>Bouvet Island</Name></Country><Country><ID>28</ID><Name>Botswana</Name></Country><Country><ID>27</ID><Name>Bosnia and Herzegovina</Name></Country><Country><ID>26</ID><Name>Bolivia</Name></Country><Country><ID>25</ID><Name>Bhutan</Name></Country><Country><ID>24</ID><Name>Bermuda</Name></Country><Country><ID>23</ID><Name>Benin</Name></Country><Country><ID>22</ID><Name>Belize</Name></Country><Country><ID>21</ID><Name>Belgium</Name></Country><Country><ID>20</ID><Name>Belarus</Name></Country><Country><ID>19</ID><Name>Barbados</Name></Country><Country><ID>18</ID><Name>Bangladesh</Name></Country><Country><ID>17</ID><Name>Bahrain</Name></Country><Country><ID>16</ID><Name>Bahamas</Name></Country><Country><ID>15</ID><Name>Azerbaijan</Name></Country><Country><ID>14</ID><Name>Austria</Name></Country><Country><ID>13</ID><Name>Australia</Name></Country><Country><ID>12</ID><Name>Aruba</Name></Country><Country><ID>11</ID><Name>Armenia</Name></Country><Country><ID>10</ID><Name>Argentina</Name></Country><Country><ID>9</ID><Name>Antigua and Barbuda</Name></Country><Country><ID>8</ID><Name>Antarctica</Name></Country><Country><ID>7</ID><Name>Anguilla</Name></Country><Country><ID>6</ID><Name>Angola</Name></Country><Country><ID>5</ID><Name>Andorra</Name></Country><Country><ID>4</ID><Name>American Samoa</Name></Country><Country><ID>3</ID><Name>Algeria</Name></Country><Country><ID>2</ID><Name>Albania</Name></Country><Country><ID>1</ID><Name>Afghanistan</Name></Country></GetCountryCodes>";
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetCountryCodes/Country");
                LoadCountryList(lstList, nodes, "ID", "Name"); //Added for FB 1712
                //LoadList(lstList, nodes, "ID", "Name");
            }
            catch (Exception ex)
            {
                log.Trace("GetCountryCodes: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetCountryStates
        /// <summary>
        /// GetCountryStates
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="strCountryCode"></param>
        public void GetCountryStates(DropDownList lstList, String strCountryCode)
        {
            try
            {
                String inXML = "<GetCountryStates><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>" + OrgXMLElement() + "<CountryCode>" + strCountryCode + "</CountryCode></GetCountryStates>";//Organization Module Fixes
                String outXML = CallMyVRMServer("GetCountryStates", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                //outXML = "<GetCountryStates><State>ID>1</ID><Name>Alabama </Name><Code>AL</Code></State><State>ID>2</ID><Name>Alaska </Name><Code>AK</Code></State><State>ID>3</ID><Name>Arizona </Name><Code>AZ</Code></State><State>ID>4</ID><Name>Arkansas </Name><Code>AR</Code></State><State>ID>5</ID><Name>California </Name><Code>CA</Code></State><State>ID>6</ID><Name>Colorado </Name><Code>CO</Code></State><State>ID>7</ID><Name>Connecticut </Name><Code>CT</Code></State><State>ID>8</ID><Name>Delaware </Name><Code>DE</Code></State><State>ID>9</ID><Name>District of Columbia </Name><Code>DC</Code></State><State>ID>10</ID><Name>Florida </Name><Code>FL</Code></State><State>ID>11</ID><Name>Georgia </Name><Code>GA</Code></State><State>ID>12</ID><Name>Guam </Name><Code>GU</Code></State><State>ID>13</ID><Name>Hawaii </Name><Code>HI</Code></State><State>ID>14</ID><Name>Idaho </Name><Code>ID</Code></State><State>ID>15</ID><Name>Illinois </Name><Code>IL</Code></State><State>ID>16</ID><Name>Indiana </Name><Code>IN</Code></State><State>ID>17</ID><Name>Iowa </Name><Code>IA</Code></State><State>ID>18</ID><Name>Kansas </Name><Code>KS</Code></State><State>ID>19</ID><Name>Kentucky </Name><Code>KY</Code></State><State>ID>20</ID><Name>Louisiana </Name><Code>LA</Code></State><State>ID>21</ID><Name>Maine </Name><Code>ME</Code></State><State>ID>22</ID><Name>Maryland </Name><Code>MD</Code></State><State>ID>23</ID><Name>Massachusetts </Name><Code>MA</Code></State><State>ID>24</ID><Name>Michigan </Name><Code>MI</Code></State><State>ID>25</ID><Name>Minnesota </Name><Code>MN</Code></State><State>ID>26</ID><Name>Mississippi </Name><Code>MS</Code></State><State>ID>27</ID><Name>Missouri </Name><Code>MO</Code></State><State>ID>28</ID><Name>Montana </Name><Code>MT</Code></State><State>ID>29</ID><Name>Nebraska </Name><Code>NE</Code></State><State>ID>30</ID><Name>Nevada </Name><Code>NV</Code></State><State>ID>31</ID><Name>New Hampshire </Name><Code>NH</Code></State><State>ID>32</ID><Name>New Jersey </Name><Code>NJ</Code></State><State>ID>33</ID><Name>New Mexico </Name><Code>NM</Code></State><State>ID>34</ID><Name>New York </Name><Code>NY</Code></State><State>ID>35</ID><Name>North Carolina </Name><Code>NC</Code></State><State>ID>36</ID><Name>North Dakota </Name><Code>ND</Code></State><State>ID>37</ID><Name>Ohio </Name><Code>OH</Code></State><State>ID>38</ID><Name>Oklahoma </Name><Code>OK</Code></State><State>ID>39</ID><Name>Oregon </Name><Code>OR</Code></State><State>ID>40</ID><Name>Pennyslvania </Name><Code>PA</Code></State><State>ID>41</ID><Name>Puerto Rico </Name><Code>PR</Code></State><State>ID>42</ID><Name>Rhode Island </Name><Code>RI</Code></State><State>ID>43</ID><Name>South Carolina </Name><Code>SC</Code></State><State>ID>44</ID><Name>South Dakota </Name><Code>SD</Code></State><State>ID>45</ID><Name>Tennessee </Name><Code>TN</Code></State><State>ID>46</ID><Name>Texas </Name><Code>TX</Code></State><State>ID>47</ID><Name>Utah </Name><Code>UT</Code></State><State>ID>48</ID><Name>Vermont </Name><Code>VT</Code></State><State>ID>49</ID><Name>Virginia </Name><Code>VA</Code></State><State>ID>50</ID><Name>Virgin Islands </Name><Code>VI</Code></State><State>ID>51</ID><Name>Washington </Name><Code>WA</Code></State><State>ID>52</ID><Name>West Virginia </Name><Code>WV</Code></State><State>ID>53</ID><Name>Wisconsin </Name><Code>WI</Code></State><State>ID>54</ID><Name>Wyoming </Name>";
                log.Trace("GetCountryStates outxml: " + outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetCountryStates/State");
                LoadList(lstList, nodes, "ID", "Code");
            }
            catch (Exception ex)
            {
                log.Trace("GetCountryStates: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //Code Added for Binding Template List - START
        #region GetTemplateNames
        /// <summary>
        /// GetCountryStates
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="strCountryCode"></param>
        public void GetTemplateNames(DropDownList lstList)
        {
            try
            {
                //FB 2027 - Strats
                StringBuilder inXML = new StringBuilder();
                inXML.Append("<login>");
                inXML.Append(OrgXMLElement());//Organization Module Fixes
                inXML.Append("<userID>" + HttpContext.Current.Session["userID"].ToString() + "</userID>");
                //inXML.Append("<sortBy>" + txtSortBy.Text + "</sortBy>");
                inXML.Append("<sortBy></sortBy>");
                inXML.Append("</login>");
                log.Trace(inXML.ToString());
                //String outXML = CallCOM("GetTemplateList", inXML, HttpContext.Current.Application["COM_ConfigPath"].ToString());
                String outXML = CallMyVRMServer("GetTemplateList", inXML.ToString(), HttpContext.Current.Application["MyVRMserver_ConfigPath"].ToString());
                //FB 2027 - End
                log.Trace("GetTemplateList outxml: " + outXML);
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//templates/template");
                LoadList(lstList, nodes, "ID", "name");
            }
            catch (Exception ex)
            {
                log.Trace("GetTemplateList: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //Code Added for Binding Template List - End


        //Code added by offshore - FB Issue No 412- start

        #region  GetOrgLocNames

        public string GetOrgLocNames(string inXML, string level)
        {
            string orglocids = "";
            string tag1 = "";
            string tag2 = "";
            string tag3 = "";
            int index = 0;
            string levelstr = "";
            string lid = "";

            string getOrgLocNames = "";
            orglocids = GetOrgLocIDs(inXML, level);
            if (orglocids == "error")
            {
                getOrgLocNames = "error";
            }
            else
            {
                switch (level)
                {
                    case "1":
                        tag1 = "level1";
                        tag2 = "level1ID";
                        tag3 = "level1Name";
                        break;
                    case "2":
                        tag1 = "level2";
                        tag2 = "level2ID";
                        tag3 = "level2Name";
                        break;
                    case "3":
                        tag1 = "level3";
                        tag2 = "level3ID";
                        tag3 = "level3Name";
                        break;
                }
                getOrgLocNames = "";
                index = 0;
                while (inXML.IndexOf("<" + tag1 + ">", index) >= 0)
                {
                    levelstr = ParseStr(Convert.ToString(inXML), tag1, index);
                    index = ParseLoc(Convert.ToString(inXML), tag1, index);
                    lid = ParseStr(levelstr, tag2, 0);
                    if ((orglocids.IndexOf(", " + lid + ", ") >= 0))
                    {
                        getOrgLocNames = getOrgLocNames + "<a href=\"#\" onclick=\"javascript:chkresource('" + lid + "');\">" + ParseStr(levelstr, tag3, 0) + "</a>, ";
                    }
                }
            }
            if ((getOrgLocNames.IndexOf(", ") >= 0))
            {
                getOrgLocNames = getOrgLocNames.Substring(0, getOrgLocNames.Length - 2);
            }
            return getOrgLocNames;
        }

        #endregion

        #region  GetOrgLocIDs

        public string GetOrgLocIDs(string inXML, String level)
        {
            string getOrgLocIDs = "";
            Int32 index = 0;
            Int32 selectedindex = 0;
            string selected3IDs = "";
            string selected2IDs = "";
            string selected1IDs = "";
            string selected = "";

            if (inXML.IndexOf("<selected>", index) >= 0)
            {
                selected = ParseStr(Convert.ToString(inXML), "selected", index);
                index = ParseLoc(Convert.ToString(inXML), "selected", index);

                while ((selected.IndexOf("<level3ID>", selectedindex) >= 0))
                {
                    selected3IDs = selected3IDs + ParseStr(selected, "level3ID", selectedindex) + ", ";
                    selectedindex = ParseLoc(selected, "level3ID", selectedindex);
                }
                if ((selected3IDs != ""))
                {
                    getOrgLocIDs = selected3IDs;
                }
                selectedindex = 0;
                while ((selected.IndexOf("<level2ID>", selectedindex) >= 0))
                {
                    selected2IDs = selected2IDs + ParseStr(selected, "level2ID", selectedindex) + ", ";
                    selectedindex = ParseLoc(selected, "level2ID", selectedindex);
                }
                if ((selected2IDs != ""))
                {
                    getOrgLocIDs = selected2IDs;
                }
                selectedindex = 0;
                while ((selected.IndexOf("<level1ID>", selectedindex) >= 0))
                {
                    selected1IDs = selected1IDs + ParseStr(selected, "level1ID", selectedindex) + ", ";
                    selectedindex = ParseLoc(selected, "level1ID", selectedindex);
                }
                if ((selected1IDs != ""))
                {
                    getOrgLocIDs = selected1IDs;
                }
                if ((Convert.ToString(level) == "1" && (selected2IDs != "" || selected3IDs != "")) || (Convert.ToString(level) == "2" && (selected1IDs != "" || selected3IDs != "")) || (Convert.ToString(level) == "3" && (selected1IDs != "" || selected2IDs != "")))
                {
                    getOrgLocIDs = "error";
                }
                else
                {
                    getOrgLocIDs = ", " + getOrgLocIDs;
                }
            }
            else
            {
                getOrgLocIDs = ", ";
                //generally should not need, because no matter whether there is pre-selected conf, it must have <selected>, but add this for COM's bug.
            }
            return getOrgLocIDs;
        }

        #endregion

        #region ParseLoc

        public int ParseLoc(string org_str, string tag, int startfrom)
        {
            int parseLoc = 0;
            string start_tag = "";
            string end_tag = "";
            int index1 = 0;

            start_tag = "<" + tag + ">";
            end_tag = "</" + tag + ">";
            index1 = org_str.IndexOf(start_tag, startfrom);
            if (index1 < 0)
            {
                parseLoc = startfrom;
            }
            else
            {
                parseLoc = org_str.IndexOf(end_tag, index1) + end_tag.Length;
            }
            return parseLoc;
        }

        #endregion

        #region ParseStr

        public string ParseStr(string org_str, string tag, int startfrom)
        {
            string parseStr = "";
            string start_tag = "";
            string end_tag = "";
            int index1 = 0;
            int index2 = 0;
            string tmpstr = "";

            // On Error Resume Next (UNSUPPORT) 
            start_tag = "<" + tag + ">";
            end_tag = "</" + tag + ">";
            if ((org_str == "") || (startfrom < 0))
            {
                parseStr = "";
            }
            else
            {
                index1 = org_str.IndexOf(start_tag, startfrom);
                if ((index1 < 0))
                {
                    parseStr = "";
                }
                else
                {
                    index2 = org_str.IndexOf(end_tag, index1);
                    if ((index2 < 0))
                    {
                        if (Convert.ToString(HttpContext.Current.Application["contactEmail"]) == "")
                        {
                            tmpstr = HttpContext.Current.Application["contactName"].ToString();
                        }
                        else
                        {
                            tmpstr = @"<a href='mailto:" + Convert.ToString(HttpContext.Current.Application["contactEmail"]) + "'>" + Convert.ToString(HttpContext.Current.Application["contactName"]) + "</a>";
                        }
                        //err.Raise 152, "parseStr", "error occurrs in parseStr: " & tag
                        //throw new Exception("<br><br><font color='red' size=2><b>Sorry, error occurrs in parseStr: " + tag + ". Please contact with " + Convert.ToString(tmpstr) + " for this problem. Thank you.</b></font>");//FB 1881
                        log.Trace("Sorry, error occurs in parseStr:" + tag + ". Please contact with " + Convert.ToString(tmpstr) + " for this problem. Thank you.");
                        throw new Exception(errXML);
                    }
                    else
                    {
                        //parseStr = org_str.Substring(index1 + start_tag.Length - 1, index2 - index1 - start_tag.Length);
                        parseStr = org_str.Substring(index1 + start_tag.Length, index2 - index1 - start_tag.Length);
                    }
                }
            }
            return parseStr;
        }
        #endregion

        #region DelStringRev - Reverse String

        public String DelStringRev(String string1, String string2)
        {
            if (string1 != "")
            {
                if (string1.LastIndexOf(string2) == (string1.Length - string2.Length))
                {
                    return string1.Substring(0, (string1.Length - string2.Length));
                }
            }
            return string1;
        }
        #endregion

        #region mkLocJSstr - Method to build the location string for JS
        /// <summary>
        ///  mkLocJSstr method of Functions.cs
        ///  queryStringVal - value of the querystring param f
        /// </summary>
        /// <param name="inXML"></param>
        /// <param name="onlySelected"></param>
        /// <param name="queryStringFVal"></param>
        /// <returns></returns>
        public String mkLocJSstr(String inXML, Boolean onlySelected, String queryStringFVal)
        {
            string roomID = "";
            string orderOnly = "";
            string selected1IDs = "";
            string selected2IDs = "";
            string selected3IDs = "";
            string selectedIDs = "";
            string selected = "";
            string errMsg = "";
            string locstr = "";
            string locXML = "";
            bool isWrong = false;
            bool isOnlyDelRooms = true;//Added for Location Issues

            string l3s = "";
            string l2s = "";
            string l1s = "";
            string resultStr = "";

            Int32 index = 0;    //Location Issues
            int selectedindex = 0;
            int l3i = 0;
            int l2i = 0;
            int l1i = 0;

            string curl3id = "";
            string curl2id = "";
            string curl1id = "";
            try
            {
                /* for GetFoodOrderByOrder  begin */

                orderOnly = ParseStr(inXML, "orderOnly", 0);
                if (orderOnly == "1")
                {
                    roomID = ParseStr(inXML, "roomID", 0);
                    if (roomID == "")
                    {
                        return "";
                    }
                    else
                    {
                        selected1IDs = "#" + roomID + "#";
                        selected2IDs = "#";
                        selected3IDs = "#";
                        selectedIDs = "";
                    }
                }
                /* for GetFoodOrderByOrder end */

                /* added for GetFoodOrderByOrder */
                if (roomID == "")
                {
                    selected1IDs = "#";
                    selected2IDs = "#";
                    selected3IDs = "#";
                    selectedIDs = "";
                }

                if (inXML != "")    //Location Issues
                {
                    if (inXML.IndexOf("<selected>", index) >= 0) //Location Issues
                    {
                        selected = ParseStr(inXML, "selected", index);
                        index = ParseLoc(inXML, "selected", index);

                        if (selected != "")
                        {
                            selectedindex = 0;
                            while (selected.IndexOf("<level3ID>", selectedindex) >= 0) //Location Issues
                            {
                                selected3IDs = selected3IDs + ParseStr(selected, "level3ID", selectedindex) + "#";
                                selectedindex = ParseLoc(selected, "level3ID", selectedindex);
                            }

                            selectedindex = 0;
                            while (selected.IndexOf("<level2ID>", selectedindex) >= 0) //Location Issues
                            {
                                selected2IDs = selected2IDs + ParseStr(selected, "level2ID", selectedindex) + "#";
                                selectedindex = ParseLoc(selected, "level2ID", selectedindex);
                            }

                            selectedindex = 0;
                            while (selected.IndexOf("<level1ID>", selectedindex) >= 0) //Location Issues
                            {
                                selected1IDs = selected1IDs + ParseStr(selected, "level1ID", selectedindex) + "#";
                                selectedindex = ParseLoc(selected, "level1ID", selectedindex);
                            }

                            if (!((selected1IDs != "#" && selected2IDs == "#" && selected3IDs == "#") || (selected1IDs == "#" && selected2IDs != "#" && selected3IDs == "#") || (selected1IDs == "#" && selected2IDs == "#" && selected3IDs != "#") || (selected1IDs == "#" && selected2IDs == "#" && selected3IDs == "#")))
                            {
                                isWrong = true;
                            }
                        }
                    }

                    /* added for GetFoodOrderByOrder */

                    locXML = ParseStr(inXML, "level3List", 0);

                    if (locXML == "")
                        isWrong = true;


                    if (inXML.IndexOf("<level1>", index) >= 0)  //Location Issues
                    {
                        if (selected2IDs != "#" || selected3IDs != "#")
                        {
                            isWrong = true;
                        }
                    }
                    else
                    {
                        if (inXML.IndexOf("<level2>", index) >= 0) //Location Issues
                        {
                            if (selected1IDs != "#" || selected3IDs != "#")
                            {
                                isWrong = true;
                            }
                            else
                            {
                                if (inXML.IndexOf("<level3>", index) >= 0)  //Location Issues
                                {
                                    if (selected1IDs != "#" || selected2IDs != "#")
                                        isWrong = true;
                                }
                            }
                        }
                    }

                    l3i = 0;    //Location Issues
                    while (locXML.IndexOf("<level3>", l3i) >= 0)
                    {
                        l3s = ParseStr(locXML, "level3", l3i);
                        l3i = ParseLoc(locXML, "level3", l3i);

                        locstr = locstr + ParseStr(l3s, "level3ID", 0) + "?" + ParseStr(l3s, "level3Name", 0);
                        curl3id = ParseStr(l3s, "level3ID", 0);

                        if (selected3IDs.IndexOf("#" + curl3id + "#") >= 0) //Location Issues
                        {
                            selectedIDs = selectedIDs + curl3id + ";";
                        }
                        l2i = 0;
                        if (l3s.IndexOf("<level2>", l2i) >= 0)  //Location Issues
                        {
                            locstr = locstr + "^";
                        }

                        while (l3s.IndexOf("<level2>", l2i) >= 0)   //Location Issues
                        {
                            l2s = ParseStr(l3s, "level2", l2i);
                            l2i = ParseLoc(l3s, "level2", l2i);

                            locstr = locstr + ParseStr(l2s, "level2ID", 0) + "?" + ParseStr(l2s, "level2Name", 0);
                            curl2id = ParseStr(l2s, "level2ID", 0);
                            if (selected2IDs.IndexOf("#" + curl2id + "#") >= 0)
                            {
                                selectedIDs = selectedIDs + curl3id + "," + curl2id + ";";
                            }
                            l1i = 0;
                            if (l2s.IndexOf("<level1>", l1i) >= 0)
                            {
                                locstr = locstr + "+";
                            }

                            while (l2s.IndexOf("<level1>", l1i) >= 0)
                            {
                                l1s = ParseStr(l2s, "level1", l1i);

                                //Added for Location Issues -- start
                                if (l1s.IndexOf("deleted") < 0)
                                {
                                    isOnlyDelRooms = false;
                                }

                                if (ParseStr(l1s, "deleted", 0) == "0")
                                {
                                    isOnlyDelRooms = false;
                                }

                                //Added for Location Issues -- End

                                l1i = ParseLoc(l2s, "level1", l1i);

                                curl1id = ParseStr(l1s, "level1ID", 0);

                                if (!(onlySelected && (selected1IDs.IndexOf("#" + curl1id + "#") == 0)))   //need to check
                                {
                                    if (queryStringFVal.IndexOf("alendar") < 0) //code changed
                                    {
                                        locstr = locstr + curl1id + "?" + ParseStr(l1s, "level1Name", 0);
                                        if (selected1IDs.IndexOf("#" + curl1id + "#") < 0)
                                        {
                                            locstr = locstr + "?0?";
                                        }
                                        else
                                        {
                                            locstr = locstr + "?1?";
                                            selectedIDs = selectedIDs + curl3id + "," + curl2id + "," + curl1id + ";";
                                        }

                                        if (l1s.IndexOf("<capacity>", 0) >= 0) //Location Issues
                                        {
                                            if (l1s.IndexOf("<deleted>", 0) < 0)
                                            {
                                                locstr = locstr + ParseStr(l1s, "capacity", 0) + "`" + ParseStr(l1s, "projector", 0) + "`" + ParseStr(l1s, "maxNumConcurrent", 0) + "`" + ParseStr(l1s, "videoAvailable", 0) + "`0`0";
                                            }
                                            else
                                            {
                                                locstr = locstr + ParseStr(l1s, "capacity", 0) + "`" + ParseStr(l1s, "projector", 0) + "`" + ParseStr(l1s, "maxNumConcurrent", 0) + "`" + ParseStr(l1s, "videoAvailable", 0) + "`" + ParseStr(l1s, "deleted", 0) + "`" + ParseStr(l1s, "locked", 0);
                                            }
                                            locstr = locstr + "=";
                                        }
                                    }
                                    else
                                    {
                                        if (ParseStr(l1s, "deleted", 0) == "0") //need to check
                                        {
                                            locstr = locstr + curl1id + "?" + ParseStr(l1s, "level1Name", 0);

                                            if (selected1IDs.IndexOf("#" + curl1id + "#") < 0)
                                            {
                                                locstr = locstr + "?0?";
                                            }
                                            else
                                            {
                                                locstr = locstr + "?1?";
                                                selectedIDs = selectedIDs + curl3id + "," + curl2id + "," + curl1id + ";";
                                            }

                                            if (l1s.IndexOf("<capacity>", 0) >= 0) //code changed
                                            {
                                                if (l1s.IndexOf("<deleted>", 0) < 0)
                                                {
                                                    locstr = locstr + ParseStr(l1s, "capacity", 0) + "`" + ParseStr(l1s, "projector", 0) + "`" + ParseStr(l1s, "maxNumConcurrent", 0) + "`" + ParseStr(l1s, "videoAvailable", 0) + "`0`0";
                                                }
                                                else
                                                {
                                                    locstr = locstr + ParseStr(l1s, "capacity", 0) + "`" + ParseStr(l1s, "projector", 0) + "`" + ParseStr(l1s, "maxNumConcurrent", 0) + "`" + ParseStr(l1s, "videoAvailable", 0) + "`" + ParseStr(l1s, "deleted", 0) + "`" + ParseStr(l1s, "locked", 0);
                                                }
                                                locstr = locstr + "=";
                                            }
                                        }
                                    }
                                }

                            }
                            locstr = DelStringRev(locstr, "=");
                            locstr = locstr + "%";

                        }
                        locstr = DelStringRev(locstr, "%");
                        locstr = locstr + "|";
                    }
                    locstr = DelStringRev(locstr, "|");
                }

                if (locstr != "")
                {
                    while ((locstr.IndexOf("undefined", 0) >= 0))
                    {
                        locstr = locstr.Replace("undefined", "");
                    }
                }
                //Added for Location Issues -- Start
                if (queryStringFVal.IndexOf("Manageroom") < 0)
                {
                    if (isOnlyDelRooms)
                        locstr = "";
                }
                //Added for Location Issues -- End

                resultStr = selectedIDs + "!" + locstr;
                //Added for FB 1440 Issues -- Start

                if (queryStringFVal.IndexOf("alendar") >= 0)
                {
                    String locStr2 = "";
                    //String[] strLst = null;
                    //String[] strLst2 = null;
                    locStr2 = resultStr;
                    if (resultStr.Trim().Length > 0)
                    {
                        String[] strLst = resultStr.ToString().Split('|');

                        for (int i = 0; i < strLst.Length; i++)
                        {
                            if (i == 0)
                                locStr2 = "";

                            String[] strLst2 = strLst[i].Split('+');
                            if (strLst2.Length > 0)
                            {
                                if (strLst2[1].Trim().Length <= 0)
                                {
                                    strLst[i] = "";

                                }
                            }

                            if (strLst[i].Trim() != "")
                            {
                                if (locStr2 == "")
                                    locStr2 = strLst[i];
                                else if (strLst[i].Length > 0)
                                    locStr2 = locStr2 + "|" + strLst[i];

                            }

                        }
                    }
                    resultStr = locStr2.Trim();
                    if (resultStr.IndexOf("!") < 0)
                        resultStr = "!" + resultStr;

                }

                //Added for FB 1440 Issues -- End
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return resultStr;
        }
        #endregion

        #region GetAllRoomIDName - Retrieves all rooms ID and Name

        public String GetAllRoomIDName(String inXML)
        {
            string roomIDname = "";
            int roomNo = 0;
            string locXML = "";
            string l3s = "";
            string l2s = "";
            string l1s = "";
            string curl3id = "";
            bool isOnlyDelRooms = true;//Added for Location Issues
            Int32 l3i;
            int l2i;
            int l1i;

            /* For GetFoodOrderByOrder  Begin */

            locXML = ParseStr(inXML, "level3List", 0);
            l3i = 0;

            //Added For Calander Issue Start
            if (locXML != null)
            {
                if (locXML != "")
                {
                    //Added For Calander Issue Start
                    while ((locXML.IndexOf("<level3>", l3i) >= 0))
                    {
                        l3s = ParseStr(locXML, "level3", l3i);

                        if (l3s == "")
                            break;

                        l3i = ParseLoc(locXML, "level3", l3i);

                        curl3id = ParseStr(l3s, "level3ID", 0);
                        l2i = 0;

                        while ((l3s.IndexOf("<level2>", l2i) >= 0))
                        {
                            l2s = ParseStr(l3s, "level2", l2i);

                            if (l2s == "")
                                break;

                            l2i = ParseLoc(l3s, "level2", l2i);
                            l1i = 0;

                            while ((l2s.IndexOf("<level1>", l1i) >= 0))
                            {
                                l1s = ParseStr(l2s, "level1", l1i);

                                if (l1s == "")
                                    break;

                                //Added For Location Issues Start
                                if (ParseStr(l1s, "deleted", 0) == "0")
                                    isOnlyDelRooms = false;
                                //Added For Location Issues End

                                l1i = ParseLoc(l2s, "level1", l1i);
                                roomNo = roomNo + 1;
                                roomIDname = roomIDname + roomNo.ToString() + "``" + ParseStr(l1s, "level1ID", 0) + "``" + clrSDQ_JS(ParseStr(l1s, "level1Name", 0)) + "^^";
                            }
                        }
                    }
                    //Added For Calander Issue Start
                }
            }
            //Added for Location Issues -- Start
            if (isOnlyDelRooms)
                roomIDname = "";
            //Added for Location Issues -- End

            //Added For Calander Issue End
            return roomIDname;
            /* For GetFoodOrderByOrder  End */
        }
        #endregion

        #region clrSDQ_JS - Replace

        public String clrSDQ_JS(String orgString)
        {
            string tempStr = orgString;
            if (orgString != "")
            {
                tempStr = orgString.Replace("'", "&#39;");
                tempStr = tempStr.Replace("\"", "&quot;");
            }
            return tempStr;
        }
        #endregion

        #region Method to retrieve Conference Room Details
        /// <summary>
        /// Method to retrieve Conference Room Details
        /// Command Name: ManageConfRoom
        /// </summary>
        /// <param name="queryStrFVal"></param>
        /// <param name="queryStrHFVal"></param>
        /// <param name="queryStrFrmVal"></param>
        /// <param name="queryStrMVal"></param>
        public void GetManageConfRoomData(String queryStrFVal, String queryStrHFVal, String queryStrFrmVal, String queryStrMVal)
        {
			//FB 2450
            //msxml4_Net.DOMDocument40Class XmlDoc = null;
            //msxml4_Net.IXMLDOMNodeList nodes = null;
            XmlDocument XmlDoc = new XmlDocument();
            XmlNodeList nodes = null;

            string userID = "";
            string inXML = "";
            string outXML = "";
            string startHour = "";
            string startMin = "";
            string startSet = "";
            string endHour = "";
            string endMin = "";
            string endSet = "";
            string uptz = "";
            try
            {
                if (HttpContext.Current != null)
                    userID = HttpContext.Current.Session["userID"].ToString();

                if (userID != "")
                {
                    OrgXMLElement();
					//FB 2450
                    XmlDoc = new XmlDocument(); // new msxml4_Net.DOMDocument40Class();

                    //XmlDoc.LoadXml("<login/>");
                    //msxml4_Net.IXMLDOMElement root = XmlDoc.DocumentElement;
                    //AppendTextNode(XmlDoc, root, "organizationID", organizationID);//Organization Module Fixes
                    //AppendTextNode(XmlDoc, root, "userID", userID);
                    //msxml4_Net.IXMLDOMElement user = XmlDoc.createElement("user");
                    //root.appendChild(user);
                    //AppendTextNode(XmlDoc, user, "userID", userID);
                    //inXML = XmlDoc.xml;

                    string usrIDnode = "<userID>" + userID + "</userID>";
                    inXML = "<login>"
                          + OrgXMLElement()
                          + usrIDnode
                          + "<user>" + usrIDnode + "</user>"
                          + "</login>";
                    //FB 2027 Start
                    outXML = CallMyVRMServer("GetOldUser", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                    //outXML = CallCOM("GetOldUser", inXML, HttpContext.Current.Application["COM_ConfigPath"].ToString());
                    //FB 2027 End
					//FB 2450 - Start
                    XmlDoc.LoadXml(outXML);

                    if (XmlDoc.SelectSingleNode("timeZone") != null)
                        uptz = XmlDoc.SelectSingleNode("timeZone").InnerText;

                    nodes = XmlDoc.SelectNodes("timezones/timezone");

                    if (nodes != null)
                    {
                        //foreach (msxml4_Net.IXMLDOMNode node in nodes)
                        for (int i = 0; i < nodes.Count; i++)
                        {
                            if (nodes[i].SelectSingleNode("timezoneID") != null)
                            {
                                if (nodes[i].SelectSingleNode("timezoneID").InnerText == uptz)
                                {
                                    HttpContext.Current.Session["uptz"] = nodes[i].SelectSingleNode("timezoneName").InnerText;
                                    break;
                                }
                            }
                        }
                    }
                    //OrgXMLElement();
                    //XmlDoc.loadXML("<login/>");
                    //root = XmlDoc.documentElement;
                    //AppendTextNode(XmlDoc, root, "organizationID", organizationID);//Organization Module Fixes
                    //AppendTextNode(XmlDoc, root, "userID", userID);
                    //root = null;
                    //inXML = XmlDoc.xml;
                    
                    inXML = "<login>" + OrgXMLElement();
                    inXML += "<userID>" + userID + "</userID></login>";                         

                    //outXML = CallCOM("ManageConfRoom", inXML, HttpContext.Current.Application["COM_ConfigPath"].ToString());
                    outXML = CallMyVRMServer("ManageConfRoom", inXML, HttpContext.Current.Application["MyVRMserver_ConfigPath"].ToString());//FB 2027
                    XmlDoc.LoadXml(outXML);

                    //msxml4_Net.IXMLDOMElement myXMLFuncNode = null;
                    if (outXML.IndexOf("<error>") == 0)
                    {
                        //XmlDoc.loadXML(outXML);
                        if (XmlDoc.SelectNodes("//error/locationList/selected") != null)
                        {
                            if (XmlDoc.SelectNodes("//error/locationList/selected").Count > 0)
                            {
                                if (XmlDoc.SelectSingleNode("//error/locationList/selected") != null)
                                    XmlDoc.SelectSingleNode("//error/locationList/selected").InnerText = "";

                                //myXMLFuncNode = XmlDoc.createElement("selected");
                                //myXMLFuncNode.text = "";

                                ////following block replaces the existing old node with new node
                                //root = XmlDoc.documentElement;
                                //msxml4_Net.IXMLDOMNode myXMLFuncRefNode = root.selectSingleNode("/locationList/selected");
                                //msxml4_Net.IXMLDOMNode myXMLFuncPrtNode = myXMLFuncRefNode.parentNode;
                                //myXMLFuncPrtNode.replaceChild(myXMLFuncNode, myXMLFuncRefNode);

                                //outXML = XmlDoc.xml;
                                //myXMLFuncNode = null;
                            }
                        }
                    }

                    if (queryStrFVal == "v")    //For Calendar pages alone
                    {
                        if (XmlDoc.SelectSingleNode("systemAvail/startTime/startHour") != null)
                            startHour = XmlDoc.SelectSingleNode("systemAvail/startTime/startHour").InnerText;

                        if (XmlDoc.SelectSingleNode("systemAvail/startTime/startMin") != null)
                            startMin = XmlDoc.SelectSingleNode("systemAvail/startTime/startMin").InnerText;

                        if (XmlDoc.SelectSingleNode("systemAvail/startTime/startSet") != null)
                            startSet = XmlDoc.SelectSingleNode("systemAvail/startTime/startSet").InnerText;

                        if (XmlDoc.SelectSingleNode("systemAvail/endTime/endHour") != null)
                            endHour = XmlDoc.SelectSingleNode("systemAvail/endTime/endHour").InnerText;

                        if (XmlDoc.SelectSingleNode("systemAvail/endTime/endMin") != null)
                            endMin = XmlDoc.SelectSingleNode("systemAvail/endTime/endMin").InnerText;


                        if (XmlDoc.SelectSingleNode("systemAvail/endTime/endSet") != null)
                            endSet = XmlDoc.SelectSingleNode("systemAvail/endTime/endSet").InnerText;
						//FB 2450 - End
                        HttpContext.Current.Session.Remove("startTime");
                        HttpContext.Current.Session.Remove("endTime");
                        HttpContext.Current.Session.Remove("tmpCalXML");

                        string stTime = "";
                        string edTime = "";

                        stTime = startHour + ":" + startMin + " " + startSet;
                        edTime = endHour + ":" + endMin + " " + endSet;

                        HttpContext.Current.Session.Add("startTime", stTime);
                        HttpContext.Current.Session.Add("endTime", edTime);

                        if (HttpContext.Current.Session["outXML"] != null)
                            HttpContext.Current.Session.Add("tmpCalXML", HttpContext.Current.Session["outXML"].ToString());

                        HttpContext.Current.Session.Remove("outXML");
                        HttpContext.Current.Session.Add("outXML", outXML);

                        if (HttpContext.Current.Session["errMsg"] != null)
                            HttpContext.Current.Session["errMsg"] = "";

                        if (HttpContext.Current.Session["errLevel"] != null)
                            HttpContext.Current.Session["errLevel"] = "";

                        if (queryStrHFVal == "1")
                        {
                            HttpContext.Current.Session.Remove("calXML");
                            HttpContext.Current.Session.Add("calXML", HttpContext.Current.Session["outXML"].ToString());

                            if (HttpContext.Current.Session["tmpCalXML"] != null)
                                HttpContext.Current.Session["outXML"] = HttpContext.Current.Session["tmpCalXML"].ToString();
                        }
                    }
                    else
                    {
                        if (queryStrMVal == "1")
                        {
							//FB 2450
                            //msxml4_Net.IXMLDOMElement errnode = XmlDoc.createElement("successnode");
                            //XmlDoc.documentElement.appendChild(errnode);

                            //AppendTextNode(XmlDoc, errnode, "success", "1");

                            //outXML = XmlDoc.xml;

                            outXML = XmlDoc.InnerXml;
                            outXML += "<successnode><success>1</success></successnode>";
                        }

                        if (queryStrFrmVal == "report")   //managereportroom.asp, managereporttier2.asp, managereporttier1.asp
                        {
                            if (HttpContext.Current.Session["outXML"] != null)
                                HttpContext.Current.Session.Add("tmpCalXML", HttpContext.Current.Session["outXML"].ToString());

                            HttpContext.Current.Session.Remove("outXML");
                            HttpContext.Current.Session.Add("outXML", outXML);

                            HttpContext.Current.Session.Remove("calXML");
                            HttpContext.Current.Session.Add("calXML", HttpContext.Current.Session["outXML"].ToString());

                            if (HttpContext.Current.Session["tmpCalXML"] != null)
                                HttpContext.Current.Session["outXML"] = HttpContext.Current.Session["tmpCalXML"].ToString();

                        }
                        else //For ManageRoom.aspx
                        {
                            HttpContext.Current.Session.Remove("outXML");
                            HttpContext.Current.Session.Add("outXML", outXML);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Appends the leaf node to the Xml document
        /// <summary>
        /// Appends the leaf node to the Xml document
        /// </summary>
        /// <param name="xDoc"></param>
        /// <param name="parentNode"></param>
        /// <param name="nodeName"></param>
        /// <param name="nodeValue"></param>
        public void AppendTextNode(msxml4_Net.DOMDocument40Class xDoc, msxml4_Net.IXMLDOMElement parentNode, string nodeName, string nodeValue)
        {

            msxml4_Net.IXMLDOMElement myXMLFuncNode = xDoc.createElement(nodeName);
            myXMLFuncNode.text = nodeValue;
            parentNode.appendChild(myXMLFuncNode);
        }
        #endregion

        #region Method to get the original location sources

        public String GetOrgLocSrcs(String inXML)
        {
            msxml4_Net.DOMDocument40Class XmlDoc = null;
            msxml4_Net.IXMLDOMNodeList nodes = null;
            msxml4_Net.IXMLDOMNodeList nodes2 = null;
            string OrgLocSrcs = "";

            try
            {
                XmlDoc = new msxml4_Net.DOMDocument40Class();
                XmlDoc.async = false;
                XmlDoc.loadXML("<locationList>" + ParseStr(inXML, "locationList", 1) + "</locationList>");

                if (XmlDoc.parseError.errorCode > 0)
                {
                    HttpContext.Current.Response.Write("<br><br><p align='center'><font size=4><b>Outcoming XML document is illegal<b></font></p>");
                    HttpContext.Current.Response.Write("<br><br><p align='left'><font size=2><b>" + inXML + "<b></font></p>");
                    XmlDoc = null;
                    HttpContext.Current.Response.End();
                }

                nodes = XmlDoc.documentElement.selectNodes("selected/level1ID");

                if (nodes == null)
                    return "";

                if (nodes.length <= 0)
                    return "";

                nodes2 = XmlDoc.documentElement.selectNodes("level3List/level3/level2List/level2/level1List/level1");
                if (nodes2 != null)
                {
                    foreach (msxml4_Net.IXMLDOMNode node2 in nodes2)
                    {
                        foreach (msxml4_Net.IXMLDOMNode node in nodes)
                        {
                            if (node2.selectSingleNode("level1ID").text == node.text)
                            {
                                msxml4_Net.IXMLDOMNodeList delNodes = XmlDoc.documentElement.selectNodes("level3List/level3/level2List/level2/level1List/level1/deleted");

                                //if (delNodes == null)
                                if (delNodes.length == 0)
                                {
                                    OrgLocSrcs += node2.selectSingleNode("capacity").text + "`" + node2.selectSingleNode("projector").text + "`";
                                    OrgLocSrcs += node2.selectSingleNode("maxNumConcurrent").text + "`" + node2.selectSingleNode("videoAvailable").text + "`0`0`";
                                    OrgLocSrcs += node2.selectSingleNode("level1Name").text + "``";
                                }
                                else
                                {
                                    OrgLocSrcs += node2.selectSingleNode("capacity").text + "`" + node2.selectSingleNode("projector").text + "`";
                                    OrgLocSrcs += node2.selectSingleNode("maxNumConcurrent").text + "`" + node2.selectSingleNode("videoAvailable").text + "`";
                                    OrgLocSrcs += node2.selectSingleNode("deleted").text + "`" + node2.selectSingleNode("locked").text + "`";
                                    OrgLocSrcs += node2.selectSingleNode("level1Name").text + "``";
                                }
                            }
                        }
                    }
                }

                if (OrgLocSrcs != "")
                    OrgLocSrcs = "``" + OrgLocSrcs;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                nodes2 = null;
                nodes = null;
            }
            return OrgLocSrcs;
        }
        #endregion

        /* Code added by offshore - FB Issue No 412- End */

        //Code added by Offshore for DateFormat String FB Issue 1073 -- Start

        #region Method to change the DD/MM/YYYY format to MM/DD/YYYY format
        /// <summary>
        /// Method to convert the date in the DDMMYYYY format to MMDDYYYY
        /// Convertion is done manually
        /// </summary>
        /// <param name="orginalDate"></param>
        /// <returns></returns>
        public static String GetDefaultDate(String originalDate)
        {
            String formattedDate = "";
            try
            {
                if (originalDate == "")
                    throw new ArgumentNullException("originalDate", "The date column is empty or null");

                if (HttpContext.Current.Session["FormatDateType"] == null)
                    throw new Exception("Session expired");

                if (HttpContext.Current.Session["FormatDateType"].ToString() == "MM/dd/yyyy")
                    return originalDate;

                if (originalDate.IndexOf("/") > 0)
                {
                    String[] dateArr = originalDate.Split('/');
                    if (dateArr.Length == 3)
                    {
                        Int32 monthNo = Convert.ToInt32(dateArr[1]);
                        Int32 dateNo = Convert.ToInt32(dateArr[0]);
                        bool isLeapYear = false;

                        if (monthNo < 0 || monthNo > 12)
                            return "";

                        if (dateNo < 0 || dateNo > 31)
                            return "";

                        string[] yrArr = dateArr[2].Split(' ');

                        if (yrArr[0].Trim().Length != 4)
                            return "";

                        Int32 yearNo = Convert.ToInt32(yrArr[0]);

                        if (yearNo < 1900)
                            return "";

                        if (yearNo % 4 == 0)
                            isLeapYear = true;

                        if (monthNo == 2 || monthNo == 4 || monthNo == 6 || monthNo == 9 || monthNo == 11)
                        {
                            if (dateNo > 30)
                                return "";

                            if (monthNo == 2)
                            {
                                if (isLeapYear)
                                {
                                    if (dateNo > 29)
                                        return "";
                                }
                                else
                                {
                                    if (dateNo > 28)
                                        return "";
                                }
                            }
                        }

                        if (dateArr[1].Length < 2)
                            dateArr[1] = "0" + dateArr[1];

                        if (dateArr[0].Length < 2)
                            dateArr[0] = "0" + dateArr[0];

                        formattedDate = dateArr[1] + '/' + dateArr[0] + '/' + dateArr[2];
                    }
                }
                else if (originalDate.IndexOf("-") > 0)
                {
                    String[] dateArr = originalDate.Split('-');
                    if (dateArr.Length == 3)
                    {
                        Int32 monthNo = Convert.ToInt32(dateArr[1]);
                        Int32 dateNo = Convert.ToInt32(dateArr[0]);
                        bool isLeapYear = false;

                        if (monthNo < 0 || monthNo > 12)
                            return "";

                        if (dateNo < 0 || dateNo > 31)
                            return "";

                        string[] yrArr = dateArr[2].Split(' ');

                        if (yrArr[0].Trim().Length != 4)
                            return "";

                        Int32 yearNo = Convert.ToInt32(yrArr[0]);

                        if (yearNo < 1900)
                            return "";

                        if (yearNo % 4 == 0)
                            isLeapYear = true;

                        if (monthNo == 2 || monthNo == 4 || monthNo == 6 || monthNo == 9 || monthNo == 11)
                        {
                            if (dateNo > 30)
                                return "";

                            if (monthNo == 2)
                            {
                                if (isLeapYear)
                                {
                                    if (dateNo > 29)
                                        return "";
                                }
                                else
                                {
                                    if (dateNo > 28)
                                        return "";
                                }
                            }
                        }
                        formattedDate = monthNo.ToString() + '-' + dateNo.ToString() + '-' + yearNo.ToString();
                    }
                }
                return formattedDate;
            }
            catch (Exception ex)
            {
                ns_Logger.Logger errLog = new ns_Logger.Logger();
                errLog.Trace("GetDefaultDate: " + ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }
        #endregion

        #region Method to change the MM/DD/YYYY format to DD/MM/YYYY format

        /// <summary>
        /// Method to convert the date in the MMDDYYYY format to DDMMYYYY
        /// Convertion is done manually
        /// </summary>
        /// <param name="orginalDate"></param>
        /// <returns></returns>
        public static String GetFormattedDate(String originalDate)
        {
            String formattedDate = "";
            try
            {
                if (originalDate == "")
                    throw new ArgumentNullException("originalDate", "The date column is empty or null");

                if (HttpContext.Current.Session["FormatDateType"] == null)
                    throw new Exception("Session expired");

                //ZD 100995
                if (HttpContext.Current.Session["FormatDateType"].ToString() == "MM/dd/yyyy" && HttpContext.Current.Session["EmailDateFormat"].ToString() == "0")
                    return Convert.ToDateTime(originalDate).ToString("MM/dd/yyyy");

                if (!IsCorrectdate(originalDate))
                {
                    //ZD 100995
                    if (HttpContext.Current.Session["FormatDateType"].ToString() == "dd/MM/yyyy" && HttpContext.Current.Session["EmailDateFormat"].ToString() == "1")
                        originalDate = GetDefaultDate(originalDate);
                    else
                        return originalDate;
                }

                //ZD 100995
                if (HttpContext.Current.Session["EmailDateFormat"].ToString() == "1")
                {
                    DateTime dateStr;
                    dateStr = Convert.ToDateTime(originalDate);
                    return dateStr.ToString("dd MMM yyyy");
                }
                else if (originalDate.IndexOf("/") > 0)
                {
                    String[] dateArr = originalDate.Split('/');
                    if (dateArr.Length == 3)
                    {
                        if (Convert.ToInt32(dateArr[0]) < 0 && Convert.ToInt32(dateArr[0]) > 12)
                            return "";

                        if (Convert.ToInt32(dateArr[1]) < 0 && Convert.ToInt32(dateArr[1]) > 31)
                            return "";

                        string[] yrArr = dateArr[2].Split(' ');

                        if (yrArr[0].Length != 4)
                            return "";

                        if (dateArr[1].Length < 2)
                            dateArr[1] = "0" + dateArr[1];

                        if (dateArr[0].Length < 2)
                            dateArr[0] = "0" + dateArr[0];

                        formattedDate = dateArr[1] + '/' + dateArr[0] + '/' + yrArr[0];
                    }
                }
                else if (originalDate.IndexOf("-") > 0)
                {
                    String[] dateArr = originalDate.Split('-');
                    if (dateArr.Length == 3)
                    {
                        if (Convert.ToInt32(dateArr[0]) < 0 && Convert.ToInt32(dateArr[0]) > 12)
                            return "";

                        if (Convert.ToInt32(dateArr[1]) < 0 && Convert.ToInt32(dateArr[1]) > 31)
                            return "";

                        string[] yrArr = dateArr[2].Split(' ');

                        if (yrArr[0].Length != 4)
                            return "";

                        formattedDate = dateArr[1] + '-' + dateArr[0] + '-' + yrArr[0];
                    }
                }
                return formattedDate;
            }
            catch (Exception ex)
            {
                ns_Logger.Logger errLog = new ns_Logger.Logger();
                errLog.Trace("GetFormattedDate: " + ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }

        private static Boolean IsCorrectdate(String input)
        {

            DateTime dt = DateTime.MinValue;

            try
            {
                dt = Convert.ToDateTime(input);
                return true;

            }
            catch
            {
                return false;

            }
        }

        /// <summary>
        /// Method to convert the date in the MMDDYYYY format to DDMMYYYY
        /// Convertion is done manually
        /// </summary>
        /// <param name="orginalDate"></param>
        /// <returns></returns>
        public static String GetFormattedDate(String originalDate, String formatDateType, String emailFormat)
        {
            String formattedDate = "";
            try
            {
                if (originalDate == "")
                    throw new ArgumentNullException("originalDate", "The date column is empty or null");

                if (formatDateType == "")
                    throw new Exception("Session expired");

                //ZD 100995
                if (formatDateType == "MM/dd/yyyy" && emailFormat == "0")
                    return Convert.ToDateTime(originalDate).ToString("MM/dd/yyyy");

                if (!IsCorrectdate(originalDate))
                {
                    //ZD 100995
                    if (formatDateType == "dd/MM/yyyy" && emailFormat == "1")
                        originalDate = GetDefaultDate(originalDate);
                    else
                        return originalDate;
                }
                //ZD 100995
                if (emailFormat == "1")
                {
                    DateTime dateStr;
                    dateStr = Convert.ToDateTime(originalDate);
                    return dateStr.ToString("dd MMM yyyy");
                }
                else if (originalDate.IndexOf("/") > 0)
                {
                    String[] dateArr = originalDate.Split('/');
                    if (dateArr.Length == 3)
                    {
                        if (Convert.ToInt32(dateArr[0]) < 0 && Convert.ToInt32(dateArr[0]) > 12)
                            return "";

                        if (Convert.ToInt32(dateArr[1]) < 0 && Convert.ToInt32(dateArr[1]) > 31)
                            return "";

                        if (dateArr[1].Length < 2)
                            dateArr[1] = "0" + dateArr[1];

                        if (dateArr[0].Length < 2)
                            dateArr[0] = "0" + dateArr[0];

                        string[] yrArr = dateArr[2].Split(' ');

                        if (yrArr[0].Length != 4)
                            return "";

                        formattedDate = dateArr[1] + '/' + dateArr[0] + '/' + yrArr[0];
                    }
                }
                else if (originalDate.IndexOf("-") > 0)
                {
                    String[] dateArr = originalDate.Split('-');
                    if (dateArr.Length == 3)
                    {
                        if (Convert.ToInt32(dateArr[0]) < 0 && Convert.ToInt32(dateArr[0]) > 12)
                            return "";

                        if (Convert.ToInt32(dateArr[1]) < 0 && Convert.ToInt32(dateArr[1]) > 31)
                            return "";

                        string[] yrArr = dateArr[2].Split(' ');

                        if (yrArr[0].Length != 4)
                            return "";

                        formattedDate = dateArr[1] + '-' + dateArr[0] + '-' + yrArr[0];
                    }
                }
                return formattedDate;
            }
            catch (Exception ex)
            {
                ns_Logger.Logger errLog = new ns_Logger.Logger();
                errLog.Trace("GetFormattedDate: " + ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }

        public static String GetFormattedDate(DateTime inputDate)
        {
            try
            {
                if (inputDate == DateTime.MinValue)
                    throw new Exception("Invalid date");

                return GetFormattedDate(inputDate.ToString());
            }
            catch (Exception ex)
            {
                ns_Logger.Logger errLog = new ns_Logger.Logger();
                errLog.Trace("GetFormattedDate: " + ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }
        #endregion

        //Code added by Offshore for DateFormat String --1073 End

        //Code Added for Recurring Conference -- start
        // FB 2362
        #region Recurring Enums

        public enum RecurringPattern
        {
            Daily = 1,
            Weekly,
            Monthly,
            Yearly,
            Custom
        }

        public enum WeekDays
        {
            Sunday = 1,
            Monday,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday
        }

        public enum DayCount
        {
            first = 1,
            second,
            third,
            fourth,
            last
        }

        public enum DaysType
        {
            day = 1,
            weekday,
            weekend,
            Sunday,
            Monday,
            Tuesday,
            Wednesday,
            Thursday,
            Friday,
            Saturday
        }

        public enum MonthNames
        {
            January = 1,
            February,
            March,
            April,
            May,
            June,
            July,
            August,
            September,
            October,
            November,
            December
        }

        #endregion
        #region Avparams Enums
        public enum videocodecs
        {
            Auto=0,
            H261,
            H263,
            H264
        }
        public enum audiocodecs
        {
            Auto=0,
            g728siren716,
            g722siren724,
            g722siren732,
            g722g71156,
            g71156

        }
        
        
        #endregion
        // FB 2362
        //Code Added for Recurring Conference -- end

        //Code Added for FB 1426 
        #region GetFormattedTime
        /// <summary>
        /// GetFormattedTime
        /// </summary>
        /// <param name="originalTime"></param>
        /// <param name="formatTimeType"></param>
        /// <returns></returns>
        public static String GetFormattedTime(String originalTime, String formatTimeType)
        {
            try
            {
                if (originalTime == "")
                    throw new ArgumentNullException("originalTime", "The time column is empty or null");

                if (formatTimeType == "")
                    throw new Exception("Session expired");

                if (formatTimeType == "1")
                    return Convert.ToDateTime(originalTime).ToString("hh:mm tt");//To handle preceding Zero - Modified during Work Order fixes
                else if (formatTimeType == "2") //FB 2588
                    return Convert.ToDateTime(ChangeTimeFormat(originalTime)).ToString("HHmmZ");
                else
                    return Convert.ToDateTime(originalTime).ToString("HH:mm");
            }
            catch (Exception ex)
            {
                ns_Logger.Logger errLog = new ns_Logger.Logger();
                errLog.Trace("GetFormattedDate: " + ex.StackTrace + " : " + ex.Message);
                return "";

            }
        }

        #endregion

        public void BindTimeToListBox(MetaBuilders.WebControls.ComboBox cntrl, Boolean blnOH, Boolean blnInterval)
        {
            String val = "";
            Int32 sHours = 0;
            Int32 eHours = 24;
            Int32 sMins = 0;
            Int32 eMins = 0;
            Int32 intval = 60;
            String tFormat = "hh:mm tt";
            try
            {

                if (blnOH)
                {
                    if (HttpContext.Current.Session["SystemStartTime"] != null)//Organization Module Fixes
                        if (HttpContext.Current.Session["SystemStartTime"].ToString() != "")//Organization Module Fixes
                        {
                            sHours = DateTime.Parse(HttpContext.Current.Session["SystemStartTime"].ToString()).Hour;//Organization Module Fixes
                            sMins = DateTime.Parse(HttpContext.Current.Session["SystemStartTime"].ToString()).Minute;//Organization Module Fixes
                        }
                    if (HttpContext.Current.Session["SystemEndTime"] != null)//Organization Module Fixes
                        if (HttpContext.Current.Session["SystemEndTime"].ToString() != "")//Organization Module Fixes
                        {
                            eHours = DateTime.Parse(HttpContext.Current.Session["SystemEndTime"].ToString()).Hour;//Organization Module Fixes
                            eMins = DateTime.Parse(HttpContext.Current.Session["SystemEndTime"].ToString()).Minute;//Organization Module Fixes
                        }
                }
                if (HttpContext.Current.Session["timeFormat"] != null)
                    if (HttpContext.Current.Session["timeFormat"].ToString().Equals("0"))
                        tFormat = "HH:mm";
                    else if (HttpContext.Current.Session["timeFormat"].ToString().Equals("2")) //FB 2588
                        tFormat = "HHmmZ";
                if (blnInterval)
                    if (HttpContext.Current.Application["interval"] != null)
                        if (HttpContext.Current.Application["interval"].ToString() != "")
                            intval = Int32.Parse(HttpContext.Current.Application["interval"].ToString());

                Int32 k = 0;
                sHours = ((eHours < sHours) ? eHours : sHours);
                eHours = ((eHours == 0) ? 24 : eHours);
                eHours = (((eMins > 0) && (eHours != 24)) ? (eHours + 1) : eHours);
                for (k = sHours; k < eHours; k++)
                {
                    for (Int32 i = 0; i < 60; i = i + intval)
                    {
                        i = (((k == sHours) && (i == 0)) ? sMins : i);
                        if ((k == eHours) && (eMins == i))
                            break;
                        String j = "";
                        j = ((i.ToString().Length == 1) ? ("0" + i.ToString()) : i.ToString());
                        val = ((k.ToString().Length == 1) ? ("0" + k.ToString() + ":" + j.ToString()) : (k.ToString() + ":" + j.ToString()));
                        cntrl.Items.Add(Convert.ToDateTime(val).ToString(tFormat));
                    }
                }
                if ((k == eHours) && (eMins == 0) && (k < 24))
                {
                    val = eHours.ToString() + ":" + eMins.ToString();
                    cntrl.Items.Add(Convert.ToDateTime(val).ToString(tFormat));
                }
                if (k > 22) // ZD 100284
                {
                    val = "23:59";
                    cntrl.Items.Add(Convert.ToDateTime(val).ToString(tFormat));
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                throw ex;
            }

        }

        /* *** Code added for FB 1426 * ***/
        #region ShowErrorMessage In Conference Setup
        /// <summary>
        /// ShowErrorMessage
        /// </summary>
        /// <param name="xmlString"></param>
        /// <returns></returns>
        public string ShowErrorMessageConferenceSetup(string xmlString)
        {
            Int32 sHours = 0;
            Int32 eHours = 24;
            Int32 sMins = 0;
            Int32 eMins = 0;
            Boolean isTimezone = false;
            String[] msgs = null;
            try
            {
                if (HttpContext.Current.Session["SystemStartTime"] != null)//Organization Module Fixes
                {
                    if (HttpContext.Current.Session["SystemStartTime"].ToString() != "")//Organization Module Fixes
                    {
                        sHours = DateTime.Parse(HttpContext.Current.Session["SystemStartTime"].ToString()).Hour;//Organization Module Fixes
                        sMins = DateTime.Parse(HttpContext.Current.Session["SystemStartTime"].ToString()).Minute;//Organization Module Fixes


                    }
                }
                if (HttpContext.Current.Session["SystemEndTime"] != null)//Organization Module Fixes
                {
                    if (HttpContext.Current.Session["SystemEndTime"].ToString() != "")//Organization Module Fixes
                    {
                        eHours = DateTime.Parse(HttpContext.Current.Session["SystemEndTime"].ToString()).Hour;//Organization Module Fixes
                        eMins = DateTime.Parse(HttpContext.Current.Session["SystemEndTime"].ToString()).Minute;//Organization Module Fixes
                    }
                }

                if (HttpContext.Current.Application["timeZoneDisplay"] != null)
                {
                    if (HttpContext.Current.Application["timeZoneDisplay"].ToString() == "1")
                        isTimezone = true;

                }

                string errMSG = "";
                XmlDocument xmlDOC = new XmlDocument();
                xmlDOC.LoadXml(xmlString);
                errMSG = xmlDOC.SelectSingleNode("//error/message").InnerText;

                msgs = errMSG.Split(':');

                if (xmlDOC.SelectSingleNode("//error/errorCode") != null)
                {
                    if (xmlDOC.SelectSingleNode("//error/errorCode").InnerText == "405")//ZD 104542 
                    {
                        if (HttpContext.Current.Session["timeFormat"] == null)
                            throw new Exception("Session expired");

                        if (HttpContext.Current.Session["timeFormat"].ToString() == "0")
                        {
                            errMSG = GetTranslatedText("The myVRM system is unavailable during the date and time selected. Please check the myVRM system availability and try again. Hours of operation: ") + sHours + ":" + ((sMins < 10) ? "0" + sMins.ToString() : sMins.ToString()) + " to " + eHours + ":" + ((eMins < 10) ? "0" + eMins.ToString() : eMins.ToString());//FB 1830 - Translation //ZD 104542
                            if (isTimezone)
                                errMSG += "(" + HttpContext.Current.Application["systemTimezone"].ToString() + ")";

                            if (msgs != null)
                            {
                                for (int i = 0; i < msgs.Length; i++) //FB 1426 Edited
                                {
                                    if (msgs[i].Contains("Closed"))
                                    {
                                        if (msgs.Length > i + 1)
                                        {
                                            errMSG += "." + GetTranslatedText(" Closed: ") + msgs[i + 1];//ZD 104542
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                        else if (HttpContext.Current.Session["timeFormat"].ToString() == "2")
                        {

                            errMSG = GetTranslatedText("The myVRM system is unavailable during the date and time selected. Please check the myVRM system availability and try again. Hours of operation: ") + ((sHours < 10) ? "0" + sHours.ToString() : sHours.ToString()) + ((sMins < 10) ? "0" + sMins.ToString() : sMins.ToString()) + "Z to " + ((eHours < 10) ? "0" + eHours.ToString() : eHours.ToString()) + ((eMins < 10) ? "0" + eMins.ToString() : eMins.ToString()) + "Z"; //ZD 104542
                            if (isTimezone)
                                errMSG += "(" + HttpContext.Current.Application["systemTimezone"].ToString() + ")";

                            if (msgs != null)
                            {
                                for (int i = 0; i < msgs.Length; i++) //FB 1426 Edited
                                {
                                    if (msgs[i].Contains("Closed"))
                                    {
                                        if (msgs.Length > i + 1)
                                        {
                                            errMSG += ".Closed: " + msgs[i + 1];
                                            break;
                                        }
                                    }
                                }
                            }

                        }
                    }

                    errMSG += GetTranslatedText(" Error Code : ") + xmlDOC.SelectSingleNode("//error/errorCode").InnerText;//ZD 104542
                }
                return errMSG;
            }
            catch (Exception ex)
            {
                log.Trace(xmlString);
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return errXML;//FB 1881
                //return "Error 122: Please contact your VRM Administrator";
                 
            }
        }
        #endregion
        /* *** Code added for FB 1426 * ***/

        /* Code Added For FB 1453 - Start */
        #region LoadTimeZoneList
        /// <summary>
        /// LoadList
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="nodes"></param>
        /// <param name="col1"></param>
        /// <param name="col2"></param>
        public void LoadTimeZoneList(DropDownList lstList, XmlNodeList nodes, String col1, String col2)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    DataTable dtTZ;
                    dtTZ = ds.Tables[0]; //104410 start
                    //dtTZ.Columns.Add(new DataColumn("TimeZoneOrder", typeof(Int32)));

                    //foreach (DataRow drTZ in dtTZ.Rows)
                    //{
                    //    String varTZ = drTZ["timezonename"].ToString();
                    //    String var1 = varTZ.Split(' ')[0];
                    //    String var2 = var1.Substring(4);
                    //    String var3 = var2.Split(')', ':')[0];
                    //    if (var3 == "")
                    //        drTZ["TimeZoneOrder"] = "0";
                    //    else
                    //        drTZ["TimeZoneOrder"] = var3;
                    //}
                    dv = new DataView(dtTZ);
                    //dv.Sort = "TimeZoneOrder Asc";//ZD 104410 End
                    dt = dv.ToTable();

                    DataRow dr = dt.NewRow();
                    dr[0] = "-1";
                    dr[1] = GetTranslatedText("Please select...");//FB 1830 - Translation
                    if ((lstList.ID.IndexOf("lstBridge") >= 0) && (HttpContext.Current.Request.Url.ToString().IndexOf("ConferenceSetup") >= 0))
                        dr[1] = GetTranslatedText("Auto select...");//FB 1830 - Translation
                    dt.Rows.InsertAt(dr, 0);
                }
                else
                {
                    dv = new DataView();
                    dt = new DataTable();
                    dt.Columns.Add(col1);
                    dt.Columns.Add(col2);
                    DataRow dr = dt.NewRow();
                    dr[0] = "-1";
                    dr[1] = GetTranslatedText("No Items...");//FB 2272
                    dt.Rows.InsertAt(dr, 0);
                }
                lstList.DataSource = dt;
                lstList.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion
        /* Code Added For FB 1453 - End */

        #region LoadDataTable
        /// <summary>
        /// LoadDataTable
        /// </summary>
        /// <param name="dtValues"></param>
        /// <param name="nodes"></param>
        /// <param name="colNames"></param>
        public DataTable LoadDataTable(XmlNodeList nodes, ArrayList colNames)
        {
            DataTable dtValues = new DataTable();
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                if (nodes != null)
                {
                    foreach (XmlNode node in nodes)
                    {
                        xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                        ds.ReadXml(xtr, XmlReadMode.InferSchema);
                    }
                }
                DataView dv;

                if (ds.Tables.Count > 0)
                {
                    dtValues = ds.Tables[0];
                }
                else
                {
                    dtValues = new DataTable();
                    if (colNames != null)
                    {
                        for (int i = 0; i < colNames.Count; i++)
                        {
                            dtValues.Columns.Add(new DataColumn(colNames[i].ToString()));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
            return dtValues;
        }
        #endregion

        #region GetCountryStatesSearch
        /// <summary>
        /// GetCountryStates
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="strCountryCode"></param>
        public void GetCountryStatesSearch(DropDownList lstList, String strCountryCode)
        {
            try
            {
                String inXML = "<GetCountryStates><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID><CountryCode>" + strCountryCode + "</CountryCode></GetCountryStates>";
                String outXML = CallMyVRMServer("GetCountryStates", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetCountryStates/State");
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;
                lstList.Items.Clear();
                if (ds.Tables.Count > 0)
                {

                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    DataRow dr = dt.NewRow();
                    dr[0] = "-1";
                    dr[1] = GetTranslatedText("Any");//FB 1830 - Translation
                    dt.Rows.InsertAt(dr, 0);
                }
                else
                {
                    dv = new DataView();
                    dt = new DataTable();
                    dt.Columns.Add("ID");
                    dt.Columns.Add("Code");
                    DataRow dr = dt.NewRow();
                    dr[0] = "-1";
                    dr[1] = GetTranslatedText("No Items...");//FB 1830 - Translation
                    dt.Rows.InsertAt(dr, 0);
                    lstList.Enabled = false;
                }
                lstList.DataSource = dt;
                lstList.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace("GetCountryStates: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetRoomDetailsString
        /// <summary>
        /// GetCountryStates
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="strCountryCode"></param>
        public String RoomDetailsString(String rmIds)
        {
            String ret = "";
            try
            {
                if (rmIds != "")
                {
                    String inXML = "<Room><RoomIds>" + rmIds + "</RoomIds></Room>";
                    String outXML = CallMyVRMServer("GetRoomBasicDetails", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(outXML);
                    XmlNodeList nds = doc.SelectNodes("Rooms/Room");

                    foreach (XmlNode nd in nds)
                    {
                        XmlNode roomNm = nd.SelectSingleNode("RoomName");
                        XmlNode roomID = nd.SelectSingleNode("RoomID");

                        if (roomNm != null && roomID != null)
                        {
                            if (ret == "")
                                ret = roomID.InnerText + "|" + roomNm.InnerText;
                            else
                                ret += "++" + roomID.InnerText + "|" + roomNm.InnerText;//Room Search Fixes
                        }
                    }

                }


            }
            catch (Exception ex)
            {
                log.Trace("GetCountryStates: " + ex.StackTrace + " : " + ex.Message);
            }

            return ret;
        }
        #endregion

        /* Added for Org - Start */
        #region Get Organisation ID
        /// <summary>
        /// Get Organisation ID
        /// </summary>
        /// <returns></returns>
        public String OrgXMLElement()
        {
            int orgID = 0;
            int multisiloOrgID = 0;
            try
            {
                //FB 1830
                if (HttpContext.Current.Session["organizationID"] == null)
                    HttpContext.Current.Response.Redirect("~/en/genlogin.aspx"); //Login Management

                organizationID = HttpContext.Current.Session["organizationID"].ToString().Trim();

                if (organizationID == "")
                    HttpContext.Current.Response.Redirect("~/en/genlogin.aspx");//Login Management

                Int32.TryParse(organizationID, out orgID);

                if (orgID < defaultOrgID)
                    HttpContext.Current.Response.Redirect("~/en/genlogin.aspx");//Login Management

                orgXMLElement = "<organizationID>" + organizationID + "</organizationID>";//Organisation Module

                //FB 2274 Starts
                if (HttpContext.Current.Session["multisiloOrganizationID"] != null)
                    Int32.TryParse(HttpContext.Current.Session["multisiloOrganizationID"].ToString(), out multisiloOrgID);
                if (multisiloOrgID <= 11)
                    multisiloOrgID = 0;

                orgXMLElement += "<multisiloOrganizationID>" + multisiloOrgID + "</multisiloOrganizationID>";

                //HttpContext.Current.Session.Remove("multisiloOrganizationID");
                //FB 2274 Ends
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
            return orgXMLElement;
        }
        #endregion

        #region BindOrganizationNames
        /// <summary>
        /// BindOrganizationNames
        /// </summary>
        /// <param name="sender"></param>
        public void BindOrganizationNames(DropDownList sender)
        {
            try
            {
                String inXML = "<Organization><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></Organization>";
                String outXML;
                outXML = CallMyVRMServer("GetOrganizationList", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//GetOrganizationList/Organization");
                {
                    LoadOrgList(sender, nodes, "OrgId", "OrganizationName");
                }
            }
            catch (Exception ex)
            {
                log.Trace("GetAddressType: " + ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

        #region LoadOrgList
        /// <summary>
        /// LoadList
        /// </summary>
        /// <param name="drpOrg"></param>
        /// <param name="nodes"></param>
        /// <param name="col1"></param>
        /// <param name="col2"></param>
        public void LoadOrgList(DropDownList drpOrg, XmlNodeList nodes, String col1, String col2)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt = new DataTable();

                if (ds.Tables.Count > 0)
                {
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                }
                else
                {
                    dv = new DataView();
                    dt = new DataTable();
                    dt.Columns.Add(col1);
                    dt.Columns.Add(col2);
                    DataRow dr = dt.NewRow();
                    dr[0] = "-1";
                    dr[1] = GetTranslatedText("No Items...");
                    dt.Rows.InsertAt(dr, 0);
                }
                drpOrg.DataSource = dt;
                drpOrg.DataBind();

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion

        //FB 2486
        #region SetToolTip
        /// <summary>
        /// SetToolTip
        /// </summary>
        /// <param name="drpdwnList"></param>
        public void SetToolTip(DropDownList drpdwnList)
        {
            try
            {
                if (drpdwnList.Items.Count > 0)
                {
                    foreach (ListItem item in drpdwnList.Items)                        
                        item.Attributes.Add("title", item.Text);

                    drpdwnList.DataBind();
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region SetOrgSession
        /// <summary>
        /// LoadList
        /// </summary>
        public void SetOrgSession(Int32 orgId)
        {
            String xml = "";
            XmlDocument xmldoc = null;
            String orgBaseLang = "1", OrgEmailLang = ""; //FB 2283

            try
            {
                if (orgId < 11)
                    orgId = 11;

                HttpContext.Current.Session.Remove("organizationID");
                HttpContext.Current.Session.Remove("multisiloOrganizationID"); //FB 2274 Session Issue


                if (HttpContext.Current.Session["organizationID"] == null)
                    HttpContext.Current.Session.Add("organizationID", orgId);


                xml = CallMyVRMServer("GetAllOrgSettings", "<GetAllOrgSettings>" + OrgXMLElement() + "</GetAllOrgSettings>", HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                if (xml.IndexOf("<error>") <= 0)
                {
                    xmldoc = new XmlDocument();
                    xmldoc.LoadXml(xml);


                    HttpContext.Current.Session.Remove("DefaultConferenceType");
                    HttpContext.Current.Session.Remove("EnableRoomConfType");
                    HttpContext.Current.Session.Remove("EnableAudioVideoConfType");
                    HttpContext.Current.Session.Remove("EnableAudioOnlyConfType");
                    HttpContext.Current.Session.Remove("EnableNumericID");//FB 2870
                    HttpContext.Current.Session.Remove("EnableHotdeskingConference");//FB 100719
                    HttpContext.Current.Session.Remove("DefaultCalendarToOfficeHours");
                    HttpContext.Current.Session.Remove("DaysClosed"); //FB 2804
                    HttpContext.Current.Session.Remove("EnableEntity");
                    HttpContext.Current.Session.Remove("EnableBufferZone");
                    HttpContext.Current.Session.Remove("OrgSetupTime");//FB 2398
                    HttpContext.Current.Session.Remove("OrgTearDownTime");
                    HttpContext.Current.Session.Remove("dynInvite");
                    HttpContext.Current.Session.Remove("doubleBooking");
                    HttpContext.Current.Session.Remove("SystemEndTime");
                    HttpContext.Current.Session.Remove("SystemStartTime");
                    HttpContext.Current.Session.Remove("tzsystemid");
                    HttpContext.Current.Session.Remove("roomExpandLevel");
                    HttpContext.Current.Session.Remove("RoomListView");
                    HttpContext.Current.Session.Remove("RoomLimit");
                    HttpContext.Current.Session.Remove("McuLimit");
                    HttpContext.Current.Session.Remove("UserLimit");
                    HttpContext.Current.Session.Remove("organizationName");
                    HttpContext.Current.Session.Remove("recurEnable");
                    HttpContext.Current.Session.Remove("contactName");
                    HttpContext.Current.Session.Remove("contactEmail");
                    HttpContext.Current.Session.Remove("contactPhone");
                    HttpContext.Current.Session.Remove("contactAddInfo");
                    HttpContext.Current.Session.Remove("P2PEnable");
                    HttpContext.Current.Session.Remove("defaultPublic");
                    HttpContext.Current.Session.Remove("dialoutEnabled");
                    HttpContext.Current.Session.Remove("hkModule");
                    HttpContext.Current.Session.Remove("foodModule");
                    HttpContext.Current.Session.Remove("EnableAPIs");
                    HttpContext.Current.Session.Remove("VideoRooms");
                    HttpContext.Current.Session.Remove("NonVideoRooms");
                    HttpContext.Current.Session.Remove("EndPoints");
                    HttpContext.Current.Session.Remove("roomModule");
                    HttpContext.Current.Session.Remove("ExchangeUserLimit");
                    HttpContext.Current.Session.Remove("DominoUserLimit");
                    HttpContext.Current.Session.Remove("MobileUserLimit"); // FB 1979
                    HttpContext.Current.Session.Remove("WebexUserLimit"); //ZD 100221
                    //HttpContext.Current.Session.Remove("PCModule"); // FB 2347 //FB 2693
                    HttpContext.Current.Session.Remove("GuestRooms"); // FB 2426
                    HttpContext.Current.Session.Remove("MCUEnchancedLimit");//FB 2486
                    
                    //Audio Add On..
                    HttpContext.Current.Session.Remove("ConferenceCode");
                    HttpContext.Current.Session.Remove("LeaderPin");
                    HttpContext.Current.Session.Remove("AdvAvParams");
                    HttpContext.Current.Session.Remove("AudioParams");
                    //Audio Add On..
                    HttpContext.Current.Session.Remove("EnablePartyCode"); //ZD 101446
                    //FB 2359 start
                    HttpContext.Current.Session.Remove("EnableRoomParam");
                    //FB 2359 End
                    HttpContext.Current.Session["confid"] = null; //FB 1746

                    HttpContext.Current.Session.Remove("isVIP");// FB 1864
                    //HttpContext.Current.Session.Remove("Holidays");// FB 1861 //Commented for FB 2395
                    HttpContext.Current.Session.Remove("isAssignedMCU");// FB 1901
                    HttpContext.Current.Session.Remove("EnableRoomServiceType");//FB 2219
                    HttpContext.Current.Session.Remove("EnableRPRMRoomSync");//ZD 101527
                    HttpContext.Current.Session.Remove("isSpecialRecur");// FB 2052
                    HttpContext.Current.Session.Remove("OrgBaseLang");// FB 2283
                    HttpContext.Current.Session.Remove("OrgEmailLangID");// FB 2283
                    HttpContext.Current.Session.Remove("EmailDateFormat");// ZD 100958
                    HttpContext.Current.Session.Remove("EnableImmConf");//FB 2036
                    HttpContext.Current.Session.Remove("EnablePasswordRule");//FB 2339
                    HttpContext.Current.Session.Remove("EnableAudioBridges");//FB 2023
                    HttpContext.Current.Session.Remove("DedicatedVideo");//FB 2334
                    HttpContext.Current.Session.Remove("EnableConfPassword");//FB 2359
                    HttpContext.Current.Session.Remove("EnablePublicConf");//FB 2359
					HttpContext.Current.Session.Remove("WorkingHours");//FB 2343
                    HttpContext.Current.Session.Remove("EnableSurvey");//FB 2348
                    HttpContext.Current.Session.Remove("EnableEPDetails");//FB 2401
                    HttpContext.Current.Session.Remove("EnableAcceptDecline");//FB 2419
                    HttpContext.Current.Session.Remove("OrgLineRate"); //FB 2429
                    HttpContext.Current.Session.Remove("EnableFileWhiteList"); //ZD 100263
                    HttpContext.Current.Session.Remove("FileWhiteList"); //ZD 100263
                    HttpContext.Current.Session.Remove("OnflyTopTierID"); //FB 2426
                    HttpContext.Current.Session.Remove("OnflyMiddleTierID"); //FB 2426
                    HttpContext.Current.Session.Remove("OnflyTopTierName"); //FB 2426
                    HttpContext.Current.Session.Remove("OnflyMiddleTierName"); //FB 2426

                    HttpContext.Current.Session.Remove("VMRTopTierID"); //ZD 100068
                    HttpContext.Current.Session.Remove("VMRMiddleTierID"); //ZD 100068
                    
                    HttpContext.Current.Session.Remove("EnableConfTZinLoc"); //FB 2469
                    HttpContext.Current.Session.Remove("SendConfirmationEmail"); //FB 2469
                    HttpContext.Current.Session.Remove("MCUAlert"); //FB 2472
					HttpContext.Current.Session.Remove("EnableSmartP2P"); //FB 2430
					HttpContext.Current.Session.Remove("DefaultConfDuration");//FB 2501 
                    HttpContext.Current.Session.Remove("MaxPublicVMRParty"); //FB 2550 
                    //FB 2571 Start
                    HttpContext.Current.Session.Remove("EnableFECC");
                    HttpContext.Current.Session.Remove("DefaultFECC");
                    //FB 2571 End
                    HttpContext.Current.Session.Remove("VMRRooms");//FB 2586
                    //FB 2598 Starts
                    HttpContext.Current.Session.Remove("EnableCallmonitor");//Enable Call Monitor
                    HttpContext.Current.Session.Remove("EnableEM7");//Enable EM7
                    HttpContext.Current.Session.Remove("EnableCDR");//Enable CDR
                    HttpContext.Current.Session.Remove("ShowCusAttInCalendar");//ZD 100151
                    
                    //FB 2598 Ends
                    HttpContext.Current.Session.Remove("MeetandGreetBuffer"); //FB 2609
					HttpContext.Current.Session.Remove("Cloud"); //FB 2262 - J  //FB 2599
                    HttpContext.Current.Session.Remove("EnablePublicRooms");//2594
					HttpContext.Current.Session.Remove("AlertforTier1"); //FB 2637
                    //FB 2595
                    HttpContext.Current.Session.Remove("SecureSwitch");
                    HttpContext.Current.Session.Remove("EnableZulu");//FB 2588
					HttpContext.Current.Session.Remove("NetworkSwitching"); //FB 2993
                    //FB 2670 START
                    HttpContext.Current.Session.Remove("EnableOnsiteAV");
                    HttpContext.Current.Session.Remove("EnableMeetandGreet");
                    HttpContext.Current.Session.Remove("EnableConciergeMonitoring");
                    HttpContext.Current.Session.Remove("EnableDedicatedVNOC");
                    //FB 2670 END
					HttpContext.Current.Session.Remove("EnableLinerate");//FB 2641
                    HttpContext.Current.Session.Remove("EnableStartMode");//FB 2641
					//FB 2693 Starts
                    HttpContext.Current.Session.Remove("PCUserLimit");
                    HttpContext.Current.Session.Remove("EnableBlueJeans");
                    HttpContext.Current.Session.Remove("EnableJabber");
                    HttpContext.Current.Session.Remove("EnableLync");
                    HttpContext.Current.Session.Remove("EnableVidtel");
                    //FB 2693 Ends

					//FB 2694 Start
                    HttpContext.Current.Session.Remove("HotdeskingVideoRooms");
                    HttpContext.Current.Session.Remove("HotdeskingNonVideoRooms");
                    //FB 2694 End
                    HttpContext.Current.Session.Remove("ThemeType"); //FB 2779
                    HttpContext.Current.Session.Remove("EnableSingleRoomConfEmails"); //FB 2817

                    HttpContext.Current.Session.Remove("EnableAdvancedReport"); //FB 2593
                    HttpContext.Current.Session.Remove("EnableProfileSelection");//FB 2839
                    HttpContext.Current.Session.Remove("EnablePoolOrderSelection");//ZD 104256
                    //FB 2998
                    HttpContext.Current.Session.Remove("MCUTearDisplay");
                    HttpContext.Current.Session.Remove("MCUSetupDisplay");
                    HttpContext.Current.Session.Remove("McuSetupTime");
                    HttpContext.Current.Session.Remove("MCUTeardonwnTime");
                    //ZD 100164 START
                    HttpContext.Current.Session.Remove("EnableAdvancedUserOption");
                    //ZD 100164 END
                    HttpContext.Current.Session.Remove("IsOpen24Hours"); //ZD 100157
                    HttpContext.Current.Session.Remove("EnableExpressConfType"); //ZD 100704
                    HttpContext.Current.Session.Remove("EnableDetailedExpressForm"); // ZD 100834
					HttpContext.Current.Session.Remove("EnableWebExIntg"); //ZD 100935
                    HttpContext.Current.Session.Remove("DefaultSubject"); //ZD 100167
                    HttpContext.Current.Session.Remove("EnableRoomCalendarView"); //ZD 100963
                    HttpContext.Current.Session.Remove("EnableGuestLocWarningMsg"); //ZD 101120
                    HttpContext.Current.Session.Remove("GuestLocApprovalTime"); //ZD 101120
                    HttpContext.Current.Session.Remove("VMRPINChange"); //ZD 100522
                    HttpContext.Current.Session.Remove("PasswordCharLength"); //ZD 100522
					HttpContext.Current.Session.Remove("iControlRooms");//ZD 101098
				    HttpContext.Current.Session.Remove("SmartP2PNotify"); //ZD 100815
                    HttpContext.Current.Session.Remove("RoomDenialCommt");//ZD 101445
					//ZD 101228 Starts
                    HttpContext.Current.Session.Remove("AVWOAlertTime");
                    HttpContext.Current.Session.Remove("CatWOAlertTime");
                    HttpContext.Current.Session.Remove("FacilityWOAlertTime");
                    //ZD 101228 Ends
					HttpContext.Current.Session.Remove("EnableWETConference");//FB 100513
                    HttpContext.Current.Session.Remove("WebExLaunch");//FB 100513
                    HttpContext.Current.Session.Remove("IsLDAP");//ZD 101443
					HttpContext.Current.Session.Remove("EnableTemplateBooking"); //ZD 101562
                    HttpContext.Current.Session.Remove("EnableSetupTimeDisplay"); //ZD 101755
                    HttpContext.Current.Session.Remove("EnableTeardownTimeDisplay"); //ZD 101755
                    HttpContext.Current.Session.Remove("EnableActMsgDelivery"); //ZD 101757
                    HttpContext.Current.Session.Remove("defPolycomRMXLO"); //ZD 101803
                    HttpContext.Current.Session.Remove("defPolycomMGCLO"); //ZD 101803
                    HttpContext.Current.Session.Remove("DefCodianLO"); //ZD 101869    
                    HttpContext.Current.Session.Remove("ShowVideoLayout");//ZD 101931
                    HttpContext.Current.Session.Remove("EnableCalDefaultDisplay");//ZD 102356
                    HttpContext.Current.Session.Remove("EnableWaitList");//ZD 102532
                    HttpContext.Current.Session.Remove("EnableTravelAvoidTrack");//ZD 103216
                    //ZD 103550 - Start
                    HttpContext.Current.Session.Remove("EnableBJNIntegration");                    
                    HttpContext.Current.Session.Remove("BJNSelectOption");
                    HttpContext.Current.Session.Remove("BJNMeetingType");
                    HttpContext.Current.Session.Remove("EmptyConferencePush"); // ZD 104151
                    //ZD 103550 - End
                    HttpContext.Current.Session.Remove("BJNDisplay");//ZD 104116
                    HttpContext.Current.Session.Remove("PartyToRoom");//ZD 102916
                    HttpContext.Current.Session.Remove("EnableBlockUserDI");//ZD 104862
                    
                    if (HttpContext.Current.Session["ConferenceCode"] == null)
                        HttpContext.Current.Session.Add("ConferenceCode", xmldoc.SelectSingleNode("//GetAllOrgSettings/ConferenceCode").InnerText);

                    if (HttpContext.Current.Session["LeaderPin"] == null)
                        HttpContext.Current.Session.Add("LeaderPin", xmldoc.SelectSingleNode("//GetAllOrgSettings/LeaderPin").InnerText);

                    if (HttpContext.Current.Session["AdvAvParams"] == null)
                        HttpContext.Current.Session.Add("AdvAvParams", xmldoc.SelectSingleNode("//GetAllOrgSettings/AdvAvParams").InnerText);

                    if (HttpContext.Current.Session["AudioParams"] == null)
                        HttpContext.Current.Session.Add("AudioParams", xmldoc.SelectSingleNode("//GetAllOrgSettings/AudioParams").InnerText);
                    //FB 2359 Start
                    if (HttpContext.Current.Session["EnableRoomParam"] == null)
                        HttpContext.Current.Session.Add("EnableRoomParam", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomParam").InnerText);

                    //FB 2359 End
                    if (HttpContext.Current.Session["EnablePartyCode"] == null) //ZD 101446
                        HttpContext.Current.Session.Add("EnablePartyCode", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePartyCode").InnerText);

                    if (HttpContext.Current.Session["P2PEnable"] == null)
                        HttpContext.Current.Session.Add("P2PEnable", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableP2PConference").InnerText);

                    if (HttpContext.Current.Session["dialoutEnabled"] == null)
                        HttpContext.Current.Session.Add("dialoutEnabled", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableDialout").InnerText);

                    if (HttpContext.Current.Session["defaultPublic"] == null)
                        HttpContext.Current.Session.Add("defaultPublic", xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultConferencesAsPublic").InnerText);

                    if (HttpContext.Current.Session["DefaultConferenceType"] == null)
                        HttpContext.Current.Session.Add("DefaultConferenceType", xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultConferenceType").InnerText);

                    if (HttpContext.Current.Session["EnableRoomConfType"] == null)
                        HttpContext.Current.Session.Add("EnableRoomConfType", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomConference").InnerText);

                    if (HttpContext.Current.Session["EnableAudioVideoConfType"] == null)
                        HttpContext.Current.Session.Add("EnableAudioVideoConfType", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAudioVideoConference").InnerText);

                    if (HttpContext.Current.Session["EnableAudioOnlyConfType"] == null)
                        HttpContext.Current.Session.Add("EnableAudioOnlyConfType", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAudioOnlyConference").InnerText);

                    if (HttpContext.Current.Session["EnableNumericID"] == null) //FB 2870
                        HttpContext.Current.Session.Add("EnableNumericID", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableNumericID").InnerText);
                    
                    if (HttpContext.Current.Session["EnableHotdeskingConference"] == null) //FB 100719
                        HttpContext.Current.Session.Add("EnableHotdeskingConference", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableHotdeskingConference").InnerText);
                    //ZD 100513 Starts
                    if (HttpContext.Current.Session["EnableWETConference"] == null) //FB 100719
                        HttpContext.Current.Session.Add("EnableWETConference", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableWETConference").InnerText);
                    if (HttpContext.Current.Session["WebExLaunch"] == null) //FB 100719
                        HttpContext.Current.Session.Add("WebExLaunch", xmldoc.SelectSingleNode("//GetAllOrgSettings/WebExLaunch").InnerText);
                    //ZD 100513 Ends
                    if (HttpContext.Current.Session["DefaultCalendarToOfficeHours"] == null)
                        HttpContext.Current.Session.Add("DefaultCalendarToOfficeHours", xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultCalendarToOfficeHours").InnerText);

                    if (HttpContext.Current.Session["EnableEntity"] == null)
                        HttpContext.Current.Session.Add("EnableEntity", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCustomOption").InnerText);

                    if (HttpContext.Current.Session["EnableBufferZone"] == null)
                        HttpContext.Current.Session.Add("EnableBufferZone", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBufferZone").InnerText);

                    if (HttpContext.Current.Session["OrgSetupTime"] == null)//FB 2398
                        HttpContext.Current.Session.Add("OrgSetupTime", xmldoc.SelectSingleNode("//GetAllOrgSettings/SetupTime").InnerText);

                    if (HttpContext.Current.Session["OrgTearDownTime"] == null)
                        HttpContext.Current.Session.Add("OrgTearDownTime", xmldoc.SelectSingleNode("//GetAllOrgSettings/TearDownTime").InnerText);

                    if (HttpContext.Current.Session["dynInvite"] == null)
                        HttpContext.Current.Session.Add("dynInvite", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableDynamicInvite").InnerText);

                    if (HttpContext.Current.Session["doubleBooking"] == null)
                        HttpContext.Current.Session.Add("doubleBooking", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableDoubleBooking").InnerText);

                    if (HttpContext.Current.Session["IsOpen24Hours"] == null) //ZD 100157
                        HttpContext.Current.Session.Add("IsOpen24Hours", xmldoc.SelectSingleNode("//GetAllOrgSettings/SystemAvailableTime/IsOpen24Hours").InnerText);

                    if (HttpContext.Current.Session["SystemStartTime"] == null)
                        HttpContext.Current.Session.Add("SystemStartTime", xmldoc.SelectSingleNode("//GetAllOrgSettings/SystemAvailableTime/StartTime").InnerText);

                    if (HttpContext.Current.Session["SystemEndTime"] == null)
                        HttpContext.Current.Session.Add("SystemEndTime", xmldoc.SelectSingleNode("//GetAllOrgSettings/SystemAvailableTime/EndTime").InnerText);

                    if (HttpContext.Current.Session["DaysClosed"] == null) //FB 2804
                        HttpContext.Current.Session.Add("DaysClosed", xmldoc.SelectSingleNode("//GetAllOrgSettings/SystemAvailableTime/DaysClosed").InnerText);

                    if (HttpContext.Current.Session["tzsystemid"] == null)
                        HttpContext.Current.Session.Add("tzsystemid", xmldoc.SelectSingleNode("//GetAllOrgSettings/TimezoneSystemID").InnerText);

                    if (HttpContext.Current.Session["roomExpandLevel"] == null)
                        HttpContext.Current.Session.Add("roomExpandLevel", xmldoc.SelectSingleNode("//GetAllOrgSettings/RoomTreeExpandLevel").InnerText);

                    HttpContext.Current.Session.Add("RoomListView", "level");

                    if (HttpContext.Current.Session["roomExpandLevel"].ToString() == "list")
                        HttpContext.Current.Session["RoomListView"] = "list";

                    if (HttpContext.Current.Session["RoomLimit"] == null)
                        HttpContext.Current.Session.Add("RoomLimit", xmldoc.SelectSingleNode("//GetAllOrgSettings/RoomLimit").InnerText);

                    if (HttpContext.Current.Session["McuLimit"] == null)
                        HttpContext.Current.Session.Add("McuLimit", xmldoc.SelectSingleNode("//GetAllOrgSettings/MCULimit").InnerText);

                    if (HttpContext.Current.Session["McuEnchancedLimit"] == null)//FB 2486
                        HttpContext.Current.Session.Add("McuEnchancedLimit", xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUEnchancedLimit").InnerText);//FB 2486


                    if (HttpContext.Current.Session["UserLimit"] == null)
                        HttpContext.Current.Session.Add("UserLimit", xmldoc.SelectSingleNode("//GetAllOrgSettings/UserLimit").InnerText);

                    if (HttpContext.Current.Session["organizationName"] == null)
                        HttpContext.Current.Session.Add("organizationName", xmldoc.SelectSingleNode("//GetAllOrgSettings/organizationName").InnerText);

                    if (HttpContext.Current.Session["recurEnable"] == null)
                        HttpContext.Current.Session.Add("recurEnable", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRecurringConference").InnerText);

                    if (HttpContext.Current.Session["contactName"] == null)
                    {
                        //FB 1888 start
                        String contactname = ReplaceOutXMLSpecialCharacters(xmldoc.SelectSingleNode("//GetAllOrgSettings/ContactDetails/Name").InnerText, 2); //ZD 104391
                        contactname = contactname.Replace("\"", "||").Replace("\'", "!!");
                        HttpContext.Current.Session.Add("contactName", contactname);
                        //FB 1888 end
                    }

                    if (HttpContext.Current.Session["contactEmail"] == null)
                        HttpContext.Current.Session.Add("contactEmail", ReplaceOutXMLSpecialCharacters(xmldoc.SelectSingleNode("//GetAllOrgSettings/ContactDetails/Email").InnerText,2)); //ZD 104391

                    if (HttpContext.Current.Session["contactPhone"] == null)
                        HttpContext.Current.Session.Add("contactPhone", ReplaceOutXMLSpecialCharacters(xmldoc.SelectSingleNode("//GetAllOrgSettings/ContactDetails/Phone").InnerText, 2)); //ZD 104391

                    if (HttpContext.Current.Session["contactAddInfo"] == null)
                        HttpContext.Current.Session.Add("contactAddInfo", ReplaceOutXMLSpecialCharacters(xmldoc.SelectSingleNode("//GetAllOrgSettings/ContactDetails/AdditionInfo").InnerText,2)); //ZD 104391

                    if (HttpContext.Current.Session["hkModule"] == null)
                        HttpContext.Current.Session.Add("hkModule", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableHouseKeeping").InnerText);

                    if (HttpContext.Current.Session["foodModule"] == null)
                        HttpContext.Current.Session.Add("foodModule", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCatering").InnerText);


                    if (HttpContext.Current.Session["EnableAPIs"] == null)
                        HttpContext.Current.Session.Add("EnableAPIs", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAPIs").InnerText);

                    if (HttpContext.Current.Session["VideoRooms"] == null)
                        HttpContext.Current.Session.Add("VideoRooms", xmldoc.SelectSingleNode("//GetAllOrgSettings/VideoRooms").InnerText);

                    if (HttpContext.Current.Session["NonVideoRooms"] == null)
                        HttpContext.Current.Session.Add("NonVideoRooms", xmldoc.SelectSingleNode("//GetAllOrgSettings/NonVideoRooms").InnerText);

                    if (HttpContext.Current.Session["VMRRooms"] == null)//FB 2586
                        HttpContext.Current.Session.Add("VMRRooms", xmldoc.SelectSingleNode("//GetAllOrgSettings/VMRRooms").InnerText);

                    if (HttpContext.Current.Session["iControlRooms"] == null)//ZD 101098
                        HttpContext.Current.Session.Add("iControlRooms", xmldoc.SelectSingleNode("//GetAllOrgSettings/iControlRooms").InnerText);

                    //FB 2694 Start
                    if (HttpContext.Current.Session["HotdeskingVideoRooms"] == null)
                        HttpContext.Current.Session.Add("HotdeskingVideoRooms", xmldoc.SelectSingleNode("//GetAllOrgSettings/HotdeskingVideoRooms").InnerText);

                    if (HttpContext.Current.Session["HotdeskingNonVideoRooms"] == null)
                        HttpContext.Current.Session.Add("HotdeskingNonVideoRooms", xmldoc.SelectSingleNode("//GetAllOrgSettings/HotdeskingNonVideoRooms").InnerText);
                    //FB 2694 End

                    if (HttpContext.Current.Session["EndPoints"] == null)
                        HttpContext.Current.Session.Add("EndPoints", xmldoc.SelectSingleNode("//GetAllOrgSettings/EndPoints").InnerText);

                    if (HttpContext.Current.Session["roomModule"] == null)
                        HttpContext.Current.Session.Add("roomModule", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableFacilites").InnerText);

                    if (HttpContext.Current.Session["ExchangeUserLimit"] == null)
                        HttpContext.Current.Session.Add("ExchangeUserLimit", xmldoc.SelectSingleNode("//GetAllOrgSettings/ExchangeUserLimit").InnerText);

                    if (HttpContext.Current.Session["DominoUserLimit"] == null)
                        HttpContext.Current.Session.Add("DominoUserLimit", xmldoc.SelectSingleNode("//GetAllOrgSettings/DominoUserLimit").InnerText);

                    if (HttpContext.Current.Session["MobileUserLimit"] == null) //FB 1979
                        HttpContext.Current.Session.Add("MobileUserLimit", xmldoc.SelectSingleNode("//GetAllOrgSettings/MobileUserLimit").InnerText);

                    if (HttpContext.Current.Session["WebexUserLimit"] == null) //ZD 100221
                        HttpContext.Current.Session.Add("WebexUserLimit", xmldoc.SelectSingleNode("//GetAllOrgSettings/WebexUserLimit").InnerText);

                    if (HttpContext.Current.Session["GuestRooms"] == null) //FB 2426
                        HttpContext.Current.Session.Add("GuestRooms", xmldoc.SelectSingleNode("//GetAllOrgSettings/GuestRooms").InnerText);

                    //if (HttpContext.Current.Session["PCModule"] == null) //FB 2347 //FB 2693
                    //  HttpContext.Current.Session.Add("PCModule", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePC").InnerText);

                    if (HttpContext.Current.Session["EnablePublicRooms"] == null) //FB 2594
                        HttpContext.Current.Session.Add("EnablePublicRooms", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePublicRoom").InnerText);
                    
                    if(HttpContext.Current.Session["Cloud"] == null) //FB 2645
                        HttpContext.Current.Session.Add("Cloud", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCloud").InnerText);


                    if (HttpContext.Current.Session["isVIP"] == null)// FB 1864
                        HttpContext.Current.Session.Add("isVIP", xmldoc.SelectSingleNode("//GetAllOrgSettings/isVIP").InnerText);

                    if (HttpContext.Current.Session["EnableRPRMRoomSync"] == null)// ZD 101527
                        HttpContext.Current.Session.Add("EnableRPRMRoomSync", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRPRMRoomSync").InnerText);

                    if (HttpContext.Current.Session["isAssignedMCU"] == null)// FB 1901
                        HttpContext.Current.Session.Add("isAssignedMCU", xmldoc.SelectSingleNode("//GetAllOrgSettings/isAssignedMCU").InnerText);

                    if (HttpContext.Current.Session["isMultiLingual"] == null)//FB 1830 - Translation
                        HttpContext.Current.Session.Add("isMultiLingual", xmldoc.SelectSingleNode("//GetAllOrgSettings/isMultiLingual").InnerText);
                    //Commented for FB 2395
                    //if (HttpContext.Current.Session["Holidays"] == null)// FB 1861
                    //    HttpContext.Current.Session.Add("Holidays", xmldoc.SelectSingleNode("//GetAllOrgSettings/SystemHolidays").InnerXml);

                    if (HttpContext.Current.Session["EnableRoomServiceType"] == null)// FB 2219
                        HttpContext.Current.Session.Add("EnableRoomServiceType", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomServiceType").InnerText);

                    if (HttpContext.Current.Session["EnableImmConf"] == null)// FB 2036
                        HttpContext.Current.Session.Add("EnableImmConf", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableImmConf").InnerText);

                    if (HttpContext.Current.Session["EnablePasswordRule"] == null)// FB 2339
                        HttpContext.Current.Session.Add("EnablePasswordRule", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePasswordRule").InnerText);

                    if (HttpContext.Current.Session["DedicatedVideo"] == null)// FB 2334
                        HttpContext.Current.Session.Add("DedicatedVideo", xmldoc.SelectSingleNode("//GetAllOrgSettings/DedicatedVideo").InnerText);

                    if (HttpContext.Current.Session["EnableAudioBridges"] == null)// FB 2023
                        HttpContext.Current.Session.Add("EnableAudioBridges", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAudioBridges").InnerText);

                    HttpContext.Current.Session.Add("EnableConfPassword", "1");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableConferencePassword") != null)// FB 2359
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableConferencePassword").InnerText != "")
                            HttpContext.Current.Session["EnableConfPassword"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableConferencePassword").InnerText;

                    HttpContext.Current.Session.Add("EnablePublicConf", "1");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePublicConference") != null)// FB 2359
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePublicConference").InnerText != "")
                            HttpContext.Current.Session["EnablePublicConf"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePublicConference").InnerText;
                    
                    //ZD 100707 - Start
                    HttpContext.Current.Session.Add("ShowHideVMR", "1");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/ShowHideVMR") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/ShowHideVMR").InnerText != "")
                            HttpContext.Current.Session["ShowHideVMR"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/ShowHideVMR").InnerText;

                    HttpContext.Current.Session.Add("EnablePersonaVMR", "1");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePersonaVMR") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePersonaVMR").InnerText != "")
                            HttpContext.Current.Session["EnablePersonaVMR"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePersonaVMR").InnerText;

                    HttpContext.Current.Session.Add("EnableRoomVMR", "1");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomVMR") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomVMR").InnerText != "")
                            HttpContext.Current.Session["EnableRoomVMR"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomVMR").InnerText;

                    HttpContext.Current.Session.Add("EnableExternalVMR", "1");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableExternalVMR") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableExternalVMR").InnerText != "")
                            HttpContext.Current.Session["EnableExternalVMR"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableExternalVMR").InnerText;
                    //ZD 100707 - End

                    //ZD 100704 - Starts
                    HttpContext.Current.Session.Add("EnableExpressConfType", "1");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableExpressConfType") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableExpressConfType").InnerText != "")
                            HttpContext.Current.Session["EnableExpressConfType"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableExpressConfType").InnerText;
                    //ZD 100704 - End
                    //ZD 100834- Starts
                    HttpContext.Current.Session.Add("EnableDetailedExpressForm", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableDetailedExpressForm") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableDetailedExpressForm").InnerText != "")
                         HttpContext.Current.Session["EnableDetailedExpressForm"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableDetailedExpressForm").InnerText;
                    //ZD 100834 - End

                    //ZD 100963 - Starts
                    HttpContext.Current.Session.Add("EnableRoomCalendarView", "1");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomCalendarView") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomCalendarView").InnerText != "")
                            HttpContext.Current.Session["EnableRoomCalendarView"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableRoomCalendarView").InnerText;
                    //ZD 100963 - End

                    //ZD 101120 - Starts
                    HttpContext.Current.Session.Add("EnableGuestLocWarningMsg", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableGuestLocWarningMsg") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableGuestLocWarningMsg").InnerText != "")
                            HttpContext.Current.Session["EnableGuestLocWarningMsg"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableGuestLocWarningMsg").InnerText;

                    HttpContext.Current.Session.Add("GuestLocApprovalTime", "1");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/GuestLocApprovalTime") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/GuestLocApprovalTime").InnerText != "")
                            HttpContext.Current.Session["GuestLocApprovalTime"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/GuestLocApprovalTime").InnerText;
                    //ZD 101120 - End
                    //ZD 100522 - Starts
                    HttpContext.Current.Session.Add("PasswordCharLength", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/PasswordCharLength") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/PasswordCharLength").InnerText != "")
                            HttpContext.Current.Session["PasswordCharLength"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/PasswordCharLength").InnerText;

                    HttpContext.Current.Session.Add("VMRPINChange", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/VMRPINChange") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/VMRPINChange").InnerText != "")
                            HttpContext.Current.Session["VMRPINChange"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/VMRPINChange").InnerText;
                    //ZD 100522 - End
                    //ZD 101445 - Start
                    HttpContext.Current.Session.Add("RoomDenialCommt", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/RoomDenialCommt") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/RoomDenialCommt").InnerText != "")
                            HttpContext.Current.Session["RoomDenialCommt"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/RoomDenialCommt").InnerText;
                    //ZD 101445 - End
                    //ZD  100815 
                    HttpContext.Current.Session.Add("SmartP2PNotify", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/SmartP2PNotify") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/SmartP2PNotify").InnerText != "")
                            HttpContext.Current.Session["SmartP2PNotify"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/SmartP2PNotify").InnerText; 
                    //ZD 101228 Starts
                    HttpContext.Current.Session.Add("CatWOAlertTime", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/CatWOAlertTime") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/CatWOAlertTime").InnerText != "")
                            HttpContext.Current.Session["CatWOAlertTime"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/CatWOAlertTime").InnerText;

                    HttpContext.Current.Session.Add("FacilityWOAlertTime", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/FacilityWOAlertTime") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/FacilityWOAlertTime").InnerText != "")
                            HttpContext.Current.Session["FacilityWOAlertTime"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/FacilityWOAlertTime").InnerText;

                    HttpContext.Current.Session.Add("AVWOAlertTime", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/AVWOAlertTime") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/AVWOAlertTime").InnerText != "")
                            HttpContext.Current.Session["AVWOAlertTime"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/AVWOAlertTime").InnerText; 
                    //ZD 101228 Ends

                    if (HttpContext.Current.Session["isSpecialRecur"] == null)//FB 2052
                        HttpContext.Current.Session.Add("isSpecialRecur", xmldoc.SelectSingleNode("//GetAllOrgSettings/isSpecialRecur").InnerText);

                    if (HttpContext.Current.Session["WorkingHours"] == null)//FB 2343
                        HttpContext.Current.Session.Add("WorkingHours", xmldoc.SelectSingleNode("//GetAllOrgSettings/WorkingHours").InnerText);

                    if (HttpContext.Current.Session["EnableSurvey"] == null)//FB 2348
                        HttpContext.Current.Session.Add("EnableSurvey", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSurvey").InnerText);
                    //FB 2283 start

                    //FB 2472 start
                    HttpContext.Current.Session.Add("MCUAlert", "1");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUAlert") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUAlert").InnerText != "")
                            HttpContext.Current.Session["MCUAlert"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUAlert").InnerText;
                    //FB 2472 end
                    
                    //FB 2401
                    if (HttpContext.Current.Session["EnableEPDetails"] == null)
                        HttpContext.Current.Session.Add("EnableEPDetails", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableEPDetails").InnerText);

                    //FB 2429
                    if (HttpContext.Current.Session["OrgLineRate"] == null)
                        HttpContext.Current.Session.Add("OrgLineRate", xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultLineRate").InnerText);

                    //ZD 100263

                    if (HttpContext.Current.Session["EnableFileWhiteList"] == null)
                        HttpContext.Current.Session.Add("EnableFileWhiteList", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableFileWhiteList").InnerText);
                    if (HttpContext.Current.Session["FileWhiteList"] == null)
                        HttpContext.Current.Session.Add("FileWhiteList", xmldoc.SelectSingleNode("//GetAllOrgSettings/FileWhiteList").InnerText);

                    HttpContext.Current.Session["Cloud"] = "0"; //FB 2599
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCloud") != null)
                    {
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCloud").InnerText != "")
                            HttpContext.Current.Session.Add("Cloud", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCloud").InnerText);
                    }
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/OrgLanguage") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/OrgLanguage").InnerText.Trim() != "")
                            orgBaseLang = xmldoc.SelectSingleNode("//GetAllOrgSettings/OrgLanguage").InnerText;

                    HttpContext.Current.Session.Add("OrgBaseLang", orgBaseLang);

                    //ZD 100958

                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EmailDateFormat") != null)
                        HttpContext.Current.Session.Add("EmailDateFormat", xmldoc.SelectSingleNode("//GetAllOrgSettings/EmailDateFormat").InnerText);

                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/OrgEmailLanguageID") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/OrgEmailLanguageID").InnerText.Trim() != "")
                            OrgEmailLang = xmldoc.SelectSingleNode("//GetAllOrgSettings/OrgEmailLanguageID").InnerText;

                    HttpContext.Current.Session.Add("OrgEmailLangID", OrgEmailLang);

                    //FB 2283 end

                    //FB 2419 
                    if (HttpContext.Current.Session["EnableAcceptDecline"] == null)
                        HttpContext.Current.Session.Add("EnableAcceptDecline", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAcceptDecline").InnerText);
                    
                    //// FB 2620 Starts
                    //    if (HttpContext.Current.Session["EnableVMR"] == null)
                    //    HttpContext.Current.Session.Add("EnableVMR", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableVMR").InnerText);
                    //// FB 2620 Ends                    

                    //FB 2426 Start
                    if (HttpContext.Current.Session["OnflyTopTierID"] == null)
                        HttpContext.Current.Session.Add("OnflyTopTierID", xmldoc.SelectSingleNode("//GetAllOrgSettings/OnflyTier/OnflyTopTierID").InnerText);
                    if (HttpContext.Current.Session["OnflyMiddleTierID"] == null)
                        HttpContext.Current.Session.Add("OnflyMiddleTierID", xmldoc.SelectSingleNode("//GetAllOrgSettings/OnflyTier/OnflyMiddleTierID").InnerText);
                    if (HttpContext.Current.Session["OnflyTopTierName"] == null)
                        HttpContext.Current.Session.Add("OnflyTopTierName", xmldoc.SelectSingleNode("//GetAllOrgSettings/OnflyTier/DefTopTier").InnerText);
                    if (HttpContext.Current.Session["OnflyMiddleTierName"] == null)
                        HttpContext.Current.Session.Add("OnflyMiddleTierName", xmldoc.SelectSingleNode("//GetAllOrgSettings/OnflyTier/DefMiddleTier").InnerText);
                    //FB 2426 End
                    //ZD 100068 Starts
                    if (HttpContext.Current.Session["VMRTopTierID"] == null && xmldoc.SelectSingleNode("//GetAllOrgSettings/VMRTier/VMRTopTierID") != null)
                        HttpContext.Current.Session.Add("VMRTopTierID", xmldoc.SelectSingleNode("//GetAllOrgSettings/VMRTier/VMRTopTierID").InnerText);
                    if (HttpContext.Current.Session["VMRMiddleTierID"] == null && xmldoc.SelectSingleNode("//GetAllOrgSettings/VMRTier/VMRMiddleTierID") != null)
                        HttpContext.Current.Session.Add("VMRMiddleTierID", xmldoc.SelectSingleNode("//GetAllOrgSettings/VMRTier/VMRMiddleTierID").InnerText);
                    //ZD 100068 Ends
                    //FB 2469 - Starts
                    if (HttpContext.Current.Session["EnableConfTZinLoc"] == null)
                        HttpContext.Current.Session.Add("EnableConfTZinLoc", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableConfTZinLoc").InnerText);
                    if (HttpContext.Current.Session["SendConfirmationEmail"] == null)
                        HttpContext.Current.Session.Add("SendConfirmationEmail", xmldoc.SelectSingleNode("//GetAllOrgSettings/SendConfirmationEmail").InnerText);
					//FB 2469 - End
                    HttpContext.Current.Session.Add("EnableSmartP2P", 0); //FB 2430
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSmartP2P") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSmartP2P").InnerText.Trim() != "")
                            HttpContext.Current.Session["EnableSmartP2P"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSmartP2P").InnerText.Trim();
					if (HttpContext.Current.Session["DefaultConfDuration"] == null)//FB 2501
                        HttpContext.Current.Session.Add("DefaultConfDuration", xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultConfDuration").InnerText);
                    //FB 2550 Starts
                    HttpContext.Current.Session.Add("MaxPublicVMRParty", 0); //FB 2430
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/MaxPublicVMRParty") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/MaxPublicVMRParty").InnerText.Trim() != "")
                            HttpContext.Current.Session["MaxPublicVMRParty"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/MaxPublicVMRParty").InnerText.Trim();
                    //FB 2550 Ends
                    //FB 2571 Start
                    HttpContext.Current.Session.Add("EnableFECC", 0); 
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableFECC") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableFECC").InnerText.Trim() != "")
                            HttpContext.Current.Session["EnableFECC"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableFECC").InnerText.Trim();
                    HttpContext.Current.Session.Add("DefaultFECC", 0);
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultFECC") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultFECC").InnerText.Trim() != "")
                            HttpContext.Current.Session["DefaultFECC"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultFECC").InnerText.Trim();
                    //FB 2571 End

                    //FB 2598 Starts
                    if (HttpContext.Current.Session["EnableCallmonitor"] == null)
                        HttpContext.Current.Session.Add("EnableCallmonitor", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCallmonitor").InnerText);
                    if (HttpContext.Current.Session["EnableEM7"] == null)
                        HttpContext.Current.Session.Add("EnableEM7", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableEM7").InnerText);
                    if (HttpContext.Current.Session["EnableCDR"] == null)
                        HttpContext.Current.Session.Add("EnableCDR", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCDR").InnerText);
                    //FB 2598 Ends
                    //ZD 100151
                    if (HttpContext.Current.Session["ShowCusAttInCalendar"] == null)
                        HttpContext.Current.Session.Add("ShowCusAttInCalendar", xmldoc.SelectSingleNode("//GetAllOrgSettings/ShowCusAttInCalendar").InnerText);

                    //FB 2609 Starts
                    HttpContext.Current.Session.Add("MeetandGreetBuffer", 0);
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/MeetandGreetBuffer") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/MeetandGreetBuffer").InnerText.Trim() != "")
                            HttpContext.Current.Session["MeetandGreetBuffer"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/MeetandGreetBuffer").InnerText.Trim();
                    //FB 2609 Ends
                    //FB 2637 Starts
                    if (HttpContext.Current.Session["AlertforTier1"] == null)
                        HttpContext.Current.Session.Add("AlertforTier1", xmldoc.SelectSingleNode("//GetAllOrgSettings/AlertforTier1").InnerText);

                    //FB 2595
                    if (HttpContext.Current.Session["SecureSwitch"] == null)
                        HttpContext.Current.Session.Add("SecureSwitch", xmldoc.SelectSingleNode("//GetAllOrgSettings/SecureSwitch").InnerText);

                    if (HttpContext.Current.Session["EnableZulu"] == null)
                        HttpContext.Current.Session.Add("EnableZulu", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableZulu").InnerText);

                    if (HttpContext.Current.Session["NetworkSwitching"] == null) //FB 2993
                        HttpContext.Current.Session.Add("NetworkSwitching", xmldoc.SelectSingleNode("//GetAllOrgSettings/NetworkSwitching").InnerText);
					 //FB 2670 START
                    if (HttpContext.Current.Session["EnableOnsiteAV"] == null)//Enable On-Site A/V Support
                        HttpContext.Current.Session.Add("EnableOnsiteAV", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableOnsiteAV").InnerText);

                    if (HttpContext.Current.Session["EnableMeetandGreet"] == null)//Enable Meet and Greet
                        HttpContext.Current.Session.Add("EnableMeetandGreet", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableMeetandGreet").InnerText);

                    if (HttpContext.Current.Session["EnableConciergeMonitoring"] == null)//Enable Concierge Monitoring
                        HttpContext.Current.Session.Add("EnableConciergeMonitoring", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableConciergeMonitoring").InnerText);

                    if (HttpContext.Current.Session["EnableDedicatedVNOC"] == null)//Enable Dedicated VNOC Operator 
                        HttpContext.Current.Session.Add("EnableDedicatedVNOC", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableDedicatedVNOC").InnerText);
                    //FB 2670 END
					//FB 2693 Starts
                    if (HttpContext.Current.Session["PCUserLimit"] == null)
                        HttpContext.Current.Session.Add("PCUserLimit", xmldoc.SelectSingleNode("//GetAllOrgSettings/PCUserLimit").InnerText);

                    if (HttpContext.Current.Session["EnableBlueJeans"] == null)
                        HttpContext.Current.Session.Add("EnableBlueJeans", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBlueJeans").InnerText);

                    if (HttpContext.Current.Session["EnableJabber"] == null)
                        HttpContext.Current.Session.Add("EnableJabber", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableJabber").InnerText);

                    if (HttpContext.Current.Session["EnableLync"] == null)
                        HttpContext.Current.Session.Add("EnableLync", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableLync").InnerText);

                    if (HttpContext.Current.Session["EnableVidtel"] == null)
                        HttpContext.Current.Session.Add("EnableVidtel", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableVidtel").InnerText);
                    //FB 2693 Ends
					//FB 2593 Starts
                    if (HttpContext.Current.Session["EnableAdvancedReport"] == null)//FB 2593
                        HttpContext.Current.Session.Add("EnableAdvancedReport", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAdvancedReport").InnerText);
					//FB 2593 End
                    //FB 2641 starts
                    HttpContext.Current.Session.Add("EnableLinerate", 0);
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableLinerate") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableLinerate").InnerText.Trim() != "")
                            HttpContext.Current.Session["EnableLinerate"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableLinerate").InnerText.Trim();

                    HttpContext.Current.Session.Add("EnableStartMode", 0);
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableStartMode") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableStartMode").InnerText.Trim() != "")
                            HttpContext.Current.Session["EnableStartMode"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableStartMode").InnerText.Trim();
                    //FB 2641 End

                    //FB 2779 Starts
                    if (HttpContext.Current.Session["ThemeType"] == null)
                        HttpContext.Current.Session.Add("ThemeType", xmldoc.SelectSingleNode("//GetAllOrgSettings/ThemeName").InnerText);

                    //FB 2779 Ends
                    if (HttpContext.Current.Session["EnableSingleRoomConfEmails"] == null) //FB 2817
                        HttpContext.Current.Session.Add("EnableSingleRoomConfEmails", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSingleRoomConfEmails").InnerText.Trim());

                    //FB 2839
                    if (HttpContext.Current.Session["EnableProfileSelection"] == null)
                        HttpContext.Current.Session.Add("EnableProfileSelection", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableProfileSelection").InnerText);

                    if (HttpContext.Current.Session["EnablePoolOrderSelection"] == null) //ZD 104256
                        HttpContext.Current.Session.Add("EnablePoolOrderSelection", xmldoc.SelectSingleNode("//GetAllOrgSettings/EnablePoolOrderSelection").InnerText);

                    //FB 2998
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUSetupDisplay") != null)
                        HttpContext.Current.Session["MCUSetupDisplay"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUSetupDisplay").InnerText;

                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUTearDisplay") != null)
                        HttpContext.Current.Session["MCUTearDisplay"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUTearDisplay").InnerText;

                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/McuSetupTime") != null)
                        HttpContext.Current.Session["McuSetupTime"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/McuSetupTime").InnerText;

                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUTeardonwnTime") != null)
                        HttpContext.Current.Session["MCUTeardonwnTime"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/MCUTeardonwnTime").InnerText;

                    if (HttpContext.Current.Session["DefaultSubject"] == null) //ZD 100167
                        HttpContext.Current.Session.Add("DefaultSubject", xmldoc.SelectSingleNode("//GetAllOrgSettings/DefaultSubject").InnerText.Trim());
					//ZD 100164 START
                    HttpContext.Current.Session.Add("EnableAdvancedUserOption", 0);
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAdvancedUserOption") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAdvancedUserOption").InnerText.Trim() != "")
                            HttpContext.Current.Session["EnableAdvancedUserOption"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableAdvancedUserOption").InnerText.Trim();
                    //ZD 100164 END

                    //ZD 100935 Starts
                    HttpContext.Current.Session.Add("EnableWebExIntg", 0);
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableWebExIntg") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableWebExIntg").InnerText.Trim() != "")
                            HttpContext.Current.Session["EnableWebExIntg"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableWebExIntg").InnerText.Trim();
                    //ZD 100935 Ends

                    //ZD 101443 Starts
                    HttpContext.Current.Session.Add("IsLDAP", 0);
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/licensedetails/IsLDAP") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/licensedetails/IsLDAP").InnerText.Trim() == "1")
                        HttpContext.Current.Session["IsLDAP"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/licensedetails/IsLDAP").InnerText.Trim();
                    //ZD 101443 End
					//ZD 101562 START
                    HttpContext.Current.Session.Add("EnableTemplateBooking", "1");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableTemplateBooking") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableTemplateBooking").InnerText != "")
                            HttpContext.Current.Session["EnableTemplateBooking"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableTemplateBooking").InnerText;
                    //ZD 101562 END

                    //ZD 101755 start
                    HttpContext.Current.Session.Add("EnableSetupTimeDisplay" ,"0");
                     if(xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSetupTimeDisplay")!=null && xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSetupTimeDisplay").InnerText.Trim() != "")
                         HttpContext.Current.Session["EnableSetupTimeDisplay"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableSetupTimeDisplay").InnerText;

                     HttpContext.Current.Session.Add("EnableTeardownTimeDisplay", "0");
                     if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableTeardownTimeDisplay") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableTeardownTimeDisplay").InnerText.Trim() != "")
                         HttpContext.Current.Session["EnableTeardownTimeDisplay"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableTeardownTimeDisplay").InnerText;
					 //ZD 101869 Starts
                     HttpContext.Current.Session.Add("DefCodianLO", "1");
                     if (xmldoc.SelectSingleNode("//GetAllOrgSettings/DefCodianLO") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/DefCodianLO").InnerText.Trim() != "")
                         HttpContext.Current.Session["DefCodianLO"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/DefCodianLO").InnerText;
					//ZD 101869 End
					 //ZD 101803 Starts
                     HttpContext.Current.Session.Add("defPolycomRMXLO", "1");
                     if (xmldoc.SelectSingleNode("//GetAllOrgSettings/defPolycomRMXLO") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/defPolycomRMXLO").InnerText.Trim() != "")
                         HttpContext.Current.Session["defPolycomRMXLO"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/defPolycomRMXLO").InnerText;

                     HttpContext.Current.Session.Add("defPolycomMGCLO", "1");
                     if (xmldoc.SelectSingleNode("//GetAllOrgSettings/defPolycomMGCLO") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/defPolycomMGCLO").InnerText.Trim() != "")
                         HttpContext.Current.Session["defPolycomMGCLO"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/defPolycomMGCLO").InnerText;
					 //ZD 101803 End

                    //ZD 101755 End
					//ZD 101757
                    HttpContext.Current.Session.Add("EnableActMsgDelivery", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableActMsgDelivery") != null)
                        if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableActMsgDelivery").InnerText != "")
                            HttpContext.Current.Session["EnableActMsgDelivery"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableActMsgDelivery").InnerText;

                    //ZD  101931 Start
                    HttpContext.Current.Session.Add("ShowVideoLayout", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/ShowVideoLayout") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/ShowVideoLayout").InnerText.Trim() != "")
                        HttpContext.Current.Session["ShowVideoLayout"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/ShowVideoLayout").InnerText;
                    //ZD 101931 End

                    HttpContext.Current.Session.Add("EnableCalDefaultDisplay", "2");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCalDefaultDisplay") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCalDefaultDisplay").InnerText.Trim() != "")
                        HttpContext.Current.Session["EnableCalDefaultDisplay"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableCalDefaultDisplay").InnerText;

                    //ZD 102532 Start
                    HttpContext.Current.Session.Add("EnableWaitList", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableWaitList") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableWaitList").InnerText.Trim() != "")
                        HttpContext.Current.Session["EnableWaitList"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableWaitList").InnerText;
                    //ZD 102532 End
                    //ZD 103216
                    HttpContext.Current.Session.Add("EnableTravelAvoidTrack", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableTravelAvoidTrack") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableTravelAvoidTrack").InnerText.Trim() != "")
                        HttpContext.Current.Session["EnableTravelAvoidTrack"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableTravelAvoidTrack").InnerText;

                    //ZD 103550 - Start
                    HttpContext.Current.Session.Add("EnableBJNIntegration", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBJNIntegration") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBJNIntegration").InnerText.Trim() != "")
                        HttpContext.Current.Session["EnableBJNIntegration"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBJNIntegration").InnerText;
                    
                    HttpContext.Current.Session.Add("BJNSelectOption", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNSelectOption") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNSelectOption").InnerText.Trim() != "")
                        HttpContext.Current.Session["BJNSelectOption"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNSelectOption").InnerText;

                    HttpContext.Current.Session.Add("BJNMeetingType", "1");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNMeetingType") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNMeetingType").InnerText.Trim() != "")
                        HttpContext.Current.Session["BJNMeetingType"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNMeetingType").InnerText;
                    //ZD 103550 - End
                    // ZD 104151 Start
                    if (HttpContext.Current.Session["EmptyConferencePush"] == null)
                        HttpContext.Current.Session.Add("EmptyConferencePush", xmldoc.SelectSingleNode("//GetAllOrgSettings/EmptyConferencePush").InnerText);
                    // ZD 104151 End
                    //ZD 104116 - Start
                    HttpContext.Current.Session.Add("BJNDisplay", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNDisplay") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNDisplay").InnerText.Trim() != "")
                        HttpContext.Current.Session["BJNDisplay"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/BJNDisplay").InnerText;
                    //ZD 104116 - End
                    //ZD 102916 - Start
                    HttpContext.Current.Session.Add("PartyToRoom", "0");
                    if (xmldoc.SelectSingleNode("//GetAllOrgSettings/AssignParticipantsToRoom") != null && xmldoc.SelectSingleNode("//GetAllOrgSettings/AssignParticipantsToRoom").InnerText.Trim() != "")
                        HttpContext.Current.Session["PartyToRoom"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/AssignParticipantsToRoom").InnerText;
                    //ZD 102916 - End
                    //ZD 104862 start
                    HttpContext.Current.Session.Add("EnableBlockUserDI", "0");
                    if(xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBlockUserDI") != null &&  xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBlockUserDI").InnerText.Trim() != "")
                        HttpContext.Current.Session["EnableBlockUserDI"] = xmldoc.SelectSingleNode("//GetAllOrgSettings/EnableBlockUserDI").InnerText;
                    //ZD 104862 End 
                }
                /* Organization CSS Module */

                SetCSSFilePath();
                SetOrgTextChangeXML();

                CustomizationUtil.CSSReplacementUtility cssUtil = new CustomizationUtil.CSSReplacementUtility();
                cssUtil.ApplicationPath = HttpContext.Current.Server.MapPath("."); //FB 1830 - Translation Menu
                String orgID = "";
                if (HttpContext.Current.Session["organizationID"] != null)
                    orgID = HttpContext.Current.Session["organizationID"].ToString();

                orgID = "Org_" + orgID;
                cssUtil.FolderName = orgID;
                cssUtil.CreateOrgStyles();
                //FB 2272 Starts
                if (cssUtil.ApplicationPath.ToString() != HttpContext.Current.Server.MapPath("..") + "//en")
                    cssUtil.ApplicationPath = HttpContext.Current.Server.MapPath("..") + "//en";
                cssUtil.CreateOrgStyles();
                //FB 2272 Ends

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + "Error in getting organization details: " + ex.Message);
                throw new Exception(errXML);//FB 1881
                //throw new Exception("Error in getting organization details");
            }
        }
        #endregion

        #region GetTimezones
        /// <summary>
        /// GetTimezones
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="sel"></param>
        public void GetAllTimezones(DropDownList sender, ref String sel)
        {
            String userID = "11";//Code added for Error 200
            try
            {

                if (HttpContext.Current.Session["userID"] != null)//Code added for Error 200
                    userID = HttpContext.Current.Session["userID"].ToString();//Code added for Error 200

                String inXML = "<GetTimezones><UserID>" + userID + "</UserID>" + OrgXMLElement() + "</GetTimezones>";//Code added for Error 200 //Organization Module Fixes
                String outXML;
                //if (HttpContext.Current.Session["Timezones"] == null)
                {
                    outXML = CallMyVRMServer("GetAllTimezones", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                    //outXML = "<timezones><selected>26</selected><timezone><timezoneID>26</timezoneID><timezoneName>Eastern Standard Time</timezoneName></timezone></timezones>";
                    HttpContext.Current.Session.Add("AllTimezones", outXML);
                }
                //else
                //    outXML = HttpContext.Current.Session["Timezones"].ToString();
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                if (xmldoc.SelectSingleNode("//Timezones/selected") != null)//Code added for Error 200
                    sel = xmldoc.SelectSingleNode("//Timezones/selected").InnerText;//Code added for Error 200
                XmlNodeList nodes = xmldoc.SelectNodes("//Timezones/timezones/timezone");
                if (nodes.Count > 0)
                {
                    /* Code Modified For FB 1453- Start */
                    //LoadList(sender, nodes, "timezoneID", "timezoneName");
                    LoadTimeZoneList(sender, nodes, "timezoneID", "timezoneName");
                    /* Code Modified For FB 1453- Start */
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region Get System License Information
        /// <summary>
        /// Get System License Information
        /// </summary>
        public void GetSysLicenseInfo()
        {
            string licenseXml = "";
            try
            {
                remainingRooms = 0;
                remainingUsers = 0;
                remExchangeUsers = 0;
                remDominoUsers = 0;
                remMobileUsers = 0; //FB 1979
                remWebexUsers = 0; //ZD 100221
                remBJNUsers = 0; //ZD 103550
                MaxBJNUsers = 0;
                remainingMCUs = 0;
                remainingEndPoints = 0;
                remainingNVRooms = 0;
                remainingVRooms = 0;
                remainingCatering = 0;
                remainingFacilities = 0;
                remainingHouseKeeping = 0;
                remainingAPI = 0;
                //remainingPC = 0; //FB 2347 //FB 2693
                remainingExtRooms = 0; //FB 2426
                remainingEnchancedMCUs = 0; //FB 2486
                remainingVMRRooms = 0;//FB 2586
                //FB 2693 Starts
                remainingPCUsers = 0;                
                remainingBJ = "0";//ZD 103550
                remainingJabber = 0;
                remainingLync = 0;
                remainingVidtel = 0;
                remainingVidyo = 0;
                //FB 2693 Ends
                remainingAdvReport = 0;//FB 2593
                remainingiControlRooms = 0;//ZD 101098

                licenseXml = CallMyVRMServer("GetLicenseDetails", "", HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (licenseXml.IndexOf("<error>") < 0)
                {
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(licenseXml);
                    XmlNode node = null;

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemainingRooms");
                    string remroom = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemVideoRooms");
                    string remvroom = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemNonVideoRooms");
                    string remnvroom = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemainingMCUs");
                    string remmcu = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemainingEnchancedMCUs");//FB 2486
                    string remenchmcu = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemainingUsers");
                    string remuser = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemExchangeUsers");
                    string remeuser = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemDominoUsers");
                    string remduser = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemMobileUsers"); //FB 1979
                    string remmuser = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemWebexUsers"); //ZD 100221
                    string remWebexuser = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/MaxBlueJeansUsers"); //ZD 103550
                    string MaxBJNuser = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemBlueJeans"); //ZD 103550
                    string remBJNuser = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemainingEndPoints");
                    string remendpt = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemFacilities");
                    string remFacility = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemCatering");
                    string remCat = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemHouseKeeping");
                    string remHK = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemAPI");
                    string remAPI = node.InnerText.Trim();

                    //node = xd.SelectSingleNode("//GetLicenseDetails/RemPC"); //FB 2347 //FB 2693
                    //string remPC = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/MaxOrganizations"); //FB 1639
                    string maxOrg = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemainingGuestRooms"); //FB 2426
                    string remExtRooms = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemVMRRooms");//FB 2586
                    string remvmrrooms = node.InnerText.Trim();

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemiControlRooms");//ZD 101098
                    string remiControlrooms = node.InnerText.Trim();

                    //FB 2599 Start
                    //FB 2262 -J
                    string EnableCloud = "";
                    node = xd.SelectSingleNode("//GetLicenseDetails/Cloud"); //FB 2426
                    if(node != null)
                        EnableCloud = node.InnerText.Trim();
                    //FB 2599 End

                    //FB 2645 Start
                    string EnablePublicRooms = "";
                    node = xd.SelectSingleNode("//GetLicenseDetails/EnablePublicRooms");
                    if (node != null)
                        EnablePublicRooms = node.InnerText.Trim();
                    //FB 2645 End

                    //FB 2693 Starts
                    node = xd.SelectSingleNode("//GetLicenseDetails/RemPCUsers");
                    if(node != null)
                        int.TryParse(node.InnerText.Trim(), out remainingPCUsers);

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemBlueJeans");
                    if (node != null)
                        remainingBJ = node.InnerXml.Trim();//ZD 103550                  

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemJabber");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out remainingJabber);

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemLync");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out remainingLync);

                    node = xd.SelectSingleNode("//GetLicenseDetails/RemVidtel");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out remainingVidtel);
                    //FB 2693 Ends
					//FB 2694 Starts
                    string remVCRooms = "";
                    node = xd.SelectSingleNode("//GetLicenseDetails/RemVCHotRooms");
                    if(node != null)
                        remVCRooms = node.InnerText.Trim();

                    string remRORooms  = "";
                    node = xd.SelectSingleNode("//GetLicenseDetails/RemROHotRooms");
                    if (node != null)
                        remRORooms = node.InnerText.Trim();
                    //FB 2694 End

                    //FB 2593 Start
                    node = xd.SelectSingleNode("//GetLicenseDetails/RemAdvancedReport");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out remainingAdvReport);
                    //FB 2593 End
                    

                    //Remaining License Details
                    Int32.TryParse(remroom, out remainingRooms);
                    Int32.TryParse(remmcu, out remainingMCUs);
                    Int32.TryParse(remuser, out remainingUsers);
                    Int32.TryParse(remeuser, out remExchangeUsers);
                    Int32.TryParse(remduser, out remDominoUsers);
                    Int32.TryParse(remmuser, out remMobileUsers); //FB 1979
                    Int32.TryParse(remWebexuser, out remWebexUsers); //ZD 100221
                    Int32.TryParse(remBJNuser, out remBJNUsers); //ZD 103550
                    Int32.TryParse(MaxBJNuser, out MaxBJNUsers);
                    Int32.TryParse(remendpt, out remainingEndPoints);
                    Int32.TryParse(remvroom, out remainingVRooms);
                    Int32.TryParse(remnvroom, out remainingNVRooms);
                    Int32.TryParse(remFacility, out remainingFacilities);
                    Int32.TryParse(remCat, out remainingCatering);
                    Int32.TryParse(remHK, out remainingHouseKeeping);
                    Int32.TryParse(remAPI, out remainingAPI);
                    //Int32.TryParse(remPC, out remainingPC); //FB 2347 //FB 2693
                    Int32.TryParse(remExtRooms, out remainingExtRooms); //FB 2426
                    Int32.TryParse(remenchmcu, out remainingEnchancedMCUs); //FB 2486
                    Int32.TryParse(remvmrrooms, out remainingVMRRooms); //FB 2586
                    //Maximum License Details
                    Int32.TryParse(maxOrg, out maxOrganizations); //FB 1639
                    Int32.TryParse(EnableCloud, out enableCloud); //FB 2262  //FB 2599
                    Int32.TryParse(EnablePublicRooms, out enablePublicRooms); //FB 2262  //FB 2645
                    int.TryParse(remVCRooms, out remainingVCHotRooms); //FB 2694
                    int.TryParse(remRORooms, out remainingROHotRooms); //FB 2694
                    int.TryParse(remiControlrooms, out remainingiControlRooms); //ZD 101098

                    if (maxOrganizations <= 0)
                        maxOrganizations = 1;

                }
            }
            catch (Exception e)
            {
                log.Trace(e.StackTrace + " : " + e.Message);
            }
        }
        #endregion

        /* Added for Org - End */

        #region Front-end facade Methods

        #region CallCOM2
        /// <summary>
        /// CallCOM2
        /// </summary>
        /// <param name="comCommand"></param>
        /// <param name="inputXML"></param>
        /// <param name="configPath"></param>
        /// <returns></returns>
        public string CallCOM2(string comCommand, string inputXML, string configPath)
        {
            string resultXML = "";
            try
            {
                resultXML = CallCommand(comCommand, inputXML);
                return (resultXML);
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message + " : " + ex.StackTrace);
                return errXML;
            }
        }
        #endregion

        #region CallCOM
        /// <summary>
        /// CallCOM
        /// </summary>
        /// <param name="comCommand"></param>
        /// <param name="inputXML"></param>
        /// <param name="configPath"></param>
        /// <returns></returns>
        public string CallCOM(string comCommand, string inputXML, string configPath)
        {

            try
            {
                string resultXML = "";
                resultXML = CallCommand(comCommand, inputXML);
                return (resultXML);
            }
            catch (Exception ex)
            {
                errXML += ex.Message + " : " + ex.StackTrace;
                return errXML;
            }

        }
        #endregion

        #region CallMyVRMServer
        /// <summary>
        /// CallMyVRMServer
        /// </summary>
        /// <param name="comCommand"></param>
        /// <param name="inputXML"></param>
        /// <param name="configPath"></param>
        /// <returns></returns>
        public string CallMyVRMServer(string comCommand, string inputXML, string configPath)
        {
            try
            {
                string resultXML = "";
                resultXML = CallCommand(comCommand, inputXML);
                return (resultXML);
            }
            catch (Exception ex)
            {
                errXML += " : Error in MyVRMServer : " + ex.Message;
                return (errXML);
            }
        }
        #endregion

        #region Private Members
        private List<String> NetCmds = null;
        private List<String> COMCmds = null;
        private List<String> RTCCommands = null;
        private String ComConfigPath = @"C:\VRMSchemas_v1.8.3\COMConfig.xml";
        private String NetConfigPath = @"C:\VRMSchemas_v1.8.3\";
        private String RTCConfigPath = @"C:\VRMSchemas_v1.8.3\VRMRTCConfig.xml";

        #endregion

        #region CallCommand Method

        /// <summary>
        /// 
        /// </summary>
        /// <param name="configPath"></param>
        /// <param name="operation"></param>
        /// <param name="inXml"></param>
        /// <returns></returns>

        public string CallCommand(string sCommand, string sInputXML)
        {
            string sCmdName = "";
            string sLoginId = "";
            string sPwd = "";
            string resultXML = "";
            ASPIL.VRMServer myvrmCom = null;
            web_com_v18_Net.Com com = null;
            VRMRTC.VRMRTC obj = null;
            string sDektopURL = "";
            string sE164DialNumber = "";
            string sInternalXML = "";
            StringBuilder tdbXML = null, tempinXML = null; //ZD 100694
            int retCode = 0;
            string retMessage = "Success";
            string retDefaultError = "";
            XmlDocument xd = null; //ZD 100518
            int EnableCloudInstallation = 0; //FB 2659
            bool isConfScheduable = true; //ZD 100518
            string confID="";//ZD 100694
            try
            {
                //FB 1888
                //ReplaceJunkCharacters(ref sInputXML, "J", sCommand); //FB 2321
                try
                {
                    if (HttpContext.Current.Application["COM_ConfigPath"] != null)
                        ComConfigPath = HttpContext.Current.Application["COM_ConfigPath"].ToString();
                    if (HttpContext.Current.Application["MyVRMServer_ConfigPath"] != null)
                        NetConfigPath = HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString();
                    if (HttpContext.Current.Application["RTC_ConfigPath"] != null)
                        RTCConfigPath = HttpContext.Current.Application["RTC_ConfigPath"].ToString();
                }
                catch { }

                sCmdName = sCommand;
                if (NetCmds == null || COMCmds == null || RTCCommands == null)
                {
                    GetNetCommands();
                    GetCOMCommands();
                    GetRTCCommands();
                }
                System.Diagnostics.StackTrace st = new System.Diagnostics.StackTrace();
                System.Diagnostics.StackFrame sf = st.GetFrame(1); //To Get calling method
                if (sCmdName != "")
                {
                    if (sCmdName == "GetRoombyMediaService") //FB 2038
                        sCmdName = "GetAvailableRoom";

                    log.Trace(sf.GetMethod().Name.ToUpper() + " Command - " + sCmdName + " InXML - " + sInputXML);
                    if (sCmdName == "TerminateConference" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2"))
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    else if (sCmdName == "ConnectDisconnectTerminal" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2"))
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    else if (sCmdName == "GetHome" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2")) //Login Management
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    else if (sCmdName == "SearchConference" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM")) // MCU report Fixes
                    {
                        com = new web_com_v18_Net.ComClass();
                        resultXML = com.comCentral(sCommand, sInputXML, ComConfigPath);
                    }
                    else if (sCmdName == "SetTerminalControl" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2")) // Extend Time Fix
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    else if (sCmdName == "DisplayTerminal" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2")) // FB 2276
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    else if (sCmdName == "MuteTerminal" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2")) // FB 2530
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    else if (sCmdName == "LockTerminal" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2")) // FB 2501 - Call Monitoring
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    else if (sCmdName == "DeleteTerminal" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2")) // FB 2578
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    else if (sCmdName == "GetMCUProfiles" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2")) // FB 2591
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }   
                    //FB 2441 Starts
                    else if (sCmdName == "MuteAllExcept" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2")) // FB 2530
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    else if (sCmdName == "ConferenceRecording" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2")) // FB 2501 - Call Monitoring
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    //FB 2441 Ends
                    //FB 2556 - Starts
                    else if ((sCmdName == "GetExtMCUSilo" || sCmdName == "GetExtMCUServices") && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2")) // FB 2591
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    else if (sCmdName == "CreateUpdateUserOnRAD" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2"))//ZD 100168 ZD 100890
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    else if(sCmdName == "DeleteUserOnRAD" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2"))//ZD 100755
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
					//FB 2556 - Ends
                    else if (sCmdName == "GetMCUPoolOrders" && sf.GetMethod().Name.ToUpper().Equals("CALLCOM2")) // ZD 104256
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }  
                    else if (NetCmds.Contains(sCmdName))
                    {
                        myvrmCom = new ASPIL.VRMServer();
                        //FB 1881 Error Handling - start

                        try
                        {
                            if (HttpContext.Current.Session["languageid"] != null)
                                int.TryParse(HttpContext.Current.Session["languageid"].ToString(), out loginUsrLanguage);
                        }
                        catch { }

                        if (loginUsrLanguage < 1)
                            loginUsrLanguage = 1; //default language english

                        ASPIL.VRMServer.m_Language = loginUsrLanguage;
                       
                        //ZD 100518 Starts
                        if (sCommand == "SetConference" || sCmdName == "TDScheduleNewConference" || sCmdName == "TDUpdateConference")
                        {
                            try
                            {
                                if (HttpContext.Current.Session["EnableCloudInstallation"] != null)
                                    int.TryParse(HttpContext.Current.Session["EnableCloudInstallation"].ToString(), out EnableCloudInstallation);
                            }
                            catch { }
                             
                            DateTime confStartdatetime = DateTime.Now, confEnddatetime = DateTime.Now;
                            tdbXML = new StringBuilder();
                            int organizationID = 11, mutliOrgID = 0, immediate = 0, tzID = 26, recurring = 0; //default EST
                            string Editconfid = "", sOrgID = "11", StaticID = "", MeetingId = "",EnableStaticID="";//ZD 100890
                            string timeset = "", timehour = "", timemin = "", confdate = "";
                            xd = new XmlDocument();
                            xd.LoadXml(sInputXML);
                            XmlNode node = null;

                            if (sCommand == "SetConference")
                            {
                                if (EnableCloudInstallation == 1)
                                {
                                    node = xd.SelectSingleNode("//conference/organizationID");
                                    if (node != null)
                                        Int32.TryParse(node.InnerXml.Trim(), out organizationID);

                                    node = xd.SelectSingleNode("//conference/multisiloOrganizationID");
                                    if (node != null)
                                        Int32.TryParse(node.InnerXml.Trim(), out mutliOrgID);
                                    if (mutliOrgID > 11)
                                        organizationID = mutliOrgID;

                                    sOrgID = organizationID.ToString();

                                    node = xd.SelectSingleNode("//conference/confInfo/confID");
                                    if (node != null)
                                        Editconfid = node.InnerText.Trim();

                                    if (Editconfid.Contains(','))
                                        Editconfid = Editconfid.Split(',')[0];

                                    node = xd.SelectSingleNode("//conference/confInfo/immediate");
                                    if (node != null)
                                        int.TryParse(node.InnerText, out immediate);
                                    if (immediate < 0)
                                        immediate = 0;

                                    node = xd.SelectSingleNode("//conference/confInfo/recurring");
                                    if (node != null)
                                        int.TryParse(node.InnerText, out recurring);
                                    

                                    node = xd.SelectSingleNode("//conference/confInfo/startDate");
                                    if (node != null)
                                        confdate = node.InnerText.Trim();

                                    node = xd.SelectSingleNode("//conference/confInfo/startHour");
                                    if (node != null)
                                        timehour = node.InnerText.Trim();
                                    if (timehour == "")
                                        timehour = "00";

                                    node = xd.SelectSingleNode("//conference/confInfo/startMin");
                                    if (node != null)
                                        timemin = node.InnerText.Trim();

                                    if (timemin == "")
                                        timemin = "00";

                                    node = xd.SelectSingleNode("//conference/confInfo/startSet");
                                    if (node != null)
                                        timeset = node.InnerText.Trim();
                                    if (timeset == "")
                                        timeset = "AM";

                                    string conftime = timehour + ":" + timemin + ":45 " + timeset;

                                    string confsttime = confdate + " " + conftime;

                                    if (immediate == 1)
                                        confStartdatetime = DateTime.Now;
                                    else
                                        DateTime.TryParse(confsttime, out confStartdatetime);

                                    int durationMin = 0;
                                    node = xd.SelectSingleNode("//conference/confInfo/durationMin");
                                    if (node != null)
                                        int.TryParse(node.InnerText, out durationMin);

                                    confEnddatetime = confStartdatetime.AddMinutes(durationMin);

                                    node = xd.SelectSingleNode("//conference/confInfo/timeZone");
                                    if (node != null)
                                        int.TryParse(node.InnerText, out tzID);

                                    //ZD 100890
                                    node = xd.SelectSingleNode("//conference/confInfo/MeetingId"); 
                                    if (node != null)
                                        MeetingId = node.InnerText.Trim();

                                    node = xd.SelectSingleNode("//conference/confInfo/StaticID"); 
                                    if (node != null)
                                        StaticID = node.InnerText.Trim();
                                    //ZD 100890
                                }
                            }
                            else if (sCommand == "TDScheduleNewConference")
                            {
                                if (xd.SelectSingleNode("//TDScheduleNewConference/organizationID") != null)
                                    sOrgID = xd.SelectSingleNode("//TDScheduleNewConference/organizationID").InnerText.Trim();

                                if (xd.SelectSingleNode("//TDScheduleNewConference/startDate") != null)
                                    DateTime.TryParse(xd.SelectSingleNode("//TDScheduleNewConference/startDate").InnerText.Trim(), out confStartdatetime);

                                if (xd.SelectSingleNode("//TDScheduleNewConference/endDate") != null)
                                    DateTime.TryParse(xd.SelectSingleNode("//TDScheduleNewConference/endDate").InnerText.Trim(), out confEnddatetime);
                                tzID = 33; //UTC 

                                //ZD 100890 Start
                                node = xd.SelectSingleNode("//TDScheduleNewConference/EnableStaticID");
                                if (node != null)
                                    EnableStaticID = node.InnerText.Trim();

                                if(EnableStaticID == "1")
                                    MeetingId = "0";
                                else
                                    MeetingId = "1";

                                node = xd.SelectSingleNode("//TDScheduleNewConference/StaticID");
                                if (node != null)
                                    StaticID = node.InnerText.Trim();
                                //ZD 100890 End

                            }
                            else
                            {
                                if (xd.SelectSingleNode("//TDUpdateConference/organizationID") != null)
                                    sOrgID = xd.SelectSingleNode("//TDUpdateConference/organizationID").InnerText.Trim();

                                if (xd.SelectSingleNode("//TDUpdateConference/startDate") != null)
                                    DateTime.TryParse(xd.SelectSingleNode("//TDUpdateConference/startDate").InnerText.Trim(), out confStartdatetime);

                                if (xd.SelectSingleNode("//TDUpdateConference/endDate") != null)
                                    DateTime.TryParse(xd.SelectSingleNode("//TDUpdateConference/endDate").InnerText.Trim(), out confEnddatetime);

                                if (xd.SelectSingleNode("//TDUpdateConference/conferenceId") != null)
                                    Editconfid = xd.SelectSingleNode("//TDUpdateConference/conferenceId").InnerText.Trim();

                                tzID = 33; //UTC 

                                //ZD 100890 Start
                                node = xd.SelectSingleNode("//TDUpdateConference/EnableStaticID");
                                if (node != null)
                                    EnableStaticID = node.InnerText.Trim();

                                if (EnableStaticID == "1")
                                    MeetingId = "0";
                                else
                                    MeetingId = "1";

                                node = xd.SelectSingleNode("//TDUpdateConference/StaticID");
                                if (node != null)
                                    StaticID = node.InnerText.Trim();
                                //ZD 100890 End

                            }
                            if (recurring == 0)
                            {
                                tdbXML.Append("<CheckConferenceCalls>");
                                tdbXML.Append("<StartDateTime>" + confStartdatetime.ToString() + "</StartDateTime>");
                                tdbXML.Append("<EndDateTime>" + confEnddatetime.ToString() + "</EndDateTime>");
                                tdbXML.Append("<editConfId>" + Editconfid + "</editConfId>");
                                tdbXML.Append("<orgId>" + sOrgID + "</orgId>");
                                tdbXML.Append("<timeZone>" + tzID.ToString() + "</timeZone>");
                                //ZD 100890 
                                tdbXML.Append("<MeetingId>" + MeetingId + "</MeetingId>");
                                tdbXML.Append("<StaticID>" + StaticID + "</StaticID>");
                                //ZD 100890
                                tdbXML.Append("</CheckConferenceCalls>");

                                obj = new VRMRTC.VRMRTC();
                                resultXML = obj.Operations(RTCConfigPath, "CheckConferenceCalls", tdbXML.ToString());
                            }
							//ZD 101431	
                            if (resultXML.IndexOf("<error>") >= 0)
                                isConfScheduable = false;
                        }

                        if (sCmdName == "TDScheduleNewConference" || sCmdName == "TDUpdateConference")
                        {
                            sCommand = "SetConferenceDetails"; //ZD 102578
                        }
                        else if (sCmdName == "TDDeleteConference")
                        {
                            sCommand = "DeleteConference";
                        }
                        else if (sCmdName == "TDGetTemplate") //FB 2659T
                            sCommand = "GetOldTemplate";
                        else if (sCmdName == "TDGetTemplateList")
                            sCommand = "GetTemplateList";

                        
                        if (isConfScheduable)
                        {
                            resultXML = myvrmCom.Operations(NetConfigPath, sCommand, sInputXML);
                            //FB 2392
                            if (sCommand == "SetAdvancedAVSettings")
                            {

                                obj = new VRMRTC.VRMRTC();
                                obj.Operations(RTCConfigPath, "PushtoWhyGO", sInputXML);
                            }
                            //ZD 100694 Starts
                            if (sCommand == "DeleteConference")
                            {
                                xd = new XmlDocument();
                                xd.LoadXml(sInputXML);
                                XmlNode node = null;

                                if (sCommand == "DeleteConference")
                                {
                                    node = xd.SelectSingleNode("//login/delconference/conference/confID");
                                    if (node != null)
                                        confID = node.InnerText.Trim();
                                    tempinXML = new StringBuilder();
                                    tempinXML.Append("<EmailtoWhygoAdmin>");
                                    tempinXML.Append("<ConfID>" + confID + "</ConfID>");
                                    tempinXML.Append("<isDelete>1</isDelete>");
                                    tempinXML.Append("</EmailtoWhygoAdmin>");
                                }

                                obj = new VRMRTC.VRMRTC();
                                obj.Operations(RTCConfigPath, "EmailtoWhygoAdmin", tempinXML.ToString());
                            }
                            //ZD 100694 Ends
                        }
						//ZD 100518 End
                    }
                    else if (COMCmds.Contains(sCmdName))
                    {
                        com = new web_com_v18_Net.ComClass();
                        resultXML = com.comCentral(sCommand, sInputXML, ComConfigPath);
                    }
                    else if (RTCCommands.Contains(sCmdName))
                    {
                        obj = new VRMRTC.VRMRTC();
                        resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                    }
                    else //Commands not registered in either 3 of Command Lists
                    {
                        log.Trace("Command Name - " + sCmdName + " is not in the Command List");
                        if (sf.GetMethod().Name.ToUpper().Equals("CALLCOM2"))
                        {
                            obj = new VRMRTC.VRMRTC();
                            resultXML = obj.Operations(RTCConfigPath, sCommand, sInputXML);
                        }
                        else if (sf.GetMethod().Name.ToUpper().Equals("CALLMYVRMSERVER"))
                        {
                            myvrmCom = new ASPIL.VRMServer();
                            //FB 1881 Error Handling - start

                            try
                            {
                                if (HttpContext.Current.Session["languageid"] != null)
                                    int.TryParse(HttpContext.Current.Session["languageid"].ToString(), out loginUsrLanguage);
                            }
                            catch { }

                            if (loginUsrLanguage < 1)
                                loginUsrLanguage = 1; //default language english

                            ASPIL.VRMServer.m_Language = loginUsrLanguage;
                            //FB 1881 Error Handling - end
                            resultXML = myvrmCom.Operations(NetConfigPath, sCommand, sInputXML);
                        }
                        else if (sf.GetMethod().Name.ToUpper().Equals("CALLCOM"))
                        {
                            com = new web_com_v18_Net.ComClass();
                            resultXML = com.comCentral(sCommand, sInputXML, ComConfigPath);
                        }
                    }
                }
                log.Trace("Command - " + sCmdName + " OutXML - " + resultXML);
				//ZD 101308 Starts	
                if (resultXML != "" && sCmdName == "ChkADAuthentication")
                {
                    String issoMode = "0";
                    XmlNode iNode = null;
                    xd = new XmlDocument();
                    xd.LoadXml(resultXML);
                    iNode = xd.SelectSingleNode("//ChkADAuthentication/IsAD");
                    if (iNode == null)
                        iNode = xd.CreateElement("IsAD");
                    if (HttpContext.Current.Application["ssoMode"] != null && HttpContext.Current.Application["ssoMode"].ToString().ToUpper() == "YES")
                        issoMode = "1";
                    iNode.InnerText = issoMode;
                    xd.DocumentElement.InsertBefore(iNode, xd.SelectSingleNode("//ChkADAuthentication/LDAPServerAddress"));
                    resultXML = xd.InnerXml;

                }
				//ZD 101308 End
                
                
                //FB 2659T Starts
                if (resultXML != "" && (sCmdName == "TDGetTemplate" || sCmdName == "TDGetTemplateList"))
                {
                    if (resultXML.IndexOf("<message>Success</message>") < 0)
                    {
                        retCode = 2;
                        retMessage = "Unhandled exception";
                        retDefaultError = resultXML;

                        tdbXML = new StringBuilder();
                        if (sCmdName == "TDGetTemplate")
                        {
                            tdbXML.Append("<TDGetTemplate>");
                            TDBReturnXML(retCode, retMessage, retDefaultError, ref tdbXML);
                            tdbXML.Append("</TDGetTemplate>");
                        }
                        else if (sCmdName == "TDGetTemplateList")
                        {
                            tdbXML.Append("<TDGetTemplateList>");
                            TDBReturnXML(retCode, retMessage, retDefaultError, ref tdbXML);
                            tdbXML.Append("</TDGetTemplateList>");
                        }

                        if (tdbXML.ToString() != "")
                            resultXML = tdbXML.ToString();
                    }
                }
                //FB 2659T End

                if (resultXML != "" && (sCmdName == "SetInstantConferenceDetails" || sCmdName == "SetConferenceDetails" || sCmdName == "SetAdvancedAVSettings" || sCmdName == "SetApproveConference" || sCommand == "DeleteConference" || sCommand == "ChangeConfMCU")// ZD 100369 MCU Faileover //ZD 100167
                    || (sCmdName == "TDScheduleNewConference" || sCmdName == "TDUpdateConference" || sCommand == "TDDeleteConference" || sCmdName == "ForceConfDelete")) //TerminateConference-Doubt //ZD 100221 //ZD 102195
                {
                    //FB 2659
                    retDefaultError = "<error>";
                    retDefaultError += "<errorCode>-1</errorCode>";
                    retDefaultError += "<message>Success</message>";
                    retDefaultError += "<Description>Operation Successful</Description>";
                    retDefaultError += "<level>S</level>";
                    retDefaultError += "</error>";

                    xd = new XmlDocument();
                    string outXML = "", confid = "", userID = "", editFromWeb = "0", sOrgID = "11", BJNUniqueId = ""; //ZD 103263
                    string inXML = "";
                    int isRPRMConf = 0, isBJNConf = 0, Status = 0, BJNMeetingType = 0, isConfApprovalProcess = 0;//ZD 100959 //ZD 103263 //ZD 104189
                    xd.LoadXml(resultXML);
                    XmlNodeList confs = null;
                    XmlNode Node = null;
                    XmlDocument Xdocsync = new XmlDocument(); //ZD 100221
					int PushtoGoogle = 0; //ZD 100152

                    if (xd.SelectNodes("//conferences/conference") != null)
                        confs = xd.SelectNodes("//conferences/conference");

                    if (confs != null)
                    {
                        //ZD 100152 Starts
                        if (xd.SelectSingleNode("/conferences/conference/ExporttoExternal/PushtoGoogle") != null)
                            int.TryParse(xd.SelectSingleNode("/conferences/conference/ExporttoExternal/PushtoGoogle").InnerText.Trim(), out PushtoGoogle);
                        if (PushtoGoogle == 1)
                        {
                            MyVRMNet.GoogleUtil GoogleUtil = new MyVRMNet.GoogleUtil();
                            if (sCommand == "DeleteConference" || sCommand=="ForceConfDelete")
                                GoogleUtil.DeleteEvent(resultXML);
                            else
                            {
                                outXML = GoogleUtil.ScheduleEvent(resultXML);
                                obj = new VRMRTC.VRMRTC();
                                outXML = obj.Operations(RTCConfigPath, "SaveGoolgeUID", outXML); //ZD 102980
                            }
                        }
                        //ZD 100152 Ends
                        if (xd.SelectSingleNode("//conferences/sCommand") != null)
                            sCommand = xd.SelectSingleNode("//conferences/sCommand").InnerText.Trim();
                        for (int i = 0; i < confs.Count; i++)
                        {
                            Node = confs[i];

                            if (Node.SelectSingleNode("confID") != null) // ZD 100221
                                confid = Node.SelectSingleNode("confID").InnerText.Trim();

                            if (Node.SelectSingleNode("UserID") != null)
                                userID = Node.SelectSingleNode("UserID").InnerText.Trim();

                            if (Node.SelectSingleNode("editFromWeb") != null)
                                editFromWeb = Node.SelectSingleNode("editFromWeb").InnerText.Trim();

                            //ZD 100959
                            if (Node.SelectSingleNode("confbrdige/isRPRMConf") != null)
                                int.TryParse(Node.SelectSingleNode("confbrdige/isRPRMConf").InnerText.Trim(), out isRPRMConf);

                            //ZD 103263 Start
                            if (Node.SelectSingleNode("isBJNConf") != null)
                                int.TryParse(Node.SelectSingleNode("isBJNConf").InnerText.Trim(), out isBJNConf);

                            if (Node.SelectSingleNode("Status") != null)
                                int.TryParse(Node.SelectSingleNode("Status").InnerText.Trim(), out Status);

                            if (Node.SelectSingleNode("BJNUniqueId") != null)
                                BJNUniqueId = Node.SelectSingleNode("BJNUniqueId").InnerText.Trim();
							// ZD 103550 Start
                            if (Node.SelectSingleNode("BJNMeetingType") != null)
                                int.TryParse(Node.SelectSingleNode("BJNMeetingType").InnerText.Trim(), out BJNMeetingType);

                            if (Node.SelectSingleNode("isConfApprovall") != null) //ZD 104189
                                int.TryParse(Node.SelectSingleNode("isConfApprovall").InnerText.Trim(), out isConfApprovalProcess);

                            if ((isBJNConf == 1 || BJNUniqueId != "") && Status != 1)
                            {
                                if (BJNMeetingType == 2) //One Time Meeting
                                {
                                    obj = new VRMRTC.VRMRTC();
                                    switch (sCommand)
                                    {
                                        case "SetConferenceOnMcu":
                                            inXML = "<Conference><confID>" + confid + "</confID></Conference>";
                                            break;

                                        case "TerminateConference":
                                            inXML = "<login><conferenceID>" + confid + "</conferenceID></login>";
                                            break;
                                    }
                                    outXML = obj.Operations(RTCConfigPath, sCommand, inXML);
                                }
								// ZD 103550 End
                                if (sCommand == "SetConferenceOnMcu" && isBJNConf == 1)
                                {
                                    inXML = "<Conference><confID>" + confid + "</confID><editFromWeb>" + editFromWeb + "</editFromWeb><isConfApprovall>" + isConfApprovalProcess + "</isConfApprovall></Conference>";  //ZD 104189
                                    sInternalXML = myvrmCom.Operations(NetConfigPath, "SendBJNBridgeemails", inXML);
                                }
                            }
                            //ZD 103263 End

                            if (Node.SelectSingleNode("confbrdige/isSynchronous") != null)
                            {
                                if (Node.SelectSingleNode("confbrdige/isSynchronous").InnerText.Trim() == "1")
                                {
                                    obj = new VRMRTC.VRMRTC();
                                    if (Node.SelectSingleNode("confID") != null)
                                        confid = Node.SelectSingleNode("confID").InnerText.Trim();

                                    if (Node.SelectSingleNode("UserID") != null)
                                        userID = Node.SelectSingleNode("UserID").InnerText.Trim();

                                    if (Node.SelectSingleNode("editFromWeb") != null)
                                        editFromWeb = Node.SelectSingleNode("editFromWeb").InnerText.Trim();

                                    
                                    switch (sCommand)
                                    {
                                        case "SetConferenceOnMcu":
                                            inXML = "<Conference><confID>" + confid + "</confID></Conference>";
                                            break;

                                        case "TerminateConference":
                                            inXML = "<login><conferenceID>" + confid + "</conferenceID></login>";
                                            break;


                                    }
                                    if (!(Node.SelectSingleNode("confbrdige/isSynchronous").InnerText.Trim() == "1" && isRPRMConf == 1)) //ZD 104520 
                                        outXML = obj.Operations(RTCConfigPath, sCommand, inXML);

                                    
                                    if (outXML.IndexOf("<error>") < 0 || isRPRMConf == 1) //ZD 100959
                                    {
                                        try
                                        {
                                            if (HttpContext.Current.Session["EnableCloudInstallation"] != null)
                                                int.TryParse(HttpContext.Current.Session["EnableCloudInstallation"].ToString(), out EnableCloudInstallation);
                                        }
                                        catch { }

                                        if (EnableCloudInstallation == 1 || sCmdName == "TDScheduleNewConference" || sCmdName == "TDUpdateConference" || sCommand == "TDDeleteConference" || isRPRMConf == 1)//FB 2659T //ZD 100959
                                        {
                                            inXML = "<Conference><UserID>" + userID + "</UserID><confID>" + confid + "</confID><editFromWeb>" + editFromWeb + "</editFromWeb><IsConfApproved>" + isRPRMConf + "</IsConfApproved></Conference>";

                                            if (sCmdName == "SetInstantConferenceDetails" || sCmdName == "SetAdvancedAVSettings" || sCmdName == "SetConferenceDetails" || sCmdName == "TDScheduleNewConference" || sCmdName == "TDUpdateConference") //ZD 100167 //ZD 100959 //ZD 102195 //ZD 102985 - 
                                                sInternalXML = myvrmCom.Operations(NetConfigPath, "SendSynchronousBridgeemails", inXML);

                                            if (sInternalXML != "")
                                            {
                                                //ZD 100221 Starts
                                                Xdocsync = new XmlDocument(); 
                                                Xdocsync.LoadXml(sInternalXML);

                                                if (Xdocsync.SelectSingleNode("//success/externalId") != null)
                                                    sE164DialNumber = Xdocsync.SelectSingleNode("//success/externalId").InnerText;

                                                if (Xdocsync.SelectSingleNode("//success/conferenceUrl") != null)
                                                    sDektopURL = Xdocsync.SelectSingleNode("//success/conferenceUrl").InnerText;
                                                //ZD 100221 Ends
                                            }
                                        }
                                    }
                                }
                            }
							//FB 2441 II Starts
							if (Node.SelectSingleNode("Deleted") != null)
                            {
                                  obj = new VRMRTC.VRMRTC();
                                  if (Node.SelectSingleNode("confID") != null)
                                      confid = Node.SelectSingleNode("confID").InnerText.Trim();
                                  sInputXML = "<login><conferenceID>" + confid + "</conferenceID></login>";
                                  outXML = obj.Operations(RTCConfigPath, "TerminateSyncConference", sInputXML);
                            }
							//FB 2441 II Starts
                        }
                        //ZD 100221 Email Starts
                        if (xd.SelectSingleNode("//conferences/conference/WebEXIntegration/EnableWebEXMeeting") != null)
                        {
                            isRPRMConf = 0;
                            if (xd.SelectSingleNode("//conferences/conference/WebEXIntegration/isRPRMConf") != null)
                                int.TryParse(xd.SelectSingleNode("//conferences/conference/WebEXIntegration/isRPRMConf").InnerText, out isRPRMConf);

                            obj = new VRMRTC.VRMRTC();

                            if (sCommand == "SetConferenceOnMcu")
                            {
                                outXML = obj.Operations(RTCConfigPath, "ScheduleWebEXMeeting", resultXML);
                                inXML = "<Conference><UserID>" + userID + "</UserID><confID>" + confid + "</confID><editFromWeb>" + editFromWeb + "</editFromWeb></Conference>";
                                if (isRPRMConf == 0) //ZD 104812
                                    sInternalXML = myvrmCom.Operations(NetConfigPath, "SendSynchronousBridgeemails", inXML);

                            }
                            else
                                outXML = obj.Operations(RTCConfigPath, "DeleteWebEXMeeting", resultXML);
                        }
                        //ZD 100221 Email Ends
                    }
                    if ((sCmdName == "SetInstantConferenceDetails" && resultXML.IndexOf("<error>") < 0)) //FB 2674 ZD 100167 //ZD 102195
                        resultXML = "<success><externalId>" + sE164DialNumber + "</externalId><conferenceUrl>" + sDektopURL + "</conferenceUrl></success>";
                    
                    if ((sCmdName == "SetAdvancedAVSettings" || sCmdName == "SetApproveConference") && resultXML.IndexOf("<error>") < 0) //FB 2674
                    {
                       
                         //ZD 101971 Starts
                        XmlDocument XMLLO = new XmlDocument();
                        XMLLO.LoadXml(resultXML);
                        int LOAlert = 0;
                        if (XMLLO.SelectSingleNode("//conferences/AlertLOChange") != null)
                            int.TryParse(XMLLO.SelectSingleNode("//conferences/AlertLOChange").InnerText, out LOAlert);

                        resultXML = "<success>1," + LOAlert + "</success>";
                        //resultXML = "<success>1</success>";
                        //ZD 101971 Ends
                    }
                    //FB 2659 - Starts

                    if (resultXML.IndexOf("<error>") >= 0)
                    {
                        retCode = 2;
                        retMessage = "Unhandled exception";//resultXML
                        retDefaultError = resultXML;
                        //retMessage = ShowErrorMessage(resultXML);
                    }
                    tdbXML = new StringBuilder();
                    if (sCmdName == "TDDeleteConference")
                    {
                        tdbXML.Append("<TDDeleteConference>");
                        TDBReturnXML(retCode, retMessage, retDefaultError, ref tdbXML);
                        tdbXML.Append("</TDDeleteConference>");
                    }
                    else if (sCmdName == "TDScheduleNewConference")
                    {
                        tdbXML.Append("<TDScheduleNewConference>");
                        tdbXML.Append("<conferenceId>" + confid + "</conferenceId>");
                        tdbXML.Append("<externalId>" + sE164DialNumber + "</externalId>");
                        tdbXML.Append("<conferenceUrl>" + sDektopURL + "</conferenceUrl>");
                        TDBReturnXML(retCode, retMessage, retDefaultError, ref tdbXML);
                        tdbXML.Append("</TDScheduleNewConference>");
                    }
                    else if (sCmdName == "TDUpdateConference")
                    {
                        tdbXML.Append("<TDUpdateConference>");
                        tdbXML.Append("<conferenceId>" + confid + "</conferenceId>");
                        tdbXML.Append("<externalId>" + sE164DialNumber + "</externalId>");
                        tdbXML.Append("<conferenceUrl>" + sDektopURL + "</conferenceUrl>");
                        TDBReturnXML(retCode, retMessage, retDefaultError, ref tdbXML);
                        tdbXML.Append("</TDUpdateConference>");
                    }
                    if (tdbXML.ToString() != "")
                        resultXML = tdbXML.ToString();
                    //FB 2659 - End
                }

                //FB 2441 Ends

                return (resultXML);
            }
            catch (Exception e)
            {
                log.Trace("sytemException occured - " + e.Message);
                log.Trace("Command OutXML - " + resultXML);
                return (resultXML);
            }

        }
        private void ProcessPublicRoomCalendar(ref String inXML, ref String outXML)
        {
            String resultXML = "";
            String tempXML = "";
            XmlDocument xmlDoc = null;
            VRMRTC.VRMRTC obj = null;
            try
            {

                obj = new VRMRTC.VRMRTC();
                tempXML = obj.Operations(RTCConfigPath, "GetLocationAvailability", inXML);
                if (!tempXML.Contains("<days>"))
                    tempXML = "";

                if (tempXML.Trim() != "")
                {
                    xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(outXML);
                    if (xmlDoc.SelectSingleNode("/monthlyView") != null)
                        xmlDoc.SelectSingleNode("/monthlyView").InnerXml = xmlDoc.SelectSingleNode("/monthlyView").InnerXml + tempXML;

                    outXML = xmlDoc.OuterXml;
                }

            }
            catch (Exception e)
            {
                log.Trace("sytemException occured - " + e.Message);
                log.Trace("Command OutXML - " + tempXML);

            }
        }

        private void ProcessWhygoApproval(ref String inXML)
        {
            String resultXML = "";
            String tempXML = "";
            XmlDocument xmlDoc = null;
            VRMRTC.VRMRTC obj = null;
            try
            {
                xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(inXML);
                if (xmlDoc.SelectSingleNode("/ExternalPortal") != null && int.Parse(xmlDoc.SelectSingleNode("/ExternalPortal").InnerText.Trim()) == (int)ExternalPortal.WhyGo)
                {
                    obj = new VRMRTC.VRMRTC();
                    tempXML = obj.Operations(RTCConfigPath, "UpdateConferenceApproval", inXML);
                }

            }
            catch (Exception e)
            {
                log.Trace("sytemException occured - " + e.Message);
                log.Trace("Command OutXML - " + tempXML);

            }
        }
        #endregion

        #region GetNetCommands

        private List<String> GetNetCommands()
        {
            try
            {
                NetCmds = new List<String>();
                NetCmds.Add("BlockAllUsers");
                NetCmds.Add("ChangeGuestStatus");
                NetCmds.Add("ChangeUserStatus");
                NetCmds.Add("CheckAPIEnable");
                NetCmds.Add("CheckUserCredentials");
                NetCmds.Add("ChkUsrAuthentication");
                NetCmds.Add("ConferenceReports");
                NetCmds.Add("ConfirmConferenceInvitation");
                NetCmds.Add("ConvertFromGMT");
                NetCmds.Add("ConvertToGMT");
                NetCmds.Add("DeleteAllData");
                NetCmds.Add("DeleteCategoryItem");
                NetCmds.Add("DeleteConferenceReport");
                NetCmds.Add("DeleteConfsAttributeByID");
                NetCmds.Add("DeleteCiscoICAL");    //Cisco ICAL FB 1602
                NetCmds.Add("DeleteCustomAttribute");
                NetCmds.Add("DeleteEndpoint");
                NetCmds.Add("DeleteInventory");
                NetCmds.Add("DeleteLDAPUser");
                NetCmds.Add("DeleteOrganizationProfile");
                NetCmds.Add("DeletePastConference");
                NetCmds.Add("DeleteProviderMenu");
                NetCmds.Add("DeleteSearchTemplate");
                NetCmds.Add("DeleteTier1");
                NetCmds.Add("DeleteTier2");
                NetCmds.Add("DeleteUserTemplate");
                NetCmds.Add("DeleteWorkOrder");
                NetCmds.Add("Feedback");
                NetCmds.Add("FetchBridgeInfo");
                NetCmds.Add("GetAllEndpoints");
                NetCmds.Add("GenerateReport");
                NetCmds.Add("GetActivation");
                NetCmds.Add("GetActiveOrgDetails");
                NetCmds.Add("GetAddressType");
                NetCmds.Add("GetAdvancedAVSettings");
                NetCmds.Add("GetAllOrgSettings");
                NetCmds.Add("GetAllManageUser");
                NetCmds.Add("GetAllRooms");
                NetCmds.Add("GetAllRoomsBasicInfo"); //FB 1756
                NetCmds.Add("GetAllRoomsInfo"); //ZD 100563
                NetCmds.Add("GetAllRoomsInfoforCache"); //ZD 103790
                NetCmds.Add("GetAllRoomsInfoOpt"); //ZD 104482
                NetCmds.Add("GetAllTimezones");
                NetCmds.Add("GetAudioCodecs");
                NetCmds.Add("GetBridge");
                NetCmds.Add("GetBridgeList");
                NetCmds.Add("GetBridges");
                NetCmds.Add("GetBusyRooms");
                NetCmds.Add("GetCateringServices");
                NetCmds.Add("GetConfBaseDetails");
                NetCmds.Add("GetConferenceEndpoint");
                NetCmds.Add("GetConferenceReportList");
                NetCmds.Add("GetConfListByCustOptID");
                NetCmds.Add("GetCountryCodes");
                NetCmds.Add("GetCountryStates");
                NetCmds.Add("GetCustomAttributeByID");
                NetCmds.Add("GetCustomAttributeDescription");
                NetCmds.Add("GetCustomAttributes");
                NetCmds.Add("GetDeactivatedRooms");
                NetCmds.Add("GetDeliveryTypes");
                NetCmds.Add("GetDepartmentsForLocation");
                NetCmds.Add("GetDialingOptions");
                NetCmds.Add("GetEncrpytedText");
                NetCmds.Add("GetDecrpytedText"); //ZD 100152
                NetCmds.Add("GetEndpointDetails");
                NetCmds.Add("GetEndpointStatus");
                NetCmds.Add("GetIfDirty");
                NetCmds.Add("GetIfDirtyorPast");
                NetCmds.Add("GetInputParameters");
                NetCmds.Add("GetInventoryDetails");
                NetCmds.Add("GetInventoryList");
                NetCmds.Add("GetItemsList");
                NetCmds.Add("GetLastMailRunDate"); //DashBoard
                NetCmds.Add("GetLDAPUsers");
                NetCmds.Add("GetLicenseDetails");
                NetCmds.Add("GetLineRate");
                NetCmds.Add("GetLocations");
                NetCmds.Add("GetLocations2");
                NetCmds.Add("GetLocationsList");
                NetCmds.Add("GetLocationsUsage");
                NetCmds.Add("GetManageDepartment");
                NetCmds.Add("GetMCUAvailableResources");
                NetCmds.Add("GetMCUCards");
                NetCmds.Add("GetMCUUsage");
                NetCmds.Add("GetMediaTypes");
                NetCmds.Add("GetMultipleUsers");
                NetCmds.Add("GetNewBridge");
                NetCmds.Add("GetOldBridge");
                NetCmds.Add("GetOldScheduledReport");
                NetCmds.Add("GetOrganizationList");
                NetCmds.Add("GetOrganizationProfile");
                NetCmds.Add("GetOrgOptions");
                NetCmds.Add("GetOrgSettings");
                NetCmds.Add("GetProviderDetails");
                NetCmds.Add("GetProviderWorkorderDetails");
                NetCmds.Add("GetPurgeDetails");
                NetCmds.Add("GetReportTypeList");
                NetCmds.Add("GetRoomBasicDetails");
                NetCmds.Add("GetRoomLicenseDetails");
                NetCmds.Add("GetRoomProfile");
                NetCmds.Add("GetRoomsDepts");
                NetCmds.Add("GetRoomSets");
                NetCmds.Add("GetRoomTimeZoneMapping");
                NetCmds.Add("GetSearchTemplate");
                NetCmds.Add("GetSpecificUsageMonth");
                NetCmds.Add("GetSQLServerInfo");
                NetCmds.Add("GetSysMailData");   //RSS Fix
                NetCmds.Add("GetTimezones");
                NetCmds.Add("GetTotalUsageMonth");
                NetCmds.Add("GetUserTemplateDetails");
                NetCmds.Add("GetUserTemplateList");
                NetCmds.Add("GetVideoCodecs");
                NetCmds.Add("GetVideoEquipment");
                NetCmds.Add("GetManufacturer");//ZD 100736
                NetCmds.Add("GetManufacturerModel");//ZD 100736
                NetCmds.Add("GetVideoModes");
                NetCmds.Add("GetVideoProtocols");
                NetCmds.Add("GetWorkOrderDetails");
                NetCmds.Add("IsCustomAttrLinkedToConf");
                NetCmds.Add("runReport");
                NetCmds.Add("SearchConference");
                NetCmds.Add("SearchAllConference");  //Fix
                NetCmds.Add("SearchConferenceWorkOrders");
                NetCmds.Add("SearchEndpoint");
                NetCmds.Add("SearchProviderMenus");
                NetCmds.Add("SearchRooms");
                NetCmds.Add("SendWorkOrderReminder");
                NetCmds.Add("SetAdvancedAVSettings");
                NetCmds.Add("SetBridge");
                NetCmds.Add("SetBulkUserUpdate");
                NetCmds.Add("SetCategoryItem");
                NetCmds.Add("SetConferenceEndpoint");
                NetCmds.Add("SetConferenceWorkOrders");
                NetCmds.Add("SetCustomAttribute");
                NetCmds.Add("SetEndpoint");
                NetCmds.Add("SetInventoryDetails");
                NetCmds.Add("SetMultipleUsers");
                NetCmds.Add("SetOrgSettings");
                NetCmds.Add("SetOrgOptions");
                NetCmds.Add("SetProviderDetails");
                NetCmds.Add("SetProviderWorkorderDetails");
                NetCmds.Add("SetPurgeDetails");
                NetCmds.Add("SetRoomProfile");
                NetCmds.Add("SetSearchTemplate");
                NetCmds.Add("SetTemplateOrder");
                NetCmds.Add("SetTier1");
                NetCmds.Add("SetTier2");
                NetCmds.Add("SetUserTemplate");
                NetCmds.Add("SetOrganizationProfile");
                NetCmds.Add("UpdateManageDepartment");
                NetCmds.Add("UnBlockAllUsers");
                NetCmds.Add("DeleteImage");
                //Site Logo...
                NetCmds.Add("SetImagekey");
                NetCmds.Add("GetSiteImage");
                NetCmds.Add("DeleteSiteImage");

                NetCmds.Add("UpdateOrgImages");  //CSS Module
                NetCmds.Add("SetTextChangeXML");
                NetCmds.Add("SetDefaultCSSXML");
                NetCmds.Add("GetOrgImages"); //Login Management
                NetCmds.Add("GetAudioUserList");//Audio UserList
                NetCmds.Add("DeleteRecurInstance"); //FB 1772
                NetCmds.Add("DelRecurInstanceByUID"); //FB 1772
                NetCmds.Add("EditRecurInstance"); //FB 1772
                NetCmds.Add("EditRecurInstanceByUID"); //FB 1772
                NetCmds.Add("CreateCiscoICALOnApproval"); //FB 1643
                NetCmds.Add("Isconferenceschedulable");
                NetCmds.Add("DeleteParticipantICAL");//FB 1782
                NetCmds.Add("CreateParticipantICALOnApproval");//FB 1782
                NetCmds.Add("UpdateICalID");//FB 1782
                NetCmds.Add("GetImages");//FB 1756
                //FB 1830 start
                NetCmds.Add("CheckApprovalEntity");
                NetCmds.Add("GetEmailTypes");
                NetCmds.Add("GetEmailContent");
                NetCmds.Add("SetEmailContent");
                NetCmds.Add("GetEmailLanguages");
                NetCmds.Add("GetLanguages");
                NetCmds.Add("DeleteEmailLanguage");
                NetCmds.Add("RequestPassword");
                NetCmds.Add("SendPartyReminder");
                NetCmds.Add("RequestVRMAccount");
                //FB 1830 end

                //FB 1860
                NetCmds.Add("GetEmailsBlockStatus");
                //NetCmds.Add("GetUserEmailsBlockStatus");FB 2027
                NetCmds.Add("GetUserEmails");
                NetCmds.Add("GetOrgEmails");
                NetCmds.Add("SetUserEmailsBlockStatus");
                NetCmds.Add("SetBlockEmail");
                NetCmds.Add("DeleteEmails");

                //FB 1861
                NetCmds.Add("GetOrgHolidays");
                NetCmds.Add("SetOrgHolidays");

                NetCmds.Add("GetCompleteMCUusageReport"); //FB 1938
                NetCmds.Add("FetchUserLogin"); //FB 1969
                NetCmds.Add("SetPreferedRoom"); //FB 1959
                NetCmds.Add("GetPreferedRoom"); //FB 1959
                //FB 2027 - Start
                NetCmds.Add("DeleteTerminal");
                NetCmds.Add("DeleteBridge");
                NetCmds.Add("GetEndpoint");
                NetCmds.Add("PartyInvitation");
                NetCmds.Add("GetTemplateList");
                NetCmds.Add("GetLogPreferences");
                NetCmds.Add("SetUserRoles");
                NetCmds.Add("SearchUserOrGuest");
                NetCmds.Add("SetBridgeList");
                NetCmds.Add("TerminateConference");
                NetCmds.Add("GetFeedback");
                NetCmds.Add("RetrieveUsers");
                NetCmds.Add("RetrieveGuest");
                NetCmds.Add("GetManageUser");
                NetCmds.Add("GetManageGuest");
                NetCmds.Add("GetUsers");
                NetCmds.Add("SetOldUser");
                NetCmds.Add("GetSearchTemplateList");
                NetCmds.Add("GetUserPreference");
                NetCmds.Add("DeleteTemplate");
                NetCmds.Add("DeleteAllGuests");
                NetCmds.Add("GetAllocation");
                NetCmds.Add("SetGroup");
                NetCmds.Add("GetGroup");
                NetCmds.Add("SearchGroup");
                NetCmds.Add("SetUserStatus");
                NetCmds.Add("GuestRegister");
                NetCmds.Add("CounterInvite");
                NetCmds.Add("DisplayTerminal");
                NetCmds.Add("MuteTerminal");
                NetCmds.Add("DeleteConference");
                NetCmds.Add("GetUserRoles");
                NetCmds.Add("DeleteGroup");
                NetCmds.Add("GetRoomMonthlyView");
                NetCmds.Add("GetRoomWeeklyView");
                NetCmds.Add("GetRoomDailyView");
                NetCmds.Add("SetSuperAdmin");
                NetCmds.Add("GetEmailList");
                NetCmds.Add("GetGuestList");
                NetCmds.Add("GetNewTemplate");
                NetCmds.Add("GetSearchConference");
                NetCmds.Add("GetNewConference");
                NetCmds.Add("GetApprovalStatus");
                NetCmds.Add("GetManageBulkUsers");
                NetCmds.Add("GetTerminalControl");
                NetCmds.Add("GetOldRoom");
                NetCmds.Add("GetSettingsSelect");
                NetCmds.Add("GetOldTemplate");
                NetCmds.Add("GetSystemDateTime");
                NetCmds.Add("GetConfGMTInfo");
                NetCmds.Add("SetBulkUserAddMinutes");
                NetCmds.Add("SetBulkUserBridge");
                NetCmds.Add("SetBulkUserDelete");
                NetCmds.Add("SetBulkUserDepartment");
                NetCmds.Add("SetBulkUserExpiryDate");
                NetCmds.Add("SetBulkUserLanguage");
                NetCmds.Add("SetBulkUserLock");
                NetCmds.Add("SetBulkUserRole");
                NetCmds.Add("SetBulkUserTimeZone");
                NetCmds.Add("SearchLog");
                NetCmds.Add("GetOldUser");
                NetCmds.Add("ManageConfRoom");
                NetCmds.Add("SetDynamicUser");
                NetCmds.Add("GetOldConference");
                NetCmds.Add("SetTerminalControl");
                NetCmds.Add("SetUser");
                NetCmds.Add("GetAvailableRoom");
                NetCmds.Add("GetTemplate");
                NetCmds.Add("ResponseInvite");
                NetCmds.Add("GetInstances");
                NetCmds.Add("GetSuperAdmin");
                NetCmds.Add("GetApproveConference");
                NetCmds.Add("SetTemplate");
                NetCmds.Add("SetApproveConference");
                NetCmds.Add("GetHome");
                NetCmds.Add("SetConference"); //FB 2027 SetConference
                NetCmds.Add("GetRecurDateList"); //FB 2027 SetConference
                NetCmds.Add("GetIconsReference"); //NewLobby start
                NetCmds.Add("SetIconReference");
                NetCmds.Add("GetUserLobbyIcons");
                NetCmds.Add("SetUserLobbyIcons"); //NewLobby End
                NetCmds.Add("GetLanguageTexts");
                NetCmds.Add("GetLastModifiedRoom");//FB 2149
				NetCmds.Add("DeleteModuleLog");
                NetCmds.Add("DeleteRoom");//FB 2027 DeleteRoom
                NetCmds.Add("ActiveRoom");//FB 2027 ActiveRoom
				NetCmds.Add("McuUsage");//FB 2027 McuUsage
                NetCmds.Add("GetOrgEmailDomains"); //FB 2154
                NetCmds.Add("SaveEmailDomain"); //FB 2154
                NetCmds.Add("SaveFiles"); //FB 2153
                //FB 2027 - End
                NetCmds.Add("GetUsageReports");//FB 2155
                NetCmds.Add("SetBridgenumbers");//FB 2227
                NetCmds.Add("SetEntityCode"); //FB 2045 - Starts
                NetCmds.Add("UpdateEntityCode");
                NetCmds.Add("GetEntityCode");
                NetCmds.Add("DeleteEntityCode");
                NetCmds.Add("GetConfListByEntityOptID");
                NetCmds.Add("EditConfEntityOption");
                NetCmds.Add("DeleteConfEntityOptionID"); //FB 2045 - End
				NetCmds.Add("GetServiceType");//FB 2219
				//FB 2052
                NetCmds.Add("GetHolidayType");
                NetCmds.Add("DeleteHolidayDetails");
				NetCmds.Add("PurgeOrganization");//FB 2074
                NetCmds.Add("HelpRequestMail");//FB 2268
                NetCmds.Add("SetPhoneNumber");//FB 2268
                NetCmds.Add("GetRoombyMediaService");//FB 2038
				NetCmds.Add("GetMCCReport"); //FB 2047
                NetCmds.Add("GetEntityCodes");//ZD 102909
                NetCmds.Add("GetAllDepartments"); //FB 2047
                //FB 2136 Starts
                NetCmds.Add("GetSecurityBadgeType");
                NetCmds.Add("GetAllSecImages");
                NetCmds.Add("GetSecurityImage");
                NetCmds.Add("SetSecurityImage");
                NetCmds.Add("DeleteSecurityImage");
                NetCmds.Add("GetSecImages"); 
                //FB 2136 Ends
				NetCmds.Add("GetOrgLicenseAgreement"); //FB 2337
                NetCmds.Add("SetOrgLicenseAgreement"); //FB 2337
                //FB 2343 Start
                NetCmds.Add("GetMonthlydays");
                NetCmds.Add("SetMonthlydays");
                NetCmds.Add("GetWeeklydays");
                NetCmds.Add("SetWeeklydays");
                NetCmds.Add("Setdays");
                //FB 2343 End
				NetCmds.Add("GetRoomDetails"); //FB 2361
                NetCmds.Add("GenerateESUserReport"); //FB 2363
                NetCmds.Add("GetRoomQueue");
                NetCmds.Add("UpdateCancelEvents"); //FB 2363
                NetCmds.Add("GenerateESErrorReport"); //FB 2363
                NetCmds.Add("GetConfMCUDetails");//FB 2448
                //FB 2486
                NetCmds.Add("GetAllMessage");
                NetCmds.Add("SetMessage");
                NetCmds.Add("DeleteMessage");
                NetCmds.Add("GetConfMsg");
                NetCmds.Add("SetOrgTextMsg");
                //FB 2501 Call Monitoring Start
                NetCmds.Add("GetCallsforMonitor");
                NetCmds.Add("GetPacketLossTerminal");
                NetCmds.Add("LockTerminal");
                NetCmds.Add("MonitorMuteTerminal");
                //FB 2501 Call Monitoring End
                //FB 2501 p2p Call Monitoring Start
                NetCmds.Add("GetP2PCallsforMonitor");
                NetCmds.Add("SetP2PCallerEndpoint");
                NetCmds.Add("SetP2PCallerLinerate");
                //NetCmds.Add("GetEventLog");Commented for FB 2569
                //FB 2501 p2p Call Monitoring End
			    NetCmds.Add("SetBatchReportConfig"); //FB 2410
                NetCmds.Add("GetAllBatchReports"); //FB 2410
                NetCmds.Add("GenerateBatchReport"); //FB 2410
                NetCmds.Add("DeleteBatchReport"); //FB 2410
                NetCmds.Add("GetMCUProfiles");//FB 2591 
				NetCmds.Add("SetPrivatePublicRoomProfile"); //FB 2392                
                NetCmds.Add("GetPrivatePublicRoomProfile"); //whygo T
                NetCmds.Add("GetPrivatePublicRoomID"); //whygo T
				NetCmds.Add("GetConfMCUDetails");//FB 2448
                NetCmds.Add("GetConfAvailableRoom");//FB 2392
                NetCmds.Add("UserAuthentication");//FB 2558-WhyGo
                NetCmds.Add("DeltePublicRoomEP");//FB 2594
                NetCmds.Add("GetCallsForSwitching");//FB 2595 
				NetCmds.Add("ConferenceRecording"); // FB 2441
                NetCmds.Add("MuteUnMuteParticipants"); //FB 2441
				NetCmds.Add("GetCallsforFMS");//FB 2616
                NetCmds.Add("KeepAlive");//FB 2616
				NetCmds.Add("GetVNOCUserList"); //FB 2670
                NetCmds.Add("GetPCDetails");//FB 2693
                NetCmds.Add("FetchSelectedPCDetails");//FB 2693
				NetCmds.Add("GetExtMCUSilo"); //FB 2556
                NetCmds.Add("GetExtMCUServices"); //FB 2556
				NetCmds.Add("SetLeaderPerson"); //FB 2553
                NetCmds.Add("SetLectureParty"); //FB 2553
                //FB 2724 Start
                NetCmds.Add("GetRoomConferenceMonthlyView");
                NetCmds.Add("RoomValidation");
                NetCmds.Add("GetModifiedConference");
                NetCmds.Add("SetConferenceSignin");
                //FB 2724 End
                //FB 2659 Start
                NetCmds.Add("SetDefaultLicense");
                NetCmds.Add("GetDefaultLicense");
                NetCmds.Add("TDGetAvailableSeats"); 
                NetCmds.Add("TDGetMaximumAvailableSeats"); 
                NetCmds.Add("SendSynchronousBridgeemails"); 
                NetCmds.Add("TDScheduleNewConference");
                NetCmds.Add("TDUpdateConference");
                NetCmds.Add("TDDeleteConference"); 
                NetCmds.Add("TDGetSlotAvailableSeats"); 
                NetCmds.Add("TDGetDefaultSettings");
                //FB 2659 End
                NetCmds.Add("GetAllRoomsList"); //FB 2593
                NetCmds.Add("GetPublicCountries");
                NetCmds.Add("GetPublicCountryStates");
                NetCmds.Add("GetPublicCountryStateCities");
                NetCmds.Add("TDGetTemplateList"); //FB 2659T
                NetCmds.Add("TDGetTemplate"); //FB 2659T
                NetCmds.Add("ChkRoomAuthentication"); //ZD 100196
                NetCmds.Add("SetCalendarTimes"); //ZD 100157
                NetCmds.Add("GetCalendarTimes"); //ZD 100157
                NetCmds.Add("ChangePasswordRequest");//ZD 100263
                NetCmds.Add("GetRequestIDUser");//ZD 100263
                NetCmds.Add("ChangeConfMCU"); //ZD 100369-MCU Fail Over
				NetCmds.Add("SendMailtoAdmin"); //ZD 100230
                NetCmds.Add("ForceConfDelete"); //ZD 100221
                NetCmds.Add("SetInstantConferenceDetails");//ZD 100167 102195
				NetCmds.Add("PasswordChangeRequest"); //ZD 100781
                NetCmds.Add("SendPasswordRequest"); //ZD 100781
				//ZD 100152 Starts
				NetCmds.Add("SetUserToken"); 
                NetCmds.Add("GetGoogleChannelExpiredList");
                NetCmds.Add("GetGoogleChannelDetails");
                NetCmds.Add("ForceConfDelete");
                //ZD 100152 Ends
                NetCmds.Add("SetUserRoomViewType");//ZD 100621
                NetCmds.Add("GetUserRoomViewType");//ZD 100621
				NetCmds.Add("CheckForExtendTimeConflicts");//ZD 100819
                NetCmds.Add("GetGMTTime"); //ZD 101120
                NetCmds.Add("GetBridgeDetails"); //ZD 100522
                NetCmds.Add("GetUserType");//ZD 100815
                NetCmds.Add("GetEndPointP2PQuery");//ZD 100815
				NetCmds.Add("SelectedRooms");//ZD 101175
                NetCmds.Add("GetLocationsTier1");//ZD 101244
                NetCmds.Add("GetLocationsTier2");//ZD 101244
				NetCmds.Add("SearchUserInfo");//ZD 101443
                NetCmds.Add("SetAssignedUserAdmin");//ZD 101443
                //ZD 101525 Starts
                NetCmds.Add("SetLDAPGroup");
                NetCmds.Add("GetLDAPGroup");
                NetCmds.Add("GetOrgLDAP");
                NetCmds.Add("UpdateLDAPGroupRole");
                //ZD 101525 End
                NetCmds.Add("ChkADAuthentication"); //ZD 101308
                NetCmds.Add("GetSystemLocation");//ZD 101522
                NetCmds.Add("setRoomResizeImage");//ZD 101611
				NetCmds.Add("CheckDialNumber");// ZD 101657
                NetCmds.Add("GetRoomEWSDetails");//ZD 101736
                NetCmds.Add("UpdateRoomEWSDetails");//ZD 101736
 				//ZD 101527 Starts
                NetCmds.Add("SetSyncEndpoints");
                NetCmds.Add("GetSyncEndpoints");
                NetCmds.Add("SetSyncRooms");
                NetCmds.Add("GetSyncRooms");
                //ZD 101527 Ends
				NetCmds.Add("SetConferenceFromDateImport"); //ZD 101879
				NetCmds.Add("SyncWithLdap");//ZD 102156
                NetCmds.Add("SetConferenceDetails"); //ZD 102195
				//ZD 102358
                NetCmds.Add("GetTopTiers");
                NetCmds.Add("GetMiddleTiersByTopTier");
                NetCmds.Add("GetRoomsByTiers");
                //ZD 101835
                NetCmds.Add("GetConfArchiveConfiguration");
                NetCmds.Add("SetConfArchiveConfiguration");
                NetCmds.Add("GetUserDetails");//ZD 102826
                //ZD 102123 Starts
                NetCmds.Add("SetFloorPlan");
                NetCmds.Add("GetFloorPlans");
                NetCmds.Add("DeleteFloorPlan");
                //ZD 102123 Ends
                NetCmds.Add("OrgOptionsInfo"); //103398    
                NetCmds.Add("UpdateCompletedConference");//ZD 102640
                NetCmds.Add("SendBJNBridgeemails");//ZD 103263
				NetCmds.Add("GetIcontrolconference");//ZD 103460
                NetCmds.Add("SetExpirePassword"); // ZD 103954
				NetCmds.Add("GetMCUPoolOrders");//ZD 104256
                NetCmds.Add("InsertConfAuditDetails"); //ZD 102754
                //ZD 100040 Starts
                NetCmds.Add("SetMCUGroup");
                NetCmds.Add("GetMCUGroup");
                NetCmds.Add("GetMCUGroups");
                NetCmds.Add("DeleteMCUGroup");
                NetCmds.Add("SearchMCUGroup");
                NetCmds.Add("GetVirtualBridges");
                //ZD 100040 Ends
                
                return NetCmds;
            }
            catch (Exception e)
            {
                log.Trace("systemException occured - " + e.Message);
                throw e;
            }
        }

        #endregion

        #region GetCOMCommands

        private List<String> GetCOMCommands()
        {
            try
            {
                COMCmds = new List<String>();
                //COMCmds.Add("AcceptInvite"); //FB 2027
                //COMCmds.Add("CounterInvite"); //FB 2027
                //COMCmds.Add("DeleteAllGuests"); //FB 2027
                //COMCmds.Add("DeleteBridge");//FB 2027
                //COMCmds.Add("DeleteConference");
                //COMCmds.Add("DeleteGroup");
                //COMCmds.Add("DeleteTemplate");//FB 2027
                //COMCmds.Add("DeleteTerminal");//FB 2027
                //COMCmds.Add("DisplayTerminal");//FB 2027
                //COMCmds.Add("GetAllocation"); //FB 2027
                //COMCmds.Add("GetApprovalStatus"); //FB 2027
                //COMCmds.Add("GetApproveConference");//FB 2027
                //COMCmds.Add("GetAvailableRoom");
                //COMCmds.Add("GetConfGMTInfo");//FB 2027
                //COMCmds.Add("GetEmailList");//FB 2027
                //COMCmds.Add("GetEndpoint");//FB 2027
                //COMCmds.Add("GetFeedback"); // FB 2027
                //COMCmds.Add("GetGroup");//FB 2027
                //COMCmds.Add("GetGuestList");//FB 2027
                //COMCmds.Add("GetHome");//FB 2027
                //COMCmds.Add("GetInstances"); //FB 2027
                //COMCmds.Add("GetLogPreferences");//FB 2027
                //COMCmds.Add("GetManageBulkUsers");//FB 2027
                //COMCmds.Add("GetManageGuest");//FB 2027
                //COMCmds.Add("GetManageUser"); // FB 2027
                //COMCmds.Add("GetNewConference");//FB 2027
                //COMCmds.Add("GetNewTemplate");//FB 2027
                //COMCmds.Add("GetOldConference");//FB 2027
                //COMCmds.Add("GetOldRoom");//FB 2027
                //COMCmds.Add("GetOldTemplate"); //FB 2027
                //COMCmds.Add("GetOldUser");//FB 2027
                //COMCmds.Add("GetRoomDailyView");
                //COMCmds.Add("GetRoomMonthlyView");
                //COMCmds.Add("GetRoomWeeklyView");
                //COMCmds.Add("GetSearchConference");//FB 2027
                //COMCmds.Add("GetSettingsSelect"); //FB 2027
                //COMCmds.Add("GetSuperAdmin");//FB 2027
                //COMCmds.Add("GetSystemDateTime"); //FB 2027
                //COMCmds.Add("GetTemplate");//FB 2027
                //COMCmds.Add("GetTemplateList");//FB 2027
                //COMCmds.Add("GetTerminalControl");//FB 2027
                //COMCmds.Add("GetUserPreference"); FB 2027
                //COMCmds.Add("GetUserRoles");
                //COMCmds.Add("GetUsers"); //FB 2027
                //COMCmds.Add("GuestRegister");//FB 2027
                //COMCmds.Add("ManageConfRoom");//FB 2027
                //COMCmds.Add("MuteTerminal"); //FB 2027
                //COMCmds.Add("RejectInvite"); //FB 2027
                //COMCmds.Add("RequestVRMAccount"); //FB 1830
                //COMCmds.Add("ResponseInvite");//FB 2027
                //COMCmds.Add("RetrieveGuest"); //FB 2027
                //COMCmds.Add("RetrieveUsers"); //FB 2027
                //COMCmds.Add("SearchGroup");//FB 2027
                //COMCmds.Add("SearchGuestForManage");//FB 2027
                //COMCmds.Add("SearchLog");//FB 2027
                //COMCmds.Add("SearchUserForManage");//FB 2027
                //COMCmds.Add("SetApproveConference");//FB 2027
                //COMCmds.Add("GetSearchTemplateList");FB 2027
                //COMCmds.Add("SetBridgeList");//FB 2027
                //COMCmds.Add("SetBulkUserAddMinutes");//FB 2027
                //COMCmds.Add("SetBulkUserBridge");//FB 2027
                //COMCmds.Add("SetBulkUserDelete");//FB 2027
                //COMCmds.Add("SetBulkUserDepartment");//FB 2027
                //COMCmds.Add("SetBulkUserExpiryDate");//FB 2027
                //COMCmds.Add("SetBulkUserLanguage");//FB 2027
                //COMCmds.Add("SetBulkUserLock");//FB 2027
                //COMCmds.Add("SetBulkUserRole");//FB 2027
                //COMCmds.Add("SetBulkUserTimeZone");//FB 2027
                //COMCmds.Add("SetConference"); //FB 2027 SetConference
                //COMCmds.Add("SetDynamicUser");//FB 2027
                //COMCmds.Add("SetGroup"); //FB 2027
                //COMCmds.Add("SetOldUser"); FB 2027
                //COMCmds.Add("SetSuperAdmin"); //FB 2027
                //COMCmds.Add("SetTemplate"); //FB 2027
                //COMCmds.Add("SetTerminalControl");//FB 2027
                //COMCmds.Add("SetUser");//FB 2027
                //COMCmds.Add("SetUserRoles");//FB 2027
                //COMCmds.Add("SetUserStatus");FB 2027
                //COMCmds.Add("TerminateConference");//FB 2027
                //COMCmds.Add("ManageConfRoom");//API issues //FB 2027

                //Following comments are not in USE - so commented without converting them into .NET (2027)
                /*
                COMCmds.Add("SetBulkUserUpdate");
                COMCmds.Add("SetGuest");
                COMCmds.Add("SetLogPreferences");
                COMCmds.Add("SetSystemDetails");
                COMCmds.Add("SetTerminalDetail");
                COMCmds.Add("UserReport");
                COMCmds.Add("GetRecurDateList");
                COMCmds.Add("GetTerminal");
                COMCmds.Add("GetTimezones");
                COMCmds.Add("GetOldGuest");
                COMCmds.Add("ActiveRoom");
                COMCmds.Add("ConnectDisconnectTerminal");
                COMCmds.Add("DeleteModuleLog");
                COMCmds.Add("DeleteRoom");
                COMCmds.Add("DeleteSearchTemplate");
                COMCmds.Add("EmailLoginInfo");
                COMCmds.Add("GetAccountFuture");
                COMCmds.Add("GetAccountPending");
                COMCmds.Add("GetDeleteConference");
                COMCmds.Add("GetEndpointList");
                COMCmds.Add("GetGroups");
                COMCmds.Add("GetLocations");
                COMCmds.Add("GetLocations2");
                COMCmds.Add("RoomReport");
                COMCmds.Add("SaveEmail");
                COMCmds.Add("SaveRoom");
                COMCmds.Add("SaveSearch");
                COMCmds.Add("SearchRoom");
                COMCmds.Add("SearchTemplate");
                COMCmds.Add("SearchUser");
                COMCmds.Add("SendParticipantReminder");
                COMCmds.Add("SetBridge");
                */

                return COMCmds;
            }
            catch (Exception e)
            {
                log.Trace("systemException occured - " + e.Message);
                throw e;
            }
        }

        #endregion

        #region GetRTCCommands

        private List<String> GetRTCCommands()
        {
            try
            {
                RTCCommands = new List<String>();

                RTCCommands.Add("BulkLoadUsers");
                RTCCommands.Add("ConnectDisconnectTerminal");
                RTCCommands.Add("DeleteTerminal");
                RTCCommands.Add("DisplayTerminal");
                RTCCommands.Add("ForceConfDelete");
                RTCCommands.Add("GenerateMcuResourceAllocationReport");
                RTCCommands.Add("GetConferenceAlerts");
                RTCCommands.Add("GetP2PConfStatus");
                RTCCommands.Add("GetTerminalStatus");
                RTCCommands.Add("HK_GetConferenceAdHocReport");
                RTCCommands.Add("HK_SetConference_CommitDiffs");
                RTCCommands.Add("HK_SetConference_SaveDiffs");
                RTCCommands.Add("MuteTerminal");
                RTCCommands.Add("SendEmails");
                RTCCommands.Add("SendMessageToConference");
                RTCCommands.Add("SendMessageToEndpoint");
                RTCCommands.Add("SetConferenceOnMcu");
                //  RTCCommands.Add("SetTerminalControl"); //Extend Time Fix
                RTCCommands.Add("SyncLdapNow");
                RTCCommands.Add("SyncWithLdap");//FB 2462
                RTCCommands.Add("TerminateConference");
                RTCCommands.Add("TestExchangeConnection");
                RTCCommands.Add("TestLDAPConnection");
                RTCCommands.Add("TestMailConnection");
                RTCCommands.Add("TestMCUConnection");
                RTCCommands.Add("TerminateCompletedP2PConfs");
                RTCCommands.Add("TestOrgEmail"); // FB 1758
                RTCCommands.Add("ModifyTerminal"); // FB 2249
                RTCCommands.Add("TerminateVMRConference"); //ZD 100522
                RTCCommands.Add("SetExternalScheduling"); //FB 2363
                RTCCommands.Add("TriggerEventService"); //FB 2363
                RTCCommands.Add("TriggerEvent"); //FB 2363
                RTCCommands.Add("AddConferenceEndpoint"); //FB 2261
                RTCCommands.Add("FetchConfMCUDetails"); //FB 2448
                RTCCommands.Add("GetMCUProfiles");//FB 2591
				RTCCommands.Add("PollVidyo"); //FB 2448 //FB 2599
				RTCCommands.Add("PushtoWhyGO"); //FB 2392 start -WhyGo
                RTCCommands.Add("GetLocationUpdate");
                RTCCommands.Add("CreateUserinWhyGo");
                RTCCommands.Add("GetPublicRoomsPrices");
                RTCCommands.Add("GetPublicRoomsAvailability"); //FB 2392 end -WhyGo
				RTCCommands.Add("FetchConfMCUDetails"); //FB 2448
                RTCCommands.Add("SetSwitchingtoConf");//FB 2595
                RTCCommands.Add("MuteUnMuteParties");//FB 2441
                RTCCommands.Add("GetExtMCUSilo"); //FB 2556
                RTCCommands.Add("GetExtMCUServices"); //FB 2556
				RTCCommands.Add("SetLeaderParty"); //FB 2553
                RTCCommands.Add("SetLectureMode"); //FB 2553
                RTCCommands.Add("CallDetailRecords"); //FB 2593
                RTCCommands.Add("GetLocationAvailability");//FB 2392
                RTCCommands.Add("EmailtoWhygoAdmin"); //ZD 100694
				RTCCommands.Add("FetchSystemLocation"); //ZD 101522
                RTCCommands.Add("TestEndpointConnection"); //ZD 101363
				RTCCommands.Add("SyncRoom"); //ZD 101527 
                RTCCommands.Add("ManualSyncRoom"); //ZD 101527 
                RTCCommands.Add("GetMCUPoolOrders");//ZD 104256
                return RTCCommands;

            }
            catch (Exception e)
            {
                log.Trace("systemException occured -" + e.Message);
                throw e;
            }
        }

        #endregion

        #endregion

        // Methods added for Organization\Css Module  -- Start
        #region SetCSSFilePath
        /// <summary>
        /// Set CSSFilePath to Session Variables
        /// </summary>
        public void SetCSSFilePath()
        {
            try
            {
                String roomsxmlPath = "";//FB room search
                String orgName = "";
                string configPath = "";
                XmlDocument OrgDocument = null;
                string OrgCSSPath = "";
                string OrgJsPath = "";
                String mainPath = "";
                string OrgBanner1024Path = "";
                string OrgBanner1600Path = "";
                string companyLogo = "";
                XmlNode node = null;
                configPath = HttpContext.Current.Server.MapPath(".") + "\\WDConfig.xml"; //FB 1830 - Translation Menu
                if (!File.Exists(configPath))
                {
                    throw new FileNotFoundException("WDConfig.xml file does not exist", configPath);
                }

                OrgDocument = new XmlDocument();
                OrgDocument.Load(configPath);

                mainPath = OrgDocument.SelectSingleNode("//Location/MainFolder").InnerText.Trim();

                node = OrgDocument.SelectSingleNode("//Location/Mirror/Styles");
                String[] orgCPath = null;
                String csspath = "";
                String csspath1 = "";

                if (node != null)
                {
                    OrgCSSPath = node.InnerText.Trim();
                    orgCPath = OrgCSSPath.Split('\\');
                    for (int i = 0; i < orgCPath.Length; i++)
                    {
                        csspath1 = orgCPath[0];
                        if (orgCPath[i] != csspath1)
                            csspath += "/" + orgCPath[i];
                        else
                            csspath = orgCPath[0];
                    }
                }

                node = OrgDocument.SelectSingleNode("//Location/Mirror/Javascript");
                String[] orgPath = null;
                String path = "";
                String path1 = "";
                if (node != null)
                {
                    OrgJsPath = node.InnerText.Trim();
                    orgPath = OrgJsPath.Split('\\');
                    for (int i = 0; i < orgPath.Length; i++)
                    {
                        path1 = orgPath[0];
                        if (orgPath[i] != path1)
                            path += "/" + orgPath[i];
                        else
                            path = orgPath[0];
                    }
                }

                if (HttpContext.Current.Session["organizationID"] != null)
                    orgName = "Org_" + HttpContext.Current.Session["organizationID"].ToString().Trim();

                // OrgCSSPath = HttpContext.Current.Server.MapPath(".") + "\\" + mainPath + "\\" + orgName + "\\" + OrgCSSPath;
                OrgCSSPath = mainPath + "/" + orgName + "/" + csspath;
                OrgJsPath = mainPath + "/" + orgName + "/" + path;
                roomsxmlPath = mainPath + "/" + orgName + "/Rooms/Room.xml";//FB room search 
                HttpContext.Current.Session.Remove("OrgCSSPath");
                HttpContext.Current.Session.Remove("OrgJsPath");
                HttpContext.Current.Session.Remove("RoomXmlPath");//FB room search
                HttpContext.Current.Session.Remove("EptXmlPath");//FB 2361

                if (HttpContext.Current.Session["OrgCSSPath"] == null)
                {
                    HttpContext.Current.Session.Add("OrgCSSPath", OrgCSSPath); //FB 1830 - Translation Menu
                }
                if (HttpContext.Current.Session["OrgJsPath"] == null)
                {
                    HttpContext.Current.Session.Add("OrgJsPath", OrgJsPath); //FB 1830 - Translation Menu
                }

                if (HttpContext.Current.Session["RoomXmlPath"] == null)//FB room search
                {
                    HttpContext.Current.Session.Add("RoomXmlPath", roomsxmlPath); //FB 1830 - Translation Menu
                }

                if (HttpContext.Current.Session["EptXmlPath"] == null)//FB 2361
                    HttpContext.Current.Session.Add("EptXmlPath", roomsxmlPath.Replace("Rooms/Room.xml", "Endpoints/Endpoints.xml")); //FB 2361
                

                node = OrgDocument.SelectSingleNode("//Location/Mirror/Banner1024");
                path = "";
                orgPath = null;
                if (node != null)
                {
                    OrgJsPath = node.InnerText.Trim();
                    orgPath = OrgJsPath.Split('\\');
                    for (int i = 0; i < orgPath.Length; i++)
                    {
                        path1 = orgPath[0];
                        if (orgPath[i] != path1)
                            path += "/" + orgPath[i];
                        else
                            path = orgPath[0];
                    }
                }
                OrgBanner1024Path = mainPath + "/" + orgName + "/" + path;

                HttpContext.Current.Session.Remove("OrgBanner1024Path");
                if (HttpContext.Current.Session["OrgBanner1024Path"] == null)
                {
                    HttpContext.Current.Session.Add("OrgBanner1024Path", OrgBanner1024Path);
                }

                node = OrgDocument.SelectSingleNode("//Location/Mirror/Banner1600");
                path = "";
                orgPath = null;
                if (node != null)
                {
                    OrgJsPath = node.InnerText.Trim();
                    orgPath = OrgJsPath.Split('\\');
                    for (int i = 0; i < orgPath.Length; i++)
                    {
                        path1 = orgPath[0];
                        if (orgPath[i] != path1)
                            path += "/" + orgPath[i];
                        else
                            path = orgPath[0];
                    }
                }
                OrgBanner1600Path = mainPath + "/" + orgName + "/" + path;

                HttpContext.Current.Session.Remove("OrgBanner1600Path");
                if (HttpContext.Current.Session["OrgBanner1600Path"] == null)
                {
                    HttpContext.Current.Session.Add("OrgBanner1600Path", OrgBanner1600Path); 
                }

                node = OrgDocument.SelectSingleNode("//Location/Mirror/Logo");
                path = "";
                orgPath = null;
                if (node != null)
                {
                    OrgJsPath = node.InnerText.Trim();
                    orgPath = OrgJsPath.Split('\\');
                    for (int i = 0; i < orgPath.Length; i++)
                    {
                        path1 = orgPath[0];
                        if (orgPath[i] != path1)
                            path += "/" + orgPath[i];
                        else
                            path = orgPath[0];
                    }
                }
                companyLogo = mainPath + "/" + orgName + "/" + path;

                HttpContext.Current.Session.Remove("CompanyLogo");
                if (HttpContext.Current.Session["CompanyLogo"] == null)
                {
                    HttpContext.Current.Session.Add("CompanyLogo", "../" + companyLogo); //FB 1830
                }

            }
            catch (Exception e)
            {
                log.Trace(e.Message);
            }
        }
        #endregion

        #region Set TextChangeXML in Session for respective organization
        /// <summary>
        /// Set TextChangeXML in Session for respective organization
        /// </summary>
        public void SetOrgTextChangeXML()
        {
            try
            {
                String orgName = "";
                string configPath = "";
                XmlDocument OrgDocument = null;
                XmlDocument OrgXMLDoc = null;
                string OrgXMLPath = "";
                String mainPath = "";
                XmlNode node = null;

                configPath = HttpContext.Current.Server.MapPath(".") + "\\WDConfig.xml"; //FB 1830 - Translation Menu
                if (!File.Exists(configPath))
                {
                    throw new FileNotFoundException("WDConfig.xml file does not exist", configPath);
                }

                OrgDocument = new XmlDocument();
                OrgDocument.Load(configPath);

                mainPath = OrgDocument.SelectSingleNode("//Location/MainFolder").InnerText.Trim();

                node = OrgDocument.SelectSingleNode("//Location/Mirror/TextXmlVersion");


                if (node != null)
                    OrgXMLPath = node.InnerText.Trim();


                if (HttpContext.Current.Session["organizationID"] != null)
                    orgName = "Org_" + HttpContext.Current.Session["organizationID"].ToString().Trim();

                OrgXMLPath = HttpContext.Current.Server.MapPath(".") + "\\" + mainPath + "\\" + orgName + "\\" + OrgXMLPath; //FB 1830 - Translation Menu
                OrgXMLDoc = new XmlDocument();
                OrgXMLDoc.Load(OrgXMLPath);

                string textXml = OrgXMLDoc.InnerXml;

                HttpContext.Current.Session.Remove("OrgTextXML");

                if (HttpContext.Current.Session["OrgTextXML"] == null)
                {
                    HttpContext.Current.Session.Add("OrgTextXML", textXml);
                }
            }
            catch (Exception e)
            {
                log.Trace(e.Message);
            }
        }
        #endregion

        //Organization\Css Module End

        #region to wait for file untill its available
        /// <summary>
        /// Blocks until the file is not locked any more.
        /// </summary>
        /// <param name="fullPath"></param>
        public bool WaitForFile(string fullPath)
        {
            int numTries = 0;
            FileStream fs = null;
            try
            {
                while (true)
                {
                    ++numTries;
                    try
                    {
                        if (fullPath != "")
                        {
                            if (File.Exists(fullPath))
                            {
                                // Attempt to open the file exclusively.
                                using (fs = new FileStream(fullPath,
                                    FileMode.Open, FileAccess.ReadWrite,
                                    FileShare.None, 100))
                                {
                                    fs.ReadByte();
                                    fs.Close();
                                    fs.Dispose();
                                    fs = null;
                                    break;
                                }
                            }
                        }

                        break;

                    }
                    catch (Exception ex)
                    {
                        if (numTries > 10)
                        {
                            return false;
                        }

                        // Wait for the lock to be released
                        System.Threading.Thread.Sleep(5);

                    }
                    finally
                    {
                        if (fs != null)
                        {
                            fs.Close();
                            fs.Dispose();
                            fs = null;
                        }

                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                if (fs != null)
                {
                    fs.Close();
                    fs.Dispose();
                    fs = null;
                }

            }

        }
        #endregion

        //FB 1830 start
        #region GetLanguages
        /// <summary>
        /// GetLanguages
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="sel"></param>
        public void GetLanguages(DropDownList sender)
        {
            String userID = "11";
            try
            {

                if (HttpContext.Current.Session["userID"] != null)
                    userID = HttpContext.Current.Session["userID"].ToString();

                String inXML = "<GetLanguages><UserID>" + userID + "</UserID>" + OrgXMLElement() + "</GetLanguages>";//Code added for Error 200 //Organization Module Fixes
                String outXML = "";
                outXML = CallMyVRMServer("GetLanguages", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                //if (xmldoc.SelectSingleNode("//Timezones/selected") != null)//Code added for Error 200
                //    sel = xmldoc.SelectSingleNode("//Timezones/selected").InnerText;//Code added for Error 200

                XmlNodeList nodes = xmldoc.SelectNodes("//GetLanguages/Language");
                if (nodes.Count > 0)
                    LoadList(sender, nodes, "ID", "Name");

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        #region GetEmailLanguages

        public void GetEmailLanguages(DropDownList sender)
        {
            String userPreLang = "";
            try
            {

                if (HttpContext.Current.Session["language"] != null)
                    userPreLang = HttpContext.Current.Session["language"].ToString();

                String inXML = "<GetEmailLanguages><languageid>" + userPreLang + "</languageid>" + OrgXMLElement() + "</GetEmailLanguages>";
                String outXML = "";

                outXML = CallMyVRMServer("GetEmailLanguages", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);

                XmlNodeList nodes = xmldoc.SelectNodes("//GetEmailLanguages/emaillanguages");

                if (nodes.Count < 1)
                    LoadList(sender, nodes, "emaillangid", "emaillanguage");

            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //FB 1830 Languages start
        #region drpLoadList
        /// <summary>
        /// drpLoadList
        /// </summary>
        /// <param name="lstList"></param>
        /// <param name="nodes"></param>
        /// <param name="col1"></param>
        /// <param name="col2"></param>
        public void drpLoadList(DropDownList lstList, XmlNodeList nodes, String col1, String col2)
        {
            try
            {
                XmlTextReader xtr;
                DataSet ds = new DataSet();

                foreach (XmlNode node in nodes)
                {
                    xtr = new XmlTextReader(node.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                    ds.ReadXml(xtr, XmlReadMode.InferSchema);
                }
                DataView dv;
                DataTable dt;

                if (ds.Tables.Count > 0)
                {
                    //Response.Write(ds.Tables[0].Columns[0].ColumnName);
                    dv = new DataView(ds.Tables[0]);
                    dt = dv.Table;
                    if (dt.Columns.Contains("Name"))//FB 2272
                        foreach (DataRow dr in dt.Rows)
                            dr["Name"] = GetTranslatedText(dr["Name"].ToString());
                }
                else
                {
                    dv = new DataView();
                    dt = new DataTable();
                    dt.Columns.Add(col1);
                    dt.Columns.Add(col2);
                    DataRow dr = dt.NewRow();
                    dr[0] = "-1";
                    dr[1] = GetTranslatedText("No Items...");
                    dt.Rows.InsertAt(dr, 0);
                }
                lstList.DataSource = dt;
                lstList.DataBind();
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }

        }
        #endregion
        //FB 1830 Languages end

        //FB 1888
        #region ReplaceJunkCharacters
        /// <summary>
        /// ReplaceJunkCharacters
        /// </summary>
        /// <param name="strXML"></param>
        /// <param name="replaceSource"></param>
        private void ReplaceJunkCharacters(ref String strXML, String replaceSource, String sCommand)
        {
            try
            {
                if (sCommand.ToUpper() != "SETEMAILCONTENT" && sCommand.ToUpper() != "SETORGSETTINGS" 
                    && sCommand.ToUpper() != "SETBLOCKEMAIL" && sCommand.ToUpper() != "TESTORGEMAIL" && sCommand.ToUpper() != "TESTMCUCONNECTION") //FB 2232
                {
                    if (replaceSource == "J")
                        strXML = strXML.Replace("'", "!!").Replace("\"", "||");
                    else
                    {
                        if (sCommand.ToUpper() != "GETEMAILLIST" && sCommand.ToUpper() != "GETAPPROVALSTATUS")
                            strXML = strXML.Replace("!!", "'").Replace("||", "\"");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Trace(ex.Message);
            }
        }

        #endregion

        //FB 1911
        #region AppendRecur
        /// <summary>
        /// AppendSpecialRecur
        /// </summary>
        /// <param name="instr"></param>
        /// <returns></returns>
        public string AppendSpecialRecur(string instr, string bufferxml, string timezone,ref string recXML)
        {
            StringBuilder recurXML = new StringBuilder();
            MyVRMNet.SpecialReccurence spRec = null;
            

            try
            {
                int TotalDur = 0; //FB 2398
                string[] recurArray = instr.Split('#');
                string[] recurArray0 = recurArray[0].Split('&');
                string[] recurArray2 = recurArray[2].Split('&'); //FB 2398
                
                recurArray0[0] = timezone;
                
                //FB 2398 start
                //if (Int32.Parse(recurArray0[1]) > 12)
                //{
                //    recurArray0[1] = Convert.ToString(Int32.Parse(recurArray0[1]) - 12);
                //    recurArray0[3] = "PM";
                //}
                //if (Int32.Parse(recurArray0[1]) == 0)
                //{
                //    recurArray0[1] = "12";
                //    recurArray0[3] = "AM";
                //}

                Int32.TryParse(recurArray0[4], out TotalDur);
                DateTime BuffSetup = DateTime.Parse(GetDefaultDate(recurArray2[0]) + " " + recurArray0[1] + ":" + recurArray0[2] + " " + recurArray0[3]);
                //FB 2634
                //if (!isBufferChecked && EnableBufferZone == 1)
                //{
                //    BuffSetup = BuffSetup.AddMinutes(-OrgSetupTime);
                //    TotalDur += OrgSetupTime + OrgTearDownTime;
                //}
                
                recurXML.Append("<appointmentTime>");
                recurXML.Append("<timeZone>" + recurArray0[0] + "</timeZone>");
                recurXML.Append("<startHour>" + BuffSetup.ToString("hh") + "</startHour>");
                recurXML.Append("<startMin>" + BuffSetup.ToString("mm") + "</startMin>");
                recurXML.Append("<startSet>" + BuffSetup.ToString("tt") + "</startSet>");
                recurXML.Append("<durationMin>" + TotalDur + "</durationMin>"); //FB 2398 end
                recurXML.Append(bufferxml);
                recurXML.Append("</appointmentTime>");
                recurXML.Append("<recurrencePattern>");
                recurXML.Append("<recurType>5</recurType>");

                spRec = new MyVRMNet.SpecialReccurence();
                spRec.recurString = instr;
                spRec.GetSpecialReccurence();
                recurXML.Append(spRec.recurXML);
                recXML = spRec.recurXML;
                recurXML.Append("</recurrencePattern>");
            }
            catch (Exception ex)
            {
                recurXML = new StringBuilder();
                recurXML.Append("<error>"+ ex.Message +"</error>"); //FB 2027
 
            }

            return recurXML.ToString();
        }
        #endregion        

        //FB 1881 - Starts
        #region ShowSystemMessage
        /// <summary>
        /// ShowSystemMessage
        /// </summary>
        /// <returns></returns>
        public String ShowSystemMessage()
        {
            String systemError = GetTranslatedText("A system error has occurred. Please contact your myVRM system administrator and give them the following error code.");//FB 2272 //ZD 102432
            if (HttpContext.Current.Session["SystemError"] != null)
                if (HttpContext.Current.Session["SystemError"].ToString() != "")
                    systemError = HttpContext.Current.Session["SystemError"].ToString();

            return systemError;
        }
        #endregion

        #region ShowSuccessMessage
        /// <summary>
        /// showSuccessMessage
        /// </summary>
        /// <returns></returns>
        public String ShowSuccessMessage()
        {
            return GetTranslatedText("Operation Successful!");
        }
        #endregion

        #region GetErrorMessage
        /// <summary>
        /// GetErrorMessage
        /// </summary>
        /// <returns></returns>
        public String GetErrorMessage(int errorid)
        {
            string strErrMsg = "";
            try
            {
                ASPIL.VRMServer myvrmCom = new ASPIL.VRMServer();
                strErrMsg = ShowErrorMessage(myvrmCom.GetErrorTextByID(errorid));
            }
            catch (Exception e)
            {           
                log.Trace("GetErrorMessage : " + e.Message);
            }
            return strErrMsg;
        }
        #endregion
        //FB 1881 - End

        //FB 1522
        #region ViewUserRole
        public bool ViewUserRole()
        {
            bool ViewUser = true;
            try
            {
                if (HttpContext.Current.Session["sMenuMask"] == null)
                    return false;

                string[] mm_aryc = HttpContext.Current.Session["sMenuMask"].ToString().Split('-');
                string mmm_strc = mm_aryc[0];
                string[] mmm_aryc = mmm_strc.Split('*');
                int mmm_numc = Convert.ToInt32(mmm_aryc[0], 10);
                int mmm_intc = Convert.ToInt32(mmm_aryc[1], 10);
                for (int i = 1; i <= mmm_numc; i++)
                {
                    string menu = (Convert.ToBoolean(mmm_intc & (1 << (mmm_numc - i)))) ? ("menu_" + i) : "";
                    if (menu == "menu_3")
                        ViewUser = false; //menuset = "1";
                }
            }
            catch (Exception ex)
            {
                log.Trace("userRole" + ex.Message);
            }
            return ViewUser;
        }
        #endregion

        //FB 1830 -Translation
        #region Get Translated Text
        /// <summary>
        /// GetTranslatedText
        /// </summary>
        public string GetTranslatedText(string text)
        {
            string transText = "";
            try
            {
                transText = text;
                if (HttpContext.Current != null && HttpContext.Current.Session != null && HttpContext.Current.Session["languageID"] != null)
                {
                    log.Trace(HttpContext.Current.Session["languageID"].ToString());

                    if (HttpContext.Current.Session["languageID"].ToString() == "1")
                        return text;

                    if (HttpContext.Current.Session["TranslationText"] != null && HttpContext.Current.Session["TranslationText"].ToString().Trim() != "")
                    {
                        transText = text;

                        XElement root = XElement.Parse(HttpContext.Current.Session["TranslationText"].ToString());
                        IEnumerable<XElement> translatedText =
                            from translatedTextelmnts in root.Elements("Translation")
                            where (int)translatedTextelmnts.Element("LanguageID") == Convert.ToInt32(HttpContext.Current.Session["languageID"].ToString())
                            && ((string)translatedTextelmnts.Element("Text")).Trim().ToUpper() == text.Trim().ToUpper()
                            select translatedTextelmnts;

                        foreach (XElement elmnts in translatedText)
                        {
                            if ((string)elmnts.Element("TranslatedText") != "")
                            {
                                transText = (string)elmnts.Element("TranslatedText");
                                transText = transText.Replace("'", "`"); //ZD 101714
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                errLabel.Text = ex.StackTrace;
                log.Trace(ex.InnerException.ToString());
            }
            return transText;
        }

        #endregion

        //FB 2153 start
        #region Getavailable File name

        public string AvailableFilename(string directory, string fileName)
        {
            string numberPattern = " ({0})";
            string srchPattern = "*";
            string srchFiles = "";
            string[] files = null;
            string path = String.Empty;
            Int32 fileNum = 0;
            try
            {
                if (!directory.Trim().Equals(""))
                {
                    path = directory + "\\" + fileName;


                    if (!File.Exists(path))
                        return path;

                    if (Path.HasExtension(path))
                        path = path.Insert(path.LastIndexOf(Path.GetExtension(path)), numberPattern);
                    else
                        path = path + numberPattern;


                    srchFiles = fileName + srchPattern;

                    if (Path.HasExtension(fileName))
                        srchFiles = fileName.Insert(fileName.LastIndexOf(Path.GetExtension(fileName)), srchPattern);

                    files = Directory.GetFiles(directory, srchFiles);

                    if (files.Length > 0)
                    {
                        fileNum = files.Length;

                        while (File.Exists(string.Format(path, fileNum)))
                            fileNum++;

                    }

                    path = string.Format(path, fileNum);
                }

            }
            catch (Exception ex)
            {

            }

            return path;
        }

        #endregion
        //FB 2153 end

        //FB 2599 Start
        //FB 2262
        #region Room - Type
        public enum Type
        {
            Legacy = 1,
            Public,
            UserVMR
        }
        #endregion
        //FB 2599 End

        //FB 2392-Whygo Strats
        #region Enum for Region
        public enum Region
        {
            AU = 1,
            EU,
            US
        }
        #endregion

        #region Enum for external portal

        public enum ExternalPortal
        {
            WhyGo = 1
        }

        #endregion
        //FB 2392-Whygo End

        //FB 2659 - Starts
        #region TDBReturnXML
        /// <summary>
        /// TDBReturnXML
        /// </summary>
        /// <param name="retCode"></param>
        /// <param name="message"></param>
        /// <param name="retXML"></param>
        /// <returns></returns>
        internal bool TDBReturnXML(int retCode, string message, string error, ref StringBuilder retXML)
        {
            try
            {
                retXML.Append("<retState>");
                retXML.Append("<retCode>" + retCode + "</retCode>");
                retXML.Append("<retMessage>" + message + "</retMessage>");
                retXML.Append("<settingsVersion>1</settingsVersion>");
                retXML.Append(error);
                retXML.Append("</retState>");
            }
            catch (Exception ex)
            {
                return false;
            }
            return true;
        }
        #endregion

        #region TDBReturnXML
        internal enum TDBReturnCode
        {
            Success = 0,
            InvalidTicket = 1,
            UnhandledException = 2,
            InvalidDatesRange = 3,
            IncorrectConference = 4,
            ConferenceNotExist = 5
        }
        #endregion
        //FB 2659 - End

        //FB 2588 Starts
        #region Method to change the ZuluTime HHmmZ format to HH:mm 
        /// <summary>
        /// Method to change the ZuluTime HHmmZ format to HH:mm 
        /// Convertion is done manually
        /// </summary>
        /// <param name="orginalDate"></param>
        /// <returns></returns>
        public static string ChangeTimeFormat(string originalTime)
        {
            string formattedTime = originalTime;
            try
            {
                if (originalTime == "")
                    throw new ArgumentNullException("originalDate", "The date column is empty or null");

                if (HttpContext.Current.Session["timeFormat"] == null)
                    throw new Exception("Session expired");

                if (HttpContext.Current.Session["timeFormat"].ToString() == "2")
                {
                    if (!(originalTime.Contains(":")))
                    {
                        originalTime = originalTime.Replace("Z", "");
                        originalTime = originalTime.Insert(2, ":");

                    }
                    formattedTime = originalTime; 
                }

                return formattedTime;
            }
            catch (Exception ex)
            {
                ns_Logger.Logger errLog = new ns_Logger.Logger();
                errLog.Trace("ChangeTimeFormat: " + ex.StackTrace + " : " + ex.Message);
                return "";
            }
        }
        #endregion
        //FB 2588 Ends
          
        //FB 2694 Starts - Enum available in Business layer
        #region Enum for RoomCategory
        public enum RoomCategory
        {
            NormalRoom = 1,
            VMRRoom = 2,
            GuestRoom = 3,
            HotdeskingRoom = 4,
			VidyoRoom = 5,
            iControl = 6
        }
        #endregion
        //FB 2694 Ends
        
        //FB 2839 - Start
        #region BindProfileDetails
        public void BindProfileDetails(DropDownList lstMCUProfile, String BridgeID) //FB Case 198: Saima added col2
        {
            String userID = "11";
            try
            {
                String outXML = "";
                StringBuilder inXML = new StringBuilder();
                XmlDocument xmlDoc = null;
                DataSet ds = null;
                if (HttpContext.Current.Session["userID"] != null)
                    userID = HttpContext.Current.Session["userID"].ToString();

                inXML.Append("<GetMCUProfiles>");
                inXML.Append("<UserID>" + userID + "</UserID>" + OrgXMLElement());                
                inXML.Append("<MCUId>" + BridgeID + "</MCUId>");
                inXML.Append("</GetMCUProfiles>");
                log.Trace("MCU Profile Details InXML " + inXML.ToString());
                outXML = CallMyVRMServer("GetMCUProfiles", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(outXML);
                    ds = new DataSet();
                    ds.ReadXml(new XmlNodeReader(xmlDoc));
                    XmlNodeList nodes = xmlDoc.SelectNodes("//GetMCUProfiles/Profile");
                    if (nodes.Count > 0)
                        LoadList(lstMCUProfile, nodes, "ID", "Name");                   
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //FB 2839 - End     

        #region GetPublicCountries
        /// NOT USED
        public void GetPublicCountries()
        {
            try
            {
                String inXML = "<GetPublicCountries><UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID>" + OrgXMLElement() + "</GetPublicCountries>";
                String outXML = CallMyVRMServer("GetPublicCountries", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
            }
            catch (Exception ex)
            {
                log.Trace("GetPublicCountries: " + ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion

        //ZD 100157 Starts
        public static String GetEndTime(String originalTime, String formatTimeType)
        {
            try
            {
                if (originalTime == "")
                    throw new ArgumentNullException("originalTime", "The time column is empty or null");

                if (formatTimeType == "")
                    throw new Exception("Session expired");

                DateTime EndTime;
                DateTime.TryParse(originalTime, out EndTime);
                ////if (HttpContext.Current.Session["IsOpen24Hours"].ToString() == "1")
                ////    originalTime = EndTime.AddMinutes(59).ToString();

                if (formatTimeType == "1")
                    return Convert.ToDateTime(originalTime).ToString("hh:mm tt");
                else if (formatTimeType == "2")
                    return Convert.ToDateTime(ChangeTimeFormat(originalTime)).ToString("HHmmZ");
                else
                    return Convert.ToDateTime(originalTime).ToString("HH:mm");
            }
            catch (Exception ex)
            {
                ns_Logger.Logger errLog = new ns_Logger.Logger();
                errLog.Trace("GetFormattedDate: " + ex.StackTrace + " : " + ex.Message);
                return "";

            }
        }
        //ZD 100157 Ends

        // ZD 100263 Starts
        public void AccessandURLConformityCheck(string page, string completeURL)
        {
            String redirectedPage = "ShowError.aspx";
            bool stat = false;
            string[] accessList = null;
            int cnt = 0;
            try
            {
                HttpContext.Current.Response.AppendHeader("Cache-Control", "no-store");
                HttpContext.Current.Response.CacheControl = "no-cache";
                HttpContext.Current.Response.AddHeader("Pragma", "no-cache");
                if (HttpContext.Current.Session["roomCascadingControl"] != null && HttpContext.Current.Session["roomCascadingControl"].ToString().Equals("1"))
                {
                    completeURL = completeURL.ToLower();
                    for (cnt = 0; cnt < urlAccess.Length; cnt++)
                    {
                        if (completeURL.IndexOf("recurnet.aspx") > -1 && (urlAccess[cnt].ToString() == "object" || urlAccess[cnt].ToString() == "html"))
                            continue;
                        if (completeURL.IndexOf("allocation.aspx") > -1 && urlAccess[cnt].ToString() == "location")
                            continue;
                        if (completeURL.IndexOf("itemslist.aspx") > -1 && urlAccess[cnt].ToString() == "src")
                            continue;
                        if (completeURL.IndexOf("emaillist2.aspx") > -1 && urlAccess[cnt].ToString() == "src")
                            continue;
                        if (completeURL.IndexOf("emaillist2main.aspx") > -1 && urlAccess[cnt].ToString() == "src")
                            continue;
                        if (completeURL.IndexOf("userhistoryreport.aspx") > -1 && urlAccess[cnt].ToString() == "history")
                            continue;
                        if (completeURL.IndexOf("confirmtemplate.aspx") > -1 && urlAccess[cnt].ToString() == "confirm")
                            continue;
                        if (completeURL.IndexOf(urlAccess[cnt].ToLower().Trim()) > -1)
                            HttpContext.Current.Response.Redirect(redirectedPage);
                    }


                }

                if (HttpContext.Current.Session["AccessCheck"] != null && page.Trim() != "")
                {
                    page = page.ToLower().Trim();
                    accessList = (string[])HttpContext.Current.Session["AccessCheck"];
                    stat = accessList.Contains(page);
                }

                if (!stat && accessList != null)
                    HttpContext.Current.Response.Redirect(redirectedPage);

            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {

                log.Trace("Where =" + ex.StackTrace + "What =" + ex.Message);
            }

        }
        public void URLConformityCheck(string completeURL)
        {
            
            
            String redirectedPage = "ShowError.aspx";
            int cnt = 0;
            try
            {
                HttpContext.Current.Response.AppendHeader("Cache-Control", "no-store");
                HttpContext.Current.Response.CacheControl = "no-cache";
                HttpContext.Current.Response.AddHeader("Pragma", "no-cache");
                if (HttpContext.Current.Session["roomCascadingControl"] != null && HttpContext.Current.Session["roomCascadingControl"].ToString().Equals("1"))
                {
                    completeURL = completeURL.ToLower();
                    for (cnt = 0; cnt < urlAccess.Length; cnt++)
                    {
                        if (completeURL.IndexOf("manageconference.aspx") > -1 && urlAccess[cnt].ToString() == "confirm")
                            continue;
                        if (completeURL.IndexOf("ifrmaduserlist.aspx") > -1 && urlAccess[cnt].ToString() == "location")
                            continue;
                        if (completeURL.IndexOf("ifrmvrmuserlist.aspx") > -1 && urlAccess[cnt].ToString() == "location")
                            continue;
                        if (completeURL.IndexOf(urlAccess[cnt].ToLower().Trim()) > -1)
                            HttpContext.Current.Response.Redirect(redirectedPage);
                    }
                }

            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {

                log.Trace("Where =" + ex.StackTrace + "What =" + ex.Message);
            }

        }
        public void AccessConformityCheck(string page)
        {
            String redirectedPage = "ShowError.aspx";
            bool stat = false;
            string[] accessList = null;
            try
            {
                HttpContext.Current.Response.AppendHeader("Cache-Control", "no-store");
                HttpContext.Current.Response.CacheControl = "no-cache";
                HttpContext.Current.Response.AddHeader("Pragma", "no-cache");
                if (HttpContext.Current.Session["AccessCheck"] != null)
                {
                    page = page.ToLower().Trim();
                    accessList = (string[])HttpContext.Current.Session["AccessCheck"];
                    stat = accessList.Contains(page);
                }

                if (!stat && accessList!= null)
                    HttpContext.Current.Response.Redirect(redirectedPage);
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {

                log.Trace("Where =" + ex.StackTrace + "What =" + ex.Message);
            }

        }
        // ZD 100263 Ends
        public void URLConformityCheckNoSession(string completeURL)
        {


            String redirectedPage = "genlogin.aspx";
            int cnt = 0;
            try
            {
                
                
                    completeURL = completeURL.ToLower();
                    for (cnt = 0; cnt < urlAccess.Length; cnt++)
                    {
                        if (completeURL.IndexOf(urlAccess[cnt].ToLower().Trim()) > -1)
                            HttpContext.Current.Response.Redirect(redirectedPage);
                    }
                

            }
            catch (System.Threading.ThreadAbortException) { }
            catch (Exception ex)
            {

                log.Trace("Where =" + ex.StackTrace + "What =" + ex.Message);
            }

        }

        public string ControlConformityCheck(string txt)
        {
            if (HttpContext.Current.Session["roomCascadingControl"] != null && HttpContext.Current.Session["roomCascadingControl"].ToString().Equals("1"))
            {

                if (txt.IndexOf("%3c") > -1)
                    txt = txt.Replace("%3c", "");

                if (txt.IndexOf("%3e") > -1)
                    txt = txt.Replace("%3e", "");

                if (txt.IndexOf("&") > -1)
                    txt = txt.Replace("&", "");

                if (txt.IndexOf("<") > -1)
                    txt = txt.Replace("<", "");

                if (txt.IndexOf(">") > -1)
                    txt = txt.Replace(">", "");
            }
            return txt;
        }
        //ZD 100263 ENDS 

        public string GetTranslatedWeekMonth(string str) // ZD 100288
        {
            string[] days = { "day", "weekday", "weekend", "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };
            string[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
            string[] daynum = {"first", "second", "third", "fourth", "last"};

            if (str.IndexOf("day") > -1 || str.IndexOf("weekend") > -1)
            {
                for (int i = 0; i < 10; i++)
                {
                    if (str.IndexOf(days[i]) > -1)
                        str = str.Replace(days[i], GetTranslatedText(days[i]));
                }
            }

            for (int i = 0; i < 12; i++)
            {
                if (str.IndexOf(months[i]) > -1)
                {
                    str = str.Replace(months[i], GetTranslatedText(months[i]));
                    break;
                }
            }

            
            for (int i = 0; i < 5; i++)
            {
                if (str.IndexOf(daynum[i]) > -1)
                {
                    str = str.Replace(daynum[i], GetTranslatedText(daynum[i]));
                    break;
                }
            }
            
            return str;
        }

        public bool checkURLSessionStatus(string url) // ZD 100172
        {
            bool stat = false;

            string urls = "monitormcu.aspx,point2point.aspx,em7dashboard.aspx,MasterChildReport.aspx,InventoryManagement.aspx?t=1,ConferenceOrders.aspx?t=1,InventoryManagement.aspx?t=2,ConferenceOrders.aspx?t=2,InventoryManagement.aspx?t=3,ConferenceOrders.aspx?t=3";
            if (urls.IndexOf(url) == -1)
                return true;

            switch (url)
            {
                case "monitormcu.aspx":
                case "point2point.aspx":
                    if (HttpContext.Current.Session["EnableCallmonitor"] != null && HttpContext.Current.Session["EnableCallmonitor"].ToString().Equals("1"))
                        stat = true;
                    break;
                case "em7dashboard.aspx":
                    if (HttpContext.Current.Session["EnableEM7"] != null && HttpContext.Current.Session["EnableEM7"].ToString().Equals("1"))
                        stat = true;
                    break;


                case "MasterChildReport.aspx":
                    if (HttpContext.Current.Session["EnableAdvancedReport"] != null && HttpContext.Current.Session["EnableAdvancedReport"].ToString().Equals("1"))
                        stat = true;
                    break;


                case "InventoryManagement.aspx?t=1":
                case "ConferenceOrders.aspx?t=1":
                    if (HttpContext.Current.Session["roomModule"] != null && HttpContext.Current.Session["roomModule"].ToString().Equals("1"))
                        stat = true;
                    break;
                case "InventoryManagement.aspx?t=2":
                case "ConferenceOrders.aspx?t=2":
                    if (HttpContext.Current.Session["foodModule"] != null && HttpContext.Current.Session["foodModule"].ToString().Equals("1"))
                        stat = true;
                    break;
                case "InventoryManagement.aspx?t=3":
                case "ConferenceOrders.aspx?t=3":
                    if (HttpContext.Current.Session["hkModule"] != null && HttpContext.Current.Session["hkModule"].ToString().Equals("1"))
                        stat = true;
                    break;
            }
            return stat;
        }

        #region PasswotdEncrpyt
        /// <summary>
        /// PasswotdEncrpyt
        /// </summary>
        /// <param name="PW"></param>
        /// <returns></returns>
        public string PasswotdEncrpyt(string PW)
        {
            string Encrypted = "";
            XmlDocument docs = null;
            try
            {
                string inxmls = "<System><Cipher>" + PW + "</Cipher></System>";

                string outXML = CallMyVRMServer("GetEncrpytedText", inxmls, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("System/Cipher");
                    if (nde != null)
                        Encrypted = nde.InnerXml;
                }
                else
                {
                    errLabel.Text = ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                log.Trace("PasswotdEncrpyt:" + ex.Message);//ZD 100263
            }
            return Encrypted;
        }
        #endregion

        //ZD 100619 Starts
        #region BindBridgesType
        /// <summary>
        /// BindBridges
        /// </summary>
        /// <param name="sender"></param>
        public void BindBridgesType(DropDownList sender)
        {
            try
            {
                String inXML = "<GetBridges>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetBridges>";
                String outXML = CallMyVRMServer("GetBridges", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//Bridges/Bridge");
                LoadList(sender, nodes, "BridgeID", "BridgeType");
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 100619 Ends

        //ZD 101120 Starts
        #region changeToGMTTime
        /// <summary>
        /// PasswotdEncrpyt
        /// </summary>
        /// <param name="PW"></param>
        /// <returns></returns>
        public void changeToGMTTime(int timezoneID, ref DateTime ConfDateTime)
        {
            XmlDocument docs = null;
            try
            {
                string inxmls = "<GetGMTTime><TimeZoneID>" + timezoneID + "</TimeZoneID><DateTime>" + ConfDateTime.ToString() + "</DateTime></GetGMTTime>";

                string outXML = CallMyVRMServer("GetGMTTime", inxmls, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                if (outXML.IndexOf("<error>") < 0)
                {
                    docs = new XmlDocument();
                    docs.LoadXml(outXML);
                    XmlNode nde = docs.SelectSingleNode("GetGMTTime/GMTDateTime");
                    if (nde != null)
                        DateTime.TryParse(nde.InnerText.Trim(), out ConfDateTime);
                }
                else
                {
                    errLabel.Text = ShowErrorMessage(outXML);
                }
            }
            catch (Exception ex)
            {
                log.Trace("changeToGMTTime:" + ex.Message);
            }
        }
        #endregion
        //ZD 101120 Ends

        //ZD 101233 Starts

        internal bool CheckConferenceRights(int filterType, ref int hasView, ref int hasManage, ref int hasExtendTime, ref int hasMCUInfo, ref int hasEdit, ref int hasDelete, ref int hasClone)
        {
            try
            {
                if (filterType == 2)//Ongoing 
                {
                    if (HttpContext.Current.Session["hasOngoingView"] != null && HttpContext.Current.Session["hasOngoingView"].ToString() == "1")
                        hasView = 1;
                    if (HttpContext.Current.Session["hasOngoingManage"] != null && HttpContext.Current.Session["hasOngoingManage"].ToString() == "1")
                    {
                        hasDelete = 1;
                        hasManage = 1;
                    }
                    if (HttpContext.Current.Session["hasOngoingExtendTime"] != null && HttpContext.Current.Session["hasOngoingExtendTime"].ToString() == "1")
                        hasExtendTime = 1;
                    if (HttpContext.Current.Session["hasOngoingMCUInfo"] != null && HttpContext.Current.Session["hasOngoingMCUInfo"].ToString() == "1")
                        hasMCUInfo = 1;
                }
                else if (filterType == 4) //Public
                {

                    if (HttpContext.Current.Session["hasPublicView"] != null && HttpContext.Current.Session["hasPublicView"].ToString() == "1")
                        hasView = 1;
                    if (HttpContext.Current.Session["hasPublicEdit"] != null && HttpContext.Current.Session["hasPublicEdit"].ToString() == "1")
                        hasEdit = 1;
                    if (HttpContext.Current.Session["hasPublicDelete"] != null && HttpContext.Current.Session["hasPublicDelete"].ToString() == "1")
                        hasDelete = 1;
                    if (HttpContext.Current.Session["hasPublicManage"] != null && HttpContext.Current.Session["hasPublicManage"].ToString() == "1")
                        hasManage = 1;
                    if (HttpContext.Current.Session["hasPublicClone"] != null && HttpContext.Current.Session["hasPublicClone"].ToString() == "1")
                        hasClone = 1;
                }
                else if (filterType == 5)//Pending
                {

                    if (HttpContext.Current.Session["hasPendingView"] != null && HttpContext.Current.Session["hasPendingView"].ToString() == "1")
                        hasView = 1;
                    if (HttpContext.Current.Session["hasPendingEdit"] != null && HttpContext.Current.Session["hasPendingEdit"].ToString() == "1")
                        hasEdit = 1;
                    if (HttpContext.Current.Session["hasPendingDelete"] != null && HttpContext.Current.Session["hasPendingDelete"].ToString() == "1")
                        hasDelete = 1;
                    if (HttpContext.Current.Session["hasPendingManage"] != null && HttpContext.Current.Session["hasPendingManage"].ToString() == "1")
                        hasManage = 1;
                    if (HttpContext.Current.Session["hasPendingClone"] != null && HttpContext.Current.Session["hasPendingClone"].ToString() == "1")
                        hasClone = 1;
                }
                else if (filterType == 7)//Conference Support
                {

                    if (HttpContext.Current.Session["hasConfSupportView"] != null && HttpContext.Current.Session["hasConfSupportView"].ToString() == "1")
                        hasView = 1;
                    if (HttpContext.Current.Session["hasConfSupportManage"] != null && HttpContext.Current.Session["hasConfSupportManage"].ToString() == "1")
                        hasManage = 1;
                }
                else if (filterType == 8)//On MCU
                {
                    if (HttpContext.Current.Session["hasOnMCUView"] != null && HttpContext.Current.Session["hasOnMCUView"].ToString() == "1")
                        hasView = 1;
                    if (HttpContext.Current.Session["hasOnMCUManage"] != null && HttpContext.Current.Session["hasOnMCUManage"].ToString() == "1")
                        hasManage = 1;
                    if (HttpContext.Current.Session["hasOnMCUMCUInfo"] != null && HttpContext.Current.Session["hasOnMCUMCUInfo"].ToString() == "1")
                        hasMCUInfo = 1;
                    if (HttpContext.Current.Session["hasOnMCUDelete"] != null && HttpContext.Current.Session["hasOnMCUDelete"].ToString() == "1")
                        hasDelete = 1;
                }
                else if (filterType == 11) //Wait List //ZD 103095 Starts
                {
                    if (HttpContext.Current.Session["hasWaitListView"] != null && HttpContext.Current.Session["hasWaitListView"].ToString() == "1")
                        hasView = 1;
                    if (HttpContext.Current.Session["hasWaitListEdit"] != null && HttpContext.Current.Session["hasWaitListEdit"].ToString() == "1")
                        hasEdit = 1;
                    if (HttpContext.Current.Session["hasWaitListDelete"] != null && HttpContext.Current.Session["hasWaitListDelete"].ToString() == "1")
                        hasDelete = 1;
                    if (HttpContext.Current.Session["hasWaitListManage"] != null && HttpContext.Current.Session["hasWaitListManage"].ToString() == "1")
                        hasManage = 1;
                }//ZD 103095 End
                else if (filterType == 9 || filterType == 10) //9->Deleted or Terminated //10->Completed
                {
                    hasView = 1;
                    hasEdit = 0;
                    hasDelete = 0;
                    hasManage = 1;
                    if (filterType == 10)
                        hasClone = 1;
                    else
                        hasClone = 0;
                }
                else //Reservation and default-Doubt
                {
                    if (HttpContext.Current.Session["hasReservationsView"] != null && HttpContext.Current.Session["hasReservationsView"].ToString() == "1")
                        hasView = 1;
                    if (HttpContext.Current.Session["hasReservationsEdit"] != null && HttpContext.Current.Session["hasReservationsEdit"].ToString() == "1")
                        hasEdit = 1;
                    if (HttpContext.Current.Session["hasReservationsDelete"] != null && HttpContext.Current.Session["hasReservationsDelete"].ToString() == "1")
                        hasDelete = 1;
                    if (HttpContext.Current.Session["hasReservationsManage"] != null && HttpContext.Current.Session["hasReservationsManage"].ToString() == "1")
                        hasManage = 1;
                    if (HttpContext.Current.Session["hasReservationsClone"] != null && HttpContext.Current.Session["hasReservationsClone"].ToString() == "1")
                        hasClone = 1;
                }
            }
            catch (Exception ex)
            {
                log.Trace("CheckConferenceRights:" + ex.Message);
            }   
            return true;
        }

        //ZD 101233 End

        //ZD 101611 start
        #region
        /// <summary>
        /// Imageresize
        /// </summary>
        /// <param name="imageWidth"></param>
        /// <param name="imageHeight"></param>
        /// <param name="resizeWidth"></param>
        /// <param name="resizeHeight"></param>
        /// <param name="img"></param>
        /// <returns></returns>
        public byte[] Imageresize(int imageWidth, int imageHeight, int resizeWidth, int resizeHeight, System.Drawing.Image img)
        {
            try
            {
                double aspectRatio;
                int aspectWidth, aspectHight;
                double dresizewidth = Convert.ToDouble(resizeWidth);
                double dresizeHeight = Convert.ToDouble(resizeHeight);
                if (imageWidth > resizeWidth || imageHeight > resizeHeight)
                {

                    if (((double)imageWidth / imageHeight) > (dresizewidth / dresizeHeight))
                        aspectRatio = (double)imageWidth / dresizewidth;
                    else
                        aspectRatio = (double)imageHeight / dresizeHeight;
                    aspectWidth = (int)Math.Round(imageWidth / aspectRatio);
                    aspectHight = (int)Math.Round(imageHeight / aspectRatio);

                }
                else if (imageWidth < dresizewidth || imageHeight < dresizeHeight)
                {
                    if (((double)imageWidth / imageHeight) > (dresizewidth / dresizeHeight))
                        aspectRatio = dresizewidth / imageWidth;
                    else
                        aspectRatio = dresizeHeight / imageHeight;

                    aspectWidth = (int)Math.Round(imageWidth * aspectRatio);
                    aspectHight = (int)Math.Round(imageHeight * aspectRatio);
                }
                else
                {
                    aspectWidth = imageWidth;
                    aspectHight = imageHeight;
                }

                Bitmap bmp2 = new Bitmap(resizeWidth, resizeHeight, PixelFormat.Format24bppRgb);
                bmp2.SetResolution(72, 72);

                Graphics gfx = Graphics.FromImage(bmp2);
                gfx.SmoothingMode = SmoothingMode.AntiAlias;
                gfx.InterpolationMode = InterpolationMode.HighQualityBicubic;
                gfx.PixelOffsetMode = PixelOffsetMode.HighQuality;
                gfx.Clear(Color.LightGray);
                gfx.DrawImage(img, new Rectangle((int)Math.Round((double)(resizeWidth - aspectWidth) / 2), (int)Math.Round((double)(resizeHeight - aspectHight) / 2), aspectWidth, aspectHight), 0, 0, imageWidth, imageHeight, GraphicsUnit.Pixel);
                gfx.Dispose();

                MemoryStream ms = new MemoryStream();
                bmp2.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                byte[] imgArray = new byte[ms.Length];
                imgArray = ms.ToArray();
                return imgArray;
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
                throw ex;
            }
        }

        #endregion
        //ZD 101611 start

        //ZD 102909 - Start
        #region GetEntityCodes1
        /// <summary>
        /// GetEntityCodes1
        /// </summary>
        /// <param name="PW"></param>
        /// <returns></returns>
        public DataTable GetEntityCodes1(string prefixText)
        {
            StringBuilder inXml = new StringBuilder();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            DataTable dtEntityCode = new DataTable();
            XmlDocument xmlDoc = null;
            string outXML = "";
            DataRow[] datrow;
            try
            {
                if (HttpContext.Current.Session["EntityCodesVal"] == null)
                {
                    inXml.Append("<GetEntityCodes>");
                    inXml.Append(OrgXMLElement());
                    inXml.Append("<languageid>" + HttpContext.Current.Session["languageID"].ToString() + "</languageid>");
                    inXml.Append("</GetEntityCodes>");

                    outXML = CallMyVRMServer("GetEntityCodes", inXml.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());

                    xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(outXML);
                    ds = new DataSet();
                    ds.ReadXml(new XmlNodeReader(xmlDoc));

                    dtEntityCode = ds.Tables[0];

                    HttpContext.Current.Session.Add("EntityCodesVal", dtEntityCode);
                }
                else
                    dtEntityCode = (DataTable)HttpContext.Current.Session["EntityCodesVal"];

                 
                if(HttpContext.Current.Session["EditEntityCodeVal"] != null)
                     datrow = dtEntityCode.Select("Caption like '" + HttpContext.Current.Session["EditEntityCodeVal"].ToString() + "%'");
                else
                     datrow = dtEntityCode.Select("Caption like '" + prefixText + "%'");

                if (!dt.Columns.Contains("OptionID"))
                    dt.Columns.Add(new DataColumn("OptionID"));
                if (!dt.Columns.Contains("Caption"))
                    dt.Columns.Add(new DataColumn("Caption"));

                for (int row = 0; row < datrow.Length; row++)
                    dt.ImportRow(datrow[row]);
            }
            catch (Exception ex)
            {
                log.Trace("GetEntityCodes1:" + ex.Message);
            }
            return dt;
        }
        #endregion
        //ZD 102909 - End

        //ZD 103496 - Starts
        #region GetAllRoomsInfo
        /// <summary>
        /// GetAllRoomsInfo
        /// </summary>
        /// <param name="roomId"></param>
        /// <param name="disabled"></param>
        /// <param name="dtTable"></param>
        /// <returns></returns>
        public bool GetAllRoomsInfo(string roomId, string disabled, ref DataTable dtTable)
        {
            try
            {
                XmlWriter xWriter = null;
                XmlWriterSettings xSettings = new XmlWriterSettings();
                StringBuilder RoomXML = new StringBuilder();
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(RoomXML, xSettings))
                {
                    xWriter.WriteStartElement("GetAllRoomsBasicInfo");
                    xWriter.WriteElementString("UserID", HttpContext.Current.Session["userID"].ToString());
                    xWriter.WriteString(OrgXMLElement());
                    xWriter.WriteElementString("RoomID", roomId);
                    xWriter.WriteElementString("isMemCached", "1");
                    xWriter.WriteStartElement("SearchCriteria");
                    xWriter.WriteElementString("selQuery", " RoomID NOT IN (11) and ( VideoAvailable = 0 or VideoAvailable = 1 or VideoAvailable = 2 )");
                    xWriter.WriteElementString("Disabled", disabled);
                    xWriter.WriteElementString("Favorites", "0");
                    xWriter.WriteElementString("Department", "");
                    xWriter.WriteElementString("isEnableAVItem", "0");
                    xWriter.WriteElementString("ListAVItem", "");
                    xWriter.WriteElementString("AVItemNone", "0");
                    xWriter.WriteElementString("avItemQuery", "or");
                    xWriter.WriteFullEndElement();
                    xWriter.WriteElementString("pageNo", "0");
                    xWriter.WriteElementString("MaxRecords", "0");
                    xWriter.WriteFullEndElement();
                    xWriter.Flush();
                }
                RoomXML = RoomXML.Replace("&lt;", "<").Replace("&gt;", ">");
                string outXML = CallMyVRMServer("GetAllRoomsInfoforCache", RoomXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(outXML);
                    XmlNodeList nodes = xmldoc.SelectNodes("//Rooms/Room");
                    DataSet ds = new DataSet();
                    XmlTextReader txtrd = null;
                    if (nodes.Count > 0)
                    {
                        foreach (XmlNode node1 in nodes)
                        {
                            txtrd = new XmlTextReader(node1.OuterXml, XmlNodeType.Element, new XmlParserContext(null, null, null, XmlSpace.None));
                            ds.ReadXml(txtrd, XmlReadMode.InferSchema);
                        }
                        dtTable = ds.Tables[0];
                    }
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Trace("GetRoomsInfo:" + ex.Message);
                return false;
            }
            return true;
        }
        public bool GetAllRoomsInfo(string roomId, string disabled, ref DataTable dtTable, ref DataSet dsCache)
        {
            XmlDocument xmldoc = null;
            XmlReader txtrd = null;
            DataView dv = null;
            XmlWriter xWriter = null;
            XmlWriterSettings xSettings = new XmlWriterSettings();
            StringBuilder RoomXML = new StringBuilder();
            xSettings = new XmlWriterSettings();
            xSettings.OmitXmlDeclaration = true;
            try
            {
               
                using (xWriter = XmlWriter.Create(RoomXML, xSettings))
                {
                    xWriter.WriteStartElement("GetAllRoomsBasicInfo");
                    xWriter.WriteElementString("UserID", HttpContext.Current.Session["userID"].ToString());
                    xWriter.WriteString(OrgXMLElement());
                    xWriter.WriteElementString("RoomID", roomId);
                    xWriter.WriteElementString("isMemCached", "1");
                    xWriter.WriteStartElement("SearchCriteria");
                    xWriter.WriteElementString("selQuery", " RoomID NOT IN (11) and ( VideoAvailable = 0 or VideoAvailable = 1 or VideoAvailable = 2 )");
                    xWriter.WriteElementString("Disabled", disabled);
                    xWriter.WriteElementString("Favorites", "0");
                    xWriter.WriteElementString("Department", "");
                    xWriter.WriteElementString("isEnableAVItem", "0");
                    xWriter.WriteElementString("ListAVItem", "");
                    xWriter.WriteElementString("AVItemNone", "0");
                    xWriter.WriteElementString("avItemQuery", "or");
                    xWriter.WriteFullEndElement();
                    xWriter.WriteElementString("pageNo", "0");
                    xWriter.WriteElementString("MaxRecords", "0");
                    xWriter.WriteFullEndElement();
                    xWriter.Flush();
                }
                RoomXML = RoomXML.Replace("&lt;", "<").Replace("&gt;", ">");
                string outXML = CallMyVRMServer("GetAllRoomsInfoforCache", RoomXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") < 0)
                {
                    xmldoc.LoadXml(outXML);

                    if (dsCache == null)
                        dsCache = new DataSet();

                    txtrd = new XmlTextReader(xmldoc.OuterXml, XmlNodeType.Document, new XmlParserContext(null, null, null, XmlSpace.None));
                    dsCache.ReadXml(txtrd, XmlReadMode.InferSchema);

                    dv = new DataView(dsCache.Tables[0]);
                    dv.Sort = "Tier1Name,Tier2Name,RoomName ASC";
                    dtTable = dv.ToTable();
                    
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.Trace("GetRoomsInfo:" + ex.Message);
                return false;
            }
            return true;
        }

        public void GetAllRoomsInfo(string roomId, string disabled, ref String dtTable)
        {
            try
            {
                string outXML = "";
                XmlWriter xWriter = null;
                XmlWriterSettings xSettings = new XmlWriterSettings();
                StringBuilder RoomXML = new StringBuilder();
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(RoomXML, xSettings))
                {
                    xWriter.WriteStartElement("GetAllRoomsBasicInfo");
                    xWriter.WriteElementString("UserID", HttpContext.Current.Session["userID"].ToString());
                    xWriter.WriteString(OrgXMLElement());
                    xWriter.WriteElementString("RoomID", roomId);
                    xWriter.WriteElementString("isMemCached", "1");
                    xWriter.WriteStartElement("SearchCriteria");
                    xWriter.WriteElementString("selQuery", " RoomID NOT IN (11) and ( VideoAvailable = 0 or VideoAvailable = 1 or VideoAvailable = 2 )");
                    xWriter.WriteElementString("Disabled", disabled);
                    xWriter.WriteElementString("Favorites", "0");
                    xWriter.WriteElementString("Department", "");
                    xWriter.WriteElementString("isEnableAVItem", "0");
                    xWriter.WriteElementString("ListAVItem", "");
                    xWriter.WriteElementString("AVItemNone", "0");
                    xWriter.WriteElementString("avItemQuery", "or");
                    xWriter.WriteFullEndElement();
                    xWriter.WriteElementString("pageNo", "0");
                    xWriter.WriteElementString("MaxRecords", "0");
                    xWriter.WriteFullEndElement();
                    xWriter.Flush();
                }
                RoomXML = RoomXML.Replace("&lt;", "<").Replace("&gt;", ">");
                dtTable = CallMyVRMServer("GetAllRoomsInfo", RoomXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                
            }
            catch (Exception ex)
            {
                log.Trace("GetRoomsInfo:" + ex.Message);
                
            }
            
        }

        public void GetTablefromCache(ref Enyim.Caching.MemcachedClient memClient,  ref DataSet  dsCache)
        {
            String roomXML = "";
            XmlDocument xmldoc = new XmlDocument();
            DataView dv = null;
            
            XmlTextReader txtrd = null;
            try
            {
                if (memClient == null)
                    memClient = new Enyim.Caching.MemcachedClient();
                
                    roomXML = DecompressString(memClient.Get<String>("myVRMRoomsDatatable"));
                    if (String.IsNullOrEmpty(roomXML))
                        throw new Exception("Caching of rooms failed!!");

                    xmldoc.LoadXml(roomXML);

                    if (dsCache == null)
                        dsCache = new DataSet();

                    txtrd = new XmlTextReader(xmldoc.OuterXml, XmlNodeType.Document, new XmlParserContext(null, null, null, XmlSpace.None));
                    dsCache.ReadXml(txtrd, XmlReadMode.InferSchema);

            }
            catch (Exception ex)
            {
                log.Trace("GetRoomsfromcache:" + ex.Message);

            }
        }

        public void AddRoomTabletoCache(ref Enyim.Caching.MemcachedClient memClient, ref DataSet dsCache)
        {
            String roomXML = "";
            StringWriter txtWriter = null;
            try
            {
                if (dsCache == null)
                    throw new Exception("Error adding to cache");

                txtWriter = new StringWriter();
                dsCache.WriteXml(txtWriter);
                roomXML = txtWriter.ToString();

                if (!String.IsNullOrEmpty(roomXML))
                    memClient.Store(Enyim.Caching.Memcached.StoreMode.Set, "myVRMRoomsDatatable", CompressString(roomXML));
            }
            catch (Exception ex)
            {
                log.Trace("GetRoomsfromcache:" + ex.Message);

            }
           

        }

        #endregion
        //ZD 103496 - End

        #region Compress/Decompress String

        public static string CompressString(string text)
        {
            try
            {
                if (String.IsNullOrEmpty(text))
                    return "";

                byte[] buffer = Encoding.UTF8.GetBytes(text);
                var memoryStream = new MemoryStream();
                using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Compress, true))
                {
                    gZipStream.Write(buffer, 0, buffer.Length);
                }

                memoryStream.Position = 0;

                var compressedData = new byte[memoryStream.Length];
                memoryStream.Read(compressedData, 0, compressedData.Length);

                var gZipBuffer = new byte[compressedData.Length + 4];
                Buffer.BlockCopy(compressedData, 0, gZipBuffer, 4, compressedData.Length);
                Buffer.BlockCopy(BitConverter.GetBytes(buffer.Length), 0, gZipBuffer, 0, 4);
                return Convert.ToBase64String(gZipBuffer);

            }
            catch (Exception ex)
            {


            }

            return "";

        }

        /// <summary>
        /// Decompresses the string.
        /// </summary>
        /// <param name="compressedText">The compressed text.</param>
        /// <returns></returns>
        public static string DecompressString(string compressedText)
        {
            try
            {
                if (String.IsNullOrEmpty(compressedText))
                    return "";

                byte[] gZipBuffer = Convert.FromBase64String(compressedText);
                using (var memoryStream = new MemoryStream())
                {
                    int dataLength = BitConverter.ToInt32(gZipBuffer, 0);
                    memoryStream.Write(gZipBuffer, 4, gZipBuffer.Length - 4);

                    var buffer = new byte[dataLength];

                    memoryStream.Position = 0;
                    using (var gZipStream = new GZipStream(memoryStream, CompressionMode.Decompress))
                    {
                        gZipStream.Read(buffer, 0, buffer.Length);
                    }

                    return Encoding.UTF8.GetString(buffer);
                }

            }
            catch (Exception ex)
            {


            }

            return "";

        }

        #endregion

        //ZD 100040 start
        #region BindEptResolution
        /// <summary>
        /// BindEptResolution
        /// </summary>
        /// <param name="sender"></param>
        public void BindEptResolution(DropDownList sender)
        {
            try
            {
                String inXML = "<GetEptResolution>" + OrgXMLElement() + "<UserID>" + HttpContext.Current.Session["userID"].ToString() + "</UserID></GetEptResolution>";
                String outXML;
                outXML = CallMyVRMServer("GetEptResolution", inXML, HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outXML);
                XmlNodeList nodes = xmldoc.SelectNodes("//EptResolutionList/EptResolution");
                if (nodes.Count > 0)
                {
                    LoadList(sender, nodes, "RID", "Resolution");
                }
            }
            catch (Exception ex)
            {
                log.Trace("BindEptResolution: " + ex.Message);
            }
        }
        #endregion
        //ZD 100040  End
        //ZD 104391 - Start
        #region ReplaceOutXMLSpecialCharacters
        public string ReplaceOutXMLSpecialCharacters(string strXml, int control)
        {
            try
            {
                if (control == 1) //Textbox
                    strXml = strXml.Replace(ns_MyVRMNet.vrmSpecialChar.LessThan, "<")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.GreaterThan, ">")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.ampersand, "&");
                else if (control == 2) //Label
                    strXml = strXml.Replace(ns_MyVRMNet.vrmSpecialChar.LessThan, "&lt;")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.GreaterThan, "&gt;")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.ampersand, "&amp;")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.doubleQuotes, @"\""");
                else if (control == 3) //RSS Label
                    strXml = strXml.Replace(ns_MyVRMNet.vrmSpecialChar.LessThan, "&lt; ")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.GreaterThan, "&gt;")
                                   .Replace(ns_MyVRMNet.vrmSpecialChar.ampersand, "&amp;");
            }
            catch (Exception ex)
            {
                log.Trace("Util_OutXML :" + ex.StackTrace);
            }
            return strXml;
        }
        #endregion
        //ZD 104391 - End
			//ZD 104256 Starts
        #region BindPoolOrders
        /// <summary>
        /// BindPoolOrders
        /// </summary>
        /// <param name="lstMCUProfile"></param>
        /// <param name="BridgeID"></param>
        public void BindPoolOrders(DropDownList lstPoolOrder, String BridgeID)
        {
            String userID = "11";
            try
            {
                String outXML = "";
                StringBuilder inXML = new StringBuilder();
                XmlDocument xmlDoc = null;
                DataSet ds = null;
                if (HttpContext.Current.Session["userID"] != null)
                    userID = HttpContext.Current.Session["userID"].ToString();

                inXML.Append("<GetMCUPoolOrders>");
                inXML.Append("<UserID>" + userID + "</UserID>" + OrgXMLElement());
                inXML.Append("<MCUId>" + BridgeID + "</MCUId>");
                inXML.Append("</GetMCUPoolOrders>");
                outXML = CallMyVRMServer("GetMCUPoolOrders", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
                if (outXML.IndexOf("<error>") >= 0)
                {
                    errLabel.Text = ShowErrorMessage(outXML);
                    errLabel.Visible = true;
                }
                else
                {
                    xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(outXML);
                    ds = new DataSet();
                    ds.ReadXml(new XmlNodeReader(xmlDoc));
                    XmlNodeList nodes = xmlDoc.SelectNodes("//GetMCUPoolOrders/PoolOrder");
                    if (nodes.Count > 0)
                        LoadList(lstPoolOrder, nodes, "Id", "Name");
                }
            }
            catch (Exception ex)
            {
                errLabel.Visible = true;
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }
        #endregion
        //ZD 104256 Ends

        //ZD 104686
        #region GetAllCustomOptions

        public void GetAllCustomOptions(ref String outXml)
        {
            try
            {
                StringBuilder inXML = new StringBuilder();

                inXML.Append("<CustomAttribute>" + OrgXMLElement() + "<DeptID></DeptID></CustomAttribute>");
                outXml = CallMyVRMServer("GetCustomAttributes", inXML.ToString(), HttpContext.Current.Application["MyVRMServer_ConfigPath"].ToString());
            }
            catch (Exception ex)
            {
                log.Trace(ex.StackTrace + " : " + ex.Message);
            }
        }

        #endregion

    }


}
