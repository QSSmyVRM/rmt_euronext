﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*///ZD 100147 End ZD 100886
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using myVRM.DataLayer;
using System.Xml.Linq;
using NHibernate.Criterion;
using myVRM.DataLayer;

namespace myVRM.BusinessLayer
{
    //ZD 102829 Starts
    #region enum Transaltion Text Types

    //1 - ASPXCodeBehind Texts
    //2 - BusniessLayer Texts
    //3 - HardwareLayer Texts

    public enum TextType { None = 0, ASPXCodeBehind, BusniessLayer, HardwareLayer }; 
    
    #endregion
    //ZD 102829 Ends

    public class vrmSpecialChar
    {
        #region Const Special characters
        internal const String LessThan = "õ"; //Alt 0245
        internal const String GreaterThan = "æ"; //Alt 145
        internal const String ampersand = "σ"; //Alt 229
        #endregion
    }

    public class UtilFactory
    {
        #region Private Members
        private ILog m_log;
        private ILanguageTextDao m_ILanguagetext;//ZD 100288_4Dec2013
        private userDAO m_userDAO;
        #endregion

        #region Constructor
        public UtilFactory(ref vrmDataObject obj)
        {
            m_log = obj.log;
            m_userDAO = new userDAO(obj.ConfigPath, obj.log);
            m_ILanguagetext = m_userDAO.GetLanguageTextDao();//ZD 100288_4Dec2013
        }
        #endregion

        #region ReplaceInXMLSpecialCharacters
        internal string ReplaceInXMLSpecialCharacters(string strXml)
        {
            try
            {
                strXml = strXml.Replace(vrmSpecialChar.LessThan, "<")
                               .Replace(vrmSpecialChar.GreaterThan, ">")
                               .Replace(vrmSpecialChar.ampersand, "&");
            }
            catch (Exception ex)
            {
                m_log.Error("Util_InXML :" + ex.StackTrace);
            }
            return strXml;
        }
        #endregion

        #region ReplaceOutXMLSpecialCharacters
        internal string ReplaceOutXMLSpecialCharacters(string strXml)
        {
            try
            {
                strXml = strXml.Replace("<", vrmSpecialChar.LessThan)
                               .Replace(">", vrmSpecialChar.GreaterThan)
                               .Replace("&", vrmSpecialChar.ampersand);
            }
            catch (Exception ex)
            {
                m_log.Error("Util_OutXML :" + ex.StackTrace);
            }
            return strXml;
        }
        #endregion

        //ZD 100288_4Dec2013 Start

        #region Get Translated Text
        /// <summary>
        /// GetTranslatedText
        /// </summary>
        /// <param name="languageID"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        internal String GetTranslatedText(int languageID , String text)
        {
            String transText = "";
            try
            {
                transText = text;
               
                    if (languageID > 0)
                    {
                        if (languageID == 1)
                            return text;

                        if (text != null)
                        {
                            if (text.Trim() != "")
                            {
                                transText = text.Replace("\n", "").Replace("\r", "").ToString();
                                transText = GetText(languageID, transText);
                            }
                        }
                    }
                
            }
            catch (Exception ex)
            {

                m_log.Error("GetTranslatedText :" + ex.StackTrace);
            }

            return transText;
        }

        #endregion

        #region Get Text
        /// <summary>
        /// GetText
        /// </summary>
        /// <param name="languageID"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        private string GetText(int languageID , String text)
        {
            List<ICriterion> critList = new List<ICriterion>();
            string Transtext = "";
            try
            {
                critList.Add(Expression.Eq("LanguageID", languageID));
                critList.Add(Expression.Like("Text",  "%%" + text + "%%"));
                critList.Add(Expression.Eq("TextType", 2)); //ZD 102829

                List<vrmLanguageText> languagetexts = m_ILanguagetext.GetByCriteria(critList, true);
                if (languagetexts != null)
                {
                    if (languagetexts.Count > 0)
                    {
                        languagetexts = languagetexts.Where(l => l.Text.Trim().ToUpper() == text.Trim().ToUpper()).ToList();
                        if (languagetexts.Count > 0)
                        {
                            return Transtext = languagetexts[0].TranslatedText.Replace("\r", "").Replace("\n", "").ToString();
                        }

                    }
                }
            }
            catch (Exception ex)
            {

                m_log.Error("GetTranslatedText :" + ex.StackTrace);
            }

            return text;
        }

        #endregion

        //ZD 100288_4Dec2013 End
    }
}
