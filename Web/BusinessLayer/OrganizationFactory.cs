//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using NHibernate;
using NHibernate.Criterion;
using System.Threading;
using System.Linq;
//FB 2639 Start 
using System.IO;
using System.Xml.XPath;
//FB 2639 End
using log4net;
using myVRM.DataLayer;
using System.Globalization;

namespace myVRM.BusinessLayer
{
    /// <summary>
    /// Summary description for myVRMSearch
    /// </summary>
    /// 
    //ZD 101835
    public enum eConfAchivePeriod { Monthly = 1, Quarterly, SemiAnnually, Annually };
    public enum eConfAchiveOldeThanByMonth { M1 = 1, M3, M6, M9, M12 };
    public enum eBJNMeetingType {PersonalMeeting=1, OneTimeMeeting }; //ZD 104021

    public class OrganizationFactory
    {
        #region Private Members

        private myVRMException myvrmEx;
        private const int defaultOrgID = 11;    //Defaul organization UID
        private static log4net.ILog m_log;
        private string m_configPath;

        private SystemDAO m_systemDAO;
        private orgDAO m_OrgDAO;
        private GeneralDAO m_generalDAO;
        private userDAO m_userDAO;
        private LocationDAO m_locDAO;
        private hardwareDAO m_Hardware;

        private IVrmVidyoSettingsDAO m_IVidyoSetDAO; //FB 2262 //FB 2599
        private ISysTimeZonePrefDAO m_ISysTimeZonePrefDAO;
        private IAccSchemeDAO m_IAccSchemeDAO;
        private IOrgDAO m_IOrgDAO;
        private IOrgSettingsDAO m_IOrgSettingDAO;
        private ISysTechDAO m_ISysTechDAO;
        private ISysApproverDAO m_ISysApproverDAO;
        private IStateDAO m_IStateDAO;
        private ICountryDAO m_ICountryDAO;
        private IUserDao m_IuserDAO;
        private IRoomDAO m_IRoomDAO;
        private IMCUDao m_vrmMCU;
        private IDiagnosticDAO m_IDiagnosticsDAO;
        private vrmSystemFactory sysFact;
        private IConfMessageDao m_IConfMessageDao; //FB 2486
        private IMessageDao m_IMessageDao; //FB 2486
        //ZD 101176 Starts
        private ILanguageDAO m_ILanguageDAO;
        private deptDAO m_deptDAO;
        private IDeptCustomAttrDao m_IDeptCustDAO; 
        private ICustomLangDao m_ICustomAttrLangDAO;
        //ZD 101176 Ends
        private ILDAPGroupsDAO m_ILDAPGroupsDAO;//ZD 101525
        private IUserTemplateDao m_ItempDao; //ZD 102052

        internal int organizationID;
        private int orgRoomLimit;
        private int orgVideoRoomLimit;
        private int orgNVRoomLimit;
        private int orgEndPointLimit;
        private int orgMCULimit;
        private int orgMCUEnchancedLimit;//FB 2486
        private int orgUserLimit;
        private int orgExchangeUserLimit;
        private int orgDominoUserLimit;
        private int orgMobileUserLimit; //FB 1979
        private int orgWebexUserLimit; //ZD 100221
        private int loginUser;
        private int enableAV;
        private int enableCatering;
        private int enableHK;
        private int enableAPI;
        //private int enablePCs; //FB 2347 //FB 2693
        private int orgVMRRoomLimit;//FB 2586
        private int enableClouds; //FB 2262//FB 2599
        private int enablePublicRoom; //FB 2594
        private int orgSeats; //FB 2659
        private int orgBJNUserLimit, enableJabber, enableLync, enableVidtel, orgPcUserLimit; //FB 2693 //ZD 104021
		private int orgVCHotRoomLimit, orgROHotRoomLimit; //FB 2694
        private int enableAdvReport;//FB 2593
        private int orgiControlRoomLimit;//ZD 101098
        private OrgData orgInfo;
        private IList<sysApprover> sysAppList = null;
        private vrmFactory vrmfact = null; //Image Project
        private imageDAO m_imageDAO = null;
        private IImageDAO m_IImageDAO = null;
        private imageFactory vrmImg = null; //FB 2136
        //FB 2426 Start
        private RoomFactory m_RoomFactory;
        private int orgGuestRoomLimit; 
        private int orgGuestRoomPerUserLimit;
        private myVRMSearch m_myVRMSearch;
        //FB 2426 End
        //FB 1830
        private IEmailLanguageDao m_IEmailLanguageDAO;
        private conferenceDAO m_confDAO;

        private IHolidaysDAO m_IHolidayDAO;// FB 1861
        private emailFactory m_emailFactory;// FB 1860
        private IHolidaysTypeDAO m_IHolidayDAOType;// FB 1861
        private IvrmErrorLogDAO m_IvrmErrLog;//FB 2027(DeleteModuleLog) 
        private errorlogDAO m_errorDAO;
        private IEmailDomainDAO m_IEmailDomainDAO; //FB 2154
        private IOrgLicAgreementDAO m_IOrgLicAgrDAO; //FB 2337
        private IESMailUsrRptSettingsDAO m_ESMailUsrRptSetDAO; //FB 2363
        private IEM7OrgSettingsDAO m_EM7SettingsDao; //FB 2501 EM7
        private IDefaultLicenseDAO m_IDefaultLicense; //FB 2659
        private ILDAPConfigDAO m_ILDAPConfigDAO; //ZD 101443
        private IUserRolesDao m_IUserRolesDao;

        private IConfArchiveSettingsDao m_IConfArchiveSettingsDao; //ZD 101835

        // FB 2639 Start  
        XmlWriter xWriter = null;
        XmlWriterSettings xSettings = null;
        XPathNavigator xNavigator = null;
        XPathDocument xDoc = null;
        StringReader xStrReader = null;
        XPathNavigator xNode = null;
        StringBuilder OrgOUTXML = null;
        // FB 2639 End
        private int orgMaxParticipants, orgMaxConcurrentCall;//ZD 100518
        private UtilFactory m_UtilFactory; //ZD 104391


        #endregion

        #region Constructors
        /// <summary>
        /// construct report factory with session reference
        /// </summary>
        /// 

        public OrganizationFactory()
        {
            //Default Constructor
        }
        public OrganizationFactory(ref vrmDataObject obj)
        {
            try
            {
                init(obj.ConfigPath, obj.log);
                vrmImg = new imageFactory(ref obj); // FB 2307
                m_RoomFactory = new RoomFactory(ref obj);//FB 2426
                m_myVRMSearch = new myVRMSearch(obj);//FB 2426
                m_UtilFactory = new UtilFactory(ref obj); //ZD 104391
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        public OrganizationFactory(string ConfigPath, log4net.ILog log)
        {
            try
            {
                init(ConfigPath, log);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region Private Methods

        #region Page Init
        private bool init(string ConfigPath, log4net.ILog log)
        {
            try
            {
                m_configPath = ConfigPath;
                m_log = log;

                m_systemDAO = new SystemDAO(m_configPath, log);
                m_OrgDAO = new orgDAO(m_configPath, log);
                m_generalDAO = new GeneralDAO(m_configPath, log);
                m_userDAO = new userDAO(m_configPath, log);
                m_locDAO = new LocationDAO(m_configPath, log);
                m_Hardware = new hardwareDAO(m_configPath, log);
                m_imageDAO = new imageDAO(m_configPath, log); //Image Project
                m_confDAO = new conferenceDAO(m_configPath, m_log); //FB 1830
                m_deptDAO = new deptDAO(m_configPath, m_log); //ZD 101176

                m_IOrgDAO = m_OrgDAO.GetOrgDao();
                m_IOrgSettingDAO = m_OrgDAO.GetOrgSettingsDao();
                m_ISysTechDAO = m_OrgDAO.GetSysTechDao();
                m_ISysApproverDAO = m_OrgDAO.GetSysApproverDao();
                m_IStateDAO = m_generalDAO.GetStateDAO();
                m_ICountryDAO = m_generalDAO.GetCountryDAO();
                m_IuserDAO = m_userDAO.GetUserDao();
                m_IRoomDAO = m_locDAO.GetRoomDAO();
                m_vrmMCU = m_Hardware.GetMCUDao();
                m_ISysTimeZonePrefDAO = m_systemDAO.GetSysTimeZonePrefDao();
                m_IAccSchemeDAO = m_systemDAO.GetAccSchemeDao();
                m_IDiagnosticsDAO = m_OrgDAO.GetDiagnosticDAO();
                m_IImageDAO = m_imageDAO.GetImageDao(); //Image Project
                m_IVidyoSetDAO = m_OrgDAO.GetVidyoSettingsDAO(); //FB 2262 //FB 2599
				m_IConfMessageDao = m_confDAO.GetConfMessageDao(); //FB 2486
                m_IMessageDao = m_Hardware.GetMessageDao(); //FB 2486
                m_ILanguageDAO = m_generalDAO.GetLanguageDAO();
                
                //ZD 101176 Starts
                m_IDeptCustDAO = m_deptDAO.GetDeptCustomAttrDao();
                m_ICustomAttrLangDAO = m_deptDAO.GetCustomLangDao(); 
                //ZD 101176 Ends

                m_IEmailLanguageDAO = m_confDAO.GetEmailLanguageDao(); //FB 1830
                m_errorDAO = new errorlogDAO(m_configPath, m_log);//FB 2027(DeleteModuleLog)
                m_IvrmErrLog = m_errorDAO.GetErrorLogDAO();
				m_IEmailDomainDAO = m_OrgDAO.GetEmailDomainDAO(); //FB 2154
				m_IHolidayDAOType = m_OrgDAO.GetHolidaysTypeDAO();//FB 2052
                m_IOrgLicAgrDAO = m_OrgDAO.GetOrgLicAgreementDAO(); //FB 2337
                m_ESMailUsrRptSetDAO = m_systemDAO.GetESMailUsrRptSettingsDAO(); //FB 2363
                m_EM7SettingsDao = m_OrgDAO.GetEM7Settings(); //FB 2501 EM7
                m_IDefaultLicense = m_systemDAO.GetDefaultLicenseDao(); //FB 2659
                m_ILDAPConfigDAO = m_OrgDAO.GetLDAPConfigDao();//ZD 101443
				m_ILDAPGroupsDAO = m_OrgDAO.GetLDAPGroupDao(); //ZD 101525
                m_IUserRolesDao = m_userDAO.GetUserRolesDao(); 
                m_ItempDao = m_userDAO.GetUserTemplateDao();//ZD 102052
                m_IConfArchiveSettingsDao = m_OrgDAO.GetConfArchiveSettingsDao() ; //ZD 101835
                myvrmEx = new myVRMException();
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region Check whether conferences requires system approval
        /// <summary>
        /// Check whether conferences requires system approval
        /// </summary>
        /// <param name="approverList"></param>
        /// <returns></returns>
        private bool IsSysApprovalRequired(List<int> approverList)
        {
            List<int> confIDs = new List<int>();
            List<int> appIDs = new List<int>();// FB 1936
            sysApprover sysApp = null;
            vrmConfApproval vca = null;
            int i=0;
            try
            {
                DateTime currentUTCTime = DateTime.UtcNow;

                if (approverList == null)
                    approverList = new List<int>();

                // FB 1936
                if (sysAppList == null)
                    sysAppList = m_ISysApproverDAO.GetSysApproversByOrgId(organizationID);

                for (i = 0; i < sysAppList.Count; i++)
                {
                    sysApp = null;
                    sysApp = sysAppList[i];
                    appIDs.Add(sysApp.approverid);
                }
                approverList.Sort();
                appIDs.Sort();


                bool isNewApprover = approverList.SequenceEqual(appIDs);

                if (isNewApprover)
                    return false;
                // FB 1936
                if (approverList.Count <= 0)
                {
                    approverList = new List<int>();
                    if (sysAppList != null)
                    {
                        for (i = 0; i < sysAppList.Count; i++)
                        {
                            sysApp = null;
                            sysApp = sysAppList[i];
                            approverList.Add(sysApp.approverid);
                        }
                    }
                }

                conferenceDAO m_confDAO = new conferenceDAO(m_configPath, m_log);
                IConfApprovalDAO m_IApprover = m_confDAO.GetConfApprovalDao();
                IConferenceDAO m_IconfDao = m_confDAO.GetConferenceDao();

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("approverid", appIDs)); // System Approver Issue
                criterionList.Add(Expression.Eq("decision", 0));
                criterionList.Add(Expression.Eq("entitytype", 4));
                List<vrmConfApproval> ConfApprovalList = m_IApprover.GetByCriteria(criterionList);
                if (ConfApprovalList != null)
                {
                    for (i = 0; i < ConfApprovalList.Count; i++)
                    {
                        vca = null;
                        vca = ConfApprovalList[i];
                        confIDs.Add(vca.confid);
                    }
                }
                criterionList = null;
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("confid", confIDs));
                criterionList.Add(Expression.Ge("confdate", currentUTCTime));
                criterionList.Add(Expression.Eq("status", 1));
                criterionList.Add(Expression.Eq("deleted", 0));
                List<vrmConference> approvalPendConfs = m_IconfDao.GetByCriteria(criterionList);

                if (approvalPendConfs != null)
                {
                    if (approvalPendConfs.Count > 0)
                        return true;
                }
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return true;
            }
        }
        #endregion

        #region Set System Approvers for Organization
        /// <summary>
        /// Set System Approvers for Organization
        /// </summary>
        /// <param name="sysApproversList"></param>
        /// <returns></returns>
        private bool SetSystemApprovers(List<int> sysApproversList)
        {
            List<sysApprover> sysApprsList = null;
            int approverID = 0;
            sysApprover sysApp = null;
            try
            {
                if (sysApproversList == null)
                    return false;

                if (sysApproversList.Count < 1)
                    return false;

                sysApprsList = new List<sysApprover>();
                for (approverID = 0; approverID < sysApproversList.Count; approverID++)
                {
                    sysApp = new sysApprover();
                    sysApp.approverid = sysApproversList[approverID];//FB 2441
                    sysApp.OrgId = organizationID;
                    sysApprsList.Add(sysApp);
                }
                m_ISysApproverDAO.SaveOrUpdateList(sysApprsList);
                
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region Get Maximum OrgID
        /// <summary>
        /// Get Maximum OrgID
        /// </summary>
        /// <returns></returns>
        private int getMaxOrgID()
        {
            try
            {
                string qString = "select max(LA.orgId) from myVRM.DataLayer.vrmOrganization LA";
                IList list = m_IOrgDAO.execQuery(qString);

                string sMax = "";

                if (list != null)
                {
                    if (list.Count > 0)
                    {
                        if (list[0] != null && list[0].ToString().Trim() != "")
                        {
                            sMax = list[0].ToString().Trim();
                        }
                    }
                }
                int maxId = 0;
                int.TryParse(sMax, out maxId);

                return maxId;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region Remove System Approvers for Organization
        /// <summary>
        /// Remove System Approvers for Organization
        /// </summary>
        /// <returns></returns>
        private bool RemoveSystemApprovers()
        {
            try
            {
                sysApprover sysAppr = null;
                int i = 0;
                if(sysAppList == null)
                    sysAppList = m_ISysApproverDAO.GetSysApproversByOrgId(organizationID);

                if (sysAppList == null)
                    return true;

                if (sysAppList.Count < 1)
                    return true;

                m_ISysApproverDAO.clearFetch();

                for (i = 0; i < sysAppList.Count; i++)
                {
                    sysAppr = null;
                    sysAppr = sysAppList[i];
                    m_ISysApproverDAO.Delete(sysAppr);
                }
                
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region Create Dflt TechData
        /// <summary>
        /// Create Dflt TechData
        /// </summary>
        /// <returns></returns>
        private bool CreateDfltTechData()
        {
            try
            {
                sysTechData sysTechDta;
                sysTechData sysTechDtaNew = new sysTechData();

                sysTechDta = m_ISysTechDAO.GetTechByOrgId(defaultOrgID);  //Default organization data

                if (sysTechDta == null)
                {
                    sysTechDta = new sysTechData();
                    sysTechDta.OrgId = organizationID;
                }

                sysTechDtaNew.name = sysTechDta.name;
                sysTechDtaNew.email = sysTechDta.email;
                sysTechDtaNew.phone = sysTechDta.phone;
                sysTechDtaNew.info = sysTechDta.info;
                sysTechDtaNew.OrgId = organizationID;

                m_ISysTechDAO.Save(sysTechDtaNew);
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException occured in Inserting Default Sys_techSupport_d", ex);
                return false;
            }
        }
        #endregion

        #region Create Default Organization Data
        /// <summary>
        /// Create Default Organization Data
        /// </summary>
        /// <returns></returns>
        private bool CreateDfltOrgData()
        {
            OrgData baseOrgData = null;
            OrgData newOrgData = null;
            DateTime currentDate = DateTime.Now;
            ns_SqlHelper.SqlHelper sqlCon = null; //FB 2693
            

            try
            {
                baseOrgData = m_IOrgSettingDAO.GetByOrgId(defaultOrgID);

                if (baseOrgData == null)
                    return false;

                newOrgData = new OrgData();

                timeZone.changeToGMTTime(sysSettings.TimeZone, ref currentDate);

                newOrgData.OrgId = organizationID;
                newOrgData.LastModified = currentDate;
                newOrgData.LastModifiedUser = loginUser;
                newOrgData.tzsystemid = baseOrgData.tzsystemid;
                newOrgData.Logo = baseOrgData.Logo;
                newOrgData.CompanyTel = baseOrgData.CompanyTel;
                newOrgData.CompanyEmail = baseOrgData.CompanyEmail;
                newOrgData.CompanyURL = baseOrgData.CompanyURL;
                newOrgData.Connect2 = baseOrgData.Connect2;
                newOrgData.DialOut = baseOrgData.DialOut;
                newOrgData.AccountingLogic = 0;//ZD 101127 None- Default
                newOrgData.BillPoint2Point = baseOrgData.BillPoint2Point;
                newOrgData.AllLocation = baseOrgData.AllLocation;
                newOrgData.RealtimeStatus = baseOrgData.RealtimeStatus;
                newOrgData.BillRealTime = baseOrgData.BillRealTime;
                newOrgData.MultipleDepartments = baseOrgData.MultipleDepartments;
                newOrgData.overAllocation = 1;//ZD 101127  Yes- Default
                newOrgData.Threshold = baseOrgData.Threshold;
                newOrgData.autoApproveImmediate = baseOrgData.autoApproveImmediate;
                newOrgData.adminEmail1 = baseOrgData.adminEmail1;
                newOrgData.adminEmail2 = baseOrgData.adminEmail2;
                newOrgData.adminEmail3 = baseOrgData.adminEmail3;
                newOrgData.AutoAcceptModConf = baseOrgData.AutoAcceptModConf;
                newOrgData.recurEnabled = baseOrgData.recurEnabled;
                newOrgData.responsetime = baseOrgData.responsetime;
                newOrgData.responsemessage = baseOrgData.responsemessage;
                newOrgData.SystemStartTime = baseOrgData.SystemStartTime;
                newOrgData.SystemEndTime = baseOrgData.SystemEndTime;
                newOrgData.Open24hrs = baseOrgData.Open24hrs;
                newOrgData.Offdays = baseOrgData.Offdays;
                newOrgData.ISDNLineCost = baseOrgData.ISDNLineCost;
                newOrgData.ISDNPortCost = baseOrgData.ISDNPortCost;
                newOrgData.IPLineCost = baseOrgData.IPLineCost;
                newOrgData.IPPortCost = baseOrgData.IPPortCost;
                newOrgData.ISDNTimeFrame = baseOrgData.ISDNTimeFrame;
                newOrgData.dynamicinviteenabled = baseOrgData.dynamicinviteenabled;
                newOrgData.doublebookingenabled = baseOrgData.doublebookingenabled;
                newOrgData.IMEnabled = baseOrgData.IMEnabled;
                newOrgData.IMRefreshConn = baseOrgData.IMRefreshConn;
                newOrgData.IMMaxUnitConn = baseOrgData.IMMaxUnitConn;
                newOrgData.IMMaxSysConn = baseOrgData.IMMaxSysConn;
                newOrgData.DefaultToPublic = baseOrgData.DefaultToPublic;
                newOrgData.RoomLimit = orgRoomLimit;
                newOrgData.MCULimit = orgMCULimit;
                newOrgData.UserLimit = orgUserLimit;
                newOrgData.MaxVideoRooms = orgVideoRoomLimit;
                newOrgData.MaxNonVideoRooms = orgNVRoomLimit;
                newOrgData.MaxEndpoints = orgEndPointLimit;
                newOrgData.EnableAPI = enableAPI;
                newOrgData.EnableFacilities = enableAV;
                newOrgData.EnableHousekeeping = enableHK;
                newOrgData.EnableCatering = enableCatering;
                newOrgData.ExchangeUserLimit = orgExchangeUserLimit;
                newOrgData.MCUEnchancedLimit = orgMCUEnchancedLimit;//FB 2486
                newOrgData.MaxVMRRooms = orgVMRRoomLimit;//FB 2586
                newOrgData.MaxiControlRooms = orgiControlRoomLimit;//ZD 101098
                //FB 2426 Start
                newOrgData.GuestRoomLimit = orgGuestRoomLimit;
                newOrgData.GuestRoomPerUser = orgGuestRoomPerUserLimit;
                //FB 2426 End
                newOrgData.DominoUserLimit = orgDominoUserLimit;
                newOrgData.MobileUserLimit = orgMobileUserLimit; //FB 1979
                newOrgData.WebexUserLimit = orgWebexUserLimit; //ZD 100221
                newOrgData.BlueJeansUserLimit = orgBJNUserLimit; //ZD 104021
                newOrgData.DefaultConferenceType = baseOrgData.DefaultConferenceType;
                newOrgData.RFIDTagValue = baseOrgData.RFIDTagValue;//FB 2724
                newOrgData.iControlTimeout = baseOrgData.iControlTimeout;//FB 2724
                newOrgData.VideoRefreshTimer = baseOrgData.VideoRefreshTimer;//ZD 103398
                newOrgData.EnableRoomConference = baseOrgData.EnableRoomConference;
                newOrgData.EnableHotdeskingConference = baseOrgData.EnableHotdeskingConference;//ZD 100719
                newOrgData.EnableAudioVideoConference = baseOrgData.EnableAudioVideoConference;
                newOrgData.EnableAudioOnlyConference = baseOrgData.EnableAudioOnlyConference;
                newOrgData.EnableNumericID = baseOrgData.EnableNumericID;//FB 2870
                newOrgData.DefaultCalendarToOfficeHours = baseOrgData.DefaultCalendarToOfficeHours;
                newOrgData.RoomTreeExpandLevel = baseOrgData.RoomTreeExpandLevel;
                newOrgData.EnableCustomOption = baseOrgData.EnableCustomOption;
                newOrgData.EnableBufferZone = "0"; //ZD 101755
                newOrgData.CustomAttributeLimit = baseOrgData.CustomAttributeLimit; //FB 1779
                newOrgData.Language = baseOrgData.Language; //FB 1830
                newOrgData.EmailLangId = baseOrgData.Language; //FB 1830 
                newOrgData.MailBlocked = 0; //FB 1860
                newOrgData.MailBlockedDate = DateTime.Now; //FB 1860
                newOrgData.WorkingHours = baseOrgData.WorkingHours; //FB 2343
				//newOrgData.EnablePCModule = enablePCs; //FB 2347 //FB 2693
                newOrgData.SetupTime = baseOrgData.SetupTime;//FB 2398
                newOrgData.TearDownTime = baseOrgData.TearDownTime;
                newOrgData.DefaultConfDuration = baseOrgData.DefaultConfDuration;//FB 2635
                newOrgData.EnableCloud = enableClouds; //FB 2262//FB 2599
                //FB 2693 Starts
                newOrgData.PCUserLimit = orgPcUserLimit; 
                //newOrgData.EnableBlueJeans = enableBJ; //ZD 104021
                newOrgData.EnableJabber = enableJabber;
                newOrgData.EnableLync = enableLync;
                newOrgData.EnableVidtel = enableVidtel ;
                //FB 2693 Ends
                newOrgData.EnableAdvancedReport = enableAdvReport;//FB 2593
                newOrgData.MaxROHotdesking = orgROHotRoomLimit;//FB 2838
                newOrgData.MaxVCHotdesking = orgVCHotRoomLimit;//FB 2838
                newOrgData.EnableProfileSelection = baseOrgData.EnableProfileSelection;//FB 2839
                newOrgData.EnableRoomCalendarView = 3;//ZD 100963

                //ZD 100781
                newOrgData.FooterMessage = "";
                newOrgData.IcalReqEmailID = "";
                newOrgData.SecurityDeskEmailId = "";
                newOrgData.SurveyURL = "";
                newOrgData.CustomerID = "";
                newOrgData.CustomerName = "";
                newOrgData.AlertforTier1 = "";
                newOrgData.HardwareAdminEmail = "";
                newOrgData.FileWhiteList = "";
                newOrgData.WebExURL = "";
                newOrgData.WebExSiteID = "";
                newOrgData.WebExPartnerID = "";
                newOrgData.PasswordMonths = 3;
                newOrgData.VideoSourceURL1 = "";
                newOrgData.VideoSourceURL2 = "";
                newOrgData.VideoSourceURL3 = "";
                newOrgData.VideoSourceURL4 = "";
                newOrgData.ScreenPosition1 = "";
                newOrgData.ScreenPosition2 = "";
                newOrgData.ScreenPosition3 = "";
                newOrgData.ScreenPosition4 = "";
                //ZD 100781
                //ZD 101120 Starts
                newOrgData.GuestLocApprovalTime = 1;
                newOrgData.EnableGuestLocWarningMsg = 0;
                //ZD 101120 Ends
   				//ZD 100522 Starts
                newOrgData.VMRPINChange = 0;
                newOrgData.RoomDenialCommt = 0;//ZD 101445
                newOrgData.PasswordCharLength = 4;
                //ZD 100522 Ends
                newOrgData.SmartP2PNotify = 1;//ZD 100815
                //ZD 101228 Starts
                newOrgData.AVWOAlertTime = 0;
                newOrgData.CatWOAlertTime = 0;
                newOrgData.FacilityWOAlertTime = 0;
                //ZD 101228 Ends
 				newOrgData.EnableWebExIntg = 0; //ZD 100513
                newOrgData.WebExLaunch = 0; //ZD 100513
                newOrgData.EnableTemplateBooking = 0;//ZD 101562
                newOrgData.EnableSetupTimeDisplay = 0; //ZD 101755
                newOrgData.EnableTeardownTimeDisplay = 0; //ZD 101755
				newOrgData.EnableActMsgDelivery = 0;//ZD 101757
				 //ZD 101527 - Start
                newOrgData.EnableRPRMRoomSync = 0;
                newOrgData.RoomSyncAuto = 0;
                newOrgData.RoomSyncPollTime = 0;
                //ZD 101527 - End

                //FB 2717 Starts
                if (newOrgData.EnableCloud == 1)
                {
                    newOrgData.EnableAudioVideoConference = 1;
                    newOrgData.isAssignedMCU = 0;
                }
                //if (newOrgData.EnableCloud == 1) //FB 2599
                //{
                //    newOrgData.EnableAudioBridges = 0;
                //    newOrgData.DefaultConferenceType = 2;
                //    newOrgData.EnableVMR = 3;
                //}
                //FB 2717 Starts

                //ZD 100166 Starts
                string newInvitation = ""; //ZD 100395
                if (sysSettings.EnableCloudInstallation == 1)
                {
                    newOrgData.EnableBufferZone = "0"; 
                    newOrgData.SetupTime = 0; 
                    newOrgData.TearDownTime = 0; 
                    newOrgData.McuSetupTime = 0; 
                    newOrgData.MCUTeardonwnTime = 0; 
                    newOrgData.MCUSetupDisplay = 0; 
                    newOrgData.MCUTearDisplay = 0;
					//ZD 100395 Starts
                    newOrgData.DefaultSubject = "SCOPIA Cloud Video Discussion"; //ZD 100395
                    newInvitation = "You have been invited to attend a SCOPIA Meeting. At the time of the meeting, please choose an access method described below:\r\n\r\nDesktop / Mobile Link:\r\nTo CONNECT from your computer or mobile device click the following link:\r\n{72}?ID={71}&amp;autojoin\r\n\r\nFor other options (including connecting with presentation only or watching the webcast), go to {72}?ID={71}\r\n\r\nH.323 dial-in:\r\nTo connect from any H.323 device (i.e. Polycom, LifeSize, Radvision, Tandberg/CISCO, etc.) dial by using one of the following numbers:\r\n{71}@159.54.57.11 or\r\n159.54.57.11##{71}\r\n\r\n";
                    newInvitation += "Alternatively dial 159.54.57.11, then when prompted enter your meeting ID - {71} via DTMF tones (# sign brings up DTMF key pad on most systems) followed by the # sign\r\n\r\nAudio Dial in numbers\r\n\r\nToll free: 888.238.5279\r\n\r\nAt the voice prompt enter your Meeting ID {71} followed by the # sign\r\n\r\nWhat Do I Need to Get Started? \r\nBelow are active links to all the varying support documentation to help get you started. Please click any of the following links to provide more information regarding your SCOPIA virtual rooms.\r\n\r\n";
                    newInvitation += "SCOPIA Best Practices\r\nBe sure to check your audio and video settings before calls  Consult the Best Practice Guide: http://specs.ucanytime.com/Virtual_Meeting_Room_Quick_Start_Guide.pdf\r\n\r\nWhen you join a SCOPIA call, youre un-muted to start. Its best to mute your microphone when not actively speaking. Alternatively your meeting room can be configured so that all participants enter muted by default.";
                    newOrgData.DefaultInvitation = newInvitation;
                }
                else
                {
                    newOrgData.DefaultSubject = "";
                    newOrgData.DefaultInvitation = "";
                }
				//ZD 100395 End
                //ZD 100166 End

                newOrgData.EnablePublicRoomService = enablePublicRoom; //FB 2594
                
                newOrgData.Seats = orgSeats; //ZD 100190
                //ZD 100518 Starts
                newOrgData.MaxParticipants = orgMaxParticipants; 
                newOrgData.MaxConcurrentCall = orgMaxConcurrentCall; 
                //ZD 100518 Ends

                //ZD 101869 Starts
                newOrgData.DefCodianLO = 1;
                newOrgData.DefPolycomMGCLO = 1;
                newOrgData.DefPolycomRMXLO = 1;
                //ZD 101869 End
                newOrgData.ShowVideoLayout = 0; //ZD 101931
                newOrgData.EWSConfAdmins = string.Empty; //ZD 10208
                newOrgData.EnableCalDefaultDisplay = 2; //ZD 102356
                newOrgData.DefLinerate = 1024;//ZD 102268
                newOrgData.EnableWaitList = 0;//ZD 102532
                newOrgData.PersonnelAlert = "";//ZD 103046
                newOrgData.EnableTravelAvoidTrack = 0; // ZD 103216
                //ZD 104021 - Start //ZD 104116
                if (sysSettings.MaxBlueJeansUsers == -2)
                {
                    newOrgData.EnableBJNIntegration = 1;
                    newOrgData.EnableBlueJeans = 1;
                }
                else
                    newOrgData.EnableBJNIntegration = 0;  
				newOrgData.BJNDisplay = 0;//ZD 104116               
                newOrgData.BJNMeetingType = (int)eBJNMeetingType.PersonalMeeting; //Default to Personal Meeting as there is no None option in Org option UI
                newOrgData.BJNSelectOption = 0;
                //ZD 104021 - End
                newOrgData.EnablePoolOrderSelection = 0;//ZD 104221
                newOrgData.EnableBlockUserDI = 0;//ZD 104862
                m_IOrgSettingDAO.clearFetch();
                m_IOrgSettingDAO.Save(newOrgData);
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException occured in Inserting Default Sys_techSupport_d", ex);
                return false;
            }
        }
        #endregion

        //Method added for FB 1779
        #region CreateDfltCustomAttributes
        /// <summary>
        /// CreateDfltCustomAttributes
        /// </summary>
        /// <returns></returns>
        private bool CreateDfltCustomAttributes()
        {
           
        string CusDes=""; int CustID=0; //ZD 101176
            try
            {
                vrmDeptCustomAttr vrmDeptCA = new vrmDeptCustomAttr();

                vrmDeptCA.DeptID = 0;
                vrmDeptCA.deleted = 0;
                vrmDeptCA.Mandatory = 0;
                vrmDeptCA.status = 0;
                vrmDeptCA.Scheduler = 0;
                vrmDeptCA.RoomAdmin = 0;
                vrmDeptCA.RoomApp = 0;
                vrmDeptCA.McuApp = 0;
                vrmDeptCA.SystemApp = 0;
                vrmDeptCA.IncludeInCalendar = 1;//FB 2045
                vrmDeptCA.CreateType = 1;  //System
                vrmDeptCA.orgId = organizationID.ToString();

                vrmDeptCA.Description = "Special Instructions";
                vrmDeptCA.DisplayTitle = "Special Instructions";
                vrmDeptCA.Type = 10;
                vrmDeptCA.IncludeInEmail = 1;
                vrmDeptCA.Host = 0;
                vrmDeptCA.Party = 0;
                vrmDeptCA.McuAdmin = 1;
                vrmDeptCA.CustomAttributeId = GetCustomAttributeId(m_IDeptCustDAO);
                //ZD 101176 Starts
                vrmDeptCA.FieldController = "Special Instructions";
                CusDes = "Instrucciones Especiales";
                CustID = vrmDeptCA.CustomAttributeId;
                //ZD 101176 Ends
                m_IDeptCustDAO.Save(vrmDeptCA);
                SetCustomLang(CusDes,CustID); //ZD 101176


                vrmDeptCA.Description = "Host";
                vrmDeptCA.DisplayTitle = "Work";
                vrmDeptCA.Type = 4;
                vrmDeptCA.IncludeInEmail = 0;
                vrmDeptCA.McuAdmin = 0;
                vrmDeptCA.CustomAttributeId = GetCustomAttributeId(m_IDeptCustDAO);
                //ZD 101176 Starts
                vrmDeptCA.FieldController = "Host"; //ZD 102312
                CusDes = "Trabajo";
                CustID = vrmDeptCA.CustomAttributeId;
                //ZD 101176 Ends
                m_IDeptCustDAO.Save(vrmDeptCA);
                SetCustomLang(CusDes, CustID); //ZD 101176


                vrmDeptCA.DisplayTitle = "Cell";
                vrmDeptCA.CustomAttributeId = GetCustomAttributeId(m_IDeptCustDAO);
                //ZD 101176 Starts
                vrmDeptCA.FieldController = "Host"; //ZD 102312
                CusDes = "Telf. móvil";
                CustID = vrmDeptCA.CustomAttributeId;
                //ZD 101176 Ends
                m_IDeptCustDAO.Save(vrmDeptCA);
                SetCustomLang(CusDes, CustID); //ZD 101176

                //FB 2045 - Start
                vrmDeptCA.DisplayTitle = "Entity Code";
                vrmDeptCA.Description = "Entity Code";
                vrmDeptCA.Type = 6;
                vrmDeptCA.CustomAttributeId = GetCustomAttributeId(m_IDeptCustDAO);
                //ZD 101176 Starts
                vrmDeptCA.FieldController = "Entity Code";
                CusDes = "Código de Entidad";
                CustID = vrmDeptCA.CustomAttributeId;
                //ZD 101176 Ends
                m_IDeptCustDAO.Save(vrmDeptCA);
                //FB 2045 - End
                SetCustomLang(CusDes, CustID); //ZD 101176

                //vrmDeptCA.Description = "Web Conference Instructions";
                //vrmDeptCA.DisplayTitle = "Web Conference No";
                //vrmDeptCA.CustomAttributeId = GetCustomAttributeId(m_IDeptCustDAO);
                //m_IDeptCustDAO.Save(vrmDeptCA);

                //vrmDeptCA.DisplayTitle = "Session URL";
                //vrmDeptCA.Type = 7;
                //vrmDeptCA.CustomAttributeId = GetCustomAttributeId(m_IDeptCustDAO);
                //m_IDeptCustDAO.Save(vrmDeptCA);

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException occured in Inserting Default Custom attributes", ex);
                return false;
            }
            finally
            {
                m_deptDAO = null;
                m_IDeptCustDAO = null;
            }
        }

        private int GetCustomAttributeId(IDeptCustomAttrDao m_IDeptCustDAO)
        {
            int customId = 0;
            string resVal = "UID";
            lock (resVal)
            {
                string checkAttributeId = "SELECT max(vc.CustomAttributeId) FROM myVRM.DataLayer.vrmDeptCustomAttr vc";
                IList result = m_IDeptCustDAO.execQuery(checkAttributeId);


                if (result != null)
                {
                    if (result.Count > 0)
                    {
                        if (result[0] != null)
                        {
                            int.TryParse(result[0].ToString(), out customId);
                            customId = customId + 1;
                        }
                    }
                }
            }
            return customId;
        }
        #endregion

        #region SetOrgImage
        /// <summary>
        /// SetOrgImage
        /// </summary>
        /// <param name="imgName"></param>
        /// <param name="imgSource"></param>
        /// <returns></returns>
        /// Image Attribute types
        /// room = 1 / roommap1 = 2 / roommap2 =3/ roomsec1 = 4 /roomsec2 = 5
        /// roommisc =6/ roommisc2 = 7/ av = 8 / catering = 9 / hk =10
        /// banner = 11/ highresbanner = 12/ companylogo = 13 emaillogo = 14 sitelogo = 15 
        /// MirrorXml = 16/ Artifacts = 17
        private int SetOrgImage(string actualImage, int attrType)
        {
            int imgId = 0;
            byte[] imageData = null;
            try
            {
                imageData = vrmImg.ConvertBase64ToByteArray(actualImage); //FB 2136
                vrmImage imgObj = new vrmImage();
                imgObj.OrgId = organizationID;
                imgObj.AttributeType = attrType;
                imgObj.AttributeImage = imageData;

                vrmfact.SetImage(imgObj, ref imgId);
                return imgId;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in SetOrgImage", ex);
                return 0;
            }
        }
        #endregion

        //FB 2154 start
        #region Get Organization EmailDomains
        /// <summary>
        /// Get Organization EmailDomains
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetOrgEmailDomains(ref vrmDataObject obj)
        {
            try
            {
                int userID = 11, i = 0;
                organizationID = 11;
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                

                node = xd.SelectSingleNode("//GetOrgEmailDomains/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < 11)
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }

                node = xd.SelectSingleNode("//GetOrgEmailDomains/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userID);

                List<vrmEmailDomain> EmailDomains = m_IEmailDomainDAO.GetEmailDomainOrgId(organizationID);
                obj.outXml += "<getOrgEmailDomains>";
                obj.outXml += "<EmailDomains>";
                for (i = 0; i < EmailDomains.Count; i++)
                {
                    obj.outXml += "<EmailDomain>";
                    obj.outXml += "<DomainID>" + EmailDomains[i].DomainID + "</DomainID>";
                    obj.outXml += "<Companyname>" + EmailDomains[i].Companyname + "</Companyname>";
                    obj.outXml += "<Emaildomain>" + EmailDomains[i].Emaildomain + "</Emaildomain>";
                    obj.outXml += "<isActive>" + EmailDomains[i].Active + "</isActive>";
                    obj.outXml += "</EmailDomain>";
                }
                obj.outXml += "</EmailDomains>";
                obj.outXml += "</getOrgEmailDomains>";

                return true;
            }
            catch (Exception ex)
            {
                obj.outXml = "";
                m_log.Error("GetEmailDomain" + ex.Message);
                return false;
            }
        }

        #endregion

        #region Save Organization EmailDomain
        /// <summary>
        /// Save Organization EmailDomain
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SaveEmailDomain(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                vrmEmailDomain EmailDomain = new vrmEmailDomain();
                XmlNode node = null;
                string domainID = "", companyName = "", Domain = "";
                int userID = 11, isActive = 1;

                organizationID = 11;
                obj.outXml = "";
                xd.LoadXml(obj.inXml);
                node = xd.SelectSingleNode("//SaveEmailDomain/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < 11)
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }

                node = xd.SelectSingleNode("//SaveEmailDomain/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userID);

                node = xd.SelectSingleNode("//SaveEmailDomain/EmailDomain/DomainID");
                if (node != null)
                    domainID = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SaveEmailDomain/EmailDomain/Companyname");
                if (node != null)
                    companyName = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SaveEmailDomain/EmailDomain/Emaildomain");
                if (node != null)
                    Domain = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SaveEmailDomain/EmailDomain/isActive");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out isActive);

                EmailDomain.Active = isActive;
                EmailDomain.Companyname = companyName;
                EmailDomain.Emaildomain = Domain;
                EmailDomain.orgId = organizationID;
                if (domainID.Trim() == "New")
                    m_IEmailDomainDAO.Save(EmailDomain);
                else
                {
                    int id;
                    int.TryParse(domainID, out id);
                    EmailDomain.DomainID = id;
                    m_IEmailDomainDAO.Update(EmailDomain);
                }

                return true;
            }
            catch (Exception ex)
            {
                obj.outXml = "";
                m_log.Error("GetEmailDomain" + ex.Message);
                return false;
            }
        }

        #endregion
        //FB 2154 end

        #endregion

        #region Internal Methods

        #region Get Organization timeZonesToXML
        /// <summary>
        /// Get Organization timeZonesToXML
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        internal bool timeZonesToXML(ref StringBuilder outputXML)
        {
            Hashtable timeZones = new Hashtable();
            string timediff = "" ; int tdiff= 0; //ZD 104410
            try
            {
                timeZoneData time = null;
                int i = 0;

                outputXML = new StringBuilder(); 
                if (organizationID < defaultOrgID)
                    organizationID = defaultOrgID;

                OrgData orgdt = m_IOrgSettingDAO.GetByOrgId(organizationID);

                if (!timeZone.GetAllTimeZone(ref timeZones))
                    return false;
                if (timeZones.Count <= 0)
                    return false;
                List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();

                //ZD 104410 start
                for (i = 0; i < timeZoneList.Count; i++)
                {
                    timediff = timeZoneList[i].TimeZoneDiff.Substring(4).Split(')', ':')[0];
                    if (timediff == "")
                        timeZoneList[i].TimeZonorder = 0;
                    else
                    {
                        int.TryParse(timediff, out tdiff);
                        timeZoneList[i].TimeZonorder = tdiff;
                    }
                }
                timeZoneList = timeZoneList.OrderBy(o => o.TimeZonorder).ToList();
                //ZD 104410 End

                outputXML.Append("<timezones>");
                for (i = 0; i < timeZoneList.Count; i++)
                {
                    time = null;
                    time = timeZoneList[i];
                    if (orgdt.tzsystemid == 0 || (orgdt.tzsystemid != 0 &&
                                                        orgdt.tzsystemid == time.systemID))
                    {
                        outputXML.Append("<timezone>");
                        outputXML.Append("<timezoneID>" + time.TimeZoneID.ToString() + "</timezoneID>");
                        outputXML.Append("<timezoneName>" + time.TimeZoneDiff.ToString() + " ");
                        outputXML.Append(time.TimeZone + "</timezoneName>");
                        outputXML.Append("<StandardName>" + time.StandardName + "</StandardName>"); //ZD 100456
                        outputXML.Append("</timezone>");
                    }
                }
                outputXML.Append("</timezones>");
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        //FB 2639_GetOldConf Start
        internal bool timeZonesToXML(ref XmlWriter outputXML)
        {
            Hashtable timeZones = new Hashtable();
            try
            {
                timeZoneData time = null;
                int i = 0;

                if (organizationID < defaultOrgID)
                    organizationID = defaultOrgID;

                OrgData orgdt = m_IOrgSettingDAO.GetByOrgId(organizationID);

                if (!timeZone.GetAllTimeZone(ref timeZones))
                    return false;
                if (timeZones.Count <= 0)
                    return false;
                List<timeZoneData> timeZoneList = timeZone.GetTimeZoneList();

                outputXML.WriteStartElement("timezones");
                for (i = 0; i < timeZoneList.Count; i++)
                {
                    time = null;
                    time = timeZoneList[i];
                    if (orgdt.tzsystemid == 0 || (orgdt.tzsystemid != 0 &&
                                                        orgdt.tzsystemid == time.systemID))
                    {
                        outputXML.WriteStartElement("timezone");
                        outputXML.WriteElementString("timezoneID", time.TimeZoneID.ToString());
                        outputXML.WriteElementString("timezoneName", time.TimeZoneDiff.ToString() + " " + time.TimeZone);
                        outputXML.WriteFullEndElement(); //ZD 101086
                    }
                }
                outputXML.WriteFullEndElement(); //ZD 101086
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        //FB 2639_GetOldConf End


        #endregion

        #region Set Image
        /// <summary>
        /// Set Mail Logo
        /// </summary>
        /// <param name="imgName"></param>
        /// <param name="imgSource"></param>
        /// <returns></returns>
        /// Image Attribute types
        ///room = 1 / roommap1 = 2 / roommap2 =3/ roomsec1 = 4 /roomsec2 = 5
        ///roommisc =6/ roommisc2 = 7/ av = 8 / catering = 9 / hk =10
        ///banner = 11/ highresbanner = 12/ companylogo = 13/ Mail Logo = 14
        private int SetImage(string actualImage, int attrType)
        {
            int imgId = 0;
            byte[] imageData = null;
            try
            {
                imageData = vrmImg.ConvertBase64ToByteArray(actualImage); //FB 2136
                vrmImage imgObj = new vrmImage();
                imgObj.OrgId = organizationID;
                imgObj.AttributeType = attrType;
                imgObj.AttributeImage = imageData;

                vrmfact.SetImage(imgObj, ref imgId);
                return imgId;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in SetRoomProfile", ex);
                return 0;
            }
        }
        #endregion

        #endregion

        #region Public Methods

        #region Get Organization Settings
        /// <summary>
        /// Get Organization Settings
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetOrgSettings(ref vrmDataObject obj)
        {
            string responseMsg = "", imgDt = "";
            int responseTime = 60, sa = 0,sysAcc =0,EM7Acc =0;
            vrmfact = new vrmFactory(ref obj); //fb 1602
            vrmImg = new imageFactory(ref obj); //FB 2136
            vrmImage imObj = null;
            obj.outXml = "";
            OrgOUTXML = new StringBuilder();
            OrgData orgdata = null;
            vrmUser approver = null;
            xSettings = null;
            cryptography.Crypto crypto = null; //FB 2717
            try
            {
                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//GetOrgSettings/organizationID");

                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out organizationID);

                        if (organizationID < 11)
                        {
                            obj.outXml = "<error>Invalid Organization ID</error>";
                            return false;
                        }
                    }
                }

                m_IOrgSettingDAO.clearFetch();
                orgdata = m_IOrgSettingDAO.GetByOrgId(organizationID);
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OrgOUTXML, xSettings))
                {
                    xWriter.WriteStartElement("GetOrgSettings");
                    xWriter.WriteStartElement("licensedetails");
                    xWriter.WriteElementString("MaxRoomLimit", sysSettings.RoomLimit.ToString());
                    xWriter.WriteElementString("MaxVideoRoomLimit", sysSettings.MaxVidRooms.ToString());
                    xWriter.WriteElementString("MaxNonVideoRoomLimit", sysSettings.MaxNVidRooms.ToString());
                    xWriter.WriteElementString("MaxVMRRoomLimit", sysSettings.MaxVMRooms.ToString());//FB 2586
                    xWriter.WriteElementString("MaxiControlRoomLimit", sysSettings.MaxiControlRooms.ToString());//ZD 101098
                    xWriter.WriteElementString("MaxMCULimit", sysSettings.MCULimit.ToString());
                    xWriter.WriteElementString("MaxMCUEnchancedLimit", sysSettings.MCUEnchancedLimit.ToString());//FB 2486
                    xWriter.WriteElementString("MaxUserLimit", sysSettings.UserLimit.ToString());
                    xWriter.WriteElementString("MaxExchangeUsers", sysSettings.MaxExcUsrs.ToString());
                    xWriter.WriteElementString("MaxDominoUsers", sysSettings.MaxDomUsrs.ToString());
                    xWriter.WriteElementString("MaxMobileUsers", sysSettings.MaxMobUsrs.ToString()); //FB 1979
                    xWriter.WriteElementString("MaxWebexUsers", sysSettings.MaxWebexUsr.ToString()); //ZD 100221
                    xWriter.WriteElementString("MaxEndPoints", sysSettings.MaxEndPts.ToString());
                    xWriter.WriteElementString("MaxFacilities", sysSettings.MaxFacilities.ToString());
                    xWriter.WriteElementString("MaxCatering", sysSettings.MaxCatering.ToString());
                    xWriter.WriteElementString("MaxHousekeeping", sysSettings.MaxHousekeeping.ToString());
                    //xWriter.WriteElementString("MaxPCModule", sysSettings.MaxPCModule.ToString()); //FB 2347 //FB 2693
                    //xWriter.WriteElementString("MaxGroupsPerAccount>" + sysSettings.MaxGroups + "</MaxGroupsPerAccount>"; - depricated
                    //xWriter.WriteElementString("MaxTemplatesPerAccount>" + sysSettings.MaxTemplates + "</MaxTemplatesPerAccount>"; - depricated
                    xWriter.WriteElementString("MaxGuestsPerAccount", sysSettings.MaxGuests.ToString());
                    xWriter.WriteFullEndElement(); //ZD 101086

                    xWriter.WriteElementString("organizationID", organizationID.ToString());
                    xWriter.WriteStartElement("preference");

                    if (orgdata != null)
                    {
                        xWriter.WriteElementString("accountingLogic", orgdata.AccountingLogic.ToString());
                        xWriter.WriteElementString("billPoint2Point", orgdata.BillPoint2Point.ToString());
                        xWriter.WriteElementString("allowAllocation", orgdata.overAllocation.ToString());
                        xWriter.WriteElementString("enableCustomAttribute", orgdata.EnableCustomOption);
                        xWriter.WriteElementString("multiDepartment", orgdata.MultipleDepartments.ToString());
                        xWriter.WriteElementString("RoomLimit", orgdata.RoomLimit.ToString());
                        xWriter.WriteElementString("MCULimit", orgdata.MCULimit.ToString());
                        xWriter.WriteElementString("MCUEnchancedLimit", orgdata.MCUEnchancedLimit.ToString());//FB 2486
                        xWriter.WriteElementString("UserLimit", orgdata.UserLimit.ToString());
                        xWriter.WriteElementString("ExchangeUserLimit", orgdata.ExchangeUserLimit.ToString());
                        xWriter.WriteElementString("DominoUserLimit", orgdata.DominoUserLimit.ToString());
                        xWriter.WriteElementString("MobileUserLimit", orgdata.MobileUserLimit.ToString()); //FB 1979
                        xWriter.WriteElementString("WebexUserLimit", orgdata.WebexUserLimit.ToString()); //ZD 100221
                        xWriter.WriteElementString("EndPoints", orgdata.MaxEndpoints.ToString());
                        xWriter.WriteElementString("VideoRooms", orgdata.MaxVideoRooms.ToString());
                        xWriter.WriteElementString("NonVideoRooms", orgdata.MaxNonVideoRooms.ToString());
                        xWriter.WriteElementString("VMRRooms", orgdata.MaxVMRRooms.ToString());//FB 2586
                        xWriter.WriteElementString("iControlRooms", orgdata.MaxiControlRooms.ToString());//ZD 101098
                        //FB 2694 Start
                        xWriter.WriteElementString("HotdeskingVideoRooms", orgdata.MaxVCHotdesking.ToString());
                        xWriter.WriteElementString("HotdeskingNonVideoRooms", orgdata.MaxROHotdesking.ToString());
                        //FB 2694 End
                        //FB 2426 Start
                        xWriter.WriteElementString("GuestRooms", orgdata.GuestRoomLimit.ToString());
                        xWriter.WriteElementString("GuestRoomPerUser", orgdata.GuestRoomPerUser.ToString());
                        //FB 2426 End
                        xWriter.WriteElementString("BluejeanPerUser", orgdata.BlueJeansUserLimit.ToString()); //ZD 104021
                        xWriter.WriteElementString("EnableFacilites", orgdata.EnableFacilities.ToString());
                        xWriter.WriteElementString("EnableCatering", orgdata.EnableCatering.ToString());
                        xWriter.WriteElementString("EnableHouseKeeping", orgdata.EnableHousekeeping.ToString());
                        xWriter.WriteElementString("EnableAPIs", orgdata.EnableAPI.ToString());
                        //xWriter.WriteElementString("EnablePC", orgdata.EnablePCModule.ToString()); //FB 2347 //FB 2693
                        xWriter.WriteElementString("EnableCloud", orgdata.EnableCloud.ToString()); //FB 2599
                        xWriter.WriteElementString("EnablePublicRoom", orgdata.EnablePublicRoomService.ToString()); //FB 2594
                        //FB 2693 Starts
                        xWriter.WriteElementString("PCUserLimit", orgdata.PCUserLimit.ToString());
                        xWriter.WriteElementString("EnableBlueJeans", orgdata.EnableBlueJeans.ToString());
                        xWriter.WriteElementString("EnableJabber", orgdata.EnableJabber.ToString());
                        xWriter.WriteElementString("EnableLync", orgdata.EnableLync.ToString());
                        xWriter.WriteElementString("EnableVidtel", orgdata.EnableVidtel.ToString());
                        //FB 2693 Ends
                        xWriter.WriteElementString("EnableAdvancedReport", orgdata.EnableAdvancedReport.ToString());//FB 2593
                        
                        //FB 1642-Audio Add on..
                        xWriter.WriteElementString("ConferenceCode", orgdata.ConferenceCode.ToString());
                        xWriter.WriteElementString("LeaderPin", orgdata.LeaderPin.ToString());
                        xWriter.WriteElementString("AdvAvParams", orgdata.AdvAvParams.ToString());
                        xWriter.WriteElementString("AudioParams", orgdata.AudioParams.ToString());
                        xWriter.WriteStartElement("IMTalk");
                        xWriter.WriteElementString("IMEnabled", orgdata.IMEnabled.ToString());
                        xWriter.WriteElementString("refreshTime", orgdata.IMRefreshConn.ToString());
                        xWriter.WriteElementString("maxUnitConnection", orgdata.IMMaxUnitConn.ToString());
                        xWriter.WriteElementString("maxSystemConnection", orgdata.IMMaxSysConn.ToString());
                        xWriter.WriteFullEndElement(); //ZD 101086

                        responseMsg = orgdata.responsemessage;
                        responseTime = orgdata.responsetime;
                    }

                    xWriter.WriteStartElement("approvers");
                    xWriter.WriteElementString("responseMsg", responseMsg);
                    xWriter.WriteElementString("responseTime", responseTime.ToString());

                    m_ISysApproverDAO.clearFetch();
                    IList<sysApprover> sysApprovers = m_ISysApproverDAO.GetSysApproversByOrgId(organizationID);
                    if (sysApprovers != null)
                    {
                        for (sa = 0; sa < sysApprovers.Count; sa++)
                        {
                            approver = m_IuserDAO.GetByUserId(sysApprovers[sa].approverid);
                            if (approver != null)
                            {
                                xWriter.WriteStartElement("approver");
                                xWriter.WriteElementString("ID", sysApprovers[sa].approverid.ToString());
                                xWriter.WriteElementString("firstName", approver.FirstName);
                                xWriter.WriteElementString("lastName", approver.LastName);
                                xWriter.WriteFullEndElement(); //ZD 101086
                            }
                        }
                    }
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.WriteStartElement("accountingLogics");

                    m_IAccSchemeDAO.clearFetch();
                    List<sysAccScheme> accSchemes = m_IAccSchemeDAO.GetAll();
                    if (accSchemes != null)
                    {
                        for (sysAcc = 0; sysAcc < accSchemes.Count; sysAcc++)
                        {
                            xWriter.WriteStartElement("logic");
                            xWriter.WriteElementString("ID", accSchemes[sysAcc].id.ToString());
                            xWriter.WriteElementString("name", accSchemes[sysAcc].name);
                            xWriter.WriteFullEndElement(); //ZD 101086
                        }
                    }
                    xWriter.WriteFullEndElement(); //ZD 101086

                    //fb 1602
                    imObj = m_IImageDAO.GetById(orgdata.MailLogo);
                    if (imObj != null)
                        imgDt = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage); //FB 2136

                    xWriter.WriteElementString("MailLogoImage", imgDt);
                    //FB 1710 code added starts
                    xWriter.WriteElementString("FooterMessage", orgdata.FooterMessage);

                    imgDt = "";
                    imObj = null;

                    if (orgdata.FooterImage > 0)
                    {
                        imObj = m_IImageDAO.GetById(orgdata.FooterImage);
                        if (imObj != null)
                            imgDt = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage); //FB 2136

                        xWriter.WriteElementString("FooterImage", imgDt);
                        xWriter.WriteElementString("FooterImgName", "Org_" + orgdata.OrgId.ToString() + "_footerimage.gif"); //+ "Org_ô" + orgdata.OrgId + "|" + orgdata.FooterImage + "ô_footer.gif" +
                    }
                    else
                    {
                        xWriter.WriteElementString("FooterImage", string.Empty);
                        xWriter.WriteElementString("FooterImgName", string.Empty);
                    }
                    //FB 1710 code ends
                    //FB 1830 - Start

                    string langID = "0";

                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("EmailLangId", orgdata.EmailLangId));

                    List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterionList);
                    if (emailLangs != null && (emailLangs.Count > 0))
                    {
                        xWriter.WriteElementString("OrgEmailLanguageName", emailLangs[0].EmailLanguage);
                        langID = orgdata.EmailLangId.ToString();
                    }
                    else
                        xWriter.WriteElementString("OrgEmailLanguageName", string.Empty);

                    xWriter.WriteElementString("OrgEmailLanguageID", langID);

                    xWriter.WriteElementString("OrgLanguage", orgdata.Language.ToString());
                    //FB 1830 - End
                    xWriter.WriteElementString("EmailDateFormat", orgdata.EmailDateFormat.ToString()); //FB 2555
                    xWriter.WriteElementString("MailBlocked", orgdata.MailBlocked.ToString());//FB 1860 
                    xWriter.WriteElementString("WorkingHours", orgdata.WorkingHours.ToString()); //FB 2343
                    //FB 2363
                    xWriter.WriteElementString("CustomerID", orgdata.CustomerID);
                    xWriter.WriteElementString("CustomerName", orgdata.CustomerName);

                    //ZD 100350
                    /*
                    List<ICriterion> criteria = new List<ICriterion>();
                    criteria.Add(Expression.Eq("orgId", organizationID));
                    criteria.Add(Expression.Eq("Type", "O"));
                    IList<ESMailUsrRptSettings> sysESRpt = m_ESMailUsrRptSetDAO.GetByCriteria(criteria);
                    if (sysESRpt != null && sysESRpt.Count > 0) // FB 2474
                    {
                        xWriter.WriteElementString("RptDestination", sysESRpt[0].RptDestination);
                        xWriter.WriteElementString("FrequencyCount", sysESRpt[0].FrequencyCount.ToString());
                    }
                     */

                    //FB 2262 //FB 2599 Starts
                    VrmVidyoSettings VidyoSettings = m_IVidyoSetDAO.GetVidyoSettingsByOrgId(organizationID);
                    crypto = new cryptography.Crypto(); //FB 2717
                    if (VidyoSettings != null)
                    {
                        //if (!string.IsNullOrEmpty(VidyoSettings.Password)) //FB 2717 //FB 3054
                        //    VidyoSettings.Password = crypto.decrypt(VidyoSettings.Password);
                        xWriter.WriteStartElement("VidyoSettings");
                        xWriter.WriteElementString("VidyoURL", VidyoSettings.VidyoURL);
                        xWriter.WriteElementString("VidyoLogin", VidyoSettings.AdminLogin);
                        xWriter.WriteElementString("VidyoPassword", VidyoSettings.Password);
                        xWriter.WriteElementString("ProxyAdd", VidyoSettings.ProxyAddress);
                        xWriter.WriteElementString("VidyoPort", VidyoSettings.Port.ToString());
                        xWriter.WriteFullEndElement(); //ZD 101086
                    }
                    //FB 2262 //FB 2599 Ends
                    //FB 2501 EM7 Starts
                    m_EM7SettingsDao.clearFetch();
                    List<vrmEM7OrgSilo> EM7accSchemes = m_EM7SettingsDao.GetAll();
                    if (EM7accSchemes != null)
                    {
                        xWriter.WriteStartElement("EM7Organization");
                        for (EM7Acc = 0; EM7Acc < EM7accSchemes.Count; EM7Acc++)
                        {
                            xWriter.WriteStartElement("Profile");
                            xWriter.WriteElementString("ID", EM7accSchemes[EM7Acc].OrgID.ToString());
                            xWriter.WriteElementString("Name", EM7accSchemes[EM7Acc].EM7Orgname);
                            xWriter.WriteFullEndElement(); //ZD 101086
                        }
                        xWriter.WriteElementString("EM7SelectedSilo", orgdata.EM7OrgId.ToString());
                        xWriter.WriteFullEndElement(); //ZD 101086
                    }
                    xWriter.WriteElementString("DefaultSubject", orgdata.DefaultSubject.ToString());//FB 2659 
                    xWriter.WriteElementString("DefaultInvitation", orgdata.DefaultInvitation.ToString());//FB 2659 

                    //ZD 101443 Starts
                    vrmLDAPConfig ldapSettings = null;
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", organizationID));
                    List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList);
                    if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                        ldapSettings = ldapSettingsList[0];

                    if (ldapSettings == null)
                    {
                        xWriter.WriteStartElement("LDAP");
                        xWriter.WriteElementString("serverAddress", "");
                        xWriter.WriteElementString("loginName", "");
                        xWriter.WriteElementString("loginPassword", "");
                        xWriter.WriteElementString("portNo", "");
                        xWriter.WriteElementString("connectionTimeout", "");
                        xWriter.WriteElementString("loginkey", "");
                        xWriter.WriteElementString("synctime", "");
                        xWriter.WriteElementString("LDAPPrefix", "");
                        xWriter.WriteElementString("searchfilter", "");
                        xWriter.WriteStartElement("scheduler");
                        xWriter.WriteElementString("Time", "");
                        xWriter.WriteElementString("Days", "");
                        xWriter.WriteEndElement();
                        xWriter.WriteElementString("AuthenticationType", "");
                        xWriter.WriteFullEndElement();
                    }
                    else
                    {
                        xWriter.WriteStartElement("LDAP");
                        xWriter.WriteElementString("serverAddress", ldapSettings.serveraddress.Trim());
                        xWriter.WriteElementString("loginName", ldapSettings.login.Trim());
                        xWriter.WriteElementString("loginPassword", ldapSettings.password.Trim());
                        xWriter.WriteElementString("portNo", ldapSettings.port.ToString());
                        xWriter.WriteElementString("connectionTimeout", ldapSettings.timeout.ToString());
                        xWriter.WriteElementString("loginkey", ldapSettings.LoginKey.Trim());
                        xWriter.WriteElementString("synctime", ldapSettings.SyncTime.ToString("hh:mm tt"));
                        xWriter.WriteElementString("LDAPPrefix", ldapSettings.domainPrefix.Trim());
                        xWriter.WriteElementString("searchfilter", ldapSettings.SearchFilter.Trim());
                        xWriter.WriteStartElement("scheduler");
                        xWriter.WriteElementString("Time", ldapSettings.scheduleTime.ToString("hh:mm tt"));
                        xWriter.WriteElementString("Days", ldapSettings.scheduleDays);
                        xWriter.WriteEndElement();
                        xWriter.WriteElementString("AuthenticationType", ldapSettings.AuthType.ToString());
                        xWriter.WriteFullEndElement();
                    }
                    //ZD 101443 End

                    xWriter.WriteFullEndElement(); //ZD 101086
                    //FB 2501 EM7 Ends
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.Flush();
                    obj.outXml = OrgOUTXML.ToString();
                    return true;
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region Set Organization Settings
        /// <summary>
        /// Set Organization Settings
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetOrgSettings(ref vrmDataObject obj)
        {
            string orgId = "",userID = "11";  //default user
            int responseTime = 60;  //One hour default
            UserFactory userObject = null;
            vrmfact = new vrmFactory(ref obj); //fb 1602
            vrmImg = new imageFactory(ref obj);//FB 2136
            XmlNode xnode = null;
            int i=0;
            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//SetOrgSettings/organizationID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);

                node = xd.SelectSingleNode("//SetOrgSettings/userID");
                if (node != null)
                    userID = node.InnerText;
               
                int.TryParse(userID, out loginUser);

                node = xd.SelectSingleNode("//SetOrgSettings/preference/accountingLogic");
                string accountLog = node.InnerText.Trim();
                int accLogic = 0;
                int.TryParse(accountLog, out accLogic);

                node = xd.SelectSingleNode("//SetOrgSettings/preference/billPoint2Point");
                string billPtoP = node.InnerText.Trim();
                int billPoint = 0;
                int.TryParse(billPtoP, out billPoint);

                node = xd.SelectSingleNode("//SetOrgSettings/preference/allowAllocation");
                string allowAll = node.InnerText.Trim();
                int allowAllLoc = 0;
                int.TryParse(allowAll, out allowAllLoc);

                node = xd.SelectSingleNode("//SetOrgSettings/preference/enableCustomAttribute");
                string enableCust = node.InnerText.Trim();
                
                node = xd.SelectSingleNode("//approvers/responseMsg");
                string respMsg = node.InnerText.Trim();

                node = xd.SelectSingleNode("//approvers/responseTime");
                string respTime = node.InnerText.Trim();
                int.TryParse(respTime, out responseTime);

                

                string imEnab = "";
                string imRefTime = "";
                string maxUnit = "";
                string maxSys = "";

                node = xd.SelectSingleNode("//IMTalk/IMEnabled");
                if(node != null)
                    imEnab = node.InnerText.Trim();

                node = xd.SelectSingleNode("//IMTalk/refreshTime");
                if(node != null)
                    imRefTime = node.InnerText.Trim();

                node = xd.SelectSingleNode("//IMTalk/maxUnitConnection");
                if(node != null)
                    maxUnit = node.InnerText.Trim();

                node = xd.SelectSingleNode("//IMTalk/maxSystemConnection");
                if(node != null)
                    maxSys = node.InnerText.Trim();

                int IMEnable = -1;
                int.TryParse(imEnab, out IMEnable);

                float IMRefreshTime = -1;
                Single.TryParse(imRefTime, out IMRefreshTime);

                int IMmaxUnit = -1;
                int.TryParse(maxUnit, out IMmaxUnit);

                int IMmaxSys = -1;
                int.TryParse(maxSys, out IMmaxSys);

                OrgData orgdata = m_IOrgSettingDAO.GetByOrgId(organizationID);

                if (orgdata == null)
                {
                    myvrmEx = new myVRMException(439);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                


                orgdata.AccountingLogic = accLogic;
                orgdata.BillPoint2Point = billPoint;
                orgdata.overAllocation = allowAllLoc;
                orgdata.EnableCustomOption = enableCust;

                orgdata.responsemessage = respMsg;
                orgdata.responsetime = responseTime;

                if (IMEnable > -1)
                    orgdata.IMEnabled = IMEnable;
                
                if (IMRefreshTime > -1)
                    orgdata.IMRefreshConn = IMRefreshTime;

                if (IMmaxUnit > -1)
                    orgdata.IMMaxUnitConn = IMmaxUnit;

                if (IMmaxSys > -1)
                    orgdata.IMMaxSysConn = IMmaxSys;

                node = xd.SelectSingleNode("//SetOrgSettings/preference/MailLogoImage");
                string map1Image = node.InnerXml.Trim();

                if (orgdata.MailLogo > 0) //FB 1658
                {
                    vrmImage imgObj = m_IImageDAO.GetById(orgdata.MailLogo);
                    if (imgObj != null)
                        m_IImageDAO.Delete(imgObj);
                }

                int imageId = 0;
                if (map1Image != "")
                    imageId = SetImage(map1Image, 14);
                
                orgdata.MailLogo = imageId;

                //Code added for FB 1710 - Start
                string footerImage = "";
                string footerMsg = "";

                node = xd.SelectSingleNode("//SetOrgSettings/preference/FooterImage"); //converted toBase64 of Image
                if (node != null)
                    footerImage = node.InnerXml.Trim();

                imageId = 0;
                if (footerImage != "")
                    imageId = SetImage(footerImage, 22);

                orgdata.FooterImage = imageId; 

                node = xd.SelectSingleNode("//SetOrgSettings/preference/FooterMessage");
                if (node != null)
                    footerMsg = node.InnerXml.Trim();
                    
                orgdata.FooterMessage = footerMsg;
                //FB 1710 - End
                //FB 1830 Starts
                string orgemaillanguage = "";
                node = xd.SelectSingleNode("//SetOrgSettings/preference/OrgEmailLanguage");
                if (node != null)
                    orgemaillanguage = node.InnerText.Trim();

                int orgEmailID = 1;
                int.TryParse(orgemaillanguage, out orgEmailID);
                
                orgdata.EmailLangId = orgEmailID;

                string orglang = "";
                node = xd.SelectSingleNode("//SetOrgSettings/preference/OrgLanguage");
                if (node != null)
                    orglang = node.InnerText.Trim();

                 int orgLangID = 1;
                 int.TryParse(orglang, out orgLangID);

                orgdata.Language = orgLangID;

                //FB 2555 Starts
                int emailDateFormat = 0;
                node = xd.SelectSingleNode("//SetOrgSettings/preference/EmailDateFormat");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out emailDateFormat);

                orgdata.EmailDateFormat = emailDateFormat;
                //FB 2555 Ends

                //FB 2343
                int workingHours = 0;
                node = xd.SelectSingleNode("//SetOrgSettings/preference/WorkingHours");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out workingHours);
                orgdata.WorkingHours = workingHours;

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("EmailLangId", orgdata.EmailLangId));

                List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterionList);
                if (!(emailLangs.Count > 0))
                    orgdata.EmailLangId = orgdata.Language;
               
                //FB 1830 Ends

                //FB 1860 start
                string mailBlocked = "";
                node = xd.SelectSingleNode("//SetOrgSettings/preference/MailBlocked");
                if (node != null)
                    mailBlocked = node.InnerText.Trim();

                int MailBlocked = 0;
                int.TryParse(mailBlocked, out MailBlocked);

                if (orgdata.MailBlocked != MailBlocked)
                {
                    orgdata.MailBlocked = MailBlocked;
                    DateTime mailBlckd = DateTime.Now;
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref mailBlckd);

                    orgdata.MailBlockedDate = mailBlckd;
                }
                //FB 1860 Ends

                //FB 2363 - Start
                node = xd.SelectSingleNode("//SetOrgSettings/preference/CustomerID");
                if (node != null)
                    orgdata.CustomerID = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SetOrgSettings/preference/CustomerName");
                if (node != null)
                    orgdata.CustomerName = node.InnerText.Trim();
                //FB 2363 - End

                //FB 2501 EM7 Starts
                int em7SiloId = -1;
                node = xd.SelectSingleNode("//SetOrgSettings/preference/EM7OrgID");
                if (node != null)
                {
                    int.TryParse(xd.SelectSingleNode("//SetOrgSettings/preference/EM7OrgID").InnerText, out em7SiloId);
                    orgdata.EM7OrgId = em7SiloId;
				}
                //FB 2501 EM7 Ends    

                //FB 2262 //FB 2599 Starts
                VrmVidyoSettings vidyoData = new VrmVidyoSettings();
                vidyoData.orgID = organizationID;
                DateTime currentDate = DateTime.Now;

                node = xd.SelectSingleNode("//SetOrgSettings/preference/VidyoSettings/VidyoURL");
                if (node != null)
                    vidyoData.VidyoURL= node.InnerText.Trim();

                node = xd.SelectSingleNode("//SetOrgSettings/preference/VidyoSettings/VidyoLogin");
                if (node != null)
                    vidyoData.AdminLogin = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SetOrgSettings/preference/VidyoSettings/VidyoPassword");
                if (node != null)
                {
                    vidyoData.Password = "";
                    if (node.InnerText.Trim() != "")
                    {
                        //cryptography.Crypto crypto = new cryptography.Crypto(); //FB 3054
                        //string password = crypto.encrypt(node.InnerText.Trim());
                        vidyoData.Password = node.InnerText.Trim();
                    }
                }
                node = xd.SelectSingleNode("//SetOrgSettings/preference/VidyoSettings/ProxyAdd");
                if (node != null)
                    vidyoData.ProxyAddress = node.InnerText.Trim();

                int vidyoPort=0;
                node = xd.SelectSingleNode("//SetOrgSettings/preference/VidyoSettings/VidyoPort");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(),out vidyoPort);
                vidyoData.Port = vidyoPort;
                vidyoData.Lastmodifieddatetime = currentDate;
                vidyoData.PollTime = currentDate;

                VrmVidyoSettings VidyoSettings = m_IVidyoSetDAO.GetVidyoSettingsByOrgId(organizationID);
                
                if (VidyoSettings == null)
                    m_IVidyoSetDAO.Save(vidyoData);
                else
                {
                    vidyoData.Id = VidyoSettings.Id;
                    m_IVidyoSetDAO.Update(vidyoData);
                }

                //FB 2262 //FB 2599 Ends
                //FB 2659 - Starts
                string defaultSubject = "", defaultInvitation = "";
                node = xd.SelectSingleNode("//SetOrgSettings/preference/DefaultSubject");
                if (node != null)
                    defaultSubject = node.InnerXml.Trim();

                orgdata.DefaultSubject = defaultSubject;

                node = xd.SelectSingleNode("//SetOrgSettings/preference/DefaultInvitation");
                if (node != null)
                    defaultInvitation = node.InnerXml.Trim();
                orgdata.DefaultInvitation = defaultInvitation;
                //FB 2659 - End
                
                m_IOrgSettingDAO.Update(orgdata);

                //ZD 101443 Starts
                string lserverAddress = "";
                node = xd.SelectSingleNode("//SetOrgSettings/preference/LDAP/serverAddress");
                if (node != null)
                    lserverAddress = node.InnerXml.Trim();

                string loginName = "";
                node = xd.SelectSingleNode("//SetOrgSettings/preference/LDAP/loginName");
                if (node != null)
                    loginName = node.InnerXml.Trim();

                string loginPassword = "";
                node = xd.SelectSingleNode("//SetOrgSettings/preference/LDAP/loginPassword");
                if (node != null)
                    loginPassword = node.InnerXml.Trim();

                int lportNo = 389; //LDAP default port no
                node = xd.SelectSingleNode("//SetOrgSettings/preference/LDAP/portNo");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out lportNo);
                if (lportNo <= 0)
                    lportNo = 389;

                int lconnectionTimeout = 20; //default 20 seconds
                node = xd.SelectSingleNode("//SetOrgSettings/preference/LDAP/connectionTimeout");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out lconnectionTimeout);
                if (lconnectionTimeout <= 0)
                    lconnectionTimeout = 20;

                DateTime schedulerTime = DateTime.UtcNow;
                node = xd.SelectSingleNode("//SetOrgSettings/preference/LDAP/scheduler/Time");
                if (node != null)
                    DateTime.TryParse(node.InnerXml.Trim(), out schedulerTime);

                string schedulerDays ="";
                node = xd.SelectSingleNode("//SetOrgSettings/preference/LDAP/scheduler/Days");
                if (node != null)
                    schedulerDays = node.InnerXml.Trim();

                string loginkey = "";
                node = xd.SelectSingleNode("//SetOrgSettings/preference/LDAP/loginkey");
                if (node != null)
                    loginkey = node.InnerXml.Trim();

                string searchfilter = "";
                node = xd.SelectSingleNode("//SetOrgSettings/preference/LDAP/searchfilter");
                if (node != null)
                    searchfilter = node.InnerXml.Trim();

                string LDAPPrefix = "";
                node = xd.SelectSingleNode("//SetOrgSettings/preference/LDAP/LDAPPrefix");
                if (node != null)
                    LDAPPrefix = node.InnerXml.Trim();

                int AuthType = 0; // 0- None, 1- Signing,2- Sealing,3- Secure
                node = xd.SelectSingleNode("//SetOrgSettings/preference/LDAP/AuthenticationType");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out AuthType);


                //Update LDAP Configurations
                vrmLDAPConfig ldapConfig = null;
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", organizationID));
                List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList);
                if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                    ldapConfig = ldapSettingsList[0];

                if (ldapConfig == null)
                    ldapConfig = new vrmLDAPConfig();
                //cryptography.Crypto crypto = new cryptography.Crypto();
                ldapConfig.OrgId = organizationID;
                ldapConfig.serveraddress = lserverAddress;
                ldapConfig.login = loginName;
                ldapConfig.password = loginPassword;
                ldapConfig.port = lportNo;
                ldapConfig.timeout = lconnectionTimeout;
                ldapConfig.LoginKey = loginkey;
                ldapConfig.SyncTime = schedulerTime;
                ldapConfig.domainPrefix = LDAPPrefix;
                ldapConfig.SearchFilter = searchfilter;
                ldapConfig.scheduleTime = schedulerTime;
                ldapConfig.scheduleDays = schedulerDays;
                ldapConfig.AuthType = AuthType;
                m_ILDAPConfigDAO.SaveOrUpdate(ldapConfig);
                //ZD 101443 LDAP END


                string appID = "";
                int approverId = 0;
                List<int> approverList = new List<int>();
                XmlNodeList sysApproversList = xd.SelectNodes("//SetOrgSettings/preference/approvers/approver");
                for (i = 0; i < sysApproversList.Count; i++)
                {
                    xnode = null;
                    xnode = sysApproversList[i];

                    if (xnode.SelectSingleNode("ID") != null)
                        appID = xnode.SelectSingleNode("ID").InnerText.Trim();

                    //FB 2539 Start
                    if (!m_myVRMSearch.CheckApproverRights(appID))
                    {
                        myVRMException myVRMEx = new myVRMException(632);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                    //FB 2539 End

                    int.TryParse(appID, out approverId);
                    if (approverId > 0)
                    {
                        approverList.Add(approverId);
                    }
                }

                bool retVal = false;
                retVal = IsSysApprovalRequired(approverList);
                if (retVal)
                {
                    myvrmEx = new myVRMException(437);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                
                userObject = new UserFactory(ref obj);
                retVal = userObject.ManageApproverCounters((int)LevelEntity.SYSTEM, 2, approverList);
                if (!retVal)
                {
                    myvrmEx = new myVRMException(440);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                retVal = RemoveSystemApprovers();
                if (!retVal)
                {
                    myvrmEx = new myVRMException(441);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if (approverList.Count > 0)
                {
                    retVal = SetSystemApprovers(approverList);
                    if (!retVal)
                    {
                        myvrmEx = new myVRMException(441);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                retVal = userObject.ManageApproverCounters((int)LevelEntity.SYSTEM,1, approverList);

                #region ES Mail User Report Setting
                // ZD 100350
                /*
                ESMailUsrRptSettings ESMailUsrRptSet = new ESMailUsrRptSettings();
                List<ICriterion> criteria = new List<ICriterion>();
                int FrequencyType = 1, FrequencyCount = 1, Sent = 0;
                DateTime Starttime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1); //FB 2563

                criteria.Add(Expression.Eq("orgId", organizationID));
                criteria.Add(Expression.Eq("Type", "O"));
                IList<ESMailUsrRptSettings> ESMailUsrRptSets = m_ESMailUsrRptSetDAO.GetByCriteria(criteria);

                node = xd.SelectSingleNode("//SetOrgSettings/preference/RptDestination");
                if (node != null)
                    ESMailUsrRptSet.RptDestination = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrgSettings/preference/FrequencyCount");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out FrequencyCount);
                ESMailUsrRptSet.FrequencyCount = FrequencyCount;

                node = xd.SelectSingleNode("//SetOrgSettings/preference/FrequencyType");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out FrequencyType);
                ESMailUsrRptSet.FrequencyType = FrequencyType;
                
                node = xd.SelectSingleNode("//SetOrgSettings/preference/Type");
                if (node != null)
                    ESMailUsrRptSet.Type = node.InnerXml.Trim();

                ESMailUsrRptSet.Sent = Sent;
                ESMailUsrRptSet.orgId = organizationID;

                if (ESMailUsrRptSets == null || ESMailUsrRptSets.Count == 0)
                    m_ESMailUsrRptSetDAO.Save(ESMailUsrRptSet);
                else
                {
                    ESMailUsrRptSet.UId = ESMailUsrRptSets[0].UId;
                    m_ESMailUsrRptSetDAO.Update(ESMailUsrRptSet);
                }
                */

                #endregion

                if (!retVal)
                {
                    myvrmEx = new myVRMException(440);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region Get Organization Options
        /// <summary>
        /// Get Organization Options By OrgID
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetOrgOptions(ref vrmDataObject obj)
        {
            string rmxLO, mgcLO, ctsLO, ctmsLO , CodianLO= ""; //FB 2335 //ZD 101869
            int i = 0, responseTIme = 0; //FB 2993
            sysTimeZonePref stzPref = null;
            sysTechData sysTechDta = null;
            List<sysTimeZonePref> sysTZPrefs = null;
            OrgOUTXML = new StringBuilder();
            xSettings = null;
            obj.outXml = "";
            // ZD 104151 Start
            List<ICriterion> criterionList = null;
            List<vrmMCU> Mcus = null;
            // ZD 104151 End
            try
            {
                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//GetOrgOptions/organizationID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out organizationID);

                        if (organizationID < 11)
                        {
                            obj.outXml = "<error>Invalid Organization ID</error>";
                            return false;
                        }
                    }
                }

                m_IOrgSettingDAO.clearFetch();
                OrgData orgdata = m_IOrgSettingDAO.GetByOrgId(organizationID);
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OrgOUTXML, xSettings))
                {
                    xWriter.WriteStartElement("GetOrgOptions");
                    xWriter.WriteStartElement("TimezoneSystems");
                    sysTZPrefs = new List<sysTimeZonePref>();
                    sysTZPrefs = m_ISysTimeZonePrefDAO.GetAllPrefTimeZones(m_configPath);
                    for (i = 0; i < sysTZPrefs.Count; i++)
                    {
                        stzPref = null;
                        stzPref = sysTZPrefs[i];
                        xWriter.WriteStartElement("TimezoneSystem");
                        xWriter.WriteElementString("ID", stzPref.systemid.ToString());
                        xWriter.WriteElementString("Name", stzPref.name.ToString());
                        xWriter.WriteFullEndElement(); //ZD 101086
                    }
                    xWriter.WriteFullEndElement(); //ZD 101086

                    sysTechDta = new sysTechData();
                    sysTechDta = m_ISysTechDAO.GetTechByOrgId(organizationID);
                    if (sysTechDta != null)
                    {
                        xWriter.WriteStartElement("ContactDetails");
                        xWriter.WriteElementString("Name", m_UtilFactory.ReplaceOutXMLSpecialCharacters(sysTechDta.name));//ZD 104391
                        xWriter.WriteElementString("Email", m_UtilFactory.ReplaceOutXMLSpecialCharacters(sysTechDta.email)); //ZD 104391
                        xWriter.WriteElementString("Phone", m_UtilFactory.ReplaceOutXMLSpecialCharacters(sysTechDta.phone)); //ZD 104391
                        xWriter.WriteElementString("AdditionInfo", m_UtilFactory.ReplaceOutXMLSpecialCharacters(sysTechDta.info));//ZD 104391
                        xWriter.WriteFullEndElement(); //ZD 101086
                    }

                    if (orgdata != null)
                    {
                        xWriter.WriteElementString("TimezoneSystemID", orgdata.tzsystemid.ToString());
                        xWriter.WriteElementString("AutoAcceptModifiedConference", orgdata.AutoAcceptModConf.ToString());
                        xWriter.WriteElementString("EnableRecurringConference", orgdata.recurEnabled.ToString());
                        xWriter.WriteElementString("EnableDynamicInvite", orgdata.dynamicinviteenabled.ToString());
                        xWriter.WriteElementString("EnableP2PConference", orgdata.Connect2.ToString());//FB 1070
                        xWriter.WriteElementString("EnableRealtimeDisplay", orgdata.RealtimeStatus.ToString());
                        xWriter.WriteElementString("EnableDialout", orgdata.DialOut.ToString());
                        xWriter.WriteElementString("DefaultConferencesAsPublic", orgdata.DefaultToPublic.ToString());
                        xWriter.WriteElementString("DefaultConferenceType", orgdata.DefaultConferenceType.ToString());
                        xWriter.WriteElementString("RFIDTagValue", orgdata.RFIDTagValue.ToString());//FB 2724
                        xWriter.WriteElementString("iControlTimeout", orgdata.iControlTimeout.ToString());//FB 2724
                        xWriter.WriteElementString("EnableRoomConference", orgdata.EnableRoomConference.ToString());
                        xWriter.WriteElementString("EnableAudioVideoConference", orgdata.EnableAudioVideoConference.ToString());
                        xWriter.WriteElementString("EnableAudioOnlyConference", orgdata.EnableAudioOnlyConference.ToString());
                        xWriter.WriteElementString("EnableNumericID", orgdata.EnableNumericID.ToString());//FB 2870
                        xWriter.WriteElementString("EnableHotdeskingConference", orgdata.EnableHotdeskingConference.ToString());//ZD 100719
                        xWriter.WriteElementString("DefaultCalendarToOfficeHours", orgdata.DefaultCalendarToOfficeHours.ToString());
                        xWriter.WriteElementString("RoomTreeExpandLevel", orgdata.RoomTreeExpandLevel);
                        xWriter.WriteElementString("EnableCustomOption", orgdata.EnableCustomOption);
                        xWriter.WriteElementString("EnableBufferZone", orgdata.EnableBufferZone);
                        xWriter.WriteElementString("SetupTime", orgdata.SetupTime.ToString()); //FB 2398
                        xWriter.WriteElementString("TearDownTime", orgdata.TearDownTime.ToString());
                        //FB-1642 Starts
                        xWriter.WriteElementString("ConferenceCode", orgdata.ConferenceCode.ToString());
                        xWriter.WriteElementString("LeaderPin", orgdata.LeaderPin.ToString());
                        xWriter.WriteElementString("AdvAvParams", orgdata.AdvAvParams.ToString());
                        xWriter.WriteElementString("AudioParams", orgdata.AudioParams.ToString());
                        //FB-1642 Ends
                        xWriter.WriteStartElement("SystemAvailableTime");
                        xWriter.WriteElementString("IsOpen24Hours", orgdata.Open24hrs.ToString());
                        xWriter.WriteElementString("StartTime", orgdata.SystemStartTime.ToString("hh:mm tt"));
                        xWriter.WriteElementString("EndTime", orgdata.SystemEndTime.ToString("hh:mm tt"));
                        xWriter.WriteElementString("DaysClosed", orgdata.Offdays);
                        xWriter.WriteFullEndElement(); //ZD 101086
                        xWriter.WriteElementString("MailLogoImage", GetImage(orgdata.MailLogo));//fb 1602
                        xWriter.WriteElementString("iCalReqEmailId", orgdata.IcalReqEmailID);//FB 1786
                        xWriter.WriteElementString("showBridgeExt", orgdata.IsBridgeExtNo.ToString());//FB 2610
                        xWriter.WriteElementString("sendIcal", orgdata.SendIcal.ToString()); //FB 1782
                        xWriter.WriteElementString("sendApprovalIcal", orgdata.SendApprovalIcal.ToString()); //FB 1782
                        xWriter.WriteElementString("isVIP", orgdata.isVIP.ToString());// FB 1864
                        xWriter.WriteElementString("isUniquePassword", orgdata.isUniquePassword.ToString()); // FB 1865
                        xWriter.WriteElementString("isAssignedMCU", orgdata.isAssignedMCU.ToString()); // FB 1901
                        xWriter.WriteElementString("PIMNotifications", orgdata.PluginConfirmations.ToString()); // FB 2141
                        xWriter.WriteElementString("ReminderMask", orgdata.ReminderMask.ToString());// FB 1926
                        xWriter.WriteElementString("isMultiLingual", orgdata.isMultiLingual.ToString()); //FB 1830
                        xWriter.WriteElementString("ExternalAttachments", orgdata.SendAttachmentsExternal.ToString()); //FB 2154
                        xWriter.WriteElementString("filterTelepresence", orgdata.TelepresenceFilter.ToString()); //FB 2170
                        xWriter.WriteElementString("EnableRoomServiceType", orgdata.EnableRoomServiceType.ToString());//FB 2219
                        xWriter.WriteElementString("isSpecialRecur", orgdata.SpecialRecur.ToString()); //2052                                        
                        xWriter.WriteElementString("isDeptUser", orgdata.isDeptUser.ToString()); //2269                                        
                        xWriter.WriteElementString("EnablePIMServiceType", orgdata.EnablePIMServiceType.ToString());//FB 2038
                        xWriter.WriteElementString("EnableImmConf", orgdata.EnableImmediateConference.ToString());//FB 2036 
                        xWriter.WriteElementString("EnableAudioBridges", orgdata.EnableAudioBridges.ToString());//FB 2023 
                        //FB 2359 Start
                        xWriter.WriteElementString("EnableConferencePassword", orgdata.EnableConfPassword.ToString());
                        xWriter.WriteElementString("EnablePublicConference", orgdata.EnablePublicConf.ToString());
                        xWriter.WriteElementString("EnableRoomParam", orgdata.EnableRoomParam.ToString());
                        //FB 2136 start
                        //xWriter.WriteElementString("EnableSecurityBadge", orgdata.EnableSecurityBadge.ToString());
                        //xWriter.WriteElementString("SecurityBadgeType", orgdata.SecurityBadgeType.ToString());
                        //xWriter.WriteElementString("SecurityDeskEmailId", orgdata.SecurityDeskEmailId);
                        //FB 2136 end
                        xWriter.WriteElementString("EnablePasswordRule", orgdata.EnablePasswordRule.ToString());//FB 2339
                        xWriter.WriteElementString("DedicatedVideo", orgdata.DedicatedVideo.ToString()); //FB 2334
                        //FB 2335 start
                        rmxLO = (orgdata.DefPolycomRMXLO > 9) ? orgdata.DefPolycomRMXLO.ToString() : "0" + orgdata.DefPolycomRMXLO.ToString();
                        mgcLO = (orgdata.DefPolycomMGCLO > 9) ? orgdata.DefPolycomMGCLO.ToString() : "0" + orgdata.DefPolycomMGCLO.ToString();
                        ctsLO = (orgdata.DefCiscoTPLO > 9) ? orgdata.DefCiscoTPLO.ToString() : "0" + orgdata.DefCiscoTPLO.ToString();
                        ctmsLO = (orgdata.DefCTMSLO > 9) ? orgdata.DefCTMSLO.ToString() : "0" + orgdata.DefCTMSLO.ToString();
                        xWriter.WriteElementString("defPolycomRMXLO", rmxLO);
                        xWriter.WriteElementString("defPolycomMGCLO", mgcLO);
                        xWriter.WriteElementString("defCiscoTPLO", ctsLO);
                        xWriter.WriteElementString("defCTMSLO", ctmsLO);
                        //FB 2335 end
                        xWriter.WriteElementString("WorkingHours", orgdata.WorkingHours.ToString()); //FB 2343
                        xWriter.WriteElementString("EnableVMR", orgdata.EnableVMR.ToString());
                        //FB 2348
                        xWriter.WriteElementString("EnableSurvey", orgdata.EnableSurvey.ToString());
                        xWriter.WriteElementString("SurveyOption", orgdata.SurveyOption.ToString());
                        xWriter.WriteElementString("SurveyURL", orgdata.SurveyURL);
                        xWriter.WriteElementString("TimeDuration", orgdata.TimeDuration.ToString());
                        //FB 2348
                        xWriter.WriteElementString("EnableEPDetails", orgdata.EnableEPDetails.ToString()); //FB 2401
                        xWriter.WriteElementString("EnableAcceptDecline", orgdata.EnableAcceptDecline.ToString()); //FB 2419
                        xWriter.WriteElementString("DefaultLineRate", orgdata.DefLinerate.ToString());//FB 2429
                        xWriter.WriteElementString("EnableFileWhiteList", orgdata.EnableFileWhiteList.ToString());//ZD 100263
                        xWriter.WriteElementString("FileWhiteList", orgdata.FileWhiteList);//ZD 100263
                        xWriter.WriteElementString("McuSetupTime", orgdata.McuSetupTime.ToString()); //FB 2440
                        xWriter.WriteElementString("MCUTeardonwnTime", orgdata.MCUTeardonwnTime.ToString()); //FB 2440
                        xWriter.WriteElementString("MCUSetupDisplay", orgdata.MCUSetupDisplay.ToString()); //FB 2998
                        xWriter.WriteElementString("MCUTearDisplay", orgdata.MCUTearDisplay.ToString()); //FB 2998
                        //FB 2426 Start
                        xWriter.WriteStartElement("OnflyTier");
                        xWriter.WriteElementString("DefTopTier", orgdata.TopTier);
                        xWriter.WriteElementString("DefMiddleTier", orgdata.MiddleTier);
                        xWriter.WriteElementString("OnflyTopTierID", orgdata.OnflyTopTierID.ToString());
                        xWriter.WriteElementString("OnflyMiddleTierID", orgdata.OnflyMiddleTierID.ToString());
                        xWriter.WriteFullEndElement(); //ZD 101086
                        //FB 2426 End

                        //ZD 100068 Starts
                        xWriter.WriteStartElement("VMRTier");
                        xWriter.WriteElementString("VMRTopTierID", orgdata.VMRTopTierID.ToString());
                        xWriter.WriteElementString("VMRMiddleTierID", orgdata.VMRMiddleTierID.ToString());
                        xWriter.WriteFullEndElement(); //ZD 101086
                        //ZD 100068 Ends


                        //FB 2469 Start
                        xWriter.WriteElementString("EnableConfTZinLoc", orgdata.EnableConfTZinLoc.ToString());
                        xWriter.WriteElementString("SendConfirmationEmail", orgdata.SendConfirmationEmail.ToString());
                        //FB 2469 End
                        xWriter.WriteElementString("DefaultConfDuration", orgdata.DefaultConfDuration.ToString());//FB 2501 
                        xWriter.WriteElementString("MCUAlert", orgdata.MCUAlert.ToString());//FB 2472
                        xWriter.WriteElementString("OrgMessage", orgdata.DefaultTxtMsg.Trim());//FB 2486

                        if (orgdata.Connect2 == 0)  //FB 2430
                            orgdata.EnableSmartP2P = 0;

                        xWriter.WriteElementString("EnableSmartP2P", orgdata.EnableSmartP2P.ToString()); //FB 2430
                        xWriter.WriteElementString("MaxPublicVMRParty", orgdata.MaxPublicVMRParty.ToString()); //FB 2550
                        //FB 2571 Start
                        xWriter.WriteElementString("EnableFECC", orgdata.EnableFECC.ToString());
                        xWriter.WriteElementString("DefaultFECC", orgdata.DefaultFECC.ToString());
                        //FB 2571 End
                        //FB 2598 starts
                        xWriter.WriteElementString("EnableCallmonitor", orgdata.EnableCallmonitor.ToString());
                        xWriter.WriteElementString("EnableEM7", orgdata.EnableEM7.ToString());
                        xWriter.WriteElementString("EnableCDR", orgdata.EnableCDR.ToString());
                        //FB 2598 Ends
                        xWriter.WriteElementString("MeetandGreetBuffer", orgdata.MeetandGreetBuffer.ToString());//FB 2609
                        //FB 2632 Starts
                        xWriter.WriteElementString("EnableCongSupport", orgdata.EnableCncigSupport.ToString());
                        xWriter.WriteElementString("MeetandGreetinEmail", orgdata.MeetandGreetinEmail.ToString());
                        xWriter.WriteElementString("OnSiteAVSupportinEmail", orgdata.AVOnsiteinEmail.ToString());
                        xWriter.WriteElementString("ConciergeMonitoringinEmail", orgdata.CncigMoniteringinEmail.ToString());
                        xWriter.WriteElementString("DedicatedVNOCOperatorinEmail", orgdata.VNOCinEmail.ToString());
                        //FB 2632 Ends
                        xWriter.WriteElementString("EnableRoomAdminDetails", orgdata.EnableRoomAdminDetails.ToString());//FB 2631
                        xWriter.WriteElementString("AlertforTier1", orgdata.AlertforTier1); //FB 2637
                        xWriter.WriteElementString("EnableDialPlan", orgdata.EnableDialPlan.ToString()); // FB 2636
                        //FB 2595 Starts
                        xWriter.WriteElementString("HardwareAdminEmail", orgdata.HardwareAdminEmail);
                        xWriter.WriteElementString("SecureSwitch", orgdata.SecureSwitch.ToString());
                        xWriter.WriteElementString("NetworkSwitching", orgdata.NetworkSwitching.ToString());
                        xWriter.WriteElementString("NetworkCallLaunch", orgdata.NetworkCallLaunch.ToString());
                        xWriter.WriteElementString("SecureLaunchBuffer", orgdata.SecureLaunchBuffer.ToString());
                        //FB 2595 Ends
                        xWriter.WriteElementString("EnableZulu", orgdata.EnableZulu.ToString());//FB 2588
						//FB 2670 START
                        xWriter.WriteElementString("EnableOnsiteAV", orgdata.EnableOnsiteAV.ToString());
                        xWriter.WriteElementString("EnableMeetandGreet", orgdata.EnableMeetandGreet.ToString());
                        xWriter.WriteElementString("EnableConciergeMonitoring", orgdata.EnableConciergeMonitoring.ToString());
                        xWriter.WriteElementString("EnableDedicatedVNOC", orgdata.EnableDedicatedVNOC.ToString());
                        //FB 2670 END
                        xWriter.WriteElementString("EnableLinerate", orgdata.EnableLinerate.ToString());//FB 2641 
                        xWriter.WriteElementString("EnableStartMode", orgdata.EnableStartMode.ToString());//FB 2641 
                        xWriter.WriteElementString("DefaultSubject", orgdata.DefaultSubject.ToString());//FB 2659 
                        xWriter.WriteElementString("DefaultInvitation", orgdata.DefaultInvitation.ToString());//FB 2659
						xWriter.WriteElementString("EnableSingleRoomConfEmails", orgdata.SingleRoomConfMail.ToString());//FB 2817 
						xWriter.WriteElementString("EnableProfileSelection", orgdata.EnableProfileSelection.ToString());//FB 2839
                        xWriter.WriteElementString("ShowCusAttInCalendar", orgdata.ShowCusAttInCalendar.ToString()); //ZD 100151
						//ZD 100164 START
                        xWriter.WriteElementString("EnableAdvancedUserOption", orgdata.EnableAdvancedUserOption.ToString());//FB 2817 
                        //ZD 100164 END
                        //ZD 100707 Start
                        xWriter.WriteElementString("ShowHideVMR", orgdata.ShowHideVMR.ToString());
                        xWriter.WriteElementString("EnablePersonaVMR", orgdata.EnablePersonaVMR.ToString());
                        xWriter.WriteElementString("EnableRoomVMR", orgdata.EnableRoomVMR.ToString());
                        xWriter.WriteElementString("EnableExternalVMR", orgdata.EnableExternalVMR.ToString());
                        //ZD 100707 End

                        if (orgdata.ResponseTimeout > 0) //FB 2993
                            responseTIme = orgdata.ResponseTimeout / 60;
                        else
                            responseTIme = orgdata.ResponseTimeout;
                        xWriter.WriteElementString("ResponseTimeout", responseTIme.ToString());//FB 2993
                        //ZD 100221 Starts
                        xWriter.WriteElementString("WebExURL", orgdata.WebExURL);
                        xWriter.WriteElementString("WebExSiteID", orgdata.WebExSiteID);
                        xWriter.WriteElementString("WebExPartnerID", orgdata.WebExPartnerID);
                        //ZD 100221 Ends
						xWriter.WriteElementString("ScheduleLimit", orgdata.ScheduleLimit.ToString());//ZD 100899
                        xWriter.WriteElementString("EnableExpressConfType", orgdata.EnableExpressConfType.ToString()); //ZD 100704
                        xWriter.WriteElementString("EnableDetailedExpressForm", orgdata.EnableDetailedExpressForm.ToString()); // ZD 100834
						xWriter.WriteElementString("EnableWebExIntg", orgdata.EnableWebExIntg.ToString()); //ZD 100935
                        xWriter.WriteElementString("EnablePasswordExp", orgdata.EnablePasswordExp.ToString()); //ZD 100781
                        xWriter.WriteElementString("PasswordExpDays", orgdata.PasswordMonths.ToString()); //ZD 100781
						xWriter.WriteElementString("EnableRoomCalendarView", orgdata.EnableRoomCalendarView.ToString()); //ZD 100963

                        //ZD 101019 START
                        xWriter.WriteElementString("VideoSourceURL1", orgdata.VideoSourceURL1);
                        xWriter.WriteElementString("VideoSourceURL2", orgdata.VideoSourceURL2);
                        xWriter.WriteElementString("VideoSourceURL3", orgdata.VideoSourceURL3);
                        xWriter.WriteElementString("VideoSourceURL4", orgdata.VideoSourceURL4);

                        xWriter.WriteElementString("ScreenPosition1", orgdata.ScreenPosition1);
                        xWriter.WriteElementString("ScreenPosition2", orgdata.ScreenPosition2);
                        xWriter.WriteElementString("ScreenPosition3", orgdata.ScreenPosition3);
                        xWriter.WriteElementString("ScreenPosition4", orgdata.ScreenPosition4);
                        //ZD 101019 END
                        //ZD 101120 Starts
                        xWriter.WriteElementString("EnableGuestLocWarningMsg", orgdata.EnableGuestLocWarningMsg.ToString());
                        xWriter.WriteElementString("GuestLocApprovalTime", orgdata.GuestLocApprovalTime.ToString());
                        xWriter.WriteElementString("SmartP2PNotify", orgdata.SmartP2PNotify.ToString());//ZD 100815
                        //ZD 101120 Ends
						//ZD 100522 Starts
                        xWriter.WriteElementString("PasswordCharLength", orgdata.PasswordCharLength.ToString());
                        xWriter.WriteElementString("VMRPINChange", orgdata.VMRPINChange.ToString());
                        //ZD 100522 Ends
                        xWriter.WriteElementString("RoomDenialCommt", orgdata.RoomDenialCommt.ToString());//ZD 101445
                        //ZD 101228 Starts
                        xWriter.WriteElementString("AVWOAlertTime", orgdata.AVWOAlertTime.ToString());
                        xWriter.WriteElementString("CatWOAlertTime", orgdata.CatWOAlertTime.ToString());
                        xWriter.WriteElementString("FacilityWOAlertTime", orgdata.FacilityWOAlertTime.ToString());
                        //ZD 101228 Ends
						xWriter.WriteElementString("WebExLaunch", orgdata.WebExLaunch.ToString()); //ZD 100513
                        xWriter.WriteElementString("EnableWETConference", orgdata.EnableWETConference.ToString()); //ZD 100513
                        xWriter.WriteElementString("EnableTemplateBooking", orgdata.EnableTemplateBooking.ToString());//ZD 101562
                        xWriter.WriteElementString("EnableSetupTimeDisplay", orgdata.EnableSetupTimeDisplay.ToString()); //ZD 101755
                        xWriter.WriteElementString("EnableTeardownTimeDisplay", orgdata.EnableTeardownTimeDisplay.ToString()); //ZD 101755	
						xWriter.WriteElementString("EnableActMsgDelivery", orgdata.EnableActMsgDelivery.ToString());//ZD 101757
						//ZD 101527 - Start
                        xWriter.WriteElementString("EnableRPRMRoomSync", orgdata.EnableRPRMRoomSync.ToString());
                        xWriter.WriteElementString("RoomSyncAuto", orgdata.RoomSyncAuto.ToString());
                        xWriter.WriteElementString("RoomSyncPollTime", orgdata.RoomSyncPollTime.ToString());                       
                        //ZD 101527 - End
						xWriter.WriteElementString("EnablePartyCode", orgdata.EnablePartyCode.ToString());//ZD 101446
						//ZD 101869 start
                        CodianLO = (orgdata.DefCodianLO > 9) ? orgdata.DefCodianLO.ToString() : "0" + orgdata.DefCodianLO.ToString();
                        xWriter.WriteElementString("DefCodianLO", CodianLO);
                        //ZD 101869 End
                        xWriter.WriteElementString("ShowVideoLayout", orgdata.ShowVideoLayout.ToString());//ZD 101931
                        xWriter.WriteElementString("EWSConfAdmins", orgdata.EWSConfAdmins);//ZD 102085
                        xWriter.WriteElementString("EnableCalDefaultDisplay", orgdata.EnableCalDefaultDisplay.ToString());//ZD 102356
                        xWriter.WriteElementString("EnableWaitList", orgdata.EnableWaitList.ToString());//ZD 102532  
						xWriter.WriteElementString("PersonnelAlert", orgdata.PersonnelAlert);//ZD 103046                        
                        xWriter.WriteElementString("VideoRefreshTimer", orgdata.VideoRefreshTimer.ToString());//ZD 103398
                        //ZD 102514 Start
                        xWriter.WriteElementString("OnSiteAVSupportBuffer", orgdata.OnSiteAVSupportBuffer.ToString());
                        xWriter.WriteElementString("CallMonitoringBuffer", orgdata.CallMonitoringBuffer.ToString());
                        xWriter.WriteElementString("DedicatedVNOCOperatorBuffer", orgdata.DedicatedVNOCOperatorBuffer.ToString());
                        //ZD Start 102514 End
                        xWriter.WriteElementString("EnableTravelAvoidTrack", orgdata.EnableTravelAvoidTrack.ToString()); // ZD 103216
                        //ZD 104021 - Start
                        xWriter.WriteElementString("EnableBJNIntegration", orgdata.EnableBJNIntegration.ToString());
                        xWriter.WriteElementString("BJNMeetingType", orgdata.BJNMeetingType.ToString());
                        xWriter.WriteElementString("BJNSelectOption", orgdata.BJNSelectOption.ToString());
                        //ZD 104021 - End
                        xWriter.WriteElementString("BJNDisplay", orgdata.BJNDisplay.ToString());//ZD 104116
                        xWriter.WriteElementString("AssignParticipantsToRoom", orgdata.AssignPartyToRoom.ToString()); // ZD 102916
                        xWriter.WriteElementString("EnableAudbridgefreebusy", orgdata.EnableAudbridgefreebusy.ToString()); // ZD 104854-Disney

                        // ZD 104151 Start
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Gt("Status", 0));
                        criterionList.Add(Expression.Eq("deleted", 0));
                        criterionList.Add(Expression.Eq("Synchronous", 0));
                        criterionList.Add(Expression.Or(Expression.Eq("orgId", organizationID), Expression.Eq("isPublic", 1)));
                        Mcus = m_vrmMCU.GetByCriteria(criterionList);
                        xWriter.WriteStartElement("EmptyConferencePush");
                        if (Mcus.Count > 0)
                        {
                            for (int j = 0; j < Mcus.Count; j++)
                            {
                                xWriter.WriteStartElement("MCU");
                                xWriter.WriteElementString("Name", Mcus[j].BridgeName);
                                xWriter.WriteElementString("ID", Mcus[j].BridgeID.ToString());
                                xWriter.WriteElementString("Selected", (orgdata.EmptyConferencePush == Mcus[j].BridgeID) ? "1" : "0");
                                xWriter.WriteEndElement();
                            }
                        }
                        xWriter.WriteEndElement();
                        // ZD 104151 End
						xWriter.WriteElementString("EnablePoolOrderSelection", orgdata.EnablePoolOrderSelection.ToString());//ZD 104221
                        xWriter.WriteElementString("EnableBlockUserDI", orgdata.EnableBlockUserDI.ToString());//ZD 104862
                    }
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.Flush();
                    obj.outXml = OrgOUTXML.ToString();
                    return true;
                }
               
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

        #region Get Organization Settings for Loading Session
        /// <summary>
        /// Get Organization Settings for Loading Session
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetAllOrgSettings(ref vrmDataObject obj)
        {
            string responseMsg = "", color = "";
            int responseTime = 60, i = 0, hldys = 0, sysAcc = 0, EM7Acc = 0, isImages = 0; //FB 2943
            vrmfact = new vrmFactory(ref obj); 
            vrmImg = new imageFactory(ref obj); //FB 2136
            int priority = 0, typeId = 0;//FB 1861
            sysApprover sa = null;
            vrmUser approver = null;
            holidaysType sysHolidayType = null;
            sysTechData sysTechDta = null;
            vrmOrganization vrmOrg = null;
            holidays sysHoliday = null;
            IList<holidays> sysHolidays = null;
            IList<sysApprover> sysApprovers = null;
            OrgOUTXML = new StringBuilder();
            xSettings = null;
            obj.outXml = "";
            List<sysTimeZonePref> sysTZPrefs = null;
            sysTimeZonePref stzPref = null;
            string rmxLO, mgcLO, ctsLO, ctmsLO = "", imgDt = "",CodianLO=""; //ZD 101869
            vrmImage imObj = null;
            string langID = "0";
            cryptography.Crypto crypto = null;
            int networkResponseTIme = 0; //FB 2993
            List<ICriterion> criterionList = new List<ICriterion>();
            List<vrmMCU> Mcus = null; // ZD 104151
            try
            {
                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//GetAllOrgSettings/organizationID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out organizationID);

                        if (organizationID < 11)
                        {
                            obj.outXml = "<error>Invalid Organization ID</error>";
                            return false;
                        }

                        xNode = xNavigator.SelectSingleNode("//GetAllOrgSettings/isBannerImages"); //FB 2943 - Reduce the image payload in the command (GetAllOrgSettings) if request is from outlook.
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out isImages);

                    }
                }

                m_IOrgSettingDAO.clearFetch();
                OrgData orgdata = m_IOrgSettingDAO.GetByOrgId(organizationID);
                if (orgdata == null)
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OrgOUTXML, xSettings))
                {
                    xWriter.WriteStartElement("GetAllOrgSettings");
                    xWriter.WriteElementString("organizationID", organizationID.ToString());

                    vrmOrg = m_IOrgDAO.GetById(organizationID);
                    if (vrmOrg == null)
                    {
                        obj.outXml = "<error>Invalid Organization ID</error>";
                        return false;
                    }
                    xWriter.WriteElementString("organizationName", vrmOrg.orgname);
                    if (isImages == 1) //FB 2943 Starts
                    {
                        xWriter.WriteElementString("LogoImage"," ");
                        xWriter.WriteElementString("LobytopImage", " ");
                        xWriter.WriteElementString("LobytopHighImage", " ");
                    }
                    else
                    {
                        xWriter.WriteElementString("LogoImage", GetImage(orgdata.LogoImageId));
                        xWriter.WriteElementString("LobytopImage", GetImage(orgdata.LobytopImageId));
                        xWriter.WriteElementString("LobytopHighImage", GetImage(orgdata.LobytopHighImageId));
                    } //FB 2943 Ends
                    xWriter.WriteStartElement("TimezoneSystems");
                    sysTZPrefs = new List<sysTimeZonePref>();
                    sysTZPrefs = m_ISysTimeZonePrefDAO.GetAllPrefTimeZones(m_configPath);
                    for (i = 0; i < sysTZPrefs.Count; i++)
                    {
                        stzPref = null;
                        stzPref = sysTZPrefs[i];
                        xWriter.WriteStartElement("TimezoneSystem");
                        xWriter.WriteElementString("ID", stzPref.systemid.ToString());
                        xWriter.WriteElementString("Name", stzPref.name.ToString());
                        xWriter.WriteFullEndElement(); //ZD 101086
                    }
                    xWriter.WriteFullEndElement(); //ZD 101086

                    sysTechDta = new sysTechData();
                    sysTechDta = m_ISysTechDAO.GetTechByOrgId(organizationID);
                    if (sysTechDta != null)
                    {
                        xWriter.WriteStartElement("ContactDetails");
                        xWriter.WriteElementString("Name", m_UtilFactory.ReplaceOutXMLSpecialCharacters(sysTechDta.name)); //ZD 104391
                        xWriter.WriteElementString("Email",  m_UtilFactory.ReplaceOutXMLSpecialCharacters(sysTechDta.email)); //ZD 104391
                        xWriter.WriteElementString("Phone", m_UtilFactory.ReplaceOutXMLSpecialCharacters(sysTechDta.phone));//ZD 104391
                        xWriter.WriteElementString("AdditionInfo", m_UtilFactory.ReplaceOutXMLSpecialCharacters(sysTechDta.info));//ZD 104391
                        xWriter.WriteFullEndElement(); //ZD 101086
                    }
                    xWriter.WriteStartElement("accountingLogics");

                    m_IAccSchemeDAO.clearFetch();
                    List<sysAccScheme> accSchemes = m_IAccSchemeDAO.GetAll();
                    if (accSchemes != null)
                    {
                        for (sysAcc = 0; sysAcc < accSchemes.Count; sysAcc++)
                        {
                            xWriter.WriteStartElement("logic");
                            xWriter.WriteElementString("ID", accSchemes[sysAcc].id.ToString());
                            xWriter.WriteElementString("name", accSchemes[sysAcc].name);
                            xWriter.WriteFullEndElement(); //ZD 101086
                        }
                    }
                    xWriter.WriteFullEndElement(); //ZD 101086
                    if (orgdata != null)
                    {
                        xWriter.WriteStartElement("licensedetails");
                        xWriter.WriteElementString("MaxRoomLimit", sysSettings.RoomLimit.ToString());
                        xWriter.WriteElementString("MaxVideoRoomLimit", sysSettings.MaxVidRooms.ToString());
                        xWriter.WriteElementString("MaxNonVideoRoomLimit", sysSettings.MaxNVidRooms.ToString());
                        xWriter.WriteElementString("MaxVMRRoomLimit", sysSettings.MaxVMRooms.ToString());
                        xWriter.WriteElementString("MaxiControlRoomLimit", sysSettings.MaxiControlRooms.ToString());//ZD 101098
                        xWriter.WriteElementString("MaxMCULimit", sysSettings.MCULimit.ToString());
                        xWriter.WriteElementString("MaxMCUEnchancedLimit", sysSettings.MCUEnchancedLimit.ToString());
                        xWriter.WriteElementString("MaxUserLimit", sysSettings.UserLimit.ToString());
                        xWriter.WriteElementString("MaxExchangeUsers", sysSettings.MaxExcUsrs.ToString());
                        xWriter.WriteElementString("MaxDominoUsers", sysSettings.MaxDomUsrs.ToString());
                        xWriter.WriteElementString("MaxMobileUsers", sysSettings.MaxMobUsrs.ToString());
                        xWriter.WriteElementString("MaxEndPoints", sysSettings.MaxEndPts.ToString());
                        xWriter.WriteElementString("MaxFacilities", sysSettings.MaxFacilities.ToString());
                        xWriter.WriteElementString("MaxCatering", sysSettings.MaxCatering.ToString());
                        xWriter.WriteElementString("MaxHousekeeping", sysSettings.MaxHousekeeping.ToString());
                        xWriter.WriteElementString("MaxGuestsPerAccount", sysSettings.MaxGuests.ToString());
                        xWriter.WriteElementString("IsLDAP", sysSettings.IsLDAP.ToString());//ZD 101443
                        xWriter.WriteFullEndElement(); //ZD 101086

                        xWriter.WriteElementString("accountingLogic", orgdata.AccountingLogic.ToString());
                        xWriter.WriteElementString("billPoint2Point", orgdata.BillPoint2Point.ToString());
                        xWriter.WriteElementString("allowAllocation", orgdata.overAllocation.ToString());
                        xWriter.WriteElementString("enableCustomAttribute", orgdata.EnableCustomOption);
                        xWriter.WriteElementString("multiDepartment", orgdata.MultipleDepartments.ToString());
                        
                        if (isImages == 1) //FB 2943 Starts
                        {
                            xWriter.WriteElementString("MailLogoImage", " ");
                            xWriter.WriteElementString("FooterImage", " ");
                            xWriter.WriteElementString("FooterImgName", " ");
                            xWriter.WriteElementString("FooterMessage", " ");
                        }
                        else
                        {
                            xWriter.WriteElementString("MailLogoImage", GetImage(orgdata.MailLogo));

                            imgDt = "";
                            imObj = null;

                            if (orgdata.FooterImage > 0)
                            {
                                imObj = m_IImageDAO.GetById(orgdata.FooterImage);
                                if (imObj != null)
                                    imgDt = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage); //FB 2136

                                xWriter.WriteElementString("FooterImage", imgDt);
                                xWriter.WriteElementString("FooterImgName", "Org_" + orgdata.OrgId.ToString() + "_footerimage.gif"); //+ "Org_ô" + orgdata.OrgId + "|" + orgdata.FooterImage + "ô_footer.gif" +
                            }
                            else
                            {
                                xWriter.WriteElementString("FooterImage", string.Empty);
                                xWriter.WriteElementString("FooterImgName", string.Empty);
                            }
                            xWriter.WriteElementString("FooterMessage", orgdata.FooterMessage);
                        } //FB 2943 Ends
                        

                        xWriter.WriteElementString("iCalReqEmailId", orgdata.IcalReqEmailID);
                        xWriter.WriteElementString("showBridgeExt", orgdata.IsBridgeExtNo.ToString());
                        xWriter.WriteElementString("sendIcal", orgdata.SendIcal.ToString()); 

                        xWriter.WriteElementString("TimezoneSystemID", orgdata.tzsystemid.ToString());
                        xWriter.WriteElementString("AutoAcceptModifiedConference", orgdata.AutoAcceptModConf.ToString());
                        xWriter.WriteElementString("EnableRecurringConference", orgdata.recurEnabled.ToString());
                        xWriter.WriteElementString("EnableDynamicInvite", orgdata.dynamicinviteenabled.ToString());
                        xWriter.WriteElementString("EnableP2PConference", orgdata.Connect2.ToString()); //FB 1070
                        xWriter.WriteElementString("EnableRealtimeDisplay", orgdata.RealtimeStatus.ToString());
                        xWriter.WriteElementString("EnableDialout", orgdata.DialOut.ToString());
                        xWriter.WriteElementString("DefaultConferencesAsPublic", orgdata.DefaultToPublic.ToString());
                        xWriter.WriteElementString("DefaultConferenceType", orgdata.DefaultConferenceType.ToString());
                        xWriter.WriteElementString("RFIDTagValue", orgdata.RFIDTagValue.ToString());//FB 2724
                        xWriter.WriteElementString("iControlTimeout", orgdata.iControlTimeout.ToString());//FB 2724
                        xWriter.WriteElementString("VideoRefreshTimer", orgdata.VideoRefreshTimer.ToString());//ZD 103398
                        xWriter.WriteElementString("EnableRoomConference", orgdata.EnableRoomConference.ToString());
                        xWriter.WriteElementString("EnableAudioVideoConference", orgdata.EnableAudioVideoConference.ToString());
                        xWriter.WriteElementString("EnableAudioOnlyConference", orgdata.EnableAudioOnlyConference.ToString());
                        xWriter.WriteElementString("EnableNumericID", orgdata.EnableNumericID.ToString());//FB 2870
                        xWriter.WriteElementString("EnableHotdeskingConference", orgdata.EnableHotdeskingConference.ToString());//ZD 100719
                        xWriter.WriteElementString("DefaultCalendarToOfficeHours", orgdata.DefaultCalendarToOfficeHours.ToString());
                        xWriter.WriteElementString("RoomTreeExpandLevel", orgdata.RoomTreeExpandLevel);
                        xWriter.WriteElementString("EnableCustomOption", orgdata.EnableCustomOption);
                        xWriter.WriteElementString("EnableBufferZone", orgdata.EnableBufferZone);
                        xWriter.WriteElementString("SetupTime", orgdata.SetupTime.ToString()); //FB 2398
                        xWriter.WriteElementString("TearDownTime", orgdata.TearDownTime.ToString());
                        xWriter.WriteElementString("EnableDoubleBooking", orgdata.doublebookingenabled.ToString());
                        xWriter.WriteElementString("AutoApproveImmediate", orgdata.autoApproveImmediate.ToString());
                        xWriter.WriteElementString("sendApprovalIcal", orgdata.SendApprovalIcal.ToString());
                        xWriter.WriteElementString("isUniquePassword", orgdata.isUniquePassword.ToString());
                        xWriter.WriteElementString("ReminderMask", orgdata.ReminderMask.ToString());
                        
                        xWriter.WriteElementString("ExternalAttachments", orgdata.SendAttachmentsExternal.ToString());
                        xWriter.WriteElementString("filterTelepresence", orgdata.TelepresenceFilter.ToString()); 
                        xWriter.WriteElementString("isDeptUser", orgdata.isDeptUser.ToString()); 
                        xWriter.WriteElementString("EnablePIMServiceType", orgdata.EnablePIMServiceType.ToString());

                        xWriter.WriteElementString("EnableConferencePassword", orgdata.EnableConfPassword.ToString());
                        xWriter.WriteElementString("EnablePublicConference", orgdata.EnablePublicConf.ToString());
                        xWriter.WriteElementString("EnableRoomParam", orgdata.EnableRoomParam.ToString());
                        //xWriter.WriteElementString("EnableSecurityBadge", orgdata.EnableSecurityBadge.ToString());
                        //xWriter.WriteElementString("SecurityBadgeType", orgdata.SecurityBadgeType.ToString());
                        //xWriter.WriteElementString("SecurityDeskEmailId", orgdata.SecurityDeskEmailId);
                        
                        rmxLO = (orgdata.DefPolycomRMXLO > 9) ? orgdata.DefPolycomRMXLO.ToString() : "0" + orgdata.DefPolycomRMXLO.ToString();
                        mgcLO = (orgdata.DefPolycomMGCLO > 9) ? orgdata.DefPolycomMGCLO.ToString() : "0" + orgdata.DefPolycomMGCLO.ToString();
                        ctsLO = (orgdata.DefCiscoTPLO > 9) ? orgdata.DefCiscoTPLO.ToString() : "0" + orgdata.DefCiscoTPLO.ToString();
                        ctmsLO = (orgdata.DefCTMSLO > 9) ? orgdata.DefCTMSLO.ToString() : "0" + orgdata.DefCTMSLO.ToString();
                        xWriter.WriteElementString("defPolycomRMXLO", rmxLO);
                        xWriter.WriteElementString("defPolycomMGCLO", mgcLO);
                        xWriter.WriteElementString("defCiscoTPLO", ctsLO);
                        xWriter.WriteElementString("defCTMSLO", ctmsLO);
                        xWriter.WriteElementString("McuSetupTime", orgdata.McuSetupTime.ToString()); //FB 2440
                        xWriter.WriteElementString("MCUTeardonwnTime", orgdata.MCUTeardonwnTime.ToString()); //FB 2440
                        xWriter.WriteElementString("MCUSetupDisplay", orgdata.MCUSetupDisplay.ToString()); //FB 2998
                        xWriter.WriteElementString("MCUTearDisplay", orgdata.MCUTearDisplay.ToString()); //FB 2998

                        xWriter.WriteElementString("OrgMessage", orgdata.DefaultTxtMsg.Trim());//FB 2486

                        xWriter.WriteStartElement("SystemAvailableTime");
                        xWriter.WriteElementString("IsOpen24Hours", orgdata.Open24hrs.ToString());
                        
                        //xWriter.WriteElementString("StartTime", orgdata.SystemStartTime.ToString("hh:mm tt"));
                        //xWriter.WriteElementString("EndTime", orgdata.SystemEndTime.ToString("hh:mm tt"));
                        xWriter.WriteElementString("StartTime", orgdata.SystemStartTime.ToString(new CultureInfo("en-US"))); //ZD 103309
                        xWriter.WriteElementString("EndTime", orgdata.SystemEndTime.ToString(new CultureInfo("en-US")));//ZD 103309

                        xWriter.WriteElementString("DaysClosed", orgdata.Offdays);
                        xWriter.WriteFullEndElement(); //ZD 101086
                        // FB 1861
                        xWriter.WriteStartElement("SystemHolidays");
                        m_IHolidayDAO = m_OrgDAO.GetHolidaysDao();
                        sysHoliday = null;
                        sysHolidays = null;
                        sysHolidays = m_IHolidayDAO.GetHolidaysByOrgId(organizationID);

                        if (sysHolidays.Count > 0)
                        {
                            xWriter.WriteStartElement("Holidays");
                            for (hldys = 0; hldys < sysHolidays.Count; hldys++)
                            {
                                sysHoliday = sysHolidays[hldys];
                                xWriter.WriteStartElement("Holiday");
                                xWriter.WriteElementString("Date", sysHoliday.Date.ToShortDateString());
                                xWriter.WriteElementString("HolidayType", sysHoliday.HolidayType.ToString());

                                if (typeId == 0 || typeId != sysHoliday.HolidayType)
                                {
                                    m_IHolidayDAOType = m_OrgDAO.GetHolidaysTypeDAO();
                                    sysHolidayType = null;
                                    sysHolidayType = m_IHolidayDAOType.GetHolidayTypebyID(sysHoliday.HolidayType);
                                    if (sysHolidayType != null)
                                    {
                                        priority = sysHolidayType.Priority;
                                        color = sysHolidayType.Color;
                                    }
                                    xWriter.WriteElementString("Color", color);
                                    xWriter.WriteElementString("Priority", priority.ToString());
                                }
                                xWriter.WriteFullEndElement(); //ZD 101086
                            }
                            xWriter.WriteFullEndElement(); //ZD 101086
                        }
                        xWriter.WriteFullEndElement(); //ZD 101086

                        // FB 1861
                        xWriter.WriteStartElement("CompanyDetails");
                        xWriter.WriteElementString("Logo", orgdata.Logo);
                        xWriter.WriteElementString("CompanyTel", orgdata.CompanyTel);
                        xWriter.WriteElementString("CompanyEmail", orgdata.CompanyEmail);
                        xWriter.WriteElementString("CompanyURL", orgdata.CompanyURL);
                        xWriter.WriteFullEndElement(); //ZD 101086

                        xWriter.WriteStartElement("ProtocolDetails");
                        xWriter.WriteElementString("ISDNLineCost", orgdata.ISDNLineCost.ToString());
                        xWriter.WriteElementString("ISDNPortCost", orgdata.ISDNPortCost.ToString());
                        xWriter.WriteElementString("IPLineCost", orgdata.IPLineCost.ToString());
                        xWriter.WriteElementString("IPPortCost", orgdata.IPPortCost.ToString());
                        xWriter.WriteElementString("ISDNTimeFrame", orgdata.ISDNTimeFrame.ToString());
                        xWriter.WriteFullEndElement(); //ZD 101086

                        xWriter.WriteElementString("AllLocation", orgdata.AllLocation.ToString());
                        xWriter.WriteElementString("BillRealTime", orgdata.BillRealTime.ToString());
                        xWriter.WriteElementString("Threshold", orgdata.Threshold.ToString());

                        xWriter.WriteElementString("adminEmail1", orgdata.adminEmail1.ToString());
                        xWriter.WriteElementString("adminEmail2", orgdata.adminEmail2.ToString());
                        xWriter.WriteElementString("adminEmail3", orgdata.adminEmail3.ToString());

                        xWriter.WriteElementString("RoomLimit", orgdata.RoomLimit.ToString());
                        xWriter.WriteElementString("MCULimit", orgdata.MCULimit.ToString());
                        xWriter.WriteElementString("MCUEnchancedLimit", orgdata.MCUEnchancedLimit.ToString()); //FB 2486
                        xWriter.WriteElementString("UserLimit", orgdata.UserLimit.ToString());
                        xWriter.WriteElementString("ExchangeUserLimit", orgdata.ExchangeUserLimit.ToString());
                        xWriter.WriteElementString("DominoUserLimit", orgdata.DominoUserLimit.ToString());
                        xWriter.WriteElementString("MobileUserLimit", orgdata.MobileUserLimit.ToString()); //FB 1979
                        xWriter.WriteElementString("WebexUserLimit", orgdata.WebexUserLimit.ToString()); //ZD 100221
                        xWriter.WriteElementString("EndPoints", orgdata.MaxEndpoints.ToString());
                        xWriter.WriteElementString("VideoRooms", orgdata.MaxVideoRooms.ToString());
                        xWriter.WriteElementString("NonVideoRooms", orgdata.MaxNonVideoRooms.ToString());
                        xWriter.WriteElementString("VMRRooms", orgdata.MaxVMRRooms.ToString());
                        xWriter.WriteElementString("iControlRooms", orgdata.MaxiControlRooms.ToString());//ZD 101098
                        //FB 2694 Start
                        xWriter.WriteElementString("HotdeskingVideoRooms", orgdata.MaxVCHotdesking.ToString());
                        xWriter.WriteElementString("HotdeskingNonVideoRooms", orgdata.MaxROHotdesking.ToString());
                        //FB 2694 End
                        //FB 2426 Start
                        xWriter.WriteElementString("GuestRooms", orgdata.GuestRoomLimit.ToString());
                        xWriter.WriteElementString("GuestRoomPerUser", orgdata.GuestRoomPerUser.ToString());
                        //FB 2426 End
                        xWriter.WriteElementString("EnableFacilites", orgdata.EnableFacilities.ToString());
                        xWriter.WriteElementString("EnableCatering", orgdata.EnableCatering.ToString());
                        xWriter.WriteElementString("EnableHouseKeeping", orgdata.EnableHousekeeping.ToString());
                        xWriter.WriteElementString("EnableAPIs", orgdata.EnableAPI.ToString());
                        //xWriter.WriteElementString("EnablePC", orgdata.EnablePCModule.ToString());//FB 2347 //FB 2693
                        xWriter.WriteElementString("EnableCloud", orgdata.EnableCloud.ToString()); //FB 2599
                        xWriter.WriteElementString("EnablePublicRoom", orgdata.EnablePublicRoomService.ToString()); //FB 2594
                        xWriter.WriteElementString("ConferenceCode", orgdata.ConferenceCode.ToString());
                        xWriter.WriteElementString("LeaderPin", orgdata.LeaderPin.ToString());
                        xWriter.WriteElementString("AdvAvParams", orgdata.AdvAvParams.ToString());
                        xWriter.WriteElementString("AudioParams", orgdata.AudioParams.ToString());
                        xWriter.WriteElementString("isVIP", orgdata.isVIP.ToString());// FB 1864
                        xWriter.WriteElementString("isAssignedMCU", orgdata.isAssignedMCU.ToString());// FB 1901
                        xWriter.WriteElementString("isMultiLingual", orgdata.isMultiLingual.ToString());  //FB 1830 
                        xWriter.WriteElementString("PIMNotifications", orgdata.PluginConfirmations.ToString());// FB 2141
                        xWriter.WriteElementString("EnableRoomServiceType", orgdata.EnableRoomServiceType.ToString());//FB 2219
                        xWriter.WriteElementString("isSpecialRecur", orgdata.SpecialRecur.ToString());// FB 2052                                        
                        xWriter.WriteElementString("EnableImmConf", orgdata.EnableImmediateConference.ToString());// FB 2036
                        xWriter.WriteElementString("EnableAudioBridges", orgdata.EnableAudioBridges.ToString());//FB 2023 
                        //FB 2359 Start
                        xWriter.WriteElementString("EnableConferencePassword", orgdata.EnableConfPassword.ToString());
                        xWriter.WriteElementString("EnablePublicConference", orgdata.EnablePublicConf.ToString());
                        xWriter.WriteElementString("EnableRoomParam", orgdata.EnableRoomParam.ToString());
                        //FB 2359 End
                        
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("EmailLangId", orgdata.EmailLangId));

                        List<vrmEmailLanguage> emailLangs = m_IEmailLanguageDAO.GetByCriteria(criterionList);
                        if (emailLangs != null && (emailLangs.Count > 0))
                        {
                            xWriter.WriteElementString("OrgEmailLanguageName", emailLangs[0].EmailLanguage);
                            langID = orgdata.EmailLangId.ToString();
                        }
                        else
                            xWriter.WriteElementString("OrgEmailLanguageName", string.Empty);
                        xWriter.WriteElementString("OrgLanguage", orgdata.Language.ToString());// FB 2283                                        
                        xWriter.WriteElementString("EmailDateFormat", orgdata.EmailDateFormat.ToString());// FB 2555                                     
                        xWriter.WriteElementString("OrgEmailLanguageID", orgdata.EmailLangId.ToString());// FB 2283                                       
                        xWriter.WriteElementString("EnablePasswordRule", orgdata.EnablePasswordRule.ToString());// FB 2339
                        xWriter.WriteElementString("DedicatedVideo", orgdata.DedicatedVideo.ToString());//FB 2334
                        xWriter.WriteStartElement("IMTalk");
                        xWriter.WriteElementString("IMEnabled", orgdata.IMEnabled.ToString());
                        xWriter.WriteElementString("refreshTime", orgdata.IMRefreshConn.ToString());
                        xWriter.WriteElementString("maxUnitConnection", orgdata.IMMaxUnitConn.ToString());
                        xWriter.WriteElementString("maxSystemConnection", orgdata.IMMaxSysConn.ToString());
                        xWriter.WriteFullEndElement(); //ZD 101086

                        responseMsg = orgdata.responsemessage;
                        responseTime = orgdata.responsetime;
                    }
                    xWriter.WriteElementString("MCUAlert", orgdata.MCUAlert.ToString());//FB 2472
                    xWriter.WriteStartElement("approvers");
                    xWriter.WriteElementString("responseMsg", responseMsg);
                    xWriter.WriteElementString("responseTime", responseTime.ToString());

                    m_ISysApproverDAO.clearFetch();
                    sysApprovers = m_ISysApproverDAO.GetSysApproversByOrgId(organizationID);
                    if (sysApprovers != null)
                    {
                        for (i = 0; i < sysApprovers.Count; i++)
                        {
                            sa = null;
                            sa = sysApprovers[i];
                            approver = m_IuserDAO.GetByUserId(sa.approverid);
                            if (approver != null)
                            {
                                xWriter.WriteStartElement("approver");
                                xWriter.WriteElementString("ID", sa.approverid.ToString());
                                xWriter.WriteElementString("firstName", approver.FirstName);
                                xWriter.WriteElementString("lastName", approver.LastName);
                                xWriter.WriteFullEndElement(); //ZD 101086
                            }
                        }
                    }
                    xWriter.WriteFullEndElement(); //ZD 101086

                    xWriter.WriteElementString("CustomerID", orgdata.CustomerID);
                    xWriter.WriteElementString("CustomerName", orgdata.CustomerName);

                    //ZD 100350
                    /*
                    List<ICriterion> criteria = new List<ICriterion>();
                    criteria.Add(Expression.Eq("orgId", organizationID));
                    criteria.Add(Expression.Eq("Type", "O"));
                    IList<ESMailUsrRptSettings> sysESRpt = m_ESMailUsrRptSetDAO.GetByCriteria(criteria);
                    if (sysESRpt != null && sysESRpt.Count > 0) 
                    {
                        xWriter.WriteElementString("RptDestination", sysESRpt[0].RptDestination);
                        xWriter.WriteElementString("FrequencyCount", sysESRpt[0].FrequencyCount.ToString());
                    }
                     */
 
                    VrmVidyoSettings VidyoSettings = m_IVidyoSetDAO.GetVidyoSettingsByOrgId(organizationID);
                    crypto = new cryptography.Crypto();
                    if (VidyoSettings != null)
                    {
                        //if (!string.IsNullOrEmpty(VidyoSettings.Password)) //FB 2717 //FB 3054
                        //    VidyoSettings.Password = crypto.decrypt(VidyoSettings.Password);
                        xWriter.WriteStartElement("VidyoSettings");
                        xWriter.WriteElementString("VidyoURL", VidyoSettings.VidyoURL);
                        xWriter.WriteElementString("VidyoLogin", VidyoSettings.AdminLogin);
                        xWriter.WriteElementString("VidyoPassword", VidyoSettings.Password);
                        xWriter.WriteElementString("ProxyAdd", VidyoSettings.ProxyAddress);
                        xWriter.WriteElementString("VidyoPort", VidyoSettings.Port.ToString());
                        xWriter.WriteFullEndElement(); //ZD 101086
                    }
                    m_EM7SettingsDao.clearFetch();
                    List<vrmEM7OrgSilo> EM7accSchemes = m_EM7SettingsDao.GetAll();
                    if (EM7accSchemes != null)
                    {
                        xWriter.WriteStartElement("EM7Organization");
                        for (EM7Acc = 0; EM7Acc < EM7accSchemes.Count; EM7Acc++)
                        {
                            xWriter.WriteStartElement("Profile");
                            xWriter.WriteElementString("ID", EM7accSchemes[EM7Acc].OrgID.ToString());
                            xWriter.WriteElementString("Name", EM7accSchemes[EM7Acc].EM7Orgname);
                            xWriter.WriteFullEndElement(); //ZD 101086
                        }
                        xWriter.WriteElementString("EM7SelectedSilo", orgdata.EM7OrgId.ToString());
                        xWriter.WriteFullEndElement(); //ZD 101086
                    }
                    xWriter.WriteElementString("MailBlocked", orgdata.MailBlocked.ToString());//FB 1860 
                    xWriter.WriteElementString("MailBlockedDate", orgdata.MailBlockedDate.ToString());//FB 1860 
                    xWriter.WriteElementString("WorkingHours", orgdata.WorkingHours.ToString());//FB 2343 
                    xWriter.WriteElementString("EnableVMR", orgdata.EnableVMR.ToString());
                    //FB 2348
                    xWriter.WriteElementString("EnableSurvey", orgdata.EnableSurvey.ToString());
                    xWriter.WriteElementString("SurveyOption", orgdata.SurveyOption.ToString());
                    xWriter.WriteElementString("SurveyURL", orgdata.SurveyURL);
                    xWriter.WriteElementString("TimeDuration", orgdata.TimeDuration.ToString());
                    //FB 2348
                    xWriter.WriteElementString("EnableEPDetails", orgdata.EnableEPDetails.ToString());//FB 2401
                    xWriter.WriteElementString("EnableAcceptDecline", orgdata.EnableAcceptDecline.ToString()); //FB 2419
                    xWriter.WriteElementString("DefaultLineRate", orgdata.DefLinerate.ToString()); //FB 2429
                    xWriter.WriteElementString("EnableFileWhiteList", orgdata.EnableFileWhiteList.ToString());//ZD 100263
                    xWriter.WriteElementString("FileWhiteList", orgdata.FileWhiteList); //ZD 100263
                    //FB 2426 Start
                    xWriter.WriteStartElement("OnflyTier");
                    xWriter.WriteElementString("DefTopTier", orgdata.TopTier);
                    xWriter.WriteElementString("DefMiddleTier", orgdata.MiddleTier);
                    xWriter.WriteElementString("OnflyTopTierID", orgdata.OnflyTopTierID.ToString());
                    xWriter.WriteElementString("OnflyMiddleTierID", orgdata.OnflyMiddleTierID.ToString());
                    xWriter.WriteFullEndElement(); //ZD 101086
                    //FB 2426 End
                   //ZD 100068 Starts
                    xWriter.WriteStartElement("VMRTier");
                    xWriter.WriteElementString("VMRTopTierID", orgdata.VMRTopTierID.ToString());
                    xWriter.WriteElementString("VMRMiddleTierID", orgdata.VMRMiddleTierID.ToString());
                    xWriter.WriteFullEndElement(); //ZD 101086
                    //ZD 100068 Ends
                    //FB 2469 - Starts
                    xWriter.WriteElementString("EnableConfTZinLoc", orgdata.EnableConfTZinLoc.ToString());
                    xWriter.WriteElementString("SendConfirmationEmail", orgdata.SendConfirmationEmail.ToString());
                    //FB 2469 - End
                    xWriter.WriteElementString("DefaultConfDuration", orgdata.DefaultConfDuration.ToString());//FB 2501
                    if (orgdata.Connect2 == 0)  //FB 2430
                        orgdata.EnableSmartP2P = 0;

                    xWriter.WriteElementString("EnableSmartP2P", orgdata.EnableSmartP2P.ToString());//FB 2430
                    xWriter.WriteElementString("MaxPublicVMRParty", orgdata.MaxPublicVMRParty.ToString()); //FB 2550
                    //FB 2571 Start
                    xWriter.WriteElementString("EnableFECC", orgdata.EnableFECC.ToString());
                    xWriter.WriteElementString("DefaultFECC", orgdata.DefaultFECC.ToString());
                    //FB 2571 End
                    //2598 Starts
                    xWriter.WriteElementString("EnableCallmonitor", orgdata.EnableCallmonitor.ToString());
                    xWriter.WriteElementString("EnableEM7", orgdata.EnableEM7.ToString());
                    xWriter.WriteElementString("EnableCDR", orgdata.EnableCDR.ToString());
                    //2598 Ends
                    xWriter.WriteElementString("MeetandGreetBuffer", orgdata.MeetandGreetBuffer.ToString()); //FB 2609
                    //FB 2632 Starts
                    xWriter.WriteElementString("EnableCongSupport", orgdata.EnableCncigSupport.ToString());
                    xWriter.WriteElementString("MeetandGreetinEmail", orgdata.MeetandGreetinEmail.ToString());
                    xWriter.WriteElementString("OnSiteAVSupportinEmail", orgdata.AVOnsiteinEmail.ToString());
                    xWriter.WriteElementString("ConciergeMonitoringinEmail", orgdata.CncigMoniteringinEmail.ToString());
                    xWriter.WriteElementString("DedicatedVNOCOperatorinEmail", orgdata.VNOCinEmail.ToString());
                    //FB 2632 Ends
                    xWriter.WriteElementString("EnableRoomAdminDetails", orgdata.EnableRoomAdminDetails.ToString());//FB 2631
                    xWriter.WriteElementString("AlertforTier1", orgdata.AlertforTier1);//FB 2637
                    xWriter.WriteElementString("EnableDialPlan", orgdata.EnableDialPlan.ToString()); // FB 2636
                    //FB 2595 Starts
                    xWriter.WriteElementString("HardwareAdminEmail", orgdata.HardwareAdminEmail);
                    xWriter.WriteElementString("SecureSwitch", orgdata.SecureSwitch.ToString());
                    xWriter.WriteElementString("NetworkSwitching", orgdata.NetworkSwitching.ToString());
                    xWriter.WriteElementString("NetworkCallLaunch", orgdata.NetworkCallLaunch.ToString());
                    xWriter.WriteElementString("SecureLaunchBuffer", orgdata.SecureLaunchBuffer.ToString());
                    //FB 2595 Ends
                    xWriter.WriteElementString("EnableZulu", orgdata.EnableZulu.ToString());//FB 2588	
					//FB 2670 START
                    xWriter.WriteElementString("EnableOnsiteAV", orgdata.EnableOnsiteAV.ToString());
                    xWriter.WriteElementString("EnableMeetandGreet", orgdata.EnableMeetandGreet.ToString());
                    xWriter.WriteElementString("EnableConciergeMonitoring", orgdata.EnableConciergeMonitoring.ToString());
                    xWriter.WriteElementString("EnableDedicatedVNOC", orgdata.EnableDedicatedVNOC.ToString());
                    //FB 2670 END
                    xWriter.WriteElementString("EnableLinerate",orgdata.EnableLinerate.ToString());//FB 2641
                    xWriter.WriteElementString("EnableStartMode",orgdata.EnableStartMode.ToString());//FB 2641
					//FB 2693 Starts
                    xWriter.WriteElementString("PCUserLimit", orgdata.PCUserLimit.ToString());
                    xWriter.WriteElementString("EnableBlueJeans", orgdata.EnableBlueJeans.ToString());
                    xWriter.WriteElementString("EnableJabber", orgdata.EnableJabber.ToString());
                    xWriter.WriteElementString("EnableLync", orgdata.EnableLync.ToString());
                    xWriter.WriteElementString("EnableVidtel", orgdata.EnableVidtel.ToString());
                    //FB 2693 Ends
                    xWriter.WriteElementString("ThemeName", orgdata.ThemeType.ToString()); //FB 2779
                    xWriter.WriteElementString("EnableSingleRoomConfEmails", orgdata.SingleRoomConfMail.ToString());//FB 2817
                    xWriter.WriteElementString("DefaultSubject", orgdata.DefaultSubject.ToString());//FB 2659 
                    xWriter.WriteElementString("DefaultInvitation", orgdata.DefaultInvitation.ToString());//FB 2659
					xWriter.WriteElementString("EnableAdvancedReport", orgdata.EnableAdvancedReport.ToString());//FB 2593 
					xWriter.WriteElementString("EnableProfileSelection", orgdata.EnableProfileSelection.ToString()); //FB 2839

                    xWriter.WriteElementString("ShowCusAttInCalendar", orgdata.ShowCusAttInCalendar.ToString()); //ZD 100151
					//ZD 100164 START
                    xWriter.WriteElementString("EnableAdvancedUserOption", orgdata.EnableAdvancedUserOption.ToString());//FB 2817
                    //ZD 100164 END
                    //ZD 100707 Start
                    xWriter.WriteElementString("ShowHideVMR", orgdata.ShowHideVMR.ToString());
                    xWriter.WriteElementString("EnablePersonaVMR", orgdata.EnablePersonaVMR.ToString());
                    xWriter.WriteElementString("EnableRoomVMR", orgdata.EnableRoomVMR.ToString());
                    xWriter.WriteElementString("EnableExternalVMR", orgdata.EnableExternalVMR.ToString());
                    //ZD 100707 End
                    if (orgdata.ResponseTimeout > 0) //FB 2993
                        networkResponseTIme = orgdata.ResponseTimeout / 60;
                    else
                        networkResponseTIme = orgdata.ResponseTimeout;
                    xWriter.WriteElementString("ResponseTimeout", networkResponseTIme.ToString());//FB 2993
                    //ZD 100221 Starts
                    xWriter.WriteElementString("WebExURL", orgdata.WebExURL);
                    xWriter.WriteElementString("WebExSiteID", orgdata.WebExSiteID);
                    xWriter.WriteElementString("WebExPartnerID", orgdata.WebExPartnerID);
                    xWriter.WriteElementString("ScheduleLimit", orgdata.ScheduleLimit.ToString());//ZD 100899
                    //ZD 100221 Ends
                    xWriter.WriteElementString("EnableExpressConfType", orgdata.EnableExpressConfType.ToString()); //ZD 100704
                    xWriter.WriteElementString("EnableDetailedExpressForm", orgdata.EnableDetailedExpressForm.ToString()); //ZD 100834
					xWriter.WriteElementString("EnableWebExIntg", orgdata.EnableWebExIntg.ToString()); //ZD 100935
                    xWriter.WriteElementString("EnablePasswordExp", orgdata.EnablePasswordExp.ToString()); //ZD 100781
                    xWriter.WriteElementString("PasswordExpDays", orgdata.PasswordMonths.ToString()); //ZD 100781
					xWriter.WriteElementString("EnableRoomCalendarView", orgdata.EnableRoomCalendarView.ToString()); //ZD 100963
                    
                    //ZD 101019 START
                    xWriter.WriteElementString("VideoSourceURL1", orgdata.VideoSourceURL1);
                    xWriter.WriteElementString("VideoSourceURL2", orgdata.VideoSourceURL2);
                    xWriter.WriteElementString("VideoSourceURL3", orgdata.VideoSourceURL3);
                    xWriter.WriteElementString("VideoSourceURL4", orgdata.VideoSourceURL4);

                    xWriter.WriteElementString("ScreenPosition1", orgdata.ScreenPosition1);
                    xWriter.WriteElementString("ScreenPosition2", orgdata.ScreenPosition2);
                    xWriter.WriteElementString("ScreenPosition3", orgdata.ScreenPosition3);
                    xWriter.WriteElementString("ScreenPosition4", orgdata.ScreenPosition4);
                    //ZD 101019 END
                    //ZD 101120 Starts
                    xWriter.WriteElementString("EnableGuestLocWarningMsg", orgdata.EnableGuestLocWarningMsg.ToString());
                    xWriter.WriteElementString("GuestLocApprovalTime", orgdata.GuestLocApprovalTime.ToString());
                    //ZD 101120 Ends
					//ZD 100522 Starts
                    xWriter.WriteElementString("PasswordCharLength", orgdata.PasswordCharLength.ToString());
                    xWriter.WriteElementString("VMRPINChange", orgdata.VMRPINChange.ToString());
                    //ZD 100522 End
                    xWriter.WriteElementString("RoomDenialCommt", orgdata.RoomDenialCommt.ToString());//ZD 101445
                    xWriter.WriteElementString("SmartP2PNotify", orgdata.SmartP2PNotify.ToString());//ZD 100815
                    //ZD 101228 Starts
                    xWriter.WriteElementString("AVWOAlertTime", orgdata.AVWOAlertTime.ToString());
                    xWriter.WriteElementString("CatWOAlertTime", orgdata.CatWOAlertTime.ToString());
                    xWriter.WriteElementString("FacilityWOAlertTime", orgdata.FacilityWOAlertTime.ToString());
                    //ZD 101228 Ends
					xWriter.WriteElementString("WebExLaunch", orgdata.WebExLaunch.ToString()); //ZD 100513
                    xWriter.WriteElementString("EnableWETConference", orgdata.EnableWETConference.ToString()); //ZD 100513
					xWriter.WriteElementString("EnableTemplateBooking", orgdata.EnableTemplateBooking.ToString());//ZD 101562
                    xWriter.WriteElementString("EnableSetupTimeDisplay", orgdata.EnableSetupTimeDisplay.ToString()); //ZD 101755
                    xWriter.WriteElementString("EnableTeardownTimeDisplay", orgdata.EnableTeardownTimeDisplay.ToString()); //ZD 101755	
					xWriter.WriteElementString("EnableActMsgDelivery", orgdata.EnableActMsgDelivery.ToString());//ZD 101757
					//ZD 101527 - Start
                    xWriter.WriteElementString("EnableRPRMRoomSync", orgdata.EnableRPRMRoomSync.ToString());
                    xWriter.WriteElementString("RoomSyncAuto", orgdata.RoomSyncAuto.ToString());
                    xWriter.WriteElementString("RoomSyncPollTime", orgdata.RoomSyncPollTime.ToString());
                    //ZD 101527 - End
					xWriter.WriteElementString("EnablePartyCode", orgdata.EnablePartyCode.ToString());//ZD 101446
					//ZD 101869 start
                    CodianLO = (orgdata.DefCodianLO > 9) ? orgdata.DefCodianLO.ToString() : "0" + orgdata.DefCodianLO.ToString();
                    xWriter.WriteElementString("DefCodianLO", CodianLO);
                    //ZD 101869 End
                    xWriter.WriteElementString("ShowVideoLayout", orgdata.ShowVideoLayout.ToString());//ZD 101931
                    xWriter.WriteElementString("EWSConfAdmins", orgdata.EWSConfAdmins);//ZD 102085
                    xWriter.WriteElementString("EnableCalDefaultDisplay", orgdata.EnableCalDefaultDisplay.ToString());//ZD 102356
                    xWriter.WriteElementString("EnableWaitList", orgdata.EnableWaitList.ToString());//ZD 102532   
                    xWriter.WriteElementString("PersonnelAlert", orgdata.PersonnelAlert);//103046
                    //ZD 102514 Start
                    xWriter.WriteElementString("OnSiteAVSupportBuffer", orgdata.OnSiteAVSupportBuffer.ToString());
                    xWriter.WriteElementString("CallMonitoringBuffer", orgdata.CallMonitoringBuffer.ToString());
                    xWriter.WriteElementString("DedicatedVNOCOperatorBuffer", orgdata.DedicatedVNOCOperatorBuffer.ToString());
                    //ZD Start 102514 End
                    xWriter.WriteElementString("EnableTravelAvoidTrack", orgdata.EnableTravelAvoidTrack.ToString()); // ZD 103216
                    //ZD 104021 - Start
                    xWriter.WriteElementString("EnableBJNIntegration", orgdata.EnableBJNIntegration.ToString());
                    xWriter.WriteElementString("BJNMeetingType", orgdata.BJNMeetingType.ToString());
                    xWriter.WriteElementString("BJNSelectOption", orgdata.BJNSelectOption.ToString());
                    //ZD 104021 - End
                    xWriter.WriteElementString("BJNDisplay", orgdata.BJNDisplay.ToString());//ZD 104116
                    xWriter.WriteElementString("EnablePoolOrderSelection", orgdata.EnablePoolOrderSelection.ToString());//ZD 104221
                    //ZD 101443 Starts
                    xWriter.WriteElementString("AssignParticipantsToRoom", orgdata.AssignPartyToRoom.ToString()); // ZD 102916
                    xWriter.WriteElementString("EnableAudbridgefreebusy", orgdata.EnableAudbridgefreebusy.ToString()); // ZD 104854-Disney
                    xWriter.WriteElementString("EnableBlockUserDI", orgdata.EnableBlockUserDI.ToString());//ZD 104862
                    // ZD 104151 Start
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Gt("Status", 0));
                    criterionList.Add(Expression.Eq("deleted", 0));
                    criterionList.Add(Expression.Eq("Synchronous", 0));
                    criterionList.Add(Expression.Or(Expression.Eq("orgId", organizationID), Expression.Eq("isPublic", 1)));
                    Mcus = m_vrmMCU.GetByCriteria(criterionList);
                    xWriter.WriteStartElement("EmptyConferencePush");
                    if (Mcus.Count > 0)
                    {
                        for (int j = 0; j < Mcus.Count; j++)
                        {
                            xWriter.WriteStartElement("MCU");
                            xWriter.WriteElementString("Name", Mcus[j].BridgeName);
                            xWriter.WriteElementString("ID", Mcus[j].BridgeID.ToString());
                            xWriter.WriteElementString("Selected", (orgdata.EmptyConferencePush == Mcus[j].BridgeID) ? "1" : "0");
                            xWriter.WriteEndElement();
                        }
                    }
                    xWriter.WriteEndElement();
                    // ZD 104151 End

                    vrmLDAPConfig ldapSettings = null;
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", organizationID));
                    List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList);
                    if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                        ldapSettings = ldapSettingsList[0];

                    
                    if (ldapSettings == null)
                    {
                        xWriter.WriteStartElement("LDAP");
                        xWriter.WriteElementString("serverAddress", "");
                        xWriter.WriteElementString("loginName", "");
                        xWriter.WriteElementString("loginPassword", "");
                        xWriter.WriteElementString("portNo", "");
                        xWriter.WriteElementString("connectionTimeout", "");
                        xWriter.WriteElementString("loginkey", "");
                        xWriter.WriteElementString("synctime", "");
                        xWriter.WriteElementString("LDAPPrefix", "");
                        xWriter.WriteElementString("searchfilter", "");
                        xWriter.WriteStartElement("scheduler");
                        xWriter.WriteElementString("Time", "");
                        xWriter.WriteElementString("Days", "");
                        xWriter.WriteEndElement();
                        xWriter.WriteElementString("AuthenticationType", "");
                        xWriter.WriteFullEndElement();
                    }
                    else
                    {
                        xWriter.WriteStartElement("LDAP");
                        xWriter.WriteElementString("serverAddress", ldapSettings.serveraddress.Trim());
                        xWriter.WriteElementString("loginName", ldapSettings.login.Trim());
                        xWriter.WriteElementString("loginPassword", ldapSettings.password.Trim());
                        xWriter.WriteElementString("portNo", ldapSettings.port.ToString());
                        xWriter.WriteElementString("connectionTimeout", ldapSettings.timeout.ToString());
                        xWriter.WriteElementString("loginkey", ldapSettings.LoginKey.Trim());
                        xWriter.WriteElementString("synctime", ldapSettings.SyncTime.ToString("hh:mm tt"));
                        xWriter.WriteElementString("LDAPPrefix", ldapSettings.domainPrefix.Trim());
                        xWriter.WriteElementString("searchfilter", ldapSettings.SearchFilter.Trim());
                        xWriter.WriteStartElement("scheduler");
                        xWriter.WriteElementString("Time", ldapSettings.scheduleTime.ToString("hh:mm tt"));
                        xWriter.WriteElementString("Days", ldapSettings.scheduleDays);
                        xWriter.WriteEndElement();
                        xWriter.WriteElementString("AuthenticationType", ldapSettings.AuthType.ToString());
                        xWriter.WriteEndElement();
                    }
                    //ZD 101443 End

                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.Flush();
                    obj.outXml = OrgOUTXML.ToString();
                    return true;
                }
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }

        //Image Project
        private string GetImage(int imageid)
        {
            vrmImage imObj = null;
            string imgDt = " ";
            try
            {
                imObj = m_IImageDAO.GetById(imageid);
                if (imObj != null)
                    imgDt = vrmImg.ConvertByteArrToBase64(imObj.AttributeImage); //FB 2136

                return imgDt;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException in GetRoomProfile", e);
                return "";
            }
        }
        #endregion

        #region Set Organization Options
        /// <summary>
        /// Set Organization Options By OrgID
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetOrgOptions(ref vrmDataObject obj)
        {
            string orgId = "";
            string userId = "11";
            int filterTelepresence = 0;//FB 2170
            int enabpassrule = 0, DedicatedVideo= -1;//FB 2339 FB 2334
			int rmxLO = 1, mgcLO =1, ctsLO=1, ctmsLO = 1; //FB 2335
            ns_SqlHelper.SqlHelper sqlCon = null; //FB 2588
            string strSQL = "", timeFormat = "", FileWhiteList = "";//ZD 100263
            int strExec = -1, EnableFileWhiteList = 0;
            int CodianLO = 1; //ZD 101869
            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//SetOrgOptions/organizationID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);

                node = xd.SelectSingleNode("//SetOrgOptions/userID");
                if (node != null)
                    userId = node.InnerText;

                int.TryParse(userId, out loginUser);

                sysTechData sysTechDta = m_ISysTechDAO.GetTechByOrgId(organizationID);
                if (sysTechDta == null)
                {
                    myvrmEx = new myVRMException(442);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //FB 2440
                int mcuSetup = 0, roomSetup = 0;
                int mcuTeardown = 0, roomTeardown = 0;
                XmlNode ndeBufferPriority = null;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableBufferZone");
                ndeBufferPriority = xd.SelectSingleNode("//SetOrgOptions/MCUBufferPriority");
                if (node != null && ndeBufferPriority != null)
                {
                    if (node.InnerText.Trim() == "1" && ndeBufferPriority.InnerText.Trim() == "0")
                    {
                        node = xd.SelectSingleNode("//SetOrgOptions/McuSetupTime");
                        if (node != null)
                            int.TryParse(node.InnerText.Trim(), out mcuSetup);
                        node = xd.SelectSingleNode("//SetOrgOptions/SetupTime");
                        if (node != null)
                            int.TryParse(node.InnerText.Trim(), out roomSetup);

                        node = xd.SelectSingleNode("//SetOrgOptions/MCUTeardonwnTime");
                        if (node != null)
                            int.TryParse(node.InnerText.Trim(), out mcuTeardown);
                        node = xd.SelectSingleNode("//SetOrgOptions/TearDownTime");
                        if (node != null)
                            int.TryParse(node.InnerText.Trim(), out roomTeardown);
                        if (mcuSetup > 0)
                        {
                            if (mcuSetup > roomSetup)
                            {
                                myvrmEx = new myVRMException(613);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        if (mcuTeardown < 0)
                        {
                            if ((roomTeardown+mcuTeardown) < 0)
                            {
                                myvrmEx = new myVRMException(614);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                }
                //FB 2440
                
                node = xd.SelectSingleNode("//ContactDetails/Name");
                sysTechDta.name = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerXml.Trim()); //ZD 104391
                
                node = xd.SelectSingleNode("//ContactDetails/Email");
                sysTechDta.email = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerXml.Trim()); //ZD 104391
                
                node = xd.SelectSingleNode("//ContactDetails/Phone");
                sysTechDta.phone = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerXml.Trim());//ZD 104391
                
                node = xd.SelectSingleNode("//ContactDetails/AdditionInfo");
                sysTechDta.info = m_UtilFactory.ReplaceInXMLSpecialCharacters(node.InnerXml.Trim());//ZD 104391

                OrgData orgdata = m_IOrgSettingDAO.GetByOrgId(organizationID);
                if (orgdata == null)
                {
                    myvrmEx = new myVRMException(443);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//SetOrgOptions/TimezoneSystemID");
                string tzsysid = node.InnerXml.Trim();
                int tzsysId = 0; //US Timezone as default
                int.TryParse(tzsysid, out tzsysId);
                orgdata.tzsystemid = tzsysId;

                node = xd.SelectSingleNode("//SetOrgOptions/AutoAcceptModifiedConference");
                string autoModConf = node.InnerXml.Trim();
                int autoacc = 0;
                int.TryParse(autoModConf, out autoacc);
                orgdata.AutoAcceptModConf = autoacc;

                node = xd.SelectSingleNode("//SetOrgOptions/EnableRecurringConference");
                string recEnable = node.InnerXml.Trim();
                int recEna = 0;
                int.TryParse(recEnable, out recEna);
                orgdata.recurEnabled = recEna;

                node = xd.SelectSingleNode("//SetOrgOptions/EnableDynamicInvite");
                string dynamicInv = node.InnerXml.Trim();
                int dynInv = 0;
                int.TryParse(dynamicInv, out dynInv);
                orgdata.dynamicinviteenabled = dynInv;

                node = xd.SelectSingleNode("//SetOrgOptions/EnableP2PConference");
                string conn2 = node.InnerXml.Trim();
                int connect2 = 1;
                int.TryParse(conn2, out connect2);
                orgdata.Connect2 = connect2;

                node = xd.SelectSingleNode("//SetOrgOptions/EnableRealtimeDisplay");
                string realTime = node.InnerXml.Trim();
                int realSt = 0;
                int.TryParse(realTime, out realSt);
                orgdata.RealtimeStatus = realSt;

                node = xd.SelectSingleNode("//SetOrgOptions/EnableDialout");
                string dialout = node.InnerXml.Trim();
                int diaout = 1;
                int.TryParse(dialout, out diaout);
                orgdata.DialOut = diaout;

                node = xd.SelectSingleNode("//SetOrgOptions/DefaultConferencesAsPublic");
                string defaultPublic = node.InnerXml.Trim();
                int defPub = 0;
                int.TryParse(defaultPublic, out defPub);
                orgdata.DefaultToPublic = defPub;

                node = xd.SelectSingleNode("//SetOrgOptions/DefaultConferenceType");
                string defConfType = node.InnerXml.Trim();
                int defCType = 7;
                int.TryParse(defConfType, out defCType);
                orgdata.DefaultConferenceType = defCType;

                //FB 2724
                node = xd.SelectSingleNode("//SetOrgOptions/RFIDTagValue");
                string RFIDTagValue = node.InnerXml.Trim();
                int RFID = 1;
                int.TryParse(RFIDTagValue, out RFID);
                orgdata.RFIDTagValue = RFID;

                int Timeout = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/iControlTimeout");
                int.TryParse(node.InnerXml.Trim(), out Timeout);
                orgdata.iControlTimeout = Timeout;
                //ZD 103398 Start
                int RefreshTimer = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/VideoRefreshTimer");
                if (node != null)
                	int.TryParse(node.InnerXml.Trim(), out RefreshTimer);
                orgdata.VideoRefreshTimer = RefreshTimer;
                //ZD 103398 End
                node = xd.SelectSingleNode("//SetOrgOptions/EnableRoomConference");
                string enableRoomConf = node.InnerXml.Trim();
                int enabRoom = 1;
                int.TryParse(enableRoomConf, out enabRoom);
                orgdata.EnableRoomConference = enabRoom;

                node = xd.SelectSingleNode("//SetOrgOptions/EnableAudioVideoConference");
                string enableAVConf = node.InnerXml.Trim();
                int enabAV = 1;
                int.TryParse(enableAVConf, out enabAV);
                orgdata.EnableAudioVideoConference = enabAV;

                node = xd.SelectSingleNode("//SetOrgOptions/EnableAudioOnlyConference");
                string enableAudio = node.InnerXml.Trim();
                int enabAudio = 1;
                int.TryParse(enableAudio, out enabAudio);
                orgdata.EnableAudioOnlyConference = enabAudio;

                node = xd.SelectSingleNode("//SetOrgOptions/EnableNumericID"); //FB 2870
                int EnableNumericID = 0;
                int.TryParse(node.InnerXml.Trim(), out EnableNumericID);
                orgdata.EnableNumericID = EnableNumericID;

                //ZD 100719 - Start
                node = xd.SelectSingleNode("//SetOrgOptions/EnableHotdeskingConference");
                string enableHotdeksingConf = node.InnerXml.Trim();
                int enabHotdesking = 1;
                int.TryParse(enableHotdeksingConf, out enabHotdesking);
                orgdata.EnableHotdeskingConference = enabHotdesking;
                //ZD 100719 - End

                node = xd.SelectSingleNode("//SetOrgOptions/DefaultCalendarToOfficeHours");
                string officeHours = node.InnerXml.Trim();
                int defCalendar = 1;
                int.TryParse(officeHours, out defCalendar);
                orgdata.DefaultCalendarToOfficeHours = defCalendar;
				//FB 1782 start
                node = xd.SelectSingleNode("//SetOrgOptions/sendApprovalIcal");
                string sendAppIcal = node.InnerXml.Trim();
                int sendAppIcalint = 1;
                int.TryParse(sendAppIcal, out sendAppIcalint);
                orgdata.SendApprovalIcal = sendAppIcalint;
				//FB 1782 end

                //FB 1782 start
                node = xd.SelectSingleNode("//SetOrgOptions/sendIcal");
                string sendIcal = node.InnerXml.Trim();
                int sendIcalint = 1;
                int.TryParse(sendIcal, out sendIcalint);
                orgdata.SendIcal = sendIcalint;
                //FB 1782 end

                node = xd.SelectSingleNode("//SetOrgOptions/isVIP");// FB 1864
                string isvip = node.InnerXml.Trim();
                int isVIP = 1;
                int.TryParse(isvip, out isVIP);
                orgdata.isVIP = isVIP;

                node = xd.SelectSingleNode("//SetOrgOptions/isUniquePassword");// FB 1864
                string isuniquepassword = node.InnerXml.Trim();
                int isUniquePassword = 0;
                int.TryParse(isuniquepassword, out isUniquePassword);
                orgdata.isUniquePassword = isUniquePassword;

                node = xd.SelectSingleNode("//SetOrgOptions/isAssignedMCU");// FB 1901
                string isassignedMCU = node.InnerXml.Trim();
                int isAssignedMCU = 0;
                int.TryParse(isassignedMCU, out isAssignedMCU);
                orgdata.isAssignedMCU = isAssignedMCU;
				//FB 1830 - Translation

                node = xd.SelectSingleNode("//SetOrgOptions/isSpecialRecur");// FB 2052
                string isspecialRecur = node.InnerXml.Trim();
                int isSpecialRecur = 0;
                int.TryParse(isspecialRecur, out isSpecialRecur);
                orgdata.SpecialRecur = isSpecialRecur;

                node = xd.SelectSingleNode("//SetOrgOptions/isMultiLingual");
                string ismultilingual = node.InnerXml.Trim();
                int isMultiLingual = 0;
                int.TryParse(ismultilingual, out isMultiLingual);
                orgdata.isMultiLingual = isMultiLingual;
 				node = xd.SelectSingleNode("//SetOrgOptions/PIMNotifications");// FB 2141
                if (node != null)
                {
                    string ispimnotifications = node.InnerXml.Trim();
                    int isPIMNotifications = 0;
                    int.TryParse(ispimnotifications, out isPIMNotifications);
                    orgdata.PluginConfirmations = isPIMNotifications;
                }

                node = xd.SelectSingleNode("//SetOrgOptions/ExternalAttachments");// FB 2154
                if (node != null)
                {
                    string sendexternalattachmnts = node.InnerXml.Trim();
                    int sendExternalAttachmnts = 0;
                    int.TryParse(sendexternalattachmnts, out sendExternalAttachmnts);
                    orgdata.SendAttachmentsExternal = sendExternalAttachmnts;
                }

                node = xd.SelectSingleNode("//SetOrgOptions/filterTelepresence");// FB 2170
                if (node != null)
                    int.TryParse(node.InnerText, out filterTelepresence);

                orgdata.TelepresenceFilter = filterTelepresence;

                int enabRoomServ = 1;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableRoomServiceType");//FB 2219 
                if (node != null)
                    int.TryParse(node.InnerText, out enabRoomServ);
                orgdata.EnableRoomServiceType = enabRoomServ;

                int enabPIMServ = 1;
                node = xd.SelectSingleNode("//SetOrgOptions/EnablePIMServiceType");//FB 2038
                if (node != null)
                    int.TryParse(node.InnerText, out enabPIMServ);
                orgdata.EnablePIMServiceType = enabPIMServ;
                //FB 2036 - Starts
				int enabImmConf = 1;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableImmConf");
                if (node != null)
                    int.TryParse(node.InnerText, out enabImmConf);
                orgdata.EnableImmediateConference = enabImmConf;
                //FB 2036 - End

                //FB 2023 - Starts
                int enabAudioBridge = 1;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableAudioBridges");
                if (node != null)
                    int.TryParse(node.InnerText, out enabAudioBridge);
                orgdata.EnableAudioBridges = enabAudioBridge;
                //FB 2023 - End
                //FB 2359 Start
                int enabConfPassword = 1;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableConferencePassword");
                if (node != null)
                    int.TryParse(node.InnerText, out enabConfPassword);
                orgdata.EnableConfPassword = enabConfPassword;

                int enabPublicConf = 1;
                node = xd.SelectSingleNode("//SetOrgOptions/EnablePublicConference");
                if (node != null)
                    int.TryParse(node.InnerText, out enabPublicConf);
                orgdata.EnablePublicConf = enabPublicConf;

                int enabRoomParam = 1;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableRoomParam");
                if (node != null)
                    int.TryParse(node.InnerText, out enabRoomParam);
                orgdata.EnableRoomParam = enabRoomParam;
                //FB 2359 End
                node = xd.SelectSingleNode("//SetOrgOptions/RoomTreeExpandLevel");
                orgdata.RoomTreeExpandLevel = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrgOptions/EnableBufferZone");
                string enableBZ = node.InnerText.Trim();
                orgdata.EnableBufferZone = "1";
                if(enableBZ != "")
                    orgdata.EnableBufferZone = enableBZ;

                node = xd.SelectSingleNode("//SystemAvailableTime/IsOpen24Hours");
                string open24 = node.InnerText.Trim();
                int openHrs = 1;
                int.TryParse(open24, out openHrs);
                orgdata.Open24hrs = openHrs;

                node = xd.SelectSingleNode("//SystemAvailableTime/StartTime");
                string systemStTime = DateTime.Now.ToShortDateString() + " 12:00:00 AM";
                if (node.InnerText.Trim() != "")
                    systemStTime = node.InnerText.Trim();

                DateTime sysSttime = DateTime.Now;
                DateTime.TryParse(systemStTime, out sysSttime);
                orgdata.SystemStartTime = sysSttime;

                node = xd.SelectSingleNode("//SystemAvailableTime/EndTime");
                string sysEndTime = DateTime.Now.ToShortDateString() + " 11:59:00 PM";
                if (node.InnerText.Trim() != "")
                    sysEndTime = node.InnerText.Trim();

                DateTime sysendtime = DateTime.Now;
                DateTime.TryParse(sysEndTime, out sysendtime);
                orgdata.SystemEndTime = sysendtime;
                //FB-1642-Audio Add On Starts
                node = xd.SelectSingleNode("//SetOrgOptions/ConferenceCode");
                string ConferenceCode = node.InnerText.Trim();
                int _ConferenceCode = 0;
                int.TryParse(ConferenceCode, out _ConferenceCode);
                orgdata.ConferenceCode = _ConferenceCode;

                node = xd.SelectSingleNode("//SetOrgOptions/LeaderPin");
                string LeaderPin = node.InnerText.Trim();
                int _LeaderPin = 0;
                int.TryParse(LeaderPin, out _LeaderPin);
                orgdata.LeaderPin = _LeaderPin;

                node = xd.SelectSingleNode("//SetOrgOptions/AdvAvParams");
                string AdvAvParams = node.InnerText.Trim();
                int _AdvAvParams = 0;
                int.TryParse(AdvAvParams, out _AdvAvParams);
                orgdata.AdvAvParams = _AdvAvParams;

                node = xd.SelectSingleNode("//SetOrgOptions/AudioParams");
                string AudioParams = node.InnerText.Trim();
                int _AudioParams = 0;
                int.TryParse(AudioParams, out _AudioParams);
                orgdata.AudioParams = _AudioParams;

                //FB-1642-Audio Add On Ends

                //FB 1786
                node = xd.SelectSingleNode("//SetOrgOptions/iCalReqEmailId");
                string iCalReqEmail = node.InnerXml.Trim();
                orgdata.IcalReqEmailID = iCalReqEmail;
                
                //FB 2610
                node = xd.SelectSingleNode("//SetOrgOptions/showBridgeExt");
                int ShowBrdgeExtn = 0;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out ShowBrdgeExtn);
                orgdata.IsBridgeExtNo = ShowBrdgeExtn;

                node = xd.SelectSingleNode("//SystemAvailableTime/DaysClosed");
                orgdata.Offdays = node.InnerXml.Trim();
                orgdata.LastModifiedUser = loginUser;

                node = xd.SelectSingleNode("//SetOrgOptions/ReminderMask");// FB 1926
                string remindermask = node.InnerXml.Trim();
                int ReminderMask = 0;
                int.TryParse(remindermask, out ReminderMask);
                orgdata.ReminderMask = ReminderMask;

                //FB 2269 - Starts
                node = xd.SelectSingleNode("//SetOrgOptions/isDeptUser");
                int isDeptUser = 1;
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out isDeptUser);
                orgdata.isDeptUser = isDeptUser;
                //FB 2269 - End

                //FB 2136 start
                /*node = xd.SelectSingleNode("//SetOrgOptions/EnableSecurityBadge");
                int EnableSecurity = 1;
                if (node != null)
                 int.TryParse(node.InnerText, out EnableSecurity);
                orgdata.EnableSecurityBadge = EnableSecurity;

                node = xd.SelectSingleNode("//SetOrgOptions/SecurityBadgeType");
                int SecurityBadgeType = 1;
                if (node != null)
                  int.TryParse(node.InnerText, out SecurityBadgeType);
                orgdata.SecurityBadgeType = SecurityBadgeType;

                string SecurityDeskEmailId = "";
                node = xd.SelectSingleNode("//SetOrgOptions/SecurityDeskEmailId");
                if (node != null)
                    SecurityDeskEmailId = node.InnerText.Trim();
                orgdata.SecurityDeskEmailId = SecurityDeskEmailId;
                 * */
                //FB 2136 end

                //FB 2339 - Starts
                node = xd.SelectSingleNode("//SetOrgOptions/EnablePasswordRule");
                    int.TryParse(node.InnerText, out enabpassrule);
                orgdata.EnablePasswordRule = enabpassrule;
                //FB 2339 - End

 				//FB 2335 start
                node = xd.SelectSingleNode("//SetOrgOptions/defPolycomRMXLO");                
                if (node != null)
                    int.TryParse(node.InnerText, out rmxLO);
                //ZD 101869
                if (rmxLO <= 0)
                    rmxLO = 1;
                orgdata.DefPolycomRMXLO = rmxLO;

                node = xd.SelectSingleNode("//SetOrgOptions/defPolycomMGCLO");                
                if (node != null)
                    int.TryParse(node.InnerText, out mgcLO);
                //ZD 101869
                if (mgcLO <= 0)
                    mgcLO = 1;
                orgdata.DefPolycomMGCLO = mgcLO;

               
                node = xd.SelectSingleNode("//SetOrgOptions/defCiscoTPLO");
                if (node != null)
                    int.TryParse(node.InnerText, out ctsLO);
                orgdata.DefCiscoTPLO = ctsLO;

               
                node = xd.SelectSingleNode("//SetOrgOptions/defCTMSLO");
                if (node != null)
                    int.TryParse(node.InnerText, out ctmsLO);
                orgdata.DefCTMSLO = ctmsLO;

                //FB 2335 end
                // FB 2334 Start
                node = xd.SelectSingleNode("//SetOrgOptions/DedicatedVideo");
                if (node != null)
                    int.TryParse(node.InnerText, out DedicatedVideo);

                orgdata.DedicatedVideo = DedicatedVideo;
                // FB 2334 End
                //VMR Start
                int EnableVMR = 1;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableVMR");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableVMR);

                orgdata.EnableVMR = EnableVMR;
                //VMR End

                //ZD 102916 Start
                int assignParty = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/AssignParticipantsToRoom");
                if (node != null)
                    int.TryParse(node.InnerText, out assignParty);
                orgdata.AssignPartyToRoom = assignParty;
                //ZD 102916 End

                //ZD 102916 Start
                int EnableAudbridgefreebusy = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableAudbridgefreebusy");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableAudbridgefreebusy);
                orgdata.EnableAudbridgefreebusy = EnableAudbridgefreebusy;
                //ZD 102916 End

                //FB 2472 start
                int MCUAlert = 1;
                node = xd.SelectSingleNode("//SetOrgOptions/MCUAlert");
                if (node != null)
                    int.TryParse(node.InnerText, out MCUAlert);
                orgdata.MCUAlert = MCUAlert;
                //FB 2472 end
                
                // FB 2348 Start
                int EnableSurvey = 0, SurveyOption = 0, TimeDuration = 0;
                string SurveyURL = "";
                node = xd.SelectSingleNode("//SetOrgOptions/EnableSurvey");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableSurvey);
                orgdata.EnableSurvey = EnableSurvey;

                node = xd.SelectSingleNode("//SetOrgOptions/SurveyOption");
                if (node != null)
                    int.TryParse(node.InnerText, out SurveyOption);
                orgdata.SurveyOption = SurveyOption;

                node = xd.SelectSingleNode("//SetOrgOptions/SurveyURL");
                if (node != null)
                    SurveyURL = node.InnerText.Trim();
                orgdata.SurveyURL = SurveyURL;

                node = xd.SelectSingleNode("//SetOrgOptions/TimeDuration");
                if (node != null)
                    int.TryParse(node.InnerText, out TimeDuration);
                orgdata.TimeDuration = TimeDuration;
                // FB 2348 End
				// FB 2398 Start
                int SetupTime = 0, TearDownTime = 0;   
                node = xd.SelectSingleNode("//SetOrgOptions/SetupTime");
                if (node != null)
                    int.TryParse(node.InnerText, out SetupTime);
                orgdata.SetupTime = SetupTime;

                node = xd.SelectSingleNode("//SetOrgOptions/TearDownTime");
                if (node != null)
                    int.TryParse(node.InnerText, out TearDownTime);
                orgdata.TearDownTime = TearDownTime;
				// FB 2398 end
				int EPDetails = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableEPDetails"); //FB 2401
                if (node != null)
                    int.TryParse(node.InnerText, out EPDetails);
                orgdata.EnableEPDetails = EPDetails;

                //FB 2419
                int enableAcceptDecline = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableAcceptDecline");
                if (node != null)
                    int.TryParse(node.InnerText, out enableAcceptDecline);
                orgdata.EnableAcceptDecline = enableAcceptDecline;

                //FB 2429 - Starts
                int defaultLineRate = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/DefaultLineRate");
                if (node != null)
                    int.TryParse(node.InnerText, out defaultLineRate);
                orgdata.DefLinerate = defaultLineRate;
                //FB 2429 - End
                //ZD 100263 Start
                node = xd.SelectSingleNode("//SetOrgOptions/EnableFileWhiteList");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableFileWhiteList);
                orgdata.EnableFileWhiteList = EnableFileWhiteList;

                node = xd.SelectSingleNode("//SetOrgOptions/FileWhiteList");
                if (node != null)
                    FileWhiteList = node.InnerText.Trim();
                orgdata.FileWhiteList = FileWhiteList;
                //ZD 100263 End
                //FB 2430 start
                int EnableSmartP2P = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableSmartP2P");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableSmartP2P);

                orgdata.EnableSmartP2P = EnableSmartP2P;
                //FB 2430 end
                //FB 2550 Starts
                int MaxPublicVMRParty = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/MaxPublicVMRParty");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out MaxPublicVMRParty);

                orgdata.MaxPublicVMRParty = MaxPublicVMRParty;
                //FB 2550 Ends
                // FB 2440 Start
                int iMCUSetupTime = 0, iMCUTearDownTime = 0, iMCUBufferPriority = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/McuSetupTime");
                if (node != null)
                    int.TryParse(node.InnerText, out iMCUSetupTime);
                orgdata.McuSetupTime = iMCUSetupTime;

                node = xd.SelectSingleNode("//SetOrgOptions/MCUTeardonwnTime");
                if (node != null)
                    int.TryParse(node.InnerText, out iMCUTearDownTime);
                orgdata.MCUTeardonwnTime = iMCUTearDownTime;

                node = xd.SelectSingleNode("//SetOrgOptions/MCUBufferPriority");
                if (node != null)
                    int.TryParse(node.InnerText, out iMCUBufferPriority);
                orgdata.MCUBufferPriority = iMCUBufferPriority;
                // FB 2440 end
                
                //FB 2998
                int MCUSetupDisplay = 0, MCUTearDisplay = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/MCUSetupDisplay");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out MCUSetupDisplay);
                orgdata.MCUSetupDisplay = MCUSetupDisplay;

                node = xd.SelectSingleNode("//SetOrgOptions/MCUTearDisplay");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out MCUTearDisplay);
                orgdata.MCUTearDisplay = MCUTearDisplay;

                //FB 2426 start

                string DefTopTier = "", DefMiddleTier = "";
                node = xd.SelectSingleNode("//SetOrgOptions/OnflyTier/DefTopTier");
                if (node != null)
                    DefTopTier = node.InnerText.Trim();
                orgdata.TopTier = DefTopTier;

                node = xd.SelectSingleNode("//SetOrgOptions/OnflyTier/DefMiddleTier");
                if (node != null)
                    DefMiddleTier = node.InnerText.Trim();
                orgdata.MiddleTier = DefMiddleTier;

                int TopTierID = 0, MiddleTierID = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/OnflyTier/OnflyTopTierID");
                if (node != null)
                    int.TryParse(node.InnerText, out TopTierID);
                orgdata.OnflyTopTierID = TopTierID;

                node = xd.SelectSingleNode("//SetOrgOptions/OnflyTier/OnflyMiddleTierID");
                if (node != null)
                    int.TryParse(node.InnerText, out MiddleTierID);
                orgdata.OnflyMiddleTierID = MiddleTierID;
                //FB 2426 End

                //ZD 100068 Starts
                int TopTierID1 = 0, MiddleTierID1 = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/VMRTier/VMRTopTierID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out TopTierID1);
                orgdata.VMRTopTierID = TopTierID1;

                node = xd.SelectSingleNode("//SetOrgOptions/VMRTier/VMRMiddleTierID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out MiddleTierID1);
                orgdata.VMRMiddleTierID = MiddleTierID1;
                //ZD 100068 Ends
                
                //FB 2469 Starts
                int EnableConfTZinLoc = 0, SendConfirmationEmail = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableConfTZinLoc");
                if(node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableConfTZinLoc);
                orgdata.EnableConfTZinLoc = EnableConfTZinLoc;

                node = xd.SelectSingleNode("//SetOrgOptions/SendConfirmationEmail");
                if(node != null)
                    int.TryParse(node.InnerText.Trim(), out SendConfirmationEmail);
                orgdata.SendConfirmationEmail = SendConfirmationEmail;
                //FB 2469 End
                //FB 2501 - Start
                int DefaultConfDuration = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/DefaultConfDuration");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out DefaultConfDuration);
                orgdata.DefaultConfDuration = DefaultConfDuration;
                //FB 2501 - End

                //FB 2571 Start
                int EnableFECC = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableFECC");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableFECC);
                
                orgdata.EnableFECC = EnableFECC;

                int DefaultFECC = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/DefaultFECC");
                if (node != null)
                    int.TryParse(node.InnerText, out DefaultFECC);
                
                orgdata.DefaultFECC = DefaultFECC;
                //FB 2571 End

                //DD Feature
                //FB 2598 Starts 
                int EnableCallmonitor = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableCallmonitor");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableCallmonitor);

                orgdata.EnableCallmonitor = EnableCallmonitor;

                int EnableEM7 = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableEM7");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableEM7);

                orgdata.EnableEM7 = EnableEM7;

                int EnableCDR = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableCDR");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableCDR);

                orgdata.EnableCDR = EnableCDR;

                //FB 2598 Ends
                //ZD 100151
                int ShowCusAttInCalendar = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/ShowCusAttInCalendar");
                if (node != null)
                    int.TryParse(node.InnerText, out ShowCusAttInCalendar);
                orgdata.ShowCusAttInCalendar = ShowCusAttInCalendar;

                // FB 2636 Starts
                int EnableDialPlan = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableDialPlan");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableDialPlan);

                orgdata.EnableDialPlan = EnableDialPlan;
                // FB 2636 Ends

                //FB 2588 Start
                int EnableZulu = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableZulu");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableZulu);
                
                orgdata.EnableZulu = EnableZulu;
                if(EnableZulu == 1)
                    orgdata.tzsystemid = 0; //Worl Wide TZ
                //FB 2588 Ends

                //FB 2609 Starts
                int MeetandGreetBuffer = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/MeetandGreetBuffer");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out MeetandGreetBuffer);

                orgdata.MeetandGreetBuffer = MeetandGreetBuffer;
                //FB 2609 Ends

                //FB 2632 Starts
                int EnableCongSuppoet=0,AVSupport = 0, MeetGrt = 0, CongMonitr = 0, DedicatedVNOC = 0;

                node = xd.SelectSingleNode("//SetOrgOptions/EnableCongSupport");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableCongSuppoet);

                node = xd.SelectSingleNode("//SetOrgOptions/MeetandGreetinEmail");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out MeetGrt);

                node = xd.SelectSingleNode("//SetOrgOptions/OnSiteAVSupportinEmail");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out AVSupport);

                node = xd.SelectSingleNode("//SetOrgOptions/ConciergeMonitoringinEmail");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out CongMonitr);

                node = xd.SelectSingleNode("//SetOrgOptions/DedicatedVNOCOperatorinEmail");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out DedicatedVNOC);

                orgdata.EnableCncigSupport = EnableCongSuppoet;
                orgdata.MeetandGreetinEmail = MeetGrt;
                orgdata.AVOnsiteinEmail = AVSupport;
                orgdata.CncigMoniteringinEmail = CongMonitr;
                orgdata.VNOCinEmail = DedicatedVNOC;

                //FB 2632 Ends

                //FB 2595 Starts
                int SecureSwitch = 0, NetworkSwitching = 1, NetworkCallLaunch = 3, SecureLaunch =0;
                int Responsetimeout = 0;//FB 2993
                node = xd.SelectSingleNode("//SetOrgOptions/SecureSwitch");
                if (node != null)
                    int.TryParse(node.InnerText, out SecureSwitch);

                node = xd.SelectSingleNode("//SetOrgOptions/HardwareAdminEmail");
                if (node != null)
                    orgdata.HardwareAdminEmail = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SetOrgOptions/NetworkSwitching");
                if (node != null)
                    int.TryParse(node.InnerText, out NetworkSwitching);

                node = xd.SelectSingleNode("//SetOrgOptions/NetworkCallLaunch");
                if (node != null)
                    int.TryParse(node.InnerText, out NetworkCallLaunch);

                node = xd.SelectSingleNode("//SetOrgOptions/SecureLaunchBuffer");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out SecureLaunch);
                //FB 2993 start
                node = xd.SelectSingleNode("//SetOrgOptions/ResponseTimeout");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out Responsetimeout);
                //FB 2993 End
                orgdata.SecureSwitch = SecureSwitch;

                if (SecureSwitch < 1)
                {
                    orgdata.NetworkSwitching = 1;
                    orgdata.NetworkCallLaunch = 3;
                    orgdata.HardwareAdminEmail = "";
                    orgdata.SecureLaunchBuffer = 0;
                    orgdata.ResponseTimeout = 0;//FB 2993

                }
                else
                {
                    orgdata.NetworkSwitching = NetworkSwitching;
                    orgdata.NetworkCallLaunch = NetworkCallLaunch;
                    orgdata.SecureLaunchBuffer = SecureLaunch;
                    orgdata.ResponseTimeout = Responsetimeout * 60;//FB 2993

                }
                //FB 2595 Ends

                //FB 2631
                int roomAdminDetails = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableRoomAdminDetails"); //FB 2401
                if (node != null)
                    int.TryParse(node.InnerText, out roomAdminDetails);
                orgdata.EnableRoomAdminDetails = roomAdminDetails;

				 //FB 2637 Starts
                string confTier2Alert = "";
                node = xd.SelectSingleNode("//SetOrgOptions/AlertforTier1");
                if (node != null)
                    confTier2Alert = node.InnerText.Trim();
                orgdata.AlertforTier1 = confTier2Alert;
                //FB 2637 Ends
                // FB 2641 starts
                int Enablelinerate = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableLinerate");
                if (node != null)
                    int.TryParse(node.InnerText, out Enablelinerate );
                orgdata.EnableLinerate = Enablelinerate;

                int EnablestartMode = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableStartMode");
                if (node != null)
                    int.TryParse(node.InnerText, out EnablestartMode);
                orgdata.EnableStartMode = EnablestartMode;
                // FB 2641 End

                //FB 2839 Start
                int EnableProfileSelection = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableProfileSelection");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableProfileSelection);

                orgdata.EnableProfileSelection = EnableProfileSelection;
                //FB 2839 End

				//FB 2670 START
                int Concierge = 0, OnsiteAV = 0, MeetGreet = 0, dedicateVNOC = 0;

                node = xd.SelectSingleNode("//SetOrgOptions/EnableOnsiteAV");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out OnsiteAV);

                node = xd.SelectSingleNode("//SetOrgOptions/EnableMeetandGreet");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out MeetGreet);

                node = xd.SelectSingleNode("//SetOrgOptions/EnableConciergeMonitoring");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out Concierge);

                node = xd.SelectSingleNode("//SetOrgOptions/EnableDedicatedVNOC");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out dedicateVNOC);

                orgdata.EnableOnsiteAV = OnsiteAV;
                orgdata.EnableMeetandGreet = MeetGreet;
                orgdata.EnableConciergeMonitoring = Concierge;
                orgdata.EnableDedicatedVNOC = dedicateVNOC;
                //FB 2670 END
                //ZD 100221 Starts
                string WEbExURL = "", WebEXSiteID = "", WebExPartnerID = "";

                node = xd.SelectSingleNode("//SetOrgOptions/WebExURL");
                if (node != null)
                    WEbExURL = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SetOrgOptions/WebExSiteID");
                if (node != null)
                    WebEXSiteID = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SetOrgOptions/WebExPartnerID");
                if (node != null)
                    WebExPartnerID = node.InnerText.Trim();

                orgdata.WebExURL = WEbExURL;
                orgdata.WebExSiteID = WebEXSiteID;
                orgdata.WebExPartnerID = WebExPartnerID;
                //ZD 100221 Ends
                //ZD 100935 Starts
                int EnableWebEX = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableWebExIntg");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableWebEX);
                if (orgdata.WebexUserLimit <= 0)
                    EnableWebEX = 0;
                orgdata.EnableWebExIntg = EnableWebEX;
                //ZD 100935 Ends
                //FB 2817
                int SingleRoomConfEmails = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableSingleRoomConfEmails"); 
                if (node != null)
                    int.TryParse(node.InnerText, out SingleRoomConfEmails);
                orgdata.SingleRoomConfMail = SingleRoomConfEmails;

				//ZD 100164 START
                int EnableAdvancedUserOption = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableAdvancedUserOption");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableAdvancedUserOption);
                orgdata.EnableAdvancedUserOption = EnableAdvancedUserOption;
                //ZD 100164 END

                //ZD 100707 START

                int ShowHideVMR = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/ShowHideVMR");
                if (node != null)
                    int.TryParse(node.InnerText, out ShowHideVMR);
                orgdata.ShowHideVMR = ShowHideVMR;

                int EnablePersonaVMR = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnablePersonaVMR");
                if (node != null)
                    int.TryParse(node.InnerText, out EnablePersonaVMR);
                orgdata.EnablePersonaVMR = EnablePersonaVMR;

                int EnableRoomVMR = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableRoomVMR");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableRoomVMR);
                orgdata.EnableRoomVMR = EnableRoomVMR;

                int EnableExternalVMR = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableExternalVMR");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableExternalVMR);
                orgdata.EnableExternalVMR = EnableExternalVMR;
				//ZD 100707 END
                //ZD 100704 START
                int EnableExpressConfType = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableExpressConfType");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableExpressConfType);
                orgdata.EnableExpressConfType = EnableExpressConfType;
                //ZD 100704 END
                // ZD 100834 Start
                int EnableDetailedExpressForm = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableDetailedExpressForm");
                if (node != null)
                   int.TryParse(node.InnerText, out EnableDetailedExpressForm);
                orgdata.EnableDetailedExpressForm = EnableDetailedExpressForm;
                //ZD 100834 End

                //ZD 100707 END
                //ZD 100899 Start
                int ScheduleLimit = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/ScheduleLimit");
                if (node != null)
                    int.TryParse(node.InnerText, out ScheduleLimit);
                orgdata.ScheduleLimit = ScheduleLimit;
                //ZD 100899 End

				//ZD 100781 Starts
                int PwdExp = 3, EnablePwdExp = 0;

                node = xd.SelectSingleNode("//SetOrgOptions/EnablePasswordExp");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnablePwdExp);
                orgdata.EnablePasswordExp = EnablePwdExp;

                node = xd.SelectSingleNode("//SetOrgOptions/PasswordExpDays");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out PwdExp);
                if (PwdExp <= 0)
                    PwdExp = 3;
                orgdata.PasswordMonths = PwdExp;
                //ZD 100781 Ends
				
			    //ZD 100963 START
                int EnableRoomCalendarView = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableRoomCalendarView");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableRoomCalendarView);
                orgdata.EnableRoomCalendarView = EnableRoomCalendarView;
                //ZD 100963 END

                //ZD 101019 START
                string VideoSourceURL1 = "", VideoSourceURL2 = "", VideoSourceURL3 = "", VideoSourceURL4 = "";

                node = xd.SelectSingleNode("//SetOrgOptions/VideoSourceURL1");
                if (node != null)
                    VideoSourceURL1 = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SetOrgOptions/VideoSourceURL2");
                if (node != null)
                    VideoSourceURL2 = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SetOrgOptions/VideoSourceURL3");
                if (node != null)
                    VideoSourceURL3 = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SetOrgOptions/VideoSourceURL4");
                if (node != null)
                    VideoSourceURL4 = node.InnerText.Trim();

                orgdata.VideoSourceURL1 = VideoSourceURL1;
                orgdata.VideoSourceURL2 = VideoSourceURL2;
                orgdata.VideoSourceURL3 = VideoSourceURL3;
                orgdata.VideoSourceURL4 = VideoSourceURL4;

                string ScreenPosition1 = "", ScreenPosition2 = "", ScreenPosition3 = "", ScreenPosition4 = "";

                node = xd.SelectSingleNode("//SetOrgOptions/ScreenPosition1");
                if (node != null)
                   ScreenPosition1 = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SetOrgOptions/ScreenPosition2");
                if (node != null)
                    ScreenPosition2 = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SetOrgOptions/ScreenPosition3");
                if (node != null)
                    ScreenPosition3 = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SetOrgOptions/ScreenPosition4");
                if (node != null)
                    ScreenPosition4 = node.InnerText.Trim();

                orgdata.ScreenPosition1 = ScreenPosition1;
                orgdata.ScreenPosition2 = ScreenPosition2;
                orgdata.ScreenPosition3 = ScreenPosition3;
                orgdata.ScreenPosition4 = ScreenPosition4;
                //ZD 101019 END
                //ZD 101120 Starts
                int GLApprovalTime = 0, GLWarningMessage = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableGuestLocWarningMsg");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out GLWarningMessage);

                node = xd.SelectSingleNode("//SetOrgOptions/GuestLocApprovalTime");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out GLApprovalTime);
                orgdata.EnableGuestLocWarningMsg = GLWarningMessage;
                orgdata.GuestLocApprovalTime = GLApprovalTime;
                //ZD 101120 Ends
				//ZD 100522 Starts
                int PWDCharlen = 4, VMRPinChg = 0;

                node = xd.SelectSingleNode("//SetOrgOptions/PasswordCharLength");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out PWDCharlen);

                node = xd.SelectSingleNode("//SetOrgOptions/VMRPINChange");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out VMRPinChg);

                orgdata.VMRPINChange = VMRPinChg;
                orgdata.PasswordCharLength = PWDCharlen;
                //ZD 100522 End
                //ZD 101445 - Start
                int RoomDenial = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/RoomDenialCommt");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out RoomDenial);

                orgdata.RoomDenialCommt = RoomDenial;
                //ZD 101445 - End
                //ZD 100815 Start
                int SmartP2PNotify = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/SmartP2PNotify");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out SmartP2PNotify);
                orgdata.SmartP2PNotify = SmartP2PNotify;
                //ZD 100815 End
                //ZD 101228 Starts
                int AVWOAlertTime = 0, CatWOAlertTime = 0, FacilityWOAlertTime = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/AVWOAlertTime");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out AVWOAlertTime);
                orgdata.AVWOAlertTime = AVWOAlertTime;

                node = xd.SelectSingleNode("//SetOrgOptions/CatWOAlertTime");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out CatWOAlertTime);
                orgdata.CatWOAlertTime = CatWOAlertTime;

                node = xd.SelectSingleNode("//SetOrgOptions/FacilityWOAlertTime");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out FacilityWOAlertTime);
                orgdata.FacilityWOAlertTime = FacilityWOAlertTime;
                //ZD 101228 End
 				//ZD 100513 Starts
                int WebExLaunch = 0, enbaleOBTP = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/WebExLaunch");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out WebExLaunch);
                orgdata.WebExLaunch = WebExLaunch;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableWETConference");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out enbaleOBTP);
                orgdata.EnableWETConference = enbaleOBTP;
                //ZD 100513 Ends

                //ZD 101562 - START
                int TemplateBooking = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableTemplateBooking");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out TemplateBooking);
                orgdata.EnableTemplateBooking = TemplateBooking;
                //ZD 101562 - END

                //ZD 101755 start
                int EnableSetupTimeDisplay = 0, EnableTeardownTimeDisplay = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableSetupTimeDisplay");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableSetupTimeDisplay);
                orgdata.EnableSetupTimeDisplay = EnableSetupTimeDisplay;

                node = xd.SelectSingleNode("//SetOrgOptions/EnableTeardownTimeDisplay");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableTeardownTimeDisplay);
                orgdata.EnableTeardownTimeDisplay = EnableTeardownTimeDisplay;
                //ZD 101755 End
				//ZD 101757
                int actMsgDelivery = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableActMsgDelivery");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out actMsgDelivery);
                orgdata.EnableActMsgDelivery = actMsgDelivery;
				//ZD 101527 - Start
                int RPRMRoomSync = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableRPRMRoomSync");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out RPRMRoomSync);
                orgdata.EnableRPRMRoomSync = RPRMRoomSync;

                int RoomSyncAuto = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/RoomSyncAuto");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out RoomSyncAuto);
                orgdata.RoomSyncAuto = RoomSyncAuto;

                int RoomSyncPollTime = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/RoomSyncPollTime");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out RoomSyncPollTime);
                orgdata.RoomSyncPollTime = RoomSyncPollTime;
                //ZD 101527 - End
				//ZD 101446 Starts
                int EnablePArtyCode = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnablePartyCode");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnablePArtyCode);
                orgdata.EnablePartyCode = EnablePArtyCode;
                //ZD 101446
				//ZD 101869 start
                node = xd.SelectSingleNode("//SetOrgOptions/DefCodianLO");
                if (node != null)
                    int.TryParse(node.InnerText, out CodianLO);
                if (CodianLO <= 0)
                    CodianLO = 1;
                orgdata.DefCodianLO = CodianLO;
                //ZD 101869 End

                //ZD 101931 start
                int ShowVideoLayout = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/ShowVideoLayout");
                if(node != null)
                    int.TryParse(node.InnerText,out ShowVideoLayout);
                orgdata.ShowVideoLayout = ShowVideoLayout;
                //ZD 101931 End
                //ZD 102085 Starts
                string EWSConfAdmins = "";
                node = xd.SelectSingleNode("//SetOrgOptions/EWSConfAdmins");
                if (node != null)
                    EWSConfAdmins = node.InnerText.Trim();
                orgdata.EWSConfAdmins = EWSConfAdmins;
                //ZD 102085 Ends
                //ZD 102356
                int calDefaultDisplay = 2;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableCalDefaultDisplay");
                if (node != null)
                    int.TryParse(node.InnerText, out calDefaultDisplay);
                orgdata.EnableCalDefaultDisplay = calDefaultDisplay;

                //ZD 102532 Starts
                int EnableWaitList = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableWaitList");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableWaitList);
                orgdata.EnableWaitList = EnableWaitList;
                //ZD 102532 Ends

                //ZD 103046 Starts
                string PersonnelAlert = "";
                node = xd.SelectSingleNode("//SetOrgOptions/PersonnelAlert");
                if (node != null)
                {
					//Removing duplicate semicolons and checking empty. Ex: admin@myvrm.com;;admin2@myvrm.com;
                    string[] PersonnelList = node.InnerText.Split(';');
                    for (int k = 0; k < PersonnelList.Length; k++)
                    {
                        if (PersonnelList[k].Trim() != "")
                        {
                            if (PersonnelAlert == "")
                                PersonnelAlert = PersonnelList[k];
                            else
                                PersonnelAlert += ";" + PersonnelList[k];

                        }
                    }
                }
                orgdata.PersonnelAlert = PersonnelAlert;
                //ZD 103046 Ends
                // ZD 102514 Start
                int OnSiteAVSupportBuffer = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/OnSiteAVSupportBuffer");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out OnSiteAVSupportBuffer);
                orgdata.OnSiteAVSupportBuffer = OnSiteAVSupportBuffer;

                int CallMonitoringBuffer = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/CallMonitoringBuffer");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out CallMonitoringBuffer);
                orgdata.CallMonitoringBuffer = CallMonitoringBuffer;

                int DedicatedVNOCOperatorBuffer = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/DedicatedVNOCOperatorBuffer");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out DedicatedVNOCOperatorBuffer);
                orgdata.DedicatedVNOCOperatorBuffer = DedicatedVNOCOperatorBuffer;
                //ZD 102514 End
                //ZD 103046 Ends
                // ZD 103216 Start
                int EnableTravelAvoidTrack = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableTravelAvoidTrack");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableTravelAvoidTrack);
                orgdata.EnableTravelAvoidTrack = EnableTravelAvoidTrack;
                // ZD 103216 End

                //ZD 104021 Start
                int EnableBJNIntegration = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableBJNIntegration");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out EnableBJNIntegration);
                orgdata.EnableBJNIntegration = EnableBJNIntegration;

                int BJNMeetingType = 1;
                node = xd.SelectSingleNode("//SetOrgOptions/BJNMeetingType");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out BJNMeetingType);
                orgdata.BJNMeetingType = BJNMeetingType;

                int BJNSelectOption = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/BJNSelectOption");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out BJNSelectOption);
                orgdata.BJNSelectOption = BJNSelectOption;
                //ZD 104021 End

                //ZD 104116 - Start
                int BJNDisplay = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/BJNDisplay");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out BJNDisplay);
                orgdata.BJNDisplay = BJNDisplay;
                //ZD 104116 - End 
                // ZD 104151 Start
                int Mcutype = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EmptyConferencePush");
                if (node != null)
                    Int32.TryParse(node.InnerText, out Mcutype);
                orgdata.EmptyConferencePush = Mcutype;
                //ZD 104151 End
				//ZD 104221 start
                int EnablePoolOrderSelection = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnablePoolOrderSelection");
                if (node != null)
                    int.TryParse(node.InnerText, out EnablePoolOrderSelection);
                orgdata.EnablePoolOrderSelection = EnablePoolOrderSelection;
                //ZD 104221 End
                //ZD 104862 start
                int EnableBlockUserDI = 0;
                node = xd.SelectSingleNode("//SetOrgOptions/EnableBlockUserDI");
                if (node != null)
                    int.TryParse(node.InnerText, out EnableBlockUserDI);
                orgdata.EnableBlockUserDI = EnableBlockUserDI;
                //ZD 104862 End

                m_ISysTechDAO.Update(sysTechDta);
                m_IOrgSettingDAO.Update(orgdata);
                 
                //FB 2588 Starts
                sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);

                sqlCon.OpenConnection();

                sqlCon.OpenTransaction();

                strSQL = "Select timeformat from Usr_List_D where UserID = 11";
                System.Data.DataSet ds = sqlCon.ExecuteDataSet(strSQL);
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                        if (ds.Tables[0].Rows.Count > 0)
                            timeFormat = ds.Tables[0].Rows[0]["timeformat"].ToString();
                }

                if (orgdata.EnableZulu == 1)
                {
                    strSQL = "Update Usr_List_D set TimeZone= 33 , timezonedisplay = 0 , timeformat = 2 where companyId = " + orgdata.OrgId;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);
                }
                else if (timeFormat == "2" && orgdata.EnableZulu == 0)
                {
                    strSQL = "Update Usr_List_D set timeformat = 0 where companyId = " + orgdata.OrgId;
                    strExec = sqlCon.ExecuteNonQuery(strSQL);
                }
                sqlCon.CommitTransaction();

                sqlCon.CloseConnection();
                //FB 2588 Ends

                return true;
            }
            catch (Exception e)
            {
                sqlCon.RollBackTransaction();
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

        #region GetOrganizationList
        /// <summary>
        /// GetOrganizationList
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetOrganizationList(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmOrganization Temp = null;
            int i = 0;
            vrmState objState = null;
            vrmCountry objCountry = null;
            OrgOUTXML = new StringBuilder();
            xSettings = null;
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("deleted", 0));

                List<vrmOrganization> vrmOrg = m_IOrgDAO.GetByCriteria(criterionList);
                int orgCount = 0;

                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OrgOUTXML, xSettings))
                {
                    xWriter.WriteStartElement("GetOrganizationList");
                    if (vrmOrg != null)
                    {
                        if (vrmOrg.Count > 0)
                        {
                            orgCount = vrmOrg.Count;
                            for (i = 0; i < vrmOrg.Count; i++)
                            {
                                Temp = null;
                                Temp = vrmOrg[i];
                                xWriter.WriteStartElement("Organization");
                                xWriter.WriteElementString("OrgId", Temp.orgId.ToString());
                                xWriter.WriteElementString("OrganizationName", Temp.orgname);
                                xWriter.WriteElementString("Address1", Temp.address1);
                                xWriter.WriteElementString("Address2", Temp.address2);
                                xWriter.WriteElementString("City", Temp.city);
                                xWriter.WriteElementString("State", Temp.state.ToString());
                                if (Temp.state > 0)
                                {
                                    objState = m_IStateDAO.GetById(Temp.state);
                                    xWriter.WriteElementString("StateName", objState.StateCode.ToString());
                                }
                                else
                                {
                                    xWriter.WriteElementString("StateName", "");
                                }
                                xWriter.WriteElementString("Country", Temp.country.ToString());
                                if (Temp.country > 0)
                                {
                                    objCountry = m_ICountryDAO.GetById(Temp.country);
                                    xWriter.WriteElementString("CountryName", objCountry.CountryName);
                                }
                                else
                                {
                                    xWriter.WriteElementString("CountryName", "");
                                }
                                xWriter.WriteElementString("ZipCode", Temp.zipcode);
                                xWriter.WriteElementString("Phone", Temp.phone);
                                xWriter.WriteElementString("Fax", Temp.fax);
                                xWriter.WriteElementString("EmailID", Temp.email);
                                xWriter.WriteElementString("Website", Temp.website);
                                xWriter.WriteFullEndElement(); //ZD 101086
                            }
                        }
                    }
                    int remLicense = 0;
                    remLicense = sysSettings.MaxOrganizations - orgCount;
                    xWriter.WriteElementString("OrganizationLimit", sysSettings.MaxOrganizations.ToString());
                    xWriter.WriteElementString("LicenseRemaining", remLicense.ToString());
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.Flush();

                }
                
                obj.outXml = OrgOUTXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region GetOrganizationProfile
        /// <summary>
        /// <GetOrganizationProfile>
        ///   <UserID></UserID>
        ///   <OrgId></OrgId>
        /// </GetOrganizationProfile>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetOrganizationProfile(ref vrmDataObject obj)
        {
            bool bRet = true;
            string orglicense = ""; //FB 2678
            cryptography.Crypto crypto = new cryptography.Crypto(); //FB 2678
            DateTime expirydate = DateTime.Now; //FB 2678
            string dformat = "MM/dd/yyyy"; //FB 2678
            OrgOUTXML = new StringBuilder();
            try
            {
                obj.outXml = string.Empty;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetOrganizationProfile/UserID");
                string userId = node.InnerXml.ToString();

                node = xd.SelectSingleNode("//GetOrganizationProfile/OrgId");
                string organizationId = node.InnerXml.ToString();

                organizationID = 0;
                int.TryParse(organizationId, out organizationID);
                vrmOrganization profile = m_IOrgDAO.GetById(organizationID);
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OrgOUTXML, xSettings))
                {
                    if (profile == null)
                    {
                         xWriter.WriteStartElement("error");
                         xWriter.WriteElementString("errorCode","200");
                         xWriter.WriteElementString("message","Invalid Organization");
                         xWriter.WriteElementString("level","S");
                         xWriter.WriteFullEndElement(); //ZD 101086
                    }
                    else
                    {
                        xWriter.WriteStartElement("OrganizationProfile");
                        xWriter.WriteElementString("OrganizationId",profile.orgId.ToString());
                        xWriter.WriteElementString("OrganizationName",profile.orgname);
                        xWriter.WriteElementString("Address1",profile.address1);
                        xWriter.WriteElementString("Address2",profile.address2);
                        xWriter.WriteElementString("City",profile.city);
                        xWriter.WriteElementString("State",profile.state.ToString());

                        if (profile.state > 0)
                        {
                            vrmState objState = m_IStateDAO.GetById(profile.state);
                            xWriter.WriteElementString("StateName",objState.StateCode.ToString());
                        }
                        else
                        {
                            xWriter.WriteElementString("StateName","");
                        }

                        xWriter.WriteElementString("Country",profile.country.ToString());
                        if (profile.country > 0)
                        {
                            vrmCountry objCountry = m_ICountryDAO.GetById(profile.country);
                            xWriter.WriteElementString("CountryName",objCountry.CountryName);
                        }
                        else
                        {
                            xWriter.WriteElementString("CountryName","");
                        }
                        xWriter.WriteElementString("ZipCode",profile.zipcode);
                        xWriter.WriteElementString("Phone",profile.phone);
                        xWriter.WriteElementString("Fax",profile.fax);
                        xWriter.WriteElementString("EmailID",profile.email);
                        xWriter.WriteElementString("Website",profile.website);
                        xWriter.WriteElementString("Orgexpirykey",profile.orgExpiryKey);//FB 2678
                        xWriter.WriteElementString("OrgStatus",profile.orgStatus.ToString());//FB 2678
                        //FB 2678 Start

                        #region Get Org Expiry date
                        if (organizationId != "new")
                        {
                            Int32.TryParse(organizationId, out organizationID);
                            if (!string.IsNullOrEmpty(profile.orgExpiryKey.Trim()))//ZD 100877
                            {
                                orglicense = crypto.decrypt(profile.orgExpiryKey);
                                XmlDocument xd1 = new XmlDocument();
                                xd1.LoadXml(orglicense);
                                XmlNode Orgnode;

                                Orgnode = xd1.SelectSingleNode("//Organization/ID");
                                int licenseorgId = Int32.Parse(Orgnode.InnerXml.Trim());

                                Orgnode = xd1.SelectSingleNode("//Organization/ExpiryDate");
                                string ExpirationDate = Orgnode.InnerXml.Trim();
                                DateTime.TryParse(ExpirationDate, out expirydate);
                                if (licenseorgId == organizationID)
                                {
                                    xWriter.WriteElementString("OrgexpiryDate",expirydate.ToString(dformat));
                                }
                                else
                                {
                                    xWriter.WriteElementString("OrgexpiryDate","");
                                }
                            }
                            else
                                xWriter.WriteElementString("OrgexpiryDate","");
                        }

                        #endregion

                        //FB 2678 End

                        orgInfo = m_IOrgSettingDAO.GetByOrgId(profile.orgId);
                        //FB 2659 Starts
                        if (sysSettings.EnableCloudInstallation == 1)
                        {
                            xWriter.WriteElementString("Seats", orgInfo.Seats.ToString());
                            xWriter.WriteElementString("Users", orgInfo.UserLimit.ToString());
                            //ZD 100518 Starts
                            xWriter.WriteElementString("MaxParticipants", orgInfo.MaxParticipants.ToString());
                            xWriter.WriteElementString("MaxConcurrentCall", orgInfo.MaxConcurrentCall.ToString());
                            //ZD 100518 Ends
                        }
                            xWriter.WriteElementString("Seats", orgInfo.Seats.ToString()); //FB 2659
                            //ZD 100518 Starts
                            xWriter.WriteElementString("MaxParticipants", orgInfo.MaxParticipants.ToString());
                            xWriter.WriteElementString("MaxConcurrentCall", orgInfo.MaxConcurrentCall.ToString());
                            //ZD 100518 Ends
                            xWriter.WriteElementString("Rooms", orgInfo.RoomLimit.ToString());
                            xWriter.WriteElementString("Users", orgInfo.UserLimit.ToString());
                            xWriter.WriteElementString("ExchangeUsers", orgInfo.ExchangeUserLimit.ToString());
                            xWriter.WriteElementString("DominoUsers", orgInfo.DominoUserLimit.ToString());
                            xWriter.WriteElementString("MobileUsers", orgInfo.MobileUserLimit.ToString()); //FB 1979
                            xWriter.WriteElementString("WebexUsers", orgInfo.WebexUserLimit.ToString()); //ZD 100221
                            xWriter.WriteElementString("BlueJeansUsers", orgInfo.BlueJeansUserLimit.ToString()); ////ZD 104021
                            xWriter.WriteElementString("EnableBlueJeans", orgInfo.EnableBlueJeans.ToString()); //ZD 104021 value 0 or 1 depends BJN user license
                            xWriter.WriteElementString("MCU", orgInfo.MCULimit.ToString());
                            xWriter.WriteElementString("MCUEnchanced", orgInfo.MCUEnchancedLimit.ToString());//FB 2486
                            xWriter.WriteElementString("EndPoints", orgInfo.MaxEndpoints.ToString());
                            xWriter.WriteElementString("VideoRooms", orgInfo.MaxVideoRooms.ToString());
                            xWriter.WriteElementString("NonVideoRooms", orgInfo.MaxNonVideoRooms.ToString());
                            xWriter.WriteElementString("VMRRooms", orgInfo.MaxVMRRooms.ToString());//FB 2586
                            xWriter.WriteElementString("iControlRooms", orgInfo.MaxiControlRooms.ToString());//ZD 101098
                            //FB 2426 Start
                            xWriter.WriteElementString("GuestRooms", orgInfo.GuestRoomLimit.ToString());
                            xWriter.WriteElementString("GuestRoomPerUser", orgInfo.GuestRoomPerUser.ToString());
                            //FB 2426 End
                            xWriter.WriteElementString("EnableFacilites", orgInfo.EnableFacilities.ToString());
                            xWriter.WriteElementString("EnableCatering", orgInfo.EnableCatering.ToString());
                            xWriter.WriteElementString("EnableHouseKeeping", orgInfo.EnableHousekeeping.ToString());
                            xWriter.WriteElementString("EnableAPIs", orgInfo.EnableAPI.ToString());
                            //obj.outXml += "<EnablePC>" + orgInfo.EnablePCModule + "</EnablePC>";  //FB 2347 //FB 2693
                            xWriter.WriteElementString("EnableCloud", orgInfo.EnableCloud.ToString());  //FB 2262 //FB 2599
                            xWriter.WriteElementString("EnablePublicRoom", orgInfo.EnablePublicRoomService.ToString());  //FB 2594
                            //FB 2693 Starts
                            xWriter.WriteElementString("PCUsers", orgInfo.PCUserLimit.ToString());
                            xWriter.WriteElementString("EnableJabber", orgInfo.EnableJabber.ToString());
                            xWriter.WriteElementString("EnableLync", orgInfo.EnableLync.ToString());
                            xWriter.WriteElementString("EnableVidtel", orgInfo.EnableVidtel.ToString());
                            //FB 2693 Ends
                            xWriter.WriteElementString("VCHotRooms", orgInfo.MaxVCHotdesking.ToString()); //FB 2694
                            xWriter.WriteElementString("ROHotRooms", orgInfo.MaxROHotdesking.ToString());  //FB 2694
							xWriter.WriteElementString("EnableAdvancedReport",orgInfo.EnableAdvancedReport.ToString());  //FB 2593
                            xWriter.WriteFullEndElement(); //ZD 101086
                            xWriter.Flush();
                        //}//ZD 100535 12/12/2013 Inncrewin Ends//FB 2659 Ends
                    }
                }
                obj.outXml = OrgOUTXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region SetOrganizationProfile
        /// <summary>
        /// SetOrganizationProfile
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetOrganizationProfile(ref vrmDataObject obj)
        {
            int recordCount = 0,errNo=0; //FB 2426
            IList recCnt = null;
            bool retVal = true;
            string Tier1Name = "", Tier1ID = "", Tier2Name = "", Tier2ID = "";//FB 2426
            OrgData newOrgData = null;
            int status = 0; //FB 2678
            string orglicense = ""; //FB 2678
            cryptography.Crypto crypto = new cryptography.Crypto(); //FB 2678
            DateTime expirydate = DateTime.Now; //FB 2678
            string License = "", expirydateVal = "";//FB 2678           
            int licenseorgId = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/OrgId");
                string orgId = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/OrgName");
                string orgname = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/Address1");
                string address1 = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/Address2");
                string address2 = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/City");
                string city = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/State");
                string state = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/Country");
                string country = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/ZipCode");
                string zipcode = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/Phone");
                string phone = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/Fax");
                string fax = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/EmailID");
                string email = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/Website");
                string website = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/Orgexpirykey"); //FB 2678
                string Orgexpirykey = node.InnerXml.Trim();

                //FB 2659
                string roomlimit = "", vroomlimit = "", nvroomlimit = "", mculimit = "", userlimit = "", exUserlimit = "";
                string extRoomlimit = "", extGuestRoomPerUser = "", domUserlimit = "", mobUserlimit = "", eplimit = "", enableAVMod = "", WebexUserlimit = "";//ZD 100221
                string enableCat = "", enableHKG = "", enableAPIs = "", mcuenchancedlimit = "", vmrroomlimit = "";
                string enableCloud = "", vcHotRooms = "", roHotRooms = "", BJNUserlimit = ""; //ZD 104021
                string iControlroomlimit = "";//ZD 101098

                //FB 2659 Starts
                if (sysSettings.EnableCloudInstallation == 0 || sysSettings.EnableCloudInstallation == 1)//ZD 100535 12/12/2013 Inncrewin 
                {

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/roomlimit");
                    roomlimit = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/videoroomlimit");
                    vroomlimit = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/nonvideoroomlimit");
                    nvroomlimit = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/mculimit");
                    mculimit = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/userlimit");
                    userlimit = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/exchangeusers");
                    if (node != null) exUserlimit = node.InnerXml.Trim();//ZD 100535 
                    //FB 2426 Start
                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/GuestRooms");
                    extRoomlimit = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/GuestRoomPerUser");
                    extGuestRoomPerUser = node.InnerXml.Trim();
                    //FB 2426 End
                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/dominousers");
                    domUserlimit = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/mobileusers"); //FB 1979
                    mobUserlimit = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/WebexUsers"); //ZD 100221
                    WebexUserlimit = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/BlueJeansUsers"); //ZD 104021
                    BJNUserlimit = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/endpointlimit");
                    eplimit = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/enablefacilities");
                    enableAVMod = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/enablecatering");
                    enableCat = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/enablehousekeeping");
                    enableHKG = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/enableAPI");
                    enableAPIs = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/mcuenchancedlimit");//FB 2486
                    mcuenchancedlimit = node.InnerXml.Trim();

                    vmrroomlimit = "";
                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/vmrroomlimit");//FB 2586
                    if (node != null)
                        vmrroomlimit = node.InnerXml.Trim();

                    iControlroomlimit = "";
                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/iControlroomlimit");//ZD 101098
                    if (node != null)
                        iControlroomlimit = node.InnerXml.Trim();

                    enableCloud = "";
                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/enableCloud"); //FB 2262//FB 2599
                    if (node != null)
                        enableCloud = node.InnerXml.Trim();//ZD 100535 12/12/2013 Inncrewin 

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/EnablePublicRoom"); //FB 2594
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out enablePublicRoom);

                    //FB 2693 Starts
                    orgPcUserLimit = 0; enableJabber = 0; enableLync = 0; enableVidtel = 0;
                    //enableBJ = 0; //ZD 104021

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/pcusers");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out orgPcUserLimit);
					//ZD 104021
                    //node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/enableBlueJeans");
                    //if (node != null)
                    //    int.TryParse(node.InnerText.Trim(), out enableBJ);

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/enableJabber");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out enableJabber);

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/enableLync");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out enableLync);

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/enableVidtel");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out enableVidtel);

                    //FB 2693 Ends
                    //FB 2694
                    vcHotRooms = "";
                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/VCHotRooms");
                    if (node != null)
                        vcHotRooms = node.InnerXml.Trim();

                    roHotRooms = "";
                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/ROHotRooms");
                    if (node != null)
                        roHotRooms = node.InnerXml.Trim();

                    //FB 2593 Start
                    enableAdvReport = 0;
                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/enableAdvReport");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out enableAdvReport);
                    //FB 2593 End
                    //FB 2659 Ends
                }
                //else
                //{//ZD 100535 12/12/2013 Inncrewin
                //    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/videoroomlimit");
                //    vroomlimit = node.InnerXml.Trim();
                //    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/nonvideoroomlimit");
                //    nvroomlimit = node.InnerXml.Trim();
                //    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/endpointlimit");
                //    eplimit = node.InnerXml.Trim();
                //}
                //ZD 100535 12/12/2013 Inncrewin Ends
                int orgstate = 0;
                int.TryParse(state, out orgstate);

                int orgcountry = 0;
                int.TryParse(country, out orgcountry);

                loginUser = 11;   //VRM Administrator ID
                int.TryParse(userID, out loginUser);

                //FB 2659 Starts
              
                if (sysSettings.EnableCloudInstallation == 1)
                {
                    int totalSeats = 0;
                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/Seats");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out totalSeats);

                    int activeUsers = 0;
                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/userlimit");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out activeUsers);
                    orgUserLimit = activeUsers;
                    orgSeats = totalSeats;

                    //ZD 100518 Starts
                    int MaxParticipants = 0, MaxConcurrentCall = 0;
                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/MaxParticipants");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out MaxParticipants);
                    orgMaxParticipants = MaxParticipants;

                    node = xd.SelectSingleNode("//SetOrganizationProfile/Organization/MaxConcurrentCall");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out MaxConcurrentCall);
                    orgMaxConcurrentCall = MaxConcurrentCall;
                    //ZD 100518 Ends
                }
                
                    orgRoomLimit = 0;
                    int.TryParse(roomlimit, out orgRoomLimit);

                    orgVideoRoomLimit = 0;
                    int.TryParse(vroomlimit, out orgVideoRoomLimit);

                    orgNVRoomLimit = 0;
                    int.TryParse(nvroomlimit, out orgNVRoomLimit);

                    orgMCULimit = 0;
                    int.TryParse(mculimit, out orgMCULimit);

                    orgMCUEnchancedLimit = 0; //FB 2486
                    int.TryParse(mcuenchancedlimit, out orgMCUEnchancedLimit);

                    orgUserLimit = 0;
                    int.TryParse(userlimit, out orgUserLimit);

                    orgExchangeUserLimit = 0;
                    int.TryParse(exUserlimit, out orgExchangeUserLimit);
                    //FB 2426 Start
                    orgGuestRoomLimit = 0;
                    int.TryParse(extRoomlimit, out orgGuestRoomLimit);

                    orgGuestRoomPerUserLimit = 0;
                    int.TryParse(extGuestRoomPerUser, out orgGuestRoomPerUserLimit);

                    orgVMRRoomLimit = 0;//FB 2586
                    int.TryParse(vmrroomlimit, out orgVMRRoomLimit);
                    //FB 2426 End

                    orgiControlRoomLimit = 0;//ZD 101098
                    int.TryParse(iControlroomlimit, out orgiControlRoomLimit);

                    orgDominoUserLimit = 0;
                    int.TryParse(domUserlimit, out orgDominoUserLimit);

                    orgMobileUserLimit = 0; //FB 1979
                    int.TryParse(mobUserlimit, out orgMobileUserLimit);

                    orgWebexUserLimit = 0; //ZD 100221
                    int.TryParse(WebexUserlimit, out orgWebexUserLimit);

                    orgBJNUserLimit = 0; //ZD 104021
                    int.TryParse(BJNUserlimit, out orgBJNUserLimit);

                    orgEndPointLimit = 0;
                    int.TryParse(eplimit, out orgEndPointLimit);

                    enableAPI = 0;
                    int.TryParse(enableAPIs, out enableAPI);

                    enableAV = 0;
                    int.TryParse(enableAVMod, out enableAV);

                    enableCatering = 0;
                    int.TryParse(enableCat, out enableCatering);

                    enableHK = 0;
                    int.TryParse(enableHKG, out enableHK);

                    //enablePCs = 0; //FB 2347
                    //int.TryParse(enablePC, out enablePCs);

                    //FB 2262//FB 2599 Starts
                    enableClouds = 0;
                    int.TryParse(enableCloud, out enableClouds);
                    //FB 2599 Ends

                    orgVMRRoomLimit = 0;//FB 2586
                    int.TryParse(vmrroomlimit, out orgVMRRoomLimit);

                    orgiControlRoomLimit = 0;//ZD 101098
                    int.TryParse(iControlroomlimit, out orgiControlRoomLimit);
                    
                    //FB 2694
                    orgVCHotRoomLimit = 0;
                    int.TryParse(vcHotRooms, out orgVCHotRoomLimit);

                    orgROHotRoomLimit = 0;
                    int.TryParse(roHotRooms, out orgROHotRoomLimit);
                  
               //FB 2678 Start

                #region Org License Check
                if (orgId != "new" && sysSettings.EnableCloudInstallation <= 0)//FB 2659
                {
                    try
                    {
                        Int32.TryParse(orgId, out organizationID);
                        orglicense = crypto.decrypt(Orgexpirykey);
                        XmlDocument xd1 = new XmlDocument();
                        xd1.LoadXml(orglicense);
                        XmlNode Orgnode;

                        Orgnode = xd1.SelectSingleNode("//Organization/ID");
                        licenseorgId = Int32.Parse(Orgnode.InnerXml.Trim());//FB 2678

                        Orgnode = xd1.SelectSingleNode("//Organization/ExpiryDate");
                        string ExpirationDate = Orgnode.InnerXml.Trim();
                        DateTime.TryParse(ExpirationDate, out expirydate);
                        if (licenseorgId == organizationID)
                        {
                            if (DateTime.Now > sysSettings.ExpiryDate || expirydate > sysSettings.ExpiryDate || DateTime.Now >= expirydate)
                            {
                                myvrmEx = new myVRMException(667);
                                obj.outXml = myvrmEx.FetchErrorMsg();
                                return false;
                            }
                            else
                                status = 1;
                        }
                        else
                        {
                            myvrmEx = new myVRMException(667);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }

                    }
                    catch (Exception e)
                    {
                        myvrmEx = new myVRMException(667);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }

                #endregion

                //FB 2678 End


                if (!GuestRoomPerUserValidation(ref orgGuestRoomPerUserLimit, ref errNo,ref orgId))
                {
                    myvrmEx = new myVRMException(errNo);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                recordCount = 0;
                string orgQry = "SELECT vr.crossaccess FROM myVRM.DataLayer.vrmUserRoles vr, myVRM.DataLayer.vrmUser vu WHERE vr.roleID=vu.roleID and vu.userid='"+ loginUser +"'";
                recCnt = m_IuserDAO.execQuery(orgQry);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out recordCount);
                        }
                    }
                }
                if(recordCount != 1)
                {
                    myvrmEx = new myVRMException(444);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                vrmOrganization orgProfiles = new vrmOrganization();
                orgProfiles.orgname = orgname;
                orgProfiles.address1 = address1;
                orgProfiles.address2 = address2;
                orgProfiles.city = city;
                orgProfiles.state = orgstate;
                orgProfiles.country = orgcountry;
                orgProfiles.zipcode = zipcode;
                orgProfiles.phone = phone;
                orgProfiles.fax = fax;
                orgProfiles.email = email;
                orgProfiles.website = website;
                orgProfiles.orgExpiryKey = Orgexpirykey;//FB 2678
                orgProfiles.orgStatus = status;//FB 2678
                orgProfiles.deleted = 0;

                if (orgId.ToLower() == "new")
                {
                    recordCount = 0;
                    orgQry = "SELECT count(*) FROM myVRM.DataLayer.vrmOrganization vo WHERE vo.deleted=0";
                    recCnt = m_IOrgDAO.execQuery(orgQry);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            if (recCnt[0] != null)
                            {
                                if (recCnt[0].ToString() != "")
                                    int.TryParse(recCnt[0].ToString(), out recordCount);
                            }
                        }
                    }
                    recordCount += 1;
                    if (recordCount > sysSettings.MaxOrganizations)
                    {
                        myvrmEx = new myVRMException(445);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    recordCount = 0;
                    string orgNameCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmOrganization vo WHERE vo.orgname='" + orgname.Trim() + "' and vo.deleted=0";
                    recCnt = m_IOrgDAO.execQuery(orgNameCnt);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            if (recCnt[0] != null)
                            {
                                if (recCnt[0].ToString() != "")
                                    int.TryParse(recCnt[0].ToString(), out recordCount);
                            }
                        }
                    }
                    if (recordCount > 0)
                    {
                        myvrmEx = new myVRMException(446);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    if (!ValidateResourcesLimits(ref obj, 0, sysSettings.EnableCloudInstallation))//ZD 100535
                        return false;

                    m_IOrgDAO.Save(orgProfiles);
                    organizationID = getMaxOrgID();

                    if (organizationID > defaultOrgID)
                    {
                        retVal = CreateDfltOrgData();
                        if (!retVal)
                        {
                            myvrmEx = new myVRMException(447);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        retVal = CreateDfltTechData();
                        if (!retVal)
                        {
                            myvrmEx = new myVRMException(448);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        retVal = CreateDfltCustomAttributes();  //FB 1779
                        if (!retVal)
                        {
                            myvrmEx = new myVRMException(449);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        //FB 2506 Starts
                        retVal = CreateDefaultTxtMsg();
                        if (!retVal)
                        {
                            myvrmEx = new myVRMException(556);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                        //FB Ends
                        //ZD 101835
                        StringBuilder inXML = new StringBuilder();
                        inXML.Append("<ConfArchiveConfiguration>");
                        inXML.Append("<UserID>11</UserID>");
                        inXML.Append("<organizationID>" + organizationID + "</organizationID>");
                        inXML.Append("</ConfArchiveConfiguration>");
                        vrmDataObject objnew = new vrmDataObject(m_configPath);
                        objnew.inXml = inXML.ToString();
                        retVal = SetConfArchiveConfiguration(ref objnew);
                        if(!retVal)
                        {
                            myvrmEx = new myVRMException(749);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //FB 2678 - Start
                    if (sysSettings.IndividualOrgExpiry == 0)
                    {                        
                        licenseorgId = organizationID;
                        expirydateVal = sysSettings.ExpiryDate.ToString("MM/dd/yyyy");
                        License = "<Organization><ID>" + licenseorgId + "</ID><ExpiryDate>" + expirydateVal + "</ExpiryDate></Organization>";
                        orglicense = crypto.encrypt(License);
                        if (DateTime.Now < sysSettings.ExpiryDate)
                            status = 1;                        
                        orgProfiles.orgExpiryKey = orglicense;
                        orgProfiles.orgStatus = status;
                    }
                    else
                    {
                        orgProfiles.orgExpiryKey = "";
                        orgProfiles.orgStatus = status;
                    }
                    m_IOrgDAO.Update(orgProfiles);
                    //FB 2678 - End
                        
                    //FB 2426 Start
                    //ZD 100068 Starts
                    vrmDataObject tierobj = obj;
                    int Tier2value = 0, Tier1value = 0;//ZD 100068
                    string Onfly = "";
                    Onfly += "<SetTier1>";
                    Onfly += "<organizationID>" + organizationID + "</organizationID>";
                    Onfly += "<UserID>" + userID + "</UserID>";
                    Onfly += "<ID>new</ID>";
                    Onfly += "<Name>Top Tier</Name>";
                    Onfly += "</SetTier1>";
                    tierobj.inXml = Onfly;
                    m_myVRMSearch.SetTier1(ref tierobj);
                    if (tierobj.outXml.IndexOf("<error>") < 0)
                    {
                        XmlDocument xdt = new XmlDocument();
                        xdt.LoadXml(tierobj.outXml);
                        XmlNode node1;
                        node1 = xdt.SelectSingleNode("//SetTier1/Tier1Name");
                        if (node1 != null)
                            Tier1Name = node1.InnerXml.Trim();
                        node1 = xdt.SelectSingleNode("//SetTier1/Tier1ID");
                        if (node1 != null)
                            int.TryParse(node1.InnerXml.Trim(), out Tier1value);
                    }
                    string Onfly1 = "";
                    Onfly1 += "<SetTier2>";
                    Onfly1 += "<organizationID>" + organizationID + "</organizationID>";
                    Onfly1 += "<UserID>" + userID + "</UserID>";
                    Onfly1 += "<Tier1ID>" + Tier1value + "</Tier1ID>";
                    Onfly1 += "<ID>new</ID>";
                    Onfly1 += "<Name>Middle Tier</Name>";
                    Onfly1 += "</SetTier2>";
                    tierobj.inXml = Onfly1;
                    m_myVRMSearch.SetTier2(ref tierobj);
                    if (tierobj.outXml.IndexOf("<error>") < 0)
                    {
                        XmlDocument xdm = new XmlDocument();
                        xdm.LoadXml(tierobj.outXml);
                        XmlNode node2 = null;
                        node2 = xdm.SelectSingleNode("//SetTier2/Tier2Name");
                        if (node2 != null)
                            Tier2Name = node2.InnerXml.Trim();
                        node2 = xdm.SelectSingleNode("//SetTier2/Tier2ID");
                        if (node2 != null)
                            int.TryParse(node2.InnerXml.Trim(), out Tier2value);
                    }

                    
                    newOrgData = m_IOrgSettingDAO.GetByOrgId(organizationID);
                    newOrgData.OnflyMiddleTierID = Tier2value;
                    newOrgData.OnflyTopTierID = Tier1value;
                    newOrgData.TopTier = Tier1Name;
                    newOrgData.MiddleTier = Tier2Name;
                    //ZD 100164 Changes made by Inncrewin
                    if (sysSettings.EnableCloudInstallation == 1)
                        newOrgData.EnableAdvancedUserOption = 1;
                    //ZD 100164 ends Changes made by Inncrewin
                    newOrgData.VMRMiddleTierID = Tier2value;
                    newOrgData.VMRTopTierID = Tier1value;
                    //ZD 100068 Ends
                    m_IOrgSettingDAO.Update(newOrgData);
                    //FB 2426 End
                }
                else
                {
                    organizationID = 0;
                    int.TryParse(orgId, out organizationID);
                    if (organizationID < defaultOrgID)
                    {
                        obj.outXml = myVRMException.toXml("Invalid organization ID");
                        return false;
                    }
                    if (!ValidateResourcesLimits(ref obj, 1, sysSettings.EnableCloudInstallation))//ZD 100535 12/12/2013 
                        return false;
                    
                    //FB 2705 - Start
                    List<ICriterion> criterionLists = new List<ICriterion>();

                    if(orgname != "")
                        criterionLists.Add(Expression.Eq("orgname", orgname));

                    criterionLists.Add(Expression.Eq("deleted", 0));
                    List<vrmOrganization> orgList = m_IOrgDAO.GetByCriteria(criterionLists);

                    if (orgList.Count > 0 && orgList[0].orgId != organizationID)
                    {
                        myvrmEx = new myVRMException(446);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    //FB 2705 - End

                    orgProfiles.orgId = organizationID;
                    m_IOrgDAO.Update(orgProfiles);

                    obj.outXml = "";
                    retVal = UpdateOrgLimits(obj);
                    if (!retVal)
                    {
                        if (obj.outXml == "")
                        {
                            myvrmEx = new myVRMException(450);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                        }
                        return false;
                    }
                }
                obj.outXml = "<success><organizationID>" + organizationID + "</organizationID></success>"; //Organization CSS Module
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }

        /// <summary>
        /// UpdateOrgLimits
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        private bool UpdateOrgLimits(vrmDataObject obj)
        {
            try
            {
                m_IOrgSettingDAO.clearFetch();

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingDAO.GetByOrgId(organizationID);

                if (orgInfo == null)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                
                orgInfo.RoomLimit = orgRoomLimit;
                orgInfo.UserLimit = orgUserLimit;
                orgInfo.MCULimit = orgMCULimit;
                orgInfo.MCUEnchancedLimit = orgMCUEnchancedLimit; //FB 2486
                //ZD 101757
                if (orgMCUEnchancedLimit == 0)
                    orgInfo.EnableActMsgDelivery = 0;

                orgInfo.MaxVideoRooms = orgVideoRoomLimit;
                orgInfo.MaxNonVideoRooms = orgNVRoomLimit;
                orgInfo.MaxVMRRooms = orgVMRRoomLimit;//FB 2586
                orgInfo.MaxiControlRooms = orgiControlRoomLimit;//ZD 101098
                orgInfo.MaxEndpoints = orgEndPointLimit;
                orgInfo.EnableAPI = enableAPI;
                orgInfo.EnableFacilities = enableAV;
                orgInfo.EnableHousekeeping = enableHK;
                orgInfo.EnableCatering = enableCatering;
                orgInfo.ExchangeUserLimit = orgExchangeUserLimit;
                orgInfo.DominoUserLimit = orgDominoUserLimit;
                orgInfo.MobileUserLimit = orgMobileUserLimit; // FB 1979
                orgInfo.WebexUserLimit = orgWebexUserLimit; //ZD 100221
                //ZD 104021 start //ZD 104116 - Start
                orgInfo.BlueJeansUserLimit = orgBJNUserLimit;
                if (orgBJNUserLimit > 0)
                {
                    orgInfo.EnableBlueJeans = 1;
                }
                else
                {
                    if (sysSettings.MaxBlueJeansUsers != -2)
                        orgInfo.EnableBlueJeans = 0;
                }
                //ZD 104021 end //ZD 104116 - End

                //orgInfo.EnablePCModule = enablePCs; // FB 2347 //FB 2693
                //FB 2426 Start
                orgInfo.GuestRoomLimit = orgGuestRoomLimit; 
                orgInfo.GuestRoomPerUser = orgGuestRoomPerUserLimit;
                //FB 2694 Starts
                orgInfo.MaxVCHotdesking = orgVCHotRoomLimit;
                orgInfo.MaxROHotdesking = orgROHotRoomLimit;
				//FB 2694 End
                //FB 2426 End
                orgInfo.EnableCloud = enableClouds; // FB 2262 - J //FB 2599
                //FB 2693 Starts
                orgInfo.PCUserLimit = orgPcUserLimit;
                //orgInfo.EnableBlueJeans = enableBJ; //ZD 104021
                orgInfo.EnableJabber = enableJabber;
                orgInfo.EnableLync = enableLync;
                orgInfo.EnableVidtel = enableVidtel;
                //FB 2693 Ends
                orgInfo.EnableAdvancedReport = enableAdvReport;//FB 2593
				//FB 2717 Starts
                if (orgInfo.EnableCloud == 1) 
                {
                    orgInfo.EnableAudioVideoConference = 1;
                    orgInfo.isAssignedMCU = 0;
                }
                //if (orgInfo.EnableCloud == 1)
                //{
                //    orgInfo.EnableAudioBridges = 0;
                //    orgInfo.DefaultConferenceType = 2;
                //    orgInfo.EnableVMR = 3;
                //}
                //FB 2717 End

                orgInfo.EnablePublicRoomService = enablePublicRoom; //FB 2594
                orgInfo.Seats = orgSeats; //FB 2659
                //ZD 100518 Starts
                orgInfo.MaxParticipants = orgMaxParticipants;
                orgInfo.MaxConcurrentCall = orgMaxConcurrentCall;
                //ZD 100518  Ends
                m_IOrgSettingDAO.Update(orgInfo);

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("UpdateOrgLimits : " + ex.Message); //FB 1881
                obj.outXml = ""; //FB 1881 // myVRMException.toXml(ex.Message);
                return false;
            }
        }

        /// <summary>
        /// ValidateResourcesLimits
        /// 0-Add mode; 1 -Edit mode
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="mode"></param>
        /// <returns></returns>
        private bool ValidateResourcesLimits(ref vrmDataObject obj, int mode, int EnableCloudInstallation)//ZD 100535 12/12/2013 Inncrewin
        {
            int exRoomlimit = 0;
            int exMaxVRooms = 0;
            int exMaxNVRooms = 0;
            int exMaxMCU = 0;
            int exEndPoint = 0;
            int exUserLimit = 0;
            int exExchUsrLimit = 0;
            int exDominoUsrLimit = 0;
            int exMobileUsrLimit = 0; //FB 1979
            int exWebexUsrLimit = 0; //ZD 100221
            int exBJNUsrLimit = 0; //ZD 104021
            //FB 2426 Start
            int extRoomLimit = 0;
            int extGuestRoomPerUserLimit = 0;
            //FB 2426 End
            int exMaxEnchancedMCU = 0;//FB 2486
            int exMaxVMRRooms = 0;//FB 2586
            int exPCUserLimit = 0; //FB 2693
            int exMaxiControlRooms = 0;//ZD 101098
            IList recCnt=null;
            //FB 2693 Starts
            string strSQL = ""; 
            ns_SqlHelper.SqlHelper sqlCon = null;
            int strExec = -1; 
            //FB 2693 Ends
            try
            {
                int modCnt = 0; //Facilities Module
                bool isEnabled = false;
                string orgQry = "";
                if (enableAV == 1)
                {
                    orgQry = "SELECT od.OrgId FROM myVRM.DataLayer.OrgData od WHERE od.EnableFacilities=1";
                    recCnt = m_IOrgDAO.execQuery(orgQry);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            modCnt = recCnt.Count;
                            isEnabled = recCnt.Contains(organizationID);
                        }
                    }
                    if(!isEnabled)
                        modCnt = modCnt+1;

                    if (modCnt > (sysSettings.MaxFacilities))
                    {
                        myvrmEx = new myVRMException(451);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                modCnt = 0; //Catering Module
                isEnabled = false;
                if (enableCatering == 1)
                {
                    orgQry = "SELECT od.OrgId FROM myVRM.DataLayer.OrgData od WHERE od.EnableCatering=1";
                    recCnt = m_IOrgDAO.execQuery(orgQry);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            modCnt = recCnt.Count;
                            isEnabled = recCnt.Contains(organizationID);
                        }
                    }
                    if (!isEnabled)
                        modCnt = modCnt + 1;

                    if (modCnt > (sysSettings.MaxCatering))
                    {
                        myvrmEx = new myVRMException(452);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                modCnt = 0; //House Keeping Module
                isEnabled = false;
                if (enableHK == 1)
                {
                    orgQry = "SELECT od.OrgId FROM myVRM.DataLayer.OrgData od WHERE od.EnableHousekeeping=1";
                    recCnt = m_IOrgDAO.execQuery(orgQry);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            modCnt = recCnt.Count;
                            isEnabled = recCnt.Contains(organizationID);
                        }
                    }
                    if (!isEnabled)
                        modCnt = modCnt + 1;

                    if (modCnt > (sysSettings.MaxHousekeeping))
                    {
                        myvrmEx = new myVRMException(453);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
               
                modCnt = 0; //API Module
                isEnabled = false;
                if (enableAPI == 1)
                {
                    orgQry = "SELECT od.OrgId FROM myVRM.DataLayer.OrgData od WHERE od.EnableAPI=1";
                    recCnt = m_IOrgDAO.execQuery(orgQry);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            modCnt = recCnt.Count;
                            isEnabled = recCnt.Contains(organizationID);
                        }
                    }
                    if (!isEnabled)
                        modCnt = modCnt + 1;

                    if (modCnt > (sysSettings.MaxAPIs))
                    {
                        myvrmEx = new myVRMException(454);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                //FB 2693 Starts
                modCnt = 0; 
                isEnabled = false;
                if (enableJabber == 1)
                {
                    orgQry = "SELECT od.OrgId FROM myVRM.DataLayer.OrgData od WHERE od.EnableJabber=1";
                    recCnt = m_IOrgDAO.execQuery(orgQry);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            modCnt = recCnt.Count;
                            isEnabled = recCnt.Contains(organizationID);
                        }
                    }
                    if (!isEnabled)
                        modCnt = modCnt + 1;

                    if (modCnt > (sysSettings.MaxJabber))
                    {
                        myvrmEx = new myVRMException(674);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                modCnt = 0;
                isEnabled = false;
                if (enableLync == 1)
                {
                    orgQry = "SELECT od.OrgId FROM myVRM.DataLayer.OrgData od WHERE od.EnableLync=1";
                    recCnt = m_IOrgDAO.execQuery(orgQry);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            modCnt = recCnt.Count;
                            isEnabled = recCnt.Contains(organizationID);
                        }
                    }
                    if (!isEnabled)
                        modCnt = modCnt + 1;

                    if (modCnt > (sysSettings.MaxLync))
                    {
                        myvrmEx = new myVRMException(675);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                modCnt = 0;
                isEnabled = false;
                if (enableVidtel == 1)
                {
                    orgQry = "SELECT od.OrgId FROM myVRM.DataLayer.OrgData od WHERE od.EnableVidtel=1";
                    recCnt = m_IOrgDAO.execQuery(orgQry);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            modCnt = recCnt.Count;
                            isEnabled = recCnt.Contains(organizationID);
                        }
                    }
                    if (!isEnabled)
                        modCnt = modCnt + 1;

                    if (modCnt > (sysSettings.MaxVidtel))
                    {
                        myvrmEx = new myVRMException(676);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                //FB 2693 Ends

                //FB 2593 Start
                modCnt = 0;
                isEnabled = false;
                if (enableAdvReport == 1)
                {
                    orgQry = "SELECT od.OrgId FROM myVRM.DataLayer.OrgData od WHERE od.EnableAdvancedReport=1";
                    recCnt = m_IOrgDAO.execQuery(orgQry);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            modCnt = recCnt.Count;
                            isEnabled = recCnt.Contains(organizationID);
                        }
                    }
                    if (!isEnabled)
                        modCnt = modCnt + 1;

                    if (modCnt > (sysSettings.MaxAdvancedReport))
                    {
                        myvrmEx = new myVRMException(701);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
                //FB 2593 End

                if (mode == 1)
                {
                    m_IOrgSettingDAO.clearFetch();
                    orgInfo = m_IOrgSettingDAO.GetByOrgId(organizationID);

                    if (orgInfo == null)
                    {
                        obj.outXml = "<error>Invalid Organization.</error>";
                        return false;
                    }
                    exRoomlimit = orgInfo.RoomLimit;
                    exMaxVRooms = orgInfo.MaxVideoRooms;
                    exMaxNVRooms = orgInfo.MaxNonVideoRooms;
                    exMaxMCU = orgInfo.MCULimit;
                    exEndPoint = orgInfo.MaxEndpoints;
                    exUserLimit = orgInfo.UserLimit;
                    exExchUsrLimit = orgInfo.ExchangeUserLimit;
                    exDominoUsrLimit = orgInfo.DominoUserLimit;
                    exMobileUsrLimit = orgInfo.MobileUserLimit; //FB 1979
                    exWebexUsrLimit = orgInfo.WebexUserLimit; //ZD 100221
                    exBJNUsrLimit = orgInfo.BlueJeansUserLimit; //ZD 104021
                    //FB 2426 Start
                    extRoomLimit = orgInfo.GuestRoomLimit;
                    extGuestRoomPerUserLimit = orgInfo.GuestRoomPerUser;
                    //FB 2426 End
                    exMaxEnchancedMCU = orgInfo.MCUEnchancedLimit; //FB 2486
                    exMaxVMRRooms = orgInfo.MaxVMRRooms;//FB 2586
                    exPCUserLimit = orgInfo.PCUserLimit; //FB 2693
                    exMaxiControlRooms = orgInfo.MaxiControlRooms;//ZD 101098
                }
                
                sysFact = new vrmSystemFactory(ref obj);
                sysFact.GetRemainingLicense();

                if (orgVideoRoomLimit > (sysFact.remVideoRooms + exMaxVRooms))
                {
                    myvrmEx = new myVRMException(455);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if (orgNVRoomLimit > (sysFact.remNonVideoRooms + exMaxNVRooms))
                {
                    myvrmEx = new myVRMException(456);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //FB 2586 Start
                if (orgVMRRoomLimit > (sysFact.remVMRRooms + exMaxVMRRooms))
                {
                    myvrmEx = new myVRMException(655);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //FB 2586 End

                //ZD 101098 Start
                if (orgiControlRoomLimit > (sysFact.remiControlRooms + exMaxiControlRooms))
                {
                    myvrmEx = new myVRMException(738); //iControl Rooms limit exceeds VRM license
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //ZD 101098 End
                
                
                if (orgMCULimit > (sysFact.remMCU + exMaxMCU))
                {
                    myvrmEx = new myVRMException(457);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                //FB 2486
                if (orgMCUEnchancedLimit > (sysFact.remEnchancedMCU + exMaxEnchancedMCU))
                {
                   
                    myvrmEx = new myVRMException(624);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                
                if (orgEndPointLimit > (sysFact.remEndpoints + exEndPoint))
                {
                    myvrmEx = new myVRMException(458);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //ZD 101443
                if (sysFact.isLDAP == 0 && orgUserLimit > (sysFact.remUsers + exUserLimit))
                {
                    myvrmEx = new myVRMException(459);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //2426 Start
                if (orgGuestRoomLimit > (sysFact.remExtRoom + extRoomLimit))
                {
                    myvrmEx = new myVRMException(0);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if (orgGuestRoomPerUserLimit > orgGuestRoomLimit)
                {
                    myvrmEx = new myVRMException(0);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //2426 End
                if (sysFact.isLDAP == 0 && sysSettings.EnableCloudInstallation == 0) //ZD 104021
                {
                    if (orgExchangeUserLimit > (sysFact.remExchangeUsers + exExchUsrLimit))
                    {
                        myvrmEx = new myVRMException(460);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    if (orgMobileUserLimit > (sysFact.remMobileUsers + exMobileUsrLimit)) //FB 1979
                    {
                        myvrmEx = new myVRMException(526);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    if (orgDominoUserLimit > (sysFact.remDominoUsers + exDominoUsrLimit))
                    {
                        myvrmEx = new myVRMException(461);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    if (orgWebexUserLimit > (sysFact.remWebexusr + exWebexUsrLimit)) //ZD 100221 WebEx
                    {
                        myvrmEx = new myVRMException(718);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    //ZD 104021 BJN start
                    if (sysSettings.MaxBlueJeansUsers > 0) //if not unlimited BJN user license
                    {
                        if (orgBJNUserLimit > (sysFact.remBJNUsers + exBJNUsrLimit))
                        {
                            myvrmEx = new myVRMException(677);
                            obj.outXml = myvrmEx.FetchErrorMsg();
                            return false;
                        }
                    }
                    //ZD 104021 BJN end
                    //FB 2693 Starts
                    if (orgPcUserLimit > (sysFact.remPCUsers + exPCUserLimit))
                    {
                        myvrmEx = new myVRMException(682);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                }
               
                //FB 2693 Ends
                //ZD 100634 start 
                //ZD 101443
                if (sysFact.isLDAP == 0 && (orgExchangeUserLimit > orgUserLimit || orgDominoUserLimit > orgUserLimit || orgMobileUserLimit > orgUserLimit || orgPcUserLimit > orgUserLimit || orgWebexUserLimit > orgUserLimit || orgBJNUserLimit > orgUserLimit)) //ZD 104021
                {
                    myvrmEx = new myVRMException(715);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                //ZD 100634 End
                if (mode == 1)
                {
                                       
                    int VideormCnt = 0; //Video Rooms
                    string orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE vo.Disabled=0 and vo.IsVMR =0 and vo.Extroom=0 and vo.VideoAvailable=2 and vo.RoomCategory = 1 and vo.isPublic = 0 and  vo.orgId = '" + organizationID.ToString() + "'"; //FB 1744 //FB 2426 //FB 2594 //FB 2586 //FB 2694
                    recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            if (recCnt[0] != null)
                            {
                                if (recCnt[0].ToString() != "")
                                    int.TryParse(recCnt[0].ToString(), out VideormCnt);
                            }
                        }
                    }
                    if (orgVideoRoomLimit < VideormCnt)
                    {
                        myvrmEx = new myVRMException(463);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    recCnt = null;

                    int nonVdrmCnt = 0; //Non Video rooms
                    orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE  vo.Disabled=0 and vo.IsVMR =0 and vo.VideoAvailable < 2 and vo.RoomCategory = 1 and vo.isPublic = 0 and  vo.orgId = '" + organizationID.ToString() + "'"; //FB 1744 //FB 2594 //FB 2586 //FB 2694
                    recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            if (recCnt[0] != null)
                            {
                                if (recCnt[0].ToString() != "")
                                    int.TryParse(recCnt[0].ToString(), out nonVdrmCnt);
                            }
                        }
                    }
                    if (orgNVRoomLimit < nonVdrmCnt)
                    {
                        myvrmEx = new myVRMException(464);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    recCnt = null;

                    //FB 2694
                    int VCrmCnt = 0; //VC Hotdesking Room
                    orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE vo.Disabled=0 and vo.IsVMR =0 and vo.Extroom=0 and vo.VideoAvailable = 2 and vo.RoomCategory = 4 and vo.isPublic = 0 and  vo.orgId = '" + organizationID.ToString() + "'"; //FB 1744 //FB 2426 //FB 2594 //FB 2586 //FB 2694
                    recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            if (recCnt[0] != null)
                            {
                                if (recCnt[0].ToString() != "")
                                    int.TryParse(recCnt[0].ToString(), out VCrmCnt);
                            }
                        }
                    }
                    if (orgVCHotRoomLimit < VCrmCnt)
                    {
                        myvrmEx = new myVRMException(695);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    recCnt = null;

                    int ROrmCnt = 0; //RO Hotdesking Room
                    orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE vo.Disabled=0 and vo.IsVMR =0 and vo.Extroom=0 and vo.VideoAvailable < 2 and vo.RoomCategory = 4 and vo.isPublic = 0 and  vo.orgId = '" + organizationID.ToString() + "'"; //FB 1744 //FB 2426 //FB 2594 //FB 2586
                    recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            if (recCnt[0] != null)
                            {
                                if (recCnt[0].ToString() != "")
                                    int.TryParse(recCnt[0].ToString(), out ROrmCnt);
                            }
                        }
                    }
                    if (orgROHotRoomLimit < ROrmCnt)
                    {
                        myvrmEx = new myVRMException(692);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    recCnt = null;

                    int mcucnt = 0; //Active MCUs
                    orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmMCU vo WHERE  vo.deleted = 0 and vo.orgId = '" + organizationID.ToString() + "'";
                    recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            if (recCnt[0] != null)
                            {
                                if (recCnt[0].ToString() != "")
                                    int.TryParse(recCnt[0].ToString(), out mcucnt);
                            }
                        }
                    }
                    if (orgMCULimit < mcucnt)
                    {
                        myvrmEx = new myVRMException(465);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    int endpointcnt = 0; //End Points
                    orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmEndPoint vo WHERE vo.deleted=0 and vo.isDefault=1 and vo.Extendpoint=0 and vo.PublicEndPoint = 0 and vo.orgId='" + organizationID.ToString() + "'"; //FB 2426 //FB 2594
                    recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            if (recCnt[0] != null)
                            {
                                if (recCnt[0].ToString() != "")
                                    int.TryParse(recCnt[0].ToString(), out endpointcnt);
                            }
                        }
                    }
                    if (orgEndPointLimit < endpointcnt)
                    {
                        myvrmEx = new myVRMException(466);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }

                    int usrcnt = 0; //Active Users
                    orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmUser vo WHERE  vo.companyId = '" + organizationID.ToString() + "'";
                    recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            if (recCnt[0] != null)
                            {
                                if (recCnt[0].ToString() != "")
                                    int.TryParse(recCnt[0].ToString(), out usrcnt);
                            }
                        }
                    }
                    //ZD 101443
                    if (sysFact.isLDAP == 0 && orgUserLimit < usrcnt)
                    {
                        myvrmEx = new myVRMException(467);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    //FB 2586 Start
                    int VMRCnt = 0; //VMR Rooms
                    orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE vo.Disabled=0 and vo.Extroom=0 and vo.IsVMR =1 and vo.VideoAvailable=2 and vo.RoomCategory = 2 and vo.orgId = '" + organizationID.ToString() + "'";//FB 2694
                    recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            if (recCnt[0] != null)
                            {
                                if (recCnt[0].ToString() != "")
                                    int.TryParse(recCnt[0].ToString(), out VMRCnt);
                            }
                        }
                    }
                    if (orgVMRRoomLimit < VMRCnt)
                    {
                        //obj.outXml = "<error>Please deactivate VMR rooms to reduce the count.As there are more active VMR rooms.</error>";
                        myvrmEx = new myVRMException(657);
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    recCnt = null;
                    //FB 2586 End


                    //ZD 101098 Start
                    int iControlCnt = 0; //Validating the iControl Rooms
                    orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE vo.Disabled=0 and vo.IsVMR =0 and vo.Extroom=0 and vo.VideoAvailable=2 and vo.RoomCategory = 1 and vo.isPublic = 0 and vo.iControl = 1 and  vo.orgId = '" + organizationID.ToString() + "'"; 
                    recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                    if (recCnt != null)
                    {
                        if (recCnt.Count > 0)
                        {
                            if (recCnt[0] != null)
                            {
                                if (recCnt[0].ToString() != "")
                                    int.TryParse(recCnt[0].ToString(), out iControlCnt);
                            }
                        }
                    }
                    if (orgiControlRoomLimit < iControlCnt)
                    {
                        
                        myvrmEx = new myVRMException(733);//Please deactivate iControl rooms to reduce the count.As there are more active iControl rooms
                        obj.outXml = myvrmEx.FetchErrorMsg();
                        return false;
                    }
                    recCnt = null;
                    //ZD 101098 End

                    sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);
                    //if (enableBJ == 0) //ZD 103550
                    //{
                    //    sqlCon.OpenConnection();

                    //    sqlCon.OpenTransaction();

                    //    strSQL = "Delete from Usr_PCDetails_D where PCId= 1 and userid in (Select userid from Usr_List_D where companyId = + " + organizationID.ToString() + ")";
                    //    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    //    sqlCon.CommitTransaction();

                    //    sqlCon.CloseConnection();
                    //}
                    if (enableJabber == 0)
                    {
                        sqlCon.OpenConnection();

                        sqlCon.OpenTransaction();

                        strSQL = "Delete from Usr_PCDetails_D where PCId= 2 and userid in (Select userid from Usr_List_D where companyId = + " + organizationID.ToString() + ")";
                        strExec = sqlCon.ExecuteNonQuery(strSQL);

                        sqlCon.CommitTransaction();

                        sqlCon.CloseConnection();
                    }
                    if (enableLync == 0)
                    {
                        sqlCon.OpenConnection();

                        sqlCon.OpenTransaction();

                        strSQL = "Delete from Usr_PCDetails_D where PCId= 3 and userid in (Select userid from Usr_List_D where companyId = + " + organizationID.ToString() + ")";
                        strExec = sqlCon.ExecuteNonQuery(strSQL);

                        sqlCon.CommitTransaction();

                        sqlCon.CloseConnection();
                    }
                    if (enableVidtel == 0)
                    {
                        sqlCon.OpenConnection();

                        sqlCon.OpenTransaction();

                        strSQL = "Delete from Usr_PCDetails_D where PCId= 4 and userid in (Select userid from Usr_List_D where companyId = + " + organizationID.ToString() + ")";
                        strExec = sqlCon.ExecuteNonQuery(strSQL);

                        sqlCon.CommitTransaction();

                        sqlCon.CloseConnection();
                    }
                    //FB 2693 Ends
                }
                return true;
            }
            catch (Exception ex)
            {
                sqlCon.RollBackTransaction();
                m_log.Error("sytemException", ex);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }

        #endregion

        #region DeleteOrganizationProfile
        ///<summary>
        ///DeleteOrganizationProfile
        ///</summary>
        ///<param name="obj"></param>
        ///<returns></returns>
        public bool DeleteOrganizationProfile(ref vrmDataObject obj)
        {
            bool bRet = true;
            int i = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetOrganizationList/userID");
                string userId = node.InnerXml.ToString();

                node = xd.SelectSingleNode("//GetOrganizationList/orgId");
                string organizationId = node.InnerXml.ToString();

                organizationID = 0;
                int.TryParse(organizationId, out organizationID);
                if (organizationID < 11)
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                if (organizationID == 11)
                {
                    myvrmEx = new myVRMException(468);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                bRet = IsUserExists();
                if (bRet)
                {
                    myvrmEx = new myVRMException(469);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                bRet = IsRoomExists();
                if (bRet)
                {
                    myvrmEx = new myVRMException(470);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                bRet = IsMCUExists();
                if (bRet)
                {
                    myvrmEx = new myVRMException(471);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                vrmOrganization vrmOrgn = m_IOrgDAO.GetById(organizationID);
                if (vrmOrgn == null)
                    bRet = false;

                m_IOrgDAO.Delete(vrmOrgn);

                orgInfo = m_IOrgSettingDAO.GetByOrgId(organizationID);
                m_IOrgSettingDAO.Delete(orgInfo);

                sysTechData orgTechData = m_ISysTechDAO.GetTechByOrgId(organizationID);
                m_ISysTechDAO.Delete(orgTechData);

                //FB 2506 Starts
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", organizationID));
                List<vrmMessage> ConfMsg = m_IMessageDao.GetByCriteria(criterionList);
                for (i = 0; i < ConfMsg.Count; i++)
                {
                    m_IMessageDao.Delete(ConfMsg[i]);
                }
                //FB 2506 Ends
                //ZD 101176 Starts
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", organizationID.ToString()));
                List<vrmDeptCustomAttr> customAttlist1 = m_IDeptCustDAO.GetByCriteria(criterionList);
                List<VrmCustomLanguage> custLangTitle = null;
                for (i = 0; i < customAttlist1.Count; i++)
                {
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("CustomAttributeID", customAttlist1[i].CustomAttributeId));
                    custLangTitle = m_ICustomAttrLangDAO.GetByCriteria(criterionList);
                    for (int x = 0; x < custLangTitle.Count; x++)
                        m_ICustomAttrLangDAO.Delete(custLangTitle[x]);
                    
                    m_IDeptCustDAO.Delete(customAttlist1[i]);
                }
                //FB 101176 Ends

                //ZD 101602 Starts
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", organizationID));
                List<vrmLDAPConfig> ldapSettingsList = m_ILDAPConfigDAO.GetByCriteria(criterionList);
                if (ldapSettingsList != null && ldapSettingsList.Count > 0)
                {
                    for (int x = 0; x < ldapSettingsList.Count; x++)
                        m_ILDAPConfigDAO.Delete(ldapSettingsList[x]);
                }
                //ZD 101602 End
                bRet = true;

                if (bRet)
                    obj.outXml = "<success>1</success>";
                else
                {
                    myvrmEx = new myVRMException(472);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                }

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }

        private bool IsUserExists()
        {
            try
            {
                List<ICriterion> userCriterion = new List<ICriterion>();
                userCriterion.Add(Expression.Eq("companyId", organizationID));
                List<vrmUser> userList = m_IuserDAO.GetByCriteria(userCriterion);

                if (userList != null)
                {
                    if (userList.Count > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        private bool IsRoomExists()
        {
            try
            {
                List<ICriterion> criterionLst = new List<ICriterion>();
                criterionLst.Add(Expression.Eq("orgId", organizationID));
                criterionLst.Add(Expression.Eq("Disabled", 0));
                List<vrmRoom> roomList = m_IRoomDAO.GetByCriteria(criterionLst);

                if (roomList != null)
                {
                    if (roomList.Count > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        private bool IsMCUExists()
        {
            try
            {
                List<ICriterion> criterionLst = new List<ICriterion>();
                criterionLst.Add(Expression.Eq("orgId", organizationID));
                criterionLst.Add(Expression.Eq("deleted", 0));
                List<vrmMCU> mcuList = m_vrmMCU.GetByCriteria(criterionLst);

                if (mcuList != null)
                {
                    if (mcuList.Count > 0)
                    {
                        return true;
                    }
                }
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region SetPurgeDetails
        public bool SetPurgeDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            string message = "";
            EvtLog e_log = new EvtLog();
            string orgId = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;


                node = xd.SelectSingleNode("//PurgeDetails/organizationID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error><message>Invalid Organization ID</message></error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);

                Diagnostics diag = m_IDiagnosticsDAO.GetDiagnosticByOrgId(organizationID);
                if (diag == null)
                    diag = new Diagnostics();

                diag.OrgId = organizationID;

                node = xd.SelectSingleNode("//PurgeDetails/PurgeType");
                if (node != null && node.InnerXml.Trim() != "")
                {
                    diag.PurgeType = Convert.ToInt32(node.InnerXml.Trim());

                    message = "Purge service settings scheduled to ";
                    if (node.InnerXml.ToString() != "" && node.InnerXml.ToString() != "0")
                    {
                        switch (node.InnerXml.ToString())
                        {
                            case "1":
                                message += "12:00 AM on daily basis.";
                                break;
                            case "2":
                                message += "every Friday at 12:00 AM on weekly basis.";
                                break;
                            case "3":
                                message += "every Last day of the Month at 12:00 AM.";
                                break;
                            default:
                                message += "12:00 AM on daily basis.";
                                break;
                        }
                    }
                }
                node = xd.SelectSingleNode("//PurgeDetails/UpdatedDate");
                if (node != null && node.InnerXml.Trim() != "")
                {
                    diag.UpdatedDate = Convert.ToDateTime(node.InnerXml.Trim());
                    message += "Last updated on " + diag.UpdatedDate.ToString("yyyy-MM-dd hh:mm tt");
                }

                node = xd.SelectSingleNode("//PurgeDetails/TimeofDay");
                if (node != null && node.InnerXml.Trim() != "")
                    diag.TimeofDay = Convert.ToDateTime(node.InnerXml.Trim());
                else
                    diag.TimeofDay = Convert.ToDateTime("12:00 PM");

                node = xd.SelectSingleNode("//PurgeDetails/ServiceStatus");
                if (node != null && node.InnerXml.Trim() != "")
                    diag.ServiceStatus = Convert.ToInt32(node.InnerXml.Trim());
                else
                    diag.ServiceStatus = diag.ServiceStatus;

                node = xd.SelectSingleNode("//PurgeDetails/PurgeData");
                if (node != null && node.InnerXml.Trim() != "")
                    diag.PurgeData = Convert.ToInt32(node.InnerXml.Trim());

                m_IDiagnosticsDAO.SaveOrUpdate(diag);
                e_log.LogEvent("Purge Service Settings updated Successfully. \n" + message);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region GetPurgeDetails
        public bool GetPurgeDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            string orgId = "";
            OrgOUTXML = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;


                node = xd.SelectSingleNode("//GetPurgeDetails/organizationID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);

                Diagnostics diag = m_IDiagnosticsDAO.GetDiagnosticByOrgId(organizationID);

                if (diag == null)
                    diag = new Diagnostics();
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OrgOUTXML, xSettings))
                {
                    xWriter.WriteStartElement("PurgeDetails");
                    xWriter.WriteElementString("PurgeType",diag.PurgeType.ToString());
                    xWriter.WriteElementString("DayofWeek",diag.DayofWeek.ToString());
                    xWriter.WriteElementString("UpdatedDate", diag.UpdatedDate.ToString());
                    xWriter.WriteElementString("TimeofDay",diag.TimeofDay.ToString());
                    xWriter.WriteElementString("ServiceStatus",diag.ServiceStatus.ToString());
                    xWriter.WriteElementString("PurgeData",diag.PurgeData.ToString());
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.Flush();
                }
                obj.outXml = OrgOUTXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region GetActiveOrgDetails
        public bool GetActiveOrgDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            string orgId = "";
            IList recCnt;
            OrgOUTXML = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetActiveOrgDetails/organizationID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error><message>Invalid Organization ID</message></error>";
                    return false;
                }
                
                int.TryParse(orgId, out organizationID);

                if (organizationID < 11)
                {
                    obj.outXml = "<error><message>Invalid Organization ID</message></error>";
                    return false;
                }

                //int rmCnt = 0; //Total rooms
                //string orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE  vo.Disabled = 0 and vo.orgId = '"+ organizationID.ToString() +"'";
                //recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                //if (recCnt != null)
                //{
                //    if (recCnt.Count > 0)
                //    {
                //        if (recCnt[0] != null)
                //        {
                //            if (recCnt[0].ToString() != "")
                //                int.TryParse(recCnt[0].ToString(), out rmCnt);
                //        }
                //    }
                //}
                //recCnt = null;

                int nonVdrmCnt = 0; //Non Video rooms
                string orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE  vo.Disabled=0 and vo.IsVMR=0 and vo.iControl=0 and vo.VideoAvailable < 2 and vo.RoomCategory !=4 and vo.isPublic = 0 and vo.orgId = '" + organizationID.ToString() + "'"; //FB 1744 //FB 2586 //FB 2694 //ZD 101098
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out nonVdrmCnt);
                        }
                    }
                }
                recCnt = null;

                int VideormCnt = 0; //Video Rooms
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE vo.Disabled=0 and vo.Extroom=0 and vo.IsVMR=0 and vo.iControl=0 and vo.VideoAvailable=2 and vo.RoomCategory !=4 and vo.isPublic = 0 and vo.orgId = '" + organizationID.ToString() + "'"; //FB 1744 //FB 2426 //FB 2586 //FB 2694 //ZD 101098
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out VideormCnt);
                        }
                    }
                }
                recCnt = null;

                //2426 Start
                int ExtRoomrmCnt = 0; //Video Rooms
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE vo.Disabled=0 and vo.VideoAvailable=2 and vo.RoomCategory = 3 and vo.Extroom=1 and vo.isPublic = 0 and  vo.orgId = '" + organizationID.ToString() + "'"; //FB 2694
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out ExtRoomrmCnt);
                        }
                    }
                }
                recCnt = null;
                //2426 End

                int mcucnt = 0;
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmMCU vo WHERE vo.deleted = 0 and vo.orgId = '" + organizationID.ToString() + "'";
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out mcucnt);
                        }
                    }
                }
                recCnt = null;

                int usrcnt = 0; //Total Users
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmUser vo WHERE  vo.companyId = '" + organizationID.ToString() + "'";
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out usrcnt);
                        }
                    }
                }
                recCnt = null;

                int BJNusrcnt = 0; //ZD 104021
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmUser vo WHERE vo.EnableBJNConf=1 and vo.companyId = '" + organizationID.ToString() + "'";
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out BJNusrcnt);
                        }
                    }
                }
                recCnt = null;

                int exusrcnt = 0; //Total Exchange Users
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmUser vo WHERE  vo.enableExchange=1 and vo.companyId='" + organizationID.ToString() + "'";
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out exusrcnt);
                        }
                    }
                }
                recCnt = null;

                int domusrcnt = 0; //Total Domino Users
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmUser vo WHERE  vo.enableDomino=1 and vo.companyId='" + organizationID.ToString() + "'";
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out domusrcnt);
                        }
                    }
                }
                recCnt = null;

                int mobusrcnt = 0; //Total Mobile Users FB 1979
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmUser vo WHERE  vo.enableMobile=1 and vo.companyId='" + organizationID.ToString() + "'";
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out mobusrcnt);
                        }
                    }
                }
                recCnt = null;

                int endpointcnt = 0; //End Points
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmEndPoint vo WHERE vo.deleted=0 and vo.isDefault=1 and vo.Extendpoint=0 and vo.PublicEndPoint = 0 and vo.orgId='" + organizationID.ToString() + "'";//FB 2426
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out endpointcnt);
                        }
                    }
                }
                recCnt = null;

                //2486 Start
                int MCUEnchancedCnt = 0;
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmMCU vo WHERE vo.deleted = 0 and vo.EnhancedMCU = 1  and vo.orgId = '" + organizationID.ToString() + "'"; //FB 2636
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out MCUEnchancedCnt);
                        }
                    }
                }
                recCnt = null;
                //2486 End

                //2586 Start VMR Rooms
                int VMRCnt = 0;
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE vo.Disabled=0 and vo.VideoAvailable=2 and vo.RoomCategory != 4 and vo.IsVMR=1 and vo.orgId = '" + organizationID.ToString() + "'";
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out VMRCnt);
                        }
                    }
                }
                recCnt = null;
                //2586 End


                //ZD 101098 START
                int iControlCnt = 0;
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmRoom vo WHERE vo.Disabled=0 and vo.iControl=1 and vo.VideoAvailable=2 and vo.orgId = '" + organizationID.ToString() + "'";
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out iControlCnt);
                        }
                    }
                }
                recCnt = null;
                //ZD 101098 END

                //FB 2693 Starts
                int pcusrcnt = 0;
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmUser vo WHERE  vo.enablePCUser=1 and vo.companyId='" + organizationID.ToString() + "'";
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out pcusrcnt);
                        }
                    }
                }
                recCnt = null;
                //FB 2693 Ends

                //ZD 100221 WebEx Starts
                int webexusrcnt = 0;
                orgActiveCnt = "SELECT count(*) FROM myVRM.DataLayer.vrmUser vo WHERE  vo.enableWebexUser=1 and vo.companyId='" + organizationID.ToString() + "'";
                recCnt = m_IOrgDAO.execQuery(orgActiveCnt);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out webexusrcnt);
                        }
                    }
                }
                recCnt = null;
                //ZD 100221 WebEx Ends

                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OrgOUTXML, xSettings))
                {
                    xWriter.WriteStartElement("GetActiveOrgDetails");
                    //obj.outXml += "<ActiveRoom>"+ rmCnt.ToString() +"</ActiveRoom>";
                    xWriter.WriteElementString("ActiveNonVideoRoom",nonVdrmCnt.ToString());
                    xWriter.WriteElementString("ActiveVideoRoom",VideormCnt.ToString());
                    xWriter.WriteElementString("ActiveMCU",mcucnt.ToString());
                    xWriter.WriteElementString("ActiveUsers",usrcnt.ToString());
                    xWriter.WriteElementString("ActiveExchangeUsers",exusrcnt.ToString());
                    xWriter.WriteElementString("ActiveDominoUsers",domusrcnt.ToString());
                    xWriter.WriteElementString("ActiveEndPoints",endpointcnt.ToString());
                    xWriter.WriteElementString("ActiveMobileUsers",mobusrcnt.ToString()); // FB 1979
                    xWriter.WriteElementString("ActiveGuestRooms",ExtRoomrmCnt.ToString()); // 2426
                    xWriter.WriteElementString("ActiveMCUEnchanced",MCUEnchancedCnt.ToString()); //FB 2486
                    xWriter.WriteElementString("ActiveVMRRooms",VMRCnt.ToString()); //FB 2586
                    xWriter.WriteElementString("ActiveiControlRooms", iControlCnt.ToString()); //ZD 101098
                    xWriter.WriteElementString("ActivePCUsers",pcusrcnt.ToString()); //FB 2693
                    xWriter.WriteElementString("ActiveWebexUsers", webexusrcnt.ToString()); //ZD 100221 WebEx     
                    xWriter.WriteElementString("ActiveBlueJeanUsers", BJNusrcnt.ToString());//ZD 104021
                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.Flush();
                }
                obj.outXml = OrgOUTXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region UpdateOrgImages - Added for Image Project
        /// <summary>
        /// UpdateOrgImages
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool UpdateOrgImages(ref vrmDataObject obj)
        {
            string orgId = "";
            vrmfact = new vrmFactory(ref obj);
            vrmImage imgObj = new vrmImage();
            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//UpdateOrgImages/organizationID");
                if (node != null)
                    orgId = node.InnerText;
                
                if (orgId == "")
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);
                if (organizationID < defaultOrgID)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                m_IOrgSettingDAO.clearFetch();

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingDAO.GetByOrgId(organizationID);
                

                if (orgInfo == null)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                string logoImage = "", lobytopimage = "", lobytophighimage = "", logoImageName = "", lobytopimagename = "", lobytophighimagename = "";//ZD 104387

                
                node = xd.SelectSingleNode("//UpdateOrgImages/logoimagename");
                if (node != null) //ZD 104387
                    logoImageName = node.InnerText.Trim();

                node = xd.SelectSingleNode("//UpdateOrgImages/logoimage");
                if (node != null) //ZD 104387
                    logoImage = node.InnerText.Trim();

                int imageid = 0;
                if (logoImage != "")
                {
                    DeleteImages(13);
                    imageid = SetOrgImage(logoImage, 13);
                }

                if (imageid > 0)
                {
                    orgInfo.LogoImageId = imageid;
                    orgInfo.LogoImageName = logoImageName;
                }

                 node = xd.SelectSingleNode("//UpdateOrgImages/lobytopimagename");
                 if (node != null) //ZD 104387
                    lobytopimagename = node.InnerText.Trim();
              
                 node = xd.SelectSingleNode("//UpdateOrgImages/lobytopimage");
                 if (node != null) //ZD 104387
                    lobytopimage = node.InnerText.Trim();
                
                int image1id = 0;
                if (lobytopimage != "")
                {
                    DeleteImages(11);
                    image1id = SetOrgImage(lobytopimage, 11);
                }

                if (image1id > 0)
                {
                    orgInfo.LobytopImageId = image1id;
                    orgInfo.LobytopImageName = lobytopimagename;
                }

                node = xd.SelectSingleNode("//UpdateOrgImages/lobyhighimgname");
                if (node != null) //ZD 104387
                    lobytophighimagename = node.InnerText.Trim();

                
                node = xd.SelectSingleNode("//UpdateOrgImages/lobytophighimage");
                if (node != null) //ZD 104387
                    lobytophighimage = node.InnerText.Trim();

                int image2id = 0;
                if (lobytophighimage != "")
                {
                    DeleteImages(12);
                    image2id = SetOrgImage(lobytophighimage, 12);
                }

                if (image2id > 0)
                {
                    orgInfo.LobytopHighImageId = image2id;
                    orgInfo.LobytopHighImageName = lobytophighimagename;
                }

                //Css Module starts...
                if(xd.SelectSingleNode("//UpdateOrgImages/MirrorCssXml") != null)
                    node = xd.SelectSingleNode("//UpdateOrgImages/MirrorCssXml");
                    if (node != null)
                        logoImage = node.InnerText.Trim();

                    
                    if (logoImage != "")
                    {
                        DeleteImages(16);
                        imageid = SetOrgImage(logoImage, 16); //Mirror XML
                    }

                    if (imageid > 0)
                    {
                        orgInfo.MirrorCssId = imageid;
                    }

                    node = xd.SelectSingleNode("//UpdateOrgImages/ArtifactsXml");
                    if (node != null)
                        logoImage = node.InnerText.Trim();

                    
                    if (logoImage != "")
                    {
                        DeleteImages(17);
                        imageid = SetOrgImage(logoImage, 17); //Artifacts - Menu XML
                    }

                    if (imageid > 0)
                    {
                        orgInfo.ArtifactsCssId = imageid;
                    }
                   
                //Css Module ends...

                m_IOrgSettingDAO.Update(orgInfo);

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        
        #endregion
        
        #region Check API Enable
        /// <summary>
        /// Check API Enable
        /// Method added for New License
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        // InXML:
        //<organization><ID></ID></organization>
        public bool CheckAPIEnable(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                string orgId = "";

                node = xd.SelectSingleNode("//organization/ID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);

                if(organizationID < defaultOrgID)
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }

                m_IOrgSettingDAO.clearFetch();
                OrgData orgdata = m_IOrgSettingDAO.GetByOrgId(organizationID);

                if (orgdata.EnableAPI == 1)
                    bRet = true;
                else
                    bRet = false;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            return bRet;
        }
        #endregion
        //Css Module Start

        #region SetTextChangeXML - Added for CSS Module
        /// <summary>
        /// SetTextChangeXML
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetTextChangeXML(ref vrmDataObject obj)
        {
            string orgId = "";
            vrmfact = new vrmFactory(ref obj);
            vrmImage imgObj = new vrmImage();
            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//SetTextChangeXML/organizationID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);
                if (organizationID < defaultOrgID)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                m_IOrgSettingDAO.clearFetch();

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingDAO.GetByOrgId(organizationID);


                if (orgInfo == null)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                string logoImage = "";
                int imageid = 0;

                if (xd.SelectSingleNode("//SetTextChangeXML/TextChangeXml") != null) //Text Change XML - Menu XML
                    node = xd.SelectSingleNode("//SetTextChangeXML/TextChangeXml");

                if (node != null)
                    logoImage = node.InnerText.Trim();


                if (logoImage != "")
                {
                    DeleteImages(18);
                    imageid = SetOrgImage(logoImage, 18);
                }

                if (imageid > 0)
                {
                    orgInfo.TextchangeId = imageid;
                }
               
                m_IOrgSettingDAO.Update(orgInfo);

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }

        #endregion

        #region SetDefaultCSSXML - Added for CSS Module
        /// <summary>
        /// SetDefaultCSSXML
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetDefaultCSSXML(ref vrmDataObject obj)
        {
            string orgId = "";
            vrmfact = new vrmFactory(ref obj);
            vrmImage imgObj = new vrmImage();
            vrmImage siteObj = null;
            OrgData orgdt = null;
            int i =0;
            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//SetDefaultCSSXML/organizationID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);
                if (organizationID < defaultOrgID)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                m_IOrgSettingDAO.clearFetch();

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingDAO.GetByOrgId(organizationID);


                if (orgInfo == null)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                string logoImage = "";
                int imageid = 0;

                if (xd.SelectSingleNode("//SetDefaultCSSXML/DefaultCSSXml") != null) //Text Change XML - Menu XML
                    node = xd.SelectSingleNode("//SetDefaultCSSXML/DefaultCSSXml");

                if (node != null)
                    logoImage = node.InnerText.Trim();


                if (logoImage != "")
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("AttributeType", 19));
                    List<vrmImage> siteLogos = m_IImageDAO.GetByCriteria(criterionList);

                    if (siteLogos != null)
                    {
                        for (i = 0; i < siteLogos.Count; i++)
                        {
                            siteObj = null;
                            siteObj = siteLogos[i];
                            m_IImageDAO.Delete(siteObj);
                        }
                    }

                    imageid = SetOrgImage(logoImage, 19);
                    
                }

                m_IOrgSettingDAO.clearFetch();
                List<OrgData> allOrgs = m_IOrgSettingDAO.GetAll();
                if (allOrgs != null)
                {
                    if (allOrgs.Count > 0)
                    {
                        for (i = 0; i < allOrgs.Count; i++)
                        {
                            orgdt = null;
                            orgdt = allOrgs[i];
                            orgdt.DefaultCssId = imageid;
                        }
                        m_IOrgSettingDAO.SaveOrUpdateList(allOrgs);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }

        #endregion

        #region Delete Images
        /// <summary>
        /// Delete Images
        /// </summary>
        /// <param name="attributeID"></param>
        /// <returns></returns>
        private bool DeleteImages(int attributeID)
        {
            vrmImage siteObj = null;
            int i = 0;
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("AttributeType", attributeID));
                criterionList.Add(Expression.Eq("OrgId", organizationID));

                List<vrmImage> siteLogos = m_IImageDAO.GetByCriteria(criterionList);

                if (siteLogos != null)
                {
                    for (i = 0; i < siteLogos.Count; i++)
                    {
                        siteObj = null;
                        siteObj = siteLogos[i];
                        m_IImageDAO.Delete(siteObj);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in DeleteImages", ex);
                return false;
            }
        }
        #endregion

        //Css Module ends...

        //Method added for Login Management
        #region GetOrgImages
        /// <summary>
        /// GetOrgImages
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetOrgImages(ref vrmDataObject obj)
        {
            string orgId = "";
            vrmfact = new vrmFactory(ref obj);
            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetOrgImages/organizationID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);
                if (organizationID < defaultOrgID)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                m_IOrgSettingDAO.clearFetch();

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingDAO.GetByOrgId(organizationID);

                if (orgInfo == null)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                
                StringBuilder orgImg = new StringBuilder(); //FB 1820 start
                
                orgImg.Append("<GetOrgImages>");
                orgImg.Append("<Logo>" + GetImage(orgInfo.LogoImageId) + "</Logo>");
                orgImg.Append("<LogoName>" + orgInfo.LogoImageName + "</LogoName>");
                orgImg.Append("<LobyTop>" + GetImage(orgInfo.LobytopImageId) + "</LobyTop>");
                orgImg.Append("<LobyTopName>" + orgInfo.LobytopImageName + "</LobyTopName>");
                orgImg.Append("<LobyTopHigh>" + GetImage(orgInfo.LobytopHighImageId) + "</LobyTopHigh>");
                orgImg.Append("<LobyTopHighName>" + orgInfo.LobytopHighImageName + "</LobyTopHighName>");
                orgImg.Append("</GetOrgImages>");
                
                obj.outXml = orgImg.ToString(); //FB 1820 end

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

        //Method addeded for FB 1860
        #region Get Org Emails
        public bool GetOrgEmails(ref vrmDataObject obj)
        {
            bool bRet = true;
            int orgID = 0;
            string orgid = "";
            try
            {


                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerXml.Trim();

                int.TryParse(orgid, out orgID);

                if (orgID <= 0)
                {
                    throw new Exception("Invalid Organization");
                }

                OrgData orgdt = m_IOrgSettingDAO.GetByOrgId(orgID);

                m_emailFactory = new emailFactory(ref obj);

                m_emailFactory.GetBlockedEmails(ref orgdt, ref obj);


            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        //FB 1861 //FB 2052 start
        #region Get Organization Holidays
        /// <summary>
        /// Get Organization Holidays
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetOrgHolidays(ref vrmDataObject obj)
        {
            string orgId = "",color = "";
            StringBuilder orgDt = new StringBuilder();
            int priority = 0,tpeID = 0, hldys =0;
            holidaysType sysHolidayType = null;
            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetOrgHolidays/organizationID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);

                Boolean isNew = false;
                node = xd.SelectSingleNode("//GetOrgHolidays/typeID");
                if (node != null)
                    if (node.InnerText.ToLower().Trim() != "new")
                        int.TryParse(node.InnerText, out tpeID);
                    else
                        isNew = true;

                orgDt.Append("<SystemHolidays>");
                m_IHolidayDAO = m_OrgDAO.GetHolidaysDao();
                holidays sysHoliday = null;
                IList<holidays> sysHolidays = null;
                if (!isNew)
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("OrgId", organizationID));
                    if (tpeID > 0)
                        criterionList.Add(Expression.Eq("HolidayType", tpeID));

                    m_IHolidayDAO.clearOrderBy();
                    m_IHolidayDAO.addOrderBy(Order.Asc("Date"));
                    sysHolidays = m_IHolidayDAO.GetByCriteria(criterionList);

                    if (tpeID > 0)
                    {
                        m_IHolidayDAOType = m_OrgDAO.GetHolidaysTypeDAO();
                        sysHolidayType = m_IHolidayDAOType.GetHolidayTypebyID(tpeID);
                    }

                    if (sysHolidays.Count > 0)
                    {
                        if (tpeID > 0)
                        {
                            orgDt.Append("<HolidayDescription>" + sysHolidayType.HolidayDescription + "</HolidayDescription>");
                            orgDt.Append("<Color>" + sysHolidayType.Color + "</Color>");
                        }

                        orgDt.Append("<Holidays>");

                        for (hldys = 0; hldys < sysHolidays.Count; hldys++)
                        {
                            sysHoliday = sysHolidays[hldys];
                            orgDt.Append("<Holiday>");
                            orgDt.Append("<Date>" + sysHoliday.Date.ToString("MM/dd/yyyy") + "</Date>");
                            orgDt.Append("<HolidayType>" + sysHoliday.HolidayType + "</HolidayType>");

                            if (tpeID == 0 || tpeID != sysHoliday.HolidayType)
                            {
                                tpeID = sysHoliday.HolidayType;

                                m_IHolidayDAOType = m_OrgDAO.GetHolidaysTypeDAO();
                                sysHolidayType = m_IHolidayDAOType.GetHolidayTypebyID(sysHoliday.HolidayType);
                                if (sysHolidayType != null)
                                {
                                    priority = sysHolidayType.Priority;
                                    color = sysHolidayType.Color;
                                }
                            }
                            orgDt.Append("<Color>" + color + "</Color>");
                            orgDt.Append("<Priority>" + priority.ToString() + "</Priority>");
                            orgDt.Append("<HolidayDesc>" + sysHolidayType.HolidayDescription + "</HolidayDesc>");
                            orgDt.Append("</Holiday>");

                        }

                        orgDt.Append("</Holidays>");
                    }
                }

                orgDt.Append("</SystemHolidays>");
                obj.outXml = orgDt.ToString();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region Set Organization Holidays
        /// <summary>
        /// This is quick solution for importing holidays from a Xcel spread sheet and not fool proof
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetOrgHolidays(ref vrmDataObject obj)
        {
            string orgId = "";
            StringBuilder orgDt = new StringBuilder();
            bool isEdit = false;
            holidays sysHoliday = null;
            int hldy = 0;
            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//SetOrgHolidays/organizationID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);

                node = xd.SelectSingleNode("//SetOrgHolidays/SystemHolidays/HolidayName");
                string hName = "";
                if (node != null)
                    hName = node.InnerText;

                node = xd.SelectSingleNode("//SetOrgHolidays/SystemHolidays/Color");
                string color = "";
                if (node != null)
                    color = node.InnerText;

                node = xd.SelectSingleNode("//SetOrgHolidays/SystemHolidays/HolidayType");
                int holidayTypeID = 0;
                if (node != null)
                {
                    int.TryParse(node.InnerText, out holidayTypeID);

                    if (holidayTypeID > 0)
                        isEdit = true;
                }

                if (hName.Trim() != "" && color != "")
                {
                    m_IHolidayDAOType = m_OrgDAO.GetHolidaysTypeDAO();

                    List<ICriterion> criterionLst = new List<ICriterion>();
                    criterionLst.Add(Expression.Eq("HolidayDescription", hName.Trim()));
                    criterionLst.Add(Expression.Eq("OrgId", organizationID));
                    if (holidayTypeID > 0)
                        criterionLst.Add(Expression.Not(Expression.Eq("HolidayType", holidayTypeID)));
                    List<holidaysType> sysHolidayType = m_IHolidayDAOType.GetByCriteria(criterionLst);
                    if (sysHolidayType.Count > 0)
                    {
                        myVRMException myVRMEx = new myVRMException(488);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }

                    if (holidayTypeID <= 0)
                    {
                        string qString = "select max(a.Priority) from myVRM.DataLayer.holidaysType a";
                        IList list = m_IHolidayDAOType.execQuery(qString);
                        string sMax;
                        if (list[0] != null)
                            sMax = list[0].ToString();
                        else
                            sMax = "0";

                        int id = int.Parse(sMax);
                        id = id + 1;

                        holidaysType HolidayType = new holidaysType();

                        HolidayType.Color = color;
                        HolidayType.HolidayDescription = hName;
                        HolidayType.OrgId = organizationID;
                        HolidayType.Priority = id;

                        m_IHolidayDAOType.Save(HolidayType);

                        holidayTypeID = HolidayType.HolidayType;
                    }
                    else
                    {
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("HolidayType", holidayTypeID));
                        sysHolidayType = m_IHolidayDAOType.GetByCriteria(criterionList);
                        sysHolidayType[0].Color = color;
                        sysHolidayType[0].HolidayDescription = hName;
                        sysHolidayType[0].OrgId = organizationID;

                        m_IHolidayDAOType.SaveOrUpdate(sysHolidayType[0]);
                    }
                }

                XmlNodeList hldys = xd.SelectNodes("//SetOrgHolidays/SystemHolidays/Holidays/Holiday");
                if (m_IHolidayDAO == null)
                    m_IHolidayDAO = m_OrgDAO.GetHolidaysDao();

                List<holidays> sysHolidays = null;

                if (isEdit == true && holidayTypeID > 0)
                {
                    if (!DeleteHolidayColordays(holidayTypeID))
                        return false;
                }

                if (hldys.Count > 0)
                {
                    sysHolidays = new List<holidays>();
                    for (hldy = 0; hldy < hldys.Count; hldy++)
                    {
                        sysHoliday = new holidays();
                        sysHoliday.Date = DateTime.Parse(hldys[hldy].SelectSingleNode("Date").InnerText);
                        int hType = 0;
                        if (holidayTypeID <= 0)
                            int.TryParse(hldys[hldy].SelectSingleNode("HolidayType").InnerText, out hType);
                        else
                            hType = holidayTypeID;

                        sysHoliday.HolidayType = hType;
                        sysHoliday.OrgId = organizationID;
                        m_IHolidayDAO.Save(sysHoliday);
                    }
                }

                obj.outXml = "<success>1</success>";

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion
        //FB 1861//FB 2052 End

        //FB 1926
        #region Send Automated Reminders
        /// <summary>
        /// SendAutomatedReminders
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SendAutomatedReminders(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<Reminder> reminders = null;
            int org = 0;
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("deleted", 0));
                List<vrmOrganization> vrmOrg = m_IOrgDAO.GetByCriteria(criterionList);

                if (vrmOrg != null)
                {
                    if (vrmOrg.Count > 0)
                    {
                        m_emailFactory = new emailFactory(ref obj);
                        GetReminderList(ref reminders);
                        for (org = 0; org < vrmOrg.Count; org++)
                        {
                            m_emailFactory.SendAutomatedReminders(vrmOrg[org].orgId, ref reminders);
                            m_emailFactory.SendSurveyMail(vrmOrg[org].orgId);//FB 2348
                        }
                    }
                }
                obj.outXml = "<success>1</success>";
                
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = myVRMException.toXml(e.Message);
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region Get Reminder List
        /// <summary>
        /// Get Reminder List
        /// </summary>
        private void GetReminderList(ref List<Reminder> reminderList)
        {
            try
            {
                reminderList = new List<Reminder>();
                Reminder reminder = new Reminder();
                reminder.ReminderType = 1;
                reminder.ReminderLowerLimit = 14;
                reminder.ReminderUpperLimit = 16;
                reminderList.Add(reminder);
                reminder = new Reminder();
                reminder.ReminderType = 2;
                reminder.ReminderLowerLimit = 55;
                reminder.ReminderUpperLimit = 65;
                reminderList.Add(reminder);
                reminder = new Reminder();
                reminder.ReminderType = 4;
                reminder.ReminderLowerLimit = 1380;
                reminder.ReminderUpperLimit = 1500;
                reminderList.Add(reminder);
                reminder = new Reminder();
                reminder.ReminderType = 8;
                reminder.ReminderLowerLimit = 10020;
                reminder.ReminderUpperLimit = 10140;
                reminderList.Add(reminder);


            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        #endregion

       //FB 2027 - Start
       #region GetLogPreferences
        /// <summary>
        /// GetLogPreferences (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetLogPreferences(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx  = new myVRMException();
            OrgOUTXML = new StringBuilder();
            int userid = 0, i = 0;
            vrmLogPref lp = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//getLogPreferences/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                node = xd.SelectSingleNode("//getLogPreferences/userID");
                List<vrmLogPref> lp_List = vrmGen.getLogPref();
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OrgOUTXML, xSettings))
                {
                    xWriter.WriteStartElement("getLogPreferences");
                    for (i = 0; i < lp_List.Count; i++)
                    {
                        lp = null;
                        lp = lp_List[i];
                        xWriter.WriteStartElement("module");
                        xWriter.WriteElementString("moduleName",lp.modulename.ToString());
                        xWriter.WriteElementString("logLevel",lp.loglevel.ToString());
                        xWriter.WriteElementString("logLife",lp.loglife.ToString());
                        xWriter.WriteFullEndElement(); //ZD 101086
                    }

                    xWriter.WriteFullEndElement(); //ZD 101086
                    xWriter.Flush();
                }
                obj.outXml = OrgOUTXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region AppendOrgDetails

        public void AppendOrgDetails(ref StringBuilder outXml, int organizationID)
        {
            try
            {
                OrgData vrmOrgSettings = m_IOrgSettingDAO.GetById(organizationID);
                if (vrmOrgSettings == null)
                {
                    outXml.Append("<error>Invalid Organization ID</error>");
                }
                else
                {
                    outXml.Append("<enableRecurrance>" + vrmOrgSettings.recurEnabled + "</enableRecurrance>");
                    outXml.Append("<realtimeType>" + vrmOrgSettings.RealtimeStatus.ToString() + "</realtimeType>");
                    outXml.Append("<dialoutEnabled>" + vrmOrgSettings.DialOut.ToString() + "</dialoutEnabled>");
                    outXml.Append("<defaultPublicEnabled>" + vrmOrgSettings.DefaultToPublic.ToString() + "</defaultPublicEnabled>");
                    outXml.Append("<dynamicInviteEnabled>" + vrmOrgSettings.dynamicinviteenabled.ToString() + "</dynamicInviteEnabled>");
                    outXml.Append("<doubleBookingEnabled>" + vrmOrgSettings.doublebookingenabled.ToString() + "</doubleBookingEnabled>");
                    outXml.Append("<p2pConfEnabled>" + vrmOrgSettings.Connect2.ToString() + "</p2pConfEnabled>");
                    outXml.Append("<DefaultConferenceType>" + vrmOrgSettings.DefaultConferenceType.ToString() + "</DefaultConferenceType>");
                    outXml.Append("<RFIDTagValue>" + vrmOrgSettings.RFIDTagValue.ToString() + "</RFIDTagValue>");//FB 2724
                    outXml.Append("<iControlTimeout>" + vrmOrgSettings.iControlTimeout.ToString() + "</iControlTimeout>");//FB 2724
                    outXml.Append("<EnableRoomConference>" + vrmOrgSettings.EnableRoomConference.ToString() + "</EnableRoomConference>");
                    outXml.Append("<EnableAudioVideoConference>" + vrmOrgSettings.EnableAudioVideoConference.ToString() + "</EnableAudioVideoConference>");
                    outXml.Append("<EnableAudioOnlyConference>" + vrmOrgSettings.EnableAudioOnlyConference.ToString() + "</EnableAudioOnlyConference>");
                    outXml.Append("<EnableNumericID>" + vrmOrgSettings.EnableNumericID.ToString() + "</EnableNumericID>");//FB 2870
                    outXml.Append("<EnableHotdeskingConference>" + vrmOrgSettings.EnableHotdeskingConference.ToString() + "</EnableHotdeskingConference>");//ZD 100719
                    outXml.Append("<DefaultCalendarToOfficeHours>" + vrmOrgSettings.DefaultCalendarToOfficeHours.ToString() + "</DefaultCalendarToOfficeHours>");//FB 2870
                    outXml.Append("<RoomTreeExpandLevel>" + vrmOrgSettings.RoomTreeExpandLevel + "</RoomTreeExpandLevel>");
                    outXml.Append("<EnableEntity>" + vrmOrgSettings.EnableCustomOption + "</EnableEntity>");
                    outXml.Append("<EnableBufferZone>" + vrmOrgSettings.EnableBufferZone + "</EnableBufferZone>");
                    outXml.Append("<SetupTime>" + vrmOrgSettings.SetupTime + "</SetupTime>"); //FB 2398
                    outXml.Append("<TearDownTime>" + vrmOrgSettings.TearDownTime + "</TearDownTime>");
                    outXml.Append("<SystemEndTime>" + vrmOrgSettings.SystemEndTime.ToString() + "</SystemEndTime>");
                    outXml.Append("<SystemStartTime>" + vrmOrgSettings.SystemStartTime.ToString() + "</SystemStartTime>");
                    outXml.Append("<RoomLimit>" + vrmOrgSettings.RoomLimit.ToString() + "</RoomLimit>");
                    outXml.Append("<McuLimit>" + vrmOrgSettings.MCULimit.ToString() + "</McuLimit>");
                    outXml.Append("<MaxMCUEnchancedLimit>" + vrmOrgSettings.MCUEnchancedLimit.ToString() + "</MaxMCUEnchancedLimit>");//FB 2486
                    outXml.Append("<UserLimit>" + vrmOrgSettings.UserLimit.ToString() + "</UserLimit>");
                    //FB 2426 Start
                    outXml.Append("<MaxGuestRooms>" + vrmOrgSettings.GuestRoomLimit.ToString() + "</MaxGuestRooms>");
                    outXml.Append("<MaxGuestRoomPerUser>" + vrmOrgSettings.GuestRoomPerUser.ToString() + "</MaxGuestRoomPerUser>");
                    //FB 2426 End
                    //FB 2694 Start
                    outXml.Append("<MaxROHotdesking>" + vrmOrgSettings.MaxROHotdesking.ToString() + "</MaxROHotdesking>");
                    outXml.Append("<MaxVCHotdesking>" + vrmOrgSettings.MaxVCHotdesking.ToString() + "</MaxVCHotdesking>");
                    //FB 2694 End
                    outXml.Append("<MaxNonVideoRooms>" + vrmOrgSettings.MaxNonVideoRooms.ToString() + "</MaxNonVideoRooms>");
                    outXml.Append("<MaxVideoRooms>" + vrmOrgSettings.MaxVideoRooms.ToString() + "</MaxVideoRooms>");
                    outXml.Append("<MaxVMRRooms>" + vrmOrgSettings.MaxVMRRooms.ToString() + "</MaxVMRRooms>");//FB 2586
                    outXml.Append("<MaxEndpoints>" + vrmOrgSettings.MaxEndpoints.ToString() + "</MaxEndpoints>");
                    outXml.Append("<ExchangeUserLimit>" + vrmOrgSettings.ExchangeUserLimit.ToString() + "</ExchangeUserLimit>");
                    outXml.Append("<DominoUserLimit>" + vrmOrgSettings.DominoUserLimit.ToString() + "</DominoUserLimit>");
                    outXml.Append("<MobileUserLimit>" + vrmOrgSettings.MobileUserLimit.ToString() + "</MobileUserLimit>");
                    outXml.Append("<EnableFacilities>" + vrmOrgSettings.EnableFacilities.ToString() + "</EnableFacilities>");
                    outXml.Append("<EnableHousekeeping>" + vrmOrgSettings.EnableHousekeeping.ToString() + "</EnableHousekeeping>");
                    outXml.Append("<EnableCatering>" + vrmOrgSettings.EnableCatering.ToString() + "</EnableCatering>");
                    outXml.Append("<EnableAPI>" + vrmOrgSettings.EnableAPI.ToString() + "</EnableAPI>");
                    outXml.Append("<ConferenceCode>" + vrmOrgSettings.ConferenceCode.ToString() + "</ConferenceCode>");
                    outXml.Append("<LeaderPin>" + vrmOrgSettings.LeaderPin.ToString() + "</LeaderPin>");
                    outXml.Append("<AdvAvParams>" + vrmOrgSettings.AdvAvParams.ToString() + "</AdvAvParams>");
                    outXml.Append("<AudioParams>" + vrmOrgSettings.AudioParams.ToString() + "</AudioParams>");
                    outXml.Append("<isVIP>" + vrmOrgSettings.isVIP.ToString() + "</isVIP>");
                    outXml.Append("<isAssignedMCU>" + vrmOrgSettings.isAssignedMCU.ToString() + "</isAssignedMCU>");
                    outXml.Append("<EnableRoomServiceType>" + vrmOrgSettings.EnableRoomServiceType + "</EnableRoomServiceType>");//FB 2219
                    outXml.Append("<isSpecialRecur>" + vrmOrgSettings.SpecialRecur.ToString() + "</isSpecialRecur>"); //FB 2052
                    outXml.Append("<EnableImmConf>" + vrmOrgSettings.EnableImmediateConference + "</EnableImmConf>");//FB 2036
                    outXml.Append("<DefaultConfDuration>" + vrmOrgSettings.DefaultConfDuration + "</DefaultConfDuration>");//FB 2501
                    outXml.Append("<EnableAudioBridges>" + vrmOrgSettings.EnableAudioBridges + "</EnableAudioBridges>");//FB 2023
                    //FB 2359 Start
                    outXml.Append("<EnableConferencePassword>" + vrmOrgSettings.EnableConfPassword + "</EnableConferencePassword>");
                    outXml.Append("<EnablePublicConference>" + vrmOrgSettings.EnablePublicConf + "</EnablePublicConference>");
                    outXml.Append("<EnableRoomParam>" + vrmOrgSettings.EnableRoomParam + "</EnableRoomParam>");
                    //FB 2359 End
                    outXml.Append("<EnableVMR>" + vrmOrgSettings.EnableVMR + "</EnableVMR>");//VMR
                    outXml.Append("<MCUAlert>" + vrmOrgSettings.MCUAlert + "</MCUAlert>");//FB 2472
                    outXml.Append("<EnableCloud>" + vrmOrgSettings.EnableCloud + "</EnableCloud>");//FB 2599
                    outXml.Append("<EnablePublicRoom>" + vrmOrgSettings.EnablePublicRoomService + "</EnablePublicRoom>");//FB 2594
                    outXml.Append("<IMTalk>");
                    outXml.Append("<IMEnabled>" + vrmOrgSettings.IMEnabled.ToString() + "</IMEnabled>");
                    outXml.Append("<refreshTime>" + vrmOrgSettings.IMRefreshConn.ToString() + "</refreshTime>");
                    outXml.Append("</IMTalk>");
                    //FB 2693 Starts
                    outXml.Append("<EnableZulu>" + vrmOrgSettings.EnableZulu + "</EnableZulu>");
                    outXml.Append("<PCUserLimit>" + vrmOrgSettings.PCUserLimit + "</PCUserLimit>");
                    outXml.Append("<EnableBlueJeans>" + vrmOrgSettings.EnableBlueJeans + "</EnableBlueJeans>");
                    outXml.Append("<EnableJabber>" + vrmOrgSettings.EnableJabber + "</EnableJabber>");
                    outXml.Append("<EnableLync>" + vrmOrgSettings.EnableLync + "</EnableLync>");
                    outXml.Append("<EnableVidtel>" + vrmOrgSettings.EnableVidtel + "</EnableVidtel>");
                    //FB 2693 Ends
                    outXml.Append("<EnableAdvancedReport>" + vrmOrgSettings.EnableAdvancedReport + "</EnableAdvancedReport>");//FB 2593
					outXml.Append("<MCUSetupDisplay>" + vrmOrgSettings.MCUSetupDisplay + "</MCUSetupDisplay>");//FB 2998
                    outXml.Append("<MCUTearDisplay>" + vrmOrgSettings.MCUTearDisplay + "</MCUTearDisplay>");//FB 2998
                    outXml.Append("<EnableCallmonitor>" + vrmOrgSettings.EnableCallmonitor + "</EnableCallmonitor>");//FB 2996
                    outXml.Append("<EnableSetupTimeDisplay>" + vrmOrgSettings.EnableSetupTimeDisplay + "</EnableSetupTimeDisplay>");//ZD 101755
                    outXml.Append("<EnableTeardownTimeDisplay>" + vrmOrgSettings.EnableTeardownTimeDisplay + "</EnableTeardownTimeDisplay>");//ZD 101755
                    outXml.Append("<VideoRefreshTimer>" + vrmOrgSettings.VideoRefreshTimer + "</VideoRefreshTimer>");//ZD 103398
                }
            }
            catch (Exception ex)
            {
                m_log.Error("AppendOrgDetails" + ex.Message);
            }
        }

        #endregion

        #region DeleteModuleLog
        /// <summary>
        /// DeleteModuleLog (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteModuleLog(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            myVRMException myVRMEx = new myVRMException();
            StringBuilder outXML = new StringBuilder();
            int userid = 0, deleteAll = 0, i = 0;
            string moduleName = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNode node;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/moduleName");
                if (node != null)
                    moduleName = node.InnerText.Trim();

                node = xd.SelectSingleNode("//login/deleteAll");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out deleteAll);

                if ((deleteAll != 1) && (moduleName == "Website" || moduleName == "Services" || moduleName == "Lotus Plugin" || moduleName == "Outlook Plugin"))
                    criterionList.Add(Expression.Eq("modulename", moduleName));

                List<vrmErrorLog> list = new List<vrmErrorLog>();
                list = m_IvrmErrLog.GetByCriteria(criterionList);
                for (i = 0; i < list.Count; i++)
                    m_IvrmErrLog.Delete(list[i]);

                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2027 - End

        //FB 2052 - Start

        #region GetHolidayType
        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetHolidayType(ref vrmDataObject obj)
        {
            string orgId = "";
            StringBuilder orgDt = new StringBuilder();
            int hldys = 0;

            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);

                orgDt.Append("<SystemHolidayType>");

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgId", organizationID));

                //m_IHolidayDAOType = m_OrgDAO.GetHolidaysTypeDAO();
                m_IHolidayDAOType.addOrderBy(Order.Asc("Priority"));
                List<holidaysType> sysHolidayType = m_IHolidayDAOType.GetByCriteria(criterionList);
                m_IHolidayDAOType.clearOrderBy();

                if (sysHolidayType.Count > 0)
                {
                    orgDt.Append("<HolidayTypes>");

                    for (hldys = 0; hldys < sysHolidayType.Count; hldys++)
                    {
                        orgDt.Append("<HolidayType>");

                        orgDt.Append("<id>" + sysHolidayType[hldys].HolidayType + "</id>");
                        orgDt.Append("<HolidayDescription>" + sysHolidayType[hldys].HolidayDescription + "</HolidayDescription>");
                        orgDt.Append("<Color>" + sysHolidayType[hldys].Color + "</Color>");
                        orgDt.Append("<Priority>" + sysHolidayType[hldys].Priority.ToString() + "</Priority>");

                        orgDt.Append("</HolidayType>");
                    }

                    orgDt.Append("</HolidayTypes>");
                }

                orgDt.Append("</SystemHolidayType>");
                obj.outXml = orgDt.ToString();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881
                return false;
            }
        }
        #endregion

        #region DeleteHolidayDetails
        /// <summary>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteHolidayDetails(ref vrmDataObject obj)
        {
            StringBuilder orgDt = new StringBuilder();
            int typeId = 0;

            try
            {
                obj.outXml = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//login/delete/typeID");
                if (node != null)
                    int.TryParse(node.InnerText, out typeId);

                if (!DeleteHolidayColordays(typeId))
                    return false;

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("HolidayType", typeId));

                List<holidaysType> sysHolidayType = m_IHolidayDAOType.GetByCriteria(criterionList);

                if (sysHolidayType.Count > 0)
                    m_IHolidayDAOType.Delete(sysHolidayType[0]);

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region Delete Holiday Type
        /// <summary>
        /// DeleteHolidayColordays
        /// </summary>
        /// <param name="typeId"></param>
        /// <returns></returns>
        private Boolean DeleteHolidayColordays(int typeId)
        {
            int i = 0;
            try
            {
                IList<holidays> sysHolidays = null;

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("HolidayType", typeId));

                m_IHolidayDAO = m_OrgDAO.GetHolidaysDao();
                sysHolidays = m_IHolidayDAO.GetByCriteria(criterionList);

                if (sysHolidays.Count > 0)
                {
                    for (i = 0; i < sysHolidays.Count; i++)
                        m_IHolidayDAO.Delete(sysHolidays[i]);
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }

        }

        #endregion

        #region SetDayOrderList
        /// <summary>        
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetDayOrderList(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            holidaysType hType = null;
            int i = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgID;
                int.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgID)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                XmlElement root = xd.DocumentElement;
                XmlNodeList dlist = root.SelectNodes(@"/login/DayOrder/Day");
                for (i = 0; i < dlist.Count; i++)
                {
                    int order = 0, dayID = 0;

                    node = dlist[i].SelectSingleNode("Order");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out order);

                    node = dlist[i].SelectSingleNode("DayID");
                    if (node != null)
                        int.TryParse(node.InnerText.Trim(), out dayID);

                    hType = null;
                    hType = m_IHolidayDAOType.GetById(dayID, true);
                    hType.Priority = order;
                    m_IHolidayDAOType.SaveOrUpdate(hType);
                }

                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2052 - End

        //FB 2074
        #region Purge Organization
        /// <summary>
        /// /*** 2074 ***/
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool PurgeOrganization(ref vrmDataObject obj)
        {
            bool bRet = true;
            ns_SqlHelper.SqlHelper sqlCon = null;
            string strSQL = "";
            int strExec = -1;
            string path = "";
            EvtLog e_log = new EvtLog();
            XmlDocument xd = null;
            XmlNode node = null;
            try
            {
                lock (this)
                {
                    xd = new XmlDocument();
                    xd.LoadXml(obj.inXml);


                    node = xd.SelectSingleNode("//PurgeOrganization/Path");
                    path = node.InnerXml.Trim();

                    node = xd.SelectSingleNode("//PurgeOrganization/orgId");
                    string orgid = "";
                    if (node != null)
                        orgid = node.InnerXml.Trim();

                    if (orgid == "")
                    {
                        myVRMException myVRMEx = new myVRMException(423);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }


                    int.TryParse(orgid, out organizationID);

                    if (organizationID <= 11)
                    {
                        myVRMException myVRMEx = new myVRMException(423);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }

                    sqlCon = new ns_SqlHelper.SqlHelper(path);

                    sqlCon.OpenConnection();

                    sqlCon.OpenTransaction();

                    e_log.LogEvent("Purge Organization Command Initiated Successfully.");

                    strSQL = "Delete from Acc_Balance_D where userid <> 11 And UserID in (Select UserID from Usr_List_D where companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Acc_Group_D";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_AdvAVParams_D where ConfID in (Select ConfID from Conf_Conference_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Alerts_D where ConfID in (Select ConfID from Conf_Conference_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Approval_D where ConfID in (Select ConfID from Conf_Conference_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Attachments_D where ConfID in (Select ConfID from Conf_Conference_D where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Conf_Bridge_D where ConfID in (Select ConfID from Conf_Conference_D where   orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Cascade_D where ConfID in (Select ConfID from Conf_Conference_D where   orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_CustomAttr_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_CustomAttrEx_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_FoodOrder_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Group_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Monitor_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_RecurInfo_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_ResourceOrder_D where ConfID in (Select ConfID from Conf_Conference_D where   orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_Room_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_RoomSplit_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Conf_User_D where ConfID in (Select ConfID from Conf_Conference_D  where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);


                    strSQL = "Delete from Conf_Conference_D where confid <> 11 and  orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Dept_CustomAttr_Option_D where customAttributeId in (select customAttributeId from Dept_CustomAttr_D where  orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Dept_CustomAttr_D where  orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Dept_List_D where  orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Email_Queue_D where  orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Ept_List_D where  orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Grp_Detail_D where  orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Grp_Participant_D where  UserID in (Select UserID from Usr_List_D  where   companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_ItemList_AV_D where CategoryId in (Select ID from Inv_Category_D where orgId ='" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_ItemList_CA_D where CategoryId in (Select ID from Inv_Category_D where orgId ='" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_ItemList_HK_D where CategoryId in (Select ID from Inv_Category_D where orgId ='" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_ItemService_D where menuId in (Select id from Inv_Menu_D where CategoryId in (Select ID from Inv_Category_D where orgId ='" + organizationID.ToString() + "'))";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_Menu_D where CategoryId in (Select ID from Inv_Category_D where orgId ='" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_List_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_Room_D where CategoryId in (Select ID from Inv_Category_D where orgId ='" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_Category_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_ItemCharge_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_WorkItem_D where WorkOrderID in (Select ID from Inv_WorkOrder_D where orgId ='" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Inv_WorkOrder_D where orgId='" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Loc_Approver_D where roomid in (Select roomID from Loc_Room_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Loc_Department_D where roomid in (Select roomID from Loc_Room_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Loc_Room_D where orgId = '" + organizationID.ToString() + "' and roomid <> 11";// FB 1725
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Loc_Tier2_D where orgId = '" + organizationID.ToString() + "' and id <> 0";// FB 1725
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Loc_Tier3_D where orgId = '" + organizationID.ToString() + "' and id <> 0";// FB 1725
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_Approver_D where McuId in (Select BridgeID from Mcu_List_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_CardList_D where BridgeID in (Select BridgeID from Mcu_List_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_IPServices_D where BridgeID in (Select BridgeID from Mcu_List_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_ISDNServices_D where BridgeID in (Select BridgeID from Mcu_List_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_MPIServices_D where BridgeID in (Select BridgeID from Mcu_List_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_Tokens_D where McuId in (Select BridgeID from Mcu_List_D where orgId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Mcu_List_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Email_Language_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Email_Domain_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Sys_Approver_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Sys_LoginAudit_D where  UserID in (Select UserID from Usr_List_D  where   companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Tmp_AdvAVParams_D where TmpID in (Select tmpId  From  Tmp_List_D where orgId = '" + organizationID.ToString() + "' )";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Tmp_Group_D where TmpID in (Select tmpId  From  Tmp_List_D where orgId = '" + organizationID.ToString() + "' )";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Tmp_Participant_D where TmpID in (Select tmpId  From  Tmp_List_D where orgId = '" + organizationID.ToString() + "' )";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Tmp_Room_D where TmpID in (Select tmpId  From  Tmp_List_D where orgId = '" + organizationID.ToString() + "' )";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Tmp_List_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_Accord_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_Dept_D where UserID in (Select UserID from Usr_List_D  where   companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_GuestList_D where companyId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_LotusNotesPrefs_D where UserID in (Select UserID from Usr_List_D  where   companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_SearchTemplates_D where UserID in (Select UserID from Usr_List_D   where  companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_TemplatePrefs_D where UserID in (Select UserID from Usr_List_D  where   companyId = '" + organizationID.ToString() + "')";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_List_D where [UserID] <> 11 And  companyId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "update Usr_List_D set searchID =null,endpointid = 0 where [UserID] = 11";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Usr_Inactive_D where companyId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from Usr_Templates_D where companyId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);


                    strSQL = "update Usr_List_D set defaulttemplate =0 where UserID = 11";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from img_list_d where orgid = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  org_settings_d where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Org_Diagnostics_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    strSQL = "Delete from  Org_List_D where orgId = '" + organizationID.ToString() + "'";
                    strExec = sqlCon.ExecuteNonQuery(strSQL);

                    sqlCon.CommitTransaction();

                    sqlCon.CloseConnection();
                    e_log.LogEvent("Purge Organization Command Completed Successfully.");

                    bRet = true;
                }
            }
            catch (myVRMException e)
            {
                sqlCon.RollBackTransaction();

                m_log.Error("vrmException", e);
                obj.outXml = "";
                bRet = false;
            }
            catch (Exception e)
            {
                sqlCon.RollBackTransaction();

                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }

        #endregion

        //FB 2337
        #region GetOrgLicenseAgreement
        /// <summary>
        /// GetOrgLicenseAgreement
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetOrgLicenseAgreement(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                int UserID = 0;

                if (xd.SelectSingleNode("//GetOrgLicenseAgreement/userid") != null)
                    int.TryParse(xd.SelectSingleNode("//GetOrgLicenseAgreement/userid").InnerText.Trim(), out UserID);
                
                organizationID = defaultOrgID;
                vrmUser user = m_IuserDAO.GetByUserId(UserID);
                if (user != null)
                    organizationID = user.companyId;

                VrmOrgLicAgreement orgLicAgrmnt = m_IOrgLicAgrDAO.GetOrgLicAgreementByOrgId(organizationID);

                obj.outXml = "<OrgLicenseAgreement>"
                           + "<LicenseAgreement>"+ orgLicAgrmnt.EndUserLicAgrmnt +"</LicenseAgreement>"
                           + "</OrgLicenseAgreement>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
            return true;
        }
        #endregion

        #region SetOrgLicenseAgreement
        /// <summary>
        /// SetOrgLicenseAgreement
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetOrgLicenseAgreement(ref vrmDataObject obj)
        {
            try
            {
                string EndUsrLicAgr = "";
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                if (xd.SelectSingleNode("//SetOrgLicenseAgreement/organizationID") != null)
                    int.TryParse(xd.SelectSingleNode("//SetOrgLicenseAgreement/organizationID").InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgID)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (xd.SelectSingleNode("//SetOrgLicenseAgreement/EndUserLicAgrmnt") != null)
                  EndUsrLicAgr = xd.SelectSingleNode("//SetOrgLicenseAgreement/EndUserLicAgrmnt").InnerXml.Trim();

                if (EndUsrLicAgr.Trim() != "")
                {
                    VrmOrgLicAgreement orgLicAgr = new VrmOrgLicAgreement();
                    orgLicAgr.EndUserLicAgrmnt = EndUsrLicAgr;
                    orgLicAgr.OrgID = organizationID;

                    VrmOrgLicAgreement orgLicAgrs = m_IOrgLicAgrDAO.GetOrgLicAgreementByOrgId(organizationID);
                    if (orgLicAgrs.UID > 0)
                    {
                        orgLicAgr.UID = orgLicAgrs.UID;
                        m_IOrgLicAgrDAO.Update(orgLicAgr);
                    }
                    else
                        m_IOrgLicAgrDAO.Save(orgLicAgr);
                }

                obj.outXml = "<Sucess>1</Sucess>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
            return true;
        }
        #endregion

        //FB 2426
        #region GuestRoomPerUserValidation
        /// <summary>
        /// GuestRoomPerUserValidation
        /// </summary>
        /// <param name="updateRoomLimit"></param>
        /// <param name="errNo"></param>
        /// <returns></returns>
        private bool GuestRoomPerUserValidation(ref int updateRoomLimit,ref int errNo,ref string orgID)
        {
            try
            {
                int recordCount = 0, i = 0;
                string Query = "";

                if (orgID.ToLower() == "new" || orgID.Trim() == "")
                    return true;

                Query = "select count(AdminID) as UserRoomCount from myVRM.DataLayer.vrmRoom where Extroom = 1 and Disabled = 0 and orgId = " + orgID + " group by AdminID";
                IList rooms = m_IRoomDAO.execQuery(Query);
                for (i = 0; i < rooms.Count; i++)
                {
                    int.TryParse(rooms[i].ToString(), out recordCount);
                    if (recordCount > updateRoomLimit)
                    {
                        errNo = 615;
                        return false;
                    }
                }

            }
            catch (Exception ex)
            {
                m_log.Error("" + ex.StackTrace);
                return false;
            }
            return true;
        }
        #endregion

        //FB 2486 Starts

        #region SetOrgTextMsg
        /// <summary>
        /// SetOrgTextMsg
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetOrgTextMsg(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;
                string orgId = organizationID.ToString();

                node = xd.SelectSingleNode("//SetOrgTextMsg/organizationID");
                if (node != null)
                    orgId = node.InnerText;

                if (orgId == "")
                {
                    obj.outXml = "<error>Invalid Organization ID</error>";
                    return false;
                }
                int.TryParse(orgId, out organizationID);

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingDAO.GetByOrgId(organizationID); 

                node = xd.SelectSingleNode("//SetOrgTextMsg/OrgMessage");
                if (node != null)
                    orgInfo.DefaultTxtMsg = node.InnerText.Trim();
                
                m_IOrgSettingDAO.Update(orgInfo);

                obj.outXml = "<Sucess>1</Sucess>";
            }
            catch (myVRMException e)
            {
                m_log.Error("SetOrgTextMsg", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
            return true;
        }
        #endregion
        //FB 2486 Ends

        //FB 2506 Starts
        #region CreateDefaultTxtMsg
        /// <summary>
        /// CreateDfitConfTxtMsg
        /// </summary>
        /// <returns></returns>
        private bool CreateDefaultTxtMsg()
        {

            try
            {
                vrmMessage vrmCfMsg = new vrmMessage();

                vrmDeptCustomAttr vrmDeptCA = new vrmDeptCustomAttr();


                vrmCfMsg.Languageid = 1;
                vrmCfMsg.Type = 1;
                vrmCfMsg.orgId = organizationID;


                vrmCfMsg.Message = "The conference will end in 5 minutes";
                vrmCfMsg.msgId = 1;
                m_IMessageDao.Save(vrmCfMsg);

                vrmCfMsg.Message = "The conference will end in 2 minutes";
                vrmCfMsg.msgId = 2;
                m_IMessageDao.Save(vrmCfMsg);

                vrmCfMsg.Message = "The conference will end in 30 seconds";
                vrmCfMsg.msgId = 3;
                m_IMessageDao.Save(vrmCfMsg);

                orgInfo = m_IOrgSettingDAO.GetByOrgId(organizationID); 
                orgInfo.DefaultTxtMsg = "1#1#5M;2#2#2M;3#3#30s;";
                orgInfo.EM7OrgId = -1; // FB 2575
                m_IOrgSettingDAO.Update(orgInfo);

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException occured in Inserting Default Conf Message Data", ex);
                return false;
            }
            finally
            {
                m_Hardware = null;
                m_IMessageDao = null;
            }
        }

        #endregion
        //FB 2506 Ends

        //FB 2594 Start
        #region PublicRoomSystemApprovers
        /// <summary>
        /// PublicRoomSystemApprovers
        /// </summary>
        /// <returns></returns>
        public bool PublicRoomSystemApprovers(ref int errNo)
        {
            sysApprover sysAppr = null;
            int i = 0;
            try
            {
                organizationID = 11;
                List<int> approverList = new List<int>();
                bool retVal = false;
                if (sysAppList == null)
                    sysAppList = m_ISysApproverDAO.GetSysApproversByOrgId(organizationID);

                if (sysAppList == null)
                    return true;

                if (sysAppList.Count < 1)
                {
                    approverList.Add(organizationID); //  For Public Room Service  -Approver is always 11 
                    if (approverList.Count > 0)
                    {
                        retVal = SetSystemApprovers(approverList);
                        if (!retVal)
                        {
                            errNo = 441;
                            return false;
                        }
                    }
                    return true;
                }

                m_ISysApproverDAO.clearFetch();

                for (i = 0; i < sysAppList.Count; i++)
                {
                    sysAppr = null;
                    sysAppr = sysAppList[i];
                    m_ISysApproverDAO.Delete(sysAppr);
                }

                approverList.Add(organizationID); //  For Public Room Service  -Approver is always 11 
                if (approverList.Count > 0)
                {
                    retVal = SetSystemApprovers(approverList);
                    if (!retVal)
                    {
                        errNo = 441;
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion
        //FB 2594 Ends

        //FB 2678 Start
        #region ValidateOrgLicense
        /// <summary>
        /// ValidateOrgLicense
        /// </summary>
        /// <returns></returns>
        internal bool ValidateOrgLicense(int OrgId)
        {
            vrmOrganization OrgDetails = null;
            cryptography.Crypto crypto = new cryptography.Crypto();
            string orglicense = "";
            DateTime expirydate = DateTime.Now;
            try
            {
                if (OrgId >= 11)
                {
                    OrgDetails = m_IOrgDAO.GetById(OrgId);
                    orglicense = crypto.decrypt(OrgDetails.orgExpiryKey);
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(orglicense);
                    XmlNode node;

                    node = xd.SelectSingleNode("//Organization/ExpiryDate");
                    string ExpirationDate = node.InnerXml.Trim();
                    DateTime.TryParse(ExpirationDate, out expirydate);
                  
                    node = xd.SelectSingleNode("//Organization/ID");
                    int licenseorgId = Int32.Parse(node.InnerXml.Trim());

                    if (licenseorgId == OrgId)
                    {
                        if (DateTime.Now > sysSettings.ExpiryDate)
                            return false;

                        if (expirydate > sysSettings.ExpiryDate)
                            return false;

                        if (DateTime.Now > expirydate)
                            return false;
                    }
                    else
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("Issue in Encrypting License Details", e);
                return false;
            }
        }
        #endregion
        //FB 2678 End

        //FB 2719 Theme Starts
        #region SetTheme
        /// <summary>        
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetTheme(ref vrmDataObject obj)
        {  
            string outXml = string.Empty;
            int organizationID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                string orgid = "";
                node = xd.SelectSingleNode("//SetTheme//organizationID");                
                if (node != null)
                    orgid = node.InnerXml.Trim();

                int.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgID)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                m_IOrgSettingDAO.clearFetch();

                if (orgInfo == null)
                    orgInfo = m_IOrgSettingDAO.GetByOrgId(organizationID);

                int ThemeType=0;
                node = xd.SelectSingleNode("//SetTheme//ThemeType");                
                if (node != null)
                    int.TryParse(xd.SelectSingleNode("//SetTheme//ThemeType").InnerText,out ThemeType);

                orgInfo.ThemeType = ThemeType;
                m_IOrgSettingDAO.SaveOrUpdate(orgInfo);

                return true;
                
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }            
        }
        #endregion
        //FB 2719 Theme Ends

        //ZD 101176 Starts
        private void SetCustomLang(string Title, int CustomAttID)
        {

            IList languages = vrmGen.getLanguage();
            vrmLanguage lng = null;
            VrmCustomLanguage custTitle = null;
            
            for (int i = 0; i < languages.Count; i++)
            {

                lng = (vrmLanguage)languages[i];

                if (lng.Id == 3) //Spanish
                {
                    custTitle = new VrmCustomLanguage();
                    custTitle.LanguageID = lng.Id;
                    custTitle.DisplayTitle = Title;
                    custTitle.CustomAttributeID = CustomAttID;
                    m_ICustomAttrLangDAO.Save(custTitle);
                }

            }
        }
        //ZD 101176 Starts

        //ZD 101525 Starts

        #region SetLDAPGroup
        /// <summary>        
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetLDAPGroup(ref vrmDataObject obj)
        {
            string outXml = string.Empty;
            string groupname = "";
            int organizationID = 0, userId = 0, SelectedOrgId = 0;
            List<vrmLDAPGroups> OLDAPGroups = new List<vrmLDAPGroups>();
            List<vrmLDAPGroups> NLDAPGroups = new List<vrmLDAPGroups>();
            List<vrmLDAPGroups> CLDAPGroups = new List<vrmLDAPGroups>();
            //ZD 102897 Start
            List<vrmLDAPGroups> DLDAPGroups = new List<vrmLDAPGroups>();
            List<string> extgrplist = new List<string>();
            List<string> delgrplist = new List<string>();
            List<string> newgrplist = new List<string>();
			//ZD 102897 End
            vrmLDAPGroups LDAPgrp = null;
            List<string> grplist = null;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                XmlNodeList nodelist = null;

                node = xd.SelectSingleNode("//SetLDAPGroup/UserID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userId);
                else
                {
                    myvrmEx = new myVRMException(201);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if ((userId <= 0))
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//SetLDAPGroup/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out organizationID);

                if (organizationID < 11)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }


                node = xd.SelectSingleNode("//SetLDAPGroup/SelectedOrgId");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SelectedOrgId);

                OLDAPGroups = m_ILDAPGroupsDAO.GetLDAPGroupByOrgId(SelectedOrgId);


                nodelist = xd.SelectNodes("//SetLDAPGroup//Groups/Group");
                if (nodelist != null)
                {
                    if (nodelist.Count > 0)
                    {
                        grplist = new List<string>();
                        for (int n = 0; n < nodelist.Count; n++)
                        {
                            grplist.Add(nodelist[n].InnerText);
                        }
                    }
                }

                m_ILDAPGroupsDAO.clearFetch();
				//ZD 102897 Start
                extgrplist = new List<string>();
                delgrplist = new List<string>();
                newgrplist = new List<string>();

                for (int o = 0; o < OLDAPGroups.Count; o++)
                {
                    groupname = OLDAPGroups[o].GroupName;
                    if (grplist.Contains(groupname))
                        extgrplist.Add(groupname);
                    else
                        delgrplist.Add(groupname);
                }

                newgrplist = grplist.Except(extgrplist).ToList();

                groupname = "";
                if (newgrplist.Count > 0)
                {
                    for (int n = 0; n < newgrplist.Count; n++)
                    {
                        groupname = newgrplist[n].ToString();
                        LDAPgrp = new vrmLDAPGroups();
                        LDAPgrp.GroupName = groupname;
                        LDAPgrp.RoleOrTemplateId = 0;
                        LDAPgrp.Origin = 0;
                        LDAPgrp.OrgId = SelectedOrgId;
                        LDAPgrp.Deleted = 0;
                        LDAPgrp.ModifiedUserId = userId;
                        LDAPgrp.LastModifiedDate = DateTime.UtcNow;
                        NLDAPGroups.Add(LDAPgrp);
                    }
                }

                groupname = "";
                if (delgrplist.Count > 0)
                {
                    for (int d = 0; d < delgrplist.Count; d++)
                    {
                        groupname = delgrplist[d].ToString();
                        CLDAPGroups = OLDAPGroups.Where(l => l.GroupName == groupname).ToList();
                        for (int c = 0; c < CLDAPGroups.Count; c++)
                        {
                            m_ILDAPGroupsDAO.Delete(CLDAPGroups[c]);
                        }
                    }
                }
				//ZD 102897 End
                if (NLDAPGroups.Count > 0)
                    m_ILDAPGroupsDAO.SaveOrUpdateList(NLDAPGroups);

                return true;

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
        }
        #endregion

        #region GetLDAPGroup
        /// <summary>        
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetLDAPGroup(ref vrmDataObject obj)
        {
            string searchfor = "";
            int organizationID = 0, userId = 0, SelectedOrgId = 0;
            List<vrmLDAPGroups> LDAPGroups = new List<vrmLDAPGroups>();
            List<ICriterion> icrit = new List<ICriterion>();
            StringBuilder outxml = new StringBuilder ();
            vrmUserRoles usrRole = null;
            vrmUserTemplate usrTemplate = null;//ZD 102052
            int pageNo = 0; //ZD 102182
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetLDAPGroup/UserID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userId);
                else
                {
                    myvrmEx = new myVRMException(201);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if ((userId <= 0))
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//GetLDAPGroup/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out organizationID);

                if (organizationID < 11)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//GetLDAPGroup/SelectedOrgId");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SelectedOrgId);
                if (SelectedOrgId <= 0)
                    SelectedOrgId = 11;

                //ZD 102182
                node = xd.SelectSingleNode("//GetLDAPGroup/pageNo");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out pageNo);

                node = xd.SelectSingleNode("//GetLDAPGroup/Searchfor");
                if (node != null)
                    searchfor = node.InnerXml.Trim();

                icrit.Add(Expression.Eq("OrgId", SelectedOrgId));
                if (searchfor != "")
                    icrit.Add(Expression.Like("GroupName", "%%" + searchfor.ToLower() + "%%").IgnoreCase()); //ZD 102182
                    //icrit.Add(Expression.Eq("GroupName", searchfor));

				//ZD 102182
                long ttlRecords = 0;
                int m_PAGE_SIZE = 500;
                m_ILDAPGroupsDAO.pageSize(m_PAGE_SIZE);
                m_ILDAPGroupsDAO.pageNo(pageNo);
                ttlRecords =
                    m_ILDAPGroupsDAO.CountByCriteria(icrit);

                LDAPGroups = m_ILDAPGroupsDAO.GetByCriteria(icrit, true);
				//ZD 102182
                int ttlPages = (int)(ttlRecords / m_PAGE_SIZE);
                if (ttlRecords % m_PAGE_SIZE > 0)
                    ttlPages++;

                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(outxml, xSettings))
                {
                    xWriter.WriteStartElement("GetLDAPGroup");
                    xWriter.WriteStartElement("Groups");
                    for (int i = 0; i < LDAPGroups.Count; i++)
                    {
                        xWriter.WriteStartElement("Group");
                        xWriter.WriteElementString("GroupId", LDAPGroups[i].UID.ToString());
                        xWriter.WriteElementString("GroupName", LDAPGroups[i].GroupName);
						//ZD 102052 Starts
                        xWriter.WriteElementString("RoleId", LDAPGroups[i].RoleOrTemplateId.ToString());
                        if (LDAPGroups[i].RoleOrTemplateId > 0)
                        {
                            if (LDAPGroups[i].Origin == 1)
                            {
                                usrTemplate = m_ItempDao.GetById(LDAPGroups[i].RoleOrTemplateId);
                                xWriter.WriteElementString("RoleName", usrTemplate.name);
                            }
                            else
                            {
                                usrRole = m_IUserRolesDao.GetById(LDAPGroups[i].RoleOrTemplateId);
                                xWriter.WriteElementString("RoleName", usrRole.roleName);
                            }
                        }//ZD 102052 End
                        else
                            xWriter.WriteElementString("RoleName", "");
                        xWriter.WriteElementString("Deleted", LDAPGroups[i].Deleted.ToString());
                        xWriter.WriteElementString("Origin", LDAPGroups[i].Origin.ToString());//ZD 102052 
                        xWriter.WriteFullEndElement();
                    }
                    xWriter.WriteFullEndElement();
                    xWriter.WriteElementString("totalPages", ttlPages.ToString());//ZD 102182
                    xWriter.WriteFullEndElement();
                    xWriter.Flush();
                }
                obj.outXml = outxml.ToString();

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
        }
        #endregion

        public bool UpdateLDAPGroupRole(ref vrmDataObject obj)
        {
            string outXml = string.Empty;
			//ZD 102052
            int roleId = 0, userId = 0, SelectedOrgId = 0, Origin = 0;
            string groupName = "", RoleAndOriginId = "";
            string tempValue = "";
            int RoleorTemplateId = 0;
            int originId = 0;
			
            List<vrmLDAPGroups> LDAPGroups = new List<vrmLDAPGroups>();
            Hashtable groupList = null;
            XmlDocument xd = new XmlDocument();
            XmlNodeList nodelist = null;
            XmlNode node;

            try
            {
                xd.LoadXml(obj.inXml);

                node = xd.SelectSingleNode("//UpdateLDAPGroupRole/UserID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userId);
                else
                {
                    myvrmEx = new myVRMException(201);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if ((userId <= 0))
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//UpdateLDAPGroupRole/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out organizationID);

                if (organizationID < 11)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//UpdateLDAPGroupRole/SelectedOrgId");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out SelectedOrgId);

                nodelist = xd.SelectNodes("//UpdateLDAPGroupRole//Groups/Group");

                groupList = new Hashtable();
                for (int i = 0; i < nodelist.Count; i++)
                {
                    groupName = ""; roleId = 0;
                    node = nodelist[i];
                    if (node.SelectSingleNode("GroupName") != null)
                        groupName = node.SelectSingleNode("GroupName").InnerText.Trim();

                    if (node.SelectSingleNode("RoleId") != null)
                        int.TryParse(node.SelectSingleNode("RoleId").InnerText.Trim(), out roleId);
					//ZD 102052 Starts
                    if (node.SelectSingleNode("Origin") != null)
                        int.TryParse(node.SelectSingleNode("Origin").InnerText.Trim(), out Origin);
                    RoleAndOriginId = roleId + "," + Origin;
                    //if (groupName != "" && roleId > 0)
                    if(!groupList.ContainsKey(groupName))
                        groupList.Add(groupName, RoleAndOriginId);//ZD 102052
                }

                LDAPGroups = m_ILDAPGroupsDAO.GetLDAPGroupByOrgId(SelectedOrgId);
                m_ILDAPGroupsDAO.clearFetch();
                for (int g = 0; g < LDAPGroups.Count; g++)
                {
                    if (!groupList.ContainsKey(LDAPGroups[g].GroupName))
                        continue;
					//ZD 102052 Starts	
                    tempValue = (string)groupList[LDAPGroups[g].GroupName];

                    int.TryParse(tempValue.Split(',')[0].ToString(), out RoleorTemplateId);
                    int.TryParse(tempValue.Split(',')[1].ToString(), out originId);
                    
                    LDAPGroups[g].RoleOrTemplateId = RoleorTemplateId;
                    LDAPGroups[g].Origin = originId;//ZD 102052
					//ZD 102052 End
                    LDAPGroups[g].OrgId = SelectedOrgId;
                    LDAPGroups[g].Deleted = 0;
                    LDAPGroups[g].ModifiedUserId = userId;
                    LDAPGroups[g].LastModifiedDate = DateTime.UtcNow;
                }

                if (LDAPGroups.Count > 0)
                    m_ILDAPGroupsDAO.SaveOrUpdateList(LDAPGroups);
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
            catch (Exception ex)
            {
                m_log.Error("vrmException", ex);
                obj.outXml = "";
                return false;
            }
            return true;
        }

        //ZD 101525 Ends
        //ZD 101835
        #region SetConfArchiveConfiguration
        /// <summary>        
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetConfArchiveConfiguration(ref vrmDataObject obj)
        {
            string outXml = string.Empty;
            int organizationID = 0, userId = 0;
            vrmConfArchiveSettings confArchiveSettings = new vrmConfArchiveSettings();
            List<vrmConfArchiveSettings> confArchiveSettingsList = new List<vrmConfArchiveSettings>();
            List<ICriterion> criteria = new List<ICriterion>();

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//ConfArchiveConfiguration/UserID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userId);
                else
                {
                    myvrmEx = new myVRMException(201);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }
                if ((userId <= 0))
                {
                    myvrmEx = new myVRMException(422);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//ConfArchiveConfiguration/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out organizationID);

                if (organizationID < 11)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                criteria.Add(Expression.Eq("OrgID", organizationID));
                confArchiveSettingsList = m_IConfArchiveSettingsDao.GetByCriteria(criteria, true);

                if (confArchiveSettingsList.Count > 0)
                    confArchiveSettings = confArchiveSettingsList[0];

                confArchiveSettings.UserID = userId;
                confArchiveSettings.OrgID = organizationID;

                int archiveConfOlderThan = 3;
                node = xd.SelectSingleNode("//ConfArchiveConfiguration/ArchiveConfOlderThan");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out archiveConfOlderThan);
                confArchiveSettings.ArchiveConfOlderThan = archiveConfOlderThan;

                int archivePeriod = 2;
                node = xd.SelectSingleNode("//ConfArchiveConfiguration/ArchivePeriod");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out archivePeriod);
                confArchiveSettings.ArchivePeriod = archivePeriod;

                string archiveTime = "12:00 AM";
                node = xd.SelectSingleNode("//ConfArchiveConfiguration/ArchiveTime");
                if (node != null)
                    archiveTime = node.InnerXml.Trim();
                Boolean isTimeChanged = false;
                if (confArchiveSettings.ArchiveTime.ToShortTimeString() != archiveTime)
                    isTimeChanged = true;

                confArchiveSettings.ArchiveTime = DateTime.Parse(DateTime.Now.ToShortDateString() + " " + archiveTime);

                if (isTimeChanged || confArchiveSettingsList.Count == 0)
                {
                    confArchiveSettings.ConfigurationDate = DateTime.Now;
                    confArchiveSettings.LastRunDate = DateTime.Now;
                }

                m_IConfArchiveSettingsDao.SaveOrUpdate(confArchiveSettings);

                return true;

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
        }
        #endregion

        #region GetConfArchiveConfiguration
        /// <summary>        
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetConfArchiveConfiguration(ref vrmDataObject obj)
        {
            string searchfor = "";
            int organizationID = 11;
            List<ICriterion> criteria = new List<ICriterion>();
            List<vrmConfArchiveSettings> confArchiveSettings = new List<vrmConfArchiveSettings>();
            StringBuilder outxml = new StringBuilder();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//ConfArchiveConfiguration/organizationID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out organizationID);

                if (organizationID < 11)
                {
                    myvrmEx = new myVRMException(423);
                    obj.outXml = myvrmEx.FetchErrorMsg();
                    return false;
                }

                string callingFrom = "";
                node = xd.SelectSingleNode("//ConfArchiveConfiguration/From");
                if (node != null)
                    callingFrom = node.InnerText;
                
                if(callingFrom == "")
                    criteria.Add(Expression.Eq("OrgID", organizationID));
                
                confArchiveSettings = m_IConfArchiveSettingsDao.GetByCriteria(criteria, true);

                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(outxml, xSettings))
                {
                    xWriter.WriteStartElement("ConfArchiveConfiguration");
                    for (int i = 0; i < confArchiveSettings.Count; i++)
                    {
                        xWriter.WriteStartElement("Settings");
                        xWriter.WriteElementString("ArchiveConfOlderThan", confArchiveSettings[i].ArchiveConfOlderThan.ToString());
                        xWriter.WriteElementString("ArchivePeriod", confArchiveSettings[i].ArchivePeriod.ToString());
                        xWriter.WriteElementString("ArchiveTime", confArchiveSettings[i].ArchiveTime.ToString());
                        xWriter.WriteElementString("ConfigurationDate", confArchiveSettings[i].ConfigurationDate.ToString());
                        xWriter.WriteElementString("LastRunDate", confArchiveSettings[i].LastRunDate.ToString());
                        xWriter.WriteElementString("OrgID", confArchiveSettings[i].OrgID.ToString());
                        xWriter.WriteElementString("ID", confArchiveSettings[i].ID.ToString());
                        xWriter.WriteFullEndElement();
                    }
                    xWriter.WriteFullEndElement();
                    xWriter.Flush();
                }

                if (callingFrom == "FS")
                {
                    ArchiveConferences(outxml.ToString(),ref obj);
                }
                else                
                    obj.outXml = outxml.ToString();

                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                return false;
            }
        }
        #endregion

        #region ArchiveConferences
        /// <summary>
        /// ArchiveConferences
        /// </summary>
        /// <param name="outxml"></param>
        /// <param name="obj"></param>
        private void ArchiveConferences(string outxml, ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.LoadXml(outxml);
                XmlNodeList nodes = xmldoc.SelectNodes("//ConfArchiveConfiguration/Settings");
                foreach (XmlNode node in nodes)
                {
                    Int32 archiveConfOlderThan;
                    archiveConfOlderThan = Int32.Parse(node.SelectSingleNode("ArchiveConfOlderThan").InnerText);
                    Int32 ArchivePeriod;
                    ArchivePeriod = Int32.Parse(node.SelectSingleNode("ArchivePeriod").InnerText);
                    DateTime ArchiveTime;
                    ArchiveTime = DateTime.Parse(node.SelectSingleNode("ArchiveTime").InnerText);
                    DateTime ConfigurationDate;
                    ConfigurationDate = DateTime.Parse(node.SelectSingleNode("LastRunDate").InnerText);
                    Int32 orgID = 11;
                    orgID = Int32.Parse(node.SelectSingleNode("OrgID").InnerText);
                    string id = "";
                    id = node.SelectSingleNode("ID").InnerText;

                    Int32 addNoofDays = 90;
                    switch (ArchivePeriod)
                    {
                        case (Int32)eConfAchivePeriod.Monthly:
                            addNoofDays = 30;
                            break;
                        case (Int32)eConfAchivePeriod.Quarterly:
                            addNoofDays = 90;
                            break;
                        case (Int32)eConfAchivePeriod.SemiAnnually:
                            addNoofDays = 180;
                            break;
                        case (Int32)eConfAchivePeriod.Annually:
                            addNoofDays = 360;
                            break;
                    }

                    Int32 addMonths = 180;
                    switch (archiveConfOlderThan)
                    {
                        case (Int32)eConfAchiveOldeThanByMonth.M1:
                            addMonths = 30;
                            break;
                        case (Int32)eConfAchiveOldeThanByMonth.M3:
                            addMonths = 90;
                            break;
                        case (Int32)eConfAchiveOldeThanByMonth.M6:
                            addMonths = 180;
                            break;
                        case (Int32)eConfAchiveOldeThanByMonth.M9:
                            addMonths = 270;
                            break;
                        case (Int32)eConfAchiveOldeThanByMonth.M12:
                            addMonths = 360;
                            break;
                    }

                    if (ConfigurationDate < DateTime.Now)
                    {
                        if (ConfigurationDate.AddDays(addNoofDays).ToShortDateString() == DateTime.Now.ToShortDateString())
                        {
                            if (ArchiveTime.ToShortTimeString() == DateTime.Now.ToShortTimeString())
                            {
                                Conference conference = new Conference(ref obj);

                                DateTime confDateBefore = DateTime.UtcNow;
                                confDateBefore = confDateBefore.AddDays(-addMonths);

                                conference.InsertConfArchive(confDateBefore, orgID, id);
                            }
                        }
                    }
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
            }
        }
        #endregion

        //ZD 103398 Starts
        #region OrgOptionsInfo
        /// <summary>
        /// OrgOptionsInfo
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool OrgOptionsInfo(ref vrmDataObject obj)
        {
            bool bRet = true;
            string roomUID = "";
            StringBuilder outXml = new StringBuilder();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<vrmRoom> locRoom = new List<vrmRoom>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//login/roomUID");
                if (node != null)
                   roomUID = node.InnerText.Trim();

                criterionList.Add(Expression.Eq("RoomUID", roomUID));
                locRoom = m_IRoomDAO.GetByCriteria(criterionList, true);

                if (locRoom.Count > 0)
                {
                    orgInfo = m_IOrgSettingDAO.GetByOrgId(locRoom[0].orgId);
                    outXml.Append("<OrgOptionsInfo>");
                    outXml.Append("<VideoStreamingDetails>");
                    outXml.Append("<VideoSourceURL1>" + orgInfo.VideoSourceURL1 + "</VideoSourceURL1>");
                    outXml.Append("<ScreenPosition1>" + orgInfo.ScreenPosition1 + "</ScreenPosition1>");
                    outXml.Append("<VideoSourceURL2>" + orgInfo.VideoSourceURL2 + "</VideoSourceURL2>");
                    outXml.Append("<ScreenPosition2>" + orgInfo.ScreenPosition2 + "</ScreenPosition2>");
                    outXml.Append("<VideoSourceURL3>" + orgInfo.VideoSourceURL3 + "</VideoSourceURL3>");
                    outXml.Append("<ScreenPosition3>" + orgInfo.ScreenPosition3 + "</ScreenPosition3>");
                    outXml.Append("<VideoSourceURL4>" + orgInfo.VideoSourceURL4 + "</VideoSourceURL4>");
                    outXml.Append("<ScreenPosition4>" + orgInfo.ScreenPosition4 + "</ScreenPosition4>");
                    outXml.Append("</VideoStreamingDetails>");
                    outXml.Append("<VideoRefreshTimer>" + orgInfo.VideoRefreshTimer + "</VideoRefreshTimer>");
                    outXml.Append("</OrgOptionsInfo>");
                }
                else
                {
                    outXml.Append("<OrgOptionsInfo>");
                    outXml.Append("<VideoStreamingDetails>");
                    outXml.Append("<VideoSourceURL1></VideoSourceURL1>");
                    outXml.Append("<ScreenPosition1></ScreenPosition1>");
                    outXml.Append("<VideoSourceURL2></VideoSourceURL2>");
                    outXml.Append("<ScreenPosition2></ScreenPosition2>");
                    outXml.Append("<VideoSourceURL3></VideoSourceURL3>");
                    outXml.Append("<ScreenPosition3></ScreenPosition3>");
                    outXml.Append("<VideoSourceURL4></VideoSourceURL4>");
                    outXml.Append("<ScreenPosition4></ScreenPosition4>");
                    outXml.Append("</VideoStreamingDetails>");
                    outXml.Append("<VideoRefreshTimer></VideoRefreshTimer>");
                    outXml.Append("</OrgOptionsInfo>");
                }

                obj.outXml = outXml.ToString();

            }
            catch (myVRMException ex)
            {
                m_log.Error("vrmException", ex);
                obj.outXml = ex.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion
        //ZD 103398 End

        #endregion
    }

    public class Reminder
    {
        public int ReminderType { get; set; }
        public double ReminderUpperLimit { get; set; }
        public double ReminderLowerLimit { get; set; }
    }

}

