//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 End ZD 100886
using System;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.ComponentModel;
using System.Xml;
using System.Data;
using NHibernate;
using NHibernate.Criterion;
using System.Linq;
using System.Xml.Linq; //ZD 101026
using System.Reflection;
using log4net;

using myVRM.DataLayer;
using System.Xml.XPath;

using System.Threading;
using System.IO;
using ns_SqlHelper;


namespace myVRM.BusinessLayer
{
    /// <summary>
    /// Business Layer Logic for all hardware related functions. 
    /// </summary>
    /// 
    public class HardwareFactory
    {
        #region Data Members
        private hardwareDAO m_hardwareDAO;

        private IEptDao m_IeptDao;
        private IMCUDao m_ImcuDao;
        private static log4net.ILog m_log;
        private string m_configPath;

        private conferenceDAO m_confDAO;
        private IConferenceDAO m_vrmConfDAO;
        private IConfRoomDAO m_IconfRoom;
        private IConfUserDAO m_IconfUser;
        private IRoomDAO m_IRoomDAO; //FB 1886,1552
        private LocationDAO m_locDAO; //FB 1886,1552
        private IMCUIPServicesDao m_IipServices;

        private IMCUE164ServicesDao m_IE164Services; // FB 2636

        private IMCUISDNServicesDao m_IisdnServices;
        private IMCUMPIServicesDao m_ImpiService;
        private IMCUCardListDao m_IcardList;
        private IMCUApproverDao m_Iapprover;
        private ILanguageDAO m_ILanguageDAO; //FB 2486
        private GeneralDAO m_generalDAO; //FB 2486

        private userDAO m_usrDAO;
        private IUserDao m_vrmUserDAO;
        private IInactiveUserDao m_vrmIUserDAO; //FB 1462
        private IUserRolesDao m_IUserRolesDao;//ZD 100263
        private vrmSystemFactory m_systemFactory;

        private orgDAO m_OrgDAO;    //Organization Module Fixes
        private IOrgSettingsDAO m_IOrgSettingsDAO;
        private OrgData orgInfo;
        private const int defaultOrgId = 11;  //Default organization
        private int organizationID = 0;

        private IConfCascadeDAO m_IconfCascade;//FB 1937
		private IMessageDao m_msgDAO; //FB 2486
        private myVRMSearch m_SearchFactory; //FB 2539

        private IMCUProfilesDao m_IMcuProfilesDAO;//FB 2591
        private ns_SqlHelper.SqlHelper m_bridgelayer = null; //FB 2441
        private IExtMCUServiceDao m_IExtMCUService;//FB 2556
        private IExtMCUSiloDao m_IExtMCUSilo;//FB 2556
        //ZD 101527 Starts
        private ISyncEptDao m_ISyncEptDao;
        XmlWriter xWriter = null;
        XmlWriterSettings xSettings = null;
        XPathNavigator xNavigator = null;
        XPathDocument xDoc = null;
        StringReader xStrReader = null;
        XPathNavigator xNode = null;
        StringBuilder OutXML = null;
        private ISyncLocDAO m_ISyncLocDAO;
        //ZD 101527 Ends
        private int m_iMaxRecords;
        private SqlHelper sqlCon = null; //ZD 101835
        private IMCUGroupDetailsDao m_IMCUGroupDetailsDao = null; //ZD 100040
        private IMCUGrpAssignmentDao m_IMCUGrpAssignmentDao = null; //ZD 100040
        private IMCUPortResolutionDao m_IMCUPortResolutionDao = null; //ZD 100040
		private IMCUPoolOrderDao m_IMCUPoolOrderDao;//ZD 104256

        public enum Resolution { FullHDSixty = 1, FullHDThirty, HDThirty, HDSixty, SD, CIF };//ZD 100040

        #endregion

        #region HardwareFactory
        /// <summary>
        /// construct report factory with session reference
        /// </summary>
        public HardwareFactory(ref vrmDataObject obj)
        {
            try
            {
                m_log = obj.log;
                m_configPath = obj.ConfigPath;
                m_OrgDAO = new orgDAO(m_configPath, m_log); //Organization Module Fixes
                m_generalDAO = new GeneralDAO(m_configPath, m_log); //FB 2486

                m_hardwareDAO = new hardwareDAO(obj.ConfigPath, obj.log);
                m_IeptDao = m_hardwareDAO.GetEptDao();
                m_ImcuDao = m_hardwareDAO.GetMCUDao();
                m_ISyncEptDao = m_hardwareDAO.GetSyncEptDao(); //ZD 101527

                m_usrDAO = new userDAO(obj.ConfigPath, obj.log);
                m_vrmUserDAO = m_usrDAO.GetUserDao();
                m_IUserRolesDao = m_usrDAO.GetUserRolesDao();//ZD 100263

                m_vrmIUserDAO = m_usrDAO.GetInactiveUserDao(); //FB 1462

                m_confDAO = new conferenceDAO(obj.ConfigPath, obj.log);

                m_vrmConfDAO = m_confDAO.GetConferenceDao();
                m_IconfRoom = m_confDAO.GetConfRoomDao();
                m_IconfUser = m_confDAO.GetConfUserDao();

                m_IconfCascade = m_confDAO.GetConfCascadeDao();//FB 1937

                m_IE164Services = m_hardwareDAO.GetE164ServicesDao(); // FB 2636

                m_IipServices = m_hardwareDAO.GetMCUIPServicesDao();
                m_IisdnServices = m_hardwareDAO.GetMCUISDNServicesDao();
                m_ImpiService = m_hardwareDAO.GetMCUMPIServicesDao();
                m_IcardList = m_hardwareDAO.GetMCUCardListsDao();
                m_Iapprover = m_hardwareDAO.GetMCUApproverDao();
                m_systemFactory = new vrmSystemFactory(ref obj);
                m_IOrgSettingsDAO = m_OrgDAO.GetOrgSettingsDao();   //Organization Module Fixes
                m_locDAO = new LocationDAO(m_configPath, obj.log); //FB 1886,1552
                m_IRoomDAO = m_locDAO.GetRoomDAO(); //FB 1886,1552
                m_msgDAO = m_hardwareDAO.GetMessageDao(); //FB 2486
                m_ILanguageDAO = m_generalDAO.GetLanguageDAO(); //FB 2486
                m_SearchFactory = new myVRMSearch(obj); //FB 2539
                m_IMcuProfilesDAO = m_hardwareDAO.GetMCUProfilesDao();//FB 2591 
                m_iMaxRecords = 20;
                m_ISyncLocDAO = m_locDAO.GetSyncLocDAO();//ZD 101527
                m_IMCUGroupDetailsDao = m_hardwareDAO.GetMCUGroupDetailsDao(); //ZD 100040
                m_IMCUGrpAssignmentDao = m_hardwareDAO.GetMCUGrpAssignmentDao(); //ZD 100040
				m_IMCUPoolOrderDao = m_hardwareDAO.GetMCUPoolOrderDao(); //ZD 104256
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region DeleteEndpoint
        public bool DeleteEndpoint(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder searchOutXml = new StringBuilder(); //String concatenation changed to StringBuilder for Performance - FB 1820
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//DeleteEndpoint/UserID");
                string userID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//DeleteEndpoint/EndPointID");
                string endpointID = node.InnerXml.Trim();

                // this will delete ALL profiles in the endpoint table with the same endpoint ID 
                // (effectivly deleting the endpoint)
                DateTime dt = DateTime.Now;
                timeZone.changeToGMTTime(sysSettings.TimeZone, ref dt);
                dt.AddMinutes(-5);

                List<vrmEndPoint> eptList = new List<vrmEndPoint>();
                List<vrmSyncLoc> SyncRoomCount = new List<vrmSyncLoc>(); //ZD 101527
                //// first check if ANY are in use 
                //string rmQuery = "SELECT C.confid FROM myVRM.DataLayer.vrmConference C, vrmConfRoom R ";
                //rmQuery += " WHERE C.confid = R.confid AND C.instanceid = R.instanceid ";
                //rmQuery += " AND R.endpointId = " + endpointID;
                //rmQuery += " AND (C.deleted = 0 AND C.confdate >= '";
                //rmQuery += dt.ToString("d") + " " + dt.ToString("t") + "' OR C.status = 5)";

                // first check if ANY are in use      //Endpoint functionality  Fix
                string rmQuery = "SELECT R.Name FROM myVRM.DataLayer.vrmRoom R ";
                rmQuery += " WHERE R.Disabled=0 and R.endpointid = " + endpointID;//FB 2494

                IList results = m_IeptDao.execQuery(rmQuery);

                // cant delete this one, it is in use....
                if (results.Count > 0)
                {
                    String name = "";
                    name = results[0].ToString();

                    for (int i = 0; i < results.Count; i++)
                    {
                        if (name != results[i].ToString())
                            name += "," + results[i].ToString();
                    }
                    //FB 1881 start
                    //obj.outXml = myVRMException.toXml("Cannot delete the endpoint: endpoint is associated with the room(s)  " + name);
                    myVRMException myVRMEx = new myVRMException(507);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;      //Endpoint Search Fix
                }
                //ZD 101527 Starts
                int eptID = 0;
                int.TryParse(endpointID, out eptID);
                List<ICriterion> selcnt = new List<ICriterion>();
                selcnt.Add(Expression.Eq("endpointid", Int32.Parse(endpointID)));
                selcnt.Add(Expression.Eq("deleted", 0));
                eptList = m_IeptDao.GetByCriteria(selcnt);

                if (eptList.Count > 0)
                {
                    selcnt = new List<ICriterion>();
                    selcnt.Add(Expression.Eq("identifierValue", eptList[0].identifierValue));
                    SyncRoomCount = m_ISyncLocDAO.GetByCriteria(selcnt);

                }
                //rmQuery = "SELECT R.Name FROM myVRM.DataLayer.vrmSyncLoc R ";
                //rmQuery += " WHERE and R.endpointid = " + endpointID;

                //IList syncRoomLIst = m_ISyncLocDAO.execQuery(rmQuery);

                if (SyncRoomCount.Count > 0)
                {
                    String name = "";
                    name = SyncRoomCount[0].Name;

                    for (int i = 0; i < SyncRoomCount.Count; i++)
                    {
                        if (name != SyncRoomCount[i].Name)
                            name += "," + SyncRoomCount[i].Name;
                    }
                    myVRMException myVRMEx = new myVRMException(744);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;     
                }
                //ZD 101527 Starts
                criterionList.Add(Expression.Eq("endpointid", Int32.Parse(endpointID)));
                criterionList.Add(Expression.Eq("deleted", 0));
                eptList = m_IeptDao.GetByCriteria(criterionList);

                // use soft delete
                foreach (vrmEndPoint ve in eptList)
                {
                    ve.Lastmodifieddate = DateTime.UtcNow;//ZD 101026
                    ve.deleted = 1;
                }

                m_IeptDao.SaveOrUpdateList(eptList);

                SetAuditEndpoint(false, false, eptList);//ZD 100664

                //FB 1820 - Start
                searchOutXml.Append("<DeleteEndpoint>");
                searchOutXml.Append("<UserID>" + userID + "</UserID>");
                searchOutXml.Append("<ID>" + endpointID + "</ID>");
                searchOutXml.Append("</DeleteEndpoint>");
                obj.outXml = searchOutXml.ToString();
                //FB 1820 - Ends
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetAddressType
        public bool GetAddressType(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.

                List<vrmAddressType> addressType = vrmGen.getAddressType();

                obj.outXml += "<GetAddressType>";
                obj.outXml += "<AddressType>";
                foreach (vrmAddressType at in addressType)
                {
                    obj.outXml += "<Type>";
                    obj.outXml += "<ID>" + at.Id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + at.name + "</Name>";
                    obj.outXml += "</Type>";
                }
                obj.outXml += "</AddressType>";
                obj.outXml += "</GetAddressType>";

                return true;

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetBridges
        public bool GetBridges(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetBridges/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//GetBridges/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes
                //FB 2593
                node = xd.SelectSingleNode("//GetBridges/SearchType"); 
                string searchType = "";
                if (node != null)
                    searchType = node.InnerXml.Trim();

                List<ICriterion> criterionLst = new List<ICriterion>();
                if(searchType == "") //FB 2593
                    criterionLst.Add(Expression.Or(Expression.Eq("orgId", organizationID), Expression.Eq("isPublic", 1))); // FB 1920
                criterionLst.Add(Expression.Eq("deleted", 0));


                List<vrmMCU> MCUList = m_ImcuDao.GetByCriteria(criterionLst);//Organization Module Fixes
                obj.outXml += "<Bridges>";
                foreach (vrmMCU mcu in MCUList)
                {
                    obj.outXml += "<Bridge>";
                    obj.outXml += "<BridgeID>" + mcu.BridgeID.ToString() + "</BridgeID> ";
                    //FB 1920 starts
                    if (organizationID != 11)
                    {
                        if (mcu.isPublic.ToString() == "1")
                            mcu.BridgeName = mcu.BridgeName + "(*)";
                    }
                    // FB 1920 Ends
                    obj.outXml += "<BridgeName>" + mcu.BridgeName + "</BridgeName>";
                    obj.outXml += "<BridgeType>" + mcu.MCUType.id.ToString() + "</BridgeType>";//FB 2839
                    obj.outXml += "<ConferenceServiceID>" + mcu.ConfServiceID + "</ConferenceServiceID>";//FB 2839
                    obj.outXml += "<ConferencePoolOrderID>" + mcu.PoolOrderID + "</ConferencePoolOrderID>";//ZD 104256
					obj.outXml += "<OrgId>" + mcu.orgId + "</OrgId>"; //FB 2593
                    obj.outXml += "<Public>" + mcu.isPublic + "</Public>"; //FB 2593
                    obj.outXml += "<Virtual>" + mcu.VirtualBridge + "</Virtual>"; //ZD 100040
                    obj.outXml += "</Bridge>";
                }
                obj.outXml += "</Bridges>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetEndpointDetails
        public bool GetEndpointDetails(ref vrmDataObject obj)
        {
            bool bRet = true;
            int confType = 0;//FB 2602
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder searchOutXml = new StringBuilder(); //String concatenation changed to StringBuilder for Performance - FB 1820
            vrmMCU MCU = null;//FB 2839
            int userid= 0;
            vrmUser Vuser = null;//1000263_Nov11
            vrmUserRoles VuserRole = null;//1000263_Nov11
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//EndpointDetails/UserID");
                string userID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//EndpointDetails/EndpointID");
                string EndpointID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//EndpointDetails/confType");//FB 2602
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out confType);

                m_IeptDao.addOrderBy(Order.Desc("IsP2PDefault")); //ZD 101490
                m_IeptDao.addOrderBy(Order.Asc("profileId"));

                criterionList.Add(Expression.Eq("endpointid", Int32.Parse(EndpointID)));
                criterionList.Add(Expression.Eq("deleted", 0));
                List<vrmEndPoint> epList = m_IeptDao.GetByCriteria(criterionList);

                var epL = epList.OrderByDescending(c => (c.isDefault == 1)).ToList(); //ZD 100815 //ZD 101490
                epList = epL;

                if (epList.Count <= 0) //ZD 103360
                {
                    obj.outXml = "<EndpointDetails></EndpointDetails>";
                    return false;
                }
                //ZD 100263_Nov11 Start
                int.TryParse(userID, out userid);
                Vuser = m_vrmUserDAO.GetByUserId(userid);
                VuserRole = m_IUserRolesDao.GetById(Vuser.roleID);
                if (VuserRole.crossaccess == 0 && Vuser.companyId != epList[0].orgId)
                {
                    obj.outXml = "<error>-1</error>";
                    return false;
                }
                //ZD 100263_Nov11 End

                m_IeptDao.clearOrderBy();

                List<ICriterion> ProfilecriterionList = new List<ICriterion>();
               
                //FB 1820 - Starts
                searchOutXml.Append("<EndpointDetails>");

                //ZD 100664 Starts
                StringBuilder auditOutXML = null;
                searchOutXml.Append("<LastModifiedDetails>");
                if (GetAuditEndpoint(epList[0].endpointid, epList[0].profileId, Vuser.TimeZone, ref auditOutXML) && !string.IsNullOrEmpty(auditOutXML.ToString()))
                    searchOutXml.Append(auditOutXML.ToString());
                searchOutXml.Append("</LastModifiedDetails>");
                //ZD 100664 End

                int idx = 0;

                foreach (vrmEndPoint ep in epList)
                {
                    if (ep.isTelePresence == 1 && confType == 4)//FB 2602
                        continue;
                    if (idx == 0)
                    {
                        searchOutXml.Append("<Endpoint>");
                        searchOutXml.Append("<ID>" + ep.endpointid.ToString() + "</ID>");
                        searchOutXml.Append("<Name>" + ep.name + "</Name>");
                        //Code Commented for FB 1361 - Start
                        //FB 1886,1552 - Start
                        //criterionList = new List<ICriterion>();
                        //criterionList.Add(Expression.Eq("endpointid", ep.endpointid));
                        //criterionList.Add(Expression.Eq("Disabled", 0));
                        //List<vrmRoom> roomList = m_IRoomDAO.GetByCriteria(criterionList);
                        String roomnames = " ";
                        int j = 1;
                        //if (roomList.Count >= 1)
                        //{
                        //    for (int i = 0; i < roomList.Count; i++)
                        //    {
                        //        roomnames += j + ". ";
                        //        roomnames += roomList[i].Name.ToString();
                        //        if (i != (roomList.Count - 1))
                        //            roomnames += " % ";
                        //        j++;
                        //    }
                        //}
                        //Code Commented for FB 1361 - End

                        searchOutXml.Append("<RoomName>" + roomnames + "</RoomName>");
                        //FB 1886,1552 - End
                        searchOutXml.Append("<DefaultProfileID>" +
                                                 ep.profileId.ToString() + "</DefaultProfileID>");
                        searchOutXml.Append("<Profiles>");
                        idx++;
                    }
                    searchOutXml.Append("<Profile>");
                    searchOutXml.Append("<ProfileID>" + ep.profileId.ToString() + "</ProfileID>");
                    searchOutXml.Append("<ProfileName>" + ep.profileName + "</ProfileName>");
                    searchOutXml.Append("<EncryptionPreferred>" +
                                                    ep.encrypted.ToString() + "</EncryptionPreferred>");
                    searchOutXml.Append("<AddressType>" + ep.addresstype.ToString() + "</AddressType>");
                    if (ep.password != null)
                        searchOutXml.Append("<Password>" + ep.password + "</Password> ");
                    else
                        searchOutXml.Append("<Password></Password> ");
                    searchOutXml.Append("<UserName>" + ep.UserName + "</UserName>"); //ZD 100814
                    searchOutXml.Append("<Address>" + ep.address + "</Address>");
                    searchOutXml.Append("<URL>" + ep.endptURL + "</URL>");
                    searchOutXml.Append("<IsOutside>" + ep.outsidenetwork.ToString() + "</IsOutside>");
                    if (ep.GateKeeeperAddress != null)//ZD 100132
                        searchOutXml.Append("<GateKeeeperAddress>" + ep.GateKeeeperAddress.ToString() + "</GateKeeeperAddress>");//ZEN 100132
                    else
                        searchOutXml.Append("<GateKeeeperAddress></GateKeeeperAddress>");
                    
                    searchOutXml.Append("<VideoEquipment>" +
                                                    ep.videoequipmentid.ToString() + "</VideoEquipment> ");

                    searchOutXml.Append("<Manufacturer>" + ep.ManufacturerID.ToString() + "</Manufacturer>");//ZD 100736

                    searchOutXml.Append("<LineRate>" + ep.linerateid.ToString() + "</LineRate>");
                    searchOutXml.Append("<Bridge>" + ep.bridgeid.ToString() + "</Bridge>");
                    if (ep.bridgeid > 0) //FB 2839 Start
                    {
                        MCU = m_ImcuDao.GetById(ep.bridgeid);

                        searchOutXml.Append("<BridgeType>" + MCU.MCUType.id + "</BridgeType>"); //ZD 100619
                        ProfilecriterionList = new List<ICriterion>(); //ZD 104256
                        ProfilecriterionList.Add(Expression.Eq("MCUId", ep.bridgeid));
                        ProfilecriterionList.Add(Expression.Eq("ProfileId", MCU.ConfServiceID));
                        List<vrmMCUProfiles> MCUProf = m_IMcuProfilesDAO.GetByCriteria(ProfilecriterionList);

                        searchOutXml.Append("<BridgeProfileID>" + MCU.ConfServiceID + "</BridgeProfileID>"); //FB 2839 Ends
                        if (MCUProf.Count > 0)
                            searchOutXml.Append("<BridgeProfileName>" + MCUProf[0].ProfileName + "</BridgeProfileName>");
                        else
                            searchOutXml.Append("<BridgeProfileName></BridgeProfileName>"); //FB 2839 Ends

                        //ZD 104256 Starts
                        ProfilecriterionList = new List<ICriterion>();
                        ProfilecriterionList.Add(Expression.Eq("MCUId", ep.bridgeid));
                        ProfilecriterionList.Add(Expression.Eq("PoolOrderId", MCU.PoolOrderID));
                        List<vrmMCUPoolOrders>  lstPoolOrders = m_IMCUPoolOrderDao.GetByCriteria(ProfilecriterionList);

                        searchOutXml.Append("<BridgePoolOrder>" + MCU.PoolOrderID + "</BridgePoolOrder>");
                        if (lstPoolOrders.Count > 0)
                            searchOutXml.Append("<BridgePoolOrderName>" + lstPoolOrders[0].PoolOrderName + "</BridgePoolOrderName>");
                        else
                            searchOutXml.Append("<BridgePoolOrderName></BridgePoolOrderName>"); //FB 2839 Ends
                        //ZD 104256 Ends
                    }
                    else
                    {
                        searchOutXml.Append("<BridgeProfileID>0</BridgeProfileID>");
                        searchOutXml.Append("<BridgeProfileName></BridgeProfileName>"); //FB 2839 Ends
                        searchOutXml.Append("<BridgePoolOrder>0</BridgePoolOrder>");//ZD 104256
                        searchOutXml.Append("<BridgePoolOrderName></BridgePoolOrderName>"); //ZD 104256
                        searchOutXml.Append("<BridgeType>0</BridgeType>"); //ZD 100619
                    }//FB 2839 Ends
                    searchOutXml.Append("<ConnectionType>" + ep.connectiontype.ToString() + "</ConnectionType>");
                    searchOutXml.Append("<DefaultProtocol>" + ep.protocol.ToString() + "</DefaultProtocol>");
                    searchOutXml.Append("<MCUAddress>" + ep.MCUAddress + "</MCUAddress>");
                    searchOutXml.Append("<MCUAddressType>" + ep.MCUAddressType.ToString() + "</MCUAddressType>");
                    searchOutXml.Append("<ExchangeID>" + ep.ExchangeID + "</ExchangeID>"); //Cisco Telepresence fix
                    //Code Added For FB1422 -Start
                    searchOutXml.Append("<TelnetAPI>" + ep.TelnetAPI.ToString() + "</TelnetAPI>");
                    searchOutXml.Append("<SSHSupport>" + ep.SSHSupport.ToString() + "</SSHSupport>");//ZD 101363
                    //Code Added For FB1422 -End   
                    searchOutXml.Append("<IsCalendarInvite>" + ep.CalendarInvite.ToString() + "</IsCalendarInvite>");
                    searchOutXml.Append("<ApiPortno>" + ep.ApiPortNo.ToString() + "</ApiPortno>");//API Port...
                    searchOutXml.Append("<conferenceCode>" + ep.ConferenceCode + "</conferenceCode>");//FB 1642-Audio add on
                    searchOutXml.Append("<leaderPin>" + ep.LeaderPin + "</leaderPin>");//FB 1642-Audio add on
                    searchOutXml.Append("<MultiCodec>");//FB 2400 start
                    if (ep.MultiCodecAddress != null)
                    {
                        String[] multiCodec = ep.MultiCodecAddress.Split('�');
                        for (int i = 0; i < multiCodec.Length; i++)
                        {
                            if (multiCodec[i].Trim() != "")
                                searchOutXml.Append("<Address>" + multiCodec[i].Trim() + "</Address>");

                        }
                    }
                    searchOutXml.Append("</MultiCodec>");
                    searchOutXml.Append("<RearSecCameraAddress>" + ep.RearSecCameraAddress + "</RearSecCameraAddress>");
                    searchOutXml.Append("<isTelePresence>" + ep.isTelePresence + "</isTelePresence>");//FB 2400 end
                    searchOutXml.Append("<Secured>" + ep.Secured + "</Secured>");//FB 2595
                    searchOutXml.Append("<NetworkURL>" + ep.NetworkURL + "</NetworkURL>");//FB 2595
                    searchOutXml.Append("<Securedport>" + ep.Secureport + "</Securedport>");//FB 2595
                    searchOutXml.Append("<ProfileType>" + ep.ProfileType.ToString() + "</ProfileType>"); //ZD 100815
                    searchOutXml.Append("<IsP2PDefault>" + ep.IsP2PDefault.ToString() + "</IsP2PDefault>"); //ZD 100815
                    searchOutXml.Append("<IsTestEquipment>" + ep.IsTestEquipment.ToString() + "</IsTestEquipment>"); //ZD 100815
                    searchOutXml.Append("<EptResolution>" + ep.Resolution + "</EptResolution>"); //ZD 100040
                    searchOutXml.Append("<LCR>" + ep.LCR + "</LCR>"); //ZD 100040
                    searchOutXml.Append("<SysLocation>" + ep.SysLocationId + "</SysLocation>"); //ZD 104821
                    searchOutXml.Append("</Profile>");
                }
                if (idx > 0)
                {
                    searchOutXml.Append("</Profiles>");
                    searchOutXml.Append("</Endpoint>");
                }
                else // empty tags
                {
                    searchOutXml.Append("<Profiles/>");
                    searchOutXml.Append("<Endpoint/>");
                }

                

                searchOutXml.Append("</EndpointDetails>");
                obj.outXml = searchOutXml.ToString();
                //FB 1820 - Ends
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetLineRate
        public bool GetLineRate(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.

                List<vrmLineRate> LineRate = vrmGen.getLineRate();

                obj.outXml += "<LineRate>";
                foreach (vrmLineRate lr in LineRate)
                {
                    obj.outXml += "<Rate>";
                    obj.outXml += "<LineRateID>" + lr.Id.ToString() + "</LineRateID>";
                    obj.outXml += "<LineRateName>" + lr.LineRateType.ToString() + "</LineRateName>";
                    obj.outXml += "</Rate>";
                }
                obj.outXml += "</LineRate>";

                return true;

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetVideoEquipment
        public bool GetVideoEquipment(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.

                List<vrmVideoEquipment> videoList = vrmGen.getVideoEquipment();

                obj.outXml += "<VideoEquipment>";
                foreach (vrmVideoEquipment ve in videoList)
                {
                    obj.outXml += "<Equipment>";
                    obj.outXml += "<VideoEquipmentID>" + ve.Id.ToString() + "</VideoEquipmentID>";
                    obj.outXml += "<VideoEquipmentName>" + ve.VEName + "</VideoEquipmentName>";
                    obj.outXml += "<EquipmentDisplayName>" + ve.DisplayName + "</EquipmentDisplayName>";//ZD 100736
                    obj.outXml += "<Familyid>" + ve.Familyid + "</Familyid>";//ZD 104091
                    obj.outXml += "</Equipment>";
                }
                obj.outXml += "</VideoEquipment>";


            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion
        
        //ZD 100736 - Start
        #region GetManufacturer
        public bool GetManufacturer(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {               

                List<vrmManufacturer> ManufacturerList = vrmGen.GetManufacturer();

                obj.outXml += "<Manufacturer>";
                foreach (vrmManufacturer ML in ManufacturerList)
                {
                    obj.outXml += "<Equipment>";
                    obj.outXml += "<ManufacturerID>" + ML.MID.ToString() + "</ManufacturerID>";
                    obj.outXml += "<ManufacturerName>" + ML.ManufacturerName + "</ManufacturerName>";
                    obj.outXml += "</Equipment>";
                }
                obj.outXml += "</Manufacturer>";


            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);                
                obj.outXml = "";
                bRet = false;
            }

            return bRet;

        }
        #endregion
        //ZD 100736 - End

        //ZD 100736 - START
        
        #region GetManufacturerModel
        public bool GetManufacturerModel(ref vrmDataObject obj)
        {
            bool bRet = true;
            int ManufacturerID = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                
                XmlNode node;

                node = xd.SelectSingleNode("//GetManufacturerModel/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//GetManufacturerModel/ManufacturerID");
                    int.TryParse(node.InnerXml.Trim(), out ManufacturerID);

                StringBuilder sbBuilder = new StringBuilder();
                obj.outXml = string.Empty;


                List<vrmVideoEquipment> videoList = vrmGen.getVideoEquipment();

                videoList = videoList.Where(vi => vi.Familyid == ManufacturerID).ToList();

                sbBuilder.Append("<GetManufacturerModel>");

                foreach (vrmVideoEquipment equipment in videoList)
                {
                    sbBuilder.Append("<EndpointModel>");
                    sbBuilder.Append("<VideoEquipmentID>" + equipment.Id.ToString() + "</VideoEquipmentID>");
                    sbBuilder.Append("<Code>" + equipment.VEName + "</Code>");
                    sbBuilder.Append("<EquipmentDisplayName>" + equipment.DisplayName + "</EquipmentDisplayName>");
                    sbBuilder.Append("</EndpointModel>");
                }
                sbBuilder.Append("</GetManufacturerModel>");
                obj.outXml = sbBuilder.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = string.Empty;
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = string.Empty;
                bRet = false;
            }

            return bRet;

        }
        #endregion
        //ZD 100736 - END
         

        #region SetEndpoint
        public bool SetEndpoint(ref vrmDataObject obj)
        {
            bool bRet = true;
            int maxProfileId = 0;
            int isPublicEP = 0, createType = 1, identifierValue = 0;//FB 2594 //ZD 101527
            int P2Pprofile = 0, MCUProfile = 0, BothProfile = 0, IsBJNEpt = 0, RoomID = 0; //ZD 100815 //ZD 103263
            bool isNewEndpoint = false, isNewProfile = false;//ZD 101026
            List<vrmVideoEquipment> videoEqList = vrmGen.getVideoEquipment();//ZD 100815
            List<vrmVideoEquipment> slecEqList = new List<vrmVideoEquipment>();//ZD 100815
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//SetEndpoint/EndpointID");
                string EndpointID = node.InnerXml.Trim();
                if (EndpointID.ToLower() == "new")
                {
                    EndpointID = "0";
                }
                //ZD 100456 - for import Endpoint
                String editFrom = "";
                node = xd.SelectSingleNode("//SetEndpoint/EditFrom");
                if (node != null)
                    editFrom = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetEndpoint/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = 11;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//SetEndpoint/EndpointName");
                string EndpointName = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//SetEndpoint/EntityType");
                string EntityType = node.InnerXml.Trim();
				//ZD 101026
                int UserID = 0;
                node = xd.SelectSingleNode("//SetEndpoint/UserID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out UserID);
                //ZD 100456
                //ZD 103263 start
                node = xd.SelectSingleNode("//SetEndpoint/IsBJNEpt");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out IsBJNEpt);
                node = xd.SelectSingleNode("//SetEndpoint/RoomID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out RoomID);
                //ZD 103263 End
                if (EndpointID == "")
                {
                    vrmUser vrmusr = m_vrmUserDAO.GetByUserId(UserID);
                    EndpointID = vrmusr.endpointId.ToString();
                }

                if (EntityType.Trim().ToUpper() != "U" && EndpointID == "0")
                {

                    List<ICriterion> selcnt = new List<ICriterion>();
                    //selcnt.Add(Expression.Eq("deleted", 0));
                    selcnt.Add(Expression.Eq("deleted", 0));
                    selcnt.Add(Expression.Or(Expression.Eq("isDefault", 1), Expression.Eq("IsP2PDefault", 1))); //ZD 100815_M
                    selcnt.Add(Expression.Eq("orgId", organizationID));//code added for organization
                    selcnt.Add(Expression.Eq("Extendpoint", 0)); //FB 2549
                    selcnt.Add(Expression.Eq("PublicEndPoint", 0)); //FB 2594

                    List<vrmEndPoint> checkEptCount = m_IeptDao.GetByCriteria(selcnt);

                    checkEptCount = ((checkEptCount.Where(x => checkEptCount.Count(z => z.endpointid == x.endpointid) > 1).ToList()).Where(x => x.ProfileType == 1).ToList()).Union(checkEptCount.Where(x => checkEptCount.Count(z => z.endpointid == x.endpointid) == 1).ToList()).ToList();//ZD 100815_M

                    OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    if (checkEptCount.Count >= orgdt.MaxEndpoints)
                    {
                        //FB 1881 start
                        myVRMException ex = new myVRMException(458);
                        m_log.Error("Error EndPoint Limit exceeeded: ", ex);
                        //obj.outXml = myVRMException.toXml(ex.Message);
                        obj.outXml = ex.FetchErrorMsg();
                        //FB 1881 end
                        return false;
                    }
                }

                XmlNodeList itemList, multiCodelist; //FB 2400

                List<vrmEndPoint> endPoints = new List<vrmEndPoint>();

                itemList = xd.GetElementsByTagName("Profile");
                int pId = 0, eId = 0;

                foreach (XmlNode innerNode in itemList)
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    vrmEndPoint ve = new vrmEndPoint();
                    XmlElement itemElement = (XmlElement)innerNode;

                    //ZD 100456 - for user import
                    
                    String oldAddress = "";
                    if (itemElement.GetElementsByTagName("EditFrom")[0] != null)
                        editFrom = itemElement.GetElementsByTagName("EditFrom")[0].InnerText;

                    if (EndpointID == "0")
                    {
                        if (pId == 0)
                        {
                            // FB 114 (AG 04/14/08)
                            // check if EP name exists (but not in case of user end point)
                            if (EntityType != "U")
                            {
                                List<ICriterion> checkName = new List<ICriterion>();
                                checkName.Add(Expression.Eq("name", EndpointName));
                                checkName.Add(Expression.Not(Expression.Eq("profileId", 0)));
                                checkName.Add(Expression.Eq("deleted", 0));
                                checkName.Add(Expression.Eq("orgId", organizationID));
                                List<vrmEndPoint> ep = m_IeptDao.GetByCriteria(checkName);

                                if (ep.Count > 0)
                                {
                                    //FB 1881 start
                                    //myVRMException e = new myVRMException("Error endpoint name: " + 
                                    //                        EndpointName + " already exists");
                                    //obj.outXml = myVRMException.toXml(e.Message);
                                    myVRMException myVRMEx = new myVRMException(434);
                                    obj.outXml = myVRMEx.FetchErrorMsg();
                                    //FB 1881 end
                                    return false;
                                }
                            }
                            m_IeptDao.addProjection(Projections.Max("endpointid"));
                            IList maxId = m_IeptDao.GetObjectByCriteria(new List<ICriterion>());
                            if (maxId[0] != null)
                                eId = ((int)maxId[0]) + 1;
                            else
                                eId = 1;
                            m_IeptDao.clearProjection();

                            isNewEndpoint = true;//ZD 100664
                        }
                        pId++;
                        ve.endpointid = eId;
                        ve.profileId = pId;
                        ve.PublicEndPoint = 0; //FB 2594

                        //ZD 100040 start
                        if (editFrom == "D" || editFrom == "ND") //ND - NewData //ZD 104091
                                ve.Resolution = 5;
                        //ZD 100040 end
                    }
                    else
                    {
                        ve.endpointid = Int32.Parse(EndpointID);

                        //ZD 100456                        
                        if (editFrom == "D")
                        {
                            ve = m_IeptDao.GetByEptId(Int32.Parse(EndpointID));
                            oldAddress = ve.address;
                        }

                        //FB 2594 Starts
                        List<ICriterion> criterionPublicEp = new List<ICriterion>();
                        criterionPublicEp.Add(Expression.Eq("endpointid", ve.endpointid));
                        List<vrmEndPoint> eplist = m_IeptDao.GetByCriteria(criterionPublicEp);
                        isPublicEP = eplist[0].PublicEndPoint;
                        createType = eplist[0].CreateCategory; //ZD 101527
                        identifierValue = eplist[0].identifierValue; //ZD 101
                        //FB 2594 Ends
                        // 114 do not allow dupllicate endpoint in update
                        if (EntityType.ToUpper() != "U")
                        {
                            List<ICriterion> checkName = new List<ICriterion>();
                            checkName.Add(Expression.Eq("name", EndpointName));
                            checkName.Add(Expression.Eq("orgId", organizationID));
                            checkName.Add(Expression.Eq("deleted", 0));
                            checkName.Add(Expression.Not(Expression.Eq("profileId", 0)));
                            checkName.Add(Expression.Not(Expression.Eq("endpointid", ve.endpointid)));
                            List<vrmEndPoint> ep = m_IeptDao.GetByCriteria(checkName);

                            if (ep.Count > 0)
                            {
                                //FB 1881 start
                                //myVRMException e = new myVRMException("Error endpoint name: " + EndpointName + " already exists");
                                //obj.outXml = myVRMException.toXml(e.Message);
                                myVRMException myVRMEx = new myVRMException(434);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                //FB 1881 end
                                return false;
                            }
                        }

                        string sProfileId = itemElement.GetElementsByTagName("ProfileID")[0].InnerText;
                        if (sProfileId.ToLower() == "new")
                        {
                            ve.uId = 0;
                            if (EntityType.ToUpper() != "U")
                            {
                                List<ICriterion> maxProfile = new List<ICriterion>();
                                maxProfile.Add(Expression.Eq("endpointid", ve.endpointid));
                                m_IeptDao.addProjection(Projections.Max("profileId"));
                                IList maxId = m_IeptDao.GetObjectByCriteria(maxProfile);
                                if (maxId[0] != null)
                                    maxProfileId = ((int)maxId[0]) + 1;
                                else
                                    maxProfileId = 1;
                                m_IeptDao.clearProjection();

                                ve.profileId = maxProfileId;

                                isNewProfile = true; //ZD 101026
                            }
                            // user profile
                            else
                            {
                                ve.profileId = 0;
                            }

                            //ZD 100040 start
                            if (editFrom == "D" || editFrom == "ND") //ND - NewData //ZD 104091
                                ve.Resolution = 5;
                            //ZD 100040 end
                        }
                        else
                        {
                            // get uid
                            ve.profileId = Int32.Parse(sProfileId);

                            criterionList.Add(Expression.And(
                                              Expression.Eq("endpointid", ve.endpointid),
                                              Expression.Eq("profileId", ve.profileId)));

                            List<vrmEndPoint> epl = m_IeptDao.GetByCriteria(criterionList);
                            if (epl.Count > 0)
                            {
                                ve.uId = epl[0].uId;
                                ve.password = epl[0].password; //FB 3054

                                //ZD 100040 start
                                if (editFrom == "D")
                                    ve.Resolution = epl[0].Resolution;
                                //ZD 100040 end
                            }

                            else
                            {
                                //FB 1881 start
                                //myVRMException e = new myVRMException("Cannot update endpoint: " + EndpointID + "/" + ve.profileId.ToString() +
                                // "  Endpoint not found");
                                myVRMException e = new myVRMException(508);
                                m_log.Error(e.Message);
                                obj.outXml = e.FetchErrorMsg();
                                //FB 1881 end
                                bRet = false;
                            }

                        }
                    }
                    ve.name = EndpointName;
                    //ZD 100456
                    if (editFrom == "" || editFrom == "ND") //ND - NewData //ZD 104091
                        ve.profileName = itemElement.GetElementsByTagName("ProfileName")[0].InnerText;

                    if (itemElement.GetElementsByTagName("Password")[0] != null) //FB 3054
                        ve.password = itemElement.GetElementsByTagName("Password")[0].InnerText;
                    //ZD 100814
                    if (itemElement.GetElementsByTagName("UserName")[0] != null)
                        ve.UserName = itemElement.GetElementsByTagName("UserName")[0].InnerText;

                    ve.address = itemElement.GetElementsByTagName("Address")[0].InnerText;
                    ve.outsidenetwork =
                                     Int32.Parse(itemElement.GetElementsByTagName("IsOutside")[0].InnerText);
                    ve.addresstype = Int32.Parse(itemElement.GetElementsByTagName("AddressType")[0].InnerText);
                    ve.connectiontype =
                                        Int32.Parse(itemElement.GetElementsByTagName("ConnectionType")[0].InnerText);
                    ve.videoequipmentid =
                                        Int32.Parse(itemElement.GetElementsByTagName("VideoEquipment")[0].InnerText);
                    //ZD 100815 Start
                    if (itemElement.GetElementsByTagName("ProfileType")[0] != null)
                       ve.ProfileType = Int32.Parse(itemElement.GetElementsByTagName("ProfileType")[0].InnerText);
                   
                    //ZD 100815 End
                    if (itemElement.GetElementsByTagName("Manufacturer")[0] != null)
                        ve.ManufacturerID = Int32.Parse(itemElement.GetElementsByTagName("Manufacturer")[0].InnerText);//ZD 100736

                    ve.linerateid = Int32.Parse(itemElement.GetElementsByTagName("LineRate")[0].InnerText);
                    if(!string.IsNullOrEmpty(itemElement.GetElementsByTagName("Bridge")[0].InnerText)) //ZD 101175
                        ve.bridgeid = Int32.Parse(itemElement.GetElementsByTagName("Bridge")[0].InnerText);
                    ve.MCUAddress = itemElement.GetElementsByTagName("MCUAddress")[0].InnerText;
                    ve.isDefault = Int32.Parse(itemElement.GetElementsByTagName("Default")[0].InnerText);
                    
                    //ZD 100456
                    if (editFrom == "" || editFrom == "ND") //ND - NewData //ZD 104091
                    {
                        //ZD 104091 -- Moved the code into edit From ie, for create only
                        //ZD 100815 Starts
                        if (itemElement.GetElementsByTagName("IsP2PDefault")[0] != null)
                            ve.IsP2PDefault = Int32.Parse(itemElement.GetElementsByTagName("IsP2PDefault")[0].InnerText);

                        slecEqList = videoEqList.Where(vq => vq.Id == ve.videoequipmentid).ToList();
                        if (slecEqList.Count > 0)
                        {
                            if (slecEqList[0].VEName.Contains("*"))
                                ve.IsTestEquipment = 1;
                            else
                                ve.IsTestEquipment = 0;
                        }
                        else
                            ve.IsTestEquipment = 0;

                        if (ve.ProfileType == 0 && ve.isDefault == 1)
                            P2Pprofile = P2Pprofile + 1;
                        else if (ve.ProfileType == 1 && ve.isDefault == 1)
                            MCUProfile = MCUProfile + 1;
                        else if (ve.IsP2PDefault == 2)
                            BothProfile = BothProfile + 1;
                        //ZD 100815 Ends

                        ve.deleted = Int32.Parse(itemElement.GetElementsByTagName("Deleted")[0].InnerText);
                        ve.encrypted = Int32.Parse(itemElement.GetElementsByTagName("EncryptionPreferred")[0].InnerText);
                        ve.endptURL = itemElement.GetElementsByTagName("URL")[0].InnerText;
                    
                        ve.GateKeeeperAddress = "";//ZD 100132
                        if (itemElement.GetElementsByTagName("GateKeeeperAddress")[0] != null)
                            ve.GateKeeeperAddress = itemElement.GetElementsByTagName("GateKeeeperAddress")[0].InnerText;
                        
                        ve.MCUAddressType =
                                         Int32.Parse(itemElement.GetElementsByTagName("MCUAddressType")[0].InnerText);

                        ve.protocol = Int32.Parse(itemElement.GetElementsByTagName("DefaultProtocol")[0].InnerText);
                        //Code Added For FB1422- Start
                        ve.TelnetAPI = Int32.Parse(itemElement.GetElementsByTagName("TelnetAPI")[0].InnerText);
                        //Code Added For FB1422- End
                        ve.SSHSupport = int.Parse(itemElement.GetElementsByTagName("SSHSupport")[0].InnerText);//ZD 101363

                        ve.ExchangeID = itemElement.GetElementsByTagName("ExchangeID")[0].InnerText; //Cisco Telepresence fix
                        if (itemElement.GetElementsByTagName("IsCalendarInvite").Count != 0)  //Cisco ICAL FB 1602
                            ve.CalendarInvite = Int32.Parse(itemElement.GetElementsByTagName("IsCalendarInvite")[0].InnerText);  //Cisco ICAL FB 1602
                        //Code added for organization module
                        //FB 1642 Audio Add On Starts..
                        String conferenceCode = "";
                        String leaderPin = "";
                        if (itemElement.GetElementsByTagName("conferenceCode").Count > 0)
                            conferenceCode = itemElement.GetElementsByTagName("conferenceCode")[0].InnerText;
                        ve.ConferenceCode = conferenceCode;
                        if (itemElement.GetElementsByTagName("leaderPin").Count > 0)
                            leaderPin = itemElement.GetElementsByTagName("leaderPin")[0].InnerText;
                        ve.LeaderPin = leaderPin;
                        //FB 1642 Audio Add On Ends..
                        ve.PublicEndPoint = isPublicEP;//FB 2594
                        ve.CreateCategory = createType; //ZD 101527
                        ve.identifierValue = identifierValue;
                        //API Port Starts...
                        ve.orgId = organizationID;
                        int apiPort = 23;

                        if (itemElement.GetElementsByTagName("ApiPortno").Count > 0)
                            int.TryParse(itemElement.GetElementsByTagName("ApiPortno")[0].InnerText, out apiPort);
                        if (apiPort <= 0)
                            apiPort = 23;
                        ve.ApiPortNo = apiPort;
                        //API Port Ends...
                    }
                    
                    //FB 2400 start
                    if (editFrom == "" || editFrom == "ND") //ND - NewData //ZD 104091
                    {
                        int isTelePresence = 0;
                        if (itemElement.GetElementsByTagName("isTelePresence")[0] != null)
                            if (itemElement.GetElementsByTagName("isTelePresence")[0].InnerText.Trim() != "")
                                Int32.TryParse(itemElement.GetElementsByTagName("isTelePresence")[0].InnerText, out isTelePresence);

                        ve.isTelePresence = isTelePresence;
                        ve.MultiCodecAddress = "";

                        multiCodelist = innerNode.SelectNodes("MultiCodec/Address");//FB 2602
                        for (int i = 0; i < multiCodelist.Count; i++)
                        {
                            if (multiCodelist[i] != null)
                            {
                                if (multiCodelist[i].InnerText.Trim() != "")
                                {
                                    if (i > 0)
                                        ve.MultiCodecAddress += "�";

                                    ve.MultiCodecAddress += multiCodelist[i].InnerText.Trim();
                                }
                            }
                        }


                        if (isTelePresence == 1 && ve.MultiCodecAddress.Trim() == "")
                            ve.MultiCodecAddress = ve.address;
                    }
                    else
                    {
                        if (ve.isTelePresence == 1)
                        {
                            ve.MultiCodecAddress = ve.MultiCodecAddress.Replace(oldAddress, ve.address);
                        }
                    }
					//ZD 100456
                    if (editFrom == "" || editFrom == "ND") //ND - NewData //ZD 104091
                    {
                        ve.RearSecCameraAddress = "";
                        if (itemElement.GetElementsByTagName("RearSecCameraAddress")[0] != null)
                            ve.RearSecCameraAddress = itemElement.GetElementsByTagName("RearSecCameraAddress")[0].InnerText;

                        //FB 2400 end

                        //FB 2501 EM7 Starts
                        int EptOnlinestatus = 2; //ZD 100629 //ZD 100825
                        if (xd.SelectSingleNode("//SetEndpoint/EndpointStatus") != null)
                            Int32.TryParse(xd.SelectSingleNode("//SetEndpoint/EndpointStatus").InnerText, out EptOnlinestatus);
                        ve.EptOnlineStatus = EptOnlinestatus;
                        //FB 2501 EM7 Ends
                        //FB 2616 EM7 Starts
                        int Eptcurrentstatus = 0;
                        if (xd.SelectSingleNode("//SetEndpoint/Eptcurrentstatus") != null)
                            Int32.TryParse(xd.SelectSingleNode("//SetEndpoint/Eptcurrentstatus").InnerText, out Eptcurrentstatus);
                        ve.EptCurrentStatus = Eptcurrentstatus;
                        //FB 2616 EM7 Ends
                        //FB 2595 Start
                        int Secured = 0, securedport = 0;
                        if (itemElement.GetElementsByTagName("Secured")[0] != null)
                            int.TryParse(itemElement.GetElementsByTagName("Secured")[0].InnerText, out Secured);
                        ve.Secured = Secured;

                        if (itemElement.GetElementsByTagName("Securedport")[0] != null)
                            int.TryParse(itemElement.GetElementsByTagName("Securedport")[0].InnerText, out securedport);
                        ve.Secureport = securedport;

                        ve.NetworkURL = "";
                        if (itemElement.GetElementsByTagName("NetworkURL")[0] != null)
                            ve.NetworkURL = itemElement.GetElementsByTagName("NetworkURL")[0].InnerText;
                        //ZD 100040 start
                        int Resolution = 0, LCR =0;
                        if (itemElement.GetElementsByTagName("EptResolution")[0] != null)
                            int.TryParse(itemElement.GetElementsByTagName("EptResolution")[0].InnerText, out Resolution);
                        if(editFrom == "") //ZD 104091
                            ve.Resolution = Resolution;

                        if (itemElement.GetElementsByTagName("LCR")[0] != null)
                            int.TryParse(itemElement.GetElementsByTagName("LCR")[0].InnerText, out LCR);
                        ve.LCR = LCR;
                        //ZD 100040 End
                        
                    }
                    //FB 2595 End

                    //ZD 104821 Starts
                    int syslocID = 0; string SysLocationName = "";
                    if (ve.bridgeid > 0)
                    {
                        if (itemElement.GetElementsByTagName("SysLocation")[0] != null)
                            int.TryParse(itemElement.GetElementsByTagName("SysLocation")[0].InnerText, out syslocID);

                        if (itemElement.GetElementsByTagName("SysLocationName")[0] != null)
                        {
                            SysLocationName = itemElement.GetElementsByTagName("SysLocationName")[0].InnerText;

                            if (!string.IsNullOrEmpty(SysLocationName))
                            {
                                sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);
                                sqlCon.OpenConnection();
                                sqlCon.OpenTransaction();
                                String stmtArchive = "Select SysLocId from Gen_SystemLocation_D where Name =  '" + SysLocationName + "'  and MCUId=" + ve.bridgeid;

                                DataSet ds = sqlCon.ExecuteDataSet(stmtArchive);
                                sqlCon.CommitTransaction();
                                sqlCon.CloseConnection();

                                if (ds != null && ds.Tables.Count > 0)
                                    for (int c = 0; c < ds.Tables[0].Rows.Count; c++)
                                        int.TryParse(ds.Tables[0].Rows[c]["SysLocId"].ToString(), out syslocID);
                            }
                        }
                    }

                    ve.SysLocationId = syslocID;
                    //ZD 104821 Ends
                    ve.Lastmodifieddate = DateTime.UtcNow; //ZD 100664
                    ve.LastModifiedUser = UserID;
                    endPoints.Add(ve);
                }
                //ZD 100815 Starts
                if (P2Pprofile > 0)
                    P2Pprofile = MCUProfile + BothProfile + P2Pprofile;
                if (P2Pprofile > 1 || MCUProfile > 1 || BothProfile > 1)
                {
                    myVRMException e = new myVRMException(737);
                    m_log.Error(e.Message);
                    obj.outXml = e.FetchErrorMsg();
                    bRet = false;
                }
                //ZD 100815 Ends

                if (EntityType.ToUpper() == "U")
                {
                    //ZD 103263 Start
                    endPoints[0].profileId = 0; // 0 for user ...
                    endPoints[0].Lastmodifieddate = DateTime.UtcNow; //ZD 100664
                    m_IeptDao.SaveOrUpdate(endPoints[0]);

                    if (IsBJNEpt == 1)
                    {
                        vrmRoom room = m_IRoomDAO.GetByRoomId(RoomID);
                        if (room != null)
                        {
                            if (eId == 0)
                                room.endpointid = endPoints[0].endpointid;
                            else
                                room.endpointid = eId;

                            m_IRoomDAO.SaveOrUpdate(room);
                        }
                    }
                    else
                    {
                        vrmUser user = m_vrmUserDAO.GetByUserId(UserID);
                        //FB 1462 - to handle error if the user is deleted - Start
                        if (user != null)
                        {
                            if (eId == 0)
                                user.endpointId = endPoints[0].endpointid;
                            else
                                user.endpointId = eId;

                            m_vrmUserDAO.SaveOrUpdate(user);
                        }
                        //FB 1462 - to handle error if the user is deleted - End
                    }
                    //ZD 103263 End
                }
                else
                {
                    m_IeptDao.SaveOrUpdateList(endPoints);
                }

                SetAuditEndpoint(isNewEndpoint, isNewProfile, endPoints);//ZD 100664

                if (endPoints.Count < 1)
                    return false;

                obj.outXml = "<SetEndpoint><EndpointID>" + ((vrmEndPoint)endPoints[0]).endpointid.ToString() +
                   "</EndpointID></SetEndpoint>";
                // FB 1699
                if (EntityType.ToUpper() != "U")
                {
                    List<ICriterion> uptcriterion = null;
                    List<vrmConfRoom> rooms = null;
                    vrmConfRoom room = null;
                    DateTime today = DateTime.Now.AddHours(1);
                    timeZone.changeToGMTTime(sysSettings.TimeZone, ref today);

                    for (Int32 uptcnts = 0; uptcnts < endPoints.Count; uptcnts++)
                    {
                        uptcriterion = new List<ICriterion>();
                        uptcriterion.Add(Expression.Eq("endpointId", endPoints[uptcnts].endpointid));
                        uptcriterion.Add(Expression.Eq("profileId", endPoints[uptcnts].profileId));
                        uptcriterion.Add(Expression.Eq("addressType", endPoints[uptcnts].addresstype));
                        uptcriterion.Add(Expression.Not(Expression.Eq("ipisdnaddress", endPoints[uptcnts].address)));
                        uptcriterion.Add(Expression.Gt("StartDate", today));

                        rooms = m_IconfRoom.GetByCriteria(uptcriterion);

                        if (rooms.Count > 0)
                        {
                            for (Int32 uptrmcnts = 0; uptrmcnts < rooms.Count; uptrmcnts++)
                            {
                                room = rooms[uptrmcnts];
                                room.ipisdnaddress = endPoints[uptcnts].address;
                                m_IconfRoom.Update(room);

                            }

                        }

                    }
                }
                // FB 1699

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region SearchEndpoint
        public bool SearchEndpoint(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder searchOutXml = new StringBuilder(); // String concatenation changed to StringBuilder for Performance - FB 1820
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int IsPublicEP=0; //FB 2594
                node = xd.SelectSingleNode("//SearchEndpoint/EndpointName");
                string EndpointName = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SearchEndpoint/EndpointType");
                string addressType = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//SearchEndpoint/PageNo");
                string PageNo = node.InnerXml.Trim();
                
                node = xd.SelectSingleNode("//SearchEndpoint/PublicEndpoint"); //FB 2594
                if(node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out IsPublicEP);

                node = xd.SelectSingleNode("//SearchEndpoint/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes

                if (EndpointName.Length > 0)
                    criterionList.Add(Expression.Like("name", "%%" + EndpointName.ToLower() + "%%").IgnoreCase());
                if (addressType.Length > 0)
                    criterionList.Add(Expression.Eq("addresstype", Int32.Parse(addressType)));
                criterionList.Add(Expression.Or(Expression.Eq("isDefault", 1), Expression.Eq("IsP2PDefault", 1))); //ZD 100815_M
                criterionList.Add(Expression.Eq("deleted", 0));
                if(IsPublicEP == 0) //FB 2594
                    criterionList.Add(Expression.Eq("PublicEndPoint", IsPublicEP));
                criterionList.Add(Expression.Eq("orgId", organizationID));//Organization Module Fixes

                m_IeptDao.addProjection(Projections.RowCount());
                IList list = m_IeptDao.GetObjectByCriteria(criterionList);
                int MaxRecords = 0;
                if (list.Count > 0)
                    MaxRecords = Int32.Parse(list[0].ToString());
                m_IeptDao.clearProjection();

                if (PageNo.Length > 0)
                {
                    m_IeptDao.pageNo(Int32.Parse(PageNo));
                }


                m_IeptDao.addOrderBy(Order.Asc("endpointid"));

                List<vrmEndPoint> epList = m_IeptDao.GetByCriteria(criterionList);

                epList = ((epList.Where(x => epList.Count(z => z.endpointid == x.endpointid) > 1).ToList()).Where(x => x.ProfileType == 1).ToList()).Union(epList.Where(x => epList.Count(z => z.endpointid == x.endpointid) == 1).ToList()).ToList();//ZD 100815_M

                m_IeptDao.clearOrderBy();
                //FB 1820 - Start
                searchOutXml.Append("<SearchEndpoint>");
                searchOutXml.Append("<Endpoints>");


                foreach (vrmEndPoint ep in epList)
                {
                    searchOutXml.Append("<Endpoint>");
                    searchOutXml.Append("<ID>" + ep.endpointid.ToString() + "</ID>");
                    searchOutXml.Append("<Name>" + ep.name + "</Name>");
                    searchOutXml.Append("</Endpoint>");
                }

                searchOutXml.Append("<Paging>");
                int totalPages = MaxRecords / m_IeptDao.getPageSize();
                if ((MaxRecords % m_IeptDao.getPageSize()) > 0)
                    totalPages++;

                searchOutXml.Append("<TotalPages>" + totalPages.ToString() + "</TotalPages>");
                searchOutXml.Append("<PageNo>" + PageNo + "</PageNo>");
                searchOutXml.Append("<TotalRecords>" + MaxRecords.ToString() + "</TotalRecords>");
                searchOutXml.Append("</Paging>");

                searchOutXml.Append("</Endpoints>");
                searchOutXml.Append("</SearchEndpoint>");

                obj.outXml = searchOutXml.ToString();
                //FB 1820 - End
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetAudioCodecs
        public bool GetAudioCodecs(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetAudioCodecs/UserID");
                string UserID = node.InnerXml.Trim();
                List<vrmAudioAlg> al_List = vrmGen.getAudioAlg();

                obj.outXml += "<GetAudioCodecs>";
                foreach (vrmAudioAlg al in al_List)
                {
                    obj.outXml += "<AudioCodec>";
                    obj.outXml += "<ID>" + al.Id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + al.AudioType + "</Name>";
                    obj.outXml += "</AudioCodec>";
                }

                obj.outXml += "</GetAudioCodecs>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetVideoCodecs
        public bool GetVideoCodecs(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetVideoCodecs/UserID");
                string UserID = node.InnerXml.Trim();

                List<vrmVideoSession> ve_List = vrmGen.getVideoSession();
                obj.outXml += "<GetVideoCodecs>";
                foreach (vrmVideoSession ve in ve_List)
                {
                    obj.outXml += "<VideoCodec>";
                    obj.outXml += "<ID>" + ve.Id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + ve.VideoSessionType + "</Name> ";
                    obj.outXml += "</VideoCodec>";
                }
                obj.outXml += "</GetVideoCodecs>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetVideoModes
        public bool GetVideoModes(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetVideoModes/UserID");
                string UserID = node.InnerXml.Trim();
                List<vrmVideoMode> vm_List = vrmGen.getVideoMode();
                obj.outXml += "<GetVideoModes>";
                foreach (vrmVideoMode vm in vm_List)
                {
                    obj.outXml += "<VideoMode>";
                    obj.outXml += "<ID>" + vm.Id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + vm.modeName + "</Name> ";
                    obj.outXml += "</VideoMode>";
                }
                obj.outXml += "</GetVideoModes>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetMediaTypes
        public bool GetMediaTypes(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetMediaTypes/UserID");
                string UserID = node.InnerXml.Trim();
                List<vrmMediaType> mt_List = vrmGen.getMediaTypes();
                obj.outXml += "<GetMediaTypes>";
                foreach (vrmMediaType mt in mt_List)
                {
                    obj.outXml += "<Type>";
                    obj.outXml += "<ID>" + mt.Id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + mt.mediaType + "</Name> ";
                    obj.outXml += "</Type>";
                }
                obj.outXml += "</GetMediaTypes>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetVideoProtocols
        public bool GetVideoProtocols(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetVideoProtocols/UserID");
                string UserID = node.InnerXml.Trim();
                List<vrmVideoProtocol> vp_List = vrmGen.getVideoProtocols();
                obj.outXml += "<GetVideoProtocols>";
                foreach (vrmVideoProtocol vp in vp_List)
                {
                    obj.outXml += "<Protocol>";
                    obj.outXml += "<ID>" + vp.Id.ToString() + "</ID>";
                    obj.outXml += "<Name>" + vp.VideoProtocolType + "</Name> ";
                    obj.outXml += "</Protocol>";
                }
                obj.outXml += "</GetVideoProtocols>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetMCUCards
        public bool GetMCUCards(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                obj.outXml = "<GetMCUCards>";
                foreach (vrmMCUCardDetail cd in vrmGen.getMCUCardDetail())
                {
                    obj.outXml += "<MCUCard>";
                    obj.outXml += "<ID>" + cd.id.ToString() + "</ID> ";
                    obj.outXml += "<Name>" + cd.CardName + "</Name>";
                    obj.outXml += "</MCUCard>";
                }
                obj.outXml += "</GetMCUCards>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetMCUAvailableResources
        public bool GetMCUAvailableResources(ref vrmDataObject obj)
        {
            bool bRet = true;
            int audioPorts = 0;//Fb 1937
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetMCUAvailableResources/UserID");
                string UserID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//GetMCUAvailableResources/BridgeID");
                string BridgeID = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//GetMCUAvailableResources/ConfID");
                string ConfID = node.InnerXml.Trim();
                DateTime confDate;
                int iDuration;
                node = xd.SelectSingleNode("//GetMCUAvailableResources/StartDate");
                string StartDate = node.InnerXml.Trim();
                CConfID Conf = new CConfID(ConfID);

                //this bypasses the date time for now (confid is NEVER 0. 
                if (ConfID.Length == 0)
                {
                    node = xd.SelectSingleNode("//GetMCUAvailableResources/StartTime");
                    string StartTime = node.InnerXml.Trim();
                    confDate = DateTime.Parse(StartDate + " " + StartTime);
                    node = xd.SelectSingleNode("//GetMCUAvailableResources/Duration");
                    string Duration = node.InnerXml.Trim();
                    iDuration = Int32.Parse(Duration);

                    vrmUser myVrmUser = m_vrmUserDAO.GetByUserId(Int32.Parse(UserID));

                    if (myVrmUser != null)   //FB 1462 - related fix
                        timeZone.changeToGMTTime(myVrmUser.TimeZone, ref confDate);
                }
                else
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", Conf.ID));
                    if (Conf.instance == 0)
                        Conf.instance = 1;
                    criterionList.Add(Expression.Eq("instanceid", Conf.instance));

                    List<vrmConference> confList = m_vrmConfDAO.GetByCriteria(criterionList);

                    //ZD 101835
                    if (confList.Count == 0)
                    {
                        Conference cc = new Conference(ref obj);
                        cc.SearchArchiveConference(ref confList, criterionList);
                    }

                    vrmConference conf = confList[0];

                    confDate = conf.confdate;
                    iDuration = conf.duration;
                }

                vrmMCUResources vMCU = new vrmMCUResources(Int32.Parse(BridgeID));
                vrmMCU MCU = m_ImcuDao.GetById(Int32.Parse(BridgeID));

                vMCU.bridgeType = MCU.MCUType.bridgeInterfaceId;

                List<vrmMCUResources> vMCUList = new List<vrmMCUResources>();
                loadVRMResourceData(MCU, ref vMCU);
                vMCUList.Add(vMCU);
                if (GetCompleteMCUusage(confDate, iDuration, Conf.ID, Conf.instance, ref vMCUList))// FB 1937
                {
                    vrmMCU MCUData = MCU;
                    string CardsAvailable, Description;
                    Description = string.Empty;
                    CardsAvailable = string.Empty;
                    //Conditions Changed for FB 2472 Start
                    if (vMCU.bridgeType == MCUType.MSE8000) //FB 2008
                    {
                        MCUData.MaxConcurrentAudioCalls = vMCU.AUDIO.calls;
                        MCUData.MaxConcurrentVideoCalls = vMCU.VIDEO.calls;
                        CardsAvailable = "Yes";

                        if (vMCU.AUDIO.available < 0) //FB 1837 - changed <= to <
                        {
                            CardsAvailable = "No";
                            if (Description.Length > 0)
                                Description += ", ";
                            Description += "Audio";

                        }

                        if (vMCU.VIDEO.available < 0) //FB 1837 - changed <= to <
                        {
                            CardsAvailable = "No";
                            if (Description.Length > 0)
                                Description += ", ";
                            Description += "Video";
                        }


                        if (Description.Length > 0)
                            Description = "Insufficient " + Description + " ports.";

                        audioPorts = vMCU.AUDIO.available;

                    }
                    else if (vMCU.bridgeType == MCUType.CODIAN) 
                    {
                        MCUData.MaxConcurrentAudioCalls = vMCU.AUDIO.calls;
                        MCUData.MaxConcurrentVideoCalls = vMCU.VIDEO.calls;
                        CardsAvailable = "Yes";

                        if (vMCU.VIDEO.available < 0) 
                        {
                            CardsAvailable = "No";
                            if (Description.Length > 0)
                                Description += ", ";
                            Description += "Audio/Video";
                        }


                        if (Description.Length > 0)
                            Description = "Insufficient " + Description + " ports.";

                        audioPorts = vMCU.VIDEO.available;

                    }
                    else // all other bridges(polycom/Tanberg/radvision)
                    {
                        //FB 1937
                        MCUData.MaxConcurrentAudioCalls = vMCU.AUDIO.calls;
                        MCUData.MaxConcurrentVideoCalls = vMCU.VIDEO.calls;
                        CardsAvailable = "Yes";
                        if (vMCU.VIDEO.available < 0)//FB 1837 - changed <= to <
                        {

                            CardsAvailable = "No";
                            if (Description.Length > 0)
                                Description += ", ";
                            Description += "Audio/Video"; //FB 1937
                        }

                        if (Description.Length > 0)
                            Description = "Insufficient " + Description + " ports.";

                        audioPorts = -1000;//FB 1937
                        // 1134 AG 01/27/09

                    }
                    //FB 2472 End

                    obj.outXml = "<MCUResources>";
                    obj.outXml += "<AudioVideo>";
                    obj.outXml += "<Available>" + vMCU.VIDEO.available.ToString() + "</Available>";
                    obj.outXml += "<Total>" + MCUData.MaxConcurrentVideoCalls.ToString() + "</Total>";
                    obj.outXml += "</AudioVideo>";
                    obj.outXml += "<AudioOnly>";
                    obj.outXml += "<Available>" + audioPorts.ToString() + "</Available>";//FB 1937
                    obj.outXml += "<Total>" + MCUData.MaxConcurrentAudioCalls.ToString() + "</Total>";
                    obj.outXml += "</AudioOnly>";
                    obj.outXml += "<CardsAvailable>" + CardsAvailable + "</CardsAvailable>";
                    obj.outXml += "<Description>" + Description + "</Description>";
                    obj.outXml += "</MCUResources>";
                }
                else
                {
                    //FB 1881 start
                    //obj.outXml = myVRMException.toXml("ERROR cannot calculate MCU Usage");
                    myVRMException myVRMEx = new myVRMException(435);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    //FB 1881 end
                    return false;
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = ""; //FB 1881 // myVRMException.toXml(e.Message);
                bRet = false;
            }

            return bRet;

        }
        #endregion

        #region GetMCUusage
        public bool GetMCUusage(DateTime confDate, int Duration, int confId, int instanceId,
          ref List<vrmMCUResources> vMCUList)
        {
            return GetMCUusage(confDate, Duration, confId, instanceId, ref vMCUList); //FB 1937
        }
        #endregion

        #region GetMCUusage
        public bool GetMCUusage(DateTime confDate, int Duration, int confId, int instanceId,
            ref List<vrmMCUResources> vMCUList, bool bSkipCheck)
        {
            // find this nubmer anywhere (user/room) that this conference is scheduled and it is not to be used.
            try
            {
                string status = String.Format("{0,3:D},{1,3:D},{2,3:D},{3,3:D}",
                    vrmConfStatus.Scheduled,
                    vrmConfStatus.Pending,
                    vrmConfStatus.Ongoing,
                    vrmConfStatus.OnMCU);

                string hql;

                // select all converences that fall into this date time range (with correct status and are not rooms or templates
                // 
                hql = "SELECT c.confid, c.instanceid FROM myVRM.DataLayer.vrmConference c ";
                hql += " WHERE(dateadd(minute, " + Duration.ToString() + ",'";
                hql += confDate.ToString("d") + " " + confDate.ToString("t") + "')) >= c.confdate ";
                hql += " AND '" + confDate.ToString("d") + " " + confDate.ToString("t") + "'";
                hql += " <= (dateadd(minute, c.duration, c.confdate ))";
                hql += "AND c.deleted = 0 AND c.status IN( " + status + ")";
                hql += "AND c.conftype != " + vrmConfType.RooomOnly;
                hql += "AND c.conftype != " + vrmConfType.Template;


                IList conferences = m_vrmConfDAO.execQuery(hql);
                // step thru each conference and count the ports used. 
                List<ICriterion> criterionList;
                foreach (object[] obj in conferences)
                {
                    int sConfid = Int32.Parse(obj[0].ToString());
                    int sInstcanceid = Int32.Parse(obj[1].ToString());

                    vrmConfAdvAvParams advAVParams = new vrmConfAdvAvParams();
                    for (int i = 0; i < vMCUList.Count; i++)
                    {
                        vrmMCUResources vMCU = vMCUList[i];
                        vrmConference baseConf = m_vrmConfDAO.GetByConfId(sConfid, sInstcanceid);
                        vrmConfAdvAvParams AvParams = baseConf.ConfAdvAvParams;
                        int isSwitched = 0;
                        int isEncryption = 0;

                        if (AvParams.videoMode == 1 && isSwitched != 1)
                            isSwitched = 1;
                        if (AvParams.encryption == 1 && isEncryption != 1)
                            isEncryption = 1;

                        // first lookup confrooms
                        // now the users (only external)
                        // FB 4460 use the vMCU (virtual bridge alorithm for Polycomm mcu's
                        criterionList = new List<ICriterion>();
                        if (confId == 0)
                        {
                            criterionList.Add(Expression.Eq("confid", sConfid));
                            criterionList.Add(Expression.Eq("instanceid", sInstcanceid));
                            criterionList.Add(Expression.Eq("bridgeid", vMCU.bridgeId));
                        }
                        else
                        {

                            criterionList.Add(Expression.Eq("confid", sConfid));
                            criterionList.Add(Expression.Eq("instanceid", sInstcanceid));
                            if (confId != sConfid || (confId == sConfid && instanceId != sInstcanceid))
                                criterionList.Add(Expression.Eq("bridgeid", vMCU.bridgeId));
                        }
                        List<vrmConfRoom> room = m_IconfRoom.GetByCriteria(criterionList);

                        foreach (vrmConfRoom rm in room)
                        {
                            bool notBypass = true;
                            if ((bSkipCheck && (sConfid == confId && sInstcanceid == instanceId)) ||
                                 (!bSkipCheck && (sConfid == confId &&
                                        sInstcanceid == instanceId && rm.bridgeid == vMCU.bridgeId)))

                                // exclude current conference
                                if (notBypass)
                                {

                                    switch (vMCU.bridgeType)
                                    {
                                        case MCUType.POLYCOM:
                                            vMCUResourceLoad(rm.addressType, rm.audioorvideo, isSwitched,
                                                   isEncryption, vMCU);
                                            break;
                                        default:
                                            if (rm.audioorvideo == 1) //FB 1744
                                                vMCU.AUDIO.used++;
                                            else
                                                vMCU.VIDEO.used++;
                                            break;
                                    }
                                }
                        }

                        // only external users (as they are using port)
                        criterionList.Add(Expression.Eq("invitee", vrmConfUserType.External));
                        criterionList.Add(Expression.Not(Expression.Eq("status", vrmConfUserStatus.Rejectetd)));
                        List<vrmConfUser> user = m_IconfUser.GetByCriteria(criterionList);
                        foreach (vrmConfUser us in user)
                        {
                            // if we are checking mcu resource back out the resources from the conference bridge.
                            // if we are settup up a conference back out the requested resources as they 
                            //     (irespecitve of bridge) will be added in later. 
                            bool notBypass = true;
                            if ((bSkipCheck && (sConfid == confId && sInstcanceid == instanceId)) ||
                                (!bSkipCheck && (sConfid == confId &&
                                       sInstcanceid == instanceId && us.bridgeid == vMCU.bridgeId)))
                                notBypass = false;

                            if (notBypass)
                            {

                                switch (vMCU.bridgeType)
                                {
                                    case MCUType.POLYCOM:
                                        vMCUResourceLoad(us.addressType, us.audioOrVideo, isSwitched,
                                               isEncryption, vMCU);
                                        break;
                                    default:
                                        if (us.audioOrVideo == 1) //FB 1744
                                            vMCU.AUDIO.used++;
                                        else
                                            vMCU.VIDEO.used++;
                                        break;

                                }
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }

        
        #endregion

        // FB 1937
        #region GetCompleteMCUusage
        public bool GetCompleteMCUusage(DateTime confDate, int Duration, int confId, int instanceId,
            ref List<vrmMCUResources> vMCUList)
        {
            // find this nubmer anywhere (user/room) that this conference is scheduled and it is not to be used.
            try
            {
                string status = String.Format("{0,3:D},{1,3:D},{2,3:D},{3,3:D}",
                    vrmConfStatus.Scheduled,
                    vrmConfStatus.Pending,
                    vrmConfStatus.Ongoing,
                    vrmConfStatus.OnMCU);

                string hql;
                string stmt = "";
                // select all conferences that fall into this date time range (with correct status and are not rooms or templates
                // 
                hql = "SELECT c.confid, c.instanceid FROM myVRM.DataLayer.vrmConference c ";
                hql += " WHERE(dateadd(minute, " + Duration.ToString() + ",'";
                hql += confDate.ToString("d") + " " + confDate.ToString("t") + "')) >= c.confdate ";
                hql += " AND '" + confDate.ToString("d") + " " + confDate.ToString("t") + "'";
                hql += " <= (dateadd(minute, c.duration, c.confdate ))";
                hql += "AND c.deleted = 0 AND c.status IN( " + status + ")";
                hql += "AND c.conftype != " + vrmConfType.RooomOnly;
                hql += "AND c.conftype != " + vrmConfType.Template;
				//ZD 101835
                stmt = hql.Replace("myVRM.DataLayer.vrmConference", "Archive_Conf_Conference_D"); //ZD 101835

                IList conferences = m_vrmConfDAO.execQuery(hql);
				//ZD 101835	
                int sConfid = 0;
                int sInstcanceid = 1;
                IList conferenceNew = new List<string>();
                if (conferences.Count == 0)
                {
                    sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);
                    sqlCon.OpenConnection();
                    sqlCon.OpenTransaction();

                    DataSet ds = sqlCon.ExecuteDataSet(stmt);
                    sqlCon.CommitTransaction();
                    sqlCon.CloseConnection();

                    if (ds != null && ds.Tables.Count > 0)
                    {
                        for (Int32 c = 0; c < ds.Tables[0].Rows.Count; c++)
                        {
                            conferenceNew.Add(ds.Tables[0].Rows[c]["confid"].ToString() + "," + ds.Tables[0].Rows[c]["instanceid"].ToString());
                        }
                    }
                }
                else
                {
                    foreach (object[] obj in conferences)
                    {
                        sConfid = Int32.Parse(obj[0].ToString());
                        sInstcanceid = Int32.Parse(obj[1].ToString());
                        conferenceNew.Add(sConfid + "," + sInstcanceid);
                    }
                }

                // step thru each conference and count the ports used. 
                List<ICriterion> criterionList;
                foreach (string strConf in conferenceNew)
                {
                    String[] obj = strConf.Split(',');

                    sConfid = Int32.Parse(obj[0].ToString());
                    sInstcanceid = Int32.Parse(obj[1].ToString());

                    vrmConfAdvAvParams advAVParams = new vrmConfAdvAvParams();
                    for (int i = 0; i < vMCUList.Count; i++)
                    {
                        
                        vrmMCUResources vMCU = vMCUList[i];
                        vrmConference baseConf = null;
                        try
                        {
                            baseConf = m_vrmConfDAO.GetByConfId(sConfid, sInstcanceid);
                        }
                        catch (Exception ex)
                        {
                            //ZD 101835
                            if (baseConf == null)
                            {
                                vrmDataObject obj1 = new DataLayer.vrmDataObject(m_configPath);
                                Conference cc = new Conference(ref obj1);
                                criterionList = new List<ICriterion>();
                                criterionList.Add(Expression.Eq("confid", sConfid));
                                criterionList.Add(Expression.Eq("instanceid", sInstcanceid));

                                List<vrmConference> confList = new List<DataLayer.vrmConference>();
                                cc.SearchArchiveConference(ref confList, criterionList);

                                if (confList.Count > 0)
                                    baseConf = confList[0];
                            }
                        }

                        

                        vrmConfAdvAvParams AvParams = baseConf.ConfAdvAvParams;
                        int isSwitched = 0;
                        int isEncryption = 0;

                        if (AvParams.videoMode == 1)
                            isSwitched = 1;
                        if (AvParams.encryption == 1)
                            isEncryption = 1;

                        // first lookup confrooms
                        // now the users (only external)
                        // FB 4460 use the vMCU (virtual bridge alorithm for Polycomm mcu's
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("confid", sConfid));
                        criterionList.Add(Expression.Eq("instanceid", sInstcanceid));
                        criterionList.Add(Expression.Eq("bridgeid", vMCU.bridgeId));
                        List<vrmConfRoom> room = m_IconfRoom.GetByCriteria(criterionList);

                        //ZD 101835
                        if (room.Count == 0)
                        {
                            sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);
                            sqlCon.OpenConnection();
                            sqlCon.OpenTransaction();
                            String stmtArchive = "select * from Archive_Conf_Room_D where confid = " + sConfid + " and instanceid=" + sInstcanceid + " and bridgeid=" + vMCU.bridgeId;
                            DataSet ds = sqlCon.ExecuteDataSet(stmtArchive);
                            sqlCon.CommitTransaction();
                            sqlCon.CloseConnection();

                            if (ds != null && ds.Tables.Count > 0)
                            {
                                for (Int32 c = 0; c < ds.Tables[0].Rows.Count; c++)
                                {
                                    vrmConfRoom cf = new DataLayer.vrmConfRoom();
                                    cf.roomId = Int32.Parse(ds.Tables[0].Rows[c]["roomId"].ToString());
                                    room.Add(cf);
                                }
                            }
                        }

                        foreach (vrmConfRoom rm in room)
                        {
                            bool notBypass = true;


                            // exclude current conference
                            if (notBypass)
                            {

                                switch (vMCU.bridgeType)
                                {
                                    //FB 1937 & 2008
                                    //case MCUType.POLYCOM:
                                    //    vMCUResourceLoad(rm.addressType, rm.audioorvideo, isSwitched,
                                    //           isEncryption, vMCU);
                                    //    break;
                                    case MCUType.MSE8000://FB 1937 & 2008
                                    case MCUType.CODIAN:
                                        //Commented for FB 2472
                                        //if (rm.audioorvideo == 1 && vMCU.AUDIO.available > 0) //FB 1744  
                                        //    vMCU.AUDIO.used++;
                                        //else
                                        //{
                                            vMCU.VIDEO.used++;
                                            //vMCU.AUDIO.used++; //FB 1937
                                        //}
                                        break;
                                    default:
                                        //if (vMCU.AUDIO.available > 0) //FB 1744 //FB 1937
                                        //    vMCU.AUDIO.used++;
                                        //else
                                        vMCU.VIDEO.used++;
                                        vMCU.AUDIO.used = -1000;
                                        break;
                                }
                            }
                        }

                        // only external users (as they are using port)
                        criterionList.Add(Expression.Eq("invitee", vrmConfUserType.External));
                        criterionList.Add(Expression.Not(Expression.Eq("status", vrmConfUserStatus.Rejectetd)));
                        List<vrmConfUser> user = m_IconfUser.GetByCriteria(criterionList);

                        //ZD 101835
                        if (user.Count == 0)
                        {
                            sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);
                            sqlCon.OpenConnection();
                            sqlCon.OpenTransaction();
                            String stmtArchive = "select * from Archive_Conf_User_D where confid = " + sConfid + " and instanceid=" + sInstcanceid
                                + " and bridgeid=" + vMCU.bridgeId + " and invitee = 1 and status <> 2";
                            DataSet ds = sqlCon.ExecuteDataSet(stmtArchive);
                            sqlCon.CommitTransaction();
                            sqlCon.CloseConnection();

                            if (ds != null && ds.Tables.Count > 0)
                            {
                                for (Int32 c = 0; c < ds.Tables[0].Rows.Count; c++)
                                {
                                    vrmConfUser cu = new DataLayer.vrmConfUser();
                                    cu.userid = Int32.Parse(ds.Tables[0].Rows[c]["UserID"].ToString());
                                    user.Add(cu);
                                }
                            }
                        }

                        foreach (vrmConfUser us in user)
                        {
                            // if we are checking mcu resource back out the resources from the conference bridge.
                            // if we are settup up a conference back out the requested resources as they 
                            //     (irespecitve of bridge) will be added in later. 
                            bool notBypass = true;

                            if (notBypass)
                            {

                                switch (vMCU.bridgeType)
                                {
                                    //FB 1937 & 2008
                                    //case MCUType.POLYCOM:
                                    //    vMCUResourceLoad(rm.addressType, rm.audioorvideo, isSwitched,
                                    //           isEncryption, vMCU);
                                    //    break;
                                    case MCUType.MSE8000://FB 1937 & 2008
                                    case MCUType.CODIAN:
                                        //Commented for FB 2472
                                        //if (us.audioOrVideo == 1 && vMCU.AUDIO.available >= 0) //FB 1744  FB 2472
                                        //    vMCU.AUDIO.used++;
                                        //else
                                        //{
                                            vMCU.VIDEO.used++;
                                            //vMCU.AUDIO.used++; //FB 1937
                                        //}
                                        break;
                                    default:
                                        //if (vMCU.AUDIO.available > 0) //FB 1744 FB 1937
                                        //    vMCU.AUDIO.used++;
                                        //else
                                        vMCU.VIDEO.used++;
                                        vMCU.AUDIO.used = -1000;
                                        break;
                                }
                            }
                        }

                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("confid", sConfid));
                        criterionList.Add(Expression.Eq("instanceid", sInstcanceid));
                        criterionList.Add(Expression.Eq("bridgeId", vMCU.bridgeId));
                        List<vrmConfCascade> cascade = m_IconfCascade.GetByCriteria(criterionList);

                        //ZD 101835
                        if (cascade.Count == 0)
                        {
                            sqlCon = new ns_SqlHelper.SqlHelper(m_configPath);
                            sqlCon.OpenConnection();
                            sqlCon.OpenTransaction();
                            String stmtArchive = "select * from Archive_Conf_Cascade_D where confid = " + sConfid + " and instanceid=" + sInstcanceid
                                + " and bridgeid=" + vMCU.bridgeId;
                            DataSet ds = sqlCon.ExecuteDataSet(stmtArchive);
                            sqlCon.CommitTransaction();
                            sqlCon.CloseConnection();

                            if (ds != null && ds.Tables.Count > 0)
                            {
                                for (Int32 c = 0; c < ds.Tables[0].Rows.Count; c++)
                                {
                                    vrmConfCascade cu = new DataLayer.vrmConfCascade();
                                    cu.cascadeLinkId = Int32.Parse(ds.Tables[0].Rows[c]["cascadelinkid"].ToString());
                                    cascade.Add(cu);
                                }
                            }
                        }

                        foreach (vrmConfCascade csde in cascade)
                        {
                            bool notBypass = true;


                            // exclude current conference
                            if (notBypass)
                            {

                                switch (vMCU.bridgeType)
                                {
                                    //FB 1937 & 2008
                                    //case MCUType.POLYCOM:
                                    //    vMCUResourceLoad(csde.addresstype, csde.audioOrVideo, isSwitched,
                                    //           isEncryption, vMCU);
                                    //    break;
                                    case MCUType.MSE8000://FB 1937 & 2008
                                    case MCUType.CODIAN:
                                        //Commented for FB 2472
                                        //if (csde.audioOrVideo == 1 && vMCU.AUDIO.available >= 0) //FB 2472
                                        //    vMCU.AUDIO.used++;
                                        //else
                                        //{
                                            vMCU.VIDEO.used++;
                                            //vMCU.AUDIO.used++; //FB 1937
                                        //}
                                        break;
                                    default:
                                        //if (vMCU.AUDIO.available > 0) //FB 1744 FB 1937
                                        //    vMCU.AUDIO.used++;
                                        //else
                                        vMCU.VIDEO.used++;
                                        vMCU.AUDIO.used = -1000;
                                        break;
                                }
                            }
                        }

                    }
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region vMCUResourceLoad
        public bool vMCUResourceLoad(int addressType, int isVideo, int isSwitched, int isEncryted, vrmMCUResources vMCU)
        {
            return vMCUResourceLoad(addressType, isVideo, isSwitched, isEncryted, vMCU, false);

        }
        #endregion

        #region vMCUResourceLoad
        public bool vMCUResourceLoad(int addressType, int isVideo, int isSwitched, int isEncryted,
            vrmMCUResources vMCU, bool check)
        {
            try
            {
                int port = 1;
                if (isEncryted == 1)
                    port = 2;
                switch (addressType)
                {
                    case eptDetails.typeISDN:
                        // ISDN/Audio
                        if (isVideo == 0)
                        {
                            if (check)
                                vMCU.needAUDIO = true;
                            vMCU.AUDIO.used += port;
                            if (check)
                                vMCU.needISDN = true;
                            vMCU.ISDN.used += port;
                        }
                        else
                        {
                            // ISDN/Video switched
                            if (isSwitched == 1)
                            {
                                if (check)
                                    vMCU.needAUDIO = true;
                                vMCU.AUDIO.used += port;
                                if (check)
                                    vMCU.needMUX = true;
                                vMCU.MUX.used += port;
                                if (check)
                                    vMCU.needISDN = true;
                                vMCU.ISDN.used += port;
                            }
                            // ISDN/Video non-switched
                            else
                            {
                                if (check)
                                    vMCU.needMUX = true;
                                vMCU.MUX.used += port;
                                if (check)
                                    vMCU.needVIDEO = true;
                                vMCU.VIDEO.used += port;
                                if (check)
                                    vMCU.needAUDIO = true;
                                vMCU.AUDIO.used += port;
                                if (check)
                                    vMCU.needISDN = true;
                                vMCU.ISDN.used += port;
                            }
                        }
                        break;
                    case eptDetails.typeMPI:
                        if (check)
                            vMCU.needAUDIO = true;
                        vMCU.AUDIO.used += port;
                        if (check)
                            vMCU.needMUX = true;
                        vMCU.MUX.used += port;
                        if (check)
                            vMCU.needMPI = true;
                        vMCU.MPI.used += port;
                        break;

                    default:
                        // IP is default address type
                        // IP/Audio
                        if (isVideo == 0)
                        {
                            if (check)
                                vMCU.needAUDIO = true;
                            vMCU.AUDIO.used += port;
                            if (check)
                                vMCU.needVIDEO = true;
                            vMCU.VIDEO.used += port;
                        }
                        else
                        {
                            // IP/Video switched
                            if (isSwitched == 1)
                            {
                                if (check)
                                    vMCU.needAUDIO = true;
                                vMCU.AUDIO.used += port;
                                if (check)
                                    vMCU.needIP = true;
                                vMCU.IP.used += port;
                            }
                            // IP/Video non-switched
                            else
                            {
                                if (check)
                                    vMCU.needVIDEO = true;
                                vMCU.VIDEO.used += port;
                                if (check)
                                    vMCU.needAUDIO = true;
                                vMCU.AUDIO.used += port;
                                if (check)
                                    vMCU.needIP = true;
                                vMCU.IP.used += port;
                            }
                        }
                        break;


                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region loadVRMResourceData
        public bool loadVRMResourceData(vrmMCU MCU, ref vrmMCUResources vMCU)
        {
            try
            {
                //1036 auto detect MCU add support for Codian and Tanberg
                vMCU.bridgeType = MCU.MCUType.bridgeInterfaceId;
                vMCU.bridgeId = MCU.BridgeID;
                vMCU.bridgeName = MCU.BridgeName;
                vMCU.adminId = MCU.Admin;

                switch (MCU.MCUType.bridgeInterfaceId)
                {
                    case MCUType.RADVISION://Added for radvision elite - 1998
                    case MCUType.POLYCOM:
                        //Code Commented for FB 1937
                        //foreach (vrmMCUCardList cl in MCU.MCUCardList)
                        //{
                        //    if (cl.MCUCardDetail.CardName == vMCU.HDLC.tag)
                        //        vMCU.HDLC.calls = cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName == vMCU.ISDN.tag)
                        //        vMCU.ISDN.calls = cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName == vMCU.ATM.tag)
                        //        vMCU.ATM.calls = cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName == vMCU.IP.tag)
                        //        vMCU.IP.calls = cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName.Contains(vMCU.MUX.tag))
                        //        vMCU.MUX.calls += cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName.Contains(vMCU.AUDIO.tag))
                        //        vMCU.AUDIO.calls += cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName.Contains(vMCU.VIDEO.tag))
                        //        vMCU.VIDEO.calls += cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName == vMCU.DATA.tag)
                        //        vMCU.DATA.calls = cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName == vMCU.ISDN_8.tag)
                        //        vMCU.ISDN_8.calls = cl.maxCalls;
                        //    else if (cl.MCUCardDetail.CardName == vMCU.MPI.tag)
                        //        vMCU.MPI.calls = cl.maxCalls;
                        //}
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    // (FB 1036) use max concurrent for Tanberg/Codian 
                    case MCUType.TANDBERG:
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    case MCUType.CODIAN:
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    case MCUType.MSE8000://Code added for MSE 8000
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    case MCUType.LIFESIZE://Fb 2261
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    case MCUType.CISCO: //FB 2501 Call Monitoring
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    case MCUType.iVIEW://Fb 2556
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    //ZD 101929 start
                    case MCUType.PEXIP:
                        vMCU.AUDIO.calls = MCU.MaxConcurrentAudioCalls;
                        vMCU.VIDEO.calls = MCU.MaxConcurrentVideoCalls;
                        vMCU.IP.calls = MCU.MaxConcurrentVideoCalls;
                        break;
                    //ZD 101929 End
                    // default error
                    default:
                        Exception e = new Exception("Invalid MCU Type. Cannot allocate resources");
                        throw e;

                }
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetBridgeInformation
        //
        // GetBridgeList and GetBridge commands have been consolidated. 
        // true == GetBridgeList command
        //      
        public bool GetBridgeInformation(ref vrmDataObject obj, bool extendInfo)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;

                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes

                obj.outXml = "<bridgeInfo>";
                obj.outXml += FetchBridgeTypeList();
                obj.outXml += FetchBridgeStatusList();

                int bridgeCount = 0;
                obj.outXml += FetchBridgeList(ref bridgeCount);

                if (extendInfo)
                {
                    OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                    int Remaining = orgdt.MCULimit - bridgeCount;
                    obj.outXml += "<totalNumber>" + bridgeCount.ToString() + "</totalNumber>";
                    obj.outXml += "<licensesRemain>" + Remaining.ToString() + "</licensesRemain>";
                }
                obj.outXml += "</bridgeInfo>";
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        #endregion

        #region GetNewBridge
        public bool GetNewBridge(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                string UserID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);  //Organization Module Fixes

                //FB 2486 - Start
                List <ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("EnhancedMCU", 1)); //FB 2636
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("orgId", organizationID));
                List<vrmMCU> chkMcu = m_ImcuDao.GetByCriteria(criterionList);
                //FB 2486 - End
               
                obj.outXml = "<bridge>";
                obj.outXml += "<timeZone>" + sysSettings.TimeZone.ToString() + "</timeZone>";
                StringBuilder outXml = new StringBuilder();

                OrganizationFactory m_OrgFactory = new OrganizationFactory(m_configPath, m_log);    //Organization Module
                m_OrgFactory.organizationID = organizationID;
                obj.outXml += "<EnhancedMCUCnt>" + chkMcu.Count + "</EnhancedMCUCnt>";//FB 2486 //FB 2636
                m_OrgFactory.timeZonesToXML(ref outXml);
                obj.outXml += outXml;
                obj.outXml += FetchBridgeTypeList();
                obj.outXml += FetchBridgeStatusList();
                obj.outXml += FetchAddressTypeList();
                obj.outXml += GetMCUPortResXML(null);//ZD 100040
                obj.outXml += "</bridge>";
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;
        }
        #endregion

        #region GetOldBridge
        public bool GetOldBridge(ref vrmDataObject obj)
        {
           
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                //ZD 101026 Starts
                int UserID = 0;
                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out UserID);
                //ZD 101026 End

                node = xd.SelectSingleNode("//login/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                //FB 2274 Starts
                node = xd.SelectSingleNode("//login/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes

                //FB 2486 - Start
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("EnhancedMCU", 1)); // FB 2636
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("orgId", organizationID));
                List<vrmMCU> chkMcu = m_ImcuDao.GetByCriteria(criterionList);
                //FB 2486 - End

                node = xd.SelectSingleNode("//login/bridgeID");
                string bridgeID = node.InnerXml.Trim();
                if (bridgeID != "") //ZD 101175
                {
                    obj.outXml = "<bridge>";

                    vrmMCU mcu = m_ImcuDao.GetById(Int32.Parse(bridgeID));

                    cryptography.Crypto crypto = new cryptography.Crypto();

                    obj.outXml += "<bridgeID>" + mcu.BridgeID.ToString() + "</bridgeID>";

                    if (organizationID != 11 && mcu.isPublic != 0) // FB 1920
                        mcu.BridgeName = mcu.BridgeName.ToString() + "(*)";

                    obj.outXml += "<name>" + mcu.BridgeName + "</name>";
                    obj.outXml += "<login>" + mcu.BridgeLogin + "</login>";
                    //obj.outXml += "<password>" + crypto.decrypt(mcu.BridgePassword) + "</password>"; //FB 3054
                    obj.outXml += "<password>" + mcu.BridgePassword + "</password>";
                    obj.outXml += "<bridgeType>" + mcu.MCUType.id.ToString() + "</bridgeType>";
                    obj.outXml += "<bridgeStatus>" + mcu.Status.ToString() + "</bridgeStatus>";
                    obj.outXml += "<URLAccess>" + mcu.URLAccess.ToString() + "</URLAccess>"; //ZD 100113
                    obj.outXml += "<MultipleAssistant>" + mcu.MultipleAddress.ToString() + "</MultipleAssistant>"; //ZD 100369_MCU
                    obj.outXml += "<virtualBridge>" + mcu.VirtualBridge.ToString() + "</virtualBridge>";
                    obj.outXml += "<malfunctionAlert>" + mcu.malfunctionalert.ToString() + "</malfunctionAlert>";
                    obj.outXml += "<timeZone>" + mcu.timezone.ToString() + "</timeZone>";
                    obj.outXml += "<firmwareVersion>" + mcu.SoftwareVer + "</firmwareVersion>";
                    obj.outXml += "<percentReservedPort>" + mcu.reservedportperc + "</percentReservedPort>";
                    obj.outXml += "<ApiPortNo>" + mcu.ApiPortNo.ToString() + "</ApiPortNo>";//API Port...
                    obj.outXml += "<isPublic>" + mcu.isPublic.ToString() + "</isPublic>"; // FB 1920
                    obj.outXml += "<setFavourite>" + mcu.setFavourite.ToString() + "</setFavourite>"; // FB 2501 CallMonitor Favourite
                    obj.outXml += "<enableCDR>" + mcu.EnableCDR.ToString() + "</enableCDR>"; // FB 2660
                    obj.outXml += "<deleteCDRDays>" + mcu.DeleteCDRDays.ToString() + "</deleteCDRDays>"; // FB 2660
                    obj.outXml += "<EnableMessage>" + mcu.EnableMessage + "</EnableMessage>";//FB 2486
                    obj.outXml += "<EnhancedMCUCnt>" + (chkMcu.Count - mcu.EnhancedMCU).ToString() + "</EnhancedMCUCnt>"; //FB 2486 //FB 2636 // ZD 100369_MCU
                    obj.outXml += "<EnablePollFailure>" + mcu.EnablePollFailure + "</EnablePollFailure>";//ZD 100369_MCU
                    obj.outXml += "<PollCount>" + mcu.PollCount + "</PollCount>";//ZD 100369_MCU
                    ////FB 1642 - DTMF Start Commented for FB 2655
                    //obj.outXml += " <DTMF>";
                    //obj.outXml += "     <PreConfCode>" + mcu.PreConfCode + "</PreConfCode>";
                    //obj.outXml += "     <PreLeaderPin>" + mcu.PreLeaderPin + "</PreLeaderPin>";
                    //obj.outXml += "     <PostLeaderPin>" + mcu.PostLeaderPin + "</PostLeaderPin>";
                    //obj.outXml += "</DTMF>";
                    ////FB 1642 - DTMF End
                    obj.outXml += "<AccessURL>" + mcu.AccessURL + "</AccessURL>";//ZD 101078
                    obj.outXml += "<bridgeAdmin>";
                    vrmUser user = m_vrmUserDAO.GetByUserId(mcu.Admin);

                    obj.outXml += "<ID>" + mcu.Admin + "</ID>";
                    /**** Code Modified For FB 1462 - Start ****/
                    if (user != null)
                    {
                        obj.outXml += "<firstName>" + user.FirstName + "</firstName>";
                        obj.outXml += "<lastName>" + user.LastName + "</lastName>";
                        obj.outXml += "<userstate>A</userstate>";    /*A- User Active */
                    }
                    else
                    {
                        vrmInactiveUser iuser = m_vrmIUserDAO.GetByUserId(mcu.Admin);

                        if (iuser != null)
                        {
                            obj.outXml += "<firstName>" + iuser.FirstName + "</firstName>";
                            obj.outXml += "<lastName>" + iuser.LastName + "</lastName>";
                            obj.outXml += "<userstate>I</userstate>";    /* I- User in-Active */
                        }
                        else
                        {
                            obj.outXml += "<firstName></firstName>";
                            obj.outXml += "<lastName></lastName>";
                            obj.outXml += "<userstate>D</userstate>";    /*D- User Deleted */
                        }
                    }
                    /**** Code Modified For FB 1462 - End ****/

                    obj.outXml += "<maxAudioCalls>" + mcu.MaxConcurrentAudioCalls.ToString() + "</maxAudioCalls>";
                    obj.outXml += "<maxVideoCalls>" + mcu.MaxConcurrentVideoCalls.ToString() + "</maxVideoCalls>";
                    obj.outXml += "</bridgeAdmin>";

                    StringBuilder tzOutXml = new StringBuilder();
                    OrganizationFactory m_OrgFactory = new OrganizationFactory(m_configPath, m_log);    //Organization Module
                    m_OrgFactory.organizationID = organizationID;
                    m_OrgFactory.timeZonesToXML(ref tzOutXml);
                    obj.outXml += tzOutXml;

                    // All lists
                    obj.outXml += "<approvers>";
                    obj.outXml += "<responseMsg>";
                    obj.outXml += mcu.responsemessage;
                    obj.outXml += "</responseMsg>";
                    obj.outXml += "<responseTime>";
                    obj.outXml += mcu.responsetime.ToString("g");
                    obj.outXml += "</responseTime>";
                    foreach (vrmMCUApprover approver in mcu.MCUApprover)
                    {
                        obj.outXml += "<approver>";
                        obj.outXml += "<ID>" + approver.approverid.ToString() + "</ID>";

                        vrmUser appUser = m_vrmUserDAO.GetByUserId(approver.approverid);
                        if (appUser != null)    //FB 1462 - Related issue fix
                        {
                            obj.outXml += "<firstName>" + appUser.FirstName + "</firstName>";
                            obj.outXml += "<lastName>" + appUser.LastName + "</lastName>";
                            obj.outXml += "<userstate>A</userstate>";
                        }
                        else
                        {
                            vrmInactiveUser iuser = m_vrmIUserDAO.GetByUserId(mcu.Admin);

                            if (iuser != null)
                            {
                                obj.outXml += "<firstName>" + appUser.FirstName + "</firstName>";
                                obj.outXml += "<lastName>" + appUser.LastName + "</lastName>";
                                obj.outXml += "<userstate>I</userstate>";
                            }
                            else
                            {
                                obj.outXml += "<firstName></firstName>";
                                obj.outXml += "<lastName></lastName>";
                                obj.outXml += "<userstate>D</userstate>";
                            }
                        }
                        obj.outXml += "</approver>";
                    }

                    obj.outXml += "</approvers>";

                    //ZD 100040 starts
                    criterionList = null;
                    m_IMCUPortResolutionDao = m_hardwareDAO.GetMCUPortResolutionDao();
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUPortResolution> ports = m_IMCUPortResolutionDao.GetByCriteria(criterionList);
                    
                    obj.outXml += GetMCUPortResXML(ports);
                    
                    //other virtual mcus assigned to the group of this active mcu
                    string portxml = "";
                    if (mcu.VirtualBridge == 1)
                    {
                        string qString = "select a.BridgeID, a.BridgeName, b.LoadBalance from myVRM.DataLayer.vrmMCU a, myVRM.DataLayer.vrmMCUGrpAssignment b where a.BridgeID <> " + mcu.BridgeID
                        + " and a.BridgeID = b.MCUID and b.MCUGroupID = (select c.MCUGroupID from myVRM.DataLayer.vrmMCUGrpAssignment c where MCUID =" + mcu.BridgeID + ")";

                        IList assignedMCUs = m_ImcuDao.execQuery(qString);
                        if (assignedMCUs.Count > 0)
                        {
                            object[] grpMCUAssigned = null;
                            for (int k = 0; k < assignedMCUs.Count; k++)
                            {
                                grpMCUAssigned = (object[])assignedMCUs[k];
                                //0-MCUId 1-BridgeName 2-loadbalance 

                                if (grpMCUAssigned[0] != null && grpMCUAssigned[1] != null && grpMCUAssigned[2] != null)
                                {
                                    portxml += "<mcu>";
                                    portxml += "<bridgeId>" + grpMCUAssigned[0].ToString() + "</bridgeId>";
                                    portxml += "<bridgeName>" + grpMCUAssigned[1].ToString() + "</bridgeName>";
                                    portxml += "<loadBalance>" + grpMCUAssigned[2].ToString() + "</loadBalance>";
                                    portxml += "</mcu>";

                                }
                            }
                        }
                    }
                    obj.outXml += "<mcuLoadBalanceGroup>" + portxml + "</mcuLoadBalanceGroup>";
                    //ZD 100040 ends

                    obj.outXml += "<timezones>" + sysSettings.TimeZone.ToString() + "</timezones>";
                    obj.outXml += FetchBridgeDetails(mcu);
                    obj.outXml += FetchBridgeTypeList();
                    obj.outXml += FetchBridgeStatusList();
                    obj.outXml += FetchAddressTypeList();

                    obj.outXml += "<ISDNThresholdAlert>" + mcu.isdnthresholdalert.ToString() + "</ISDNThresholdAlert>";
                    if (mcu.isdnthresholdalert == 1)
                    {
                        obj.outXml += "<ISDNPortCharge>" + mcu.isdnportrate + "</ISDNPortCharge>";
                        obj.outXml += "<ISDNLineCharge>" + mcu.isdnlinerate.ToString() + "</ISDNLineCharge>";
                        obj.outXml += "<ISDNMaxCost>" + mcu.isdnmaxcost.ToString() + "</ISDNMaxCost>";
                        obj.outXml += "<ISDNThreshold>" + mcu.isdnthreshold.ToString() + "</ISDNThreshold>";

                        if (organizationID < 11)    //Organization Module
                            organizationID = defaultOrgId;

                        if (orgInfo == null)
                            orgInfo = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                        string isdntimeframe = "";
                        if (orgInfo != null)
                            isdntimeframe = orgInfo.ISDNTimeFrame.ToString();    //Organization Module

                        if (isdntimeframe == "1" || isdntimeframe == "3")
                            obj.outXml += "<ISDNThresholdTimeframe>" + isdntimeframe + "</ISDNThresholdTimeframe>";
                        else
                            obj.outXml += "<ISDNThresholdTimeframe></ISDNThresholdTimeframe>";
                    }

                    obj.outXml += "<MCUCards>";
                    foreach (vrmMCUCardList cl in mcu.MCUCardList)
                    {
                        obj.outXml += "<MCUCard>";
                        obj.outXml += "<ID>" + cl.MCUCardDetail.id.ToString() + "</ID> ";
                        obj.outXml += "<MaximumCalls>" + cl.maxCalls.ToString() + "</MaximumCalls>";
                        obj.outXml += "</MCUCard>";
                    }
                    obj.outXml += "</MCUCards>";

                    //ZD 100664 Starts
                    StringBuilder auditOutXML = null;
                    obj.outXml += "<LastModifiedDetails>";
                    vrmUser LoginUser = m_vrmUserDAO.GetByUserId(UserID);
                    if (GetAuditMCU(mcu.BridgeID, LoginUser.TimeZone, ref auditOutXML) && !string.IsNullOrEmpty(auditOutXML.ToString()))
                        obj.outXml += auditOutXML.ToString();
                    obj.outXml += "</LastModifiedDetails>";
                    //ZD 100664 End

                    obj.outXml += "</bridge>";
                }
            }
            catch (myVRMException e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            return true;

        }
        #endregion

        #region FetchBridgeDetails

        private string FetchBridgeDetails(vrmMCU mcu)
        {
            cryptography.Crypto crypto = new cryptography.Crypto(); // FB 2441
            string outputXML = string.Empty;
            List<vrmMCUOrgSpecificDetails> orgSpecificdetails = null;//FB 2556
            IMCUOrgSpecificDetailsDao m_IMCUOrgSpecificDetailsDao = null;
            List<ICriterion> osCriterionList = null;
            int cntDtls = 0;
            List<vrmExtMCUSilo> MCUSilo = null;
            List<vrmExtMCUService> MCUService = null;
            int p = 0;
            try
            {
                List<vrmMCUVendor> vendorList = vrmGen.getMCUVendor();
                int i = -1;
                foreach (vrmMCUVendor vendor in vendorList)
                {
                    i++;
                    if (vendor.id == mcu.MCUType.id)
                        break;
                }
                if (i < 0)
                {
                    //FB 1881 start
                    myVRMException myVRMEx = new myVRMException(436);
                    throw myVRMEx;
                    //FB 1881 end
                }
                #region POLYCOM
                if (vendorList[i].BridgeInterfaceId == MCUType.POLYCOM) // Polycom Accord
                {
                    outputXML = "<bridgeDetails>";
                    outputXML += "<controlPortIPAddress>" + mcu.BridgeAddress + "</controlPortIPAddress>";
                    outputXML += "<ConferenceServiceID>" + mcu.ConfServiceID + "</ConferenceServiceID>"; //FB 2427
                    
                    //FB 2591 start
                    outputXML += "<Profiles>";
                    //outputXML += "<Id>" + MCUProf[p].ProfileId + "</Id>";
                    List<ICriterion> criterionListmp = new List<ICriterion>();
                    criterionListmp.Add(Expression.Eq("MCUId", mcu.BridgeID));
                    List<vrmMCUProfiles> MCUProf = m_IMcuProfilesDAO.GetByCriteria(criterionListmp);
                    for ( p = 0; p < MCUProf.Count; p++)
                    {
                        outputXML += "<Profile>";
                        outputXML += "<ID>" + MCUProf[p].ProfileId + "</ID>";
                        outputXML += "<name>" + MCUProf[p].ProfileName + "</name>";
                        outputXML += "</Profile>";
                    }
                    
                    outputXML += "</Profiles>";

                    //FB 2591 end
                    //ZD 104256 Starts
                    outputXML += "<ConferencePoolOrderID>" + mcu.PoolOrderID + "</ConferencePoolOrderID>"; //FB 2427

                    //FB 2591 start
                    outputXML += "<PoolOrders>";
                    criterionListmp = new List<ICriterion>();
                    criterionListmp.Add(Expression.Eq("MCUId", mcu.BridgeID));
                    List<vrmMCUPoolOrders> MCUPoolOrder = m_IMCUPoolOrderDao.GetByCriteria(criterionListmp);
                    for (p = 0; p < MCUPoolOrder.Count; p++)
                    {
                        outputXML += "<PoolOrder>";
                        outputXML += "<ID>" + MCUPoolOrder[p].PoolOrderId + "</ID>";
                        outputXML += "<Name>" + MCUPoolOrder[p].PoolOrderName + "</Name>";
                        outputXML += "</PoolOrder>";
                    }
                    outputXML += "</PoolOrders>";
                    //ZD 104256 Ends
                    outputXML += "<EnableIVR>" + mcu.EnableIVR + "</EnableIVR>";//FB 1766
                    outputXML += "<IVRServiceName>" + mcu.IVRServiceName + "</IVRServiceName>";//FB 1766
                    outputXML += "<enableRecord>" + mcu.EnableRecording + "</enableRecord>";//FB 1907
                    outputXML += "<enableLPR>" + mcu.LPR + "</enableLPR>";//FB 1907
                    outputXML += "<EnableMessage>" + mcu.EnableMessage + "</EnableMessage>";//FB 2486

                    //FB 2441 RPRM Starts
                    outputXML += "<PolycomRPRM>";
                    outputXML += "<RPRMDomain>" + mcu.RPRMDomain + "</RPRMDomain>"; // FB 2441 II
                    outputXML += "<Synchronous>" + mcu.Synchronous + "</Synchronous>";
                    outputXML += "<Sendmail>" + mcu.DMASendMail + "</Sendmail>";
                    outputXML += "<MonitorMCU>" + mcu.DMAMonitorMCU + "</MonitorMCU>";
                    outputXML +=  "<DMAName>" + mcu.DMAName + "</DMAName>";
                    outputXML +=  "<DMALogin>" + mcu.DMALogin + "</DMALogin>";
                    if (mcu.DMAPassword != null && mcu.DMAPassword != "")
                        //outputXML += "<DMAPassword>" + crypto.decrypt(mcu.DMAPassword) + "</DMAPassword>"; //FB 3054
                        outputXML += "<DMAPassword>" + mcu.DMAPassword + "</DMAPassword>";
                    else
                        outputXML += "<DMAPassword></DMAPassword>";

                    outputXML +=  "<DMAURL>" + mcu.DMAURL + "</DMAURL>";
                    outputXML +=  "<DMAPort>" + mcu.DMAPort + "</DMAPort>";
					outputXML+=   "<DMADomain>"+mcu.DMADomain+"</DMADomain>"; // FB 2441 II
                    outputXML +=  "<DMADialinPrefix>"+mcu.DialinPrefix+"</DMADialinPrefix>"; //FB 2689
                    //FB 2709
                    outputXML += "<LoginAccount>" + mcu.LoginName + "</LoginAccount>";
                    outputXML += "<LoginCount>" + mcu.loginCount + "</LoginCount>";
                    outputXML += "<RPRMEmail>" + mcu.RPRMEmailaddress + "</RPRMEmail>";
                    //FB 2709
                    outputXML +=  "</PolycomRPRM>";
                    //FB 2441 RPRM Ends
                    vrmMCUIPServices ip = null; //FB 2636
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUIPServices> ipServices = m_IipServices.GetByCriteria(criterionList);
                    outputXML += "<IPServices>";
                    for (int k = 0; k < ipServices.Count; k++)
                    {
                        ip = ipServices[k];
                        outputXML += "<IPService>";
                        outputXML += "<SortID>" + ip.SortID.ToString() + "</SortID>";
                        outputXML += "<name>" + ip.ServiceName + "</name>";
                        outputXML += "<addressType>" + ip.addressType.ToString() + "</addressType>";
                        outputXML += "<address>" + ip.ipAddress + "</address>";
                        outputXML += "<networkAccess>" + ip.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + ip.usage.ToString() + "</usage>";
                        outputXML += "</IPService>";
                    }
                    outputXML += "</IPServices>";
                    vrmMCUISDNServices isdn = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    vrmMCUMPIServices mpi = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUMPIServices> mpiServices = m_ImpiService.GetByCriteria(criterionList);
                    outputXML += "<MPIServices>";
                    for (int k = 0; k < mpiServices.Count; k++)
                    {
                        mpi = mpiServices[k];
                        outputXML += "<MPIService>";
                        outputXML += "<SortID>" + mpi.SortID.ToString() + "</SortID>";
                        outputXML += "<name>" + mpi.ServiceName + "</name>";
                        outputXML += "<addressType>" + mpi.addressType.ToString() + "</addressType>";
                        outputXML += "<address>" + mpi.ipAddress + "</address>";
                        outputXML += "<networkAccess>" + mpi.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + mpi.usage.ToString() + "</usage>";
                        outputXML += "</MPIService>";
                    }
                    outputXML += "</MPIServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    outputXML += "<BridgeDomain>" + mcu.BridgeDomain + "</BridgeDomain>"; //ZD 100522
                    outputXML += "</bridgeDetails>";
                }
                #endregion
                #region CODIAN
                else if (vendorList[i].BridgeInterfaceId == MCUType.CODIAN) // Codian bridge
                {
                    // BridgeIPAddress table contains the two ports
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUIPServices> ipService = m_IipServices.GetByCriteria(criterionList);

                    outputXML = "<bridgeDetails>";
                    string portA = string.Empty, portB = string.Empty;
                    if (ipService.Count > 0)
                    {
                        portA = ipService[0].ipAddress;
                        if (ipService.Count > 0)
                            portB = ipService[1].ipAddress;
                    }
                    outputXML += "<portA>" + portA + "</portA>";
                    outputXML += "<portB>" + portB + "</portB>";
                    outputXML += "<ISDNAudioPref>" + mcu.ISDNAudioPrefix + "</ISDNAudioPref>";//FB 2003
                    outputXML += "<ISDNVideoPref>" + mcu.ISDNVideoPrefix + "</ISDNVideoPref>";
                    //NG fixes
                    vrmMCUISDNServices isdn = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    outputXML += "<BridgeDomain>" + mcu.BridgeDomain + "</BridgeDomain>"; //ZD 100522
                    // FB 2636 Starts
                    outputXML += "<E164Dialing>" + mcu.E164Dialing + "</E164Dialing>";
                    outputXML += "<H323Dialing>" + mcu.H323Dialing + "</H323Dialing>";
                    vrmMCUE164Services E164 = null;
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeID", mcu.BridgeID));
                    List<vrmMCUE164Services> E164Services = m_IE164Services.GetByCriteria(criterionList);
                    outputXML += "<E164Services>";
                    for (int j = 0; j < E164Services.Count; j++)
                    {
                        E164 = E164Services[j];
                        outputXML += "<E164Service>";
                        outputXML += "<SortID>" + (j + 1).ToString() + "</SortID>";
                        outputXML += "<StartRange>" + E164.StartRange + "</StartRange>";
                        outputXML += "<EndRange>" + E164.EndRange + "</EndRange>";
                        outputXML += "</E164Service>";
                    }
                    outputXML += "</E164Services>";
                    // FB 2636 Ends
                    outputXML += "</bridgeDetails>";
                }
                #endregion
                #region TANDBERG
                else if (vendorList[i].BridgeInterfaceId == MCUType.TANDBERG) // Tanberg bridge
                {
                    // Get the control port
                    outputXML += "<bridgeDetails>";
                    outputXML += "<controlPortIPAddress>" + mcu.BridgeAddress + "</controlPortIPAddress>";
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    vrmMCUISDNServices isdn = null; //FB 2636
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    outputXML += "<BridgeDomain>" + mcu.BridgeDomain + "</BridgeDomain>"; //ZD 100522
                    outputXML += "</bridgeDetails>";
                }
                #endregion
                #region RADVISION
                //Code Modified For FB 1222 - Start
                else if (vendorList[i].BridgeInterfaceId == MCUType.RADVISION) // Radvision 
                {
                    // Get the control port
                    outputXML = "<bridgeDetails>";
                    outputXML += "<controlPortIPAddress>" + mcu.BridgeAddress + "</controlPortIPAddress>";
                    outputXML += "<ConferenceServiceID>" + mcu.ConfServiceID + "</ConferenceServiceID>"; //FB 2016

                    vrmMCUIPServices ip = null;//FB 2636
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUIPServices> ipServices = m_IipServices.GetByCriteria(criterionList);
                    outputXML += "<IPServices>";
                    for (int k = 0; k < ipServices.Count; k++)
                    {
                        ip = ipServices[k];
                        outputXML += "<IPService>";
                        outputXML += "<SortID>" + ip.SortID.ToString() + "</SortID>";
                        outputXML += "<name>" + ip.ServiceName + "</name>";
                        outputXML += "<addressType>" + ip.addressType.ToString() + "</addressType>";
                        outputXML += "<address>" + ip.ipAddress + "</address>";
                        outputXML += "<networkAccess>" + ip.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + ip.usage.ToString() + "</usage>";
                        outputXML += "</IPService>";
                    }
                    outputXML += "</IPServices>";
                    
                    vrmMCUISDNServices isdn = null;//FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    vrmMCUMPIServices mpi = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUMPIServices> mpiServices = m_ImpiService.GetByCriteria(criterionList);
                    outputXML += "<MPIServices>";
                    for (int k = 0; k < mpiServices.Count; k++)
                    {
                        mpi = mpiServices[k];
                        outputXML += "<MPIService>";
                        outputXML += "<SortID>" + mpi.SortID.ToString() + "</SortID>";
                        outputXML += "<name>" + mpi.ServiceName + "</name>";
                        outputXML += "<addressType>" + mpi.addressType.ToString() + "</addressType>";
                        outputXML += "<address>" + mpi.ipAddress + "</address>";
                        outputXML += "<networkAccess>" + mpi.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + mpi.usage.ToString() + "</usage>";
                        outputXML += "</MPIService>";
                    }
                    outputXML += "</MPIServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    outputXML += "<BridgeDomain>" + mcu.BridgeDomain + "</BridgeDomain>"; //ZD 100522
                    outputXML += "</bridgeDetails>";
                }
                #endregion
                #region Codian  MSE8000
                else if (vendorList[i].BridgeInterfaceId == MCUType.MSE8000) //FB 2008 start
                {
                    // BridgeIPAddress table contains the two ports
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUIPServices> ipService = m_IipServices.GetByCriteria(criterionList);

                    outputXML = "<bridgeDetails>";
                    string portA = string.Empty, portB = string.Empty;
                    if (ipService.Count > 0)
                    {
                        portA = ipService[0].ipAddress;
                        if (ipService.Count > 0)
                            portB = ipService[1].ipAddress;
                    }
                    outputXML += "<portA>" + portA + "</portA>";
                    outputXML += "<portB>" + portB + "</portB>";
                    outputXML += "<ISDNGateway>" + mcu.ISDNGateway + "</ISDNGateway>";//FB 2003
                    outputXML += "<ISDNAudioPref>" + mcu.ISDNAudioPrefix + "</ISDNAudioPref>";//FB 2003
                    outputXML += "<ISDNVideoPref>" + mcu.ISDNVideoPrefix + "</ISDNVideoPref>";
                    //NG fixes
                    vrmMCUISDNServices isdn = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    outputXML += "<BridgeDomain>" + mcu.BridgeDomain + "</BridgeDomain>"; //ZD 100522
                    //FB 2636 Starts
                    outputXML += "<E164Dialing>" + mcu.E164Dialing + "</E164Dialing>";
                    outputXML += "<H323Dialing>" + mcu.H323Dialing + "</H323Dialing>";
                    vrmMCUE164Services E164 = null;
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeID", mcu.BridgeID));
                    List<vrmMCUE164Services> E164Services = m_IE164Services.GetByCriteria(criterionList);
                    outputXML += "<E164Services>";
                    for (int j = 0; j < E164Services.Count; j++)
                    {
                        E164 = E164Services[j];
                        outputXML += "<E164Service>";
                        outputXML += "<SortID>" + (j + 1).ToString() + "</SortID>";
                        outputXML += "<StartRange>" + E164.StartRange + "</StartRange>";
                        outputXML += "<EndRange>" + E164.EndRange + "</EndRange>";
                        outputXML += "</E164Service>";
                    }
                    outputXML += "</E164Services>";
                    //FB 2636 Ends
                    outputXML += "</bridgeDetails>";
                }
                //FB 2008 end
                #endregion
                # region LIFESIZE
                //FB 2261 start
                else if (vendorList[i].BridgeInterfaceId == MCUType.LIFESIZE) // lifesize bridge
                {
                    // Get the control port
                    outputXML += "<bridgeDetails>";
                    outputXML += "<controlPortIPAddress>" + mcu.BridgeAddress + "</controlPortIPAddress>";
                    outputXML += "<ISDNServices></ISDNServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    outputXML += "<BridgeDomain>" + mcu.BridgeDomain + "</BridgeDomain>"; //ZD 100522
                    outputXML += "</bridgeDetails>";
                }
                //FB 2261 end
                #endregion
                #region CISCO
                //Code Modified For FB 1222 - End
                //FB 2501 Call Monitoring Start
                else if (vendorList[i].BridgeInterfaceId == MCUType.CISCO) // CISCO bridge
                {
                    // BridgeIPAddress table contains the two ports
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUIPServices> ipService = m_IipServices.GetByCriteria(criterionList);

                    outputXML = "<bridgeDetails>";
                    string portA = string.Empty, portB = string.Empty;
                    if (ipService.Count > 0)
                    {
                        portA = ipService[0].ipAddress;
                        if (ipService.Count > 0)
                            portB = ipService[1].ipAddress;
                    }
                    outputXML += "<portA>" + portA + "</portA>";
                    outputXML += "<portB>" + portB + "</portB>";
                    outputXML += "<ISDNAudioPref>" + mcu.ISDNAudioPrefix + "</ISDNAudioPref>";
                    outputXML += "<ISDNVideoPref>" + mcu.ISDNVideoPrefix + "</ISDNVideoPref>";
                    vrmMCUISDNServices isdn = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    outputXML += "<BridgeDomain>" + mcu.BridgeDomain + "</BridgeDomain>"; //ZD 100522
                    //FB 2636 Starts
                    outputXML += "<E164Dialing>" + mcu.E164Dialing + "</E164Dialing>";
                    outputXML += "<H323Dialing>" + mcu.H323Dialing + "</H323Dialing>";
                    vrmMCUE164Services E164;
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeID", mcu.BridgeID));
                    List<vrmMCUE164Services> E164Services = m_IE164Services.GetByCriteria(criterionList);
                    outputXML += "<E164Services>";
                    for (int j = 0; j < E164Services.Count; j++)
                    {
                        E164 = E164Services[j];
                        outputXML += "<E164Service>";
                        outputXML += "<SortID>" + (j + 1).ToString() + "</SortID>";
                        outputXML += "<StartRange>" + E164.StartRange + "</StartRange>";
                        outputXML += "<EndRange>" + E164.EndRange + "</EndRange>";
                        outputXML += "</E164Service>";
                    }
                    outputXML += "</E164Services>";
                    //FB 2636 Ends
                    outputXML += "<Synchronous>" + mcu.Synchronous + "</Synchronous>"; //FB 2718
                    outputXML += "<Domain>" + mcu.RPRMDomain + "</Domain>"; //FB 2718
                    outputXML += "</bridgeDetails>";
                }
                //FB 2501 Call Monitoring Ends
                #endregion 
                #region IVIEW
                //FB 2556 - Starts
                else if (vendorList[i].BridgeInterfaceId == MCUType.iVIEW)
                {
                    outputXML += "<bridgeDetails>";
                    outputXML += "<controlPortIPAddress>" + mcu.BridgeAddress + "</controlPortIPAddress>";
                    outputXML += "<ISDNServices></ISDNServices>";
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    outputXML += "<Multitenant>" + mcu.IsMultitenant + "</Multitenant>";
                    outputXML += "<Synchronous>" + mcu.Synchronous + "</Synchronous>";
                    m_IMCUOrgSpecificDetailsDao = m_hardwareDAO.GetMCUOrgSpecificDetailsDao();//FB 2556
                    {
                        outputXML += "<orgSpecificDetails>";
                        osCriterionList = new List<ICriterion>();
                        osCriterionList.Add(Expression.Eq("bridgeID", mcu.BridgeID));
                        osCriterionList.Add(Expression.Eq("orgID", organizationID));

                        orgSpecificdetails = m_IMCUOrgSpecificDetailsDao.GetByCriteria(osCriterionList,true);

                        if (orgSpecificdetails.Count > 0)
                        {
                            for (cntDtls = 0; cntDtls < orgSpecificdetails.Count; cntDtls++)
                                outputXML += "<" + orgSpecificdetails[cntDtls].propertyName.Trim() + ">" + orgSpecificdetails[cntDtls].propertyValue.Trim() + "</" + orgSpecificdetails[cntDtls].propertyName.Trim() + ">";
 
                        }
                        outputXML += "</orgSpecificDetails>";
                    }
                    outputXML += "<ExtSiloProfiles>";
                    osCriterionList = new List<ICriterion>();
                    osCriterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    m_IExtMCUSilo = m_hardwareDAO.GetExtMCUSiloDao();
                    MCUSilo = m_IExtMCUSilo.GetByCriteria(osCriterionList);
                    for (p = 0; p < MCUSilo.Count; p++)
                    {
                        outputXML += "<Profile>";
                        outputXML += "<ID>" + MCUSilo[p].MemberId + "</ID>";
                        outputXML += "<name>" + MCUSilo[p].Name + "</name>";
                        outputXML += "</Profile>";
                    }
                    outputXML += "</ExtSiloProfiles>";

                    outputXML += "<ExtServiceProfiles>";
                    m_IExtMCUService = m_hardwareDAO.GetExtMCUServiceDao(); //FB 2556                    
                    MCUService = m_IExtMCUService.GetByCriteria(osCriterionList);
                    for (p = 0; p < MCUService.Count; p++)
                    {
                        outputXML += "<Profile>";
                        outputXML += "<ID>" + MCUService[p].ServiceId+ "</ID>";
                        outputXML += "<name>" + MCUService[p].ServiceName + "</name>";
                        outputXML += "</Profile>";
                    }
                    outputXML += "</ExtServiceProfiles>";

                    outputXML += "</bridgeDetails>";
                }
                //FB 2556 - End
                #endregion
                #region PEXIP
                //ZD 101217 Start
                else if (vendorList[i].BridgeInterfaceId == MCUType.PEXIP) // pexip bridge
                {
                    // BridgeIPAddress table contains the two ports
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("bridgeId", mcu.BridgeID));
                    List<vrmMCUIPServices> ipService = m_IipServices.GetByCriteria(criterionList);

                    outputXML = "<bridgeDetails>";
                    string portA = string.Empty, portB = string.Empty;
                    if (ipService.Count > 0)
                    {
                        portA = ipService[0].ipAddress;
                        if (ipService.Count > 0)
                            portB = ipService[1].ipAddress;
                    }
                    outputXML += "<portA>" + portA + "</portA>";
                    outputXML += "<portB>" + portB + "</portB>";
                    outputXML += "<ISDNAudioPref>" + mcu.ISDNAudioPrefix + "</ISDNAudioPref>";//FB 2003
                    outputXML += "<ISDNVideoPref>" + mcu.ISDNVideoPrefix + "</ISDNVideoPref>";
                    //NG fixes
                    vrmMCUISDNServices isdn = null; //FB 2636
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeId", mcu.BridgeID));
                    List<vrmMCUISDNServices> isdnServices = m_IisdnServices.GetByCriteria(criterionList);
                    outputXML += "<ISDNServices>";
                    for (int k = 0; k < isdnServices.Count; k++)
                    {
                        isdn = isdnServices[k];
                        outputXML += "<ISDNService>";
                        outputXML += "<SortID>" + isdn.SortID.ToString() + "</SortID>";
                        outputXML += "<RangeSortOrder>" + isdn.RangeSortOrder.ToString() + "</RangeSortOrder>";
                        outputXML += "<name>" + isdn.ServiceName + "</name>";
                        outputXML += "<prefix>" + isdn.prefix + "</prefix>";
                        outputXML += "<startRange>" + isdn.startNumber.ToString() + "</startRange>";
                        outputXML += "<endRange>" + isdn.endNumber.ToString() + "</endRange>";
                        outputXML += "<networkAccess>" + isdn.networkAccess.ToString() + "</networkAccess>";
                        outputXML += "<usage>" + isdn.usage.ToString() + "</usage>";
                        outputXML += "</ISDNService>";
                    }
                    outputXML += "</ISDNServices>";
                    //FB 2610
                    outputXML += "<BridgeExtNo>" + mcu.BridgeExtNo + "</BridgeExtNo>";
                    //FB 2610
                    outputXML += "<BridgeDomain>" + mcu.BridgeDomain + "</BridgeDomain>"; //ZD 100522
                    // FB 2636 Starts
                    outputXML += "<E164Dialing>" + mcu.E164Dialing + "</E164Dialing>";
                    outputXML += "<H323Dialing>" + mcu.H323Dialing + "</H323Dialing>";
                    vrmMCUE164Services E164 = null;
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("BridgeID", mcu.BridgeID));
                    List<vrmMCUE164Services> E164Services = m_IE164Services.GetByCriteria(criterionList);
                    outputXML += "<E164Services>";
                    for (int j = 0; j < E164Services.Count; j++)
                    {
                        E164 = E164Services[j];
                        outputXML += "<E164Service>";
                        outputXML += "<SortID>" + (j + 1).ToString() + "</SortID>";
                        outputXML += "<StartRange>" + E164.StartRange + "</StartRange>";
                        outputXML += "<EndRange>" + E164.EndRange + "</EndRange>";
                        outputXML += "</E164Service>";
                    }
                    outputXML += "</E164Services>";
                    // FB 2636 Ends

                    //ZD 101522 start
                    DataSet ds = null;
                    string hql = "";
                    if (m_bridgelayer == null)
                        m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                    hql = " Select * from Gen_SystemLocation_D where MCUId = " + mcu.BridgeID + " ";

                    if (hql != "")
                        ds = m_bridgelayer.ExecuteDataSet(hql);

                    outputXML += "<SysLocationId>" + mcu.SysLocationId + "</SysLocationId>";
                    
                    outputXML += "<GetSystemLocations>";
                    for (p = 0; p < ds.Tables[0].Rows.Count; p++)
                    {
                        outputXML += "<GetSystemLocation>";
                        outputXML += "<SysLocId>" + ds.Tables[0].Rows[p]["syslocid"].ToString() + "</SysLocId>";
                        outputXML += "<Name>" + ds.Tables[0].Rows[p]["Name"].ToString() + "</Name>";
                        outputXML += "</GetSystemLocation>";
                    }
                    outputXML += "</GetSystemLocations>";
                    //ZD 101522 End

                    outputXML += "</bridgeDetails>";
                }
                //ZD 101217 End
                #endregion
				//ZD 104021 Start
                #region BLUEJEANS
                else if (vendorList[i].BridgeInterfaceId == MCUType.BLUEJEANS) // Blue Jeans
                {
                    outputXML = "<bridgeDetails>";
                    outputXML += "<BJNAppKey>" + mcu.BJNAppKey + "</BJNAppKey>";
                    outputXML += "<BJNAppSecret>" + mcu.BJNAppSecret + "</BJNAppSecret>";
                    outputXML += "<controlPortIPAddress>" + mcu.BridgeAddress + "</controlPortIPAddress>";
                    outputXML += "</bridgeDetails>";
                }
                #endregion
				//ZD 104021 End
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
            return outputXML;
        }
        #endregion

        #region FetchBridgeTypeList
        // fetch bridge list
        private string FetchBridgeTypeList()
        {
            try
            {
                string outputXML = "<bridgeTypes>";

                List<vrmMCUVendor> vendorList = vrmGen.getMCUVendor();
                vrmMCUVendor vendor = null;
                for (int i = 0; i < vendorList.Count; i++)
                {
                    vendor = vendorList[i];
                    outputXML += "<type>";
                    outputXML += "<ID>" + vendor.id.ToString() + "</ID>";
                    outputXML += "<name>" + vendor.name + "</name>";
                    outputXML += "<interfaceType>" + vendor.BridgeInterfaceId + "</interfaceType>";
                    outputXML += "</type>";
                }

                outputXML += "</bridgeTypes>";

                return outputXML;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region FetchBridgeStatusList
        private string FetchBridgeStatusList()
        {
            try
            {
                string outputXML = "<bridgeStatuses>";
                List<vrmMCUStatus> statusList = vrmGen.getMCUStatus();

                foreach (vrmMCUStatus MCUStatus in statusList)
                {
                    outputXML += "<status>";
                    outputXML += "<ID>" + MCUStatus.id.ToString() + "</ID>";
                    outputXML += "<name>" + MCUStatus.status + "</name>";
                    outputXML += "</status>";
                }
                outputXML += "</bridgeStatuses>";

                return outputXML;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }
        #endregion

        #region FetchAddressTypeList
        // please look at GetAddressType as they are identicle accept for spelling
        public string FetchAddressTypeList()
        {
            string outXml = string.Empty;

            try
            {
                // Load the string into xml parser and retrieve the tag values.

                List<vrmAddressType> addressType = vrmGen.getAddressType();

                outXml = "<addressType>";
                foreach (vrmAddressType at in addressType)
                {
                    outXml += "<type>";
                    outXml += "<ID>" + at.Id.ToString() + "</ID>";
                    outXml += "<name>" + at.name + "</name>";
                    outXml += "</type>";
                }
                outXml += "</addressType>";

                return outXml;

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                throw e;
            }
            catch (Exception e)
            {
                m_log.Error("vrmException", e);
                throw e;
            }

        }
        #endregion

        #region FetchBridgeList

        // FetchBridgeList is a little different from FetchBridges 
        //  FetchBridges(only bridges with stats > 0, also returns a virtual tag and an administratot tag

        private string FetchBridgeList(ref int count)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Gt("Status", 0));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Or(Expression.Eq("orgId", organizationID), Expression.Eq("isPublic", 1))); // FB 1920
                //criterionList.Add(Expression.Eq("orgId", organizationID));//organization fixes
                m_ImcuDao.addOrderBy(Order.Asc("ChainPosition"));

                List<vrmMCU> MCUList = m_ImcuDao.GetByCriteria(criterionList);
                count = MCUList.Count;
                m_ImcuDao.sledgeHammerReset();

                string outputXML = "<bridges>";

                foreach (vrmMCU mcu in MCUList)
                {
                    outputXML += "<bridge>";
                    outputXML += "<ID>" + mcu.BridgeID.ToString() + "</ID>";
                    // FB 1920 Starts
                    outputXML += "<Orgid>" + mcu.orgId.ToString() + "</Orgid>";
                    outputXML += "<isPublic>" + mcu.isPublic.ToString() + "</isPublic>";
                    if (mcu.isPublic == 1)
                    {
                        if (organizationID != 11) //Default org 11
                        {
                            mcu.BridgeName = mcu.BridgeName + "(*)";
                            count = count - 1;
                        }
                    }
                    // FB 1920 Ends
                    outputXML += "<name>" + mcu.BridgeName + "</name>";
                    outputXML += "<interfaceType>" + mcu.MCUType.id.ToString() + "</interfaceType>";

                    vrmUser user = m_vrmUserDAO.GetByUserId(mcu.Admin);
                    //* *** FB 1462 - start *** */
                    if (user != null)
                    {
                        outputXML += "<administrator>" + user.FirstName + ", " + user.LastName + "</administrator>";
                        outputXML += "<userstate>A</userstate>";
                    }
                    else
                    {
                        vrmInactiveUser iuser = m_vrmIUserDAO.GetByUserId(mcu.Admin);

                        if (iuser != null)
                        {
                            outputXML += "<administrator>" + iuser.FirstName + ", " + iuser.LastName + "</administrator>";
                            outputXML += "<userstate>I</userstate>";
                        }
                        else
                        {
                            outputXML += "<administrator>User does not exist</administrator>";
                            outputXML += "<userstate>D</userstate>";
                        }
                    }
                    /* *** FB 1462 - end *** */

                    string exist = "0";
                    if (mcu.VirtualBridge == 0)
                        exist = "1";
                    outputXML += "<exist>" + exist + "</exist>";
                    outputXML += "<status>" + mcu.Status.ToString() + "</status>";
                    outputXML += "<order>" + mcu.ChainPosition.ToString() + "</order>";
                    outputXML += "<address>" + mcu.BridgeAddress + "</address>"; //FB 2362
                    outputXML += "<PollStatus>" + mcu.PollCount + "</PollStatus>"; //ZD 100369_MCU
                    outputXML += "</bridge>";
                }
                outputXML += "</bridges>";
                return outputXML;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region FetchBridges

        private string FetchBridges()
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", organizationID));
                criterionList.Add(Expression.Eq("deleted", 0));

                List<vrmMCU> MCUList = m_ImcuDao.GetByCriteria(criterionList);

                string outputXML = "<bridges>";

                foreach (vrmMCU mcu in MCUList)
                {
                    outputXML += "<bridge>";
                    outputXML += "<ID>" + mcu.BridgeID.ToString() + "</ID>";
                    outputXML += "<name>" + mcu.BridgeName + "</name>";
                    outputXML += "<interfaceType>" + mcu.BridgeTypeId.ToString() + "</interfaceType>";

                    vrmUser user = m_vrmUserDAO.GetByUserId(mcu.Admin);

                    /* *** FB 1462 - Fixes start *** */
                    if (user != null)
                    {
                        outputXML += "<administrator>" + user.FirstName + ", " + user.LastName + "</administrator>";
                        outputXML += "<userstate>A</userstate>";
                    }
                    else
                    {
                        vrmInactiveUser iuser = m_vrmIUserDAO.GetByUserId(mcu.Admin);

                        if (iuser != null)
                        {
                            outputXML += "<administrator>" + iuser.FirstName + ", " + iuser.LastName + "</administrator>";
                            outputXML += "<userstate>I</userstate>";
                        }
                        else
                        {
                            outputXML += "<administrator>User does not exist</administrator>";
                            outputXML += "<userstate>D</userstate>";
                        }
                    }
                    /* *** FB 1462 - Fixes end *** */

                    outputXML += "<exist>" + mcu.VirtualBridge.ToString() + "</exist>";
                    outputXML += "<status>" + mcu.Status.ToString() + "</status>";
                    outputXML += "<order>" + mcu.ChainPosition.ToString() + "</order>";
                    outputXML += "</bridge>";
                }
                outputXML += "</bridges>";
                return outputXML;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region SetBridge

        public bool SetBridge(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            XmlNode node;
            IConfApprovalDAO IApprover; //To Check Conf_approval Table//FB 1171

            string[] orgSpecificProps = null;//FB 2556-TDB
            List<vrmMCUOrgSpecificDetails> orgSpecificdetails = null;//FB TDB
            vrmMCUOrgSpecificDetails orgSpecificdetail = null;//FB TDB
            IMCUOrgSpecificDetailsDao m_IMCUOrgSpecificDetailsDao = null;
            List<ICriterion> osCriterionList = null;
            int cntDtls = 0;
			//ZD 101026 Starts
			bool isNewBridge = false;
            int AuditCardListStatus = 0, AuditIPServiceStatus = 0;
            int AuditE164ServiceStatus = 0, AuditISDNServiceStatus = 0;
            int AuditMPIServiceStatus = 0, AuditOrgspecificStatus = 0;
			//ZD 101026 End
            int EnhancedMCU = 0; // ZD 100040
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                int ConfServiceID = 0; //FB 2016 //FB 2427
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                cryptography.Crypto crypto = new cryptography.Crypto();
                int userID = 0;
                node = xd.SelectSingleNode("//setBridge/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);

                //ZD 100456
                string editFrom = "";
                node = xd.SelectSingleNode("//setBridge/EditFrom");
                if (node != null)
                    editFrom = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//setBridge/bridge/bridgeID");
                string bridgeid = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/name");
                string bridgename = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/login");
                string login = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/password");
                string password = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/bridgeType");
                string bridgetype = node.InnerXml.Trim();
                if (bridgetype.Length == 0) bridgetype = "1"; // Default to Accord
                node = xd.SelectSingleNode("//setBridge/bridge/bridgeStatus");
                string bridgeStatus = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/URLAccess");//ZD 100113
                string URLAccess = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/MultipleAssistant");//ZD 100369_MCU
                string MultipleAssistant = node.InnerXml.Trim();
                if (bridgeStatus.Length < 1) bridgeStatus = "1"; // Active
                // FB 1920 Starts
                node = xd.SelectSingleNode("//setBridge/bridge/isPublic");
                string chkisPublic = node.InnerXml.Trim();
                if (chkisPublic.Length < 1)
                    chkisPublic = "0"; // Private
                // FB 1920 Ends

                // FB 2501 CallMonitor Favourite Starts
                int chkSetFav = 0;
                node = xd.SelectSingleNode("//setBridge/bridge/setFavourite");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out chkSetFav);
                // FB 2501 Ends

                // 2660 Starts
                int EnableCDR = 0, DeleteCDR = 0;//FB 2714
                node = xd.SelectSingleNode("//setBridge/bridge/enableCDR");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EnableCDR);
                node = xd.SelectSingleNode("//setBridge/bridge/deleteCDRDays");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out DeleteCDR);
                //FB 2714 Starts
                //if (EnableCDR > 0 && (DeleteCDR < 1 || DeleteCDR > 100) )
                //{
                //    myVRMException myVRMEx = new myVRMException(670);
                //    obj.outXml = myVRMEx.FetchErrorMsg();
                //    return false;
                //}
                // FB 2660 Ends//FB 2714 Ends
                node = xd.SelectSingleNode("//setBridge/bridge/virtualBridge");
                //ZD 100040 start - corrected
                int virtbridge = 0;
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out virtbridge);

                if (virtbridge < 0) virtbridge = 0; // false
                //ZD 100040 end
                node = xd.SelectSingleNode("//setBridge/bridge/bridgeAdmin/ID");
                string bridgeadmin = node.InnerXml.Trim();
                if (bridgeadmin.Length < 1) bridgeadmin = "11"; // default
                node = xd.SelectSingleNode("//setBridge/bridge/malfunctionAlert");
                string malfunctionalert = node.InnerXml.Trim();
                if (malfunctionalert.Length < 1) malfunctionalert = "0"; // no alert
                node = xd.SelectSingleNode("//setBridge/bridge/timeZone");
                string timezone = node.InnerXml.Trim();
                if (timezone.Length < 1) timezone = "26"; // default EST hard-coded. Change to default system
                node = xd.SelectSingleNode("//setBridge/bridge/firmwareVersion");
                string firmwarever = node.InnerXml.Trim();
                node = xd.SelectSingleNode("//setBridge/bridge/percentReservedPort");
                string resportperc = node.InnerXml.Trim();
                if (resportperc.Length < 1) resportperc = "0"; // Reserve 0% ports by default

                node = xd.SelectSingleNode("//setBridge/bridge/maxAudioCalls");
                string maxAudioCalls = node.InnerXml.Trim();
                if (maxAudioCalls.Length < 1) maxAudioCalls = "1";
                node = xd.SelectSingleNode("//setBridge/bridge/maxVideoCalls");
                string maxVideoCalls = node.InnerXml.Trim();
                //FB 2660 Starts
                
                node = xd.SelectSingleNode("//setBridge/bridge/EnhancedMCU");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out EnhancedMCU);
                //FB 2660 Ends
                //ZD 101078 Start
                node = xd.SelectSingleNode("//setBridge/bridge/AccessURL");
                string AccessURL = node.InnerXml.Trim();
                //ZD 101078 End
                //API Port starts..
                int apiPort = 23;
                if (xd.SelectSingleNode("//setBridge/bridge/ApiPortNo") != null)
                {
                    int.TryParse(xd.SelectSingleNode("//setBridge/bridge/ApiPortNo").InnerText.Trim(), out apiPort);
                }
                if (apiPort <= 0)
                    apiPort = 23;
                //API Port ends..

                node = xd.SelectSingleNode("//setBridge/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();

                if (orgid == "")//Code added for organization
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes

                OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);
                if (maxVideoCalls.Length < 1) maxVideoCalls = "1";

                int BridgeID = 0;
                bridgeid = bridgeid.ToLower();
                if (bridgeid != "new")
                    BridgeID = Int32.Parse(bridgeid);

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("BridgeName", bridgename));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("orgId", organizationID));//code added for organization

                List<vrmMCU> checkMCU = m_ImcuDao.GetByCriteria(criterionList);

                if (checkMCU.Count > 0)
                {
                    if (checkMCU.Count > 1 || checkMCU[0].BridgeID != BridgeID)
                    {
                        myVRMException ex = new myVRMException(260);
                        m_log.Error("Error in SetBridge: ", ex);
                        obj.outXml = ex.FetchErrorMsg();//FB 1881//myVRMException.toXml(ex.Message);
                        return false;
                    }
                }
                int ChainPosition = 0;//ZD 101869
                if (BridgeID == 0) // New bridge //FB 1286 Starts
                {
                    isNewBridge = true; //ZD 101026
                    List<ICriterion> selcnt = new List<ICriterion>();
                    selcnt.Add(Expression.Eq("deleted", 0));
                    selcnt.Add(Expression.Eq("orgId", organizationID));//code added for organization
                    List<vrmMCU> checkMCUCount = m_ImcuDao.GetByCriteria(selcnt);
                    
                    if (checkMCUCount.Count >= orgdt.MCULimit)
                    {
                        myVRMException ex = new myVRMException(252);
                        m_log.Error("Error MCU Limit exceeeded: ", ex);
                        obj.outXml = ex.FetchErrorMsg();//FB 1881//myVRMException.toXml(ex.Message);
                        return false;
                    }
                    ChainPosition = checkMCUCount.Count + 1; //ZD 101869
                }
                //FB 1286 Ends

                //FB 2660 Starts
                if (EnhancedMCU > 0)
                {
                    List<ICriterion> EnhancedMCUList = new List<ICriterion>();
                    EnhancedMCUList.Add(Expression.Eq("EnhancedMCU", 1));
                    EnhancedMCUList.Add(Expression.Eq("deleted", 0));
                    EnhancedMCUList.Add(Expression.Eq("orgId", organizationID));
                    if (BridgeID > 0)
                        EnhancedMCUList.Add(Expression.Not(Expression.Eq("BridgeID", BridgeID)));
                    List<vrmMCU> chkMcu = m_ImcuDao.GetByCriteria(EnhancedMCUList);
                    if (chkMcu.Count >= orgdt.MCUEnchancedLimit)
                    {
                        myVRMException ex = new myVRMException(252);
                        m_log.Error("Error EnhancedMCU Limit exceeeded: ", ex);
                        obj.outXml = ex.FetchErrorMsg();
                        return false;
                    }
                }
                //FB 2660 Ends
                vrmMCU MCU = null;
                bool isEditMode = false; //ZD 104147
                if (BridgeID == 0)
                {
                    MCU = new vrmMCU();
                    MCU.BridgeID = 0;
                    MCU.orgId = organizationID;
                    MCU.ChainPosition = ChainPosition; //ZD 101869
                }
                else
                {
                    MCU = m_ImcuDao.GetById(BridgeID, true);
                    isEditMode = true;//ZD 104147
                }
                //ZD 100040 start
                if (isEditMode)
                {
                    if ((MCU.VirtualBridge == 1) && (virtbridge == 0))
                    {
                        int recordCount = 0;
                        IList recCnt = m_IMCUGrpAssignmentDao.execQuery("select count(*) from myVRM.DataLayer.vrmMCUGrpAssignment c where MCUID =" + MCU.BridgeID);
                        if (recCnt != null)
                        {
                            if (recCnt.Count > 0)
                            {
                                if (recCnt[0] != null)
                                {
                                    if (recCnt[0].ToString() != "")
                                        int.TryParse(recCnt[0].ToString(), out recordCount);
                                }
                            }
                        }

                        if (recordCount > 0)
                        {
                            myVRMException ex = new myVRMException(763);
                            obj.outXml = ex.FetchErrorMsg();
                            return false;
                        }
                    }
                }
                MCU.EnhancedMCU = EnhancedMCU;
                //ZD 100040 end
                //ZD 100456
                string EnablePollFailure = "";
                if (editFrom == "")
                {
                    //ZD 100369_MCU
                    node = xd.SelectSingleNode("//setBridge/bridge/EnablePollFailure");
                    EnablePollFailure = node.InnerXml.Trim();
                    if (EnablePollFailure == "1")
                    {
                        MCU.EnablePollFailure = 1;
                        MCU.EnhancedMCU = 1;
                        if (BridgeID == 0)
                            MCU.PollCount = -2;
                    }
                    else
                    {
                        MCU.EnablePollFailure = 0;
                        //MCU.EnhancedMCU = 1; ZD 100040
                    }
                    //ZD 100369_MCU
                }

                // FB 2653 Starts                
                
                // FB 2653 Ends //ZD 104407 - Start
                //ZD 100456
                string ispublic = "";
                if (editFrom == "")
                    ispublic = xd.SelectSingleNode("//setBridge/bridge/isPublic").InnerXml.Trim();
                
                if (virtbridge == 1) // ZD 100040
                    MCU.EnhancedMCU = 1;

                MCU.BridgeID = BridgeID;
                MCU.BridgeName = bridgename;
                MCU.BridgeLogin = login;
                MCU.BridgePassword = password; //FB 3054
                MCU.BridgeTypeId = Int32.Parse(bridgetype);
                MCU.LastModified = DateTime.UtcNow; //ZD 101026
                MCU.AccessURL = AccessURL;//ZD 101078
                MCU.timezone = Int32.Parse(timezone);

                
                List<vrmMCUVendor> vendorList = vrmGen.getMCUVendor();
                int i = -1;
                int bridgeInterfaceId = 0;
                foreach (vrmMCUVendor vendor in vendorList)
                {
                    i++;
                    if (vendor.id == MCU.BridgeTypeId)
                    {
                        bridgeInterfaceId = vendor.BridgeInterfaceId;
                        if (MCU.BridgeID == 0)
                            MCU.MCUType = new vrmMCUType();
                        MCU.MCUType.bridgeInterfaceId = vendor.BridgeInterfaceId;
                        MCU.MCUType.audioParticipants = vendor.audioparticipants;
                        MCU.MCUType.videoParticipants = vendor.videoparticipants;
                        MCU.MCUType.name = vendor.name;
                        MCU.MCUType.id = vendor.id;

                        break;
                    }
                }
				//ZD 104407 - End
                string McuIpAddress = "";
                XmlNode McuIp = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress");
                //if (McuIp != null) //ZD 104021 //ZD 104407
                    McuIpAddress = (McuIp != null && McuIp.InnerXml.Trim() != "") ? McuIp.InnerXml.Trim()
                                   : xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA").InnerXml.Trim();
                //ZD 100456
                if (editFrom == "D")
                    MCU.BridgeAddress = McuIpAddress;

                List<ICriterion> selMCucnt = new List<ICriterion>();
                selMCucnt.Add(Expression.Eq("deleted", 0));
                selMCucnt.Add(Expression.Eq("BridgeAddress", McuIpAddress));

                if (MCU.BridgeID != 0)
                    selMCucnt.Add(Expression.Not(Expression.Eq("BridgeID", MCU.BridgeID)));

                if(bridgeInterfaceId == MCUType.BLUEJEANS)//ZD 104407
                    selMCucnt.Add(Expression.Eq("orgId", organizationID));

                List<vrmMCU> chkMCUDuplicateRecCount = m_ImcuDao.GetByCriteria(selMCucnt);
                if (chkMCUDuplicateRecCount.Count > 0)
                {
                    myVRMException ex = new myVRMException(668);
                    m_log.Error("Mcu ip address has been duplicated", ex);
                    obj.outXml = ex.FetchErrorMsg();
                    return false;
                }                

                //ZD 100456
                if (editFrom == "")
                {
                    //FB 2539 Start
                    int adminId = Int32.Parse(bridgeadmin);
                    MCU.Admin = adminId; //Commented - FB 2539

                    MCU.Status = Int32.Parse(bridgeStatus);
                    MCU.URLAccess = Int32.Parse(URLAccess);//ZD 100113
                    MCU.MultipleAddress = MultipleAssistant;//ZD 100369_MCU
                    MCU.VirtualBridge = virtbridge; //ZD 100040
                    MCU.malfunctionalert = Int32.Parse(malfunctionalert);
                    MCU.SoftwareVer = firmwarever;
                    MCU.reservedportperc = Int32.Parse(resportperc);
                    MCU.MaxConcurrentAudioCalls = Int32.Parse(maxAudioCalls);
                    MCU.MaxConcurrentVideoCalls = Int32.Parse(maxVideoCalls);
                    MCU.CurrentDateTime = DateTime.Now;//FB 2448 Start
                    //MCU.LastModified = DateTime.Now; //ZD 101026
                    MCU.EnableCDR = EnableCDR; //FB 2660
                    MCU.DeleteCDRDays = DeleteCDR; //FB 2660
                    MCU.ApiPortNo = apiPort;//API Ports..
                    string xmlField;
                    node = xd.SelectSingleNode("//setBridge/bridge/ISDNThresholdAlert");
                    xmlField = node.InnerXml.Trim();
                    int isdnthresholdalert = 0;
                    if (xmlField.Length > 0)
                        isdnthresholdalert = Int32.Parse(xmlField);

                    MCU.isdnthresholdalert = isdnthresholdalert;
                    if (isdnthresholdalert == 1)
                    {
                        node = xd.SelectSingleNode("//setBridge/bridge/ISDNPortCharge");
                        xmlField = node.InnerXml.Trim();
                        if (xmlField.Length == 0)
                            MCU.isdnportrate = 0;
                        else
                            MCU.isdnportrate = Int32.Parse(xmlField);

                        node = xd.SelectSingleNode("//setBridge/bridge/ISDNLineCharge");
                        xmlField = node.InnerXml.Trim();
                        if (xmlField.Length == 0)
                            MCU.isdnlinerate = 0;
                        else
                            MCU.isdnlinerate = Int32.Parse(xmlField);

                        node = xd.SelectSingleNode("//setBridge/bridge/ISDNThreshold");
                        xmlField = node.InnerXml.Trim();
                        if (xmlField.Length == 0)
                            MCU.isdnthreshold = 0;
                        else
                            MCU.isdnthreshold = Int32.Parse(xmlField);

                        node = xd.SelectSingleNode("//setBridge/bridge/ISDNMaxCost");
                        xmlField = node.InnerXml.Trim();
                        if (xmlField.Length == 0)
                            MCU.isdnmaxcost = 0;
                        else
                            MCU.isdnmaxcost = Int32.Parse(xmlField);



                    }
                }

               

                //FB 1920 start
                List<vrmConfRoom> rList = new List<vrmConfRoom>();
                List<vrmConfUser> uList = new List<vrmConfUser>();
                List<vrmConference> confList = new List<vrmConference>();
                criterionList = new List<ICriterion>();
                if (chkisPublic == "0" && MCU.isPublic == 1)
                {
                    criterionList.Add(Expression.Eq("deleted", 0));
                    criterionList.Add(Expression.Not(Expression.Eq("status", (int)vrmConfStatus.Completed)));
                    criterionList.Add(Expression.Ge("confdate", DateTime.Now));
                    criterionList.Add(Expression.Not(Expression.Eq("orgId", 11)));
                    confList = m_vrmConfDAO.GetByCriteria(criterionList);
                    for (int c = 0; c < confList.Count; c++)
                    {
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("confid", confList[c].confid));
                        criterionList.Add(Expression.Eq("instanceid", confList[c].instanceid));
                        criterionList.Add(Expression.Eq("bridgeid", MCU.BridgeID));
                        rList = m_IconfRoom.GetByCriteria(criterionList);
                        uList = m_IconfUser.GetByCriteria(criterionList);
                        if (rList.Count > 0 || uList.Count > 0)
                        {
                            //Not allow to change as private
                            MCU.isPublic = 1;
                            myVRMException ex = new myVRMException(516);
                            obj.outXml = ex.FetchErrorMsg();
                            m_ImcuDao.SaveOrUpdate(MCU);
                            return false;
                        }
                    }
                }

                //ZD 100456
                string BridgeExtNo = "", BridgeDomain = " ";//FB 2610 //ZD 100522
                List<int> MCUApproverIDs = new List<int>(); int MCUAppoverID=0; //ZD 104147
                if (editFrom == "")
                {
                    MCU.isPublic = Int32.Parse(chkisPublic);
                    MCU.setFavourite = chkSetFav; // FB 2501 CallMonitor Favourite
                    //FB 1920 end
                    MCU.BridgeExtNo = ""; //FB 2610
                    MCU.BridgeDomain = "";//ZD 100522

                    //FB 2539 Start
                    XmlNodeList appList1 = xd.GetElementsByTagName("approver");
                    foreach (XmlNode innerNode in appList1)
                    {
                        XmlElement innerElement = (XmlElement)innerNode;
                        string sAppId = innerElement.GetElementsByTagName("ID")[0].InnerText;
                        if (sAppId != "" && sAppId != null)
                        {
                            int.TryParse(sAppId, out MCUAppoverID);//ZD 104147
                            MCUApproverIDs.Add(MCUAppoverID);
                            if (!m_SearchFactory.CheckApproverRights(sAppId))
                            {
                                myVRMException myVRMEx = new myVRMException(632);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }
                    }
                }
                //FB 2539 End

                m_ImcuDao.SaveOrUpdate(MCU);

                //ZD 100040 starts
                //bridge/videoPorts/port
                //Remove port resolutions if any bound to MCU
                criterionList = null;

                m_IMCUPortResolutionDao = m_hardwareDAO.GetMCUPortResolutionDao();
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("BridgeId", MCU.BridgeID));
                List<vrmMCUPortResolution> ports = m_IMCUPortResolutionDao.GetByCriteria(criterionList);
                for (int p = 0; p < ports.Count; p++)
                {
                    m_IMCUPortResolutionDao.Delete(ports[p]);
                }
                ports = null;
                
                XmlNodeList nodeList = xd.SelectNodes("//setBridge/bridge/videoPorts/port");
                vrmMCUPortResolution portResn = null;
                int resid = 0, vidPortQty = 0;
                for (int k = 0; k < nodeList.Count; k++)
                {
                    node = null; resid = 0; vidPortQty = 0;
                    node = nodeList[k];
                    if (node != null)
                    {
                        Int32.TryParse(node.SelectSingleNode("resolutionId").InnerText.Trim(), out resid);
                        Int32.TryParse(node.SelectSingleNode("qty").InnerText.Trim(), out vidPortQty);                        
                        portResn = new DataLayer.vrmMCUPortResolution();
                        portResn.BridgeId = MCU.BridgeID;
                        portResn.ResolutionId = resid;
                        portResn.Port = vidPortQty;
                        m_IMCUPortResolutionDao.Save(portResn);                        
                    }
                }
                m_IMCUPortResolutionDao = null;
                //ZD 100040 ends

                //ZD 100456
                if (editFrom == "D")//Modify the MCU from DataImprot
                {
                    if (bridgeInterfaceId == 3 || bridgeInterfaceId == 8)
                    {
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        List<vrmMCUIPServices> IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        vrmMCUIPServices ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortA";
                        ipService.addressType = 1;
                        ipService.ipAddress = MCU.BridgeAddress;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);
                        
                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortB";
                        ipService.addressType = 1;
                        ipService.ipAddress = "";
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);
                        
                        AuditIPServiceStatus = 1; //ZD 101026

                    }

                    if (obj.outXml.Trim() == "")
                        obj.outXml = "<SetBridge><success>Operation successful</success><MCUID>" + MCU.BridgeID + "</MCUID></SetBridge>";

                    //ZD 100664
                    SetAuditMCU(isNewBridge, MCU.BridgeID, MCU.LastModified, userID, AuditCardListStatus, AuditIPServiceStatus, AuditE164ServiceStatus, AuditISDNServiceStatus, AuditMPIServiceStatus, AuditOrgspecificStatus); 

                    return true; //ZD 101026-Doubt
                }

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("mcuid", MCU.BridgeID));
                List<vrmMCUApprover> approverList = m_Iapprover.GetByCriteria(criterionList);
                 
                //ZD 104147 Start
                bool isApprchange = false;
                if (isEditMode)
                {
                    List<int> appIDs = approverList.Select(brd => brd.approverid).ToList();
                    if (appIDs.Count > 0 && MCUApproverIDs.Count > 0)
                    {
                        appIDs.Sort();
                        MCUApproverIDs.Sort();
                        isApprchange = appIDs.SequenceEqual(MCUApproverIDs);
                    }
                }
                //ZD 104147 End
                
                //Code Added for FB Issu 1171 --Start //ZD 104147 End
                if (!isApprchange)  //ZD 104147
                {
                    foreach (vrmMCUApprover app in approverList)
                    {
                        criterionList = new List<ICriterion>();
                        confList = new List<vrmConference>(); //FB 1920
                        criterionList.Add(Expression.Eq("deleted", 0));
                        criterionList.Add(Expression.Eq("status", (int)vrmConfStatus.Pending));
                        criterionList.Add(Expression.Ge("confdate", DateTime.Now));
                        confList = m_vrmConfDAO.GetByCriteria(criterionList);
                        foreach (vrmConference conf in confList)
                        {
                            criterionList = new List<ICriterion>();
                            rList = new List<vrmConfRoom>(); //FB 1920
                            uList = new List<vrmConfUser>(); //FB 1920
                            List<vrmMCU> mcuAppList = new List<vrmMCU>();
                            criterionList.Add(Expression.Eq("confid", conf.confid));
                            criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                            rList = m_IconfRoom.GetByCriteria(criterionList);
                            uList = m_IconfUser.GetByCriteria(criterionList);
                            loadMCUInfo(ref mcuAppList, rList, uList);

                            foreach (vrmMCU Appmcu in mcuAppList)
                            {
                                if (MCU.BridgeID == Appmcu.BridgeID)
                                {
                                    IApprover = m_confDAO.GetConfApprovalDao();
                                    criterionList = new List<ICriterion>();
                                    criterionList.Add(Expression.Eq("approverid", app.approverid));
                                    criterionList.Add(Expression.Eq("entityid", Appmcu.BridgeID));
                                    criterionList.Add(Expression.Eq("entitytype", (int)LevelEntity.MCU));
                                    criterionList.Add(Expression.Eq("confid", conf.confid));
                                    criterionList.Add(Expression.Eq("instanceid", conf.instanceid));
                                    criterionList.Add(Expression.Eq("decision", 0)); //Pending
                                    List<vrmConfApproval> ConfApprList = IApprover.GetByCriteria(criterionList);
                                    if (ConfApprList.Count > 0)
                                    {
                                        //FB 1881 start
                                        //obj.outXml = "<error><message>There are conferences pending for approval from this user.&lt;br&gt;Please approve conferences in List>View Approval Pending prior to deleting this user.</message></error>";  
                                        myVRMException myVRMEx = new myVRMException(437);
                                        obj.outXml = myVRMEx.FetchErrorMsg();
                                        //FB 1881 end
                                        return false;
                                    }
                                }
                            }
                        }
                    }

                    //Code Added for FB Issu 1171 --Start
                    foreach (vrmMCUApprover app in approverList)
                        m_Iapprover.Delete(app);

                    MCU.MCUApprover.Clear();
                    XmlNodeList appList = xd.GetElementsByTagName("approver");
                    int a = 0;
                    foreach (XmlNode innerNode in appList)
                    {
                        a++;
                        XmlElement innerElement = (XmlElement)innerNode;
                        string sAppId = innerElement.GetElementsByTagName("ID")[0].InnerText;
                        if (sAppId.Length > 0)
                        {
                            int iApproverId = Int32.Parse(sAppId);
                            vrmMCUApprover approver = new vrmMCUApprover();
                            approver.approverid = iApproverId;
                            approver.mcuid = MCU.BridgeID;
                            MCU.MCUApprover.Add(approver);
                        }
                    }
                    if (a > 0)
                        m_ImcuDao.SaveOrUpdate(MCU);
                }  

                MCU.EnableMessage = 0; //FB 2486
                MCU.BridgeExtNo = ""; //FB 2610
                MCU.BridgeDomain = "";//ZD 100522
				
                switch (bridgeInterfaceId)
                {
                    case MCUType.POLYCOM:
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress");
                        MCU.BridgeAddress = node.InnerXml.Trim();

                        //FB 1766 - Start
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/EnableIVR");
                        int EnableIVR = 0;
                        Int32.TryParse(node.InnerXml.Trim(), out EnableIVR);
                        MCU.EnableIVR = EnableIVR;

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/IVRServiceName");
                        MCU.IVRServiceName = node.InnerXml.Trim();
                        //FB 1766 - End
                        //FB 1907 - Start
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/enableRecord");
                        int EnableRecord = 0;
                        Int32.TryParse(node.InnerXml.Trim(), out EnableRecord);
                        MCU.EnableRecording = EnableRecord;

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/enableLPR");
                        int enableLPR = 0;
                        Int32.TryParse(node.InnerXml.Trim(), out enableLPR);
                        MCU.LPR = enableLPR;

                        //FB 2486 - Start
                        int EnableMessage = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/EnableMessage");
                        int.TryParse(node.InnerXml.Trim(), out EnableMessage);
                        MCU.EnableMessage = EnableMessage;
                        //FB 2486 - End
                        //FB 2636 Starts
                        int tmpE164Dialing = 0, tmpH323Dialing = 0;
                        //MCU.EnhancedMCU = 0; // ZD 100040
                        if (EnableMessage > 0)
                            MCU.EnhancedMCU = 1;
                        //FB 2636 Ends
                        //FB 2441 II Starts
                        MCU.E164Dialing = 0;//ZD 103821
                        MCU.H323Dialing = 0;//ZD 103821

                        string RPRMDomain = "";
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/RPRMDomain");
                        if (node != null)
                            RPRMDomain = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/RPRMDomain").InnerText.Trim();
                        MCU.RPRMDomain = RPRMDomain;

                        int isSynchronous = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/Synchronous");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/Synchronous").InnerText.Trim(), out isSynchronous);

                        if (MCU.Synchronous == 1 && isSynchronous == 0)
                        {
                            string hql;
                            DataSet ds = null;

                            if (m_bridgelayer == null)
                                m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                            //hql = " Select a.confid,a.instanceid from conf_conference_d b inner join conf_bridge_d a on a.confid = b.confid ";
                            //hql += " and a.bridgeid = " + BridgeID + " and a.synchronous = 1 and b.deleted = 0 and ((getutcdate() between b.confdate and ";
                            //hql += " dateAdd(minute,b.duration,b.confdate)) or b.confdate > getutcdate() )";
                            //Doubt
                            hql = " Select a.confid,a.instanceid from conf_conference_d b inner join conf_bridge_d a on a.confid = b.confid ";
                            hql += " and a.bridgeid = " + BridgeID + " and a.synchronous = 1 and b.deleted = 0 and ";
                            hql += " dateAdd(minute,b.duration,b.confdate) >= getutcdate() ";

                            if (hql != "")
                                ds = m_bridgelayer.ExecuteDataSet(hql);

                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                myVRMException myVRMEx = new myVRMException(665);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        MCU.Synchronous = isSynchronous;

                        //FB 2441 - Starts
                        int DMASendMail = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/Sendmail");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out DMASendMail);
                        MCU.DMASendMail = DMASendMail;


                        int DMAMonitorMCU = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/MonitorMCU");
                        if (node != null)
                            int.TryParse(node.InnerText.Trim(), out DMAMonitorMCU);
                        MCU.DMAMonitorMCU = DMAMonitorMCU;

                        string Name = "", sDMALogin = "", sDMAPassword = "", sDMAURL = "", sDMADialinprefix = "", loginname = "", email = "", sDMADomain = ""; //FB 2689 //FB 2441 II //FB 2709
                        int sDMAPort = 0;

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/EnhancedMCU");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/EnhancedMCU").InnerText.Trim(), out EnhancedMCU);
                        if (EnhancedMCU > 0)
                            MCU.EnhancedMCU = 1;
                        //else
                        //   MCU.EnhancedMCU = 0; ZD 100040

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAName");
                        if (node != null)
                            Name = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAName").InnerText.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMALogin");
                        if (node != null)
                            sDMALogin = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMALogin").InnerText.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAPassword");
                        if (node != null)
                            sDMAPassword = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAPassword").InnerText.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAURL");
                        if (node != null)
                            sDMAURL = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAURL").InnerText.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAPort");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMAPort").InnerText.Trim(), out sDMAPort);

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMADomain");
                        if (node != null)
                            sDMADomain = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMADomain").InnerText.Trim();
                        //FB 2689 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMADialinPrefix");
                        if (node != null)
                        {
                            sDMADialinprefix = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/DMADialinPrefix").InnerText.Trim();
                        }
                        //FB 2689 Ends

                        //FB 2709
                        int logincount = 0; //FB 2709
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/LoginAccount");
                        if (node != null)
                            loginname = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/LoginAccount").InnerText.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/LoginCount");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/LoginCount").InnerText.Trim(), out logincount);

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/RPRMEmail");
                        if (node != null)
                            email = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/PolycomRPRM/RPRMEmail").InnerText.Trim();

                        //FB 2709

                        MCU.DMAName = Name;
                        MCU.DMALogin = sDMALogin;
                        //MCU.DMAPassword = crypto.encrypt(sDMAPassword);
                        MCU.DMAPassword = sDMAPassword; //FB 3054
                        MCU.DMAURL = sDMAURL;
                        MCU.DMAPort = sDMAPort;
                        MCU.DMADomain = sDMADomain;
                        MCU.DialinPrefix = sDMADialinprefix; //FB 2689
                        //FB 2441 II Ends
                        //FB 2709
                        MCU.LoginName = loginname;
                        MCU.loginCount = logincount;
                        MCU.RPRMEmailaddress = email;
                        //FB 2709

                        if (xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ConferenceServiceID") != null) //FB 2427
                        {
                            Int32.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ConferenceServiceID").InnerXml.Trim(), out ConfServiceID);
                            MCU.ConfServiceID = ConfServiceID;
                        }

                        if (xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ConferencePoolOrderID") != null) //ZD 104256
                        {
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ConferencePoolOrderID").InnerXml.Trim(), out ConfServiceID);
                            MCU.PoolOrderID = ConfServiceID;
                        }



                        //FB 1907 - End
                        #region IP Services
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        List<vrmMCUIPServices> IPList = m_IipServices.GetByCriteria(criterionList);
						//ZD 101026 Starts
                        XmlNodeList xmlIPList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/IPServices/IPService");

                        if (IPList.Count == 0 && xmlIPList.Count == 0)
                            AuditIPServiceStatus = 0;//No Need to check; //No Changes Made
                        else if (IPList.Count == xmlIPList.Count)
                            AuditIPServiceStatus = 1; //Need to check;
                        else
                            AuditIPServiceStatus = 2;//Changes Made;
						//ZD 101026 End
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        // Now get the services and insert the info

                        vrmMCUIPServices ipService = null;
                        foreach (XmlNode innerNode in xmlIPList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string atype = innerElement.GetElementsByTagName("addressType")[0].InnerText;
                            string address = innerElement.GetElementsByTagName("address")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;
                            ipService = new vrmMCUIPServices();

                            ipService.SortID = Int32.Parse(id);
                            ipService.ServiceName = name;
                            ipService.addressType = Int32.Parse(atype);
                            ipService.ipAddress = address;
                            ipService.networkAccess = Int32.Parse(netaccess);
                            ipService.usage = Int32.Parse(usage);
                            ipService.bridgeId = MCU.BridgeID;
                            IPList.Add(ipService);
                        }
                        m_IipServices.SaveOrUpdateList(IPList);
                        #endregion

                        #region ISDN Services
                        // ISDNServices updation in BridgeISDNServices table
                        // First delete all ISDN services for this bridge

                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeId", MCU.BridgeID));
                        List<vrmMCUISDNServices> ISDNList = m_IisdnServices.GetByCriteria(criterionList);
						//ZD 101026 Starts
                        XmlNodeList xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");

                        if (ISDNList.Count == 0 && xmlISDNList.Count == 0)
                            AuditISDNServiceStatus = 0;//No Need to check; //No Changes Made
                        else if (ISDNList.Count == xmlISDNList.Count)
                            AuditISDNServiceStatus = 1; //Need to check;
                        else
                            AuditISDNServiceStatus = 2;//Changes Made;
						//ZD 101026 End
                        foreach (vrmMCUISDNServices isdn in ISDNList)
                            m_IisdnServices.Delete(isdn);
                        ISDNList.Clear();

                        // Now get the services and insert the info

                        vrmMCUISDNServices isdnService = null;
                        foreach (XmlNode innerNode in xmlISDNList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string RSO = innerElement.GetElementsByTagName("RangeSortOrder")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string prefix = innerElement.GetElementsByTagName("prefix")[0].InnerText;
                            string start = innerElement.GetElementsByTagName("startRange")[0].InnerText;
                            string end = innerElement.GetElementsByTagName("endRange")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;

                            isdnService = new vrmMCUISDNServices();

                            isdnService.SortID = Int32.Parse(id);
                            isdnService.RangeSortOrder = Int32.Parse(RSO);
                            isdnService.ServiceName = name;
                            isdnService.prefix = prefix;
                            isdnService.startNumber = Int32.Parse(start);
                            isdnService.endNumber = Int32.Parse(end);
                            isdnService.networkAccess = Int32.Parse(netaccess);
                            isdnService.usage = Int32.Parse(usage);
                            isdnService.BridgeId = MCU.BridgeID;
                            ISDNList.Add(isdnService);
                        }
                        m_IisdnServices.SaveOrUpdateList(ISDNList);
                        #endregion

                        #region MPI Services
                        // now get MPI services information                         
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        List<vrmMCUMPIServices> MPIList = m_ImpiService.GetByCriteria(criterionList);
						//ZD 101026 Starts
                        XmlNodeList xmlMPINList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/MPIServices/MPIService");

                        if (MPIList.Count == 0 && xmlMPINList.Count == 0)
                            AuditMPIServiceStatus = 0;//No Need to check; //No Changes Made
                        else if (MPIList.Count == xmlMPINList.Count)
                            AuditMPIServiceStatus = 1; //Need to check;
                        else
                            AuditMPIServiceStatus = 2;//Changes Made;
						//ZD 101026 End
                        foreach (vrmMCUMPIServices mpi in MPIList)
                            m_ImpiService.Delete(mpi);
                        MPIList.Clear();

                        // Now get the services and insert the info

                        vrmMCUMPIServices mpiService = null;
                        foreach (XmlNode innerNode in xmlMPINList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string atype = innerElement.GetElementsByTagName("addressType")[0].InnerText;
                            string address = innerElement.GetElementsByTagName("address")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;

                            if (netaccess.Length == 0)
                                netaccess = "0";
                            if (usage.Length == 0)
                                usage = "0";

                            mpiService = new vrmMCUMPIServices();
                            mpiService.SortID = Int32.Parse(id);
                            mpiService.ServiceName = name;
                            mpiService.addressType = Int32.Parse(atype);
                            mpiService.ipAddress = address;
                            mpiService.networkAccess = Int32.Parse(netaccess);
                            mpiService.usage = Int32.Parse(usage);
                            mpiService.bridgeId = MCU.BridgeID;
                            MPIList.Add(mpiService);
                        }
                        m_ImpiService.SaveOrUpdateList(MPIList);
                        #endregion

                        #region MCU Cards
                        // Now process MCU cards
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        List<vrmMCUCardList> CardList = m_IcardList.GetByCriteria(criterionList);
						//ZD 101026 Starts
                        XmlNodeList xmlCardList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/MCUCards/MCUCard");
                        if (CardList.Count == 0 && xmlCardList.Count == 0)
                            AuditCardListStatus = 0;//No Need to check; //No Changes Made
                        else if (CardList.Count == xmlCardList.Count)
                            AuditCardListStatus = 1; //Need to check;
                        else
                            AuditCardListStatus = 2;//Changes Made;
						//ZD 101026 End
                        foreach (vrmMCUCardList cl in CardList)
                            m_IcardList.Delete(cl);

                        MCU.MCUCardList.Clear();

                        vrmMCUCardList card = null;
                        i = 0;
                        foreach (XmlNode innerNode in xmlCardList)
                        {
                            i++;
                            XmlElement innerElement = (XmlElement)innerNode;

                            string cardId = innerElement.GetElementsByTagName("ID")[0].InnerText;
                            string MaximumCalls = innerElement.GetElementsByTagName("MaximumCalls")[0].InnerText;
                            if (MaximumCalls.Length == 0)
                                MaximumCalls = "0";

                            card = new vrmMCUCardList();
                            card.MCUCardDetail = new vrmMCUCardDetail();

                            card.id = 0;
                            card.MCUCardDetail.id = Int32.Parse(cardId);
                            card.maxCalls = Int32.Parse(MaximumCalls);
                            card.bridgeId = MCU.BridgeID;

                            MCU.MCUCardList.Add(card);
                        }
                        #endregion

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends

                        //ZD 100522 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeDomain");
                        if (node != null)
                            BridgeDomain = node.InnerXml.Trim();

                        MCU.BridgeDomain = BridgeDomain;
                        //ZD 100522 Ends

                        //FB 2636 Starts
                        #region E.164 Services
                        vrmMCUE164Services E164 = null;
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeID", MCU.BridgeID));
                        List<vrmMCUE164Services> E164List = m_IE164Services.GetByCriteria(criterionList);
						//ZD 101026 Starts
                        XmlNodeList xmlE164List = xd.SelectNodes("//setBridge/bridge/bridgeDetails/E164Services/E164Service");
                        
                        if (E164List.Count == 0 && xmlE164List.Count == 0)
                            AuditE164ServiceStatus = 0;//No Need to check; //No Changes Made
                        else if (E164List.Count == xmlE164List.Count)
                            AuditE164ServiceStatus = 1; //Need to check;
                        else
                            AuditE164ServiceStatus = 2;//Changes Made;
						//ZD 101026 End
                        for (int k = 0; k < E164List.Count; k++)
                        {
                            E164 = E164List[k];
                            m_IE164Services.Delete(E164);
                        }
                        E164List.Clear();


                        if (tmpE164Dialing > 0)
                        {
                            if (xmlE164List.Count > 0)
                            {
                                MCU.E164Dialing = tmpE164Dialing;
                                MCU.EnhancedMCU = 1;
                            }
                            else
                            {
                                myVRMException myVRMEx = new myVRMException(666);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        XmlNode E164innerNode;
                        int SortID = 0;
                        string StartRange = "", EndRange = "";
                        vrmMCUE164Services E164Service = null;
                        for (int j = 0; j < xmlE164List.Count; j++)
                        {
                            E164innerNode = xmlE164List[j];
                            XmlElement innerElement = (XmlElement)E164innerNode;
                            StartRange = innerElement.GetElementsByTagName("StartRange")[0].InnerText;
                            EndRange = innerElement.GetElementsByTagName("EndRange")[0].InnerText;
                            E164Service = new vrmMCUE164Services();
                            E164Service.StartRange = StartRange;
                            E164Service.EndRange = EndRange;
                            E164Service.SortID = j + 1;
                            E164Service.BridgeID = MCU.BridgeID;
                            E164List.Add(E164Service);
                        }
                        m_IE164Services.SaveOrUpdateList(E164List);
                        #endregion
                        //FB 2636 Ends
                        m_ImcuDao.SaveOrUpdate(MCU);
                        break;

                    case MCUType.CODIAN:
                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA");
                        string portA = node.InnerXml.Trim();
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portB");
                        string portB = node.InnerXml.Trim();
                        //FB 2003 - Start
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNAudioPref");//FB 2003                        
                        MCU.ISDNAudioPrefix = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNVideoPref");//FB 2003                                               
                        MCU.ISDNVideoPrefix = node.InnerXml.Trim();
                        //FB 2003 - End

                        MCU.BridgeAddress = portA;

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends

                        //ZD 100522 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeDomain");
                        if (node != null)
                            BridgeDomain = node.InnerXml.Trim();

                        MCU.BridgeDomain = BridgeDomain;
                        //ZD 100522 Ends

                        //FB 2636 Starts
                        tmpE164Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/E164Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpE164Dialing);
                        tmpH323Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/H323Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpH323Dialing);

                        #region E.164 Services
                        E164 = null;
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeID", MCU.BridgeID));
                        E164List = m_IE164Services.GetByCriteria(criterionList);
						//ZD 101026 Starts
                        xmlE164List = xd.SelectNodes("//setBridge/bridge/bridgeDetails/E164Services/E164Service");

                        if (E164List.Count == 0 && xmlE164List.Count == 0)
                            AuditE164ServiceStatus = 0;//No Need to check; //No Changes Made
                        else if (E164List.Count == xmlE164List.Count)
                            AuditE164ServiceStatus = 1; //Need to check;
                        else
                            AuditE164ServiceStatus = 2;//Changes Made;
						//ZD 101026 End
                        for (int k = 0; k < E164List.Count; k++)
                        {
                            E164 = E164List[k];
                            m_IE164Services.Delete(E164);
                        }
                        E164List.Clear();


                        //MCU.EnhancedMCU = 0; ZD 100040
                        MCU.E164Dialing = 0;
                        MCU.H323Dialing = 0;
                        if (tmpE164Dialing > 0 || tmpH323Dialing > 0)
                        {
                            if (xmlE164List.Count > 0)
                            {
                                MCU.E164Dialing = tmpE164Dialing;
                                MCU.H323Dialing = tmpH323Dialing;
                                MCU.EnhancedMCU = 1;
                            }
                            else
                            {
                                myVRMException myVRMEx = new myVRMException(666);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        E164innerNode = null;
                        SortID = 0;
                        StartRange = ""; EndRange = "";
                        E164Service = null;
                        for (int j = 0; j < xmlE164List.Count; j++)
                        {
                            E164innerNode = xmlE164List[j];
                            XmlElement innerElement = (XmlElement)E164innerNode;
                            StartRange = innerElement.GetElementsByTagName("StartRange")[0].InnerText;
                            EndRange = innerElement.GetElementsByTagName("EndRange")[0].InnerText;
                            E164Service = new vrmMCUE164Services();
                            E164Service.StartRange = StartRange;
                            E164Service.EndRange = EndRange;
                            E164Service.SortID = j + 1;
                            E164Service.BridgeID = MCU.BridgeID;
                            E164List.Add(E164Service);
                        }
                        m_IE164Services.SaveOrUpdateList(E164List);
                        #endregion

                        //FB 2636 Ends
                        m_ImcuDao.SaveOrUpdate(MCU);

                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortA";
                        ipService.addressType = 1;
                        ipService.ipAddress = portA;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortB";
                        ipService.addressType = 1;
                        ipService.ipAddress = portB;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        AuditIPServiceStatus = 1; //Need to check;
                        #endregion

                        break;

                    case MCUType.TANDBERG: // Tandberg bridge
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress");
                        string bridgeAddress = node.InnerXml.Trim();
                        MCU.BridgeAddress = bridgeAddress;

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends

                        //ZD 100522 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeDomain");
                        if (node != null)
                            BridgeDomain = node.InnerXml.Trim();

                        MCU.BridgeDomain = BridgeDomain;
                        //ZD 100522 Ends

                        m_ImcuDao.SaveOrUpdate(MCU);

                        #region ISDN Service
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeId", MCU.BridgeID));
                        ISDNList = m_IisdnServices.GetByCriteria(criterionList);
						//ZD 101026 Starts
                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");

                        if (ISDNList.Count == 0 && xmlISDNList.Count == 0)
                            AuditISDNServiceStatus = 0;//No Need to check; //No Changes Made
                        else if (ISDNList.Count == xmlISDNList.Count)
                            AuditISDNServiceStatus = 1; //Need to check;
                        else
                            AuditISDNServiceStatus = 2;//Changes Made;
						//ZD 101026 End
                        foreach (vrmMCUISDNServices isdn in ISDNList)
                            m_IisdnServices.Delete(isdn);
                        ISDNList.Clear();


                        isdnService = null;
                        foreach (XmlNode innerNode in xmlISDNList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string RSO = innerElement.GetElementsByTagName("RangeSortOrder")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string prefix = innerElement.GetElementsByTagName("prefix")[0].InnerText;
                            string start = innerElement.GetElementsByTagName("startRange")[0].InnerText;
                            string end = innerElement.GetElementsByTagName("endRange")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;

                            isdnService = new vrmMCUISDNServices();

                            isdnService.SortID = Int32.Parse(id);
                            isdnService.RangeSortOrder = Int32.Parse(RSO);
                            isdnService.ServiceName = name;
                            isdnService.prefix = prefix;
                            isdnService.startNumber = Int32.Parse(start);
                            isdnService.endNumber = Int32.Parse(end);
                            isdnService.networkAccess = Int32.Parse(netaccess);
                            isdnService.usage = Int32.Parse(usage);
                            isdnService.BridgeId = MCU.BridgeID;
                            ISDNList.Add(isdnService);
                        }
                        m_IisdnServices.SaveOrUpdateList(ISDNList);
                        #endregion



                        break;

                    case MCUType.RADVISION: // Radvision bridge
                        MCU.BridgeAddress = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress").InnerXml.Trim();
                        Int32.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ConferenceServiceID").InnerXml.Trim(), out ConfServiceID); // FB 2016
                        MCU.ConfServiceID = ConfServiceID; //FB 2016

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends

                        //ZD 100522 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeDomain");
                        if (node != null)
                            BridgeDomain = node.InnerXml.Trim();

                        MCU.BridgeDomain = BridgeDomain;
                        //ZD 100522 Ends
                        

                        m_ImcuDao.SaveOrUpdate(MCU);

                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
						//ZD 101026 Starts
                        xmlIPList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/IPServices/IPService");

                        if (IPList.Count == 0 && xmlIPList.Count == 0)
                            AuditIPServiceStatus = 0;//No Need to check; //No Changes Made
                        else if (IPList.Count == xmlIPList.Count)
                            AuditIPServiceStatus = 1; //Need to check;
                        else
                            AuditIPServiceStatus = 2;//Changes Made;
						//ZD 101026 End
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        // Now get the services and insert the info

                        ipService = null;
                        foreach (XmlNode innerNode in xmlIPList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string atype = innerElement.GetElementsByTagName("addressType")[0].InnerText;
                            string address = innerElement.GetElementsByTagName("address")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;
                            ipService = new vrmMCUIPServices();

                            ipService.SortID = Int32.Parse(id);
                            ipService.ServiceName = name;
                            ipService.addressType = Int32.Parse(atype);
                            ipService.ipAddress = address;
                            ipService.networkAccess = Int32.Parse(netaccess);
                            ipService.usage = Int32.Parse(usage);
                            ipService.bridgeId = MCU.BridgeID;
                            IPList.Add(ipService);
                        }
                        m_IipServices.SaveOrUpdateList(IPList);

                        #endregion

                        #region ISDN Services
                        // ISDN Services
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeId", MCU.BridgeID));
                        ISDNList = m_IisdnServices.GetByCriteria(criterionList);
						//ZD 101026 Starts
                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");

                        if (ISDNList.Count == 0 && xmlISDNList.Count == 0)
                            AuditISDNServiceStatus = 0;//No Need to check; //No Changes Made
                        else if (ISDNList.Count == xmlISDNList.Count)
                            AuditISDNServiceStatus = 1; //Need to check;
                        else
                            AuditISDNServiceStatus = 2;//Changes Made;
						//ZD 101026 End
                        foreach (vrmMCUISDNServices isdn in ISDNList)
                            m_IisdnServices.Delete(isdn);
                        ISDNList.Clear();


                        isdnService = null;
                        foreach (XmlNode innerNode in xmlISDNList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string RSO = innerElement.GetElementsByTagName("RangeSortOrder")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string prefix = innerElement.GetElementsByTagName("prefix")[0].InnerText;
                            string start = innerElement.GetElementsByTagName("startRange")[0].InnerText;
                            string end = innerElement.GetElementsByTagName("endRange")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;

                            isdnService = new vrmMCUISDNServices();

                            isdnService.SortID = Int32.Parse(id);
                            isdnService.RangeSortOrder = Int32.Parse(RSO);
                            isdnService.ServiceName = name;
                            isdnService.prefix = prefix;
                            isdnService.startNumber = Int32.Parse(start);
                            isdnService.endNumber = Int32.Parse(end);
                            isdnService.networkAccess = Int32.Parse(netaccess);
                            isdnService.usage = Int32.Parse(usage);
                            isdnService.BridgeId = MCU.BridgeID;
                            ISDNList.Add(isdnService);
                        }
                        m_IisdnServices.SaveOrUpdateList(ISDNList);
                        #endregion

                        break;
                    case MCUType.MSE8000: //NGC fixes
                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA");
                        string portMSEA = node.InnerXml.Trim();
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portB");
                        string portMSEB = node.InnerXml.Trim();
                        //FB 2003 - Start
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNAudioPref");
                        MCU.ISDNAudioPrefix = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNVideoPref");
                        MCU.ISDNVideoPrefix = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNGateway");
                        MCU.ISDNGateway = node.InnerXml.Trim();
                        //FB 2003 - End

                        //FB 2636 Starts
                        tmpE164Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/E164Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpE164Dialing);
                        tmpH323Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/H323Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpH323Dialing);

                        #region E.164 Services
                        E164 = null;
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeID", MCU.BridgeID));
                        E164List = m_IE164Services.GetByCriteria(criterionList);
						//ZD 101026 Starts
                        xmlE164List = xd.SelectNodes("//setBridge/bridge/bridgeDetails/E164Services/E164Service");

                        if (E164List.Count == 0 && xmlE164List.Count == 0)
                            AuditE164ServiceStatus = 0;//No Need to check; //No Changes Made
                        else if (E164List.Count == xmlE164List.Count)
                            AuditE164ServiceStatus = 1; //Need to check;
                        else
                            AuditE164ServiceStatus = 2;//Changes Made;
						//ZD 101026 End
                        for (int k = 0; k < E164List.Count; k++)
                        {
                            E164 = E164List[k];
                            m_IE164Services.Delete(E164);
                        }
                        E164List.Clear();


                       // MCU.EnhancedMCU = 0; ZD 100040
                        MCU.E164Dialing = 0;
                        MCU.H323Dialing = 0;
                        if (tmpE164Dialing > 0 || tmpH323Dialing > 0)
                        {
                            if (xmlE164List.Count > 0)
                            {
                                MCU.E164Dialing = tmpE164Dialing;
                                MCU.H323Dialing = tmpH323Dialing;
                                MCU.EnhancedMCU = 1;
                            }
                            else
                            {
                                myVRMException myVRMEx = new myVRMException(666);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        E164innerNode = null;
                        SortID = 0;
                        StartRange = ""; EndRange = "";
                        E164Service = null;
                        for (int j = 0; j < xmlE164List.Count; j++)
                        {
                            E164innerNode = xmlE164List[j];
                            XmlElement innerElement = (XmlElement)E164innerNode;
                            StartRange = innerElement.GetElementsByTagName("StartRange")[0].InnerText;
                            EndRange = innerElement.GetElementsByTagName("EndRange")[0].InnerText;
                            E164Service = new vrmMCUE164Services();
                            E164Service.StartRange = StartRange;
                            E164Service.EndRange = EndRange;
                            E164Service.SortID = j + 1;
                            E164Service.BridgeID = MCU.BridgeID;
                            E164List.Add(E164Service);
                        }
                        m_IE164Services.SaveOrUpdateList(E164List);
                        #endregion

                        //FB 2636 Ends

                        MCU.BridgeAddress = portMSEA;

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends

                        //ZD 100522 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeDomain");
                        if (node != null)
                            BridgeDomain = node.InnerXml.Trim();

                        MCU.BridgeDomain = BridgeDomain;
                        //ZD 100522 Ends                       

                        m_ImcuDao.SaveOrUpdate(MCU);

                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortA";
                        ipService.addressType = 1;
                        ipService.ipAddress = portMSEA;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortB";
                        ipService.addressType = 1;
                        ipService.ipAddress = portMSEB;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);
                        
                        AuditIPServiceStatus = 1; //Need to check;

                        #endregion
                        break;

                    case MCUType.LIFESIZE: // lifesize bridge FB 2261
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress");
                        string portbridgeAddress = node.InnerXml.Trim();
                        MCU.BridgeAddress = portbridgeAddress;

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends

                        //ZD 100522 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeDomain");
                        if (node != null)
                            BridgeDomain = node.InnerXml.Trim();

                        MCU.BridgeDomain = BridgeDomain;
                        //ZD 100522 Ends    

                        m_ImcuDao.SaveOrUpdate(MCU);

                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortA";
                        ipService.addressType = 1;
                        ipService.ipAddress = portbridgeAddress;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortB";
                        ipService.addressType = 1;
                        ipService.ipAddress = "";
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);
                        
                        AuditIPServiceStatus = 1; //Need to check;

                        #endregion

                        break;
                    //FB 2501 Call Monitoring Start
                    case MCUType.CISCO:
                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA");
                        portA = node.InnerXml.Trim();
                        portB = ""; //FB 2718
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portB");
                        if (node != null) //FB 2718
                            portB = node.InnerXml.Trim();
                        //FB 2003 - Start
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNAudioPref");
                        if (node != null) //FB 2718
                            MCU.ISDNAudioPrefix = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNVideoPref");
                        if (node != null)
                            MCU.ISDNVideoPrefix = node.InnerXml.Trim();


                        MCU.BridgeAddress = portA;

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends

                        //ZD 100522 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeDomain");
                        if (node != null)
                            BridgeDomain = node.InnerXml.Trim();

                        MCU.BridgeDomain = BridgeDomain;
                        //ZD 100522 Ends    

                        //FB 2636 Starts
                        tmpE164Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/E164Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpE164Dialing);

                        tmpH323Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/H323Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpH323Dialing);

                        #region E.164 Services
                        E164 = null;
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeID", MCU.BridgeID));
                        E164List = m_IE164Services.GetByCriteria(criterionList);
						//ZD 101026 Starts
                        xmlE164List = xd.SelectNodes("//setBridge/bridge/bridgeDetails/E164Services/E164Service");
                        if (E164List.Count == 0 && xmlE164List.Count == 0)
                            AuditE164ServiceStatus = 0;//No Need to check; //No Changes Made
                        else if (E164List.Count == xmlE164List.Count)
                            AuditE164ServiceStatus = 1; //Need to check;
                        else
                            AuditE164ServiceStatus = 2;//Changes Made;
						//ZD 101026 End
                        for (int k = 0; k < E164List.Count; k++)
                        {
                            E164 = E164List[k];
                            m_IE164Services.Delete(E164);
                        }
                        E164List.Clear();


                        //MCU.EnhancedMCU = 0; ZD 100040
                        MCU.E164Dialing = 0;
                        MCU.H323Dialing = 0;
                        if (tmpE164Dialing > 0 || tmpH323Dialing > 0)
                        {
                            if (xmlE164List.Count > 0)
                            {
                                MCU.E164Dialing = tmpE164Dialing;
                                MCU.EnhancedMCU = 1;
                                MCU.H323Dialing = tmpH323Dialing;
                            }
                            else
                            {
                                myVRMException myVRMEx = new myVRMException(666);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        //ZD 100971 Starts
                        isSynchronous = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Synchronous");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Synchronous").InnerText.Trim(), out isSynchronous);

                        if (isSynchronous == 1)
                        {
                            MCU.EnhancedMCU = 1;
                        }
                        //ZD 100971 Ends
                        E164innerNode = null;
                        SortID = 0;
                        StartRange = ""; EndRange = "";
                        E164Service = null;
                        for (int j = 0; j < xmlE164List.Count; j++)
                        {
                            E164innerNode = xmlE164List[j];
                            XmlElement innerElement = (XmlElement)E164innerNode;
                            StartRange = innerElement.GetElementsByTagName("StartRange")[0].InnerText;
                            EndRange = innerElement.GetElementsByTagName("EndRange")[0].InnerText;
                            E164Service = new vrmMCUE164Services();
                            E164Service.StartRange = StartRange;
                            E164Service.EndRange = EndRange;
                            E164Service.SortID = j + 1;
                            E164Service.BridgeID = MCU.BridgeID;
                            E164List.Add(E164Service);
                        }
                        m_IE164Services.SaveOrUpdateList(E164List);
                        #endregion

                        //FB 2718 Starts
                        
                        if (MCU.Synchronous == 1 && isSynchronous == 0)
                        {
                            string hql;
                            DataSet ds = null;

                            if (m_bridgelayer == null)
                                m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                            //hql = " Select a.confid,a.instanceid from conf_conference_d b inner join conf_bridge_d a on a.confid = b.confid ";
                            //hql += " and a.bridgeid = " + BridgeID + " and a.synchronous = 1 and b.deleted = 0 and ((getutcdate() between b.confdate and ";
                            //hql += " dateAdd(minute,b.duration,b.confdate)) or b.confdate > getutcdate() )";
                            //Doubt
                            hql = " Select a.confid,a.instanceid from conf_conference_d b inner join conf_bridge_d a on a.confid = b.confid ";
                            hql += " and a.bridgeid = " + BridgeID + " and a.synchronous = 1 and b.deleted = 0 and ";
                            hql += " dateAdd(minute,b.duration,b.confdate) >= getutcdate() ";

                            if (hql != "")
                                ds = m_bridgelayer.ExecuteDataSet(hql);

                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                myVRMException myVRMEx = new myVRMException(665);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }
                        MCU.Synchronous = isSynchronous;

                        string MCUDomain = "";
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Domain");
                        if (node != null)
                            MCUDomain = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Domain").InnerText.Trim();
                        MCU.RPRMDomain = MCUDomain;
                        //FB 2718 Starts

                        //FB 2636 Ends

                        m_ImcuDao.SaveOrUpdate(MCU);

                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortA";
                        ipService.addressType = 1;
                        ipService.ipAddress = portA;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortB";
                        ipService.addressType = 1;
                        ipService.ipAddress = portB;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        AuditIPServiceStatus = 1;
                        #endregion

                        break;
                    //FB 2501 Call Monitoring End
                    //FB 2556 - Starts
                    case MCUType.iVIEW:
                        orgSpecificProps = new string[] { "ScopiaDesktopURL", "ScopiaOrgID", "ScopiaOrgLogin", "ScopiaServiceID" };//FB 2556-TDB
                        string portbridgeIp = "";
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress");
                        if (node != null)
                            portbridgeIp = node.InnerText.Trim();
                        MCU.BridgeAddress = portbridgeIp;

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();
                        MCU.BridgeExtNo = BridgeExtNo;

                        int multiTenant = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Multitenant");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Multitenant").InnerText.Trim(), out multiTenant);
                        MCU.IsMultitenant = multiTenant;

                        isSynchronous = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Synchronous");
                        if (node != null)
                            int.TryParse(xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/Synchronous").InnerText.Trim(), out isSynchronous);
                        if (sysSettings.EnableCloudInstallation == 1)//FB 2659
                            isSynchronous = 1;
                        MCU.Synchronous = isSynchronous;

                        m_ImcuDao.SaveOrUpdate(MCU);
                        //FB 2556-TDB
                        m_IMCUOrgSpecificDetailsDao = m_hardwareDAO.GetMCUOrgSpecificDetailsDao();
                        {

                            osCriterionList = new List<ICriterion>();
                            osCriterionList.Add(Expression.Eq("bridgeID", MCU.BridgeID));
                            osCriterionList.Add(Expression.Eq("orgID", organizationID));

                            orgSpecificdetails = m_IMCUOrgSpecificDetailsDao.GetByCriteria(osCriterionList, true);

                            if (orgSpecificdetails.Count > 0)
                            {
                                for (cntDtls = 0; cntDtls < orgSpecificdetails.Count; cntDtls++)
                                    m_IMCUOrgSpecificDetailsDao.Delete(orgSpecificdetails[cntDtls]);

                            }
                            orgSpecificdetails = new List<vrmMCUOrgSpecificDetails>();
                            for (cntDtls = 0; cntDtls < orgSpecificProps.Length; cntDtls++)
                            {
                                node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/orgSpecificDetails/" + orgSpecificProps[cntDtls].Trim());
                                if (node != null)
                                {
                                    orgSpecificdetail = new vrmMCUOrgSpecificDetails();
                                    orgSpecificdetail.bridgeID = MCU.BridgeID;
                                    orgSpecificdetail.orgID = organizationID;
                                    orgSpecificdetail.propertyName = orgSpecificProps[cntDtls].Trim();
                                    orgSpecificdetail.propertyValue = node.InnerText.Trim();
                                    orgSpecificdetails.Add(orgSpecificdetail);
                                }


                            }
                            if (orgSpecificdetails.Count > 0)
                                m_IMCUOrgSpecificDetailsDao.SaveOrUpdateList(orgSpecificdetails);
                            
                            AuditOrgspecificStatus = 1; //ZD 101026
                        }

                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortA";
                        ipService.addressType = 1;
                        ipService.ipAddress = portbridgeIp;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortB";
                        ipService.addressType = 1;
                        ipService.ipAddress = "";
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);
                        
                        AuditIPServiceStatus = 1; //Need to check; //ZD 101026
                        #endregion

                        #region ISDN Service
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeId", MCU.BridgeID));
                        ISDNList = m_IisdnServices.GetByCriteria(criterionList);
						//ZD 101026 Starts
                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");

                        if (ISDNList.Count == 0 && xmlISDNList.Count == 0)
                            AuditISDNServiceStatus = 0;//No Need to check; //No Changes Made
                        else if (ISDNList.Count == xmlISDNList.Count)
                            AuditISDNServiceStatus = 1; //Need to check;
                        else
                            AuditISDNServiceStatus = 2;//Changes Made;
						//ZD 101026 End

                        foreach (vrmMCUISDNServices isdn in ISDNList)
                            m_IisdnServices.Delete(isdn);
                        ISDNList.Clear();


                        isdnService = null;
                        foreach (XmlNode innerNode in xmlISDNList)
                        {
                            XmlElement innerElement = (XmlElement)innerNode;

                            string id = innerElement.GetElementsByTagName("SortID")[0].InnerText;
                            string RSO = innerElement.GetElementsByTagName("RangeSortOrder")[0].InnerText;
                            string name = innerElement.GetElementsByTagName("name")[0].InnerText;
                            string prefix = innerElement.GetElementsByTagName("prefix")[0].InnerText;
                            string start = innerElement.GetElementsByTagName("startRange")[0].InnerText;
                            string end = innerElement.GetElementsByTagName("endRange")[0].InnerText;
                            string netaccess = innerElement.GetElementsByTagName("networkAccess")[0].InnerText;
                            string usage = innerElement.GetElementsByTagName("usage")[0].InnerText;

                            isdnService = new vrmMCUISDNServices();

                            isdnService.SortID = Int32.Parse(id);
                            isdnService.RangeSortOrder = Int32.Parse(RSO);
                            isdnService.ServiceName = name;
                            isdnService.prefix = prefix;
                            isdnService.startNumber = Int32.Parse(start);
                            isdnService.endNumber = Int32.Parse(end);
                            isdnService.networkAccess = Int32.Parse(netaccess);
                            isdnService.usage = Int32.Parse(usage);
                            isdnService.BridgeId = MCU.BridgeID;
                            ISDNList.Add(isdnService);
                        }
                        m_IisdnServices.SaveOrUpdateList(ISDNList);
                        #endregion
                        break;
                    //FB 2556 - End

                    //ZD 101217 Start
                    case MCUType.PEXIP:
                        xmlISDNList = xd.SelectNodes("//setBridge/bridge/bridgeDetails/ISDNServices/ISDNService");
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portA");
                        portA = node.InnerXml.Trim();
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/portB");
                        portB = node.InnerXml.Trim();
                        //FB 2003 - Start
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNAudioPref");//FB 2003                        
                        MCU.ISDNAudioPrefix = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/ISDNVideoPref");//FB 2003                                               
                        MCU.ISDNVideoPrefix = node.InnerXml.Trim();
                        //FB 2003 - End

                        MCU.BridgeAddress = portA;

                        //FB 2610 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeExtNo");
                        if (node != null)
                            BridgeExtNo = node.InnerXml.Trim();

                        MCU.BridgeExtNo = BridgeExtNo;
                        //FB 2610 Ends

                        //ZD 100522 Starts
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BridgeDomain");
                        if (node != null)
                            BridgeDomain = node.InnerXml.Trim();

                        MCU.BridgeDomain = BridgeDomain;
                        //ZD 100522 Ends

                        //FB 2636 Starts
                        tmpE164Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/E164Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpE164Dialing);
                        tmpH323Dialing = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/H323Dialing");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out tmpH323Dialing);

                        #region E.164 Services
                        E164 = null;
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("BridgeID", MCU.BridgeID));
                        E164List = m_IE164Services.GetByCriteria(criterionList);
                        //ZD 101026 Starts
                        xmlE164List = xd.SelectNodes("//setBridge/bridge/bridgeDetails/E164Services/E164Service");

                        if (E164List.Count == 0 && xmlE164List.Count == 0)
                            AuditE164ServiceStatus = 0;//No Need to check; //No Changes Made
                        else if (E164List.Count == xmlE164List.Count)
                            AuditE164ServiceStatus = 1; //Need to check;
                        else
                            AuditE164ServiceStatus = 2;//Changes Made;
                        //ZD 101026 End
                        for (int k = 0; k < E164List.Count; k++)
                        {
                            E164 = E164List[k];
                            m_IE164Services.Delete(E164);
                        }
                        E164List.Clear();


                       // MCU.EnhancedMCU = 0; ZD 100040
                        MCU.E164Dialing = 0;
                        MCU.H323Dialing = 0;
                        if (tmpE164Dialing > 0 || tmpH323Dialing > 0)
                        {
                            if (xmlE164List.Count > 0)
                            {
                                MCU.E164Dialing = tmpE164Dialing;
                                MCU.H323Dialing = tmpH323Dialing;
                                MCU.EnhancedMCU = 1;
                            }
                            else
                            {
                                myVRMException myVRMEx = new myVRMException(666);
                                obj.outXml = myVRMEx.FetchErrorMsg();
                                return false;
                            }
                        }

                        E164innerNode = null;
                        SortID = 0;
                        StartRange = ""; EndRange = "";
                        E164Service = null;
                        for (int j = 0; j < xmlE164List.Count; j++)
                        {
                            E164innerNode = xmlE164List[j];
                            XmlElement innerElement = (XmlElement)E164innerNode;
                            StartRange = innerElement.GetElementsByTagName("StartRange")[0].InnerText;
                            EndRange = innerElement.GetElementsByTagName("EndRange")[0].InnerText;
                            E164Service = new vrmMCUE164Services();
                            E164Service.StartRange = StartRange;
                            E164Service.EndRange = EndRange;
                            E164Service.SortID = j + 1;
                            E164Service.BridgeID = MCU.BridgeID;
                            E164List.Add(E164Service);
                        }
                        m_IE164Services.SaveOrUpdateList(E164List);
                        #endregion

                        //FB 2636 Ends
						//ZD 101522 Starts
                        int SysLocationId = 0;
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/SysLocationId");
                        if (node != null)
                            int.TryParse(node.InnerXml.Trim(), out SysLocationId);

                        MCU.SysLocationId = SysLocationId;
						//ZD 101522 End
                        m_ImcuDao.SaveOrUpdate(MCU);

                        #region IP Service
                        // IPServices updation in BridgeIPServices table
                        // First delete all IP services for this bridge
                        criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("bridgeId", MCU.BridgeID));
                        IPList = m_IipServices.GetByCriteria(criterionList);
                        foreach (vrmMCUIPServices ip in IPList)
                            m_IipServices.Delete(ip);
                        IPList.Clear();

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortA";
                        ipService.addressType = 1;
                        ipService.ipAddress = portA;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        ipService = new vrmMCUIPServices();
                        ipService.SortID = 1;
                        ipService.ServiceName = "PortB";
                        ipService.addressType = 1;
                        ipService.ipAddress = portB;
                        ipService.bridgeId = MCU.BridgeID;
                        m_IipServices.SaveOrUpdate(ipService);

                        AuditIPServiceStatus = 1; //Need to check;
                        #endregion

                        break;
                    //ZD 101217 End
                    //ZD 104021 Start
                    case MCUType.BLUEJEANS:
                        MCU.EnhancedMCU = 0; // NOT AN Enchanced MCU  //ZD 104225
                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/controlPortIPAddress");
                        MCU.BridgeAddress = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BJNAppKey");
                        if (node != null)
                            MCU.BJNAppKey = node.InnerXml.Trim();

                        node = xd.SelectSingleNode("//setBridge/bridge/bridgeDetails/BJNAppSecret");
                        if (node != null)
                            MCU.BJNAppSecret = node.InnerXml.Trim();

                        m_ImcuDao.SaveOrUpdate(MCU);
                        break;
					//ZD 104021 End
                }

                //ZD 100369_MCU Start
                if (EnablePollFailure == "1")
                    MCU.EnhancedMCU = 1;

                MCU.LastModifiedUser = userID; //ZD 100664

                m_ImcuDao.SaveOrUpdate(MCU);
                //ZD 100369_MCU End

                //ZD 100664
                SetAuditMCU(isNewBridge, MCU.BridgeID, MCU.LastModified, userID, AuditCardListStatus, AuditIPServiceStatus, AuditE164ServiceStatus, AuditISDNServiceStatus, AuditMPIServiceStatus, AuditOrgspecificStatus);

                if (obj.outXml.Trim() == "") //FB 1920
                    obj.outXml = "<SetBridge><success>Operation successful</success><MCUID>" + MCU.BridgeID + "</MCUID></SetBridge>";//FB 2709

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                return false;
            }
        }

        #endregion

        #region loadMCUInfo
        //Code added FO FB Issue 1171

        public bool loadMCUInfo(ref List<vrmMCU> mcuList, List<vrmConfRoom> confRoom, List<vrmConfUser> confUser)
        {
            try
            {
                Hashtable bId = new Hashtable();
                foreach (vrmConfRoom cr in confRoom)
                {
                    if (cr.bridgeid > 0)
                    {
                        if (!bId.ContainsKey(cr.bridgeid))
                            bId.Add(cr.bridgeid, cr.bridgeid);
                    }
                }
                foreach (vrmConfUser cu in confUser)
                {
                    if (cu.bridgeid > 0 && cu.invitee == 1) // external only
                    {
                        if (!bId.ContainsKey(cu.bridgeid))
                            bId.Add(cu.bridgeid, cu.bridgeid);
                    }
                }
                IDictionaryEnumerator iEnum = bId.GetEnumerator();
                while (iEnum.MoveNext())
                {
                    int bridgeId = (int)iEnum.Value;
                    vrmMCU MCU = m_ImcuDao.GetById(bridgeId);
                    mcuList.Add(MCU);
                }
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                return false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        

        //FB 2486 End

        //Code added FO FB Issue 1171
        #endregion

        /* *** New Method - FB 1462 start *** */

        #region Fetches the selected MCUs details
        /// <summary>
        /// Fetches the selected MCUs details
        /// </summary>
        /// <param name="bridgeIDs"></param>
        /// <returns></returns>
        public bool FetchBridgeInfo(ref vrmDataObject obj)
        {
            List<int> bridgeIDs = new List<int>();
            int bridgeId = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                XmlNodeList nodes = null;
                nodes = xd.SelectNodes("Bridges/ID");
                if (nodes != null)
                {
                    foreach (XmlNode node in nodes)
                    {
                        if (node.InnerText.Trim() != "")
                        {
                            bridgeId = 0;
                            Int32.TryParse(node.InnerText.Trim(), out bridgeId);
                            if (!bridgeIDs.Contains(bridgeId))
                            {
                                bridgeIDs.Add(bridgeId);
                            }
                        }
                    }
                }

                if (bridgeIDs.Count <= 0)
                {
                    obj.outXml = "<bridges></bridges>";
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("BridgeID", bridgeIDs));
                List<vrmMCU> MCUList = m_ImcuDao.GetByCriteria(criterionList);

                if (MCUList == null)
                {
                    obj.outXml = "<bridges></bridges>";
                    return false;
                }

                obj.outXml = "<bridges>";

                foreach (vrmMCU mcu in MCUList)
                {
                    obj.outXml += "<bridge>";
                    obj.outXml += "<ID>" + mcu.BridgeID.ToString() + "</ID>";
                    obj.outXml += "<name>" + mcu.BridgeName + "</name>";
                    obj.outXml += "<interfaceType>" + mcu.MCUType.bridgeInterfaceId + "</interfaceType>";//ZD 101869

                    vrmUser user = m_vrmUserDAO.GetByUserId(mcu.Admin);

                    if (user != null)
                    {
                        obj.outXml += "<administrator>" + user.FirstName + ", " + user.LastName + "</administrator>";
                        obj.outXml += "<userstate>A</userstate>";
                    }
                    else
                    {
                        vrmInactiveUser iuser = m_vrmIUserDAO.GetByUserId(mcu.Admin);

                        if (iuser != null)
                        {
                            obj.outXml += "<administrator>" + iuser.FirstName + ", " + iuser.LastName + "</administrator>";
                            obj.outXml += "<userstate>I</userstate>";
                        }
                        else
                        {
                            obj.outXml += "<administrator>User does not exist</administrator>";
                            obj.outXml += "<userstate>D</userstate>";
                        }
                    }
                    obj.outXml += "<exist>" + mcu.VirtualBridge.ToString() + "</exist>";
                    obj.outXml += "<status>" + mcu.Status.ToString() + "</status>";
                    obj.outXml += "<order>" + mcu.ChainPosition.ToString() + "</order>";
                    obj.outXml += "<BridgeTypeId>" + mcu.MCUType.id + "</BridgeTypeId>"; //ZD 101869
                    obj.outXml += "</bridge>";
                }
                obj.outXml += "</bridges>";
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //FB 1881 start
                //obj.outXml = "<error>Error in fetching the bridge details</error>";
                m_log.Error("Error in fetching the bridge details");
                obj.outXml = "";
                //FB 1881 end
                return false;
            }
            return true;
        }
        #endregion

        /* *** New Method - FB 1462 end *** */

        // Method added for Endpoint Search    --- Start

        #region GetAllEndpoints
        /// <summary>
        /// Clone of GetEndpointDetails Command,Difference without endpoint id it will reaturn all endpoints.
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetAllEndpoints(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            IList EPCnt = null; //FB 2594
            IList PublicEPCnt = null; //FB 2594
            StringBuilder searchOutXml = new StringBuilder(); // String concatenation changed to StringBuilder for Performance - FB 1820
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//EndpointDetails/UserID");
                string userID = node.InnerXml.Trim();

                node = xd.SelectSingleNode("//EndpointDetails/organizationID"); //Organization Module Fixes
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
               
                //FB 2274 Starts
                node = xd.SelectSingleNode("//EndpointDetails/multisiloOrganizationID");
                string multisiloOrgID = "";
                int mutliOrgID = 0;
                if (node != null)
                    multisiloOrgID = node.InnerXml.Trim();
                Int32.TryParse(multisiloOrgID, out mutliOrgID);
                if (mutliOrgID > 11)
                    orgid = multisiloOrgID;
                //FB 2274 Ends

                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);  //Organization Module Fixes

                //FB 2594 Starts
                int PublicEP = 0;
                node = xd.SelectSingleNode("//EndpointDetails/PublicEndpoint"); 
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out PublicEP);
                //FB 2594 Ends
                criterionList.Add(Expression.Or(Expression.Eq("isDefault", 1), Expression.Eq("IsP2PDefault", 1))); //ZD 100815_M
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("orgId", organizationID));//Organization Module Fixes
                criterionList.Add(Expression.Eq("Extendpoint", 0));//FB 2426
                if(PublicEP ==0)
                    criterionList.Add(Expression.Eq("PublicEndPoint", 0));//FB 2594
                List<vrmEndPoint> epList = m_IeptDao.GetByCriteria(criterionList);

                epList = ((epList.Where(x => epList.Count(z => z.endpointid == x.endpointid) > 1).ToList()).Where(x => x.ProfileType == 1).ToList()).Union(epList.Where(x => epList.Count(z => z.endpointid == x.endpointid) == 1).ToList()).ToList();//ZD 100815_M

                //FB 2594 Starts
                int endpointcnt = 0,PubEPCNT =0;
                string totalEP = "SELECT count(*) FROM myVRM.DataLayer.vrmEndPoint vo WHERE vo.deleted=0 and vo.isDefault=1 and vo.Extendpoint=0 and vo.PublicEndPoint = 0 and vo.orgId='" + organizationID.ToString() + "'";
                string totalPubEP = "SELECT count(*) FROM myVRM.DataLayer.vrmEndPoint vo WHERE vo.deleted=0 and vo.Extendpoint=0 and vo.PublicEndPoint = 1 and vo.isDefault=1  and vo.orgId='" + organizationID.ToString() + "'";
                EPCnt = m_IeptDao.execQuery(totalEP);
                PublicEPCnt = m_IeptDao.execQuery(totalPubEP);


                if (EPCnt != null)
                {
                    if (EPCnt.Count > 0)
                    {
                        if (EPCnt[0] != null)
                        {
                            if (EPCnt[0].ToString() != "")
                                Int32.TryParse(EPCnt[0].ToString(), out endpointcnt);
                        }
                    }
                }
                if (PublicEPCnt != null)
                {
                    if (PublicEPCnt.Count > 0)
                    {
                        if (PublicEPCnt[0] != null)
                        {
                            if (PublicEPCnt[0].ToString() != "")
                                Int32.TryParse(PublicEPCnt[0].ToString(), out PubEPCNT);
                        }
                    }
                }
                //FB 2594 Ends
                endpointcnt = epList.Count; //ZD 101527
                int MaxRecords = 0;
                //FB 1820- Starts
                searchOutXml.Append("<EndpointDetails>");
                int idx = 0;

                foreach (vrmEndPoint ep in epList)
                {
                    //if (idx == 0)
                    //{                   

                    searchOutXml.Append("<Endpoint>");
                    searchOutXml.Append("<EndpointID>" + ep.endpointid.ToString() + "</EndpointID>");

                    searchOutXml.Append("<EndpointName>" + ep.name + "</EndpointName>");

                    String roomnames = " ";                  
                    searchOutXml.Append("<RoomName>" + roomnames + "</RoomName>");

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("deleted", 0));
                    criterionList.Add(Expression.Eq("endpointid", ep.endpointid));//Organization Module Fixes

                    //FB 2361
                    List<vrmEndPoint> epList1 = m_IeptDao.GetByCriteria(criterionList);
                    searchOutXml.Append("<ProfileCount>" + epList1.Count + "</ProfileCount>"); 
                    
                    searchOutXml.Append("<DefaultProfileID>" +
                                             ep.profileId.ToString() + "</DefaultProfileID>");
                    searchOutXml.Append("<TotalRecords>" + endpointcnt + "</TotalRecords>");
                    searchOutXml.Append("<TotalPublicEPRecords>" + PubEPCNT + "</TotalPublicEPRecords>"); //FB 2594
                    //obj.outXml += "<Profiles>";
                    //    idx++;
                    //}
                    //obj.outXml += "<Profile>";                     
                    searchOutXml.Append("<ProfileID>" + ep.profileId.ToString() + "</ProfileID>");
                    searchOutXml.Append("<ProfileName>" + ep.profileName + "</ProfileName>");
                    searchOutXml.Append("<ProfileType>" + ep.ProfileType + "</ProfileType>");//ZD 100815
                    searchOutXml.Append("<IsP2PDefault>" + ep.IsP2PDefault + "</IsP2PDefault>");//ZD 100815
                    searchOutXml.Append("<IsTestEquipment>" + ep.IsTestEquipment + "</IsTestEquipment>");//ZD 100815
                    searchOutXml.Append("<EncryptionPreferred>" +
                                                    ep.encrypted.ToString() + "</EncryptionPreferred>");
                    searchOutXml.Append("<AddressType>" + ep.addresstype.ToString() + "</AddressType>");
                    if (ep.password != null)
                        searchOutXml.Append("<Password>" + ep.password + "</Password> ");
                    else
                        searchOutXml.Append("<Password></Password> ");

                    searchOutXml.Append("<Address>" + ep.address + "</Address>");
                    searchOutXml.Append("<URL>" + ep.endptURL + "</URL>");
                    searchOutXml.Append("<IsOutside>" + ep.outsidenetwork.ToString() + "</IsOutside>");

                    if (ep.GateKeeeperAddress != null)//ZD 100132
                        searchOutXml.Append("<GateKeeeperAddress>" + ep.GateKeeeperAddress + "</GateKeeeperAddress>");
                    else
                        searchOutXml.Append("<GateKeeeperAddress></GateKeeeperAddress>");

                    
                    searchOutXml.Append("<VideoEquipment>" +
                                                    ep.videoequipmentid.ToString() + "</VideoEquipment> ");

                    searchOutXml.Append("<Manufacturer>" + ep.ManufacturerID.ToString() + "</Manufacturer>");//ZD 100736
                    
                    searchOutXml.Append("<LineRate>" + ep.linerateid.ToString() + "</LineRate>");
                    searchOutXml.Append("<Bridge>" + ep.bridgeid.ToString() + "</Bridge>");
                    searchOutXml.Append("<BridgeName></BridgeName>");
                    searchOutXml.Append("<ConnectionType>" + ep.connectiontype.ToString() + "</ConnectionType>");
                    searchOutXml.Append("<DefaultProtocol>" + ep.protocol.ToString() + "</DefaultProtocol>");
                    searchOutXml.Append("<MCUAddress>" + ep.MCUAddress + "</MCUAddress>");
                    searchOutXml.Append("<MCUAddressType>" + ep.MCUAddressType.ToString() + "</MCUAddressType>");
                    //Code Added For FB1422 -Start
                    searchOutXml.Append("<TelnetAPI>" + ep.TelnetAPI.ToString() + "</TelnetAPI>");
                    searchOutXml.Append("<SSHSupport>" + ep.SSHSupport.ToString() + "</SSHSupport>");//ZD 101363                    
                    searchOutXml.Append("<RearSecCameraAddress>" + ep.RearSecCameraAddress + "</RearSecCameraAddress>");//FB 2400
                    //FB 2400 start
                    searchOutXml.Append("<MultiCodec>");
                    if (ep.MultiCodecAddress != null)
                    {
                        String[] multiCodec = ep.MultiCodecAddress.Split('�');
                        for (int i = 0; i < multiCodec.Length; i++)
                        {
                            if (multiCodec[i].Trim() != "")
                                searchOutXml.Append("<Address>" + multiCodec[i].Trim() + "</Address>");

                        }
                    }
                    searchOutXml.Append("</MultiCodec>");
                    searchOutXml.Append("<isTelePresence>" + ep.isTelePresence + "</isTelePresence>");
                    //FB 2400 end
                    //Code Added For FB1422 -End   
                    //obj.outXml += "</Profile>";
                    searchOutXml.Append("<Extendpoint>" + ep.Extendpoint.ToString() + "</Extendpoint>");//FB 2426
                    searchOutXml.Append("<Secured>" + ep.Secured.ToString() + "</Secured>");//FB 2595
                    searchOutXml.Append("<NetworkURL>" + ep.NetworkURL + "</NetworkURL>");//FB 2595
                    searchOutXml.Append("<Securedport>" + ep.Secureport + "</Securedport>");//FB 2595
                    searchOutXml.Append("<OrgId>" + ep.orgId + "</OrgId>"); //FB 2593
                    searchOutXml.Append("<LCR>" + ep.LCR + "</LCR>");//ZD 100040 
                    searchOutXml.Append("<EptResolution>" + ep.LCR + "</EptResolution>"); //ZD 100040 
                    searchOutXml.Append("</Endpoint>");
                }

                searchOutXml.Append("</EndpointDetails>");
                obj.outXml = searchOutXml.ToString();
                //FB 1820 - End
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                //obj.outXml = myVRMException.toXml(e.Message);//FB 1881
                obj.outXml = ""; //FB 1881
                bRet = false;
            }

            return bRet;

        }
        #endregion

        //Method added for Endpoint Search    -- End

        //FB 1938
        #region GetCompleteMCUusageReport

        public bool GetCompleteMCUusageReport(ref vrmDataObject obj)
        {
            bool bRet = true;
            String audioOnly = "N/A";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetCompleteMCUusageReport/BridgeID");
                string BridgeID = node.InnerXml.Trim();
                DateTime startDate;
                node = xd.SelectSingleNode("//GetCompleteMCUusageReport/StartDate");
                startDate = DateTime.Parse(node.InnerXml.Trim());
                node = xd.SelectSingleNode("//GetCompleteMCUusageReport/timezone");
                Int32 tzone = Int32.Parse(node.InnerXml.Trim());

                vrmMCUResources vMCU = new vrmMCUResources(Int32.Parse(BridgeID));
                vrmMCU MCU = m_ImcuDao.GetById(Int32.Parse(BridgeID));

                vMCU.bridgeType = MCU.MCUType.bridgeInterfaceId;

                List<vrmMCUResources> vMCUList = new List<vrmMCUResources>();
                loadVRMResourceData(MCU, ref vMCU);
                vMCUList.Add(vMCU);

                string status = String.Format("{0,3:D},{1,3:D},{2,3:D},{3,3:D}",
                    vrmConfStatus.Scheduled,
                    vrmConfStatus.Pending,
                    vrmConfStatus.Ongoing,
                    vrmConfStatus.OnMCU,
                    vrmConfStatus.Completed);

                string hql;
                int Duration = 0;
                // select all conferences that fall into this date time range (with correct status and are not rooms or templates                
                hql = "SELECT c.confid, c.instanceid, c.externalname, c.confdate, c.duration, c.confnumname ";
                hql += " FROM myVRM.DataLayer.vrmConference c ";
                hql += " where (dateadd(second, 45,'" + startDate.ToString("d") + " " + startDate.ToString("t") + "')) between dbo.changeTime(" + tzone + ",confdate) ";
                hql += " and (dateadd(minute, duration,dbo.changeTime(" + tzone + ",confdate))) ";
                hql += "AND c.deleted = 0 AND c.status IN( " + status + ")";
                hql += "AND c.conftype != " + vrmConfType.RooomOnly;
                hql += "AND c.conftype != " + vrmConfType.Template;

                IList conferences = m_vrmConfDAO.execQuery(hql);
                // step thru each conference and count the ports used. 
                List<ICriterion> criterionList;
                obj.outXml = "<UsageReport>";

                foreach (object[] objConf in conferences)
                {
                    int sConfid = Int32.Parse(objConf[0].ToString());
                    int sInstcanceid = Int32.Parse(objConf[1].ToString());
                    String sExternalName = objConf[2].ToString();
                    DateTime sStartDate = DateTime.Parse(objConf[3].ToString());

                    sStartDate = sStartDate.AddSeconds(-45);
                    //timeZone.changeToGMTTime(sysSettings.TimeZone, ref serverTime);
                    timeZone.userPreferedTime(tzone, ref sStartDate);

                    DateTime sEndDate = startDate.AddMinutes(Int32.Parse(objConf[4].ToString()));
                    String sconfnumname = objConf[5].ToString();

                    vrmMCUResources vMCUs = new vrmMCUResources(Int32.Parse(BridgeID));
                    vMCUs.bridgeType = MCU.MCUType.bridgeInterfaceId;
                    loadVRMResourceData(MCU, ref vMCUs);

                    vrmConfAdvAvParams advAVParams = new vrmConfAdvAvParams();
                    vrmConference baseConf = m_vrmConfDAO.GetByConfId(sConfid, sInstcanceid);
                    vrmConfAdvAvParams AvParams = baseConf.ConfAdvAvParams;
                    int isSwitched = 0;
                    int isEncryption = 0;

                    if (AvParams.videoMode == 1)
                        isSwitched = 1;
                    if (AvParams.encryption == 1)
                        isEncryption = 1;

                    // first lookup confrooms
                    // now the users (only external)
                    // FB 4460 use the vMCU (virtual bridge alorithm for Polycomm mcu's
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", sConfid));
                    criterionList.Add(Expression.Eq("instanceid", sInstcanceid));
                    criterionList.Add(Expression.Eq("bridgeid", vMCUs.bridgeId));
                    List<vrmConfRoom> room = m_IconfRoom.GetByCriteria(criterionList);

                    foreach (vrmConfRoom rm in room)
                    {
                        bool notBypass = true;

                        // exclude current conference
                        if (notBypass)
                        {
                            switch (vMCU.bridgeType)
                            {
                                //FB 1937 & 2008
                                //case MCUType.POLYCOM:
                                //    vMCUResourceLoad(rm.addressType, rm.audioorvideo, isSwitched,
                                //           isEncryption, vMCU);
                                //    break;
                                case MCUType.MSE8000://FB 1937 & 2008
                                case MCUType.CODIAN:
                                    if (rm.audioorvideo == 1)
                                    {//FB 1744
                                        vMCU.AUDIO.used++;
                                        vMCUs.AUDIO.used++;
                                    }
                                    else
                                    {
                                        vMCU.VIDEO.used++;
                                        vMCUs.VIDEO.used++;
                                        //vMCU.AUDIO.used++; //FB 1937
                                    }
                                    break;
                                default:
                                    //if (vMCU.AUDIO.available > 0) //FB 1744 FB 1937
                                    //    vMCU.AUDIO.used++;
                                    //else
                                    vMCU.VIDEO.used++;
                                    vMCUs.VIDEO.used++;
                                    vMCU.AUDIO.used = -1000;
                                    vMCUs.AUDIO.used = -1000;
                                    break;
                            }
                        }
                    }

                    // only external users (as they are using port)
                    criterionList.Add(Expression.Eq("invitee", vrmConfUserType.External));
                    criterionList.Add(Expression.Not(Expression.Eq("status", vrmConfUserStatus.Rejectetd)));
                    List<vrmConfUser> user = m_IconfUser.GetByCriteria(criterionList);
                    foreach (vrmConfUser us in user)
                    {
                        // if we are checking mcu resource back out the resources from the conference bridge.
                        // if we are settup up a conference back out the requested resources as they 
                        //     (irespecitve of bridge) will be added in later. 
                        bool notBypass = true;

                        if (notBypass)
                        {
                            switch (vMCU.bridgeType)
                            {
                                //FB 1937 & 2008
                                //case MCUType.POLYCOM:
                                //    vMCUResourceLoad(rm.addressType, rm.audioorvideo, isSwitched,
                                //           isEncryption, vMCU);
                                //    break;
                                case MCUType.MSE8000://FB 1937 & FB 2008
                                case MCUType.CODIAN:
                                    if (us.audioOrVideo == 1)
                                    {//FB 1744
                                        vMCU.AUDIO.used++;
                                        vMCUs.AUDIO.used++;
                                    }
                                    else
                                    {
                                        vMCU.VIDEO.used++;
                                        vMCUs.VIDEO.used++;
                                        //vMCU.AUDIO.used++; //FB 1937
                                    }
                                    break;
                                default:
                                    //if (vMCU.AUDIO.available > 0) //FB 1744 FB 1937
                                    //    vMCU.AUDIO.used++;
                                    //else
                                    vMCU.VIDEO.used++;
                                    vMCUs.VIDEO.used++;
                                    vMCU.AUDIO.used = -1000;
                                    vMCUs.AUDIO.used = -1000;
                                    break;
                            }
                        }
                    }

                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("confid", sConfid));
                    criterionList.Add(Expression.Eq("instanceid", sInstcanceid));
                    criterionList.Add(Expression.Eq("bridgeId", vMCUs.bridgeId));
                    List<vrmConfCascade> cascade = m_IconfCascade.GetByCriteria(criterionList);

                    foreach (vrmConfCascade csde in cascade)
                    {
                        bool notBypass = true;

                        // exclude current conference
                        if (notBypass)
                        {
                            switch (vMCU.bridgeType)
                            {
                                //FB 1937 & 2008
                                //case MCUType.POLYCOM:
                                //    vMCUResourceLoad(rm.addressType, rm.audioorvideo, isSwitched,
                                //           isEncryption, vMCU);
                                //    break;
                                case MCUType.MSE8000://FB 1937 & 2008
                                case MCUType.CODIAN:
                                    if (csde.audioOrVideo == 1)
                                    {//FB 1744
                                        vMCU.AUDIO.used++;
                                        vMCUs.AUDIO.used++;
                                    }
                                    else
                                    {
                                        vMCU.VIDEO.used++;
                                        vMCUs.VIDEO.used++;
                                        //vMCU.AUDIO.used++; //FB 1937
                                    }
                                    break;
                                default:
                                    //if (vMCU.AUDIO.available > 0) //FB 1744 FB 1937
                                    //    vMCU.AUDIO.used++;
                                    //else
                                    vMCU.VIDEO.used++;
                                    vMCUs.VIDEO.used++;
                                    vMCU.AUDIO.used = -1000;
                                    vMCUs.AUDIO.used = -1000;
                                    break;
                            }
                        }
                    }

                    if (vMCUs.VIDEO.used > 0 || vMCUs.AUDIO.used > 0)
                    {
                        obj.outXml += "<UsageDetails>";
                        obj.outXml += "<ConfName>" + sExternalName + "</ConfName>";
                        obj.outXml += "<ConfID>" + sconfnumname + "</ConfID>";
                        obj.outXml += "<StartDate>" + sStartDate.ToString() + "</StartDate>";
                        obj.outXml += "<EndDate>" + sEndDate.ToString() + "</EndDate>";
                        obj.outXml += "<AudioVideo>" + vMCUs.VIDEO.used + "</AudioVideo>";
                        if (vMCUs.AUDIO.used <= -1000)
                            obj.outXml += "<AudioOnly>N/A</AudioOnly>";
                        else
                            obj.outXml += "<AudioOnly>" + vMCUs.AUDIO.used + "</AudioOnly>";
                        obj.outXml += "</UsageDetails>";
                    }
                }
                obj.outXml += "<TotalVideoUsed>" + vMCUList[0].VIDEO.used + "</TotalVideoUsed>";
                obj.outXml += "<TotalVideoAvailable>" + vMCUList[0].VIDEO.available + "</TotalVideoAvailable>";
                if (vMCUList[0].bridgeType == MCUType.MSE8000 || vMCUList[0].bridgeType == MCUType.CODIAN)
                {
                    obj.outXml += "<TotalAudioUsed>" + vMCUList[0].AUDIO.used + "</TotalAudioUsed>";
                    obj.outXml += "<TotalAudioAvailable>" + vMCUList[0].AUDIO.available + "</TotalAudioAvailable>";
                }
                else
                {
                    obj.outXml += "<TotalAudioUsed>N/A</TotalAudioUsed>";
                    obj.outXml += "<TotalAudioAvailable>N/A</TotalAudioAvailable>";
                }




                obj.outXml += "</UsageReport>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;
        }
        #endregion

        //FB 2027 - Starts
        #region DeleteBridge
        /// <summary>
        /// DeleteBridge (COM to .Net conversion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool DeleteBridge(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionlist = new List<ICriterion>();
            List<ICriterion> criterionlist2 = new List<ICriterion>();
            StringBuilder OutXml = new StringBuilder();
            myVRMException myVRMEx = null;
            int userID = 0;
            int bridgeID = 0;
            List<ICriterion> criterionList = null; //ZD 100298
            List<vrmMCUProfiles> MCUProf = null; //ZD 100298
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userID);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/bridge/bridgeID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out bridgeID);
                else
                {
                    myVRMEx = new myVRMException(200);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if ((userID <= 0) || (bridgeID <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                //ZD 100040 starts
                int recordCount = 0;
                IList recCnt = m_IMCUGrpAssignmentDao.execQuery("select count(*) from myVRM.DataLayer.vrmMCUGrpAssignment c where MCUID =" + bridgeID);
                if (recCnt != null)
                {
                    if (recCnt.Count > 0)
                    {
                        if (recCnt[0] != null)
                        {
                            if (recCnt[0].ToString() != "")
                                int.TryParse(recCnt[0].ToString(), out recordCount);
                        }
                    }
                }

                if (recordCount > 0)
                {
                    myVRMException ex = new myVRMException(763);
                    obj.outXml = ex.FetchErrorMsg();
                    return false;
                }
                //ZD 100040 ends

                criterionlist.Add(Expression.Eq("BridgeID", bridgeID));
                List<vrmUser> iUsers = m_vrmUserDAO.GetByCriteria(criterionlist);
               
                criterionlist2.Add(Expression.Eq("bridgeid", bridgeID));
                criterionlist2.Add(Expression.Eq("deleted", 0));
                List<vrmEndPoint> iEndPt = m_IeptDao.GetByCriteria(criterionlist2);
                
                if ((iUsers.Count == 0) && (iEndPt.Count == 0))
                {
                    vrmMCU mcu = null;
                    mcu = m_ImcuDao.GetById(bridgeID, true);
                    mcu.deleted = 1;
                    m_ImcuDao.SaveOrUpdate(mcu);
                    //ZD 100298 Starts
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("MCUId", bridgeID));
                    MCUProf = m_IMcuProfilesDAO.GetByCriteria(criterionList);
                    for (int i = 0; i < MCUProf.Count; i++)
                        m_IMcuProfilesDAO.Delete(MCUProf[i]);
                    //ZD 100298 Ends
                }
                else
                {
                    String message = "";
                    if (iUsers.Count > 0)
                    {
                        myVRMEx = new myVRMException(495);
                        message = myVRMEx.Message.Replace("{0}", iUsers.Count.ToString());
                    }

                    if (iEndPt.Count > 0)
                    {
                        myVRMEx = new myVRMException(496);
                        message += myVRMEx.Message.Replace("{0}", iEndPt.Count.ToString());
                    }

                    OutXml.Append("<error>");
                    OutXml.Append("<errorCode>495</errorCode>");
                    OutXml.Append("<message>" + message + "</message>");
                    OutXml.Append("<Description>" + message + "</Description>");
                    OutXml.Append("<level>E</level>");
                    OutXml.Append("</error>");
                    obj.outXml = OutXml.ToString();
                    return false;
                }
                obj.outXml = "<success>1</success>";
                return true;
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetEndpoint
        /// <summary>
        /// GetEndpoint (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetEndpoint(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder OutXml = new StringBuilder();
            myVRMException myVRMEx = null;
            int userid = 0, epid = 0, bridgeCount = 0;
            String orgid = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/endpointID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out epid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if ((userid <= 0) || (epid <= 0))
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);

                OutXml.Append("<getEndpoint>");

                OutXml.Append(FetchAddressTypeList());
                GetVideoEquipment(ref obj);
                GetManufacturer(ref obj);//ZD 100736
                GetLineRate(ref obj);
                OutXml.Append(obj.outXml);
                OutXml.Append(FetchBridgeList(ref bridgeCount));

                criterionList.Add(Expression.Eq("endpointid", epid));
                List<vrmEndPoint> epList = m_IeptDao.GetByCriteria(criterionList);
                vrmEndPoint ep = new vrmEndPoint();
                if (epList.Count > 0)
                    ep = epList[0];

                OutXml.Append("<endpoint>");
                OutXml.Append("<ID>" + ep.endpointid + "</ID>");
                OutXml.Append("<name>" + ep.name + "</name>");
                OutXml.Append("<ProfileType>" + ep.ProfileType + "</ProfileType>");//ZD 100815
                OutXml.Append("<IsP2PDefault>" + ep.IsP2PDefault + "</IsP2PDefault>");//ZD 100815
                OutXml.Append("<IsTestEquipment>" + ep.IsTestEquipment + "</IsTestEquipment>");//ZD 100815
                OutXml.Append("<password>" + ep.password + "</password>");
                OutXml.Append("<addressType>" + ep.addresstype + "</addressType>");
                OutXml.Append("<address>" + ep.address + "</address>");
                OutXml.Append("<isOutside>" + ep.outsidenetwork + "</isOutside>");
                OutXml.Append("<GateKeeeperAddress>" + ep.GateKeeeperAddress + "</GateKeeeperAddress>");//ZD 100132
                OutXml.Append("<connectionType>" + ep.connectiontype + "</connectionType>");
                OutXml.Append("<videoEquipment>" + ep.videoequipmentid + "</videoEquipment>");
                OutXml.Append("<lineRate>" + ep.linerateid + "</lineRate>");
                OutXml.Append("<bridge>" + ep.bridgeid + "</bridge>");
                OutXml.Append("<URL>" + ep.endptURL + "</URL>");
                OutXml.Append("<TelnetAPI>" + ep.TelnetAPI + "</TelnetAPI>");
                OutXml.Append("<SSHSupport>" + ep.SSHSupport + "</SSHSupport>");//ZD 101363
                OutXml.Append("<ExchangeID>" + ep.ExchangeID + "</ExchangeID>");
                //FB 2400 start
                OutXml.Append("<MultiCodec>");
                if (ep.MultiCodecAddress != null)
                {
                    String[] multiCodec = ep.MultiCodecAddress.Split('�');
                    for (int i = 0; i < multiCodec.Length; i++)
                    {
                        if (multiCodec[i].Trim() != "")
                            OutXml.Append("<Address>" + multiCodec[i].Trim() + "</Address>");

                    }
                }
                OutXml.Append("</MultiCodec>");
                OutXml.Append("<isTelePresence>" + ep.isTelePresence + "</isTelePresence>");
                //FB 2400 end
                OutXml.Append("</endpoint>");
                OutXml.Append("</getEndpoint>");
                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SetBridgeList
        /// <summary>
        /// SetBridgeList (COM to .Net conversion)
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetBridgeList(ref vrmDataObject obj)
        {
            bool bRet = true;
            string outXml = string.Empty;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                XmlElement root = xd.DocumentElement;
                XmlNodeList blist = root.SelectNodes(@"/login/bridgeOrder/bridge");
                for (int i = 0; i < blist.Count; i++)
                {
                    int order = 0, bridgeID = 0;
                    node = blist[i].SelectSingleNode("order");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out order);
                    node = blist[i].SelectSingleNode("bridgeID");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out bridgeID);
                    vrmMCU mcu = null;
                    mcu = m_ImcuDao.GetById(bridgeID, true);
                    mcu.ChainPosition = order;
                    m_ImcuDao.SaveOrUpdate(mcu);
                }
                outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        //FB 2027(GetOldRoom)
        #region OldFetchBridgeList
        /// <summary>
        /// OldFetchBridgeList (COM to .Net conversion) 
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool OldFetchBridgeList(int orgid, ref StringBuilder outXml)
        {
            outXml = new StringBuilder();
            List<ICriterion> critermcu = new List<ICriterion>();
            try
            {
                outXml.Append("<bridges>");
                critermcu.Add(Expression.Eq("VirtualBridge", 0));
                critermcu.Add(Expression.Eq("deleted", 0));
                critermcu.Add(Expression.Eq("orgId", orgid));
                List<vrmMCU> mculist = m_ImcuDao.GetByCriteria(critermcu);
                for (int i = 0; i < mculist.Count; i++)
                {
                    outXml.Append("<bridge>");
                    outXml.Append("<ID>" + mculist[i].BridgeID.ToString() + "</ID>");
                    outXml.Append("<name>" + mculist[i].BridgeName + "</name>");
                    outXml.Append("</bridge>");
                }
                outXml.Append("</bridges>");
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("OldFetchBridgeList :" + ex.Message);
                throw ex;
            }
        }
        #endregion

        //FB 2027(GetOldRoom)
        #region FetchEndpointList
        /// <summary>
        /// FetchEndpointList (COM to .Net conversion)
        /// </summary>
        /// <param name="?"></param>
        /// <returns></returns>
        public bool FetchEndpointList(int orgid, ref StringBuilder outXml)
        {
            outXml = new StringBuilder();
            List<ICriterion> criterept = new List<ICriterion>();
            try
            {
                outXml.Append("<endpoints>");
                m_IeptDao.addOrderBy(Order.Asc("name"));
                criterept.Add(Expression.Eq("deleted", 0));
                criterept.Add(Expression.Or(Expression.Eq("isDefault", 1), Expression.Eq("IsP2PDefault", 1))); //ZD 100815_M
                criterept.Add(Expression.Eq("orgId", orgid));
                List<vrmEndPoint> EndPointList = m_IeptDao.GetByCriteria(criterept);

                EndPointList = ((EndPointList.Where(x => EndPointList.Count(z => z.endpointid == x.endpointid) > 1).ToList()).Where(x => x.ProfileType == 1).ToList()).Union(EndPointList.Where(x => EndPointList.Count(z => z.endpointid == x.endpointid) == 1).ToList()).ToList();//ZD 100815_M

                m_IeptDao.clearOrderBy();
                for (int a = 0; a < EndPointList.Count; a++)
                {
                    outXml.Append("<endpoint>");
                    outXml.Append("<ID>" + EndPointList[a].endpointid.ToString() + "</ID>");
                    outXml.Append("<name>" + EndPointList[a].name + "</name>");
                    outXml.Append("<protocol>" + EndPointList[a].protocol.ToString() + "</protocol>");
                    outXml.Append("<connectionType>" + EndPointList[a].connectiontype.ToString() + "</connectionType>");
                    outXml.Append("<addressTypeID>" + EndPointList[a].addresstype.ToString() + "</addressTypeID>");
                    outXml.Append("<address>" + EndPointList[a].address + "</address>");
                    outXml.Append("</endpoint>");
                }
                outXml.Append("</endpoints>");
            }
            catch (Exception ex)
            {
                m_log.Error("FetchEndpointList :" + ex.Message);
                throw ex;
            }
            return true;
        }
        #endregion
        //FB 2027 - End
        
        //FB 2426 Start

        #region SetGuesttoNormalEndpoint
        /// <summary>
        /// SetGuesttoNormalEndpoint
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetGuesttoNormalEndpoint(int endpointid, ref vrmDataObject obj)
        {
            vrmEndPoint ept = null;
            List<ICriterion> Licencechk = new List<ICriterion>();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> criterionLst = new List<ICriterion>();
            try
            {
                ept =  m_IeptDao.GetByEptId(endpointid);
                Licencechk.Add(Expression.Eq("deleted", 0));
                Licencechk.Add(Expression.Or(Expression.Eq("isDefault", 1), Expression.Eq("IsP2PDefault", 1))); //ZD 100815_M
                Licencechk.Add(Expression.Eq("orgId", ept.orgId));
                Licencechk.Add(Expression.Eq("Extendpoint", 0));
                List<vrmEndPoint> checkEptCount = m_IeptDao.GetByCriteria(Licencechk);

                checkEptCount = ((checkEptCount.Where(x => checkEptCount.Count(z => z.endpointid == x.endpointid) > 1).ToList()).Where(x => x.ProfileType == 1).ToList()).Union(checkEptCount.Where(x => checkEptCount.Count(z => z.endpointid == x.endpointid) == 1).ToList()).ToList();//ZD 100815_M

                OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(ept.orgId);

                if (checkEptCount.Count >= orgdt.MaxEndpoints)
                {
                    myVRMException ex = new myVRMException(458);
                    m_log.Error("Error EndPoint Limit exceeeded: ", ex);
                    obj.outXml = ex.FetchErrorMsg();
                    return false;
                }

                criterionList.Add(Expression.Eq("endpointid", endpointid));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("Extendpoint", 1));
                criterionList.Add(Expression.Eq("orgId", ept.orgId));
                List<vrmEndPoint> checkendpoint = m_IeptDao.GetByCriteria(criterionList, true);
                if (checkendpoint.Count > 0)
                {
                    criterionLst = new List<ICriterion>();
                    criterionLst.Add(Expression.Eq("name", checkendpoint[0].name));
                    criterionLst.Add(Expression.Not(Expression.Eq("endpointid", endpointid)));
                    criterionLst.Add(Expression.Eq("Extendpoint", 0));
                    List<vrmEndPoint> EptChkList = m_IeptDao.GetByCriteria(criterionLst);
                    if (EptChkList.Count > 0)
                    {
                        myVRMException myVRMEx = new myVRMException(434);
                        obj.outXml = myVRMEx.FetchErrorMsg();
                        return false;
                    }
                }
                //ZD 100619 Starts
                for (int i = 0; i < checkendpoint.Count; i++)
                {
                    checkendpoint[i].Extendpoint = 0;
                    checkendpoint[i].ManufacturerID = 3;
                    m_IeptDao.Update(checkendpoint[i]);
                }
                //ZD 100619 Ends

                //ZD 100664 Starts
                List<vrmEndPoint> eptList = null;
                criterionList.Add(Expression.Eq("endpointid", endpointid));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("orgId", ept.orgId));
                eptList = m_IeptDao.GetByCriteria(criterionList, true);

                for (int i = 0; i < eptList.Count; i++)
                {
                    eptList[i].deleted = 1;
                    eptList[i].ManufacturerID = 3;
                    eptList[i].Lastmodifieddate = DateTime.UtcNow;//ZD 101026
                }
                m_IeptDao.SaveOrUpdateList(eptList);

                SetAuditEndpoint(false, false, eptList);
                //ZD 100664 End

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        #region DeleteGuestEndpoint
        /// <summary>
        /// DeleteGuestEndpoint
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteGuestEndpoint(int endpointid)
        {
            vrmEndPoint ept = null;
            try
            {
                ept = m_IeptDao.GetByEptId(endpointid);
                ept.deleted = 1;
                m_IeptDao.Update(ept);
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("sytemException in GetRoomProfile", ex);
                return false;
            }
        }
        #endregion

        //FB 2426 End

        //FB 2486 - Start
        #region GetAllMessage

        public bool GetAllMessage(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                StringBuilder outputXML =new StringBuilder();

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("orgId", organizationID));
                m_msgDAO.addOrderBy(Order.Asc("msgId"));
                m_msgDAO.addOrderBy(Order.Asc("ID"));
                m_msgDAO.addOrderBy(Order.Asc("Languageid"));
                List<vrmMessage> msgList = m_msgDAO.GetByCriteria(criterionList);

                outputXML.Append("<GetAllMessage><MessageList>");

                List<vrmLanguage> lang = m_ILanguageDAO.GetAll();
                Hashtable langTable = new Hashtable();
                for (int l = 0; l < lang.Count; l++)
                    langTable.Add(lang[l].Id, lang[l].Name);

                for (int i = 0; i < msgList.Count; i++)
                {
                    outputXML.Append("<Message>");
                    outputXML.Append("<ID>" + msgList[i].ID.ToString() + "</ID>");
                    outputXML.Append("<TxtMsg>" + msgList[i].Message.ToString() + "</TxtMsg>");
                    outputXML.Append("<LangID>" + msgList[i].Languageid + "</LangID>");
                    outputXML.Append("<LangName>" + langTable[msgList[i].Languageid].ToString() + "</LangName>");

                    //vrmLanguage language = m_ILanguageDAO.GetLanguageById(msgList[i].LangId);
                    //if(language != null)
                    //    outputXML.Append("<LangName>" + language.Name + "</LangName>");

                    outputXML.Append("<Type>" + msgList[i].Type.ToString() + "</Type>");
                    outputXML.Append("<MsgID>" + msgList[i].msgId.ToString() + "</MsgID>");
                    outputXML.Append("</Message>");
                }
                outputXML.Append("</MessageList></GetAllMessage>");
                obj.outXml = outputXML.ToString();

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("GetAllMessage", e);
                return false;
            }
        }

        #endregion

        #region SetMessage

        public bool SetMessage(ref vrmDataObject obj)
        {
            vrmMessage vrmMsg = new vrmMessage();
            List<vrmMessage> MsgList = new List<vrmMessage>();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                XmlNodeList nodelist;
                int MaxId = 0;

                node = xd.SelectSingleNode("//login/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                organizationID = defaultOrgId;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                Int32 msgID = 0;
                Int32 uID = 0;

                String msg = "";
                int Langid =1;
                nodelist = xd.SelectNodes("//login/MessageList/Message");
                if (nodelist != null)
                {
                    MsgList = new List<vrmMessage>();
                    for (int i = 0; i < nodelist.Count; i++)
                    {
                        vrmMsg = null;
                        vrmMsg = new vrmMessage();

                        if (nodelist[i].SelectSingleNode("ID") != null)
                            Int32.TryParse(nodelist[i].SelectSingleNode("ID").InnerText.Trim(), out uID);

                        if (nodelist[i].SelectSingleNode("LangID") != null)
                            Int32.TryParse(nodelist[i].SelectSingleNode("LangID").InnerText.Trim(), out Langid);

                        if (nodelist[i].SelectSingleNode("TxtMsg") != null)
                            msg = nodelist[i].SelectSingleNode("TxtMsg").InnerText.Trim();

                        if (uID == 0)
                        {
                            vrmMsg.Languageid = Langid;
                            vrmMsg.Message = msg;
                            vrmMsg.orgId = organizationID;
                            vrmMsg.Type = 0;
                            if (msgID == 0)
                            {
                                string MsgID = "SELECT max(vm.msgId) FROM myVRM.DataLayer.vrmMessage vm";
                                IList result = m_vrmConfDAO.execQuery(MsgID);

                                if (result[0] != null)
                                    int.TryParse(result[0].ToString(), out MaxId);

                                MaxId = MaxId + 1;
                                vrmMsg.msgId = MaxId;
                            }
                            else
                                vrmMsg.msgId = msgID;

                            MsgList.Add(vrmMsg);
                        }
                        else
                        {
                            vrmMsg = m_msgDAO.GetById(uID);
                            vrmMsg.Message = msg;
                            m_msgDAO.SaveOrUpdate(vrmMsg);
                        }
                    }

                    if(uID == 0)                    
                        m_msgDAO.SaveOrUpdateList(MsgList);
                }
                
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        #region DeleteMessage

        public bool DeleteMessage(ref vrmDataObject obj)
        {
            vrmMessage vrmMsg = new vrmMessage();
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                Int32 msgID = 0;
                node = xd.SelectSingleNode("//login/MsgID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out msgID);

                if (msgID > 0)
                {
                    List<ICriterion> criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("msgId", msgID));
                    List<vrmMessage> msgList = m_msgDAO.GetByCriteria(criterionList);

                    foreach (vrmMessage msg in msgList)
                        m_msgDAO.Delete(msg);
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                throw e;
            }
        }

        #endregion

        //FB 2486 - End

        //FB 2501 - Call Monitoring
        #region SetFavouriteMCU
        /// <summary>
        /// SetFavouriteMCU
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool SetFavouriteMCU(ref vrmDataObject obj)
        {
            bool bRet = true;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            try
            {
                XmlNode node = null;
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                int setFavourite = 0, bridgeId = 0, userid = 0;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    Int32.TryParse(node.InnerXml.Trim(), out userid);
                else
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//login/bridgeId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out bridgeId);
                else
                {
                    myVRMEx = new myVRMException(531);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                
                node = xd.SelectSingleNode("//login/favouriteMCU");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out setFavourite);

                criterionList.Add(Expression.Eq("BridgeID", bridgeId));
                //criterionList.Add(Expression.Eq("orgId", organizationID)); //FB 2646

                List<vrmMCU> McuList = m_ImcuDao.GetByCriteria(criterionList,true);
                for (int i = 0; i < McuList.Count; i++)
                {
                    McuList[i].setFavourite = setFavourite;
                    m_ImcuDao.SaveOrUpdate(McuList[i]);
                }
                obj.outXml = "<success>1</success>";

            }
            catch (Exception ex)
            {
                m_log.Error("sytemException", ex);
                bRet = false;
            }
            return bRet;
        }

        #endregion
        //FB 2501 - Call Monitoring 

        //FB 2591 Start 
        #region
        public bool GetMCUProfiles(ref vrmDataObject obj)
        {
            StringBuilder OutXml = new StringBuilder();
            List<vrmMCUProfiles> MCUProfiles = new List<vrmMCUProfiles>();

            List<ICriterion> criterionList = null;
            List<vrmMCUProfiles> lstProfileName = null;//FB 2947
            int MCUId = 0;
            vrmMCU MCU = null; //FB 2947
            try
            {

                
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetMCUProfiles/MCUId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out MCUId);

                              
                OutXml.Append("<GetMCUProfiles>");

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("MCUId", MCUId));
                if (MCUId > 0) //FB 2947
                {
                    lstProfileName = m_IMcuProfilesDAO.GetByCriteria(criterionList);
                    m_IMcuProfilesDAO.clearOrderBy();
                    MCU = m_ImcuDao.GetById(MCUId); //FB 2947
                    OutXml.Append("<DefaultProfileID>" + MCU.ConfServiceID.ToString() + "</DefaultProfileID>"); //FB 2947
                    if (lstProfileName.Count > 0) 
                    {
                        for (int i = 0; i < lstProfileName.Count; i++)
                        {
                            OutXml.Append("<Profile>");
                            OutXml.Append("<Id>" + lstProfileName[i].ProfileId.ToString() + "</Id>");
                            OutXml.Append("<Name>" + lstProfileName[i].ProfileName + "</Name>");
                            OutXml.Append("</Profile>");
                        }
                    }
                }
                OutXml.Append("</GetMCUProfiles>");
                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
            }
            return true;

        }
        #endregion
        //FB 2591 End 


        //FB 2556 - Starts
        #region GetExtMCUServices
        /// <summary>
        /// GetExtMCUServices
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetExtMCUServices(ref vrmDataObject obj)
        {
            StringBuilder OutXml = new StringBuilder();
            List<vrmExtMCUService> MCUServiceList = new List<vrmExtMCUService>();
            List<ICriterion> criterionList = null;
            int bridgeTypeId = 0;

            try
            {
               
                m_IExtMCUService = m_hardwareDAO.GetExtMCUServiceDao(); 
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetExtMCUService/BridgeTypeId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out bridgeTypeId);

                int BridgeId = 0;
                node = xd.SelectSingleNode("//GetExtMCUService/BridgeId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out BridgeId);

                OutXml.Append("<GetExtMCUServices>");

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("BridgeId", BridgeId));

                MCUServiceList = m_IExtMCUService.GetByCriteria(criterionList);
                m_IMcuProfilesDAO.clearOrderBy();

                if (MCUServiceList.Count > 0)
                {
                    for (int i = 0; i < MCUServiceList.Count; i++)
                    {

                        OutXml.Append("<GetExtMCUService>");
                         OutXml.Append("<ID>" + MCUServiceList[i].MemberId + "</ID>");
                        OutXml.Append("<name>" + MCUServiceList[i].ServiceName + "</name>");
                        OutXml.Append("</GetExtMCUService>");
                    }
                }

                OutXml.Append("</GetExtMCUServices>");
                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
            }
            return true;

        }
        #endregion

        #region GetExtMCUSilo
        /// <summary>
        /// GetExtMCUSilo
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetExtMCUSilo(ref vrmDataObject obj)
        {
            StringBuilder OutXml = new StringBuilder();
            List<vrmExtMCUSilo> MCUSiloList = new List<vrmExtMCUSilo>();
            List<ICriterion> criterionList = null;
            int bridgeTypeId = 0;

            try
            {
                m_IExtMCUSilo = m_hardwareDAO.GetExtMCUSiloDao();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetExtMCUSilo/BridgeTypeId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out bridgeTypeId);

                int BridgeId = 0;
                node = xd.SelectSingleNode("//GetExtMCUSilo/BridgeId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out BridgeId);

                
                OutXml.Append("<GetExtMCUSilos>");

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("BridgeId", BridgeId));

                MCUSiloList = m_IExtMCUSilo.GetByCriteria(criterionList);
                m_IMcuProfilesDAO.clearOrderBy();

                if (MCUSiloList.Count > 0)
                {
                    for (int i = 0; i < MCUSiloList.Count; i++)
                    {
                        OutXml.Append("<GetExtMCUSilo>");
                        OutXml.Append("<ID>" + MCUSiloList[i].MemberId + "</ID>");
                        OutXml.Append("<name>" + MCUSiloList[i].Name + "</name>");
                        OutXml.Append("</GetExtMCUSilo>");
                    }
                }

                OutXml.Append("</GetExtMCUSilos>");
                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
            }
            return true;

        }
        #endregion
        //FB 2556 - End

        //ZD 101026 Starts

        #region SetAuditMCU
        /// <summary>
        /// SetAuditMCU
        /// </summary>
        /// <param name="bridgeId"></param>
        /// <param name="LastModified"></param>
        /// <returns></returns>
        private bool SetAuditMCU(bool isNewBridge, int bridgeId, DateTime LastModifiedDate, int LastModifiedUser, int AuditCardListStatus, int AuditIPServiceStatus, int AuditE164ServiceStatus, int AuditISDNServiceStatus, int AuditMPIServiceStatus, int AuditOrgspecificStatus)
        {
            StringBuilder stmt = new StringBuilder();
            object returnVal = null;
            int bridgeCount = 0;
            string description = "No Changes Made", ModifiedDetails = "";
            try
            {
                if (m_bridgelayer == null)
                    m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                if (isNewBridge)
                    description = "Created";

                //AuditCardListStatus, AuditIPServiceStatus, AuditE164ServiceStatus, AuditISDNServiceStatus, AuditMPIServiceStatus, AuditOrgspecificStatus;

                stmt.Append(" Insert into Audit_Mcu_List_D  ");
                stmt.Append(" ([BridgeID],[BridgeLogin],[BridgePassword],[BridgeAddress],[Status],[Admin],[LastModified],[BridgeName],");
                stmt.Append(" [ChainPosition],[updateInBridge],[NewBridge],[ISDNPhonePrefix],[BridgeTypeId],[SoftwareVer],[UpdateFromBridge],");
                stmt.Append(" [VirtualBridge],[isdnportrate],[isdnlinerate],[isdnmaxcost],[isdnthreshold],[timezone],[syncflag],");
                stmt.Append(" [isdnthresholdalert],[malfunctionalert],[reservedportperc],[responsetime],[responsemessage],");
                stmt.Append(" [deleted],[maxConcurrentAudioCalls],[maxConcurrentVideoCalls],[orgId],[ApiPortNo],[EnableIVR],");
                stmt.Append(" [IVRServiceName],[EnableRecording],[isPublic],[ISDNGateway],[ISDNAudioPrefix],[ISDNVideoPrefix],");
                stmt.Append(" [ConfServiceID],[LPR],[CurrentDateTime],[EnableMessage],[setFavourite],[portsVideoTotal],[portsVideoFree],");
                stmt.Append(" [portsAudioTotal],[portsAudioFree],[BridgeExtNo],[E164Dialing],[H323Dialing],[EnhancedMCU],");
                stmt.Append(" [DMAMonitorMCU],[DMASendMail],[DMADomain],[DMAName],[DMALogin],[DMAPassword],[DMAURL],[DMAPort],");
                stmt.Append(" [Synchronous],[EnableCDR],[DeleteCDRDays],[DialinPrefix],[LoginName],[loginCount],[RPRMEmailaddress],");
                stmt.Append(" [RPRMDomain],[IsMultitenant],[LastDateTimeCDRPoll],[URLAccess],[EnablePollFailure],[PollCount],");
                stmt.Append(" [MultipleAddress],[LastModifiedUser], [LastModifiedDate]) ");
                stmt.Append(" (Select [BridgeID],[BridgeLogin],[BridgePassword],[BridgeAddress],[Status],[Admin],[LastModified],[BridgeName],");
                stmt.Append(" [ChainPosition],[updateInBridge],[NewBridge],[ISDNPhonePrefix],[BridgeTypeId],[SoftwareVer],[UpdateFromBridge],");
                stmt.Append(" [VirtualBridge],[isdnportrate],[isdnlinerate],[isdnmaxcost],[isdnthreshold],[timezone],[syncflag],");
                stmt.Append(" [isdnthresholdalert],[malfunctionalert],[reservedportperc],[responsetime],[responsemessage],");
                stmt.Append(" [deleted],[maxConcurrentAudioCalls],[maxConcurrentVideoCalls],[orgId],[ApiPortNo],[EnableIVR],");
                stmt.Append(" [IVRServiceName],[EnableRecording],[isPublic],[ISDNGateway],[ISDNAudioPrefix],[ISDNVideoPrefix],");
                stmt.Append(" [ConfServiceID],[LPR],[CurrentDateTime],[EnableMessage],[setFavourite],[portsVideoTotal],[portsVideoFree],");
                stmt.Append(" [portsAudioTotal],[portsAudioFree],[BridgeExtNo],[E164Dialing],[H323Dialing],[EnhancedMCU],");
                stmt.Append(" [DMAMonitorMCU],[DMASendMail],[DMADomain],[DMAName],[DMALogin],[DMAPassword],[DMAURL],[DMAPort],");
                stmt.Append(" [Synchronous],[EnableCDR],[DeleteCDRDays],[DialinPrefix],[LoginName],[loginCount],[RPRMEmailaddress],");
                stmt.Append(" [RPRMDomain],[IsMultitenant],[LastDateTimeCDRPoll],[URLAccess],[EnablePollFailure],[PollCount],");
                stmt.Append(" [MultipleAddress],[LastModifiedUser], '" + LastModifiedDate + "' ");
                stmt.Append(" from Mcu_List_D where BridgeID = " + bridgeId + ")");

                m_bridgelayer.OpenConnection();
                m_bridgelayer.ExecuteNonQuery(stmt.ToString());

                //MCU_List_D
                if (!isNewBridge)
                {
                    stmt = new StringBuilder();
                    stmt.Append(" select count(*) from ( ");
                    stmt.Append(" select distinct ");
                    stmt.Append(" [Admin], [ApiPortNo], [BridgeAddress], [BridgeExtNo], [BridgeID], [BridgeLogin], [BridgeName], ");
                    stmt.Append(" [BridgePassword], [BridgeTypeId], [ChainPosition], [ConfServiceID], ");
                    stmt.Append(" [DeleteCDRDays], [deleted], [DialinPrefix], [DMADomain], [DMALogin], [DMAMonitorMCU], ");
                    stmt.Append(" [DMAName], [DMAPassword], [DMAPort], [DMASendMail], [DMAURL], [E164Dialing], [EnableCDR], ");
                    stmt.Append(" [EnableIVR], [EnableMessage], [EnablePollFailure], [EnableRecording], [EnhancedMCU], ");
                    stmt.Append(" [H323Dialing], [ISDNAudioPrefix], [ISDNGateway], [isdnlinerate], [isdnmaxcost], ");
                    stmt.Append(" [ISDNPhonePrefix], [isdnportrate], [isdnthreshold], [isdnthresholdalert], [ISDNVideoPrefix], ");
                    stmt.Append(" [IsMultitenant], [isPublic], [IVRServiceName], ");
                    stmt.Append(" [LoginName], [loginCount], [LPR], [malfunctionalert], [maxConcurrentAudioCalls], [maxConcurrentVideoCalls], ");
                    stmt.Append(" [MultipleAddress], [orgId], [PollCount], [portsAudioFree], [portsAudioTotal], ");
                    stmt.Append(" [portsVideoFree], [portsVideoTotal], [reservedportperc], ");
                    stmt.Append(" [RPRMDomain], [RPRMEmailaddress], [SoftwareVer], [Status], [syncflag],  ");
                    stmt.Append(" [Synchronous], [timezone], [URLAccess], [VirtualBridge] ");
                    stmt.Append(" from ( ");
                    stmt.Append(" select top 2 * from Audit_MCU_List_D where bridgeId = " + bridgeId + "");
                    stmt.Append(" order by Lastmodifieddate desc");
                    stmt.Append(" ) as x ) as y");
                    returnVal = m_bridgelayer.ExecuteScalar(stmt.ToString());
                    if (returnVal != null)
                        int.TryParse(returnVal.ToString(), out bridgeCount);
                    if (bridgeCount > 1)
                        description = "Changes Made";
                }

                //Mcu_Approver_D
                if (!isNewBridge && description == "No Changes Made")
                {
                    bridgeCount = 0;
                    stmt = new StringBuilder();
                    stmt.Append(" select COUNT(*) from  ( ");
                    stmt.Append(" select ApproverId from  Mcu_Approver_D where mcuid = " + bridgeId + "  ");
                    stmt.Append(" and ApproverId not in (select ApproverId from Audit_Mcu_Approver_D where mcuid = " + bridgeId + " and  ");
                    stmt.Append(" LastModifiedDate = (select MAX(Lastmodifieddate) from Audit_Mcu_Approver_D ");
                    stmt.Append(" where mcuid = " + bridgeId + ")) ");
                    stmt.Append(" union  ");
                    stmt.Append(" select ApproverId from  Audit_Mcu_Approver_D where mcuid = " + bridgeId + "  and ");
                    stmt.Append(" LastModifiedDate = (select MAX(Lastmodifieddate) from Audit_Mcu_Approver_D ");
                    stmt.Append(" where mcuid = " + bridgeId + ") ");
                    stmt.Append(" and ApproverId not in (select ApproverId from Mcu_Approver_D where mcuid = " + bridgeId + ") ");
                    stmt.Append(" )  as x  ");
                    returnVal = m_bridgelayer.ExecuteScalar(stmt.ToString());
                    if (returnVal != null)
                        int.TryParse(returnVal.ToString(), out bridgeCount);
                    if (bridgeCount > 0)
                        description = "Changes Made";
                }

                stmt = new StringBuilder();
                stmt.Append(" Insert into [Audit_Mcu_Approver_D] ");
                stmt.Append(" ([mcuid], [Approverid], [LastModifiedDate])");
                stmt.Append(" select [mcuid], [Approverid], '" + LastModifiedDate + "'");
                stmt.Append(" from Mcu_Approver_D where mcuid=" + bridgeId);
                
                stmt.Append(" Insert into [Audit_Mcu_CardList_D] ");
                stmt.Append(" ([BridgeID], [cardTypeId], [maxCalls], [LastModifiedDate])");
                stmt.Append(" select [BridgeID], [cardTypeId], [maxCalls], '" + LastModifiedDate + "'");
                stmt.Append(" from Mcu_CardList_D where bridgeId=" + bridgeId);
                
                stmt.Append(" Insert into [Audit_MCU_E164Services_D] ");
                stmt.Append(" ([BridgeID], [StartRange], [EndRange], [SortID], [LastModifiedDate])");
                stmt.Append(" select [BridgeID], [StartRange], [EndRange], [SortID], '" + LastModifiedDate + "'");
                stmt.Append(" from MCU_E164Services_D where bridgeId=" + bridgeId);
                
                stmt.Append(" Insert into [Audit_Mcu_IPServices_D] ");
                stmt.Append(" ([AddressType], [BridgeID], [IPAddress], [NetworkAccess], [ServiceName], [SortID], [Usage], [LastModifiedDate])");
                stmt.Append(" select [AddressType], [BridgeID], [IPAddress], [NetworkAccess], [ServiceName], [SortID], [Usage], '" + LastModifiedDate + "'");
                stmt.Append(" from Mcu_IPServices_D where BridgeID=" + bridgeId);
                
                stmt.Append(" Insert into [Audit_Mcu_ISDNServices_D] ");
                stmt.Append(" ([BridgeID], [EndNumber], [NetworkAccess], [Prefix], [RangeSortOrder], [ServiceName], [SortID], [StartNumber], [Usage], [LastModifiedDate])");
                stmt.Append(" select [BridgeID], [EndNumber], [NetworkAccess], [Prefix], [RangeSortOrder], [ServiceName], [SortID], [StartNumber], [Usage], '" + LastModifiedDate + "'");
                stmt.Append(" from Mcu_ISDNServices_D where BridgeID=" + bridgeId);

                stmt.Append(" Insert into [Audit_Mcu_MPIServices_D] ");
                stmt.Append(" ([AddressType], [BridgeID], [IPAddress], [NetworkAccess], [RangeSortOrder], [ServiceName], [SortID], [Usage], [LastModifiedDate])");
                stmt.Append(" select [AddressType], [BridgeID], [IPAddress], [NetworkAccess], [RangeSortOrder], [ServiceName], [SortID], [Usage], '" + LastModifiedDate + "'");
                stmt.Append(" from Mcu_MPIServices_D where BridgeID=" + bridgeId);
                
                stmt.Append(" Insert into [Audit_MCU_Orgspecific_D] ");
                stmt.Append(" ([bridgeID], [orgID], [propertyName], [propertyValue], [LastModifiedDate])");
                stmt.Append(" select [bridgeID], [orgID], [propertyName], [propertyValue], '" + LastModifiedDate + "'");
                stmt.Append(" from MCU_Orgspecific_D where BridgeID=" + bridgeId);
                
                stmt.Append(" Insert into [Audit_MCU_Profiles_D] ");
                stmt.Append(" ([MCUId], [ProfileId], [ProfileName], [LastModifiedDate])");
                stmt.Append(" select [MCUId], [ProfileId], [ProfileName], '" + LastModifiedDate + "'");
                stmt.Append(" from MCU_Profiles_D where MCUId=" + bridgeId);
                m_bridgelayer.ExecuteNonQuery(stmt.ToString());

                if (AuditCardListStatus == 0 && description == "No Changes Made")
                    description = "No Changes Made";
                if (AuditCardListStatus == 2)
                    description = "Changes Made";

                //Mcu_CardList_D
                if (!isNewBridge && AuditCardListStatus == 1 && description == "No Changes Made")
                {
                    stmt = new StringBuilder();
                    stmt.Append(" select   case when disCount > actCnt then '1' when disCount < actCnt then '1' else 0  end as cnt  from ");
                    stmt.Append(" (  select COUNT(*) as disCount,   (select count (*) from [Audit_Mcu_CardList_D] where BridgeID = " + bridgeId + "  ");
                    stmt.Append(" and LastModifiedDate =   (SELECT TOP 1 LastModifiedDate From  ");
                    stmt.Append(" (select distinct Top 2 LastModifiedDate from [Audit_Mcu_CardList_D] ");
                    stmt.Append(" where BridgeID = " + bridgeId + " ORDER BY LastModifiedDate DESC) as x   ORDER BY LastModifiedDate)) as audCnt, ");
                    stmt.Append(" (select COUNT(*) from [Mcu_CardList_D] where BridgeID = " + bridgeId + ") as actCnt  from  ");
                    stmt.Append(" (  select distinct [BridgeID], [cardTypeId], [maxCalls]  from ");
                    stmt.Append(" (select * from [Audit_Mcu_CardList_D] where BridgeID = " + bridgeId + " ");
                    stmt.Append(" and LastModifiedDate in (select distinct top 2 LastModifiedDate as tt from [Audit_Mcu_CardList_D] ");
                    stmt.Append(" where BridgeID = " + bridgeId + "   order by LastModifiedDate desc)) as x  ) as y  ) as z");
                    returnVal = m_bridgelayer.ExecuteScalar(stmt.ToString());
                    if (returnVal != null)
                        int.TryParse(returnVal.ToString(), out bridgeCount);
                    if (bridgeCount > 0)
                        description = "Changes Made";
                }

                if (AuditE164ServiceStatus == 0 && description == "No Changes Made")
                    description = "No Changes Made";
                if (AuditE164ServiceStatus == 2)
                    description = "Changes Made";

                //MCU_E164Services_D
                if (!isNewBridge && AuditE164ServiceStatus == 1 && description == "No Changes Made")
                {
                    stmt = new StringBuilder();
                    stmt.Append(" select   case when disCount > actCnt then '1' when disCount < actCnt then '1' else 0  end as cnt  from ");
                    stmt.Append(" (  select COUNT(*) as disCount,   (select count (*) from [Audit_MCU_E164Services_D] where BridgeID = " + bridgeId + "  ");
                    stmt.Append(" and LastModifiedDate =   (SELECT TOP 1 LastModifiedDate From  ");
                    stmt.Append(" (select distinct Top 2 LastModifiedDate from [Audit_MCU_E164Services_D] ");
                    stmt.Append(" where BridgeID = " + bridgeId + " ORDER BY LastModifiedDate DESC) as x   ORDER BY LastModifiedDate)) as audCnt, ");
                    stmt.Append(" (select COUNT(*) from [MCU_E164Services_D] where BridgeID = " + bridgeId + ") as actCnt  from  ");
                    stmt.Append(" (  select distinct [BridgeID], [EndRange], [SortID], [StartRange] from ");
                    stmt.Append(" (select * from [Audit_MCU_E164Services_D] where BridgeID = " + bridgeId + " ");
                    stmt.Append(" and LastModifiedDate in (select distinct top 2 LastModifiedDate as tt from [Audit_MCU_E164Services_D] ");
                    stmt.Append(" where BridgeID = " + bridgeId + "   order by LastModifiedDate desc)) as x  ) as y  ) as z");
                    returnVal = m_bridgelayer.ExecuteScalar(stmt.ToString());
                    if (returnVal != null)
                        int.TryParse(returnVal.ToString(), out bridgeCount);
                    if (bridgeCount > 0)
                        description = "Changes Made";
                }

                if (AuditIPServiceStatus == 0 && description == "No Changes Made")
                    description = "No Changes Made";
                if (AuditIPServiceStatus == 2)
                    description = "Changes Made";

                //Mcu_IPServices_D
                if (!isNewBridge && AuditIPServiceStatus == 1 && description == "No Changes Made")
                {
                    stmt = new StringBuilder();
                    stmt.Append(" select   case when disCount > actCnt then '1' when disCount < actCnt then '1' else 0  end as cnt  from ");
                    stmt.Append(" (  select COUNT(*) as disCount,   (select count (*) from [Audit_Mcu_IPServices_D] where BridgeID = " + bridgeId + "  ");
                    stmt.Append(" and LastModifiedDate =   (SELECT TOP 1 LastModifiedDate From  ");
                    stmt.Append(" (select distinct Top 2 LastModifiedDate from [Audit_Mcu_IPServices_D] ");
                    stmt.Append(" where BridgeID = " + bridgeId + " ORDER BY LastModifiedDate DESC) as x   ORDER BY LastModifiedDate)) as audCnt, ");
                    stmt.Append(" (select COUNT(*) from [Mcu_IPServices_D] where BridgeID = " + bridgeId + ") as actCnt  from  ");
                    stmt.Append(" (  select distinct [AddressType], [BridgeID], [IPAddress], [NetworkAccess], [ServiceName], [SortID], [Usage] from ");
                    stmt.Append(" (select * from [Audit_Mcu_IPServices_D] where BridgeID = " + bridgeId + " ");
                    stmt.Append(" and LastModifiedDate in (select distinct top 2 LastModifiedDate as tt from [Audit_Mcu_IPServices_D] ");
                    stmt.Append(" where BridgeID = " + bridgeId + "   order by LastModifiedDate desc)) as x  ) as y  ) as z");
                    returnVal = m_bridgelayer.ExecuteScalar(stmt.ToString());
                    if (returnVal != null)
                        int.TryParse(returnVal.ToString(), out bridgeCount);
                    if (bridgeCount > 0)
                        description = "Changes Made";
                }

                if (AuditISDNServiceStatus == 0 && description == "No Changes Made")
                    description = "No Changes Made";
                if (AuditISDNServiceStatus == 2)
                    description = "Changes Made";

                //Mcu_ISDNServices_D
                if (!isNewBridge && AuditISDNServiceStatus == 1 && description == "No Changes Made")
                {
                    stmt = new StringBuilder();
                    stmt.Append(" select   case when disCount > actCnt then '1' when disCount < actCnt then '1' else 0  end as cnt  from ");
                    stmt.Append(" (  select COUNT(*) as disCount,   (select count (*) from [Audit_Mcu_ISDNServices_D] where BridgeID = " + bridgeId + "  ");
                    stmt.Append(" and LastModifiedDate =   (SELECT TOP 1 LastModifiedDate From  ");
                    stmt.Append(" (select distinct Top 2 LastModifiedDate from [Audit_Mcu_ISDNServices_D] ");
                    stmt.Append(" where BridgeID = " + bridgeId + " ORDER BY LastModifiedDate DESC) as x   ORDER BY LastModifiedDate)) as audCnt, ");
                    stmt.Append(" (select COUNT(*) from [Mcu_ISDNServices_D] where BridgeID = " + bridgeId + ") as actCnt  from  ");
                    stmt.Append(" (  select distinct [BridgeID], [EndNumber], [NetworkAccess], [Prefix], [RangeSortOrder], ");
                    stmt.Append(" [ServiceName], [SortID], [StartNumber], [Usage] from ");
                    stmt.Append(" (select * from [Audit_Mcu_ISDNServices_D] where BridgeID = " + bridgeId + " ");
                    stmt.Append(" and LastModifiedDate in (select distinct top 2 LastModifiedDate as tt from [Audit_Mcu_ISDNServices_D] ");
                    stmt.Append(" where BridgeID = " + bridgeId + "   order by LastModifiedDate desc)) as x  ) as y  ) as z");
                    returnVal = m_bridgelayer.ExecuteScalar(stmt.ToString());
                    if (returnVal != null)
                        int.TryParse(returnVal.ToString(), out bridgeCount);
                    if (bridgeCount > 0)
                        description = "Changes Made";
                }

                if (AuditMPIServiceStatus == 0 && description == "No Changes Made")
                    description = "No Changes Made";
                if (AuditMPIServiceStatus == 2)
                    description = "Changes Made";

                //Mcu_MPIServices_D
                if (!isNewBridge && AuditMPIServiceStatus == 1 && description == "No Changes Made")
                {
                    stmt = new StringBuilder();
                    stmt.Append(" select   case when disCount > actCnt then '1' when disCount < actCnt then '1' else 0  end as cnt  from ");
                    stmt.Append(" (  select COUNT(*) as disCount,   (select count (*) from [Audit_Mcu_MPIServices_D] where BridgeID = " + bridgeId + "  ");
                    stmt.Append(" and LastModifiedDate =   (SELECT TOP 1 LastModifiedDate From  ");
                    stmt.Append(" (select distinct Top 2 LastModifiedDate from [Audit_Mcu_MPIServices_D] ");
                    stmt.Append(" where BridgeID = " + bridgeId + " ORDER BY LastModifiedDate DESC) as x   ORDER BY LastModifiedDate)) as audCnt, ");
                    stmt.Append(" (select COUNT(*) from [Mcu_MPIServices_D] where BridgeID = " + bridgeId + ") as actCnt  from  ");
                    stmt.Append(" (  select distinct [bridgeID], [orgID], [propertyName], [propertyValue] from ");
                    stmt.Append(" (select * from [Audit_Mcu_MPIServices_D] where BridgeID = " + bridgeId + " ");
                    stmt.Append(" and LastModifiedDate in (select distinct top 2 LastModifiedDate as tt from [Audit_Mcu_MPIServices_D] ");
                    stmt.Append(" where BridgeID = " + bridgeId + " order by LastModifiedDate desc)) as x  ) as y  ) as z");
                    returnVal = m_bridgelayer.ExecuteScalar(stmt.ToString());
                    if (returnVal != null)
                        int.TryParse(returnVal.ToString(), out bridgeCount);
                    if (bridgeCount > 0)
                        description = "Changes Made";
                }


                if (AuditOrgspecificStatus == 0 && description == "No Changes Made")
                    description = "No Changes Made";
                if (AuditOrgspecificStatus == 2)
                    description = "Changes Made";

                //MCU_Orgspecific_D
                if (!isNewBridge && AuditOrgspecificStatus == 1 && description == "No Changes Made")
                {
                    stmt = new StringBuilder();
                    stmt.Append(" select   case when disCount > actCnt then '1' when disCount < actCnt then '1' else 0  end as cnt  from ");
                    stmt.Append(" (  select COUNT(*) as disCount,   (select count (*) from [Audit_MCU_Orgspecific_D] where BridgeID = " + bridgeId + "  ");
                    stmt.Append(" and LastModifiedDate =   (SELECT TOP 1 LastModifiedDate From  ");
                    stmt.Append(" (select distinct Top 2 LastModifiedDate from [Audit_MCU_Orgspecific_D] ");
                    stmt.Append(" where BridgeID = " + bridgeId + " ORDER BY LastModifiedDate DESC) as x   ORDER BY LastModifiedDate)) as audCnt, ");
                    stmt.Append(" (select COUNT(*) from [MCU_Orgspecific_D] where BridgeID = " + bridgeId + ") as actCnt  from  ");
                    stmt.Append(" (  select distinct [bridgeID], [orgID], [propertyName], [propertyValue] from ");
                    stmt.Append(" (select * from [Audit_MCU_Orgspecific_D] where BridgeID = " + bridgeId + " ");
                    stmt.Append(" and LastModifiedDate in (select distinct top 2 LastModifiedDate as tt from [Audit_MCU_Orgspecific_D] ");
                    stmt.Append(" where BridgeID = " + bridgeId + " order by LastModifiedDate desc)) as x  ) as y  ) as z");
                    returnVal = m_bridgelayer.ExecuteScalar(stmt.ToString());
                    if (returnVal != null)
                        int.TryParse(returnVal.ToString(), out bridgeCount);
                    if (bridgeCount > 0)
                        description = "Changes Made";
                }

                if (isNewBridge)
                    description = "Created";

                stmt = new StringBuilder();
                stmt.Append(" Update Audit_MCU_List_D Set ValueChange = ' " + description + "' where bridgeId = " + bridgeId + " and LastModified = '" + LastModifiedDate + "'");
                stmt.Append(" Insert into [Audit_Summary_MCUList_D] ");
                stmt.Append(" ([Description], [LastModifiedDate], [LastModifiedUser], [BridgeId], [ModifiedDetails])");
                stmt.Append(" VALUES ('" + description + "', '" + LastModifiedDate + "', " + LastModifiedUser + ", " + bridgeId + ", '" + ModifiedDetails + "')");

                m_bridgelayer.ExecuteNonQuery(stmt.ToString());
                m_bridgelayer.CloseConnection();
            }
            catch (Exception ex)
            {
                m_log.Error("SetAuditMCU Failed: " + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region GetAuditMCU
        /// <summary>
        /// GetAuditMCU
        /// </summary>
        /// <param name="bridgeId"></param>
        /// <param name="auditOutXML"></param>
        /// <returns></returns>
        private bool GetAuditMCU(int bridgeId, int LoginUserTimezone, ref StringBuilder auditOutXML)
        {
            string stmt = "";
            DataTable dt = null;
            DataSet ds = null;
            vrmUser LastUser = null;
            int LastModifiedUserId = 0;
            string LastModifiedDate = "", ModifiedDetails = "", Description = "";
            auditOutXML = new StringBuilder();
            try
            {
                if (m_bridgelayer == null)
                    m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                stmt = "  Select Top(4) *, dbo.changeTime(" + LoginUserTimezone + ", LastModifiedDate)  as LastModifiedDateTime from Audit_Summary_MCUList_D  where BridgeID = " + bridgeId + " order by LastModifiedDate desc ";

                ds = m_bridgelayer.ExecuteDataSet(stmt.ToString());
                if (ds != null)
                    dt = ds.Tables[0];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[0]["LastModifiedUser"] != null)
                        int.TryParse(dt.Rows[i]["LastModifiedUser"].ToString(), out LastModifiedUserId);

                    if (dt.Rows[0]["LastModifiedDateTime"] != null)
                        LastModifiedDate = dt.Rows[i]["LastModifiedDateTime"].ToString();

                    if (dt.Rows[0]["Description"] != null)
                        Description = dt.Rows[i]["Description"].ToString();

                    LastUser = m_vrmUserDAO.GetByUserId(LastModifiedUserId);
                    auditOutXML.Append("<LastModified>");
                    auditOutXML.Append("<ModifiedUserId>" + LastModifiedUserId + "</ModifiedUserId>");
                    auditOutXML.Append("<ModifiedUserName>" + LastUser.FirstName + " " + LastUser.LastName + "</ModifiedUserName>");
                    auditOutXML.Append("<ModifiedDateTime>" + LastModifiedDate + "</ModifiedDateTime>");
                    auditOutXML.Append("<ModifiedDetails>" + ModifiedDetails + "</ModifiedDetails>");
                    auditOutXML.Append("<Description>" + Description + "</Description>");
                    auditOutXML.Append("</LastModified>");
                }
            }
            catch (Exception ex)
            {
                m_log.Error("GetAuditMCU Failed: " + ex.Message);
                return false;
            }

            return true;
        }
        #endregion

		#region SetAuditEndpoint
        /// <summary>
        /// SetAuditEndpoint
        /// </summary>
        /// <param name="isNewEndpoint"></param>
        /// <param name="isNewProfile"></param>
        /// <param name="EptList"></param>
        /// <returns></returns>
        public bool SetAuditEndpoint(bool isNewEndpoint, bool isNewProfile, List<vrmEndPoint> EptList )
        {
            StringBuilder stmt = new StringBuilder();
            object returnVal = null;
            int endpointCount = 0;
            string description = "No Changes Made", ModifiedDetails = "";
            try
            {
                if (m_bridgelayer == null)
                    m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                for(int i = 0; i < EptList.Count; i++)
                {
                    stmt = new StringBuilder();
                    stmt.Append(" Insert into Audit_Ept_List_D ([endpointId],[name],[password],[protocol],[connectiontype],[addresstype],[address],");
                    stmt.Append(" [deleted],[outsidenetwork],[videoequipmentid],[linerateid],[bridgeid],[endptURL],[profileId],");
                    stmt.Append(" [profileName],[isDefault],[encrypted],[MCUAddress],[MCUAddressType],[TelnetAPI],[SSHSupport],[orgId],");//ZD 101363
                    stmt.Append(" [ExchangeID],[CalendarInvite],[ApiPortNo],[ConferenceCode],[LeaderPin],[isTelePresence],");
                    stmt.Append(" [MultiCodecAddress],[RearSecCameraAddress],[Extendpoint],[EptOnlineStatus],[PublicEndPoint],");
                    stmt.Append(" [NetworkURL],[Secureport],[Secured],[EptCurrentStatus],[GateKeeeperAddress],[UserName],[ManufacturerID],[ProfileType],[SysLocationId],");//ZD 100815
                    stmt.Append(" [LastModifiedUser],[Lastmodifieddate]) "); //ZD 104821
                    stmt.Append(" (select [endpointId],[name],[password],[protocol],[connectiontype],[addresstype],[address],");
                    stmt.Append(" [deleted],[outsidenetwork],[videoequipmentid],[linerateid],[bridgeid],[endptURL],[profileId],");
                    stmt.Append(" [profileName],[isDefault],[encrypted],[MCUAddress],[MCUAddressType],[TelnetAPI],[SSHSupport],[orgId],");//ZD 101363
                    stmt.Append(" [ExchangeID],[CalendarInvite],[ApiPortNo],[ConferenceCode],[LeaderPin],[isTelePresence],");
                    stmt.Append(" [MultiCodecAddress],[RearSecCameraAddress],[Extendpoint],[EptOnlineStatus],[PublicEndPoint],");
                    stmt.Append(" [NetworkURL],[Secureport],[Secured],[EptCurrentStatus],[GateKeeeperAddress],[UserName],[ManufacturerID],[ProfileType],[SysLocationId],"); //ZD 100815 //ZD 104821
                    stmt.Append(" [LastModifiedUser],[Lastmodifieddate]");
                    stmt.Append(" from Ept_List_D where endpointId = " + EptList[i].endpointid + " and ProfileId = " + EptList[i].profileId + ")");
                    m_bridgelayer.OpenConnection();
                    m_bridgelayer.ExecuteNonQuery(stmt.ToString());

                    if (!isNewEndpoint)
                    {
                        if (description == "Changes Made")
                            continue;
                        stmt = new StringBuilder();
                        stmt.Append(" select count(*) from ( ");
                        stmt.Append(" select distinct [address], [addresstype], [ApiPortNo], [bridgeid], [CalendarInvite], [ConferenceCode], [connectiontype],  ");
                        stmt.Append(" [deleted], [encrypted], [endpointId], [endptURL], [EptCurrentStatus],  "); //[ExchangeID], [EptOnlineStatus],
                        stmt.Append(" [Extendpoint], [GateKeeeperAddress], [isDefault], [isTelePresence],  "); //[LastModifiedUser], [Lastmodifieddate],
                        stmt.Append(" [LeaderPin], [linerateid], [ManufacturerID], [MCUAddress], [MCUAddressType], [MultiCodecAddress], [name],[ProfileType],[SysLocationId],"); //ZD 100815 //ZD 104821
                        stmt.Append(" [NetworkURL], [orgId], [outsidenetwork], [password], [profileId], [profileName], [protocol], [PublicEndPoint], ");
                        stmt.Append(" [RearSecCameraAddress], [Secured], [Secureport], [TelnetAPI],[SSHSupport], ISNULL([UserName], '') as userName, [videoequipmentid]  from (");//ZD 101363
                        stmt.Append(" select top 2 * from Audit_Ept_List_D where endpointId = " + EptList[i].endpointid + " and profileid = " + EptList[i].profileId + " ");
                        stmt.Append(" order by Lastmodifieddate desc ");
                        stmt.Append(" ) as x ) as y ");
                        returnVal = m_bridgelayer.ExecuteScalar(stmt.ToString());
                        if (returnVal != null)
                            int.TryParse(returnVal.ToString(), out endpointCount);
                        if (endpointCount > 1)
                            description = "Changes Made";
                    }
                }

                if(isNewEndpoint)
                    description = "Created";
                else if (isNewProfile)
                    description = "New Profile Created";

                stmt = new StringBuilder();
                stmt.Append(" Update Audit_Ept_List_D Set ValueChange = ' " + description + "' where endpointId = " + EptList[0].endpointid + " and ProfileId = " + EptList[0].profileId + " and Lastmodifieddate = '" + EptList[0].Lastmodifieddate + "'");
                stmt.Append(" Insert into Audit_Summary_EptList_D ");
                stmt.Append(" ([Description], [LastModifiedDate], [LastModifiedUser], [EndpointId], [ProfileId], [ModifiedDetails])");
                stmt.Append(" VALUES ('" + description + "', '" + EptList[0].Lastmodifieddate + "', " + EptList[0].LastModifiedUser + ", " + EptList[0].endpointid + ", " + EptList[0].profileId + ", '" + ModifiedDetails + "')");
                
                m_bridgelayer.ExecuteNonQuery(stmt.ToString());
                m_bridgelayer.CloseConnection();
            }
            catch (Exception ex)
            {
                m_log.Error("SetAuditEndpoint Failed: " + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region GetAuditEndpoint
        /// <summary>
        /// GetAuditEndpoint
        /// </summary>
        /// <param name="endpointId"></param>
        /// <param name="profileId"></param>
        /// <param name="LoginUserTimezone"></param>
        /// <param name="auditOutXML"></param>
        /// <returns></returns>
        private bool GetAuditEndpoint(int endpointId, int profileId, int LoginUserTimezone, ref StringBuilder auditOutXML)
        {
            DataTable dt = null;
            DataSet ds = null;
            vrmUser LastUser = null;
            int LastModifiedUserId = 0;
            string stmt = "", LastModifiedDate = "", ModifiedDetails = "", Description = "";
            auditOutXML = new StringBuilder();
            try
            {
                if (m_bridgelayer == null)
                    m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                stmt = " Select Top(4) *, dbo.changeTime(" + LoginUserTimezone + ", LastModifiedDate) AS LastModifiedDateTime from Audit_Summary_EptList_D  where endpointId = " + endpointId + " order by Lastmodifieddate desc ";

                ds = m_bridgelayer.ExecuteDataSet(stmt.ToString());
                if (ds != null)
                    dt = ds.Tables[0];

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[0]["LastModifiedUser"] != null)
                        int.TryParse(dt.Rows[i]["LastModifiedUser"].ToString(), out LastModifiedUserId);

                    if (dt.Rows[0]["LastModifiedDateTime"] != null)
                        LastModifiedDate = dt.Rows[i]["LastModifiedDateTime"].ToString();

                    if (dt.Rows[i]["ModifiedDetails"] != null)
                        ModifiedDetails = dt.Rows[i]["ModifiedDetails"].ToString();

                    if (dt.Rows[0]["Description"] != null)
                        Description = dt.Rows[i]["Description"].ToString();

                    LastUser = m_vrmUserDAO.GetByUserId(LastModifiedUserId);
                    auditOutXML.Append("<LastModified>");
                    auditOutXML.Append("<ModifiedUserId>" + LastModifiedUserId + "</ModifiedUserId>");
                    auditOutXML.Append("<ModifiedUserName>" + LastUser.FirstName + " " + LastUser.LastName + "</ModifiedUserName>");
                    auditOutXML.Append("<ModifiedDateTime>" + LastModifiedDate + "</ModifiedDateTime>");
                    auditOutXML.Append("<ModifiedDetails>" + ModifiedDetails + "</ModifiedDetails>");
                    auditOutXML.Append("<Description>" + Description + "</Description>");
                    auditOutXML.Append("</LastModified>");
                }
            }
            catch (Exception ex)
            {
                m_log.Error("GetAuditEndpoint Failed: " + ex.Message);
                return false;
            }
            return true;
        }
        #endregion
        //ZD 101026 End

        //ZD 100522 Starts
        #region GetBridgeDetails
        /// <summary>
        /// GetBridgeDetails
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetBridgeDetails(ref vrmDataObject obj)
        {
            XmlDocument xd = new XmlDocument();
            XmlNode node = null;
            StringBuilder outXML = new StringBuilder();
            int bridgeId = 0;
            vrmMCU MCU = null;

            try
            {
                xd.LoadXml(obj.inXml);

                node = xd.SelectSingleNode("//GetBridgeDetails/BridgeId");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out bridgeId);

                MCU = m_ImcuDao.GetById(bridgeId);

                if (MCU == null)
                    return false;
                outXML.Append("<GetBridgeDetails>");
                outXML.Append("<BridgeId>" + MCU.BridgeID + "</BridgeId>");
                outXML.Append("<BridgeName>" + MCU.BridgeName + "</BridgeName>");
                outXML.Append("<LoginName>" + MCU.LoginName + "</LoginName>");
                outXML.Append("<Password>" + MCU.BridgePassword + "</Password>");
                outXML.Append("<BridgeTypeId>" + MCU.MCUType.id + "</BridgeTypeId>");
                outXML.Append("<TimezoneId>" + MCU.timezone + "</TimezoneId>");
                outXML.Append("<ChainPosition>" + MCU.ChainPosition + "</ChainPosition>");//ZD 101871
                outXML.Append("<interfaceType>" + MCU.MCUType.bridgeInterfaceId + "</interfaceType>");//ZD 101871
                outXML.Append("<ConferenceServiceID>" + MCU.ConfServiceID + "</ConferenceServiceID>");//ZD 101871
                outXML.Append("<ConferencePoolOrderID>" + MCU.PoolOrderID + "</ConferencePoolOrderID>");//ZD 104256
                outXML.Append("</GetBridgeDetails>");

                obj.outXml = outXML.ToString();

            }
            catch (Exception ex)
            {
                m_log.Error("GetBridgeDetails Failed: " + ex.Message);
                obj.outXml = "";
                return false;
            }

            return true;

        }
        #endregion
        //ZD 100522 End

        //ZD 100815 Starts
        #region GetEndPointP2PQuery
        public bool GetEndPointP2PQuery(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<ICriterion> criterionList = new List<ICriterion>();
            StringBuilder searchOutXml = new StringBuilder(); 
            int EndpointID = 0,ProfileId =0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetEndPointP2PQuery/EndpointID");
                int.TryParse(node.InnerXml.Trim(), out EndpointID);
                node = xd.SelectSingleNode("//GetEndPointP2PQuery/ProfileId");
                int.TryParse(node.InnerXml.Trim(), out ProfileId);
                
                //m_IeptDao.addOrderBy(Order.Desc("isDefault"));
                //m_IeptDao.addOrderBy(Order.Asc("profileId"));
                criterionList.Add(Expression.Eq("profileId", ProfileId));
                criterionList.Add(Expression.Eq("endpointid", EndpointID));
                criterionList.Add(Expression.Eq("deleted", 0));
                List<vrmEndPoint> epList = m_IeptDao.GetByCriteria(criterionList);


                m_IeptDao.clearOrderBy();

                List<ICriterion> ProfilecriterionList = new List<ICriterion>();

                searchOutXml.Append("<EndpointP2PDetails>");

                foreach (vrmEndPoint ep in epList)
                {
                    searchOutXml.Append("<Profile>");
                    searchOutXml.Append("<ProfileID>" + ep.profileId.ToString() + "</ProfileID>");
                    searchOutXml.Append("<ProfileName>" + ep.profileName + "</ProfileName>");
                    searchOutXml.Append("<AddressType>" + ep.addresstype.ToString() + "</AddressType>");
                    searchOutXml.Append("<VideoEquipment>" +
                                                    ep.videoequipmentid.ToString() + "</VideoEquipment> ");
                    searchOutXml.Append("<ProfileType>" + ep.ProfileType.ToString() + "</ProfileType>"); 
                    searchOutXml.Append("<IsP2PDefault>" + ep.IsP2PDefault.ToString() + "</IsP2PDefault>"); 
                    searchOutXml.Append("<IsTestEquipment>" + ep.IsTestEquipment.ToString() + "</IsTestEquipment>"); 
                    searchOutXml.Append("</Profile>");
                }

                searchOutXml.Append("</EndpointP2PDetails>");
                obj.outXml = searchOutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;

        }
        #endregion
        //ZD 100815 Ends

        //ZD 101443 Start

        #region FetchUserAssignedMCUs
        /// <summary>
        /// FetchUserAssignedMCUs
        /// </summary>
        /// <param name="organizationID"></param>
        /// <param name="userid"></param>
        /// <param name="MCUList"></param>
        public void FetchUserAssignedMCUs(int organizationID, int userid, ref List<vrmMCU> MCUList)
        {
            List<ICriterion> Icrit = new List<ICriterion>();
            try
            {
                Icrit.Add(Expression.Eq("orgId", organizationID));
                Icrit.Add(Expression.Eq("deleted", 0));
                MCUList = m_ImcuDao.GetByCriteria(Icrit, true);
                MCUList = MCUList.Where(m => m.Admin == userid || m.MCUApprover.Count(l => l.approverid == userid) > 0).ToList();
            }
            catch (Exception ex)
            {
                m_log.Error("FetchUserAssignedMCUs :" + ex.Message);
            }
        }

        #endregion

        #region UpdateUserAssignedMCUs
        /// <summary>
        /// UpdateUserAssignedMCUs
        /// </summary>
        /// <param name="organizationID"></param>
        /// <param name="Extuserid"></param>
        /// <param name="SelectAll"></param>
        /// <param name="NewUserID"></param>
        /// <param name="SelectedID"></param>
        /// <returns></returns>
        public bool UpdateUserAssignedMCUs(int organizationID, int Extuserid, int SelectAll, int NewUserID, XPathNavigator xNode, string inXml,ref int ErroNo)
        {
            List<ICriterion> Icrit = new List<ICriterion>();
            List<vrmMCU> MCUList = null;
            List<vrmMCUApprover> Applist = null;
            string SelectedID = "";
            try
            {
                if (xNode != null)
                {
                    var idlist = (XDocument.Parse(inXml).Descendants("SelectedID").Elements("ID").Select(element => element.Value).ToList());

                    idlist.Remove("");
                    #region Assisant Update
                    Icrit.Add(Expression.Eq("orgId", organizationID));
                    Icrit.Add(Expression.Eq("deleted", 0));
                    Icrit.Add(Expression.Eq("Admin", Extuserid));
                    Icrit.Add(Expression.In("BridgeID", idlist));

                    MCUList = m_ImcuDao.GetByCriteria(Icrit, true);
                    if (MCUList != null && MCUList.Count > 0)
                    {
                        MCUList = MCUList.Select((data, i) => { data.Admin = NewUserID; return data; }).ToList();
                        m_ImcuDao.SaveOrUpdateList(MCUList);
                    }
                    #endregion

                    #region Approver Update

                    Icrit = new List<ICriterion>();
                    Icrit.Add(Expression.Eq("approverid", Extuserid));
                    Icrit.Add(Expression.In("mcuid", idlist));
                    Applist = m_Iapprover.GetByCriteria(Icrit);
                    if (Applist != null && Applist.Count > 0)
                    {
                        if (!m_SearchFactory.CheckApproverRights(NewUserID.ToString()))
                        {
                            ErroNo = 632;
                            return false;
                        }
                        Applist = Applist.Select((data, i) => { data.approverid = NewUserID; return data; }).ToList();
                        m_Iapprover.SaveOrUpdateList(Applist);
                    }
                    #endregion
                }
                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("UpdateUserAssignedMCUs :" + ex.Message);
                return false;
            }
        }
        #endregion

        //ZD 101443 End		

		//ZD 101522 Starts
        #region GetSystemLocation
        /// <summary>
        /// GetSystemLocation
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSystemLocation(ref vrmDataObject obj)
        {
            try
            {
                XmlDocument xd = new XmlDocument();
                XmlNode node = null;
                xd.LoadXml(obj.inXml);

                int userId = 0, orgId = 0, bridgeID = 0;

                node = xd.SelectSingleNode("//GetSystemLocation/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userId);

                node = xd.SelectSingleNode("//GetSystemLocation/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out orgId);

                if (orgId < 11)
                    organizationID = orgId;

                node = xd.SelectSingleNode("//GetSystemLocation/BridgeId");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out bridgeID);

                //Gen_SystemLocation_D

                DataSet ds = null;
                string hql = "";
                if (m_bridgelayer == null)
                    m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                hql = " Select * from Gen_SystemLocation_D where MCUId = " + bridgeID + " ";

                if (hql != "")
                    ds = m_bridgelayer.ExecuteDataSet(hql);

                StringBuilder outXML = new StringBuilder();
                outXML.Append("<GetSystemLocations>");
                outXML.Append("<bridgeId>" + bridgeID + "</bridgeId>");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        outXML.Append("<GetSystemLocation>");
                        outXML.Append("<SysLocId>" + ds.Tables[0].Rows[i]["syslocid"].ToString() + "</SysLocId>");
                        outXML.Append("<Name>" + ds.Tables[0].Rows[i]["Name"].ToString() + "</Name>");
                        outXML.Append("</GetSystemLocation>");
                    }
                }
                outXML.Append("</GetSystemLocations>");


                obj.outXml = outXML.ToString();
            }
            catch (Exception ex)
            {
                m_log.Error("GetSystemLocation Failed: " + ex.Message);
                obj.outXml = "";
                return false;
            }

            return true;
        }
        #endregion
		//ZD 101522 End

        //ZD 101527 Starts
        #region GetSyncEndpoints
        /// <summary>
        /// GetSyncEndpoints
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetSyncEndpoints(ref vrmDataObject obj)
        {
            List<ICriterion> criterionList = new List<ICriterion>();
            List<vrmSyncEndPoint> epList = new List<vrmSyncEndPoint>();
            vrmSyncEndPoint syncEpt = null;
            OutXML = new StringBuilder();
            xSettings = null;
            obj.outXml = "";
            
            string EndpointID = "",EndpointName="",Protocol="",AddressType="",Address="",ProfileID="";
            string ProfileName = "", isDefault = "", VideoEuptment = "", OrgID = "";
            long ttlRecords = 0;
            int PageNo = 0, ttlPages = 0;

            int i = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);

                
                using (xStrReader = new StringReader(obj.inXml))
                {
                    xDoc = new XPathDocument(xStrReader);
                    {
                        xNavigator = xDoc.CreateNavigator();

                        xNode = xNavigator.SelectSingleNode("//GetSyncEndpoints/organizationID");
                        if (xNode != null)
                            int.TryParse(xNode.Value.Trim(), out organizationID);

                        if (organizationID < 11)
                        {
                            obj.outXml = "<error>Invalid Organization ID</error>";
                            return false;
                        }
                    }
                }
                m_ISyncEptDao.clearFetch();
                criterionList.Add(Expression.Eq("isDefault", 1));
                criterionList.Add(Expression.Eq("orgId", organizationID));
                epList = m_ISyncEptDao.GetByCriteria(criterionList);
                
                xSettings = new XmlWriterSettings();
                xSettings.OmitXmlDeclaration = true;
                using (xWriter = XmlWriter.Create(OutXML, xSettings))
                {
                    xWriter.WriteStartElement("GetSyncEndpoints");

                    xNode = xNavigator.SelectSingleNode("//GetSyncEndpoints/PageNo");
                    if (xNode != null)
                        Int32.TryParse(xNode.Value.Trim(), out PageNo);

                    if (epList != null && epList.Count > 0)
                    {
                        ttlRecords = epList.Count;
                        epList = epList.Skip(m_iMaxRecords * (PageNo - 1)).Take(m_iMaxRecords).ToList(); 

                        ttlPages = (int)(ttlRecords / m_iMaxRecords);

                        if (ttlRecords % m_iMaxRecords > 0)
                            ttlPages++;

                        for (i = 0; i < epList.Count; i++)
                        {
                            syncEpt = new vrmSyncEndPoint();
                            syncEpt = epList[i];

                            EndpointID = Convert.ToString(syncEpt.endpointid);
                            EndpointName = Convert.ToString(syncEpt.name);
                            Protocol = Convert.ToString(syncEpt.protocol);
                            AddressType = Convert.ToString(syncEpt.addresstype);
                            Address = Convert.ToString(syncEpt.address);
                            ProfileID = Convert.ToString(syncEpt.profileId);
                            ProfileName = Convert.ToString(syncEpt.profileName);
                            isDefault = Convert.ToString(syncEpt.isDefault);
                            VideoEuptment = Convert.ToString(syncEpt.videoequipmentid);
                            OrgID = Convert.ToString(syncEpt.orgId);

                            xWriter.WriteStartElement("SyncEndpoint");
                            xWriter.WriteElementString("EndpointID", EndpointID);
                            xWriter.WriteElementString("EndpointName", EndpointName);
                            xWriter.WriteElementString("Protocol", Protocol);
                            xWriter.WriteElementString("AddressType", AddressType);
                            xWriter.WriteElementString("Address", Address);
                            xWriter.WriteElementString("ProfileID", ProfileID);
                            xWriter.WriteElementString("ProfileName", ProfileName);
                            xWriter.WriteElementString("isDefault", isDefault);
                            xWriter.WriteElementString("VideoEuptment", VideoEuptment);
                            xWriter.WriteElementString("OrgID", OrgID);
                            xWriter.WriteFullEndElement();
                        }
                    }
                                        

                    xWriter.WriteElementString("TotalPages", ttlPages.ToString());
                    xWriter.WriteElementString("PageNo", PageNo.ToString());

                    xWriter.WriteFullEndElement();
                }

                obj.outXml = OutXML.ToString();
            }
            catch (Exception ex)
            {
                m_log.Error("GetSyncEndpoints Failed: " + ex.Message);
                obj.outXml = "";
                return false;
            }

            return true;
        }
        #endregion

        #region SetSyncEndpoints
        public bool SetSyncEndpoints(ref vrmDataObject obj)
        {
            bool bRet = true;
            List<vrmVideoEquipment> videoEqList = vrmGen.getVideoEquipment();
            List<vrmVideoEquipment> slecEqList = new List<vrmVideoEquipment>();
            bool isNewEndpoint = false;
            int pId = 0, eId = 0, userID = 0;
            IList maxId;
            List<vrmEndPoint> endPoints = new List<vrmEndPoint>();
            List<ICriterion> selcnt = new List<ICriterion>();
            List<ICriterion> criterionList;
            XmlElement itemElement;
            List<vrmSyncEndPoint> syncEpts = new List<vrmSyncEndPoint>();
            vrmSyncEndPoint SyncEndpoint = new vrmSyncEndPoint();
            int SyncEptID = 0, eptID = 0;
            try
            {

                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                XmlNodeList endpointIDs;

                node = xd.SelectSingleNode("//SetSyncEndpoints/organizationID");
                string orgid = "";
                if (node != null)
                    orgid = node.InnerXml.Trim();
                if (orgid == "")
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = 11;
                Int32.TryParse(orgid, out organizationID);
                if (organizationID < defaultOrgId)
                {
                    myVRMException myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }
                OrgData orgdata = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                node = xd.SelectSingleNode("//SetSyncEndpoints/userID");
                if (node != null)
                    int.TryParse(node.InnerXml.Trim(), out userID);

                if (userID < defaultOrgId)
                    userID = 11;

                endpointIDs = xd.SelectNodes("//SetSyncEndpoints/EndpointIDs/EndpointID");

                if (endpointIDs != null && endpointIDs.Count > 0)
                {
                   
                    for (int eptcnt = 0; eptcnt < endpointIDs.Count; eptcnt++)
                    {
                        itemElement = (XmlElement)endpointIDs[eptcnt];
                        criterionList = new List<ICriterion>();
                        vrmEndPoint ve = new vrmEndPoint();
                        selcnt = new List<ICriterion>();

                        m_IeptDao.addProjection(Projections.Max("endpointid"));
                        maxId = m_IeptDao.GetObjectByCriteria(new List<ICriterion>());
                        if (maxId[0] != null)
                            eId = ((int)maxId[0]) + 1;
                        else
                            eId = 1;
                        m_IeptDao.clearProjection();

                        isNewEndpoint = true;
                        pId++;

                        eptID = eId;
                        ve.profileId = pId;
                        ve.PublicEndPoint = 0;

                        if (itemElement != null)
                            int.TryParse(itemElement.InnerXml.Trim(), out SyncEptID);


                        syncEpts = new List<vrmSyncEndPoint>();

                        syncEpts = m_ISyncEptDao.GetByEptId(SyncEptID);

                        // License Check
                        selcnt.Add(Expression.Eq("deleted", 0));
                        selcnt.Add(Expression.Or(Expression.Eq("isDefault", 1), Expression.Eq("IsP2PDefault", 1)));
                        selcnt.Add(Expression.Eq("orgId", organizationID));
                        selcnt.Add(Expression.Eq("Extendpoint", 0));
                        selcnt.Add(Expression.Eq("PublicEndPoint", 0));

                        List<vrmEndPoint> checkEptCount = m_IeptDao.GetByCriteria(selcnt);

                        checkEptCount = ((checkEptCount.Where(x => checkEptCount.Count(z => z.endpointid == x.endpointid) > 1).ToList()).Where(x => x.ProfileType == 1).ToList()).Union(checkEptCount.Where(x => checkEptCount.Count(z => z.endpointid == x.endpointid) == 1).ToList()).ToList();

                        OrgData orgdt = m_IOrgSettingsDAO.GetByOrgId(organizationID);

                        if (checkEptCount.Count >= orgdt.MaxEndpoints)
                        {
                            myVRMException ex = new myVRMException(458);
                            m_log.Error("Error EndPoint Limit exceeeded: ", ex);
                            obj.outXml = ex.FetchErrorMsg();
                            return false;
                        }

                       
                        for (int z = 0; z < syncEpts.Count; z++)
                        {
                            ve = new vrmEndPoint();
                             
                            SyncEndpoint = new vrmSyncEndPoint();
                            SyncEndpoint = syncEpts[z];

                            ve.endpointid = eptID;
                            ve.PublicEndPoint = 0;
                            ve.name = SyncEndpoint.name;
                            ve.profileName = SyncEndpoint.profileName;
                            ve.address = SyncEndpoint.address;
                            ve.addresstype = SyncEndpoint.addresstype;
                            ve.profileId = SyncEndpoint.profileId;
                            ve.isDefault = SyncEndpoint.isDefault;
                            ve.bridgeid = SyncEndpoint.mcuID;
                            ve.videoequipmentid = SyncEndpoint.ManufacturerID;
                            ve.orgId = SyncEndpoint.orgId;
                            ve.protocol = SyncEndpoint.protocol;
                            ve.identifierValue = SyncEndpoint.identifierValue; 
                            //Default Values
                            ve.password = "";
                            ve.deleted = 0;
                            ve.outsidenetwork = 0;
                            ve.linerateid = orgdata.DefLinerate;
                            ve.endptURL = "";
                            ve.encrypted = 0;
                            ve.MCUAddress = "";
                            ve.MCUAddressType = -1;
                            ve.TelnetAPI = 0;
                            ve.ExchangeID = "";
                            ve.CalendarInvite = 0;
                            ve.ApiPortNo = 23;
                            ve.ConferenceCode = "";
                            ve.LeaderPin = "";
                            ve.isTelePresence = 0;
                            ve.MultiCodecAddress = "";
                            ve.RearSecCameraAddress = "";
                            ve.Extendpoint = 0;
                            ve.EptOnlineStatus = 0;
                            ve.PublicEndPoint = 0;
                            ve.NetworkURL = "";
                            ve.Secured = 0;
                            ve.Secureport = 0;
                            ve.EptCurrentStatus = 0;
                            ve.GateKeeeperAddress = "";
                            ve.UserName = "";
                            ve.ManufacturerID = 3;
                            ve.IsTestEquipment = 0;
                            ve.IsP2PDefault = 0;
                            ve.ProfileType = 1; //MCu
                            ve.CreateCategory = 2;
                            ve.SSHSupport = 0;

                            ve.Lastmodifieddate = DateTime.UtcNow;
                            ve.LastModifiedUser = userID;
                            endPoints.Add(ve);
                        }
                        

                        selcnt = new List<ICriterion>();
                        selcnt.Add(Expression.Eq("identifierValue", ve.identifierValue));
                        selcnt.Add(Expression.Eq("orgId", organizationID));
                        List<vrmSyncLoc> SyncRoomCount = m_ISyncLocDAO.GetByCriteria(selcnt);
                        if (SyncRoomCount.Count > 0)
                        {
                            SyncRoomCount = SyncRoomCount.Select((data, i) => { data.endpointid = ve.endpointid; data.endPointCategory = 2; return data; }).ToList();
                            m_ISyncLocDAO.SaveOrUpdateList(SyncRoomCount);
                        }
                        for (int z = 0; z < syncEpts.Count; z++)
                        {
                            m_ISyncEptDao.Delete(syncEpts[z]);
                        
                        }
                        m_IeptDao.SaveOrUpdateList(endPoints);
                    }
                    

                    
                    SetAuditEndpoint(isNewEndpoint, true, endPoints);

                    if (endPoints.Count < 1)
                        return false;
                }
                obj.outXml = "<SetEndpoint><EndpointID>" + ((vrmEndPoint)endPoints[0]).endpointid.ToString() +
                   "</EndpointID></SetEndpoint>";

            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }

            return bRet;

        }
        #endregion
        //ZD 101527 Ends
        //ZD 100040 Smart MCU starts

        #region GetEptResolution
        /// <summary>
        /// GetEptResolution
        /// </summary>
        /// <param name="EptResolution"></param>
        /// <param name="outputXML"></param>
        /// <returns></returns>
        public bool GetEptResolution(ref vrmDataObject obj)
        {
            bool bRet = true;
            try
            {
                // Load the string into xml parser and retrieve the tag values.

                List<VrmEptResolution> videoList = vrmGen.getEptResolution();
                obj.outXml += "<EptResolutionList >";
                for (int i = 0; i < videoList.Count; i++)
                {
                    obj.outXml += "<EptResolution>";
                    obj.outXml += "<RID>" + videoList[i].RID.ToString() + "</RID>";
                    obj.outXml += "<Resolution>" + videoList[i].Resolution + "</Resolution>";
                    obj.outXml += "</EptResolution >";
                }
                obj.outXml += "</EptResolutionList>";


            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";//FB 1881
                bRet = false;
            }

            return bRet;
        }
        #endregion

        #region GetMCUPortResXML
        /// <summary>
        /// GetMCUPortResXML
        /// </summary>
        /// <returns></returns>
        private string GetMCUPortResXML(List<vrmMCUPortResolution> assgndPorts)
        {
            string portString = "<videoPorts>";
            try
            {
                // Load the string into xml parser and retrieve the tag values.
                vrmMCUPortResolution portResObj = null;
                List<VrmEptResolution> videoList = vrmGen.getEptResolution();

                if (assgndPorts == null)
                    assgndPorts = new List<vrmMCUPortResolution>();

                
                for (int i = 0; i < videoList.Count; i++)
                {
                    portString += "<port>";
                    portString += "<resolutionId>" + videoList[i].RID + "</resolutionId>";
                    portString += "<resolution>" + videoList[i].Resolution + "</resolution>";
                    
                    portResObj = assgndPorts.Where(r => r.ResolutionId == videoList[i].RID).FirstOrDefault();
                    
                    portString += "<qty>" + (portResObj != null ? portResObj.Port.ToString() : "0") + "</qty>";
                    portString += "</port>";
                }
            }
            catch (Exception e)
            {
                m_log.Error("GetMCUPortResXML", e);
            }
            portString += "</videoPorts>";
            return portString;
        }
        #endregion

        #region SetMCUGroup
        /// <summary>
        /// SetMCUGroup
        /// inxml structure 
        /// <setMCUGroup>
        /// <userID></userID>
        /// <organizationID></organizationID>
        /// <group>
        /// <groupID></groupID>
        /// <groupName></groupName>
        /// <description></description>
        /// <loadBalance></loadBalance>
        /// <pooled></pooled>
        /// <lcr></lcr>
        /// <mcu>
        /// <mcuID></mcuID>
        /// <mcuAdmin></mcuAdmin>
        /// <timezoneID></timezoneID>
        /// <loadBalance></loadBalance>
        /// <overFlow></overFlow>
        /// <leadMCU></leadMCU>
        /// </mcu>
        /// </group>
        /// </setMCUGroup>
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>

        public bool SetMCUGroup(ref vrmDataObject obj)
        {
            bool bRet = true;
            vrmMCUGroupDetails grpdetails;
            myVRMException myVRMEx = new myVRMException();

            string mcugroup = "";
            int loginuser = 0, mcuGroupID = 0;
            bool isEditMode = false;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//setMCUGroup/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out loginuser);

                if (loginuser <= 0)
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = defaultOrgId;
                node = xd.SelectSingleNode("//setMCUGroup/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                string groupname = "";
                node = xd.SelectSingleNode("//setMCUGroup/group/groupName");
                if (node != null)
                    groupname = node.InnerText.Trim();
                if (groupname.Length <= 0)
                {
                    myVRMEx = new myVRMException(422); //need to check error message
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//setMCUGroup/group/groupID");
                if (node != null)
                    mcugroup = node.InnerText.Trim();

                if (mcugroup.ToLower() != "new")
                {
                    int.TryParse(mcugroup, out mcuGroupID);
                    if (mcuGroupID > 0)
                        isEditMode = true;
                }

                // Duplicate MCU group name check    
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgID", organizationID));
                criterionList.Add(Expression.Eq("GroupName", groupname.ToUpper()));

                if (isEditMode)
                {
                    criterionList.Add(Expression.Not(Expression.Eq("MCUGroupID", mcuGroupID)));
                }
                IList<vrmMCUGroupDetails> mcuGroupIds = m_IMCUGroupDetailsDao.GetByCriteria(criterionList);

                if (mcuGroupIds.Count > 0)
                {
                    myVRMEx = new myVRMException(415); //Duplicate Group Name.
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                if (isEditMode)
                {
                    grpdetails = m_IMCUGroupDetailsDao.GetById(mcuGroupID);
                }
                else
                {
                    mcuGroupID = 1;

                    m_IMCUGroupDetailsDao.addProjection(Projections.Max("MCUGroupID"));
                    IList maxId = m_IMCUGroupDetailsDao.GetObjectByCriteria(new List<ICriterion>());
                    if (maxId[0] != null)
                        mcuGroupID = ((int)maxId[0]) + 1;

                    grpdetails = new vrmMCUGroupDetails();
                    grpdetails.MCUGroupID = mcuGroupID;
                    grpdetails.CreateTime = DateTime.Now;
                    grpdetails.CreatedBy = loginuser;
                    grpdetails.OrgID = organizationID;
                }
                grpdetails.GroupName = groupname;

                string description = "";
                node = xd.SelectSingleNode("//setMCUGroup/group/description");
                if (node != null)
                    description = node.InnerText.Trim();
                grpdetails.Description = description;

                int pooled = 0;
                node = xd.SelectSingleNode("//setMCUGroup/group/pooled");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out pooled);
                grpdetails.Pooled = pooled;

                int lcr = 0;
                node = xd.SelectSingleNode("//setMCUGroup/group/lcr");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out lcr);
                grpdetails.LCR = lcr;

                int loadBalance = 0;
                node = xd.SelectSingleNode("//setMCUGroup/group/loadBalance");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out loadBalance);
                grpdetails.LoadBalance = loadBalance;

                grpdetails.LastUpdateTime = DateTime.Now;
                grpdetails.ModifiedBy = loginuser;

                m_IMCUGroupDetailsDao.SaveOrUpdate(grpdetails);

                criterionList = null;
                if (isEditMode)
                {
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("MCUGroupID", mcuGroupID));
                    List<vrmMCUGrpAssignment> delgrps = m_IMCUGrpAssignmentDao.GetByCriteria(criterionList);
                    for (int i = 0; i < delgrps.Count; i++)
                    {
                        m_IMCUGrpAssignmentDao.Delete(delgrps[i]);

                    }
                    delgrps = null;
                }

                vrmMCUGrpAssignment mcuGrpAssign;
                XmlNodeList nodes = xd.SelectNodes("//setMCUGroup/group/mcu");

                int mcuid, ldBalance, overFlow, leadMCU;
                for (int i = 0; i < nodes.Count; i++)
                {
                    mcuid = 0; ldBalance = 0; overFlow = 0; leadMCU = 0;
                    mcuGrpAssign = null;
                    mcuGrpAssign = new vrmMCUGrpAssignment();
                    mcuGrpAssign.MCUGroupID = mcuGroupID;

                    node = nodes[i].SelectSingleNode("mcuID");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out mcuid);
                    mcuGrpAssign.MCUID = mcuid;

                    node = nodes[i].SelectSingleNode("loadBalance");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out ldBalance);
                    mcuGrpAssign.LoadBalance = ldBalance;

                    node = nodes[i].SelectSingleNode("overFlow");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out overFlow);
                    mcuGrpAssign.Overflow = overFlow;

                    node = nodes[i].SelectSingleNode("leadMCU");
                    if (node != null)
                        Int32.TryParse(node.InnerText.Trim(), out leadMCU);
                    mcuGrpAssign.LeadMCU = leadMCU;

                    m_IMCUGrpAssignmentDao.SaveOrUpdate(mcuGrpAssign);
                }

                obj.outXml = "<success>1</success>";
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetMCUGroup
        public bool GetMCUGroup(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder outXML = null;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = null;
            int userid = 0, mcugrpid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetMCUGroup/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userid);

                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                vrmUser user = m_vrmUserDAO.GetByUserId(userid);
                checkHardwareAuthority hasAthority = new checkHardwareAuthority(user.MenuMask);
                if (!hasAthority.hasMCUsLoadBalance)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = defaultOrgId;
                node = xd.SelectSingleNode("//GetMCUGroup/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//GetMCUGroup/groupID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out mcugrpid);

                //Fetch mcu group basic detaisl
                vrmMCUGroupDetails mcuGroupObj = m_IMCUGroupDetailsDao.GetById(mcugrpid);

                //Fetch mcu group mcu assignment
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("MCUGroupID", mcugrpid));
                List<vrmMCUGrpAssignment> mcuAssignment = m_IMCUGrpAssignmentDao.GetByCriteria(criterionList);

                outXML = new StringBuilder();
                outXML.Append("<groups>");
                if (mcuGroupObj != null)
                {
                    outXML.Append("<group>");
                    outXML.Append("<organizationID>" + mcuGroupObj.OrgID + "</organizationID>");
                    outXML.Append("<groupID>" + mcuGroupObj.MCUGroupID + "</groupID>");
                    outXML.Append("<groupName>" + mcuGroupObj.GroupName.Trim() + "</groupName>");
                    outXML.Append("<description>" + mcuGroupObj.Description + "</description>");
                    outXML.Append("<pooled>" + mcuGroupObj.Pooled + "</pooled>");
                    outXML.Append("<lcr>" + mcuGroupObj.LCR + "</lcr>");
                    outXML.Append("<loadBalance>" + mcuGroupObj.LoadBalance + "</loadBalance>");

                    outXML.Append("<mcus>");
                    user = null;
                    vrmMCU mcuObj = null;
                    for (int j = 0; j < mcuAssignment.Count; j++)
                    {
                        mcuObj = null;
                        mcuObj = m_ImcuDao.GetById(mcuAssignment[j].MCUID);
                        string mcuAdminEmail = "";
                        if (mcuObj != null)
                        {
                            user = null;
                            user = m_vrmUserDAO.GetByUserId(mcuObj.Admin);
                            if (user != null)
                                mcuAdminEmail = user.Email;

                            outXML.Append("<mcu>");
                            outXML.Append("<mcuID>" + mcuAssignment[j].MCUID + "</mcuID>");
                            outXML.Append("<mcuName>" + mcuObj.BridgeName + "</mcuName>");
                            outXML.Append("<mcuAdminId>" + mcuObj.Admin + "</mcuAdminId>");
                            outXML.Append("<mcuAdminEmail>" + mcuAdminEmail + "</mcuAdminEmail>");
                            outXML.Append("<mcuTimezone>" + timeZone.GetTimeZoneList().Where(tz => tz.TimeZoneID == mcuObj.timezone).FirstOrDefault().TimeZone + "</mcuTimezone>");
                            outXML.Append("<mcuLoadBalance>" + mcuAssignment[j].LoadBalance + "</mcuLoadBalance>");
                            outXML.Append("<mcuOverflow>" + mcuAssignment[j].Overflow + "</mcuOverflow>");
                            outXML.Append("<mcuLeadMCU>" + mcuAssignment[j].LeadMCU + "</mcuLeadMCU>");
                            outXML.Append("</mcu>");
                        }
                    }
                    outXML.Append("</mcus>");
                    outXML.Append("</group>");
                }
                outXML.Append("</groups>");
                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region SearchMCUGroup
        public bool SearchMCUGroup(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder resultXML = null;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = new List<ICriterion>();
            List<ICriterion> subCriterionList = new List<ICriterion>();
            List<vrmMCU> includedMCUs = null;
            List<vrmMCUGrpAssignment> grpAssignment = null;
            int userid = 0;
            string orgid = "", groupName = "", mcunames = "", description = "";
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//SearchMCUGroup/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userid);

                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                vrmUser user = m_vrmUserDAO.GetByUserId(userid);
                checkHardwareAuthority hasAthority = new checkHardwareAuthority(user.MenuMask);
                if (!hasAthority.hasMCUsLoadBalance)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//SearchMCUGroup/organizationID");
                if (node != null)
                    orgid = node.InnerText.Trim();
                organizationID = defaultOrgId;
                int.TryParse(orgid, out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//SearchMCUGroup/groupName");
                if (node != null)
                    groupName = node.InnerText.Trim();

                node = xd.SelectSingleNode("//SearchMCUGroup/includedMCU");
                if (node != null)
                    mcunames = node.InnerText.Trim(); //need to discuss

                node = xd.SelectSingleNode("//SearchMCUGroup/description");
                if (node != null)
                    description = node.InnerText.Trim();

                criterionList.Add(Expression.Eq("OrgID", organizationID));
                if (groupName.Length > 0)
                    criterionList.Add(Expression.Like("GroupName", "%%" + groupName + "%%").IgnoreCase());

                if (description.Length > 0)
                    criterionList.Add(Expression.Like("Description", "%%" + description + "%%").IgnoreCase());

                if (mcunames.Length > 0)
                {
                    if (subCriterionList == null)
                        subCriterionList = new List<ICriterion>();
                    if( m_ImcuDao == null)
                        m_ImcuDao = m_hardwareDAO.GetMCUDao();

                    subCriterionList.Add(Expression.Like("BridgeName", "%%" + mcunames + "%%").IgnoreCase());
                    subCriterionList.Add(Expression.Eq("VirtualBridge", 1));
                    includedMCUs = m_ImcuDao.GetByCriteria(subCriterionList);

                    if (includedMCUs != null && includedMCUs.Count > 0)
                    {
                        if(m_IMCUGrpAssignmentDao == null)
                            m_IMCUGrpAssignmentDao = m_hardwareDAO.GetMCUGrpAssignmentDao();

                          grpAssignment =   m_IMCUGrpAssignmentDao.GetAssingmentbyMCUIDS(includedMCUs.Select(mcu=> mcu.BridgeID).ToArray());

                        if (grpAssignment != null)
                            criterionList.Add(Expression.In("MCUGroupID",grpAssignment.Select(grp=>grp.MCUGroupID).ToArray()));
                        else
                            criterionList.Add(Expression.Eq("MCUGroupID", 0));
                    }
                    else
                        criterionList.Add(Expression.Eq("MCUGroupID", 0));
 

                }

                resultXML = new StringBuilder();
                resultXML.Append("<groups>");

                List<vrmMCUGroupDetails> mcuGroups = m_IMCUGroupDetailsDao.GetByCriteria(criterionList);
                criterionList = null;
                vrmMCU mcuObj = null;
                string mcuAdminEmail = "";
                string mcuString = "";
                for (int i = 0; i < mcuGroups.Count; i++)
                {
                    resultXML.Append("<group>");
                    resultXML.Append("<groupID>" + mcuGroups[i].MCUGroupID + "</groupID>");
                    resultXML.Append("<groupName>" + mcuGroups[i].GroupName + "</groupName>");
                    resultXML.Append("<description>" + mcuGroups[i].Description + "</description>");
                    resultXML.Append("<loadBalance>" + mcuGroups[i].LoadBalance + "</loadBalance>");
                    resultXML.Append("<Pooled>" + mcuGroups[i].Pooled + "</Pooled>");
                    resultXML.Append("<LCR>" + mcuGroups[i].LCR + "</LCR>");

                    criterionList = null;
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("MCUGroupID", mcuGroups[i].MCUGroupID));
                    List<vrmMCUGrpAssignment> mcuAssignment = m_IMCUGrpAssignmentDao.GetByCriteria(criterionList);

                    resultXML.Append("<mcus>");
                    mcuString = "";
                    for (int j = 0; j < mcuAssignment.Count; j++)
                    {
                        mcuObj = null;
                        mcuObj = m_ImcuDao.GetById(mcuAssignment[j].MCUID);

                        if (mcuObj != null)
                        {
                            user = null;
                            mcuAdminEmail = "";
                            user = m_vrmUserDAO.GetByUserId(mcuObj.Admin);
                            if (user != null)
                                mcuAdminEmail = user.Email;

                            resultXML.Append("<mcu>");
                            resultXML.Append("<mcuID>" + mcuAssignment[j].MCUID + "</mcuID>");
                            resultXML.Append("<mcuName>" + mcuObj.BridgeName + "</mcuName>");
                            resultXML.Append("<mcuAdminId>" + mcuObj.Admin + "</mcuAdminId>");
                            resultXML.Append("<mcuAdminEmail>" + mcuAdminEmail + "</mcuAdminEmail>");
                            resultXML.Append("<mcuTimezone>" + timeZone.GetTimeZoneList().Where(tz => tz.TimeZoneID == mcuObj.timezone).FirstOrDefault().TimeZone + "</mcuTimezone>");
                            resultXML.Append("<mcuLoadBalance>" + mcuAssignment[j].LoadBalance + "</mcuLoadBalance>");
                            resultXML.Append("<mcuOverflow>" + mcuAssignment[j].Overflow + "</mcuOverflow>");
                            resultXML.Append("<mcuLeadMCU>" + mcuAssignment[j].LeadMCU + "</mcuLeadMCU>");
                            resultXML.Append("</mcu>");

                            if (j == 0)
                                mcuString = mcuObj.BridgeName + "!!" + mcuObj.BridgeAddress + "!!" + user.Email + "||";
                            else
                                mcuString += mcuObj.BridgeName + "!!" + mcuObj.BridgeAddress + "!!" + user.Email + "||";

                        }
                    }
                    resultXML.Append("</mcus>");
                    resultXML.Append("<mcustring>" + mcuString + "</mcustring>");
                    resultXML.Append("</group>");
                }
                resultXML.Append("</groups>");
                obj.outXml = resultXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("SearchMCUGroup", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("SearchMCUGroup", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region DeleteMCUGroup
        /// <summary>
        /// DeleteMCUGroup
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool DeleteMCUGroup(ref vrmDataObject obj)
        {
            try
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;
                int groupID = 0, userid = 0;
                myVRMException myVRMEx = null;


                node = xd.SelectSingleNode("//DeleteMCUGroup/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userid);

                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                vrmUser user = m_vrmUserDAO.GetByUserId(userid);
                checkHardwareAuthority hasAthority = new checkHardwareAuthority(user.MenuMask);
                if (!hasAthority.hasMCUsLoadBalance)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//DeleteMCUGroup/groupID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out groupID);

                criterionList.Add(Expression.Eq("MCUGroupID", groupID));
                List<vrmMCUGrpAssignment> mcuGrpAssgmt = m_IMCUGrpAssignmentDao.GetByCriteria(criterionList);

                for (int i = 0; i < mcuGrpAssgmt.Count; i++)
                    m_IMCUGrpAssignmentDao.Delete(mcuGrpAssgmt[i]);

                vrmMCUGroupDetails GrpDetail = m_IMCUGroupDetailsDao.GetById(groupID);
                if (GrpDetail != null)
                    m_IMCUGroupDetailsDao.Delete(GrpDetail);

                return true;
            }
            catch (Exception ex)
            {
                m_log.Error("DeleteMCUGroup" + ex.Message);
                obj.outXml = "";
                return false;
            }
        }
        #endregion

        #region GetMCUGroups
        /// <summary>
        /// GetMCUGroups
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetMCUGroups(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder outXML = null;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = null;
            int userid = 0, sortby = 0, mcugrpid = 0;
            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//GetMCUGroups/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userid);

                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                vrmUser user = m_vrmUserDAO.GetByUserId(userid);
                checkHardwareAuthority hasAthority = new checkHardwareAuthority(user.MenuMask);
                if (!hasAthority.hasMCUsLoadBalance)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = defaultOrgId;
                node = xd.SelectSingleNode("//GetMCUGroups/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                node = xd.SelectSingleNode("//GetMCUGroups/groupID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out mcugrpid);

                node = xd.SelectSingleNode("//GetMCUGroups/sortBy");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out sortby);

                //Fetch mcu group basic detaisl
                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("OrgID", organizationID));
                List<vrmMCUGroupDetails> mcuGroups = m_IMCUGroupDetailsDao.GetByCriteria(criterionList);

                vrmMCU mcuObj = null;
                string strMCUSelected = "";
                string mcuAdminEmail = "";
                outXML = new StringBuilder();
                outXML.Append("<groups>");
                for (int i = 0; i < mcuGroups.Count; i++)
                {
                    outXML.Append("<group>");
                    outXML.Append("<organizationID>" + mcuGroups[i].OrgID + "</organizationID>");
                    outXML.Append("<groupID>" + mcuGroups[i].MCUGroupID + "</groupID>");
                    outXML.Append("<groupName>" + mcuGroups[i].GroupName.Trim() + "</groupName>");
                    outXML.Append("<description>" + mcuGroups[i].Description + "</description>");
                    outXML.Append("<pooled>" + mcuGroups[i].Pooled + "</pooled>");
                    outXML.Append("<lcr>" + mcuGroups[i].LCR + "</lcr>");
                    outXML.Append("<loadBalance>" + mcuGroups[i].LoadBalance + "</loadBalance>");

                    criterionList = null;
                    criterionList = new List<ICriterion>();
                    criterionList.Add(Expression.Eq("MCUGroupID", mcuGroups[i].MCUGroupID));
                    List<vrmMCUGrpAssignment> mcuAssignment = m_IMCUGrpAssignmentDao.GetByCriteria(criterionList);

                    outXML.Append("<mcus>");
                    user = null;
                    mcuObj = null;
                    strMCUSelected = "";
                    for (int j = 0; j < mcuAssignment.Count; j++)
                    {
                        mcuObj = null;
                        mcuObj = m_ImcuDao.GetById(mcuAssignment[j].MCUID);

                        if (mcuObj != null)
                        {
                            user = null;
                            user = m_vrmUserDAO.GetByUserId(mcuObj.Admin);
                            if (user != null)
                                mcuAdminEmail = user.Email;

                            outXML.Append("<mcu>");
                            outXML.Append("<mcuID>" + mcuAssignment[j].MCUID + "</mcuID>");
                            outXML.Append("<mcuName>" + mcuObj.BridgeName + "</mcuName>");
                            outXML.Append("<mcuAdminId>" + mcuObj.Admin + "</mcuAdminId>");
                            outXML.Append("<mcuAdminEmail>" + mcuAdminEmail + "</mcuAdminEmail>");
                            outXML.Append("<mcuTimezone>" + timeZone.GetTimeZoneList().Where(tz => tz.TimeZoneID == mcuObj.timezone).FirstOrDefault().TimeZone + "</mcuTimezone>");
                            outXML.Append("<mcuLoadBalance>" + mcuAssignment[j].LoadBalance + "</mcuLoadBalance>");
                            outXML.Append("<mcuOverflow>" + mcuAssignment[j].Overflow + "</mcuOverflow>");
                            outXML.Append("<mcuLeadMCU>" + mcuAssignment[j].LeadMCU + "</mcuLeadMCU>");
                            outXML.Append("</mcu>");

                            if (j == 0)
                                strMCUSelected = mcuObj.BridgeName + "!!" + mcuObj.BridgeAddress + "!!" + user.Email + "||";
                            else
                                strMCUSelected += mcuObj.BridgeName + "!!" + mcuObj.BridgeAddress + "!!" + user.Email + "||";

                        }
                    }
                    outXML.Append("</mcus>");
                    outXML.Append("<mcustring>" + strMCUSelected + "</mcustring>");

                    outXML.Append("</group>");
                }
                outXML.Append("</groups>");
                obj.outXml = outXML.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("GetMCUGroups", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("GetMCUGroups", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetVirtualBridges
        //GetUnAssignedVirtualBridges
        public bool GetVirtualBridges(ref vrmDataObject obj)
        {
            bool bRet = true;
            StringBuilder outXML = null;
            myVRMException myVRMEx = new myVRMException();
            List<ICriterion> criterionList = null;
            int userid = 0;

            try
            {
                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node;

                node = xd.SelectSingleNode("//login/userID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out userid);

                if (userid <= 0)
                {
                    myVRMEx = new myVRMException(201);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                vrmUser user = m_vrmUserDAO.GetByUserId(userid);
                checkHardwareAuthority hasAthority = new checkHardwareAuthority(user.MenuMask);
                if (!hasAthority.hasMCUsLoadBalance)
                {
                    myVRMEx = new myVRMException(422);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                organizationID = defaultOrgId;
                node = xd.SelectSingleNode("//login/organizationID");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out organizationID);

                if (organizationID < defaultOrgId)
                {
                    myVRMEx = new myVRMException(423);
                    obj.outXml = myVRMEx.FetchErrorMsg();
                    return false;
                }

                int activeGroupId = 2;
                node = xd.SelectSingleNode("//login/activeGroupId");
                if (node != null)
                    int.TryParse(node.InnerText.Trim(), out activeGroupId);

                string sqlfilter = "";

                if (activeGroupId > 0)
                    sqlfilter = "BridgeID not in (select distinct mcuid from MCU_GrpAssignList_D where MCUGroupID <>" + activeGroupId + ")";
                else
                    sqlfilter = "BridgeID not in (select distinct mcuid from MCU_GrpAssignList_D)";

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Gt("Status", 0));
                criterionList.Add(Expression.Eq("deleted", 0));
                criterionList.Add(Expression.Eq("VirtualBridge", 1));
                criterionList.Add(Expression.Or(Expression.Eq("orgId", organizationID), Expression.Eq("isPublic", 1))); // FB 1920
                criterionList.Add(Expression.Sql(sqlfilter));

                m_ImcuDao.addOrderBy(Order.Asc("ChainPosition"));
                m_ImcuDao.addOrderBy(Order.Asc("orgId"));
                List<vrmMCU> MCUList = m_ImcuDao.GetByCriteria(criterionList);
                m_ImcuDao.sledgeHammerReset();

                outXML = new StringBuilder();

                outXML.Append("<bridgeInfo>");
                outXML.Append("<bridges>");

                user = null;
                vrmInactiveUser iuser = null;
                for (int i = 0; i < MCUList.Count; i++ )
                {
                    outXML.Append("<bridge>");
                    outXML.Append("<id>" + MCUList[i].BridgeID.ToString() + "</id>");
                    outXML.Append("<organizationID>" + MCUList[i].orgId.ToString() + "</organizationID>");
                    outXML.Append("<isPublic>" + MCUList[i].isPublic.ToString() + "</isPublic>");

                    if (MCUList[i].isPublic == 1 && organizationID != 11) //Default org 11
                        outXML.Append("<name>" + MCUList[i].BridgeName + "(*)" + "</name>");
                    else
                        outXML.Append("<name>" + MCUList[i].BridgeName + "</name>");

                    user = m_vrmUserDAO.GetByUserId(MCUList[i].Admin);
                    if (user != null)
                    {
                        outXML.Append("<administrator>" + user.Email + "</administrator>");
                        outXML.Append("<userstate>A</userstate>");
                    }
                    else
                    {
                        iuser = m_vrmIUserDAO.GetByUserId(MCUList[i].Admin);
                        if (iuser != null)
                        {
                            outXML.Append("<administrator>" + iuser.Email + "</administrator>");
                            outXML.Append("<userstate>I</userstate>");
                        }
                        else
                        {
                            outXML.Append("<administrator>User does not exist</administrator>");
                            outXML.Append("<userstate>D</userstate>");
                        }
                    }
                    outXML.Append("<timeZone>" + timeZone.GetTimeZoneList().Where(tz => tz.TimeZoneID == MCUList[i].timezone).FirstOrDefault().StandardName + "</timeZone>");
                    outXML.Append("</bridge>");
                }
                outXML.Append("</bridges>");
                outXML.Append("</bridgeInfo>");

                obj.outXml = outXML.ToString();

            }
            catch (myVRMException e)
            {
                m_log.Error("GetVirtualBridges", e);
                obj.outXml = e.FetchErrorMsg();
                bRet = false;
            }
            catch (Exception e)
            {
                m_log.Error("GetVirtualBridges", e);
                obj.outXml = "";
                bRet = false;
            }
            return bRet;
        }
        #endregion

        #region GetMCUGroupusage
        /// <summary>
        /// GetMCUGroupusage ZD 100040
        /// </summary>
        /// <param name="confDate"></param>
        /// <param name="Duration"></param>
        /// <param name="ConfID"></param>
        /// <param name="InstanceID"></param>
        /// <param name="iMCUID"></param>
        /// <param name="epsServiceList"></param>
        /// <param name="lstMCUGroupAssignment"></param>
        /// <param name="lstFailoverLoabalanceMCU"></param>
        /// <param name="bSkipCheck"></param>
        /// <returns></returns>
        public bool GetMCUGroupusage(DateTime confDate, int Duration, int ConfID, int InstanceID, ref int iMCUID, ref List<GetEndPointService> epsServiceList, ref List<vrmMCUGrpAssignment> lstMCUGroupAssignment, ref List<VRMLoadBalancedMCU> lstFailoverLoabalanceMCU, bool bSkipCheck)
        {
            string status = String.Format("{0,3:D},{1,3:D},{2,3:D},{3,3:D}",
                    vrmConfStatus.Scheduled,
                    vrmConfStatus.Pending,
                    vrmConfStatus.Ongoing,
                    vrmConfStatus.OnMCU);

            string hql = "";

            DataSet dsRooms = null;
            DataSet dsUsers = null;
            Array resolutionsEnumArray = null;
            VRMLoadBalancedMCU baseLoabalanceMCU = null;
            vrmMCUPortResolution baseMCUResolution = null;
            int resolutionCntUsed = 0;
            VRMLoadBalancedMCU failoverLoabalanceMCU = null;
            vrmMCUPortResolution failoverMCUResolution = null;
            DataRow[] eptsRows = null;
            int MCUID = 0;
            try
            {
                resolutionsEnumArray = Enum.GetValues(typeof(Resolution));
                MCUID = iMCUID;

                if (m_IMCUPortResolutionDao == null)
                    m_IMCUPortResolutionDao = m_hardwareDAO.GetMCUPortResolutionDao();

                if (!bSkipCheck)
                {
                    hql = " Select Resolution , room_tbl.bridgeid, ";
                    hql += " room_tbl.RoomID , conf_mst.confnumname ";
                    hql += " from Conf_Conference_D conf_mst ";
                    hql += " Inner join Conf_Room_D room_tbl ";
                    hql += " on conf_mst.confnumname=room_tbl.confuId ";
                    hql += " inner join Ept_List_D ept_tbl";
                    hql += " on room_tbl.endpointId=ept_tbl.endpointId and ";
                    hql += " room_tbl.profileId =ept_tbl.profileId";
                    hql += " WHERE(dateadd(minute, " + Duration.ToString() + ",'";
                    hql += confDate.ToString("d") + " " + confDate.ToString("t") + "')) >= conf_mst.confdate ";
                    hql += " AND '" + confDate.ToString("d") + " " + confDate.ToString("t") + "'";
                    hql += " <= (dateadd(minute, conf_mst.duration, conf_mst.confdate ))";
                    hql += "AND conf_mst.deleted = 0 AND conf_mst.status IN( " + status + ")";
                    hql += "AND  conf_mst.Permanent = 0  ";
                    if (epsServiceList != null && epsServiceList.Count > 0)
                    {
                      hql += " AND ( CONVERT(varchar(10),conf_mst.confid) + ',' + CONVERT(varchar(10),conf_mst.instanceid) ) <> '" + ConfID + "," + InstanceID + "'";
                    }
                    hql += "AND conf_mst.conftype != " + vrmConfType.RooomOnly;
                    hql += "AND conf_mst.conftype != " + vrmConfType.Template;
                    hql += "AND conf_mst.confnumname  in ( select confuID from Conf_Bridge_D where bridgeID in ( ";
                    hql += "(select MCUID from MCU_GrpAssignList_D G, MCU_GrpDetails_D D where G.MCUGroupID = D.MCUGroupID AND ";
                    hql += " D.MCUGroupID = (select MCUGroupID from MCU_GrpAssignList_D where MCUID = '" + MCUID + "') AND D.LoadBalance = 1) ) ) ";



                    if (m_bridgelayer == null)
                        m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                    if (hql != "")
                        dsRooms = m_bridgelayer.ExecuteDataSet(hql);


                    for (int mcus = 0; mcus < lstMCUGroupAssignment.Count; mcus++)
                    {
                        failoverLoabalanceMCU = null;
                        failoverMCUResolution = null;
                        failoverLoabalanceMCU = new DataLayer.VRMLoadBalancedMCU();
                        failoverLoabalanceMCU.BaseMCU = m_ImcuDao.GetById(lstMCUGroupAssignment[mcus].MCUID);
                        failoverLoabalanceMCU.AvailablePortResolution = m_IMCUPortResolutionDao.GetResolutionbyMCUID(lstMCUGroupAssignment[mcus].MCUID);
                        failoverLoabalanceMCU.LeadMCU = lstMCUGroupAssignment[mcus].LeadMCU;


                        foreach (Resolution val in resolutionsEnumArray)
                        {
                            resolutionCntUsed = 0;
                            if (dsRooms != null && dsRooms.Tables.Count > 0)
                            {
                                eptsRows = null;
                                eptsRows = dsRooms.Tables[0].Select(" Resolution = " + (int)val + " AND bridgeid = " + failoverLoabalanceMCU.BaseMCU.BridgeID);
                                if (eptsRows != null)
                                    resolutionCntUsed = eptsRows.Length;
                                failoverMCUResolution = failoverLoabalanceMCU.AvailablePortResolution.Where(res => res.ResolutionId == (int)val).FirstOrDefault();
                                if (failoverMCUResolution != null)
                                    failoverMCUResolution.Port = failoverMCUResolution.Port - resolutionCntUsed;
                                /*else 
                                {// Not needed as value will be 0
                                    failoverMCUResolution = new DataLayer.vrmMCUPortResolution();
                                    failoverMCUResolution.BridgeId = failoverLoabalanceMCU.BaseMCU.BridgeID;
                                    failoverMCUResolution.ResolutionId = (int)val;
                                    failoverMCUResolution.Port = -resolutionCntUsed;
                                    failoverMCUResolution.ID = failoverLoabalanceMCU.AvailablePortResolution.Count + 1;
                                    failoverLoabalanceMCU.AvailablePortResolution.Add(failoverMCUResolution);
                                }*/

                            }

                        }

                        if (lstFailoverLoabalanceMCU == null)
                            lstFailoverLoabalanceMCU = new List<DataLayer.VRMLoadBalancedMCU>();
                        if (failoverLoabalanceMCU != null)
                            lstFailoverLoabalanceMCU.Add(failoverLoabalanceMCU);
                    }


                    hql = " Select 5 as Resolution , User_tbl.bridgeid, User_tbl.UserID, conf_mst.confnumname ";
                    hql += " from Conf_Conference_D conf_mst ";
                    hql += " Inner join Conf_User_D User_tbl ";
                    hql += " on User_tbl.confuId = conf_mst.confnumname ";
                    hql += " WHERE(dateadd(minute, " + Duration.ToString() + ",'";
                    hql += confDate.ToString("d") + " " + confDate.ToString("t") + "')) >= conf_mst.confdate ";
                    hql += " AND '" + confDate.ToString("d") + " " + confDate.ToString("t") + "'";
                    hql += " <= (dateadd(minute, conf_mst.duration, conf_mst.confdate ))";
                    hql += "AND conf_mst.deleted = 0 AND conf_mst.status IN( " + status + ")";
                    hql += "AND conf_mst.Permanent = 0 ";
                    if (epsServiceList != null && epsServiceList.Count > 0)
                    {
                        hql += " AND ( CONVERT(varchar(10),conf_mst.confid) + ',' + CONVERT(varchar(10),conf_mst.instanceid) ) <> '" + ConfID + "," + InstanceID + "'";
                    }
                    hql += "AND conf_mst.conftype != " + vrmConfType.RooomOnly;
                    hql += "AND conf_mst.conftype != " + vrmConfType.Template;
                    hql += "AND conf_mst.confnumname  in ( select confuID from Conf_Bridge_D where bridgeID in ( ";
                    hql += "(select MCUID from MCU_GrpAssignList_D G, MCU_GrpDetails_D D where G.MCUGroupID = D.MCUGroupID AND ";
                    hql += " D.MCUGroupID = (select MCUGroupID from MCU_GrpAssignList_D where MCUID = '" + MCUID + "') AND D.LoadBalance = 1) ) ) ";
                    hql += " AND User_tbl.Invitee =1  ";

                    if (m_bridgelayer == null)
                        m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                    if (hql != "")
                        dsUsers = m_bridgelayer.ExecuteDataSet(hql);


                    if (lstFailoverLoabalanceMCU != null)
                    {

                        for (int balanceCnt = 0; balanceCnt < lstFailoverLoabalanceMCU.Count; balanceCnt++)
                        {
                            resolutionCntUsed = 0;
                            if (dsUsers != null && dsUsers.Tables.Count > 0)
                            {
                                eptsRows = null;
                                failoverMCUResolution = null;
                                eptsRows = dsUsers.Tables[0].Select(" bridgeid = " + lstFailoverLoabalanceMCU[balanceCnt].BaseMCU.BridgeID);
                                if (eptsRows != null)
                                    resolutionCntUsed = eptsRows.Length;
                            }

                            failoverMCUResolution = lstFailoverLoabalanceMCU[balanceCnt].AvailablePortResolution.Where(res => res.ResolutionId == (int)Resolution.SD).FirstOrDefault();
                            if (failoverMCUResolution != null)
                                failoverMCUResolution.Port = failoverMCUResolution.Port - resolutionCntUsed;
                            /*else
                            { // Not needed as value will be 0
                                failoverMCUResolution = new DataLayer.vrmMCUPortResolution();
                                failoverMCUResolution.BridgeId = failoverLoabalanceMCU.BaseMCU.BridgeID;
                                failoverMCUResolution.ResolutionId = (int)Resolution.SD;
                                failoverMCUResolution.Port = -resolutionCntUsed;
                                failoverMCUResolution.ID = failoverLoabalanceMCU.AvailablePortResolution.Count + 1;
                                failoverLoabalanceMCU.AvailablePortResolution.Add(failoverMCUResolution);
                            }*/
                        }
                    }
                }

                if (epsServiceList != null && epsServiceList.Count > 0)
                {
                    for (int balanceCnt = 0; balanceCnt < lstFailoverLoabalanceMCU.Count; balanceCnt++)
                    {

                        baseLoabalanceMCU = lstFailoverLoabalanceMCU[balanceCnt];
                        if (baseLoabalanceMCU != null)
                        {

                            foreach (Resolution val in resolutionsEnumArray)
                            {
                                resolutionCntUsed = 0;
                                resolutionCntUsed = epsServiceList.Where(eps => eps.Resolution == (int)val && eps.bridgeid == MCUID && eps.Invitee == 1).ToList().Count();
                                baseMCUResolution = baseLoabalanceMCU.AvailablePortResolution.Where(res => res.ResolutionId == (int)val).FirstOrDefault();
                                if (baseMCUResolution != null)
                                    baseMCUResolution.Port = baseMCUResolution.Port - resolutionCntUsed;

                            }
                        }


                    }

                }

                if (lstFailoverLoabalanceMCU.Where(lbl => lbl.BaseMCU.BridgeID == MCUID).First().AvailablePortResolution.Select(prts => prts.Port).Min() >= 0)
                    return true;

                
                if (lstFailoverLoabalanceMCU.Count > 0)
                {
                    lstFailoverLoabalanceMCU = lstFailoverLoabalanceMCU.OrderByDescending(lbl => lbl.LeadMCU).ToList();
                    for (int balanceCnt = 0; balanceCnt < lstFailoverLoabalanceMCU.Count; balanceCnt++)
                    {
                        if (lstFailoverLoabalanceMCU[balanceCnt].BaseMCU.BridgeID != MCUID)
                        {
                            if (lstFailoverLoabalanceMCU[balanceCnt].AvailablePortResolution.Select(prts => prts.Port).Min() >= 0)
                            {
                                iMCUID = lstFailoverLoabalanceMCU[balanceCnt].BaseMCU.BridgeID;
                                return true;
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            finally
            {
                try
                {
                    if (m_bridgelayer != null)
                        m_bridgelayer.CloseConnection();
                    m_bridgelayer = null;

                }
                catch (Exception)
                {


                }
            }
        }


        public bool GetMCUStandaloneUsage(DateTime confDate, int Duration, int ConfID, int InstanceID, ref int iMCUID, ref List<GetEndPointService> epsServiceList, ref List<VRMLoadBalancedMCU> lstFailoverLoabalanceMCU)
        {
            string status = String.Format("{0,3:D},{1,3:D},{2,3:D},{3,3:D}",
                    vrmConfStatus.Scheduled,
                    vrmConfStatus.Pending,
                    vrmConfStatus.Ongoing,
                    vrmConfStatus.OnMCU);

            string hql = "";

            DataSet dsRooms = null;
            DataSet dsUsers = null;
            Array resolutionsEnumArray = null;
            VRMLoadBalancedMCU baseLoabalanceMCU = null;
            vrmMCUPortResolution baseMCUResolution = null;
            int resolutionCntUsed = 0;
            VRMLoadBalancedMCU failoverLoabalanceMCU = null;
            vrmMCUPortResolution failoverMCUResolution = null;
            DataRow[] eptsRows = null;
            int MCUID = 0;
            try
            {
                resolutionsEnumArray = Enum.GetValues(typeof(Resolution));
                MCUID = iMCUID;

                if (m_IMCUPortResolutionDao == null)
                    m_IMCUPortResolutionDao = m_hardwareDAO.GetMCUPortResolutionDao();


                {
                    hql = " Select Resolution , room_tbl.bridgeid, ";
                    hql += " room_tbl.RoomID , conf_mst.confnumname ";
                    hql += " from Conf_Conference_D conf_mst ";
                    hql += " Inner join Conf_Room_D room_tbl ";
                    hql += " on conf_mst.confnumname=room_tbl.confuId ";
                    hql += " inner join Ept_List_D ept_tbl";
                    hql += " on room_tbl.endpointId=ept_tbl.endpointId and ";
                    hql += " room_tbl.profileId =ept_tbl.profileId";
                    hql += " WHERE(dateadd(minute, " + Duration.ToString() + ",'";
                    hql += confDate.ToString("d") + " " + confDate.ToString("t") + "')) >= conf_mst.confdate ";
                    hql += " AND '" + confDate.ToString("d") + " " + confDate.ToString("t") + "'";
                    hql += " <= (dateadd(minute, conf_mst.duration, conf_mst.confdate ))";
                    hql += "AND conf_mst.deleted = 0 AND conf_mst.status IN( " + status + ")";
                    hql += "AND  conf_mst.Permanent = 0  ";
                    if (epsServiceList != null && epsServiceList.Count > 0)
                    {
                        hql += " AND conf_mst.confid <> '" + ConfID + "'";
                        hql += " AND  conf_mst.instanceid <> '" + InstanceID + "'";
                    }
                    hql += "AND conf_mst.conftype != " + vrmConfType.RooomOnly;
                    hql += "AND conf_mst.conftype != " + vrmConfType.Template;
                    hql += "AND conf_mst.confnumname  in ( select confuID from Conf_Bridge_D where bridgeID = '";
                    hql += MCUID + "') ";



                    if (m_bridgelayer == null)
                        m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                    if (hql != "")
                        dsRooms = m_bridgelayer.ExecuteDataSet(hql);



                    failoverLoabalanceMCU = null;
                    failoverMCUResolution = null;
                    failoverLoabalanceMCU = new DataLayer.VRMLoadBalancedMCU();
                    failoverLoabalanceMCU.BaseMCU = m_ImcuDao.GetById(MCUID);
                    failoverLoabalanceMCU.AvailablePortResolution = m_IMCUPortResolutionDao.GetResolutionbyMCUID(MCUID);
                    failoverLoabalanceMCU.LeadMCU = 0;


                    foreach (Resolution val in resolutionsEnumArray)
                    {
                        resolutionCntUsed = 0;
                        if (dsRooms != null && dsRooms.Tables.Count > 0)
                        {
                            eptsRows = null;
                            eptsRows = dsRooms.Tables[0].Select(" Resolution = " + (int)val + " AND bridgeid = " + failoverLoabalanceMCU.BaseMCU.BridgeID);
                            if (eptsRows != null)
                                resolutionCntUsed = eptsRows.Length;
                            failoverMCUResolution = failoverLoabalanceMCU.AvailablePortResolution.Where(res => res.ResolutionId == (int)val).FirstOrDefault();
                            if (failoverMCUResolution != null)
                                failoverMCUResolution.Port = failoverMCUResolution.Port - resolutionCntUsed;
                            /*else 
                            {// Not needed as value will be 0
                                failoverMCUResolution = new DataLayer.vrmMCUPortResolution();
                                failoverMCUResolution.BridgeId = failoverLoabalanceMCU.BaseMCU.BridgeID;
                                failoverMCUResolution.ResolutionId = (int)val;
                                failoverMCUResolution.Port = -resolutionCntUsed;
                                failoverMCUResolution.ID = failoverLoabalanceMCU.AvailablePortResolution.Count + 1;
                                failoverLoabalanceMCU.AvailablePortResolution.Add(failoverMCUResolution);
                            }*/

                        }

                    }

                    if (lstFailoverLoabalanceMCU == null)
                        lstFailoverLoabalanceMCU = new List<DataLayer.VRMLoadBalancedMCU>();
                    if (failoverLoabalanceMCU != null)
                        lstFailoverLoabalanceMCU.Add(failoverLoabalanceMCU);



                    hql = " Select 5 as Resolution , User_tbl.bridgeid, User_tbl.UserID, conf_mst.confnumname ";
                    hql += " from Conf_Conference_D conf_mst ";
                    hql += " Inner join Conf_User_D User_tbl ";
                    hql += " on User_tbl.confuId = conf_mst.confnumname ";
                    hql += " WHERE(dateadd(minute, " + Duration.ToString() + ",'";
                    hql += confDate.ToString("d") + " " + confDate.ToString("t") + "')) >= conf_mst.confdate ";
                    hql += " AND '" + confDate.ToString("d") + " " + confDate.ToString("t") + "'";
                    hql += " <= (dateadd(minute, conf_mst.duration, conf_mst.confdate ))";
                    hql += "AND conf_mst.deleted = 0 AND conf_mst.status IN( " + status + ")";
                    hql += "AND conf_mst.Permanent = 0 ";
                    if (epsServiceList != null && epsServiceList.Count > 0)
                    {
                        hql += " AND conf_mst.confid <> '" + ConfID + "'";
                        hql += " AND  conf_mst.instanceid <> '" + InstanceID + "'";
                    }
                    hql += "AND conf_mst.conftype != " + vrmConfType.RooomOnly;
                    hql += "AND conf_mst.conftype != " + vrmConfType.Template;
                    hql += "AND conf_mst.confnumname  in ( select confuID from Conf_Bridge_D where bridgeID = '";
                    hql += MCUID + "') ";
                    hql += " AND User_tbl.Invitee =1  ";

                    if (m_bridgelayer == null)
                        m_bridgelayer = new ns_SqlHelper.SqlHelper(m_configPath);

                    if (hql != "")
                        dsUsers = m_bridgelayer.ExecuteDataSet(hql);


                    if (lstFailoverLoabalanceMCU != null)
                    {

                        for (int balanceCnt = 0; balanceCnt < lstFailoverLoabalanceMCU.Count; balanceCnt++)
                        {
                            resolutionCntUsed = 0;
                            if (dsUsers != null && dsUsers.Tables.Count > 0)
                            {
                                eptsRows = null;
                                failoverMCUResolution = null;
                                eptsRows = dsUsers.Tables[0].Select(" bridgeid = " + lstFailoverLoabalanceMCU[balanceCnt].BaseMCU.BridgeID);
                                if (eptsRows != null)
                                    resolutionCntUsed = eptsRows.Length;
                            }

                            failoverMCUResolution = lstFailoverLoabalanceMCU[balanceCnt].AvailablePortResolution.Where(res => res.ResolutionId == (int)Resolution.SD).FirstOrDefault();
                            if (failoverMCUResolution != null)
                                failoverMCUResolution.Port = failoverMCUResolution.Port - resolutionCntUsed;
                            /*else
                            { // Not needed as value will be 0
                                failoverMCUResolution = new DataLayer.vrmMCUPortResolution();
                                failoverMCUResolution.BridgeId = failoverLoabalanceMCU.BaseMCU.BridgeID;
                                failoverMCUResolution.ResolutionId = (int)Resolution.SD;
                                failoverMCUResolution.Port = -resolutionCntUsed;
                                failoverMCUResolution.ID = failoverLoabalanceMCU.AvailablePortResolution.Count + 1;
                                failoverLoabalanceMCU.AvailablePortResolution.Add(failoverMCUResolution);
                            }*/
                        }
                    }
                }

                if (epsServiceList != null && epsServiceList.Count > 0)
                {
                    for (int balanceCnt = 0; balanceCnt < lstFailoverLoabalanceMCU.Count; balanceCnt++)
                    {

                        baseLoabalanceMCU = lstFailoverLoabalanceMCU[balanceCnt];
                        if (baseLoabalanceMCU != null)
                        {

                            foreach (Resolution val in resolutionsEnumArray)
                            {
                                resolutionCntUsed = 0;
                                resolutionCntUsed = epsServiceList.Where(eps => eps.Resolution == (int)val && eps.bridgeid == MCUID && eps.Invitee == 1).ToList().Count();
                                baseMCUResolution = baseLoabalanceMCU.AvailablePortResolution.Where(res => res.ResolutionId == (int)val).FirstOrDefault();
                                if (baseMCUResolution != null)
                                    baseMCUResolution.Port = baseMCUResolution.Port - resolutionCntUsed;

                            }
                        }


                    }

                }

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
            finally
            {
                try
                {
                    if (m_bridgelayer != null)
                        m_bridgelayer.CloseConnection();
                    m_bridgelayer = null;

                }
                catch (Exception)
                {
                    
                   
                }
            }
            return true;
        }

        #endregion
        //ZD 100040 Smart MCU Ends
		//ZD 104256 Starts
        #region GetMCUPoolOrders
        /// <summary>
        /// GetMCUPoolOrders
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public bool GetMCUPoolOrders(ref vrmDataObject obj)
        {
            StringBuilder OutXml = new StringBuilder();

            List<ICriterion> criterionList = null;
            List<vrmMCUPoolOrders> lstPoolOrders = null;
            int MCUId = 0;
            vrmMCU MCU = null; 
            try
            {


                XmlDocument xd = new XmlDocument();
                xd.LoadXml(obj.inXml);
                XmlNode node = null;

                node = xd.SelectSingleNode("//GetMCUPoolOrders/MCUId");
                if (node != null)
                    Int32.TryParse(node.InnerText.Trim(), out MCUId);


                OutXml.Append("<GetMCUPoolOrders>");

                criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("MCUId", MCUId));
                if (MCUId > 0) //FB 2947
                {
                    lstPoolOrders = m_IMCUPoolOrderDao.GetByCriteria(criterionList);
                    m_IMcuProfilesDAO.clearOrderBy();
                    MCU = m_ImcuDao.GetById(MCUId);
                    OutXml.Append("<DefaultPoolOrder>" + MCU.PoolOrderID.ToString() + "</DefaultPoolOrder>");
                    if (lstPoolOrders.Count > 0)
                    {
                        for (int i = 0; i < lstPoolOrders.Count; i++)
                        {
                            OutXml.Append("<PoolOrder>");
                            OutXml.Append("<Id>" + lstPoolOrders[i].PoolOrderId.ToString() + "</Id>");
                            OutXml.Append("<Name>" + lstPoolOrders[i].PoolOrderName + "</Name>");
                            OutXml.Append("</PoolOrder>");
                        }
                    }
                }
                OutXml.Append("</GetMCUPoolOrders>");
                obj.outXml = OutXml.ToString();
            }
            catch (myVRMException e)
            {
                m_log.Error("vrmException", e);
                obj.outXml = e.FetchErrorMsg();
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                obj.outXml = "";
            }
            return true;

        }
        #endregion
        //ZD 104256 Ends
    }
}
