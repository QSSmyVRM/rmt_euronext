//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Globalization;
using System.Collections;
using System.Xml;
using System.CodeDom;

namespace myVRM.ExcelXmlWriter
{
	public sealed class DocumentProperties : IWriter, IReader, ICodeWriter
	{
		// Fields
		private string _author;
		private string _company;
		private DateTime _created = DateTime.MinValue;
		private string _lastAuthor;
		private DateTime _lastSaved = DateTime.MinValue;
		private string _manager;
		private string _subject;
		private string _title;
		private string _version;

		// Methods
		internal DocumentProperties()
		{
		}

		void ICodeWriter.WriteTo(CodeTypeDeclaration type, CodeMemberMethod method, CodeExpression targetObject)
		{
			Util.AddComment(method, "Properties");
			if (this._title != null)
			{
				Util.AddAssignment(method, targetObject, "Title", this._title);
			}
			if (this._subject != null)
			{
				Util.AddAssignment(method, targetObject, "Subject", this._subject);
			}
			if (this._author != null)
			{
				Util.AddAssignment(method, targetObject, "Author", this._author);
			}
			if (this._lastAuthor != null)
			{
				Util.AddAssignment(method, targetObject, "LastAuthor", this._lastAuthor);
			}
			if (this._created != DateTime.MinValue)
			{
				Util.AddAssignment(method, targetObject, "Created", this._created);
			}
			if (this._lastSaved != DateTime.MinValue)
			{
				Util.AddAssignment(method, targetObject, "LastSaved", this._lastSaved);
			}
			if (this._manager != null)
			{
				Util.AddAssignment(method, targetObject, "Manager", this._manager);
			}
			if (this._company != null)
			{
				Util.AddAssignment(method, targetObject, "Company", this._company);
			}
			if (this._version != null)
			{
				Util.AddAssignment(method, targetObject, "Version", this._version);
			}
		}

		void IReader.ReadXml(XmlElement element)
		{
			if (!IsElement(element))
			{
				throw new ArgumentException("Invalid element", "element");
			}
			foreach (XmlNode node in element.ChildNodes)
			{
				XmlElement element2 = node as XmlElement;
				if (element2 != null)
				{
					if (Util.IsElement(element2, "Title", "urn:schemas-microsoft-com:office:office"))
					{
						this._title = element2.InnerText;
					}
					else
					{
						if (Util.IsElement(element2, "Subject", "urn:schemas-microsoft-com:office:office"))
						{
							this._subject = element2.InnerText;
							continue;
						}
						if (Util.IsElement(element2, "Author", "urn:schemas-microsoft-com:office:office"))
						{
							this._author = element2.InnerText;
							continue;
						}
						if (Util.IsElement(element2, "LastAuthor", "urn:schemas-microsoft-com:office:office"))
						{
							this._lastAuthor = element2.InnerText;
							continue;
						}
						if (Util.IsElement(element2, "Created", "urn:schemas-microsoft-com:office:office"))
						{
							this._created = DateTime.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "LastSaved", "urn:schemas-microsoft-com:office:office"))
						{
							this._lastSaved = DateTime.Parse(element2.InnerText, CultureInfo.InvariantCulture);
							continue;
						}
						if (Util.IsElement(element2, "Manager", "urn:schemas-microsoft-com:office:office"))
						{
							this._manager = element2.InnerText;
							continue;
						}
						if (Util.IsElement(element2, "Company", "urn:schemas-microsoft-com:office:office"))
						{
							this._company = element2.InnerText;
							continue;
						}
						if (Util.IsElement(element2, "Version", "urn:schemas-microsoft-com:office:office"))
						{
							this._version = element2.InnerText;
						}
					}
				}
			}
		}

		void IWriter.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("o", "DocumentProperties", "urn:schemas-microsoft-com:office:office");
			if (this._title != null)
			{
				writer.WriteElementString("Title", "urn:schemas-microsoft-com:office:office", this._title);
			}
			if (this._subject != null)
			{
				writer.WriteElementString("Subject", "urn:schemas-microsoft-com:office:office", this._subject);
			}
			if (this._author != null)
			{
				writer.WriteElementString("Author", "urn:schemas-microsoft-com:office:office", this._author);
			}
			if (this._lastAuthor != null)
			{
				writer.WriteElementString("LastAuthor", "urn:schemas-microsoft-com:office:office", this._lastAuthor);
			}
			if (this._created != DateTime.MinValue)
			{
				writer.WriteElementString("Created", "urn:schemas-microsoft-com:office:office", this._created.ToString("s", CultureInfo.InvariantCulture));
			}
			if (this._lastSaved != DateTime.MinValue)
			{
				writer.WriteElementString("LastSaved", "urn:schemas-microsoft-com:office:office", this._lastSaved.ToString("s", CultureInfo.InvariantCulture));
			}
			if (this._manager != null)
			{
				writer.WriteElementString("Manager", "urn:schemas-microsoft-com:office:office", this._manager);
			}
			if (this._company != null)
			{
				writer.WriteElementString("Company", "urn:schemas-microsoft-com:office:office", this._company);
			}
			if (this._version != null)
			{
				writer.WriteElementString("Version", "urn:schemas-microsoft-com:office:office", this._version);
			}
			writer.WriteEndElement();
		}

		internal static bool IsElement(XmlElement element)
		{
			return Util.IsElement(element, "DocumentProperties", "urn:schemas-microsoft-com:office:office");
		}

		// Properties
		public string Author
		{
			get
			{
				return this._author;
			}
			set
			{
				this._author = value;
			}
		}

		public string Company
		{
			get
			{
				return this._company;
			}
			set
			{
				this._company = value;
			}
		}

		public DateTime Created
		{
			get
			{
				return this._created;
			}
			set
			{
				this._created = value;
			}
		}

		public string LastAuthor
		{
			get
			{
				return this._lastAuthor;
			}
			set
			{
				this._lastAuthor = value;
			}
		}

		public DateTime LastSaved
		{
			get
			{
				return this._lastSaved;
			}
			set
			{
				this._lastSaved = value;
			}
		}

		public string Manager
		{
			get
			{
				return this._manager;
			}
			set
			{
				this._manager = value;
			}
		}

		public string Subject
		{
			get
			{
				return this._subject;
			}
			set
			{
				this._subject = value;
			}
		}

		public string Title
		{
			get
			{
				return this._title;
			}
			set
			{
				this._title = value;
			}
		}

		public string Version
		{
			get
			{
				return this._version;
			}
			set
			{
				this._version = value;
			}
		}
	}
}
