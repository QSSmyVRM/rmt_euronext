//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
//FB 1750
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.WebControls;
//using System.Web.UI.HtmlControls;
using System.Runtime.InteropServices;
using System.IO;
using myVRM.ExcelXmlWriter;

/// <summary>
/// Summary description for ExcelUtil
/// </summary>

namespace myVRM.ExcelXmlWriter
{
    public class ExcelUtil
    {
        #region public constructors

        public ExcelUtil()
        {
            //
            // TODO: Add constructor logic here
            //
        }

        #endregion

        #region private data members

        private WorksheetStyle boldTextStyle;
        private WorksheetStyle boldHeaderTextStyle;
        private WorksheetStyle numberStyle;
        private WorksheetStyle DefaultStyle;
        private WorksheetStyle RemarksStyle;
        protected Hashtable translationTable; //100288
        #endregion

        #region CreateXLFile
        /// <summary>
        /// This uses ExcelXmlWriter assembly to generate an excel
        /// If the input dataset is not null and the count = 3, Create an excel workBook
        /// In that, it will first bind the summary data/sumamry  in the 
        /// 1 Sheet. Summary data is available in ds(1).
        /// Then for each EntityCode in  arrEntities it will get the details for that entitycode from
        /// the details table (ds[0]) and generate details/summary part in the separate sheets for each entity
        /// Save the excelfile using DestFile path
        /// </summary>
        /// <param name="arrEntities">List  of all/single entitycode(s) that has been selected</param>
        /// <param name="DestFile">Path + fileName where the generated excel has to be saved</param>
        /// <param name="ds">Has 3 tables.ds[0] - Contains Details data,ds[1] - Contains Summary data,
        ///     ds[2] contains summary data that has to be displayed at the bottom of each sheet </param>

        public void CreateXLFile(ArrayList arrEntities, String destFile, System.Data.DataSet ds, Hashtable tranTable) //ZD 100288
        {
            Workbook book = null;
            Worksheet sheet = null;
            String sheetName = "";
            DataTable detailsTable = null;
            DataTable summartyDetailsTable = null;
            DataTable summaryTable = null;
            DataTable isdnTable = null;
            DataTable isdnSummary = null;
            DataRow[] filteredRows = null;

            try
            {

                translationTable = tranTable;//ZD 100288
                if (ds != null)
                {
                    if (ds.Tables.Count == 5)
                    {
                        detailsTable = ds.Tables[0];
                        summaryTable = ds.Tables[1];
                        summartyDetailsTable = ds.Tables[2];
                        isdnTable = ds.Tables[3];
                        isdnSummary = ds.Tables[4];

                        if (ds.Tables[0].Rows.Count <= 0)
                            throw new Exception(GetTranslatedText("No details available"));//ZD 100288

                        book = new Workbook();
                        book.ExcelWorkbook.WindowTopX = 0;
                        book.ExcelWorkbook.WindowTopY = 0;
                        book.ExcelWorkbook.WindowHeight = 8000;
                        book.ExcelWorkbook.WindowWidth = 19000;

                        BuildStyle(ref book);

                        //To bind Summary Sheet in a book
                        sheetName = GetTranslatedText("Summary").ToString();//ZD 100288
                        sheet = book.Worksheets.Add(sheetName);
                        if (summaryTable != null)
                        {
                            filteredRows = summaryTable.Select("");
                            if (filteredRows != null)
                            {
                                if (filteredRows.Length > 0)
                                {
                                    DumpData(filteredRows, ref sheet, sheetName);

                                    DumpSummaryforSummarySheet(summaryTable, summartyDetailsTable, ref sheet);
                                }
                            }
                        }

                        sheet = null;

                        //To bind sheets for each Entity Code in a book

                        foreach (String entityCode in arrEntities)
                        {
                            if (entityCode == "NA")
                                sheetName = GetTranslatedText("Need Entity Code");//ZD 100288
                            else
                                sheetName = GetTranslatedText(entityCode);//ZD 100288

                            switch (entityCode)
                            {
                                case "ALL":
                                    filteredRows = detailsTable.Select("");
                                    break;
                                case "ISDN":
                                    filteredRows = isdnTable.Select("");
                                    break;
                                default:
                                    filteredRows = detailsTable.Select("[Entity Code]='" + entityCode + "'");
                                    break;
                            }


                            if (filteredRows != null)
                            {
                                if (filteredRows.Length > 0)
                                {
                                    sheet = book.Worksheets.Add(sheetName);

                                    DumpData(filteredRows, ref sheet, sheetName);
                                    filteredRows = null;

                                    if (entityCode != "ALL")
                                    {
                                        if (entityCode == "ISDN")
                                        {
                                            if (isdnSummary != null)
                                                filteredRows = isdnSummary.Select("");
                                        }
                                        else
                                            filteredRows = summartyDetailsTable.Select("[Entity Code]='" + entityCode + "'");
                                    }

                                    if (summartyDetailsTable.Rows.Count > 0)
                                    {
                                        if (entityCode == "ALL")
                                            DumpSummaryforCodeSheets(summartyDetailsTable, ref sheet);
                                        else
                                        {
                                            if (filteredRows != null)
                                            {
                                                if (filteredRows.Length > 0)
                                                {
                                                    DumpSummaryforCodeSheets(filteredRows[0], ref sheet, entityCode);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            filteredRows = null;
                            sheet = null;
                        }
                    }

                    //To save the book
                    DirectoryInfo dir = new DirectoryInfo(destFile.Substring(0, destFile.LastIndexOf("\\")));

                    FileInfo[] files = dir.GetFiles("*.xls");
                    foreach (FileInfo file in files)
                    {
                        if (file.CreationTime <= DateTime.Now.AddMinutes(-20))
                            file.Delete();
                    }
                    if (book != null)
                        book.Save(destFile);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region DumpData
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filteredRows"></param>
        /// <param name="doFillHeader"></param>
        /// <param name="sheet"></param>

        private void DumpData(DataRow[] filteredRows, ref Worksheet sheet, String sheetName)
        {

            WorksheetRow row;
            WorksheetColumn wkSheetCol;
            WorksheetColumnCollection wsColColl;
            if (filteredRows != null)
            {
                DataColumnCollection colCollection = filteredRows[0].Table.Columns;
                //sheet.Table.DefaultColumnWidth = 14f;
                row = sheet.Table.Rows.Add();
                row.AutoFitHeight = true;


                //Header
                for (int iCol = 0; iCol < colCollection.Count; iCol++)
                {
                    WorksheetCell cell = new WorksheetCell(GetTranslatedText(colCollection[iCol].ColumnName), "boldHeaderTextStyle"); //ZD 100288

                    row.Cells.Add(cell);

                }

                sheet.Options.Selected = true;
                sheet.Options.SplitHorizontal = 1;
                sheet.Options.FreezePanes = true;
                sheet.Options.TopRowBottomPane = 1;
                sheet.Options.ActivePane = 2;
                sheet.Options.FitToPage = true;//newly added on 15/10/08


                //Data
                foreach (DataRow dr in filteredRows)
                {
                    row = sheet.Table.Rows.Add();
                    row.AutoFitHeight = true;
                    for (int iCol = 0; iCol < colCollection.Count; iCol++)
                    {
                        if (iCol == 0)
                        {
                            if (sheetName == GetTranslatedText("Summary"))//ZD 100288
                            {
                                //ZD 100288
                                if (dr[iCol].ToString() == "NA")
                                    row.Cells.Add(GetTranslatedText("Need Entity Code"), DataType.String, "DefaultStyle");
                                else
                                    row.Cells.Add(GetTranslatedText(dr[iCol].ToString()), DataType.String, "DefaultStyle");
                            }
                            else
                                row.Cells.Add(dr[iCol].ToString(), DataType.String, "DefaultStyle");

                        }
                        else if (sheetName == GetTranslatedText("Summary"))//ZD 100288
                            row.Cells.Add(dr[iCol].ToString(), DataType.Number, "NumberStyle");
                        else if (dr[iCol].GetType().ToString() == "System.Int32")
                            row.Cells.Add(dr[iCol].ToString(), DataType.Number, "NumberStyle");
                        else
                            row.Cells.Add(dr[iCol].ToString(), DataType.String, "DefaultStyle");

                    }
                }
                wsColColl = sheet.Table.Columns;
                ArrayList arrColWidths = GetColumnWidths(sheetName);
                for (int iCol = 0; iCol < colCollection.Count; iCol++)
                {
                    // if (arrColWidths.Count == colCollection.Count)
                    {
                        wkSheetCol = wsColColl.Add(Convert.ToInt32(arrColWidths[iCol].ToString()));
                        wkSheetCol.Index = iCol + 1;
                    }
                }

            }
        }

        #endregion

        #region DumpData
        /// <summary>
        /// 
        /// </summary>
        /// <param name="filteredRows"></param>
        /// <param name="doFillHeader"></param>
        /// <param name="sheet"></param>

        private void DumpData(DataRow[] filteredRows, ref Worksheet sheet)
        {
            WorksheetRow row;
            WorksheetColumn wkSheetCol;
            WorksheetColumnCollection wsColColl;
            if (filteredRows != null)
            {
                DataColumnCollection colCollection = filteredRows[0].Table.Columns;
                row = sheet.Table.Rows.Add();
                row.AutoFitHeight = true;

                //Header
                for (int iCol = 0; iCol < colCollection.Count; iCol++)
                {
                    WorksheetCell cell = new WorksheetCell(colCollection[iCol].ColumnName, "boldHeaderTextStyle");
                    row.Cells.Add(cell);
                }
                sheet.Options.SplitHorizontal = 1;
                sheet.Options.FreezePanes = true;
                sheet.Options.TopRowBottomPane = 1;

                //Data
                foreach (DataRow dr in filteredRows)
                {
                    row = sheet.Table.Rows.Add();
                    row.AutoFitHeight = true;
                    for (int iCol = 0; iCol < colCollection.Count; iCol++)
                    {
                        if (dr[iCol].GetType().ToString() == "System.Int32")
                            row.Cells.Add(dr[iCol].ToString(), DataType.Number, "NumberStyle");
                        else
                            row.Cells.Add(dr[iCol].ToString(), DataType.String, "DefaultStyle");

                    }
                }
                wsColColl = sheet.Table.Columns;
                for (int iCol = 1; iCol < colCollection.Count; iCol++)
                {

                    wkSheetCol = wsColColl.Add(120);
                    wkSheetCol.Index = iCol;
                    wkSheetCol.AutoFitWidth = true;
                }

            }
        }

        #endregion

        #region GetColumnWidths

        private ArrayList GetColumnWidths(String sheetName)
        {
            ArrayList arrSheetWidths = null;
            try
            {
                arrSheetWidths = new ArrayList();
                if (sheetName == GetTranslatedText("Summary"))//ZD 100288
                {
                    arrSheetWidths.Add("145");
                    arrSheetWidths.Add("46");
                    arrSheetWidths.Add("34");
                    arrSheetWidths.Add("55");
                    arrSheetWidths.Add("67");
                    arrSheetWidths.Add("42");
                    arrSheetWidths.Add("100");
                    arrSheetWidths.Add("106");
                    arrSheetWidths.Add("106");

                }
                else
                {
                    arrSheetWidths.Add("145");
                    arrSheetWidths.Add("45");
                    arrSheetWidths.Add("78");
                    arrSheetWidths.Add("47");
                    arrSheetWidths.Add("140");
                    arrSheetWidths.Add("132");
                    arrSheetWidths.Add("70");
                    arrSheetWidths.Add("70");
                    arrSheetWidths.Add("150");
                    arrSheetWidths.Add("154");
                    arrSheetWidths.Add("97");
                    arrSheetWidths.Add("97");
                    arrSheetWidths.Add("60");
                    arrSheetWidths.Add("55");
                    arrSheetWidths.Add("99");
                    arrSheetWidths.Add("86");
                    arrSheetWidths.Add("64");
                    arrSheetWidths.Add("95");
                    arrSheetWidths.Add("65");
                    arrSheetWidths.Add("345");

                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return arrSheetWidths;
        }

        #endregion

        #region DumpSummaryforCodes
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="sheet"></param>
        /// <param name="entity"></param>
        /// <param name="dt"></param>
        /// <param name="codeType"></param>

        private void DumpSummaryforCodeSheets(DataRow dr, ref Worksheet sheet, String entity)
        {
            WorksheetRow row;
            ArrayList confList = GetConferenceTypeList();
            ArrayList connList = GetConnectionTypeList();
            Int32 rowLength = 0;
            Decimal mins = 0;
            Decimal hrs = 0;
            Decimal totalMins = 0;
            Decimal totalHrs = 0;
            try
            {
                //sheet.Table.DefaultColumnWidth = 20;
                for (int i = 0; i < 3; i++)
                    row = sheet.Table.Rows.Add();

                row = sheet.Table.Rows.Add();
                row.AutoFitHeight = true;

                for (int i = 0; i < 2; i++)
                    AddEmptyCell(ref row);

                //To add the First row of the Summary Details of an entity/Summary Sheets which contains VTC Totals Minutes
                //and Report Title 
				//ZD 100288 Starts
                row.Cells.Add(new WorksheetCell(GetTranslatedText("Duration Total"), "BoldTextStyle"));
                row.Cells.Add(dr["Minutes"].ToString(), DataType.Number, "NumberStyle");
                row.Cells.Add(new WorksheetCell(GetTranslatedText("Minutes"), "BoldTextStyle"));

                if (entity == "NA")
                    entity = GetTranslatedText("Need Entity Code");

                row.Cells.Add(new WorksheetCell((String.Format(GetTranslatedText("Report Title") + " : {0}", entity)), "BoldTextStyle"));
				//ZD 100288 Ends

                //To add the second row of the Summary Details of an entity/Summary Sheets which contains VTC Totals Hours
                row = sheet.Table.Rows.Add();
                row.AutoFitHeight = true;

                for (int i = 0; i < 3; i++)
                    AddEmptyCell(ref row);

                row.Cells.Add(dr["Hours"].ToString(), DataType.Number, "NumberStyle");

                row.Cells.Add(new WorksheetCell(GetTranslatedText("Hours"), "BoldTextStyle"));//ZD 100288


                row = sheet.Table.Rows.Add();

                //To apply the heading for the row
                BindSummaryHeader(ref sheet);

                //To add the subsequent rows of the Summary Details of an entity/Summary Sheets which gets
                // the caption from ArrayList

                if (connList != null && confList != null)
                {
                    rowLength = connList.Count;
                    if (rowLength < confList.Count)
                        rowLength = confList.Count;
                }

                if (rowLength > 0)
                {
                    for (int i = 0; i < rowLength; i++)
                    {
                        row = sheet.Table.Rows.Add();
                        mins = 0;
                        hrs = 0;

                        if (confList.Count > i)
                        {

                            if (confList[i].ToString() != "Total Conferences")
                            {

                                if (dr.Table.Columns.Contains(confList[i].ToString()))
                                    mins = (dr[confList[i].ToString()] == DBNull.Value) ? 0 : Convert.ToInt32(dr[confList[i].ToString()]);

                                hrs = Math.Round(mins / 60, 2);

                                totalMins += mins;
                                totalHrs += hrs;
                            }
                            else
                            {
                                mins = totalMins;
                                hrs = totalHrs;
                            }

                            row.Cells.Add(new WorksheetCell(GetTranslatedText(confList[i].ToString()), "BoldTextStyle"));//ZD 100288
                            row.Cells.Add(new WorksheetCell(mins.ToString(), DataType.Number, "NumberStyle"));
                            row.Cells.Add(new WorksheetCell(hrs.ToString(), DataType.Number, "NumberStyle"));
                        }
                        else
                        {
                            row.Cells.Add();
                            row.Cells.Add();
                            row.Cells.Add();
                        }
                        row.Cells.Add();
                        mins = 0;
                        hrs = 0;
                        if (connList.Count > i)
                        {
                            if (dr.Table.Columns.Contains(connList[i].ToString()))
                                mins = (dr[connList[i].ToString()] == DBNull.Value) ? 0 : Convert.ToDecimal(dr[connList[i].ToString()]);

                            hrs = Math.Round(mins / 60, 2);

                            row.Cells.Add(new WorksheetCell(GetTranslatedText(connList[i].ToString()), "BoldTextStyle"));//ZD 100288
                            row.Cells.Add(new WorksheetCell(mins.ToString(), DataType.Number, "NumberStyle"));
                            row.Cells.Add(new WorksheetCell(hrs.ToString(), DataType.Number, "NumberStyle"));
                        }


                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region DumpSummaryforCodes
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dr"></param>
        /// <param name="sheet"></param>
        /// <param name="entity"></param>
        /// <param name="dt"></param>
        /// <param name="codeType"></param>

        private void DumpSummaryforCodeSheets(DataTable confTable, ref Worksheet sheet)
        {
            WorksheetRow row;
            ArrayList confList = GetConferenceTypeList();
            ArrayList connList = GetConnectionTypeList();
            Int32 rowLength = 0;
            decimal mins = 0;
            decimal hrs = 0;
            Decimal totalMins = 0;
            Decimal totalHrs = 0;
            try
            {
                //sheet.Table.DefaultColumnWidth = 20;
                for (int i = 0; i < 3; i++)
                    row = sheet.Table.Rows.Add();

                row = sheet.Table.Rows.Add();
                row.AutoFitHeight = true;

                for (int i = 0; i < 2; i++)
                    AddEmptyCell(ref row);

                //To add the First row of the Summary Details of an entity/Summary Sheets which contains VTC Totals Minutes
                //and Report Title 
				//ZD 100288 Starts
                row.Cells.Add(new WorksheetCell(GetTranslatedText("Duration Total"), "BoldTextStyle"));
                row.Cells.Add(confTable.Compute("Sum(Minutes)", "").ToString(), DataType.Number, "NumberStyle");
                row.Cells.Add(new WorksheetCell(GetTranslatedText("Minutes"), "BoldTextStyle"));

                //row.Cells.Add(new WorksheetCell((String.Format("Report Title : {0}", "ALL")), "BoldTextStyle"));
                row.Cells.Add(new WorksheetCell(GetTranslatedText("Report Title : ALL"), "BoldTextStyle"));
				//ZD 100288 Ends
                //To add the second row of the Summary Details of an entity/Summary Sheets which contains VTC Totals Hours
                row = sheet.Table.Rows.Add();
                row.AutoFitHeight = true;

                for (int i = 0; i < 3; i++)
                    AddEmptyCell(ref row);

                row.Cells.Add(confTable.Compute("Sum(Hours)", "").ToString(), DataType.Number, "NumberStyle");

                row.Cells.Add(new WorksheetCell(GetTranslatedText("Hours"), "BoldTextStyle"));//ZD 100288

                row = sheet.Table.Rows.Add();

                //To apply the heading for the row
                BindSummaryHeader(ref sheet);

                //To add the subsequent rows of the Summary Details of an entity/Summary Sheets which gets
                // the caption from ArrayList

                if (connList != null && confList != null)
                {
                    rowLength = connList.Count;
                    if (rowLength < confList.Count)
                        rowLength = confList.Count;
                }

                if (rowLength > 0)
                {
                    for (int i = 0; i < rowLength; i++)
                    {
                        mins = 0;
                        hrs = 0;
                        row = sheet.Table.Rows.Add();

                        if (confList.Count > i)
                        {
                            if (confList[i].ToString() != "Total Conferences")
                            {
                                if (confTable.Columns.Contains(confList[i].ToString()))
                                    mins = Convert.ToDecimal(confTable.Compute("sum([" + confList[i].ToString() + "])", ""));

                                hrs = Math.Round(mins / 60, 2);

                                totalMins += mins;
                                totalHrs += hrs;
                            }
                            else
                            {
                                mins = totalMins;
                                hrs = totalHrs;
                            }

                            row.Cells.Add(new WorksheetCell(GetTranslatedText(confList[i].ToString()), "BoldTextStyle"));//ZD 100288
                            row.Cells.Add(mins.ToString(), DataType.Number, "NumberStyle");
                            row.Cells.Add(hrs.ToString(), DataType.Number, "NumberStyle");
                        }
                        else
                        {
                            row.Cells.Add();
                            row.Cells.Add();
                            row.Cells.Add();
                        }


                        mins = 0;
                        hrs = 0;
                        row.Cells.Add();

                        if (connList.Count > i)
                        {
                            if (confTable != null)
                            {
                                if (confTable.Columns.Contains(connList[i].ToString()))
                                    mins = Convert.ToDecimal(confTable.Compute("sum([" + connList[i].ToString() + "])", ""));
                            }

                            hrs = Math.Round(mins / 60, 2);


                            row.Cells.Add(new WorksheetCell(GetTranslatedText(connList[i].ToString()), "BoldTextStyle"));//ZD 100288
                            row.Cells.Add(mins.ToString(), DataType.Number, "NumberStyle");
                            row.Cells.Add(hrs.ToString(), DataType.Number, "NumberStyle");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region DumpSummaryforSummarySheet
        /// <summary>
        /// For VTC Totals, display sum the columns of all the rows in the summaryTable and append the row to the ref sheet
        /// For other totals,get the total from summaryDetailsTable for each param and append the rows to the ref sheet
        /// </summary>
        /// <param name="summaryTable"></param>
        /// <param name="summaryDetailsTable"></param>
        /// <param name="sheet"></param>

        private void DumpSummaryforSummarySheet(DataTable summaryTable, DataTable summaryDetailsTable, ref Worksheet sheet)
        {
            WorksheetRow row = null;
            Decimal mins = 0;
            Decimal hrs = 0;
            Decimal totalMins = 0;
            Decimal totalHrs = 0;
            try
            {
                //To display VTC Totals Row
                if (summaryTable != null)
                {
                    for (int i = 0; i < 3; i++)
                        row = sheet.Table.Rows.Add();

                    foreach (DataColumn col in summaryTable.Columns)
                    {
                        //To write "VTC Totals" instead of Entity_code in the 1st position
                        if (col.Ordinal == 0)
                            row.Cells.Add(new WorksheetCell(GetTranslatedText("Grand Totals"), "BoldTextStyle"));//ZD 100288
                        else
                            row.Cells.Add(summaryTable.Compute("Sum([" + col.ColumnName + "])", "").ToString(), DataType.Number, "NumberStyle");

                    }

                }

                row = sheet.Table.Rows.Add();
                row = sheet.Table.Rows.Add();
                row.Cells.Add(new WorksheetCell(GetTranslatedText("CONFERENCE TYPE"), "boldHeaderTextStyle"));//ZD 100288

                //To display Other Summary Rows which gets from Summary
                ArrayList paramList = GetConferenceTypeList();
                if (paramList != null)
                {
                    foreach (String str in paramList)
                    {
                        row = sheet.Table.Rows.Add();
                        //row.Cells.Add(str, DataType.String, "boldTextStyle");

                        row.Cells.Add(new WorksheetCell(GetTranslatedText(str), "BoldTextStyle"));//ZD 100288

                        mins = 0;
                        hrs = 0;

                        if (str != GetTranslatedText("Total Conferences"))//ZD 100288
                        {
                            if (summaryDetailsTable != null)
                                mins = Convert.ToDecimal(summaryDetailsTable.Compute("Sum([" + str + "])", ""));

                            hrs = Math.Round(mins / 60, 2);

                            totalMins += mins;
                            totalHrs += hrs;
                        }
                        else
                        {
                            mins = totalMins;
                            hrs = totalHrs;
                        }

                        row.Cells.Add(mins.ToString(), DataType.Number, "NumberStyle");

                        row.Cells.Add(hrs.ToString(), DataType.Number, "NumberStyle");

                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        #endregion

        #region BindSummaryHeader

        private void BindSummaryHeader(ref Worksheet sheet)
        {
			//ZD 100288 Starts
            WorksheetRow row = sheet.Table.Rows.Add();
            row.Cells.Add(new WorksheetCell(GetTranslatedText("CONFERENCE TYPE"), "boldHeaderTextStyle"));
            row.Cells.Add(new WorksheetCell(GetTranslatedText("(Mins)"), "boldHeaderTextStyle"));
            row.Cells.Add(new WorksheetCell(GetTranslatedText("(Hrs)"), "boldHeaderTextStyle"));
            row.Cells.Add(new WorksheetCell());
            row.Cells.Add(new WorksheetCell(GetTranslatedText("CONNECTION TYPE"), "boldHeaderTextStyle"));
            row.Cells.Add(new WorksheetCell(GetTranslatedText("(Mins)"), "boldHeaderTextStyle"));
            row.Cells.Add(new WorksheetCell(GetTranslatedText("(Hrs)"), "boldHeaderTextStyle"));
			//ZD 100288 Ends

        }

        #endregion

        #region BuildStyle
        /// <summary>
        /// Builds the style for excel rows/cells that has been used to format the sheet(s).
        /// </summary>
        private void BuildStyle(ref Workbook book)
        {
            try
            {

                //To assign the bold effect/left alignment for Summary Texts
                boldTextStyle = book.Styles.Add("BoldTextStyle");
                boldTextStyle.Font.Bold = true;

                boldHeaderTextStyle = book.Styles.Add("boldHeaderTextStyle");
                boldHeaderTextStyle.Font.Bold = true;
                boldHeaderTextStyle.Alignment.Horizontal = StyleHorizontalAlignment.Center;

                //To assign the right alignment for Summary Number values
                numberStyle = book.Styles.Add("NumberStyle");
                numberStyle.Alignment.Horizontal = StyleHorizontalAlignment.Right;

                DefaultStyle = book.Styles.Add("DefaultStyle");
                DefaultStyle.Alignment.WrapText = true;//newly added on 15/10/08
                DefaultStyle.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                DefaultStyle.NumberFormat = "0";


                RemarksStyle = book.Styles.Add("RemarksStyle");
                RemarksStyle.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                RemarksStyle.Alignment.WrapText = true;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region AddEmptyCell

        private void AddEmptyCell(ref WorksheetRow row)
        {
            row.Cells.Add("");
        }

        #endregion

        #region GetConferenceTypeList

        private ArrayList GetConferenceTypeList()
        {
            ArrayList aList = new ArrayList();

            aList.Add("Point-to-Point Conferences");
            aList.Add("Audio-Video Conferences");
            aList.Add("Total Conferences");

            return aList;
        }

        #endregion

        #region GetConnectionTypeList

        private ArrayList GetConnectionTypeList()
        {
            ArrayList aList = new ArrayList();

            aList.Add("ISDN Dial-out connections");
            aList.Add("ISDN Dial-in connections");
            aList.Add("IP Dial-out connections");
            aList.Add("IP Dial-in connections");

            return aList;
        }

        #endregion

        // Event Log start
        #region CreateXLFile

        public void CreateXLFile(Hashtable logList, String DestFile, System.Data.DataSet ds)
        {
            Workbook book = null;
            Worksheet sheet = null;
            String sheetName = "";
            DataRow[] filteredRows = null;
            try
            {
                if (ds != null)
                {
                    if (ds.Tables.Count > 0)
                    {
                        book = new Workbook();

                        BuildStyle(ref book);

                        if (logList != null)
                        {
                            Int32 i = 0;
                            foreach (String key in logList.Keys)
                            {
                                sheetName = "";
                                DataTable eventTable = ds.Tables[key];
                                sheetName = logList[key].ToString();
                                sheet = book.Worksheets.Add(sheetName);

                                filteredRows = eventTable.Select("");
                                if (filteredRows != null)
                                {
                                    if (filteredRows.Length > 0)
                                    {
                                        DumpData(filteredRows, ref sheet);
                                    }
                                }

                                i++;
                            }
                        }

                        //To save the book
                        if (book != null)
                            book.Save(DestFile);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        // Event Log end
		//ZD 100288 Starts
        #region GetTranslatedText

        private string GetTranslatedText (string originalText)
        {
            String translatedText = "";
            
            if (translationTable[originalText.ToLower()] != null)
                translatedText = translationTable[originalText.ToLower()].ToString();
            else if (translationTable[originalText] == null)
                translatedText = originalText;

            return translatedText;
        }

        #endregion
		//ZD 100288 Ends
    }
}
