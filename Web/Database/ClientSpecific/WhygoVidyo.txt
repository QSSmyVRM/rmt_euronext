Vidyo: 
------

TO Enable vidyo functionality , need to set Cloud tag in license as 1.

In Org Settings screen: Vidyo Credentials can be entered and Import button is there to import Vidyo users, Room, Endpoints, 

Departmets.

1.Only External VMR provider is enabled by default. If time permits we will disable all the fields in org option with respect to conference type ,
 VMR, fly room creation and MCU buffer.

2)The following create/Edit/Delete is restricted. It is as simple as hiding the create button
1.	MCU
2.	Endpoint
3.	Room
4.	Active user (Create alone)

Conference Section: 
1.	All Conferences are Audio Video. Hide Conference Type drop down
2.	All Conferences are VMR. Checkbox is default selected and disabled.
3.	Hide AV settings tab. (For this simple solution is all users will have Enable AV set to 0 so no code changes is required).
4.	Only Room or PC attendee is allowed. Hide external attendee.
5.	Hide audio participants.

Room Section: 
1.	Hide Deactivate and Edit in manage location.
2.	Hide View Type.


User Section: 
1.	Hide Delete in manage User.
2.	Add the following columns to Active and Inactive user
a.	EntityID Integer
b.	Pin Varchar 10
3.	Add text box called in PIN in manage user profile. Also add confirm text box.
4.	Rename the external VMR to Vidyo Link in manage user profile.




WHygo :


TO Enable whygo functionality, nee to set "PublicRoomService" to 1.

In Site settings, whygo WS service credentails can entered, to poll Public Rooms, Endpoints.

In Manage ROom and Endpoint screen : Total public rooms/Endppoints will be shown.

There is a option for deleting Public rooms/EP once imported. When no rooms/ENdpoints in use.






