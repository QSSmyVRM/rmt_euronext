/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9315.3.5  Starts(24th Aug 2015)          */
/*                                                                                              */
/* ******************************************************************************************** */


/* **********************ZD 104116 -24th Aug 2015 Starts************ */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD	
	BJNDisplay int NULL	
GO
COMMIT

update Org_Settings_D set BJNDisplay = 0

/* **********************ZD 104116 -24th Aug 2015 Ends************ */

/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9315.3.5  Ends(25th Aug 2015)            */
/*                              Features & Bugs for V2.9315.3.6  Starts(25th Aug 2015)          */
/*                              Features & Bugs for V2.9315.3.6  Ends(28th Aug 2015)            */
/*                              Features & Bugs for V2.9315.3.7  Starts(28th Aug 2015)          */
/* ******************************************************************************************** */


/* **********************ZD 104200 -Sep 1st 2015 Starts************ */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Launguage_Translation_Text ADD
	TextType smallint NULL
GO
ALTER TABLE dbo.Launguage_Translation_Text SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Launguage_Translation_Text set TextType = 1

/* **********************ZD 104200 -Sep 1st 2015 Ends************ */


/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9315.3.7  Ends(8th Sep 2015)             */
/*                              Features & Bugs for V2.9315.3.8  Starts(9th Sep 2015)           */
/* ******************************************************************************************** */

/* 102754- Problem - Text in Description Box in Reservation History is Still in English When Website is in French- START*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_AuditSummary_D ADD
	LanguageId int NULL
GO
ALTER TABLE dbo.Conf_AuditSummary_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

update Conf_AuditSummary_D set LanguageId = 1

/* 102754- END*/

/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9315.3.8  Ends(15th Sep 2015)             */
/*                              Features & Bugs for V2.9315.3.8  Starts(15th Sep 2015)           */
/* ******************************************************************************************** */


/* ZD 104151 */

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EmptyConferencePush smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9315.3.9  Ends(15th Sep 2015)             */
/*                              Features & Bugs for V2.9315.3.10  Starts(16th Sep 2015)           */
/* ******************************************************************************************** */

/* **********************ZD 100040 -Sep 23rd 2015 Starts************ */

-- ZD 100040 sql changes --
-- Menu changes for Smart MCU Load Balancing work
-- IMPORTANT: Please change the existing custom role's menu mask manually.

update Usr_Roles_D
set roleMenuMask='8*240-4*11+8*254+4*15+4*15+5*31+4*15+5*31+5*31+2*0+2*3+4*11+8*0+6*0+3*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='General User'

update Usr_Roles_D
set roleMenuMask='8*248-4*15+8*255+4*15+4*15+5*31+4*15+5*31+5*31+2*3+2*3+4*11+8*255+6*63+3*7+9*511+2*3+2*3+2*3+1*0-7*93'
where roleName='Organization Administrator 1'

update Usr_Roles_D
set roleMenuMask='8*252-4*15+8*255+4*15+4*15+5*31+4*15+5*31+5*31+2*3+2*3+4*15+8*255+6*63+3*7+9*511+2*3+2*3+2*3+1*1-7*127'
where roleName='Site Administrator'


update Usr_Roles_D
set roleMenuMask='8*136-4*0+8*0+4*0+4*0+5*0+4*0+5*0+5*0+2*0+2*0+4*0+8*2+6*0+3*0+9*0+2*0+2*3+2*0+1*0-7*92'
where roleName='Catering Administrator'

update Usr_Roles_D
set roleMenuMask='8*136-4*0+8*0+4*0+4*0+5*0+4*0+5*0+5*0+2*0+2*0+4*0+8*4+6*0+3*0+9*0+2*3+2*0+2*0+1*0-7*92'
where roleName='Audiovisual Administrator'

update Usr_Roles_D
set roleMenuMask='8*136-4*0+8*0+4*0+4*0+5*0+4*0+5*0+5*0+2*0+2*0+4*0+8*1+6*0+3*0+9*0+2*0+2*0+2*3+1*0-7*92'
where roleName='Facility Administrator'

update Usr_Roles_D
set roleMenuMask='8*96-4*1+8*160+4*15+4*0+5*30+4*0+5*0+5*0+2*0+2*2+4*0+8*0+6*0+3*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='Express Profile'

update Usr_Roles_D
set roleMenuMask='8*96-4*9+8*160+4*15+4*0+5*30+4*0+5*0+5*0+2*0+2*2+4*0+8*0+6*0+3*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='Express Profile Manage'

update Usr_Roles_D
set roleMenuMask='8*112-4*9+8*254+4*15+4*15+5*30+4*15+5*30+5*30+2*0+2*2+4*4+8*0+6*0+3*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='Express Profile Advanced'

update Usr_Roles_D
set roleMenuMask='8*208-4*9+8*254+4*9+4*10+5*16+4*8+5*16+5*16+2*0+2*0+4*11+8*0+6*0+3*0+9*0+2*0+2*0+2*0+1*0-7*92'
where roleName='View-Only'

update Usr_Roles_D
set roleMenuMask='8*248-4*15+8*255+4*15+4*15+5*31+4*15+5*31+5*31+2*3+2*3+4*11+8*255+6*47+3*7+9*381+2*3+2*3+2*3+1*0-7*93'
where roleName='VNOC Operator'

update Usr_Roles_D
set roleMenuMask='8*248-4*15+8*255+4*15+4*15+5*31+4*15+5*31+5*31+2*3+2*3+4*15+8*255+6*63+3*7+9*511+2*3+2*3+2*3+1*0-7*93'
where roleName='Organization Administrator 2'



update list
set list.MenuMask = roles.roleMenuMask
from Usr_List_D as list, Usr_Roles_D as roles
where list.roleID = roles.roleID

update Inactive
set Inactive.MenuMask = roles.roleMenuMask
from Usr_Inactive_D as Inactive, Usr_Roles_D as roles
where Inactive.roleID = roles.roleID



insert into icons_ref_s values('52','../en/img/ManageMCUIcon.png','MCU Load Balancing','ManageMCUGroups.aspx')



/****** Object:  Table [MCU_PortResolution_D] Script Date: 09/10/2015 07:22:37 ******/
-- MCU port resoultions

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MCU_PortResolution_D](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[bridgeId] [int] NULL,
	[resolutionId] [int] NULL,
	[port] [int] NULL,
 CONSTRAINT [PK_MCU_PortResolution_D] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/****** Object:  Table [dbo].[MCU_GrpDetails_D]    Script Date: 09/14/2015 02:06:05 ******/
-- Virtual MCU groups table

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[MCU_GrpDetails_D](
	[MCUGroupID] [int] NOT NULL,
	[GroupName] [varchar](256) NULL,
	[Description] [varchar](2000) NULL,
	[LoadBalance] [smallint] NULL,
	[Pooled] [smallint] NULL,
	[LCR] [smallint] NULL,
	[OrgID] [int] NULL,
	[CreateTime] [datetime] NULL,
	[LastUpdateTime] [datetime] NULL,
	[CreatedBy] [int] NULL,
	[ModifiedBy] [int] NULL,
 CONSTRAINT [PK_MCU_GrpDetails_D] PRIMARY KEY CLUSTERED 
(
	[MCUGroupID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[MCU_GrpAssignList_D]    Script Date: 09/14/2015 02:06:33 
Virtual MCUs assginged to groups will be stored in this table
******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[MCU_GrpAssignList_D](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[MCUGroupID] [int] NOT NULL,
	[MCUID] [int] NULL,
	[MCUAdmin] [int] NULL,
	[TimezoneId] [int] NULL,
	[LoadBalance] [int] NULL,
	[Overflow] [int] NULL,
	[LeadMCU] [int] NULL,
 CONSTRAINT [PK_MCUGrp_AssignList_D] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

/* To prevent any potential data loss issues, you should review this script in detail before running it outside the context of the database designer.*/
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE dbo.Gen_EptResolution_S 
	(
	RID int NOT NULL,
	Resolution nvarchar(250) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE dbo.Gen_EptResolution_S  SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


--select * from Gen_EptResolution_S
Insert into Gen_EptResolution_S (RID,Resolution) values (1,'1080p60')
Insert into Gen_EptResolution_S (RID,Resolution) values (2,'1080p30')
Insert into Gen_EptResolution_S (RID,Resolution) values (3,'720p30')
Insert into Gen_EptResolution_S (RID,Resolution) values (4,'720p60')
Insert into Gen_EptResolution_S (RID,Resolution) values (5,'SD')
Insert into Gen_EptResolution_S (RID,Resolution) values (6,'CIF (H.264)')


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	Resolution smallint NULL,
	LCR smallint NULL
GO
ALTER TABLE dbo.Ept_List_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

-- to update existing Endpoints .. one time query during upgrade of this feature but no harm

update Ept_List_D set Resolution = 5,  LCR =0  where (Resolution <=0 OR Resolution is null )


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Ept_List_D ADD
	Resolution smallint NULL,
	LCR smallint NULL
GO
ALTER TABLE dbo.Audit_Ept_List_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* **********************ZD 100040 -Sep 23rd 2015 Ends************ */
--ZD 104112
ALTER TABLE Org_Settings_D ALTER COLUMN DefaultConfDuration int

/* **********************ZD 104256 -Oct 5th 2015 Starts************ */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnablePoolOrderSelection smallint NULL
GO
COMMIT

Update Org_Settings_D set EnablePoolOrderSelection = 0


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
CREATE TABLE MCU_PoolOrders_D
	(
	UID int NOT NULL IDENTITY (1, 1),
	MCUId int NULL,
	PoolOrderId int NULL,
	PoolOrderName nvarchar(50) NULL
	)  ON [PRIMARY]
GO
ALTER TABLE MCU_PoolOrders_D ADD CONSTRAINT
	PK_MCU_PoolOrders_D PRIMARY KEY CLUSTERED 
	(
	UID
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Bridge_D ADD
	PoolOrderID int NULL
GO
ALTER TABLE dbo.Conf_Bridge_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT



Update Conf_Bridge_D set PoolOrderID= -1;

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Mcu_List_D ADD
	PoolOrderID int NULL
GO
ALTER TABLE dbo.Mcu_List_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Mcu_List_D set PoolOrderID =0



/* **********************ZD 104256 -Oct 5th 2015 Ends************ */

/* **********************ZD 102131 -Oct 5th 2015 Ends************ */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Archive_Conf_User_D ADD
	MeetingSigninTime datetime NULL
GO
ALTER TABLE dbo.Archive_Conf_User_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* **********************ZD 102131 -Oct 5th 2015 Ends************ */
--ZD 100040
update icons_ref_s set iconuri = '../en/img/MCULoadBalanceIcon.png' where iconid = 52

Declare @bridgeID int, @resolID int, @port int, @defaultPort int

Declare eptReslCursor Cursor for
select BridgeID, maxConcurrentVideoCalls from Mcu_List_D where deleted = 0

open eptReslCursor 
Fetch Next from eptReslCursor into @bridgeID,@port
WHILE @@FETCH_STATUS = 0
BEGIN
	Declare cursor1 Cursor for
				
	SELECT RID FROM Gen_EPTResolution_S 

	Open cursor1
	Fetch Next from cursor1 into @resolID
	While @@Fetch_Status = 0
	Begin
		set @defaultPort = 0
		if @resolID = 5 
			set @defaultPort = @port
			
		insert into MCU_PortResolution_D (bridgeId, resolutionId, port) values (@bridgeID, @resolID, @defaultPort)
		
	Fetch Next from cursor1 into @resolID
	End
	close cursor1	
	Deallocate cursor1
Fetch Next from eptReslCursor into @bridgeID,@port
End
close eptReslCursor
Deallocate eptReslCursor


/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9315.3.10  Ends(10th Oct 2015)           */
/*                              Features & Bugs for V2.9415.3.0  Starts(10th Oct 2015)          */
/* ******************************************************************************************** */

/* ******************************************************************************************** */
/*                              Features & Bugs for V2.9415.3.0  Ends(10th Oct 2015)            */
/*                              Features & Bugs for V2.9415.3.1  Starts(10th Oct 2015)          */
/* ******************************************************************************************** */

/* **********************ZD 104556 - Oct 30th 2015 Ends************ */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Email_Content_D ADD
	ConfType int NULL
GO
ALTER TABLE dbo.Email_Content_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* **********************ZD 104556 - Oct 30th 2015 Ends************ */

/*                              Features & Bugs for V2.9415.3.1  Ends(5th Nov 2015)          */
/*                              Features & Bugs for V2.9415.3.2  Starts(5th Nov 2015)          */

/* **********************ZD 104254 /ZD 104618 -13-Nov-2015 Start************ */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	ConfStartDate datetime NULL,
	Duration int NULL,
	AudioBridge int NULL
GO
ALTER TABLE dbo.Conf_User_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Conf_User_D ADD
	ConfStartDate datetime NULL,
	Duration int NULL,
	AudioBridge int NULL
GO
ALTER TABLE dbo.Audit_Conf_User_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Archive_Conf_User_D ADD
	ConfStartDate datetime NULL,
	Duration int NULL,
	AudioBridge int NULL
GO
ALTER TABLE dbo.Archive_Conf_User_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


update ConfUser set ConfUser.ConfStartDate = Conf.confdate , ConfUser.Duration = Conf.duration
from Conf_User_D as ConfUser, Conf_Conference_D as Conf
where ConfUser.confuId = Conf.confnumname


/* **********************ZD 104254 /ZD 104618 - 13-Nov-2015 End************ */


/* **********************ZD 104681 - 17-Nov-2015 End************ */


/****** Object:  Table [dbo].[Temp_ConfRoom_D]    Script Date: 11/04/2015 04:15:46 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Temp_ConfRoom_D](
	[ConfID] [int] NOT NULL,
	[RoomID] [int] NOT NULL,
	[DefLineRate] [int] NULL,
	[DefVideoProtocol] [int] NULL,
	[StartDate] [datetime] NULL,
	[Duration] [int] NULL,
	[instanceID] [int] NOT NULL,
	[connect2] [smallint] NULL,
	[bridgeIPISDNAddress] [nvarchar](256) NULL,
	[bridgeid] [int] NULL,
	[connectiontype] [smallint] NULL,
	[ipisdnaddress] [nvarchar](256) NULL,
	[connectstatus] [smallint] NULL,
	[outsidenetwork] [smallint] NULL,
	[mcuservicename] [nvarchar](4000) NULL,
	[audioorvideo] [smallint] NULL,
	[mute] [smallint] NULL,
	[addresstype] [smallint] NULL,
	[bridgeaddresstype] [smallint] NULL,
	[layout] [int] NULL,
	[endpointId] [int] NULL,
	[uId] [int] NOT NULL,
	[confuId] [int] NOT NULL,
	[profileId] [int] NULL,
	[prefix] [nvarchar](50) NULL,
	[isLecturer] [int] NOT NULL,
	[OnlineStatus] [int] NULL,
	[LastRunDateTime] [datetime] NULL,
	[ApiPortNo] [int] NULL,
	[endptURL] [nvarchar](50) NULL,
	[remoteEndPointIP] [nvarchar](50) NULL,
	[disabled] [smallint] NOT NULL,
	[MultiCodecAddress] [nvarchar](2000) NULL,
	[Extroom] [int] NULL,
	[isTextMsg] [int] NULL,
	[GUID] [nvarchar](max) NULL,
	[Setfocus] [int] NULL,
	[Message] [int] NULL,
	[MuteRxaudio] [int] NULL,
	[MuteRxvideo] [int] NULL,
	[MuteTxvideo] [int] NULL,
	[Camera] [int] NULL,
	[Packetloss] [int] NULL,
	[LockUnLock] [int] NULL,
	[Record] [int] NULL,
	[Stream] [nvarchar](max) NULL,
	[RxAudioPacketsReceived] [nvarchar](50) NULL,
	[RxAudioPacketErrors] [nvarchar](50) NULL,
	[RxAudioPacketsMissing] [nvarchar](50) NULL,
	[RxVideoPacketsReceived] [nvarchar](50) NULL,
	[RxVideoPacketErrors] [nvarchar](50) NULL,
	[RxVideoPacketsMissing] [nvarchar](50) NULL,
	[TerminalType] [smallint] NULL,
	[BridgeExtNo] [nvarchar](50) NOT NULL,
	[Secured] [int] NULL,
	[PartyName] [nvarchar](250) NULL,
	[isDoubleBooking] [smallint] NULL,
	[ChairPerson] [smallint] NULL,
	[GateKeeeperAddress] [nvarchar](50) NULL,
	[activeSpeaker] [int] NULL,
	[TxAudioPacketsSent] [nvarchar](50) NULL,
	[TxVideoPacketsSent] [nvarchar](50) NULL,
	[PartyNameonMCU] [nvarchar](max) NULL,
	[isBJNRoom] [int] NULL,
	[HostRoom] [int] NULL,
	[NoofAttendee] [int] NULL
) ON [PRIMARY]
GO

/* **********************ZD 104681 - 17-Nov-2015 End************ */
--ZD 104705
update sys_settings_d set MemcacheEnabled = 0

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9415.3.2  Ends(20th  Nov 2015)           */
/*                              Features & Bugs for V2.9415.3.3  Starts(20th Nov 2015)          */
/*                                                                                              */
/* ******************************************************************************************** */

--ZD 104735
 
update Gen_State_S set [StateCode] = LTRIM(StateCode), [State] = LTRIM(State)

-- ZD 104733
Update Loc_Room_D set RoomIconTypeId ='Video.jpg' where iControl = 1 and RoomCategory = 6 and videoAvailable = 2

Update Loc_Room_D set RoomIconTypeId ='Audio.jpg' where iControl = 1 and RoomCategory = 6 and videoAvailable = 1

Update Loc_Room_D set RoomIconTypeId ='Room.jpg'  where iControl = 1 and RoomCategory = 6 and videoAvailable = 0

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9415.3.3  Ends(4th  Dec 2015)            */
/*                              Features & Bugs for V2.9415.3.4  Starts(4th Dec 2015)           */
/*                              Features & Bugs for V2.9415.3.4  Ends(10th Dec 2015)            */
/*                              Features & Bugs for V2.9415.3.5  Starts(10th Dec 2015)          */
/*                                                                                              */
/* ******************************************************************************************** */


/* **********************ZD 104786 - 27-Dev-2015 Starts************ */
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	AssignPartyToRoom smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Org_Settings_D set AssignPartyToRoom = 0

/* **********************ZD 104786 - 27-Dev-2015 End************ */

ALTER TABLE [Email_Content_D] ALTER COLUMN [Placeholders] nvarchar(MAX)

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9415.3.5  Ends(25th Dec 2015)            */
/*                              Features & Bugs for V2.9116.3.0  Starts(4th Jan 2016)           */
/*                                                                                              */
/* ******************************************************************************************** */


/* **********************ZD 104854-Disney Starts************ */

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD	
	EnableAudbridgefreebusy int NULL	
GO
COMMIT

update Org_Settings_D set EnableAudbridgefreebusy = 0

/* **********************ZD 104854-Disney Ends************ */

/* **********************ZD 104821 Starts************ */


BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Ept_List_D ADD
	SysLocationId int NULL
GO
ALTER TABLE dbo.Ept_List_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


Update Ept_List_D set  SysLocationId  = 0

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_Room_D ADD
	SysLocationId int NULL
GO
ALTER TABLE dbo.Conf_Room_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT
Update Conf_Room_D set  SysLocationId  = 0

BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Conf_User_D ADD
	SysLocationId int NULL
GO
ALTER TABLE dbo.Conf_User_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT


Update Conf_User_D set  SysLocationId  = 0



BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Audit_Ept_List_D ADD
	SysLocationId int NULL
GO
ALTER TABLE dbo.Audit_Ept_List_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

/* **********************ZD 104821 Ends************ */

/* ******************************************************************************************** */
/*                                                                                              */
/*                              Features & Bugs for V2.9116.3.0  Ends(18th Jan 2016)            */
/*                              Features & Bugs for V2.9116.3.1  Starts(18th Jan 2016)          */
/*                                                                                              */
/* ******************************************************************************************** */
-- ZD 104862
BEGIN TRANSACTION
SET QUOTED_IDENTIFIER ON
SET ARITHABORT ON
SET NUMERIC_ROUNDABORT OFF
SET CONCAT_NULL_YIELDS_NULL ON
SET ANSI_NULLS ON
SET ANSI_PADDING ON
SET ANSI_WARNINGS ON
COMMIT
BEGIN TRANSACTION
GO
ALTER TABLE dbo.Org_Settings_D ADD
	EnableBlockUserDI smallint NULL
GO
ALTER TABLE dbo.Org_Settings_D SET (LOCK_ESCALATION = TABLE)
GO
COMMIT

Update Org_Settings_D set EnableBlockUserDI = 0



/*   Problem for ALLBUGS-51Problem - Channel Islands are Listed Incorrectly in myVRM -STARTS(27th Jan 2016)            */

--Delete Channel Islands State 129 From United Kingdom Country 224
Delete Gen_State_S where StateID=129

-- Updating Cheshire 130 For Existing data 129 State

Update Loc_Room_D set State=130 where State=129
Update Org_List_D set State=130 where State=129

SET IDENTITY_INSERT [Gen_State_S] ON
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2554, 244, N'JSY', N'Jersey', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2555, 244, N'GSY', N'Guernsey', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2556, 244, N'ALD', N'Alderney', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2557, 244, N'SRK', N'Sark', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2558, 244, N'HRM', N'Herm', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2559, 244, N'JTH', N'Jethou', 0)
INSERT [dbo].[Gen_State_S] ([StateID], [CountryID], [StateCode], [State], [Selected]) VALUES (2560, 244, N'BRQ', N'Brecqhou', 0)
SET IDENTITY_INSERT [Gen_State_S] OFF 

/*   Problem for ALLBUGS-51Problem - Channel Islands are Listed Incorrectly in myVRM -END(27th Jan 2016)            */

/*                              Features & Bugs for V2.9116.3.1  Ends(27th Jan 2016)            */
/*                              Features & Bugs for V2.9116.3.2  Starts(27th Jan 2016)            */