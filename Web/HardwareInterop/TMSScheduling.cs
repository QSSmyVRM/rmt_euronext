﻿//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.IO;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using System.Reflection;
using System.Security.Principal;
using RTC_v181.CiscoAuthService;
using RTC_v181.CiscoSchedulerService;
using System.Collections;



namespace NS_TMS
{

    public class TMSScheduling
    {
        #region Parameters
        internal bool isRecurring = false;
        internal string errMsg = null;
        int TMSconfid = 0;
        bool ret = false;
        string URL = "";
        
        //Myvrm General class declaration starts
        private NS_LOGGER.Log logger;
        private NS_MESSENGER.ConfigParams configParams;
        private NS_DATABASE.Database db;
        NS_MESSENGER.Party Party;
        List<NS_MESSENGER.Conference> confList = null;
        //Myvrm Ends


        // Cisco TMS Declaration Starts

        //Booking service asmx Starts
        BookingService Bookingservice;
        Conference conference;
        //Booking service asmx Ends


        //Remote Service Asmx Starts
        RemoteSetupService Remotesetup;
        //Remote Service Asmx Ends

        // Cisco TMS Declaration Ends


        #endregion

        #region constructor
        internal TMSScheduling(NS_MESSENGER.ConfigParams config)
        {
            //Myvrm Declaration Starts
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            db = new NS_DATABASE.Database(configParams);
            Party = new NS_MESSENGER.Party();
            //Myvrm Declaration Ends



        }
        #endregion

        #region General Method

        #region IntegrateTMSServer
        /// <summary>
        /// Booking service credential integration
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>

        private bool IntegrateTMSBookingService(NS_MESSENGER.MCU mcu, ref BookingService Bookingservice)
        {
            try
            {
                if (string.IsNullOrEmpty(mcu.sIp) || string.IsNullOrEmpty(mcu.sLogin) || string.IsNullOrEmpty(mcu.sPwd) || string.IsNullOrEmpty(mcu.sRPRMDomain)) //ZD 100971
                {
                    logger.Trace("TMS URL, Login,Password,Domain should not be empty");
                    return false;
                }
                URL = "http://" + mcu.sIp.Trim() + "/tms/external/Booking/BookingService.asmx";

                Bookingservice = new BookingService();

                logger.Trace("TMS Booking URL:" + URL);
                logger.Trace("Login:" + mcu.sLogin + " Password:" + mcu.sPwd + " Domain:" + mcu.sRPRMDomain);

                Bookingservice.Url = URL;

                NetworkCredential Credential = new NetworkCredential(mcu.sLogin, mcu.sPwd, mcu.sRPRMDomain);

                Bookingservice.Credentials = Credential;

                if (Bookingservice.ExternalAPIVersionSoapHeaderValue == null)
                    Bookingservice.ExternalAPIVersionSoapHeaderValue = new RTC_v181.CiscoSchedulerService.ExternalAPIVersionSoapHeader();
                Bookingservice.ExternalAPIVersionSoapHeaderValue.ClientVersionIn = 5;

            }
            catch (Exception ex)
            {
                logger.Trace("IntegrateBookingService Exception:" + ex.Message);
                return false;
            }
            return true;
        }
        #endregion

        #region IntegrateTMSRemoteService
        /// <summary>
        /// Remote service Credentail intergration
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        private bool IntegrateTMSRemoteService(NS_MESSENGER.MCU mcu, ref RemoteSetupService Remotesetup)
        {

            try
            {
                if (string.IsNullOrEmpty(mcu.sIp) || string.IsNullOrEmpty(mcu.sLogin) || string.IsNullOrEmpty(mcu.sPwd) || string.IsNullOrEmpty(mcu.sRPRMDomain)) //ZD 100971
                {
                    logger.Trace("TMS URL, Login,Password,Domain should not be empty");
                    return false;
                }

                URL = "http://" + mcu.sIp.Trim() + "/tms/external/Booking/remotesetup/remotesetupservice.asmx";

                NetworkCredential Credential = new NetworkCredential(mcu.sLogin, mcu.sPwd, mcu.sRPRMDomain);
                Remotesetup = new RemoteSetupService();

                logger.Trace("TMS Booking URL:" + URL);
                logger.Trace("Login:" + mcu.sLogin + " Password:" + mcu.sPwd + " Domain:" + mcu.sRPRMDomain);
                Remotesetup.Url = URL
                    ;
                Remotesetup.Credentials = Credential;
                if (Remotesetup.ExternalAPIVersionSoapHeaderValue == null)
                    Remotesetup.ExternalAPIVersionSoapHeaderValue = new RTC_v181.CiscoAuthService.ExternalAPIVersionSoapHeader();
                Remotesetup.ExternalAPIVersionSoapHeaderValue.ClientVersionIn = 5;

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("IntegrateRemoteService Exception:" + ex.Message);
                return false;
            }
        }
        //ZD 100369_MCU Start
        private bool IntegrateTMSMCUStatusCommand(NS_MESSENGER.MCU mcu, ref RemoteSetupService Remotesetup, int Timeout)
        {

            try
            {
                if (string.IsNullOrEmpty(mcu.sIp) || string.IsNullOrEmpty(mcu.sLogin) || string.IsNullOrEmpty(mcu.sPwd) || string.IsNullOrEmpty(mcu.sRPRMDomain)) //ZD 100971
                {
                    logger.Trace("TMS URL, Login,Password,Domain should not be empty");
                    return false;
                }

                URL = "http://" + mcu.sIp.Trim() + "/tms/external/Booking/remotesetup/remotesetupservice.asmx";

                NetworkCredential Credential = new NetworkCredential(mcu.sLogin, mcu.sPwd, mcu.sRPRMDomain);
                Remotesetup = new RemoteSetupService();

                logger.Trace("TMS Booking URL:" + URL);
                logger.Trace("Login:" + mcu.sLogin + " Password:" + mcu.sPwd + " Domain:" + mcu.sRPRMDomain);

                Remotesetup.Credentials = Credential;
                Remotesetup.Timeout = Timeout;
                if (Remotesetup.ExternalAPIVersionSoapHeaderValue == null)
                    Remotesetup.ExternalAPIVersionSoapHeaderValue = new RTC_v181.CiscoAuthService.ExternalAPIVersionSoapHeader();
                Remotesetup.ExternalAPIVersionSoapHeaderValue.ClientVersionIn = 5;

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("IntegrateRemoteService Exception:" + ex.Message);
                return false;
            }
        }
        //ZD 100369_MCU End
        #endregion

        #endregion

        #region PublicMethods


        #region TestMCUConnection
        /// <summary>
        /// TestMCUConnection using is Alive Calling RemoteTMS webservice 
        /// </summary>
        /// <returns></returns>
        internal bool TestMCUConnection(NS_MESSENGER.MCU MCU)
        {
            try
            {
                ret = false;
                ret = IntegrateTMSRemoteService(MCU, ref Remotesetup);
                if (!ret)
                {
                    logger.Trace("Error in Intergrating TMS Remote Service");
                    return false;
                }
                ret = false;
                ret = Remotesetup.IsAlive();
                if (!ret)
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("TestMCUConnection Block:" + ex.Message);
                return false;
            }
        }
        # endregion

        #region GetMCUTime
        /// <summary>
        /// GetMCUTime
        /// </summary>
        /// <param name="mcu"></param>
        /// <returns></returns>
        internal bool GetMCUTime(NS_MESSENGER.MCU mcu)
        {

            try
            {
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("GetMCUTime:" + ex.Message);
                return false;
            }
        }
        #endregion

        #region SetorUpdateConference
        /// <summary>
        /// SetConference and Update Conference
        /// </summary>
        /// <param name="InXML"></param>
        /// <param name="RecieveXML"></param>
        /// <returns></returns>
        internal bool SetorUpdateConference(NS_MESSENGER.Conference conf)
        {
            List<NS_MESSENGER.Conference> confs = new List<NS_MESSENGER.Conference>();
            ExternalConference extConf = new ExternalConference(); //ZD 100513
            int confid = 0, partyCount = 0;
            try
            {
                confList = new List<NS_MESSENGER.Conference>();
                confList.Add(conf);

                if (conf.iRecurring == 1 && isRecurring)
                {
                    DeleteSyncConference(conf);
                    db.FetchConf(ref confList, ref confs, NS_MESSENGER.MCU.eType.TMSScheduling, 0); //for new and edit conference need to send delStatus as 0 
                    confList = confs;

                }

                for (int cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    //ZD 100971 Starts
                    try
                    {


                        Bookingservice = new BookingService();

                        ret = false;
                        ret = IntegrateTMSBookingService(conf.cMcu, ref Bookingservice);
                        if (!ret)
                        {
                            logger.Trace("Error in Integrating TMS");
                            return false;
                        }
                        conference = new Conference();
                        Party = new NS_MESSENGER.Party();

                        conference = Bookingservice.GetDefaultConference();

                        DateTime StartDate = DateTime.Parse(confList[cnfCnt].dtStartDateTime.ToString());
                        DateTime EndDate = StartDate.AddMinutes(confList[cnfCnt].iDuration);

                        conference.ConferenceId = -1;
                        if (!string.IsNullOrEmpty(confList[cnfCnt].ESId))
                        {
                            Int32.TryParse(confList[cnfCnt].ESId, out confid);
                            conference.ConferenceId = confid;
                        }

                        conference.Title = confList[cnfCnt].sDbName;
                        conference.StartTimeUTC = StartDate.ToString("yyyy-MM-dd HH:mm:ssZ");
                        conference.EndTimeUTC = StartDate.AddMinutes(confList[cnfCnt].iDuration).ToString("yyyy-MM-dd HH:mm:ssZ");



                        Participant[] participants = new Participant[conf.qParties.Count];
                        logger.Trace("Party count: " + conf.qParties.Count.ToString());
                        System.Collections.IEnumerator partyEnumerator;
                        partyEnumerator = conf.qParties.GetEnumerator();
                        partyCount = conf.qParties.Count;

                        for (int i = 0; i < partyCount; i++)
                        {
                            // go to next party in the queue
                            partyEnumerator.MoveNext();

                            Party = new NS_MESSENGER.Party();
                            Party = (NS_MESSENGER.Party)partyEnumerator.Current;

                            // Create the elements of the array (the actual participants)
                            participants[i] = new Participant();

                            //ZD 104365 Starts
                            //ZD 101424
                            //if (Party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                            //    participants[i].ParticipantCallType = ParticipantType.IPVideo; // Dial-in video
                            //else
                            //    participants[i].ParticipantCallType = ParticipantType.IPVideo1; // Dial-out video

                            if (Party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_IN)
                            {
                                if (Party.etProtocol == NS_MESSENGER.Party.eProtocol.IP && Party.etCallType == NS_MESSENGER.Party.eCallType.VIDEO)
                                    participants[i].ParticipantCallType = ParticipantType.IPVideo; // Dial-in video
                                else if (Party.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN && Party.etCallType == NS_MESSENGER.Party.eCallType.VIDEO)
                                    participants[i].ParticipantCallType = ParticipantType.ISDNVideo; // Dial-in video
                                if (Party.etProtocol == NS_MESSENGER.Party.eProtocol.IP && Party.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                                    participants[i].ParticipantCallType = ParticipantType.IPTel; // Dial-in video
                                else if (Party.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN && Party.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                                    participants[i].ParticipantCallType = ParticipantType.Telephone;
                                else if (Party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP && Party.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                                    participants[i].ParticipantCallType = ParticipantType.SIPTel;
                                else if (Party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP && Party.etCallType == NS_MESSENGER.Party.eCallType.VIDEO)
                                    participants[i].ParticipantCallType = ParticipantType.SIP;
                            }
                            else if (Party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                            {
                                if (Party.etProtocol == NS_MESSENGER.Party.eProtocol.IP && Party.etCallType == NS_MESSENGER.Party.eCallType.VIDEO)
                                    participants[i].ParticipantCallType = ParticipantType.IPVideo1;
                                else if (Party.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN && Party.etCallType == NS_MESSENGER.Party.eCallType.VIDEO)
                                    participants[i].ParticipantCallType = ParticipantType.ISDNVideo1; // Dial-in video
                                if (Party.etProtocol == NS_MESSENGER.Party.eProtocol.IP && Party.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                                    participants[i].ParticipantCallType = ParticipantType.IPTel1;
                                else if (Party.etProtocol == NS_MESSENGER.Party.eProtocol.ISDN && Party.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                                    participants[i].ParticipantCallType = ParticipantType.Telephone1;
                                else if (Party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP && Party.etCallType == NS_MESSENGER.Party.eCallType.AUDIO)
                                    participants[i].ParticipantCallType = ParticipantType.SIPTel1;
                                else if (Party.etProtocol == NS_MESSENGER.Party.eProtocol.SIP && Party.etCallType == NS_MESSENGER.Party.eCallType.VIDEO)
                                    participants[i].ParticipantCallType = ParticipantType.SIP1;

                                logger.Trace("Party Call Type  :" + participants[i].ParticipantCallType);
                            }
                            //ZD 101424

                            //ZD 104365 Ends
                            participants[i].NameOrNumber = Party.sAddress;
                            logger.Trace("Party Address " + Party.sAddress);
                        }
                           

                        conference.Participants = participants;
                        //ZD 100513 Starts
                        if (confList[cnfCnt].iConfType == 2 && confList[cnfCnt].iOBTP == 1 && confList[cnfCnt].iWebExconf == 1)
                            conference.ConferenceType = ConferenceType.OneButtonToPush;
                        else
                            conference.ConferenceType = ConferenceType.AutomaticCallLaunch;
                        if (confList[cnfCnt].iConfType == 2 && confList[cnfCnt].iOBTP == 0 && confList[cnfCnt].iWebExconf == 1)
                        {
                            conference.DataConference = DataConferenceMode.Yes;
                            WebEx wet = new WebEx();
                            wet.TmsShouldUpdateMeeting = true;
                            extConf.WebEx = wet;
                            conference.ExternalConference = extConf;
                        }
                        //ZD 100513 Ends

                        //logger.Trace("TMS Confernece Details:" + conference.ap);
                        conference = Bookingservice.SaveConference(conference);

                        if (conference.ConferenceId > 0)
                        {
                            ret = db.UpdateConferenceExternalID(confList[cnfCnt].iDbID, confList[cnfCnt].iInstanceID, conference.ConferenceId.ToString(), "TMS", "");
                            if (!ret)
                            {
                                logger.Trace("Error in Update Conf ESID:" + conference.ConferenceId.ToString());
                                return false;
                            }
                            else
                                logger.Trace("Successfully Updated Conf ESID:" + conference.ConferenceId.ToString());
                            //ZD 100513 Starts
                            logger.Trace("ConfID,InstanceID,isRecuring,MeetingKey,HostURL,AttendeeURL" + confList[cnfCnt].iDbID.ToString() + "," + confList[cnfCnt].iInstanceID.ToString() + "," + confList[cnfCnt].iRecurring.ToString() + "," + "  ," + conference.WebConferencePresenterUri + "," + conference.WebConferenceAttendeeUri);
                            ret = false;
                            ret = db.UpdateConfWebEXMeetingKey(confList[cnfCnt].iDbID.ToString(), confList[cnfCnt].iInstanceID.ToString(), confList[cnfCnt].iRecurring.ToString(), "", conference.WebConferencePresenterUri, conference.WebConferenceAttendeeUri);
                            if (!ret)
                            {
                                logger.Trace("Error in Update Conf WebEx:" + conference.ConferenceId.ToString());
                                return false;
                            }
                            else
                                logger.Trace("Successfully Updated Conf WebEx:" + conference.ConferenceId.ToString());
                            //ZD 100513 Ends
                        }
                        else
                            logger.Trace("Error in Fetching Conf ESID from TMS:" + conference.ConferenceId.ToString());
                    }
                    catch (Exception ex)
                    {
                        logger.Trace("Detailed Error in creating conference :" + ex.InnerException.ToString() + "Message :" + ex.Message);

                       logger.Trace("Error in creating conference :" +ex.Message);

                       
                    }
                    //ZD 100971 Ends
                }
            }
            catch (Exception ex)
            {
                logger.Trace("SetConference" + ex.StackTrace);
                return false;
            }
            return true;

        }
        #endregion

        #region DeleteSyncConference
        /// <summary>
        /// DeleteSyncConference
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool DeleteSyncConference(NS_MESSENGER.Conference conf)
        {
            List<NS_MESSENGER.Conference> confListDelete = null;
            try
            {
                confListDelete = new List<NS_MESSENGER.Conference>();
                confListDelete.Add(conf);

                db.FetchSyncAdjustments(ref confListDelete, NS_MESSENGER.MCU.eType.TMSScheduling); //ZD 100221

                for (int cnfCnt = 0; cnfCnt < confListDelete.Count; cnfCnt++)
                {

                    confListDelete[cnfCnt].cMcu = conf.cMcu;
                    ret = false;
                    ret = DeleteScheduledConference(confListDelete[cnfCnt]);
                    if (!ret)
                        logger.Exception(100, "TerminateConference failed");
                    else
                        //ZD 100221 Starts
                        if (confListDelete[cnfCnt].iWebExconf != 1)
                            db.UpdateSyncStatus(confListDelete[cnfCnt]);
                    //ZD 100221 Ends
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Trace("TerminateConference:" + e.Message);
                return false;
            }
        }
        #endregion

        #region TerminateConference
        /// <summary>
        /// DeleteConferenceBy conference ID
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool TerminateConference(NS_MESSENGER.Conference conf)
        {
            try
            {
                confList = new List<NS_MESSENGER.Conference>();
                List<NS_MESSENGER.Conference> confs = new List<NS_MESSENGER.Conference>();
                confList.Add(conf);

                if (conf.iRecurring == 1 && isRecurring)
                {
                    db.FetchConf(ref confList, ref confs, NS_MESSENGER.MCU.eType.TMSScheduling, 1); //for delete mode need to send delStatus as 1
                    confList = confs;
                }

                for (int cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {

                    try
                    {
                        ret = false;
                        ret = IntegrateTMSBookingService(conf.cMcu, ref Bookingservice);
                        if (!ret)
                            logger.Trace("Error in Integrating TMS Booking Service");


                        if (!string.IsNullOrEmpty(conf.ESId))
                            int.TryParse(conf.ESId, out TMSconfid);
                        else
                            logger.Trace("TMS Conference ID is Null");

                        Bookingservice.EndConferenceById(TMSconfid);
                    }
                    catch (Exception)
                    {
                        logger.Trace("Error in deleteing the conference:" + confList[cnfCnt].iDbID);
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("TerminateConference Block" + ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region TerminateScheduledConference
        /// <summary>
        /// DeleteConferenceBy conference ID
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool DeleteScheduledConference(NS_MESSENGER.Conference conf)
        {
            //ZD 100971 Starts
            List<NS_MESSENGER.Conference> confs = new List<NS_MESSENGER.Conference>();

            try
            {
                confList = new List<NS_MESSENGER.Conference>();
                confList.Add(conf);

                if (conf.iRecurring == 1 && isRecurring)
                {
                    DeleteSyncConference(conf);
                    db.FetchConf(ref confList, ref confs, NS_MESSENGER.MCU.eType.TMSScheduling, 1); //for new and edit conference need to send delStatus as 0 
                    confList = confs;

                }

                for (int cnfCnt = 0; cnfCnt < confList.Count; cnfCnt++)
                {
                    //ZD 100971 Starts
                    try
                    {
                        ret = false;
                        ret = IntegrateTMSBookingService(conf.cMcu, ref Bookingservice);
                        if (!ret)
                        {
                            logger.Trace("Error in Integrating TMS Booking Service");
                            return false;
                        }

                        if (!string.IsNullOrEmpty(confList[cnfCnt].ESId))
                            int.TryParse(confList[cnfCnt].ESId, out TMSconfid);
                        else
                        {
                            logger.Trace("TMS Conference ID is Null");
                            return false;
                        }

                        Bookingservice.DeleteConferenceById(TMSconfid);
                    }
                    catch (Exception ex)
                    {

                        logger.Trace("Error in delete conference in TMS :" + ex.Message + "for conf id: " + confList[cnfCnt].iDbID + " Instance iD:" + confList[cnfCnt].iInstanceID);
                    }
                }
                //ZD 100971 Ends

                return true;
            }
            //ZD 100971 Ends
            catch (Exception ex)
            {
                logger.Trace("TerminateConference Block" + ex.StackTrace);
                return false;
            }
        }
        #endregion

        #region GetEndpointStatus
        /// <summary>
        /// GetEndpointStatus
        /// </summary>
        /// <param name="conf"></param>
        /// <param name="participant"></param>
        /// <returns></returns>
        internal bool GetEndpointStatus(NS_MESSENGER.Conference conf, ref NS_MESSENGER.Party participant)
        {
            DateTime StartDate = new DateTime();
            DateTime EndDate = new DateTime();
            int confESID = 0;
            string DomainUserName = "";
            try
            {

                Conference[] conference = new Conference[100];

                ret = false;
                ret = IntegrateTMSBookingService(conf.cMcu, ref Bookingservice);
                if (!ret)
                {
                    //logger.Trace("Error in Integrating TMS");
                    return false;

                }

                StartDate = DateTime.Parse(conf.dtStartDateTimeInUTC.ToString("yyyy-MM-ddTHH:mm:ss+00:00"));
                EndDate = DateTime.Parse(conf.dtStartDateTimeInUTC.AddMinutes(conf.iDuration).ToString("yyyy-MM-ddTHH:mm:ss+00:00"));
                int.TryParse(conf.ESId, out confESID);

                DomainUserName = conf.cMcu.sRPRMDomain + "\\" + conf.cMcu.sLogin;
                conference = Bookingservice.GetConferencesForUser(DomainUserName, StartDate, EndDate, ConferenceStatus.Ongoing);


                for (int i = 0; i < conference.Count(); i++)
                {
                    if (conference[0].ConferenceId == confESID)
                    {
                        db.UpdateParticipantGUID(conf.iDbID, conf.iInstanceID, participant.sAddress.Trim(), conf.ESId, (int)NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT);
                        participant.etStatus = NS_MESSENGER.Party.eOngoingStatus.PARTIAL_CONNECT;
                    }
                    else
                    {
                        db.UpdateParticipantGUID(conf.iDbID, conf.iInstanceID, participant.sAddress.Trim(), conf.ESId, (int)NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT);
                        participant.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                //logger.Trace("GetEndpointStatus:" + ex.Message);
                return false;
            }
        }
        #endregion

        # region GetConferenceDetails
        /// <summary>
        /// GetConferenceDetails
        /// </summary>
        /// <param name="conf"></param>
        /// <returns></returns>
        internal bool GetConferenceDetails(NS_MESSENGER.Conference conf)
        {
            int confid = 0;
            string DomainUserName = "";
            try
            {
                Bookingservice = new BookingService();

                ret = false;
                ret = IntegrateTMSBookingService(conf.cMcu, ref Bookingservice);
                if (!ret)
                {
                    logger.Trace("Error in Integrating TMS");
                    return false;
                }
                Conference[] confarray = new Conference[100];

                if (!string.IsNullOrEmpty(conf.ESId))
                {
                    Int32.TryParse(conf.ESId, out confid);

                    DomainUserName = conf.cMcu.sRPRMDomain + "\\" + conf.cMcu.sLogin;
                    confarray = Bookingservice.GetConferencesForUser(DomainUserName, DateTime.UtcNow, DateTime.UtcNow.AddMinutes(30), ConferenceStatus.Ongoing);

                    if (confarray.Count() > 0)
                    {
                        for (int i = 0; i < confarray.Count(); i++)
                        {
                            if (confarray[i].ConferenceId.ToString() == conf.ESId)
                            {
                                ret = false;

                                ret = db.UpdateConferenceStatus(conf.iDbID, conf.iInstanceID, NS_MESSENGER.Conference.eStatus.ONGOING);

                                if (!ret)
                                {
                                    logger.Trace("Error in Update conference status");
                                    return false;

                                }
                            }
                        }
                    }
                    else
                    {
                        logger.Trace("No Ongoing conference in TMS interface");
                        return false;
                    }
                }
                else
                {
                    logger.Trace("Conference Esid is empty");
                    return false;
                }



                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("GetConferenceDetails :" + ex.Message);
                return false;
            }
        }
        # endregion

        //ZD 100369_MCU Start
        #region FetchMCUDetails
        /// <summary>
        /// TestMCUConnection using is Alive Calling RemoteTMS webservice 
        /// </summary>
        /// <returns></returns>
        internal bool FetchMCUDetails(NS_MESSENGER.MCU MCU, int Timeout)
        {
            int pollstatus = (int)NS_MESSENGER.MCU.PollState.PASS;
            try
            {

                ret = false;
                ret = IntegrateTMSMCUStatusCommand(MCU, ref Remotesetup, Timeout);
                if (!ret)
                {
                    logger.Trace("Error in Intergrating TMS Remote Service");
                    pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                }
                else
                {
                    ret = false;
                    ret = Remotesetup.IsAlive();
                    if (!ret)
                        pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                }
                db.UpdateMcuStatus(MCU, pollstatus);
                return true;
            }
            catch (Exception ex)
            {
                logger.Trace("TestMCUConnection Block:" + ex.Message);
                pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                db.UpdateMcuStatus(MCU, pollstatus);
                return false;
            }
        }
        # endregion
        //ZD 100369_MCU End

        #endregion



    }
}
