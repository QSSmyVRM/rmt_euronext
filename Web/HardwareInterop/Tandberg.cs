//ZD 100147 Start
/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886 End
namespace NS_TANDBERG
{
    #region References
    using System;
    using System.Collections;
    using System.Text;
    using System.Net;    
    using System.IO;
    using System.Xml;
    
    #endregion

    class Tandberg
    {
        private NS_LOGGER.Log logger;
        internal string errMsg = null;
        private NS_MESSENGER.ConfigParams configParams;
        private NS_MESSENGER.MCU tandbergMCU;

        internal Tandberg(NS_MESSENGER.ConfigParams config)
        {
            configParams = new NS_MESSENGER.ConfigParams();
            configParams = config;
            logger = new NS_LOGGER.Log(configParams);
            tandbergMCU = new NS_MESSENGER.MCU();
        }

        /// <summary>
        /// XML string sent to the Tandberg MCU
        /// </summary>
        /// <param name="sendXML"></param>
        /// <param name="receiveXML"></param>
        /// <returns></returns>
        private bool SendTransaction(string command, string sendXML, ref string responseXML)
        {
            try
            {
                logger.Trace("SendXML = " + sendXML);
                if (sendXML == null)
                {
                    sendXML = "";
                }

                // HTTP connection
                string strURL = null;
                if (command == "command")
                {
                    if (sendXML.Length < 5)
                    {
                        logger.Exception(100, "Invalid SendXML.");
                        return false;
                    }
                    else
                    {
                        strURL = "http://" + this.tandbergMCU.sIp + "/putxml";
                    }
                }
                else
                {
                    strURL = "http://" + this.tandbergMCU.sIp + "/status.xml";
                }

                logger.Trace("Connecting to MCU... " + strURL);
                Uri mcuUri = new Uri(strURL);
                NetworkCredential mcuCredential = new NetworkCredential(this.tandbergMCU.sLogin, this.tandbergMCU.sPwd);
                CredentialCache mcuCredentialCache = new CredentialCache();
                mcuCredentialCache.Add(mcuUri, "Basic", mcuCredential);
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strURL);
                req.Credentials = mcuCredentialCache;

                // send data
                logger.Trace("Sending data...");
                req.ContentType = "application/xml";
                req.Method = "POST";
                System.IO.Stream stream = req.GetRequestStream();
                byte[] arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sendXML);
                stream.Write(arrBytes, 0, arrBytes.Length);
                stream.Close();

                // receive response data
                logger.Trace("Receving response back...");
                WebResponse resp = req.GetResponse();
                Stream respStream = resp.GetResponseStream();
                StreamReader rdr = new StreamReader(respStream, System.Text.Encoding.ASCII);
                responseXML = rdr.ReadToEnd();

                logger.Trace("ResponseXML = " + responseXML);

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                logger.Exception(100, "### Error sending data to bridge. Error = " + e.Message);
                return false;
            }
        }

        //ZD 100369_MCU Start
        private bool SendMCUStatusCommand(string command, string sendXML, ref string responseXML, int Timeout)
        {
            try
            {
                logger.Trace("SendXML = " + sendXML);
                if (sendXML == null)
                {
                    sendXML = "";
                }

                // HTTP connection
                string strURL = null;
                if (command == "command")
                {
                    if (sendXML.Length < 5)
                    {
                        logger.Exception(100, "Invalid SendXML.");
                        return false;
                    }
                    else
                    {
                        strURL = "http://" + this.tandbergMCU.sIp + "/putxml";
                    }
                }
                else
                {
                    strURL = "http://" + this.tandbergMCU.sIp + "/status.xml";
                }

                logger.Trace("Connecting to MCU... " + strURL);
                Uri mcuUri = new Uri(strURL);
                NetworkCredential mcuCredential = new NetworkCredential(this.tandbergMCU.sLogin, this.tandbergMCU.sPwd);
                CredentialCache mcuCredentialCache = new CredentialCache();
                mcuCredentialCache.Add(mcuUri, "Basic", mcuCredential);
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(strURL);
                req.Credentials = mcuCredentialCache;
                req.Timeout = Timeout;
                // send data
                logger.Trace("Sending data...");
                req.ContentType = "application/xml";
                req.Method = "POST";
                System.IO.Stream stream = req.GetRequestStream();
                byte[] arrBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sendXML);
                stream.Write(arrBytes, 0, arrBytes.Length);
                stream.Close();

                // receive response data
                logger.Trace("Receving response back...");
                WebResponse resp = req.GetResponse();
                Stream respStream = resp.GetResponseStream();
                StreamReader rdr = new StreamReader(respStream, System.Text.Encoding.ASCII);
                responseXML = rdr.ReadToEnd();

                logger.Trace("ResponseXML = " + responseXML);

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                logger.Exception(100, "### Error sending data to bridge. Error = " + e.Message);
                return false;
            }
        }
        //ZD 100369_MCU End


        private bool GetStatus(ref string responseXML)
        {
            try
            {
                // get the "full" status

                // no need to create xml. 
                string statusXML = null;

                // send transaction
                bool ret = SendTransaction("status", statusXML, ref responseXML);
                if (!ret)
                {
                    logger.Trace("Invalid response.");
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool GetFirstAvailableConferenceSlotOnMcu(ref int confSlotOnMcu)
        {
            try
            {
                string responseXML = null;

                // Get Status
                bool ret = GetStatus(ref responseXML);
                if (!ret)
                {
                    logger.Trace("Invalid status response.");
                    return false;
                }

                // Process XML
                ret = false;
                ret = ProcessXML_GetFirstAvailableConferenceSlotOnMcu(responseXML, ref confSlotOnMcu);
                if (!ret)
                {
                    logger.Trace("Invalid response.");
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool ProcessXML_GetFirstAvailableConferenceSlotOnMcu(string responseXML, ref int confSlotOnMcu)
        {
            try
            {
                // load xml
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(responseXML);
                XmlNamespaceManager nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsMgr.AddNamespace("def", "http://www.tandberg.no/XML/CUIL/1.0");

                // search for conf slots which have status = "not started"
                XmlNodeList nodes = xmlDoc.SelectNodes("/def:Status/def:Conference[@status=\"NotStarted\"]", nsMgr);
                foreach (XmlNode node in nodes)
                {
                    // get the conf slot number
                    confSlotOnMcu = Int32.Parse(node.Attributes["item"].InnerXml.Trim());
                    return true;
                }

                //conf setup was unsuccessful
                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool ProcessXML_TestMcuConnection(string responseXML, string specifiedMcuSoftwareVersion)
        {
            try
            {
                // load xml
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(responseXML);
                XmlNamespaceManager nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsMgr.AddNamespace("def", "http://www.tandberg.no/XML/CUIL/1.0");

                // search for conf slots which have status = "not started"
                XmlNodeList nodes = xmlDoc.SelectNodes("/def:Status/def:SystemUnit/def:Software/def:Version/text()", nsMgr);
                foreach (XmlNode node in nodes)
                {
                    string actualSoftwareVersion = node.OuterXml.Trim();
                    logger.Trace("ActualSoftwareVersion = " + actualSoftwareVersion);
                    if (actualSoftwareVersion.Contains(specifiedMcuSoftwareVersion))
                    {
                        // version matches
                        logger.Trace("Version matches!");
                        return true;
                    }
                }

                this.errMsg = "Version doesn't match!";
                logger.Trace(this.errMsg);
                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool ProcessXML_MuteParty(string responseXML)
        {
            try
            {
                if (responseXML.IndexOf("OK") > 0)
                {
                    // mute/unmute command was successful
                    return true;
                }
                else
                {
                    // unsuccessful
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    this.errMsg = xd.SelectSingleNode("/Command/CallMuteResult[1][@status=\"Error\"]/Description").InnerText.Trim();
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool ProcessXML_DisconnectParty(string responseXML)
        {
            try
            {
                if (responseXML.IndexOf("OK") > 0)
                {
                    // success
                    return true;
                }
                else
                {
                    // error
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    this.errMsg = xd.SelectSingleNode("/Command/DisconnectCallResult[1][@status=\"Error\"]/Description").InnerText.Trim();
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool ProcessXML_TerminateConference(string responseXML)
        {
            try
            {
                if (responseXML.IndexOf("OK") > 0)
                {
                    // command was successful
                    return true;
                }
                else
                {
                    // command was unsuccessful
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    this.errMsg = xd.SelectSingleNode("/Command/ConferenceTerminateResult[1][@status=\"Error\"]/Description").InnerText.Trim();
                    return false;
                }
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool SetConference(NS_MESSENGER.Conference conf)
        {
            try
            {
                // Check that conf start time is past the current UTC time.
                // No future confs are allowed as Tandberg MCU doesn't support it.
                if (DateTime.UtcNow.CompareTo(conf.dtStartDateTimeInUTC) < 0)
                {
                    this.errMsg = "Only ongoing conferences can be pushed on to Tandberg MCU. Please try again at the conference start time.";
                    return false;
                }

                // Assign the tandberg mcu
                this.tandbergMCU = conf.cMcu;

                // find how many confs are ongoing
                int confSequenceIdOnMcu = 0;
                bool ret = GetFirstAvailableConferenceSlotOnMcu(ref confSequenceIdOnMcu);
                if (!ret)
                {
                    this.errMsg = "Conference slot not available on MCU.";
                    return false;
                }

                // create reservation xml
                string confXML = null; ret = false;
                ret = CreateXML_Reservation(conf, confSequenceIdOnMcu, ref confXML);
                if (!ret)
                {
                    this.errMsg = "Invalid conference data.";
                    return false;
                }

                // send transaction
                ret = false; string responseXML = null;
                ret = SendTransaction("command", confXML, ref responseXML);
                if (!ret)
                {
                    this.errMsg = "Transaction failed.";
                    return false;
                }

                // process response xml 
                ret = false;
                ret = ProcessXML_Reservation(responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid response.";
                    return false;
                }

                // modify the conference 
                confXML = null; ret = false;
                ret = CreateXML_ModifyConference(conf, confSequenceIdOnMcu, true, true, ref confXML);
                if (!ret)
                {
                    this.errMsg = "Invalid xml.";
                    return false;
                }

                // send transaction
                ret = false; responseXML = null;
                ret = SendTransaction("command", confXML, ref responseXML);
                if (!ret)
                {
                    this.errMsg = "Transaction failed.";
                    return false;
                }

                // process response xml 
                ret = false;
                ret = ProcessXML_ModifyConference(responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid response from MCU.";
                    return false;
                }

                // add the participants 
                while (conf.qParties.Count > 0)
                {
                    // get the participant
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();
                    party = (NS_MESSENGER.Party)conf.qParties.Dequeue();

                    // Requested by Dan for Yorktel - Disney (Jan, 29 2010)
                    // check line rate of party with conference. if conference is lesser than party, then use conference line rate. Else, don't change anything.
                    if (conf.stLineRate.etLineRate < party.stLineRate.etLineRate)
                    {
                        party.stLineRate.etLineRate = conf.stLineRate.etLineRate;
                    }

                    if (party.etConnType == NS_MESSENGER.Party.eConnectionType.DIAL_OUT)
                    {
                        // add the participant on the mcu only if its in DIAL-OUT mode
                        logger.Trace("Adding participant: " + party.sName);
                        ret = false;
                        ret = AddPartyToMcu(conf, party, confSequenceIdOnMcu);
                        if (!ret)
                        {
                            // keep continuing
                            logger.Trace("Error adding the participant.");
                        }
                    }
                }
                return true;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool CreateXML_ModifyConference(NS_MESSENGER.Conference conf, int confSequenceIdOnMcu, bool changeDuration, bool changeDisplayLayout, ref string confXML)
        {
            try
            {
                confXML = "<Command>";
                confXML += "<ConferenceModify>";
                confXML += "<Conference>" + confSequenceIdOnMcu.ToString() + "</Conference>";
                if (changeDisplayLayout)
                {
                    string newTandbergVideoLayout = "Auto";
                    bool ret = EquateVideoLayout(conf.iVideoLayout, ref newTandbergVideoLayout);
                    if (!ret)
                    {
                        logger.Trace("Error in video layout conversion.");
                    }
                    confXML += "<PictureMode>" + newTandbergVideoLayout + "</PictureMode>";
                }
                else
                {
                    confXML += "<PictureMode/>";
                }
                confXML += "<VideoFormat/>";
                confXML += "<CustomFormats/>";
                confXML += "<AGC/>";
                confXML += "<AllowIncomingCalls/>";
                if (changeDuration)
                {
                    confXML += "<Duration>" + conf.iDuration.ToString() + "</Duration>";
                }
                else
                {
                    confXML += "<Duration/>";
                }
                confXML += "<MaxAudioSites/>";
                confXML += "<MaxVideoSites/>";
                confXML += "<EntryExitTones/>";
                confXML += "<LegacyLevel/>";
                confXML += "<TelephoneFilter/>";
                confXML += "<FloorToFull/>";
                confXML += "<BandwidthThreshold/>";
                confXML += "<WebCallListTimeout/>";
                confXML += "<PhoneIndication/>";
                confXML += "<SpeakerIndication/>";
                confXML += "<VideoText/>";
                confXML += "<VideoTextTimeout/>";
                confXML += "<ChairControl/>";
                confXML += "<LectureMode/>";
                confXML += "<NetErrorHandling/>";
                confXML += "<IPLRRobustMode/>";
                confXML += "<FURBlockSites/>";
                confXML += "<FURFilterInterval/>";
                confXML += "<VoiceSwitchTimeout/>";
                confXML += "<FarTlphEchoSupression/>";
                confXML += "<OptimalVideoQuality/>";
                confXML += "<EncoderSelectionPolicy/>";
                confXML += "<BandwidthManagement/>";
                confXML += "<WebSnapshots/>";
                confXML += "</ConferenceModify>";
                confXML += "</Command>";

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                logger.Exception(100, e.Message);
                return false;
            }

        }
        private bool CreateXML_AddParty(NS_MESSENGER.Party party, int confSequenceIdOnMcu, ref string addPartyXML)
        {
            try
            {
                addPartyXML = "<Command>";
                addPartyXML += "<Dial>";
                addPartyXML += "<Conference>" + confSequenceIdOnMcu.ToString().Trim() + "</Conference>";
                addPartyXML += "<Number>" + party.sAddress.Trim() + "</Number>";
                addPartyXML += "<SecondNumber/>";
                addPartyXML += "<SubAddress/>";
                // line rate 
                //int tandbergLineRate = 384;
                //EquateLineRate(conf.etLineRate, ref tandbergLineRate);
                addPartyXML += "<CallRate/>"; // + +"</CallRate>";
                addPartyXML += "<Restrict/>";
                addPartyXML += "<NetProfile/>";
                addPartyXML += "<NetworkId/>";
                addPartyXML += "<NetworkModule/>";
                addPartyXML += "<DTMFSend/>";
                addPartyXML += "</Dial>";
                addPartyXML += "</Command>";

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool CreateXML_Reservation(NS_MESSENGER.Conference conf, int confSequenceIdOnMcu, ref string confXML)
        {
            try
            {
                confXML = "<Command>";
                confXML += "<ConferenceStart>";
                confXML += "<Conference>" + confSequenceIdOnMcu.ToString() + "</Conference>";
                confXML += "<Name>" + conf.sDbName + "</Name>";

                // line rate 
                int tandbergLineRate = 384;
                EquateLineRate(conf.stLineRate.etLineRate, ref tandbergLineRate);
                confXML += "<CallRate>" + tandbergLineRate.ToString() + "</CallRate>";
                // restricted call 
                confXML += "<Restrict/>";                
                // password
                confXML += "<Password>" + conf.sPwd + "</Password>";

                // special version-specific attributes 
                if (this.tandbergMCU.sSoftwareVer.Contains("J"))
                {
                    // only for the "J4.0" version
                    confXML += "<PasswordOnOutgoingCalls/>";
                    confXML += "<Encryption/>";
                    confXML += "<EncryptionMode/>";
                    confXML += "<EncryptionType/>";
                    confXML += "<SecondaryRate/>";
                    confXML += "<WelcomeMessage/>";
                    confXML += "<DuoVideo/>";
                    confXML += "<AudioG728/>";
                    confXML += "<CascadingPreference/>";
                    confXML += "<BillingCode/>";
                    confXML += "<CPAutoSwitch/>";
                    confXML += "<NetworkId/>";
                    confXML += "<ConferenceSelfview/>";
                    confXML += "<Protect/>";
                    confXML += "<TemplateReference/>";
                    confXML += "<HDEnabled/>";
                }
                else
                {
                    // only for the "D3.9" version
                    confXML += "<Encryption/>";
                    confXML += "<EncryptionMode/>";
                    confXML += "<WelcomeMessage/>";
                    confXML += "<DuoVideo/>";
                    confXML += "<AudioG728/>";
                    confXML += "<CascadingPreference/>";
                }
                confXML += "</ConferenceStart>";
                confXML += "</Command>";

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                logger.Exception(100, e.Message);
                return false;
            }

        }

        /// <summary>
        /// Process the response to check if conference has been successfully saved in the MCU
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ProcessXML_Reservation(string responseXML)
        {
            try 
            {
                // check if conf setup was unsuccessful
                if (responseXML.IndexOf("Error") > 0)
                {
                    if (this.tandbergMCU.sSoftwareVer.Contains("J"))
                    {                        
                        XmlDocument xd = new XmlDocument();
                        xd.LoadXml(responseXML);
                        this.errMsg = xd.SelectSingleNode("/Command/ConferenceStartResult[1][@status=\"Error\"]/Description").InnerText.Trim();
                        return false;
                    }
                    else
                    {
                        this.errMsg = "Invalid response from the MCU.";
                        return false;
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                logger.Exception(100, e.Message);
                return false;
            }
        }

        /// <summary>
        /// Process the response to check if participant has been successfully added to the conference in the MCU
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ProcessXML_AddParty(string responseXML)
        {
            try
            {
                if (responseXML.IndexOf("OK") > 0)
                {
                    // conf setup successful
                    return true;
                }
                else
                {
                    //conf setup unsuccessful
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    this.errMsg = xd.SelectSingleNode("/Command/DialResult[1][@status=\"Error\"]/Description").InnerText.Trim();
                    return false;
                }
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                logger.Exception(100, e.Message);
                return false;
            }
        }


        /// <summary>
        /// Process the response to check if conference has been successfully modified in the MCU
        /// </summary>
        /// <param name="responseXML"></param>
        /// <returns></returns>
        private bool ProcessXML_ModifyConference(string responseXML)
        {
            try
            {
                if (responseXML.IndexOf("OK") > 0)
                {
                    // conf setup successful
                    return true;
                }
                else
                {
                    //conf setup unsuccessful
                    XmlDocument xd = new XmlDocument();
                    xd.LoadXml(responseXML);
                    this.errMsg = xd.SelectSingleNode("/Command/ConferenceModifyResult[1][@status=\"Error\"]/Description").InnerText.Trim();
                    return false;
                }
            }
            catch (Exception e)
            {
                this.errMsg = e.Message;
                logger.Exception(100, e.Message);
                return false;
            }
        }


        /// <summary>
        /// Convert myVRM line rate to Tandberg format
        /// </summary>
        /// <param name="lineRate"></param>
        /// <param name="tandbergLineRate"></param>
        /// <returns></returns>
        private bool EquateLineRate(NS_MESSENGER.LineRate.eLineRate lineRate, ref int tandbergLineRate)
        {
            try
            {
                // equating VRM to Tandberg values
                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K64)
                    tandbergLineRate = 64;
                else
                {
                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.K128)
                        tandbergLineRate = 128;
                    else
                    {
                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.K256)
                            tandbergLineRate = 256;
                        else
                        {
                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.K512)
                                tandbergLineRate = 512;
                            else
                            {
                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.K768)
                                    tandbergLineRate = 768;
                                else
                                {
                                    if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1152)
                                        tandbergLineRate = 1152;
                                    else
                                    {
                                        if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1472)
                                            tandbergLineRate = 1472;
                                        else
                                        {
                                            if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1536)
                                                tandbergLineRate = 1536;
                                            else
                                            {
                                                if (lineRate == NS_MESSENGER.LineRate.eLineRate.M1920)
                                                    tandbergLineRate = 1920;
                                            }
                                        }

                                    }
                                }
                            }
                        }
                    }
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool EquateVideoLayout(int videoLayoutID, ref string tandbergVideoLayoutValue)
        {
            tandbergVideoLayoutValue = "Auto";
            try
            {
                switch (videoLayoutID)
                {
                    case 1:
                        {
                            tandbergVideoLayoutValue = "Auto";
                            break;
                        }
                    case 2:
                        {
                            tandbergVideoLayoutValue = "4Split"; //2x2
                            break;
                        }
                    case 3:
                        {
                            tandbergVideoLayoutValue = "9Split"; //3x3
                            break;
                        }
                    case 4:
                        {
                            tandbergVideoLayoutValue = "Auto"; //???
                            break;
                        }
                    case 5:
                        {
                            tandbergVideoLayoutValue = "Auto";//"1and5"; 
                            break;
                        }
                    case 6:
                        {
                            tandbergVideoLayoutValue = "Auto"; //???
                            break;
                        }
                    case 7:
                        {
                            tandbergVideoLayoutValue = "Auto"; //???
                            break;
                        }
                    case 8:
                        {
                            tandbergVideoLayoutValue = "Auto"; //???
                            break;
                        }
                    case 9:
                        {
                            tandbergVideoLayoutValue = "Auto"; //???
                            break;
                        }
                    case 10:
                        {
                            tandbergVideoLayoutValue = "Auto"; //"1and7";
                            break;
                        }
                    case 11:
                        {
                            tandbergVideoLayoutValue = "Auto"; //"1and7";
                            break;
                        }
                    case 12:
                        {
                            tandbergVideoLayoutValue = "Auto"; //"1and7";
                            break;
                        }
                    /*                    case 13:
                                            {
                                                tandbergVideoLayoutValue = "1and2HorUpper";
                                                break;
                                            }
                                        case 14:
                                            {
                                                tandbergVideoLayoutValue = "1and3HorUpper";
                                                break;
                                            }
                                        case 15:
                                            {
                                                tandbergVideoLayoutValue = "1and4HorUpper";
                                                break;
                                            }
                                        case 16:
                                            {
                                                // Changed from 1x2Ver to 1x2 on BTBoces request (case 1606)
                                                tandbergVideoLayoutValue = "1x2";
                                                break;
                                            }
                                        case 17:
                                            {
                                                tandbergVideoLayoutValue = "1and2Ver";
                                                break;
                                            }
                                        case 18:
                                            {
                                                tandbergVideoLayoutValue = "1and3Ver";
                                                break;
                                            }
                                        case 19:
                                            {
                                                tandbergVideoLayoutValue = "1and4Ver";
                                                break;
                                            }
                                        case 20:
                                            {
                                                tandbergVideoLayoutValue = "1and8Lower";
                                                break;
                                            }

                                        //case 21-23 : layout is not supported
                                        case 24:
                                            {
                                                tandbergVideoLayoutValue = "1and8Central";
                                                break;
                                            }
                                        case 25:
                                            {
                                                tandbergVideoLayoutValue = "2and8";
                                                break;
                                            }
                                        //case 26-32: layout is not supported					
                                        case 33:
                                            {
                                                tandbergVideoLayoutValue = "1and12";
                                                break;
                                            }
                                        //case 33-42: layout is not supported					

                                        //case 101-109 are all custom to btboces for MGC accord bridges
                                        case 101:
                                            {
                                                tandbergVideoLayoutValue = "1x1";
                                                break;
                                            }
                                        case 102:
                                            {
                                                tandbergVideoLayoutValue = "1x2";//"1x2Ver"; 
                                                break;
                                            }
                                        case 103:
                                            {
                                                tandbergVideoLayoutValue = "1and2HorUpper"; //"1x2"; 
                                                break;
                                            }
                                        case 104:
                                            {
                                                tandbergVideoLayoutValue = "2x2"; //"1and2HorUpper"; 
                                                break;
                                            }
                                        case 105:
                                            {
                                                tandbergVideoLayoutValue = "1and5"; //"1and2Ver"; 
                                                break;
                                            }
                                        case 106:
                                            {
                                                tandbergVideoLayoutValue = "1x2Ver"; //"2x2"; 
                                                break;
                                            }
                                        case 107:
                                            {
                                                tandbergVideoLayoutValue = "1and2Ver";//"1and3Ver"; 
                                                break;
                                            }
                                        case 108:
                                            {
                                                tandbergVideoLayoutValue = "1and3Ver"; //"1and5"; 
                                                break;
                                            }
                                        case 109:
                                            {
                                                tandbergVideoLayoutValue = "1and7";
                                                break;
                                            }
                    */
                    default:
                        {
                            // layout not supported
                            this.errMsg = "This video layout is not supported on Tandberg MCU.";
                            return false;
                        }
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return (false);
            }
        }

        internal bool TestMCUConnection(NS_MESSENGER.MCU mcu)
        {
            try
            {
                // assign the mcu
                this.tandbergMCU = mcu;

                // Get Status
                string responseXML = null;
                bool ret = GetStatus(ref responseXML);
                if (!ret)
                {
                    logger.Trace("Invalid status response.");
                    this.errMsg = "Connection to MCU failed. Please recheck the MCU information. Message : " + this.errMsg;
                    return false;
                }

                // Process XML
                ret = false;
                ret = ProcessXML_TestMcuConnection(responseXML, mcu.sSoftwareVer);
                if (!ret)
                {
                    logger.Trace("Invalid response.");
                    this.errMsg = "Connection to MCU failed. Please recheck the MCU information. Message : " + this.errMsg;
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        private bool FindOngoingConference(NS_MESSENGER.Conference conf, ref int confIdOnMcu)
        {
            try
            {
                logger.Trace("Entering FindOngoingConference...");

                // get complete "status" from mcu
                string responseXML = null;
                bool ret = GetStatus(ref responseXML);
                if (!ret)
                {
                    logger.Trace("Invalid status response.");
                    return false;
                }

                // find the "conf id on mcu" from the list of ongoing confs
                logger.Trace("Searching for active/started confs in the status xml...");
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(responseXML);
                XmlNamespaceManager nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsMgr.AddNamespace("def", "http://www.tandberg.no/XML/CUIL/1.0");
                XmlNodeList nodes = xmlDoc.SelectNodes("/def:Status/def:Conference[@status=\"Started\" or @status=\"Active\"]", nsMgr);
                logger.Trace("Total confs found: " + nodes.Count.ToString());
                foreach (XmlNode node in nodes)
                {
                    logger.Trace("---Node found: " + node.OuterXml.Trim());
                    logger.Trace("Replace the xmlns with blank space..");
                    string nodeStr = node.OuterXml.Trim().Replace("xmlns=\"http://www.tandberg.no/XML/CUIL/1.0\"", "");
                    XmlDocument xmlDoc1 = new XmlDocument();
                    xmlDoc1.LoadXml(nodeStr);                    
                    try
                    {
                        string confNameOnMcu= xmlDoc1.SelectSingleNode("/Conference/Properties/Name").InnerText.Trim();
                        logger.Trace("Conf name on mcu : " + confNameOnMcu);
                        logger.Trace("Conf name in db : " + conf.sDbName);
                        if (confNameOnMcu.Trim() == conf.sDbName.Trim())
                        {
                            logger.Trace("Conf name on mcu matches with db...");
                            confIdOnMcu = Int32.Parse(node.Attributes["item"].InnerText.Trim());
                            logger.Trace("---ConfIdOnMcu = " + confIdOnMcu.ToString());
                            return true;
                        }
                        else
                        {
                            logger.Trace("Conference doesn't match !");
                        }
/*                        int nameStart = node.OuterXml.Trim().IndexOf("<Name item=\"1\">") + 15;
                        int nameEnd = node.OuterXml.Trim().IndexOf("</Name>");
                        string confNameOnMcu = node.OuterXml.Trim().Substring(nameStart, (nameEnd - nameStart));
                        logger.Trace("Extract conf name:" + confNameOnMcu);

                        logger.Trace("Compare with db name: " + conf.sDbName);

                        if (confNameOnMcu.Trim() == conf.sDbName.Trim())
                        {
                            // conf name matches                            
                            // now get the slot number on the mcu
                            logger.Trace("Conference matches !");
                            int confSlotStart = node.OuterXml.Trim().IndexOf("<Conference item=\"") + 1;
                            int confSlotEnd = node.OuterXml.Trim().IndexOf("\" status");
                            confIdOnMcu = Int32.Parse(node.OuterXml.Trim().Substring(confSlotStart, (confSlotEnd - confSlotStart)));
                            logger.Trace("---ConfIdOnMcu = " + confIdOnMcu.ToString());
                            return true;
                        }
                        else
                        {
                            logger.Trace("Conference doesn't match !");
                        }
 */
                    }
                    catch (Exception e)
                    {
                        logger.Exception(100,e.Message);
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool FindOngoingConferenceList(ref Queue confList)
        {
            try
            {
                logger.Trace("Entering FindOngoingConferenceList...");

                // get complete "status" from mcu
                string responseXML = null;
                bool ret = GetStatus(ref responseXML);
                if (!ret)
                {
                    logger.Trace("Invalid status response.");
                    return false;
                }

                // find the "conf id on mcu" from the list of ongoing confs
                logger.Trace("Searching for active/started confs in the status xml...");
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(responseXML);
                XmlNamespaceManager nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);
                nsMgr.AddNamespace("def", "http://www.tandberg.no/XML/CUIL/1.0");
                XmlNodeList nodes = xmlDoc.SelectNodes("/def:Status/def:Conference[@status=\"Started\" or @status=\"Active\"]", nsMgr);
                logger.Trace("Total confs found: " + nodes.Count.ToString());
                foreach (XmlNode node in nodes)
                {
                    logger.Trace("---Node found: " + node.OuterXml.Trim());
                    logger.Trace("Replace the xmlns with blank space..");
                    string nodeStr = node.OuterXml.Trim().Replace("xmlns=\"http://www.tandberg.no/XML/CUIL/1.0\"", "");
                    XmlDocument xmlDoc1 = new XmlDocument();
                    xmlDoc1.LoadXml(nodeStr);
                    try
                    {
                        // active ongoing conf
                        NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                        conf.sMcuName = xmlDoc1.SelectSingleNode("/Conference/Properties/Name").InnerText.Trim();
                        logger.Trace("Conf name on mcu : " + conf.sMcuName);
                        conf.iMcuID = Int32.Parse(node.Attributes["item"].InnerText.Trim());
                        logger.Trace("---ConfIdOnMcu = " + conf.iMcuID.ToString());
                        
                        // add to queue
                        confList.Enqueue(conf);
                        return true;
                    }
                    catch (Exception e)
                    {
                        logger.Exception(100, e.Message);
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool FindOngoingParticipant(NS_MESSENGER.Party party, int confIdOnMcu, ref int partyIdOnMcu)
        {
            try
            {
                // get complete "status" from mcu
                string responseXML = null;
                bool ret = GetStatus(ref responseXML);
                if (!ret)
                {
                    logger.Trace("Invalid status response.");
                    return false;
                }

                // find the "party id on mcu" from the list of ongoing calls(connected participants)
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(responseXML);
                //XmlNamespaceManager nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);
                //nsMgr.AddNamespace("def", "http://www.tandberg.no/XML/CUIL/1.0");
                //XmlNodeList nodes = xmlDoc.SelectNodes("/def:Status/def:Call[1][@status=\"Synced\"][@conferenceRef=\"" + confIdOnMcu.ToString() + "\"]", nsMgr);
                XmlNodeList nodes = xmlDoc.SelectNodes("/Status/Call[1][@status=\"Synced\"][@conferenceRef=\"" + confIdOnMcu.ToString() + "\"]");
                logger.Trace("Total count of matching nodes: " + nodes.Count.ToString());
                
                foreach (XmlNode node in nodes)
                {
                    logger.Trace("---Node found: " + node.OuterXml.Trim());
                    logger.Trace("Replace the xmlns with blank space..");
                    string nodeStr = node.OuterXml.Trim().Replace("xmlns=\"http://www.tandberg.no/XML/CUIL/1.0\"", "");
                    XmlDocument xmlDoc1 = new XmlDocument();
                    xmlDoc1.LoadXml(nodeStr);       
                    try
                    {                        
                        string endpointAddressOnMcu = xmlDoc1.SelectSingleNode("/Call/RemoteNumber").InnerText.Trim();
                        if (endpointAddressOnMcu == party.sAddress)
                        {
                            // party matches
                            // now get the slot number on the mcu
                            partyIdOnMcu = Int32.Parse(node.Attributes["item"].InnerXml.Trim());
                            logger.Trace("--Party ID on MCU : " + partyIdOnMcu.ToString());

                            return true;
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Trace("Error getting party details out from status xml. Message = " + e.Message);
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        
        private bool FindOngoingParticipantList(NS_MESSENGER.Conference conf , ref Queue partyq)
        {
            try
            {
                // get complete "status" from mcu
                string responseXML = null;
                bool ret = GetStatus(ref responseXML);
                if (!ret)
                {
                    logger.Trace("Invalid status response.");
                    return false;
                }

                // find the "party id on mcu" from the list of ongoing calls(connected participants)
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(responseXML);
                //XmlNamespaceManager nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);
                //nsMgr.AddNamespace("def", "http://www.tandberg.no/XML/CUIL/1.0");
                //XmlNodeList nodes = xmlDoc.SelectNodes("/def:Status/def:Call[1][@status=\"Synced\"][@conferenceRef=\"" + confIdOnMcu.ToString() + "\"]", nsMgr);
                XmlNodeList nodes = xmlDoc.SelectNodes("/Status/Call[1][@status=\"Synced\"][@conferenceRef=\"" + conf.iMcuID.ToString() + "\"]");
                logger.Trace("Total count of matching nodes: " + nodes.Count.ToString());

                foreach (XmlNode node in nodes)
                {
                    NS_MESSENGER.Party party = new NS_MESSENGER.Party();

                    logger.Trace("---Node found: " + node.OuterXml.Trim());
                    logger.Trace("Replace the xmlns with blank space..");
                    string nodeStr = node.OuterXml.Trim().Replace("xmlns=\"http://www.tandberg.no/XML/CUIL/1.0\"", "");
                    XmlDocument xmlDoc1 = new XmlDocument();
                    xmlDoc1.LoadXml(nodeStr);
                    try
                    {
                        string endpointAddressOnMcu = xmlDoc1.SelectSingleNode("/Call/RemoteNumber").InnerText.Trim();
                        if (endpointAddressOnMcu == party.sAddress)
                        {
                            // party matches
                            // now get the slot number on the mcu
                            party.iMcuId = Int32.Parse(node.Attributes["item"].InnerXml.Trim());
                            logger.Trace("--Party ID on MCU : " + party.iMcuId.ToString());

                            // party name

                            // party status 

                            return true;
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Trace("Error getting party details out from status xml. Message = " + e.Message);
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool TerminateConference(NS_MESSENGER.Conference conf)
        {
            try
            {
                // assign the mcu
                this.tandbergMCU = conf.cMcu;

                // search for this particular conf on the bridge
                int confIdOnMcu = 0;
                bool ret = FindOngoingConference(conf, ref confIdOnMcu);
                if (!ret)
                {
                    this.errMsg = "Ongoing conf not found on the mcu.";
                    return false;
                }

                // send the command to terminate it
                ret = false; string terminateXML = null;
                ret = CreateXML_TerminateConference(confIdOnMcu, ref terminateXML);
                if (!ret)
                {
                    this.errMsg = "Invalid xml.";
                    return false;
                }

                // send the transaction
                ret = false; string responseXML = null;
                ret = SendTransaction("command", terminateXML, ref responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid transaction.";
                    return false;
                }

                // process the response
                ret = false;
                ret = ProcessXML_TerminateConference(responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid response from the MCU.";
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool CreateXML_TerminateConference(int confIdOnMcu, ref string terminateXML)
        {
            try
            {
                terminateXML = "<Command>";
                terminateXML += "<ConferenceTerminate>";
                terminateXML += "<Conference>" + confIdOnMcu.ToString() + "</Conference>";
                terminateXML += "</ConferenceTerminate>";
                terminateXML += "</Command>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool CreateXML_MuteParty(int partyIdOnMcu, bool mute, ref string mutePartyXML)
        {
            try
            {
                mutePartyXML = "<Command>";
                mutePartyXML += "<CallMute>";
                mutePartyXML += "<Call>" + partyIdOnMcu.ToString() + "</Call>";
                if (mute)
                {
                    mutePartyXML += "<Mode>Off</Mode>";
                }
                else
                {
                    mutePartyXML += "<Mode>On</Mode>";
                }
                mutePartyXML += "</CallMute>";
                mutePartyXML += "</Command>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        private bool CreateXML_DisconnectParty(int partyIdOnMcu, ref string disconnectPartyXML)
        {
            try
            {
                disconnectPartyXML = "<Command>";
                disconnectPartyXML += "<DisconnectCall>";
                disconnectPartyXML += "<Call>" + partyIdOnMcu.ToString() + "</Call>";
                disconnectPartyXML += "</DisconnectCall>";
                disconnectPartyXML += "</Command>";

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        internal bool ExtendConfEndTime(NS_MESSENGER.Conference conf, int extendTimeInMins)
        {
            try
            {
                // assign the mcu
                this.tandbergMCU = conf.cMcu;

                // find the conference on the mcu
                int confIdOnMcu = 0;
                bool ret = FindOngoingConference(conf, ref confIdOnMcu);
                if (!ret)
                {
                    this.errMsg = "Conference not found on the mcu.";
                    return false;
                }

                // add the duration
                conf.iDuration = conf.iDuration + extendTimeInMins;

                // modify the conference
                ret = false; string confXML = null;
                ret = CreateXML_ModifyConference(conf, confIdOnMcu, true, false, ref confXML);
                if (!ret)
                {
                    this.errMsg = "Invalid xml.";
                    return false;
                }

                // send the transaction
                ret = false; string responseXML = null;
                ret = SendTransaction("command", confXML, ref responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid transaction";
                    return false;
                }

                // Process the response 
                ret = false;
                ret = ProcessXML_ModifyConference(responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid response recived from the Tandberg MCU.";
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        internal bool TerminateEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party)
        {
            try
            {
                // assign the mcu
                this.tandbergMCU = party.cMcu;

                // find ongoing conf 
                int confIdOnMcu = 0;
                bool ret = FindOngoingConference(conf, ref confIdOnMcu);
                if (!ret)
                {
                    this.errMsg = "Ongoing conference not found on Tandberg MCU.";
                    return false;
                }

                // find ongoing participant 
                ret = false; int partyIdOnMcu = 0;
                ret = FindOngoingParticipant(party, confIdOnMcu, ref partyIdOnMcu);
                if (!ret)
                {
                    this.errMsg = "Ongoing participant not found on the Tandberg MCU.";
                    return false;
                }

                // create xml 
                ret = false; string disconnectPartyXML = null;
                ret = CreateXML_DisconnectParty(partyIdOnMcu, ref disconnectPartyXML);
                if (!ret)
                {
                    this.errMsg = "Invalid xml.";
                    return false;
                }

                // send transaction
                ret = false; string responseXML = null;
                ret = SendTransaction("command", disconnectPartyXML, ref responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid transaction.";
                    return false;
                }

                // process the response
                ret = false;
                ret = ProcessXML_DisconnectParty(responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid response recived from the Tandberg MCU.";
                    return false;
                }


                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        internal bool MuteEndpoint(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, bool mute)
        {
            try
            {
                // assign the mcu
                this.tandbergMCU = party.cMcu;

                int confIdOnMcu = 0;
                // find ongoing conf id on mcu
                bool ret = FindOngoingConference(conf, ref confIdOnMcu);
                if (!ret)
                {
                    this.errMsg = "Ongoing conference not found on the MCU.";
                    return false;
                }

                // find party 
                int partyIdOnMcu = 0; ret = false;
                ret = FindOngoingParticipant(party, confIdOnMcu, ref partyIdOnMcu);
                if (!ret)
                {
                    this.errMsg = "Ongoing participant not found on MCU.";
                    return false;
                }

                // create xml
                ret = false; string mutePartyXML = null;
                ret = CreateXML_MuteParty(partyIdOnMcu, mute, ref mutePartyXML);
                if (!ret)
                {
                    this.errMsg = "Invalid xml.";
                    return false;
                }

                // send transaction
                ret = false; string responseXML = null;
                ret = SendTransaction("command", mutePartyXML, ref responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid transaction.";
                    return false;
                }

                // process the xml
                ret = false;
                ret = ProcessXML_MuteParty(responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid response.";
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        internal bool ChangeConfDisplayLayout(NS_MESSENGER.Conference conf, int displayLayout)
        {
            try
            {
                // assign the mcu
                this.tandbergMCU = conf.cMcu;

                // search for this particular conf on the bridge
                int confIdOnMcu = 0;
                bool ret = FindOngoingConference(conf, ref confIdOnMcu);
                if (!ret)
                {
                    return false;
                }

                // create xml 
                ret = false; string confXML = null;
                ret = CreateXML_ModifyConference(conf, confIdOnMcu, false, true, ref confXML);
                if (!ret)
                {
                    this.errMsg = "Invalid xml.";
                    return false;
                }

                // process the transaction
                ret = false; string responseXML = null;
                ret = SendTransaction("command", confXML, ref responseXML);
                if (!ret)
                {
                    return false;
                }

                // process the transaction
                ret = false;
                ret = ProcessXML_ModifyConference(responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid response.";
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        internal bool ChangePartyDisplayLayout(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, int displayLayout)
        {
            this.errMsg = "Tandberg MCU doesn't support participant layout changes. Please use the conference layout option.";
            return false;
        }

        internal bool AddPartyToMcu(NS_MESSENGER.Conference conf, NS_MESSENGER.Party party, int confIdOnMcu)
        {
            try
            {
                bool ret = false;
                if (confIdOnMcu == 0)
                {
                    // find ongoing conf
                    ret = FindOngoingConference(conf, ref confIdOnMcu);
                    if (!ret)
                    {
                        this.errMsg = "Ongoing conference not found on MCU.";
                        return false;
                    }
                }

                // create xml
                ret = false; string addPartyXML = null;
                ret = CreateXML_AddParty(party, confIdOnMcu, ref addPartyXML);
                if (!ret)
                {
                    this.errMsg = "Invalid xml";
                    return false;
                }

                // send transaction
                ret = false; string responseXML = null;
                ret = SendTransaction("command", addPartyXML, ref responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid transaction.";
                    return false;
                }

                // process xml
                ret = false;
                ret = ProcessXML_AddParty(responseXML);
                if (!ret)
                {
                    this.errMsg = "Invalid response.";
                    return false;
                }

                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        internal bool FetchAllOngoingConfsStatus(NS_MESSENGER.MCU mcu, ref Queue mcuConfq)
        {
            try
            {
                // assign the mcu
                this.tandbergMCU = mcu;

                // find ongoing confs on mcu
                Queue confq = new Queue();
                bool ret = FindOngoingConferenceList(ref confq);
                if (!ret)
                {
                    this.errMsg = "Ongoing conferences not found on the MCU.";
                    return false;
                }

                // find party 
                while (confq.Count > 0)
                {
                    // get the conf from list
                    NS_MESSENGER.Conference conf = new NS_MESSENGER.Conference();
                    conf = (NS_MESSENGER.Conference)confq.Dequeue();

                    // fetch party list
                    ret = false;
                    ret = FindOngoingParticipantList(conf, ref mcuConfq);
                    if (!ret)
                    {
                        this.errMsg = "Ongoing participants for the conf not found on MCU.";
                        continue;
                    }

                    // add the conf
                    mcuConfq.Enqueue(conf);
                }
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        //New Methods Added for FB 1552 - Start

        internal bool GetEndpointStatus(NS_MESSENGER.Conference conf,ref NS_MESSENGER.Party party)
        {
            try
            {
                // assign the mcu
                this.tandbergMCU = party.cMcu;

                // find ongoing conf 
                int confIdOnMcu = 0;
                bool ret = FindOngoingConference(conf, ref confIdOnMcu);
                if (!ret)
                {
                    this.errMsg = "Ongoing conference not found on Tandberg MCU.";
                    return false;
                }

                // find ongoing participant 
                ret = false; int partyIdOnMcu = 0;
                ret = FindOngoingParticipantStatus(ref party, confIdOnMcu, ref partyIdOnMcu);
                if (!ret)
                {
                    ret = AddPartyToMcu(conf,party,confIdOnMcu);
                    if (!ret)
                    {
                        party.etStatus = NS_MESSENGER.Party.eOngoingStatus.DIS_CONNECT;
                        this.errMsg = "Ongoing participant not be added to Tandberg MCU.";
                        return false;
                    }
                    //this.errMsg = "Ongoing participant not found on the Tandberg MCU.";
                    //return false;
                }

               return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }

        
        private bool FindOngoingParticipantStatus(ref NS_MESSENGER.Party party, int confIdOnMcu, ref int partyIdOnMcu)
        {
            try
            {
                // get complete "status" from mcu
                string responseXML = null;
                bool ret = GetStatus(ref responseXML);
                if (!ret)
                {
                    logger.Trace("Invalid status response.");
                    return false;
                }

                // find the "party id on mcu" from the list of ongoing calls(connected participants)
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(responseXML);
                //XmlNamespaceManager nsMgr = new XmlNamespaceManager(xmlDoc.NameTable);
                //nsMgr.AddNamespace("def", "http://www.tandberg.no/XML/CUIL/1.0");
                //XmlNodeList nodes = xmlDoc.SelectNodes("/def:Status/def:Call[1][@status=\"Synced\"][@conferenceRef=\"" + confIdOnMcu.ToString() + "\"]", nsMgr);
                XmlNodeList nodes = xmlDoc.SelectNodes("/Status/Call[1][@status=\"Synced\"][@conferenceRef=\"" + confIdOnMcu.ToString() + "\"]");
                logger.Trace("Total count of matching nodes: " + nodes.Count.ToString());

                foreach (XmlNode node in nodes)
                {
                    logger.Trace("---Node found: " + node.OuterXml.Trim());
                    logger.Trace("Replace the xmlns with blank space..");
                    string nodeStr = node.OuterXml.Trim().Replace("xmlns=\"http://www.tandberg.no/XML/CUIL/1.0\"", "");
                    XmlDocument xmlDoc1 = new XmlDocument();
                    xmlDoc1.LoadXml(nodeStr);
                    try
                    {
                        string endpointAddressOnMcu = xmlDoc1.SelectSingleNode("/Call/RemoteNumber").InnerText.Trim();
                        if (endpointAddressOnMcu == party.sAddress)
                        {
                            // party matches
                            // now get the slot number on the mcu
                            party.etStatus = NS_MESSENGER.Party.eOngoingStatus.FULL_CONNECT;
                            partyIdOnMcu = Int32.Parse(node.Attributes["item"].InnerXml.Trim());
                            logger.Trace("--Party ID on MCU : " + partyIdOnMcu.ToString());

                            return true;
                        }
                    }
                    catch (Exception e)
                    {
                        logger.Trace("Error getting party details out from status xml. Message = " + e.Message);
                    }
                }

                return false;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        //New Method Added for FB 1552 - End

        //ZD 100369_MCU Start
        internal bool FetchMCUDetails(NS_MESSENGER.MCU mcu, int Timeout)
        {
            NS_DATABASE.Database db = new NS_DATABASE.Database(configParams);
            try
            {
                this.tandbergMCU = mcu;
                string responseXML = null, statusXML = null;
                int pollstatus = (int)NS_MESSENGER.MCU.PollState.PASS;

                // send transaction
                bool ret = SendMCUStatusCommand("status", statusXML, ref responseXML, Timeout);
                if (!ret)
                {
                    logger.Trace("Invalid response.");
                    pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                }
                else if (responseXML.Length < 1)
                {
                    pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                }
                else
                {
                    // Process XML
                    ret = false;
                    ret = ProcessXML_TestMcuConnection(responseXML, mcu.sSoftwareVer);
                    if (!ret)
                    {
                        logger.Trace("Invalid response.");
                        this.errMsg = "Connection to MCU failed. Please recheck the MCU information. Message : " + this.errMsg;
                        pollstatus = (int)NS_MESSENGER.MCU.PollState.FAIL;
                    }
                }
                db.UpdateMcuStatus(mcu, pollstatus);
                return true;
            }
            catch (Exception e)
            {
                logger.Exception(100, e.Message);
                return false;
            }
        }
        //ZD 100369_MCU End
    }
}