/* Copyright (C) 2015 myVRM - All Rights Reserved
* You may not use, distribute and/or modify this code under the
* terms of the myVRM license.
*
* You should have received a copy of the myVRM license with
* this file. If not, please write to: sales@myvrm.com, or visit: www.myvrm.com
*/
//ZD 100147 ZD 100886
using System;
using System.Collections;
using System.Collections.Generic;
using System.EnterpriseServices; //FB 2671
using System.Xml;

using NHibernate;
using NHibernate.Criterion;
using NHibernate.Mapping;//FB 2671

namespace myVRM.DataLayer
{
    public class GeneralDAO
    {
        private ISession session;
        private log4net.ILog m_log;
        private string m_configPath;
        
        public GeneralDAO(string config, log4net.ILog olog)
        {
            try
            {
                m_log = olog;
                m_configPath = config;

                bool valid = Init();
                if (!valid)
                    throw new Exception("Could not initialize GeneralDAO");

            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
            }
        }
        private bool Init()
        {
            try
            {
                session = SessionManagement.GetSession(m_configPath);
                return true;
            }
            catch (Exception e)
            {
                m_log.Error("sytemException", e);
                return false;
            }
        }
        public ICountryDAO GetCountryDAO()
        {
            return new CountryDAO(m_configPath);
        }
        //FB 2671 Start
        public IPublicCountryDAO GetPublicCountryDAO()
        {
            return new PublicCountryDAO(m_configPath);
        }

        public IPublicCountryStateDAO GetPublicCountryStateDAO()
        {
            return new PublicCountryStateDAO(m_configPath);
        }
        public IPublicCountryStateCityDAO GetPublicCountryStateCityDAO()
        {
            return new PublicCountryStateCityDAO(m_configPath);
        }
        //FB 2671 Ends
        public IStateDAO GetStateDAO()
        {
            return new StateDAO(m_configPath);
        }
        public class CountryDAO :AbstractPersistenceDao<vrmCountry, int>, ICountryDAO
        {
            public CountryDAO(string ConfigPath) : base(ConfigPath) { }
            public new List<vrmCountry> GetActive()
            {
                List<int> SelectedValues = new List<int>();
                SelectedValues.Add(0);
                SelectedValues.Add(1);
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("Selected", SelectedValues));
                return GetByCriteria(criterionList);
            }
            public vrmCountry GetCountryById(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("CountryID", Id));
                List<vrmCountry> Countries = GetByCriteria(criterionList);
                if (Countries.Count > 0)
                    return Countries[0];
                else
                    return null;
            }
        }
        //FB 2671 Starts
        public class PublicCountryDAO : AbstractPersistenceDao<vrmPublicCountry, int>, IPublicCountryDAO
        {
            public PublicCountryDAO(string ConfigPath) : base(ConfigPath) { }
            public new List<vrmPublicCountry> GetCountries()
            {
                List<vrmPublicCountry> rt = new List<vrmPublicCountry>();
                try
                {
                    using (ISession sess = SessionManagement.GetSession(SessionFactoryConfigPath)) //sessions.OpenSession())
                    {
                        ICriteria crit = sess.CreateCriteria(typeof(vrmPublicCountry));
                        rt = (List<vrmPublicCountry>)crit.List<vrmPublicCountry>();
                    }
                }
                catch (Exception ex)
                {
                }
                return rt;
            }
        }
        /**/
        public class PublicCountryStateDAO : AbstractPersistenceDao<vrmPublicCountryState, int>, IPublicCountryStateDAO
        {
            public PublicCountryStateDAO(string ConfigPath) : base(ConfigPath) { }
            public new List<vrmPublicCountryState> GetCountryStates(int countryId)
            {
                List<vrmPublicCountryState> states = new List<vrmPublicCountryState>();
                try
                {
                    using (ISession sess = SessionManagement.GetSession(SessionFactoryConfigPath)) //sessions.OpenSession())
                    {
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("CountryID", countryId));
                        states = GetByCriteria(criterionList);

                        //ICriteria crit = sess.CreateCriteria(typeof(vrmPublicCountryState));
                        //rt = (List<vrmPublicCountryState>)crit.List<vrmPublicCountryState>();
                    }
                }
                catch (Exception ex)
                {

                }
                return states;
            }
        }
        ////////
        public class PublicCountryStateCityDAO : AbstractPersistenceDao<vrmPublicCountryStateCity, int>, IPublicCountryStateCityDAO
        {
            public PublicCountryStateCityDAO(string ConfigPath) : base(ConfigPath) { }
            public new List<vrmPublicCountryStateCity> GetCountryStateCities(int countryId, int stateId)
            {
                List<vrmPublicCountryStateCity> cities = new List<vrmPublicCountryStateCity>();
                try
                {
                    using (ISession sess = SessionManagement.GetSession(SessionFactoryConfigPath)) //sessions.OpenSession())
                    {
                        List<ICriterion> criterionList = new List<ICriterion>();
                        criterionList.Add(Expression.Eq("CountryID", countryId));
                        if (stateId > 0)
                            criterionList.Add(Expression.Eq("StateID", stateId));
                        cities = GetByCriteria(criterionList);
                    }
                }
                catch (Exception ex)
                {

                }
                return cities;
            }
        }
        /**/
        //FB 2671 Ends
        public class StateDAO : AbstractPersistenceDao<vrmState, int>, IStateDAO
        {
            public StateDAO(string ConfigPath) : base(ConfigPath) { }
            public new List<vrmState> GetActive()
            {
                List<int> SelectedValues = new List<int>();
                SelectedValues.Add(0);
                SelectedValues.Add(1);
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.In("Selected", SelectedValues));
                return GetByCriteria(criterionList);
            }
            public vrmState GetStateById(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Id", Id));
                List<vrmState> States = GetByCriteria(criterionList);
                if (States.Count > 0)
                    return States[0];
                else
                    return null;
            }
        }

        //FB 1830 start
        public ILanguageDAO GetLanguageDAO()
        {
            return new LanguageDAO(m_configPath);
        }
        public class LanguageDAO : AbstractPersistenceDao<vrmLanguage, int>, ILanguageDAO
        {
            public LanguageDAO(string ConfigPath) : base(ConfigPath) { }
            public vrmLanguage GetLanguageById(int Id)
            {
                List<ICriterion> criterionList = new List<ICriterion>();
                criterionList.Add(Expression.Eq("Id", Id));
                List<vrmLanguage> langs = GetByCriteria(criterionList);
                if (langs.Count > 0)
                    return langs[0];
                else
                    return null;
            }
        }
        //FB 1830 end
        //NewLobby start
        public IIconsRefDAO GetIconRefDAO()
        {
            return new IconsRefDAO(m_configPath);
        }
        public class IconsRefDAO : AbstractPersistenceDao<vrmIconsRef, int>, IIconsRefDAO
        {
            public IconsRefDAO(string ConfigPath) : base(ConfigPath) { }
        }
        //NewLobby end

        //FB 2693 Start
        public IPCDao GetPCDao()
        {
            return new PCDao(m_configPath);
        }
        public class PCDao : AbstractPersistenceDao<vrmPCDetails, int>, IPCDao
        {
            public PCDao(string ConfigPath) : base(ConfigPath) { }
        }
        //FB 2693 End
    }
}
